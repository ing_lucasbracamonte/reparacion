<?php
require('fpdf.php');

class PDF extends FPDF
{
//Cabecera de p�gina
function Header()
{
    //Logo
    $this->Image('template/default/image/logo2.jpg',10,8,33);
    //Arial bold 15
    $this->SetFont('Arial','B',12);
    //Movernos a la derecha
    $this->Cell(80);
    //T�tulo
    $this->Cell(40,10,'MINISTERIO DE EDUCACION - GOBIERNO DE SANTA FE',0,0,'C');
    $this->Ln(4);
    $this->Cell(80);
    $this->Cell(40,15,'SISTEMA DE GESTION DE PROYECTOS PEDAGOGICOS',0,0,'C');
    //Salto de l�nea
    $this->Ln(4);
}
// titulo del reporte
function Titulo($titulo_reporte)
{
    $this->Ln(4);
    $this->Cell(80);
    $this->Cell(40,15,$titulo_reporte,0,0,'C');
    //Salto de l�nea
    $this->Ln(4);
}
//Pie de p�gina
function Footer()
{
    //Posici�n: a 1,5 cm del final
    $this->SetY(-15);
    //Arial italic 8
    $this->SetFont('Arial','I',8);
    //N�mero de p�gina
    $this->Cell(0,10,'P�gina: '.$this->PageNo().'/{nb}',0,0,'C');
}
}