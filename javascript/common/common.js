$(document).on({
    ajaxStart: function () {
        $("body").addClass("loading");
    },
    ajaxStop: function () {
        $("body").removeClass("loading");
    }
});


function guardar(formulario, controller, action) {

    $.ajax({
        type: 'POST',
        url: 'index.php?controller=' + controller + '&action=' + action,
        data: $('#' + formulario).serialize(),
        async: true,
        success: function (data) {},
        error: function (error) {}
        
    });
}

$(document).ready(function () {
    $('.sidebar-menu > ul > li:has(ul)').addClass('desplegable');
    $('.sidebar-menu > ul > li > a').click(function () {
        var comprobar = $(this).next();
        $('.sidebar-menu li').removeClass('active');
        $(this).closest('li').addClass('active');
        if ((comprobar.is('ul')) && (comprobar.is(':visible'))) {
            $(this).closest('li').removeClass('active');
            comprobar.slideUp('normal');
        }
        if ((comprobar.is('ul')) && (!comprobar.is(':visible'))) {
            $('.sidebar-menu ul ul:visible').slideUp('normal');
            comprobar.slideDown('normal');
        }
    });
    $('.sidebar-menu > ul > li > ul > li:has(ul)').addClass('desplegable');
    $('.sidebar-menu > ul > li > ul > li > a').click(function () {
        var comprobar = $(this).next();
        $('.sidebar-menu ul ul li').removeClass('active');
        $(this).closest('ul ul li').addClass('active');
        if ((comprobar.is('ul ul')) && (comprobar.is(':visible'))) {
            $(this).closest('ul ul li').removeClass('active');
            comprobar.slideUp('normal');
        }
        if ((comprobar.is('ul ul')) && (!comprobar.is(':visible'))) {
            $('.sidebar-menu ul ul ul:visible').slideUp('normal');
            comprobar.slideDown('normal');
        }
    });
});


$(function () {
    
    $('.select2').select2();
    
    $('.MiDateTimePicker').datetimepicker({
        language: 'es',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0,
        pickerPosition: 'bottom-left'
    });
});

$(function () {
    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();
});

function estiloBotones() {
    $("button").each(function () {
        if ($(this).attr("primary") != undefined) {
            props = {icons: {primary: $(this).attr("primary")}}
        } else if ($(this).attr("secondary") != undefined) {
            props = {icons: {secondary: $(this).attr("secondary")}}
        } else {
            props = {};
        }
        $(this).button(props);
    });
}

function numeric(e, tipo)
{
    if (tipo == 'int') {
        return ((e.keyCode == 9) || (e.keyCode == 8) || (e.keyCode > 32 && e.keyCode < 41) || (e.keyCode > 44 && e.keyCode < 58) || (e.keyCode > 94 && e.keyCode < 106));
    } else if (tipo == 'dec') {
        return ((e.keyCode == 9) || (e.keyCode == 8) || (e.keyCode == 110 || e.keyCode == 190) || (e.keyCode > 32 && e.keyCode < 41) || (e.keyCode > 44 && e.keyCode < 58) || (e.keyCode > 94 && e.keyCode < 106));
    }
}

function IsNumeric(valor)
{
    var log = valor.length;
    var sw = "S";
    for (x = 0; x < log; x++)
    {
        v1 = valor.substr(x, 1);
        v2 = parseInt(v1);
        //Compruebo si es un valor numérico 
        if (isNaN(v2)) {
            sw = "N";
        }
    }
    if (sw == "S") {
        return true;
    } else {
        return false;
    }
}

var primerslap = false;
var segundoslap = false;
function formateafecha(fecha)
{
    var long = fecha.length;
    var dia;
    var mes;
    var ano;

    if ((long >= 2) && (primerslap == false)) {
        dia = fecha.substr(0, 2);
        if ((IsNumeric(dia) == true) && (dia <= 31) && (dia != "00")) {
            fecha = fecha.substr(0, 2) + "/" + fecha.substr(3, 7);
            primerslap = true;
        } else {
            fecha = "";
            primerslap = false;
        }
    } else
    {
        dia = fecha.substr(0, 1);
        if (IsNumeric(dia) == false)
        {
            fecha = "";
        }
        if ((long <= 2) && (primerslap = true))
        {
            fecha = fecha.substr(0, 1);
            primerslap = false;
        }
    }
    if ((long >= 5) && (segundoslap == false))
    {
        mes = fecha.substr(3, 2);
        if ((IsNumeric(mes) == true) && (mes <= 12) && (mes != "00")) {
            fecha = fecha.substr(0, 5) + "/" + fecha.substr(6, 4);
            segundoslap = true;
        } else {
            fecha = fecha.substr(0, 3);
            ;
            segundoslap = false;
        }
    } else {
        if ((long <= 5) && (segundoslap = true)) {
            fecha = fecha.substr(0, 4);
            segundoslap = false;
        }
    }
    if (long >= 7)
    {
        ano = fecha.substr(6, 4);
        if (IsNumeric(ano) == false) {
            fecha = fecha.substr(0, 6);
        } else {
            if (long == 10) {
                if ((ano == 0) || (ano < 1900) || (ano > 2100)) {
                    fecha = fecha.substr(0, 6);
                }
            }
        }
    }

    if (long >= 10)
    {
        fecha = fecha.substr(0, 10);
        dia = fecha.substr(0, 2);
        mes = fecha.substr(3, 2);
        ano = fecha.substr(6, 4);
        // Año no viciesto y es febrero y el dia es mayor a 28 
        if ((ano % 4 != 0) && (mes == 02) && (dia > 28)) {
            fecha = fecha.substr(0, 2) + "/";
        }
    }
    return (fecha);
}

function OcultarDIV(classDIV)
{
    $(classDIV).toggle(false);
}

function MostrarDIV(classDIV)
{
    $(classDIV).toggle(true);
}

setTimeout(function () {
    $('input[focusablefirst="true"]').focus();
}, 1000);

setTimeout(function () {
    $('select[focusablefirst="true"]').focus();
}, 2000);


//iCheck for checkbox and radio inputs
$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
    checkboxClass: 'icheckbox_minimal-blue',
    radioClass: 'iradio_minimal-blue'
});
//Red color scheme for iCheck
$('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
    checkboxClass: 'icheckbox_minimal-red',
    radioClass: 'iradio_minimal-red'
});
//Flat red color scheme for iCheck
$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
    checkboxClass: 'icheckbox_flat-red',
    radioClass: 'iradio_flat-red'
});
//Flat red color scheme for iCheck
$('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-green'
});
