/**
 * Loading Modal
 */
$(body).on({

	// When ajaxStart is fired, add 'loading' to body class
	ajaxStart: function() {
		
		$(this).addClass('modal-loading');

		// After 6 seconds, change button text
		timer = setTimeout(function() {
			$('.nav-previous a').html("Still Loading...");
		}, 6000);

		// After 12 seconds, die. Reload page from scratch
		timer = setTimeout(function() {
			$(this).removeClass('modal-loading');
			$('.nav-previous a').html("Error has occured. Reloading page.");
			window.location.reload();
    		}, 12000);

	},

	// When ajaxStop is fired, clear the timer and remove 'loading' from body class
	ajaxStop: function() { 
		clearTimeout(timer);
		$(this).removeClass('modal-loading');
	}

});