<?php
if(!defined('VALID_ACCESS')){ header('Location: index.php');exit('Forbidden Path');}

function getbasepath($basepath='.') {
	if (@chdir($basepath)) { $basepath=getcwd(); }
	@chdir(dirname(__FILE__));
	return $basepath.DIRECTORY_SEPARATOR;
}

// CODE VERSION
define('CODE_VERSION','0.1');

// HTTP, HTTPS y DB
include 'config_online.php';

// PATH
define('PATH_IMAGE','image');
define('PATH_LIBRARY','library');
define('PATH_CACHE','cache');
define('PATH_DOWNLOAD','download');
define('PATH_ARCHIVOS','Archivos');
define('PATH_FOTOPERSONA','fotopersona');
define('PATH_ACTIVIDAD','actividad');
define('PATH_PROGRAMAS','programa');
define('PATH_MATERIALES','materiales');
define('PATH_LOGS','logs');
define('PATH_CONTROLLER','controller');
define('PATH_EXTENSION','extension');
define('PATH_LANGUAGE','language');
define('PATH_TEMPLATE','template');
define('PATH_STYLES','css');
define('PATH_COMMON','common');
define('DIR_INCLUDES',__DIR__);
define('DIR_BASE', getbasepath());
define('WP_MEMORY_LIMIT', '1024M');
// Tamaño maximo en bytes para subir fotos
define('TAMANIO_MAXIMO',1024000);


// DIR
if (!defined('DIR_IMAGE')) define('DIR_IMAGE', DIR_BASE.PATH_IMAGE.D_S);
if (!defined('DIR_LIBRARY')) define('DIR_LIBRARY', DIR_BASE.PATH_LIBRARY.D_S);
if (!defined('DIR_CACHE')) define('DIR_CACHE', DIR_BASE.PATH_CACHE.D_S);
if (!defined('DIR_DOWNLOAD')) define('DIR_DOWNLOAD', DIR_BASE.PATH_DOWNLOAD.D_S);
if (!defined('DIR_LOGS')) define('DIR_LOGS',DIR_BASE.PATH_LOGS.D_S);
if (!defined('DIR_CONTROLLER')) define('DIR_CONTROLLER', DIR_BASE.PATH_CONTROLLER.D_S);
if (!defined('DIR_EXTENSION')) define('DIR_EXTENSION', DIR_BASE.PATH_EXTENSION.D_S);
if (!defined('DIR_LANGUAGE')) define('DIR_LANGUAGE', DIR_BASE.PATH_LANGUAGE.D_S);
if (!defined('DIR_TEMPLATE')) define('DIR_TEMPLATE', DIR_BASE.PATH_TEMPLATE.D_S);
if (!defined('DIR_COMMON')) define('DIR_COMMON', DIR_BASE.PATH_LIBRARY.D_S.PATH_COMMON.D_S);
if (!defined('DIR_ARCHIVOS_MATERIALES')) define('DIR_ARCHIVOS_MATERIALES', PATH_ARCHIVOS.D_S.PATH_MATERIALES.D_S);
if (!defined('DIR_FOTOPERSONA')) define('DIR_FOTOPERSONA', PATH_ARCHIVOS.D_S.PATH_FOTOPERSONA.D_S);
if (!defined('IMAGE_NONE')) define('IMAGE_NONE', DIR_FOTOPERSONA.'Sin imagen disponible.jpg');

// PHP DEFINES
define('PHP_EXT','.'.pathinfo(__FILE__,PATHINFO_EXTENSION));
define('PHP_INDEX','index'.PHP_EXT);

// COMMON INCLUSION (PHP_COMPAT)
$files=glob(DIR_COMMON.'*'.PHP_EXT);
if ($files) {
	foreach ($files as $file) {
		if (basename($file) != PHP_INDEX) { include($file); }
	}
}

if (!is_writable(DIR_LOGS)) exit('Error: Could not write to logs directory!');

//Configuracion del idioma de php para la fecha en castellano
//setlocale(LC_ALL,array("es_ar","es_es","esp","es"));

//Modificacion para la hora
date_default_timezone_set('America/Argentina/Buenos_Aires');

//Mail de contacto
define('MAIL_CONTACTO', 'ing.lucasbracamonte@gmail.com');

//// Id de template de sendgrid para el registro
//define('TEMPLATE_REGISTRO','571d87e3-6896-4bb4-85ef-a7da3af86b0e');
//
//// Id de template de sendgrid para recuperar clave
//define('TEMPLATE_RECUPERAR','39bdf60c-9de1-48cc-b76d-1c888033843d');

// Id de template de sendgrid para mails masivos
define('MAIL_BATCH_TEMPLATE','12874138-7128-4d41-a156-0ac3f8e06eec');

// Cantidad de personas que se envía el mail por cada batch
define('MAIL_BATCH_PERSONAS',900);

// Mail que ven los que reciben, quién lo envió.
define('MAIL_BATCH_FROM_MAIL','basesdestel@gmail.com');
//define('MAIL_BATCH_FROM_MAIL','ing.lucasbracamonte@gmail.com');

// Nombre que ven los que reciben, quién lo envió.
define('MAIL_BATCH_FROM_NOMBRE','Reparacion');

//Página inicial
define('DEFAULT_HOMEPAGE', 'home');
?>
