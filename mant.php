<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>En mantenimiento :: UAR</title>
        <link rel="shortcut icon" href="mantenimiento/favicon.ico"></link>
        <link rel="stylesheet" type="text/css" href="mantenimiento/default.css"></link>
        <link rel="stylesheet" type="text/css" href="mantenimiento/header.css"></link>
        <link rel="stylesheet" type="text/css" href="mantenimiento/footer.css"></link>

    </head>

    <body>
        <div id="wrapper">
            <div id="header_contenedor">
                <div id="header_barra_titulo">    
                    <div id="header_fecha_hora"><?php echo htmlentities(ucfirst(strftime("%A, %d de %B de %Y")), ENT_QUOTES); ?></div>
                    <div>
                        <img src="mantenimiento/LOGO_UAR.png" class="right" alt="Unión de Rugby Argentino" title="UAR" />
                    </div>
                </div>
            </div>
            <div id="contenido" align="center">
                <div style="padding-top:150px;">
                    <h1>Estamos construyendo...</h1>
                    <h2 style="padding-bottom:60px;">Coaching UAR</h2>
                </div>
                <div align="right" style="padding-bottom:20px;">
                    <img src="mantenimiento/cono.png" />
                </div>
            </div>
            <div id="footer_pie_pagina">
                <div id="footer_pie_izquierda"><br />UNION DE RUGBY ARGENTINO
                </div>
                <div id="footer_pie_derecha">&copy; <?php echo(strftime("%Y")); ?><br />Todos los derechos reservados</div>
            </div>
        </div>
    </body>
</html>
