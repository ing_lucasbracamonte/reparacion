<?php

// Security
ini_set('register_globals', 'Off');

if (ini_get('register_globals')) {
	exit('Error: register_globals is enabled!');
}

define('VALID_ACCESS', TRUE);

// Configuration
require('config.php');

// Page Time
$time = (time() + microtime());

// Locator
require(DIR_LIBRARY . 'locator.php');
$locator = new Locator();

// Config
$config =& $locator->get('config');

// Database
$database =& $locator->get('database');

// Settings
$settings = $database->getRows("select * from setting where type = 'admin' or type = 'global'");

foreach ($settings as $setting) { $config->set($setting['key'], $setting['value']); }
//date_default_timezone_set($config->get('config_time_zone') ? $config->get('config_time_zone') : 'UTC');
date_default_timezone_set('America/Argentina/Buenos_Aires');
if($config->get('error_handler_status')){
	$error_handler = & $locator->get('errorhandler');
	set_error_handler(array(&$error_handler, "handler"));
}



$session =& $locator->get('session');

// Language
$language =& $locator->get('language');

// Template
$template =& $locator->get('template');

// Character Set
$template->set('charset', $language->get('charset'));

// Text Direction
$template->set('direction', $language->get('direction'));

// Language Code
$template->set('code', $language->get('code'));

// Nosript
$template->set('noscript', $language->get('noscript'));

// Request
$request =& $locator->get('request');

// Base URL
$template->set('base', $request->isSecure()?HTTPS_SERVER:HTTP_SERVER);

$user =& $locator->get('user');
//usuario
$usuario = array('nombre' => $user->getNombreCompleto(),
                 'descripcionGrupo' => $user->getNombresGrupos(),
                'puntovtaasignado' => $user->getPuntovtaasignado(),
                'descpuntovtaasignado' => $user->getDescpuntovtaasignado()
    );
$template->set('usuario', $usuario);

$url =& $locator->get('url');
$template->set('LinkHome', $url->ssl('home'));

$template->set('MaxiSelaCome', $url->ssl('logout'));
$template->set('MaxiSelaComeDoblada', $url->ssl('misdatos'));
// Response
$response =& $locator->get('response');

// Controller
$controller =& $locator->get('controller');

// Controller Directory
$controller->setDirectory(DIR_CONTROLLER);

// Default Controller
$controller->setDefault(DEFAULT_HOMEPAGE, 'index');

// Error Controller
$controller->setError('error', 'index');

// Login
 $controladora = $request->get('controller');
 if (@$controladora == 'consutaestadoreparacion') {
    $controller->addPreAction('consutaestadoreparacion', 'index');
}
else{
     
     $controller->addPreAction('login', 'isLogged');
}


// Permission
//$controller->addPreAction('permission', 'hasPermission');
//
// Dispatch
$controller->dispatch($request);

// Output
$response->output();

// Parse Time
if ($config->get('config_parse_time')) {
	echo($language->get('text_time', round((time() + microtime()) - $time, 4)));
}
if ($config->get('config_query_count')) {
	echo($language->get('text_query_count', $database->countQueries()));
}
if ($config->get('config_query_log')) {
	$database->log_queries();
}


?>