<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $code; ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo $charset; ?>">
        <title><?php echo $title; ?> ::. Sistema de Gestión</title>
        <link rel="shortcut icon" href="template/default/image/favicon.ico">
        <base href="<?php echo $base; ?>">

        <script src="javascript/jquery-1.11.3.min.js"></script>
        <script src="javascript/jquery-ui.min.js"></script>
        <script src="javascript/jeoquery.js"></script>
        <script src="javascript/jquery-ui-datepicker-es.js"></script>
        <!-- NOTY JAVASCRIPT -->

        <link href="template/Theme/default.css" rel="stylesheet" type="text/css" />

        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.2 -->
        <link href="template/Theme/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />    
        <!-- FontAwesome 4.3.0 -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons 2.0.0 -->
        <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />    
        <!-- Theme style -->
        <link href="template/Theme/dist/css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- AdminLTE Skins. Choose a skin from the css/skins 
             folder instead of downloading all of them to reduce the load. -->
        <link href="template/Theme/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
        <!-- iCheck -->
        <link href="template/Theme/plugins/iCheck/flat/blue.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart
        <link href="plugins/morris/morris.css" rel="stylesheet" type="text/css" /> -->
        <!-- jvectormap -->
        <link href="template/Theme/plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- Date Picker -->
        <link href="template/Theme/plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="template/Theme/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="template/Theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />

        <script src="template/Theme/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
        <script src="template/Theme/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>

        <script src="template/Theme/plugins/bootstrap-select/chosen.jquery.js" type="text/javascript"></script>
        <link href="template/Theme/plugins/bootstrap-select/bootstrap-chosen.css" rel="stylesheet" type="text/css" />

        <link href="template/default/css/autocomplete.css" rel="stylesheet" type="text/css" />

        <!-- js custom -->
    <script src="javascript/common/common.js"></script>
    
        <script>
        $(document).on({
            ajaxStart: function() { $("body").addClass("loading"); },
            ajaxStop: function() { $("body").removeClass("loading"); }    
        });
        </script>
    </head>


    <body class="login-page">

        <!-- Right side column. Contains the navbar and content of the page -->
        <div class="login-box" >

            <?php echo $content; ?>

        </div>
        
        <div class="modal"></div>
        
    </body>

    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="template/Theme/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>    
    <!-- Morris.js charts 
    <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="plugins/morris/morris.min.js" type="text/javascript"></script>-->
    <!-- Sparkline -->
    <script src="template/Theme/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <!-- jvectormap -->
    <script src="template/Theme/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
    <script src="template/Theme/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
    <!-- jQuery Knob Chart -->
    <script src="template/Theme/plugins/knob/jquery.knob.js" type="text/javascript"></script>
    <!-- daterangepicker -->
    <script src="template/Theme/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <!-- datepicker -->
    <script src="template/Theme/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="template/Theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="template/Theme/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <!-- Slimscroll -->
    <script src="template/Theme/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='template/Theme/plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="template/Theme/dist/js/app.min.js" type="text/javascript"></script>

    <!DOCTYPE html>   
    
</html>
