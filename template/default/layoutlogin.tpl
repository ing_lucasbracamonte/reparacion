<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $code; ?>">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php echo $charset; ?>">
    <title><?php echo $title; ?> ::. Sistema de Gestión</title>
    <link rel="shortcut icon" href="template/default/image/favicon.ico">
    <base href="<?php echo $base; ?>">
    <link href="template/Theme/default.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap core CSS -->
    
     <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="javascript/assets/vendor/jquery.min.js"><\/script>')</script>
    <script src="javascript/bootstrap.min.js" type="text/javascript"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  
    <!-- js custom -->
    <script src="javascript/common/common.js"></script>
    
    <script src="javascript/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="javascript/jquery-ui.min.js" type="text/javascript"></script>
    <script src="javascript/jeoquery.js" type="text/javascript"></script>
    <script src="javascript/jquery-ui-datepicker-es.js" type="text/javascript"></script>
    <script>
        $(document).on({
            ajaxStart: function() { $("body").addClass("loading"); },
            ajaxStop: function() { $("body").removeClass("loading"); }    
        });
        
        $(document).tooltip({
          track: true
        });
    </script>
 </head>
    
     <?php echo $content; ?>   

    <div class="modal"></div>
    
    <script>  
        function cerrarMenu(){
           $("#btnMenu").click(); 
        }
    </script>          
    
</html>
