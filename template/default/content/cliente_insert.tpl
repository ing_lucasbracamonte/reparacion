<div id="cliente_insert">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formCliente" name="formCliente">
        <div class="form-group">
            <label><?php echo $entry_persona; ?></label>
            <?php if ($_REQUEST['action'] == 'update') { ?> 
            <input class="form-control" type="text" name="persona" id="persona" value="<?php echo $persona; ?>" readonly/>
            <?php } else { ?>
            <input class="form-control" type="text" name="persona" id="persona" maxlength="8" value="<?php echo $persona; ?>" placeholder="Campo obligatorio"/>
            <?php } ?> 
        </div>
        <div class="form-group">
            <label><?php echo $entry_apellido; ?></label>
            <input class="form-control" type="text" name="apellido" id="apellido" value="<?php echo $apellido; ?>"  placeholder="Campo obligatorio"/>
            <input type="hidden" name="accion_form" value="<?php echo $accion_form; ?>" />
        </div>
        <div class="form-group">
            <label><?php echo $entry_nombre; ?></label>
            <input class="form-control" type="text" name="nombre" id="nombre" value="<?php echo $nombre; ?>"  placeholder="Campo obligatorio"/>
        </div>
        <div class="form-group">
            <label><?php echo $entry_tipocliente; ?></label>
            <select class="form-control" tabindex="2" name="tipocliente" id="tipocliente">
                <option value="-1" selected>Seleccione...</option>
                <?php foreach ($tiposcliente as $loc) {       
                if ($loc['tipocliente'] == $tipocliente){ ?>
                <option value="<?php echo $loc['tipocliente']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                <?php } else{ ?>
                <option value="<?php echo $loc['tipocliente']; ?>"><?php echo $loc['descripcion']; ?></option>
                <?php } ?>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label><?php echo $entry_direccion; ?></label>
            <div class="row">
                <div class="col-xs-9"><input class="form-control" type="text" name="direccion" value="<?php echo $direccion; ?>" placeholder="Calle"/></div>
                <div class="col-xs-1"><input class="form-control" type="text" name="numero" value="<?php echo $numero; ?>" placeholder="Nro"/></div>
                <div class="col-xs-1"><input class="form-control" type="text" name="piso" value="<?php echo $piso; ?>"  placeholder="Piso"/></div>
                <div class="col-xs-1"><input class="form-control" type="text" name="departamento" value="<?php echo $departamento; ?>"  placeholder="Dpto"/></div>
            </div>
        </div>
        <div class="form-group">
            <label><?php echo $entry_localidad; ?></label>
            <input type="input" class="form-control" name="auto_localidad" id="auto_localidad" value="<?php echo $auto_localidad; ?>" placeholder="Ingrese 3 letras y seleccione una opción de la lista"/>
            <input type="hidden" name="auto_localidad_cliente" id="auto_localidad_cliente" value="<?php echo $auto_localidad_cliente; ?>" />                    
        </div> 
        <div class="form-group">
            <label><?php echo $entry_fecha; ?></label>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control" value="<?php echo $fechanacimiento; ?>" id="fechanacimiento" name="fechanacimiento" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" placeholder="Campo obligatorio">
            </div>
        </div>
        <div class="form-group">
            <label><?php echo $entry_celular; ?></label>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                </div>
                <input type="text" class="form-control" name="celular" id="celular" value="<?php echo $celular; ?>" >
            </div>
        </div>
        <div class="form-group">
            <label><?php echo $entry_mail; ?></label>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="text" class="form-control" name="mail"  value="<?php echo $mail; ?>" placeholder="Email">
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        $("#auto_localidad").autocomplete({
            source: "<?php echo $script_busca_localidades; ?>",
            minLength: 3,
            select: function (event, ui) {
                $("#auto_localidad_cliente").val(ui.item.id);
            }
        });
    });

</script>
