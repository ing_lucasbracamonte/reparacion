<!-- Content Header (Page header) -->
<section class="content-header">
    <h3><?php echo $heading_title; ?></h3>
    <!-- Error -->
    <?php if ($error_texto_error != '') { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error_texto_error; ?>
    </div>
    <?php } ?> 
    <!-- Error -->
</section>

<!-- Main content -->
<section class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Registro</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <label><?php echo $entry_persona; ?></label>
                    <?php if ($_REQUEST['action'] == 'update') { ?> 
                    <input class="form-control" type="text" name="persona" value="<?php echo $persona; ?>" readonly/>
                    <?php } else { ?>
                    <input class="form-control" type="text" name="persona" maxlength="8" value="<?php echo $persona; ?>" placeholder="Campo obligatorio"/>
                    <?php } ?>
                </div>
                <div class="form-group">
                    <label><?php echo $entry_apellido; ?></label>
                    <input class="form-control" type="text" name="apellido" value="<?php echo $apellido; ?>"  placeholder="Campo obligatorio"/>
                    <input type="hidden" name="accion_form" value="<?php echo $accion_form; ?>" />
                </div>
                <div class="form-group">
                    <label><?php echo $entry_nombre; ?></label>
                    <input class="form-control" type="text" name="nombre" value="<?php echo $nombre; ?>"  placeholder="Campo obligatorio"/>
                </div>
                <?php if ($vendedor != 'no') { ?>  
                <div class="form-group">
                    <label><?php echo $entry_puntovta; ?></label>
                    <select class="chosen-select" tabindex="2" name="puntovtaasignado" id="puntovtaasignado" >
                        <?php foreach ($puntosdeventa as $loc) {       
                        if ($loc['puntovta'] == $puntovtaasignado){ ?>
                        <option value="<?php echo $loc['puntovta']; ?>" selected><?php echo $loc['descpuntoventa']; ?></option>
                        <?php } else{ ?>
                        <option value="<?php echo $loc['puntovta']; ?>"><?php echo $loc['descpuntoventa']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <?php } ?> 
                <div class="form-group">
                    <label><?php echo $entry_fecha; ?></label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control" value="<?php echo $fechanacimiento; ?>" id="fechanacimiento" name="fechanacimiento" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" placeholder="Campo obligatorio">
                    </div>
                </div>
                <div class="form-group">
                    <label><?php echo $entry_celular; ?></label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                        </div>
                        <input type="text" class="form-control" value="<?php echo $celular; ?>" name="celular" id="celular">
                    </div>
                </div>
                <div class="form-group">
                    <label><?php echo $entry_localidad; ?></label>
                    <input class="form-control" type="input" name="localidad" value="<?php echo $localidad; ?>" id="localidad"/>
                </div>
                <div class="form-group">
                    <label><?php echo $entry_mail; ?></label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        <input type="text" class="form-control" name="mail" value="<?php echo $mail; ?>" placeholder="Email">
                    </div>
                </div>
                <div class="form-group">
                    <label><?php echo $entry_remail; ?></label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        <input type="text" class="form-control"  name="remail" value="<?php echo $remail; ?>" placeholder="Email">
                    </div>
                </div>
                <div class="form-group">
                    <label><?php echo $entry_newpassword; ?></label>
                    <input class="form-control" type="password" name="newpassword" value="<?php echo $newpassword; ?>"  />
                </div>  
                <div class="form-group">
                    <label><?php echo $entry_newrepassword; ?></label>
                    <input class="form-control" type="password" name="newrepassword" value="<?php echo $newrepassword; ?>"  />
                </div> 
            </div>
        </div>
        <!-- BOTONES INICIO -->
        <div class="box-footer" style="text-align: -webkit-center;">
            <?php if ($_REQUEST['action'] != 'consulta') { ?>  
            <button type="submit" class="btn btn-primary" onclick="document.getElementById('form').submit();">Guardar</button>
            <?php } ?>   
            <button type="button" class="btn btn-primary" onclick="location = '<?php echo $cancel; ?>'">Cancelar</button>
        </div>
        <!-- BOTONES FIN -->
    </form>
</section>

<script type="text/javascript">
    
    $(function () {
        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
        //Money Euro
        $("[data-mask]").inputmask();
    });
    
</script>
