<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formABM" name="form">
    <div class="generated-table" >
        <table class="gradient-style" style="width: 430px">
            <tr>
                <td><span class="required">*</span> <?php echo $entry_persona; ?></td>
                <td><input type="text" name="persona" value="<?php echo $persona; ?>" size="50" />
                    <?php if ($error_persona) { ?>
                    <span class="error"><?php echo $error_persona; ?></span>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_nombre; ?></td>
                <td><input  type="text" name="nombre" value="<?php echo $nombre; ?>" size="50" />
                    <?php if ($error_nombre) { ?>
                    <span class="error"><?php echo $error_nombre; ?></span>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td><?php echo $entry_localidad; ?></td>
                <td><input type="input" name="localidad" value="<?php echo $localidad; ?>" id="localidad"/>
                    <?php if ($error_localidad) { ?>
                    <span class="error"><?php echo $error_localidad; ?></span>
                    <?php } ?>
                </td> 
            </tr>
            <tr>
                <td> <?php echo $entry_domicilio; ?></td>
                <td><input type="text" name="domicilio" value="<?php echo $domicilio; ?>" size="50"/>
                    <?php if ($error_domicilio) { ?>
                    <span class="error"><?php echo $error_domicilio; ?></span>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_telefono; ?></td>
                <td><input type="text" name="telefono" value="<?php echo $telefono; ?>" size="50"/>
                    <?php if ($error_telefono) { ?>
                    <span class="error"><?php echo $error_telefono; ?></span>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_mail; ?></td>
                <td><input type="text" name="mail" value="<?php echo $mail; ?>" size="50"/>
                    <?php if ($error_mail) { ?>
                    <span class="error"><?php echo $error_mail; ?></span>
                    <?php } ?>              
                </td>
            </tr>
            <tr>
                <td  class="top"><span class="required">*</span><?php echo $entry_consulta; ?>
                    <?php if ($error_consulta) { ?>
                    <span class="error"><?php echo $error_consulta; ?></span>
                    <?php } ?>                
                </td>
                <td><textarea name="consulta" cols="50" rows="11" ><?php echo $consulta; ?></textarea></td>
            </tr>
            <td colspan="2">
                <br>
                <br>
                <button id="btGuardar" onclick="enviarContacto('<?php echo $actionBoton; ?>');
                        return false;" secondary="ui-icon ui-icon-triangle-1-e">Guardar</button>
                <button id="btCerrar" onclick="$('#popup').dialog('close');
                        return false;" secondary="ui-icon-close">Cancelar</button>
            </td>
        </table>
    </div> 
</form>