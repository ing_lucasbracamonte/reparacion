<!-- Content Header (Page header) -->
<section class="content-header">
    <h3><?php echo $heading_title; ?></h3>
    <!-- Error -->
    <?php if ($error_texto_error != '') { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error_texto_error; ?>
    </div>
    <?php } ?> 
    <!-- Error -->
</section>

<!-- Main content -->
<section class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <div class="box box-primary">
            <div class="box-header" style="float:right">
                <a href="<?php echo $exportXlsModelo; ?>"><i class="fa fa-cloud-download" style="font-size: 30px" alt="Modelo de carga" title="Modelo de carga"></i> 
                    Descargar modelo
                </a>
            </div>
            <div class="box-body">
                <input id="listaprecio" type="hidden" name="listaprecio" value="<?php echo $listaprecio; ?>">
                <div class="form-group">
                    <label for="exampleInputFile">Cargar archivo</label>
                    <input id="archivoupload" type="file" name="archivoupload" >
                    <p class="help-block">La importaci&oacute;n se ejecutar&aacute; seg&uacute;n el archivo modelo.</p>
                    <p class="help-block">Primero buscara si existe id de art&iacute;culo o c&oacute;digo de barra interno:</p>
                    <p class="help-block">* si no lo encuentra informar&aacute; el error.</p>
                    <p class="help-block">* si lo encuentra validara que haya ingresado un precio de venta correcto.</p>
                    <p class="help-block">Una vez que supera las validaciones, busca el art&iacute;culo en la lista de precio.</p>
                    <p class="help-block">* si lo encuentra actualiza el precio de venta para esa lista.</p>
                    <p class="help-block">* si no lo encuentra, agrega el art&iacute;culo a la lista de precios.</p>
                </div>
            </div>
        </div>
        <!-- BOTONES INICIO -->
        <div class="box-footer" style="text-align: -webkit-center;">
            <?php if ($_REQUEST['action'] != 'consulta') { ?>  
            <button type="submit" class="btn btn-primary" onclick="document.getElementById('form').submit();">Importar</button>
            <?php } ?>   
            <button type="button" class="btn btn-primary" onclick="location = '<?php echo $cancel; ?>'">Cancelar</button>
        </div>
        <!-- BOTONES FIN -->
    </form>
</section>

<script type="text/javascript">

    $(function () {
        $('.chosen-select').chosen();
        $('.chosen-select-deselect').chosen({allow_single_deselect: true});
    });

    function numeric(e, tipo)
    {
        if (tipo == 'int') {
            return ((e.keyCode == 9) || (e.keyCode == 8) || (e.keyCode > 32 && e.keyCode < 41) || (e.keyCode > 44 && e.keyCode < 58) || (e.keyCode > 94 && e.keyCode < 106));
        } else if (tipo == 'dec') {
            return ((e.keyCode == 9) || (e.keyCode == 8) || (e.keyCode == 110 || e.keyCode == 190) || (e.keyCode > 32 && e.keyCode < 41) || (e.keyCode > 44 && e.keyCode < 58) || (e.keyCode > 94 && e.keyCode < 106));
        }
    }

</script>