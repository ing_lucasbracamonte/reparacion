<!-- Content Header (Page header) -->
<section class="content-header">
    <h3><?php echo $heading_title; ?></h3>
    <!-- Error -->
    <?php if ($error_texto_error != '') { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error_texto_error; ?>
    </div>
    <?php } ?> 
    <!-- Error -->
</section>

<!-- Main content -->
<section class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Registro</h3>
            </div>
            <div class="box-body">
                <input type="hidden" value="<?php echo $puntovta; ?>" name="puntovta" id="puntovta">
                <div class="form-group">
                    <label><?php echo $entry_descripcion; ?></label>
                    <input class="form-control" type="text" name="descripcion" value="<?php echo $descripcion; ?>"  placeholder="Campo obligatorio"/>
                </div>
                <div class="form-group">
                    <label><?php echo $entry_direccion; ?></label>
                    <div class="row">
                        <div class="col-xs-9"><input class="form-control" type="text" name="direccion" value="<?php echo $direccion; ?>" placeholder="Calle"/></div>
                        <div class="col-xs-1"><input class="form-control" type="text" name="numero" value="<?php echo $numero; ?>" placeholder="Nro"/></div>
                        <div class="col-xs-1"><input class="form-control" type="text" name="piso" value="<?php echo $piso; ?>"  placeholder="Piso"/></div>
                        <div class="col-xs-1"><input class="form-control" type="text" name="departamento" value="<?php echo $departamento; ?>"  placeholder="Dpto"/></div>
                    </div>
                </div>
                <div class="form-group">
                    <label><?php echo $entry_localidad; ?></label>
                    <input type="input" class="form-control" name="auto_puntovta" id="auto_localidad" value="<?php echo $auto_localidad; ?>" placeholder="Ingrese 3 letras y seleccione una opción de la lista"/>
                    <input type="hidden" name="auto_localidad_puntovta" id="auto_localidad_puntovta" value="<?php echo $auto_localidad_puntovta; ?>" />                    
                </div> 
                <div class="form-group">
                    <label><?php echo $entry_zona; ?></label>
                    <select class="form-control" tabindex="2" name="zona" id="zona">
                        <option value="-1" selected>Seleccione...</option>
                        <?php foreach ($zonas as $loc) {       
                        if ($loc['zona'] == $zona){ ?>
                        <option value="<?php echo $loc['zona']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                        <?php } else{ ?>
                        <option value="<?php echo $loc['zona']; ?>"><?php echo $loc['descripcion']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label><?php echo $entry_empresa; ?></label>
                    <select class="form-control" tabindex="2" name="empresa" id="empresa">
                        <option value="-1" selected>Seleccione...</option>
                        <?php foreach ($empresas as $loc) {       
                        if ($loc['empresa'] == $empresa){ ?>
                        <option value="<?php echo $loc['empresa']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                        <?php } else{ ?>
                        <option value="<?php echo $loc['empresa']; ?>"><?php echo $loc['descripcion']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label><?php echo $entry_listaprecio; ?></label>
                    <select class="form-control" tabindex="2" name="listaprecio" id="listaprecio">
                        <option value="-1" selected>Seleccione...</option>
                        <?php foreach ($listasdeprecio as $loc) {       
                        if ($loc['listaprecio'] == $listaprecio){ ?>
                        <option value="<?php echo $loc['listaprecio']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                        <?php } else{ ?>
                        <option value="<?php echo $loc['listaprecio']; ?>"><?php echo $loc['descripcion']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div  class="form-group">
                    <label>
                        <input type="checkbox" name="puntovtapropio" value="puntovtapropio" style="width: 20px;margin-top: -3px;" <?php echo ($puntovtapropio == 1) ?  'checked' : '' ?>>
                               <?php echo $entry_puntovtapropio; ?>
                    </label>
                </div>

                <?php if ($_REQUEST['action'] == 'delete') { ?> 
                <div class="form-group">
                    <label><?php echo $entry_fechabaja; ?></label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control" value="<?php echo $fechabaja; ?>" id="fechabaja" name="fechabaja" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                    </div>
                </div>
                <div class="form-group">
                    <label><?php echo $entry_motivobaja; ?></label>
                    <input class="form-control" type="text" name="motivobaja" value="<?php echo $motivobaja; ?>" />
                </div>
                <?php } ?> 
            </div>
        </div>
        <!-- BOTONES INICIO -->
        <div class="box-footer" style="text-align: -webkit-center;">
            <?php if ($_REQUEST['action'] != 'consulta') { ?>  
            <button type="submit" class="btn btn-primary" onclick="document.getElementById('form').submit();">Guardar</button>
            <?php } ?>   
            <button type="button" class="btn btn-primary" onclick="location = '<?php echo $cancel; ?>'">Cancelar</button>
        </div>
        <!-- BOTONES FIN -->
    </form>
</section>

<script type="text/javascript">

    $(document).ready(function () {

        $("#auto_localidad").autocomplete({
            source: "<?php echo $script_busca_localidades; ?>",
            minLength: 3,
            select: function (event, ui) {
                $("#auto_localidad_puntovta").val(ui.item.id);
            }
        });
    });

</script>