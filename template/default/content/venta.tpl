<!-- Content Header (Page header) -->
<section class="content-header">
    <?php if ($validaciones_caja != '') { ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Error!</h4>
                <?php echo $validaciones_caja; ?>
                <?php } else{ ?>
    
    <h3><?php echo $heading_title; ?></h3>
    <!-- Error -->
    <?php if ($error_texto_error != '') { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error_texto_error; ?>
    </div>
    <?php } ?> 
    <!-- Error -->
    <?php } ?> 
</section>

<!-- Main content -->
<section class="content">
    
    
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <input class="form-control" type="hidden" id="listaprecio" name="listaprecio" value="<?php echo $listaprecio; ?>" readonly/>
        
        <div class="box box-primary" <?php if ($validaciones_caja != '') { ?> style="display:none" <?php } ?> >
            
            <div class="box-header">

            </div>
            
            <!-- DETALLE DE VENTA INICIO -->
            <div class="detalleventa" id="detalleventa" >
                <div class="box-body" id="box-body">
                    <div class="box">
                        <div class="row">  
                            <div class="col-md-6">
                                <table id="encavezadoventa" name="encavezadoventa" class="table table-condensed" aria-describedby="example1_info">

                                    <thead>
                                        <tr role="row" >
                                            <th  role="columnheader" tabindex="0" aria-controls="example2" rowspan="1" colspan="2">
                                            </th>
                                            <th  role="columnheader" tabindex="1" aria-controls="example2" rowspan="1" colspan="2">
                                            </th>
                                            <th  role="columnheader" tabindex="2" aria-controls="example2" rowspan="1" colspan="2">
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody aria-live="polite" aria-relevant="all">

                                    <input class="form-control" type="hidden" name="venta" value="<?php echo $venta; ?>" readonly/>

                                    <tr class="even"  >
                                        <td class="col-xs-4">
                                            <label><?php echo $entry_fecha; ?></label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control" value="<?php echo $fecha; ?>" id="fecha" name="fecha" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" placeholder="Campo obligatorio">
                                            </div>
                                        </td>
                                        <td class="col-xs-4">
                                            <label><?php echo $entry_vendedor; ?></label>
                                            <select class="form-control" tabindex="2" name="vendedor" id="vendedor">  
                                                <option value="-1" selected>Seleccione...</option>
                                                <?php foreach ($vendedores as $loc) {       
                                                if ($loc['persona'] == $vendedor){ ?>
                                                <option value="<?php echo $loc['persona']; ?>" selected><?php echo $loc['apellido']; ?>, <?php echo $loc['nombre']; ?></option>
                                                <?php } else{ ?>
                                                <option value="<?php echo $loc['persona']; ?>"><?php echo $loc['apellido']; ?>, <?php echo $loc['nombre']; ?></option>
                                                <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </td>
                                        <td class="col-xs-4">
                                            <label><?php echo $entry_puntovta; ?></label>
                                            <input class="form-control" type="hidden" id="puntovta" name="puntovta" value="<?php echo $puntovta; ?>" readonly/>
                                            <input class="form-control" type="hidden" id="puntovtaid" name="puntovtaid" value="<?php echo $puntovta; ?>" readonly/>
                                            <input class="form-control" type="text" name="descpuntovta" value="<?php echo $descpuntovta; ?>" readonly/>
                                        </td>
                                    </tr>
                                    <tr class="even"  >			
                                        <td class="col-xs-4">
                                            <label><?php echo $entry_tipofacturacion; ?></label>
                                            <select class="form-control" tabindex="2" name="tipofacturacion" id="tipofacturacion" readonly>                                       
                                                <?php foreach ($tiposfacturacion as $loc) {       
                                                if ($loc['tipofacturacion'] == $tipofacturacion){ ?>
                                                <option value="<?php echo $loc['tipofacturacion']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                                                <?php } else{ ?>
                                                <option value="<?php echo $loc['tipofacturacion']; ?>"><?php echo $loc['descripcion']; ?></option>
                                                <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </td>
                                        <td class="col-xs-4">
                                            <label><?php echo $entry_tipocomprobante; ?></label>
                                            <select class="form-control" tabindex="2" name="tipocomprobante" id="tipocomprobante" >
                                                <?php foreach ($tiposcomprobante as $loc) {       
                                                if ($loc['tipocomprobante'] == $tipocomprobante){ ?>
                                                <option value="<?php echo $loc['tipocomprobante']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                                                <?php } else{ ?>
                                                <option value="<?php echo $loc['tipocomprobante']; ?>"><?php echo $loc['descripcion']; ?></option>
                                                <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </td>
                                        <td class="col-xs-4">
                                            <label><?php echo $entry_personacomprador; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                            <input type="input" class="form-control" name="auto_personacomprador" id="auto_personacomprador" value="<?php echo $auto_personacomprador; ?>" placeholder="Ingrese 3 letras y seleccione una opción de la lista"/>
                                            <input type="hidden" name="auto_personacomprador_venta" id="auto_personacomprador_venta" value="<?php echo $auto_personacomprador_venta; ?>" />

                                        </td>
                                        <?php if ($_REQUEST['action'] != 'consulta') { ?> 
                                        <td class="col-xs-1">
                                            <label>&nbsp;</label>
                                            <button class="enabled" onclick="PersonaComprador();
                                                    return false;">
                                                <i class="fa fa-plus" style="font-size: 25px" alt="<?php echo $button_insert; ?>" title="Agregar"></i> 
                                            </button>
                                        </td>
                                        <?php } ?> 
                                    </tr>
                                    </tbody>
                                </table>
                            </div>   
                            <div class="col-md-5" style="padding-left: 5%;padding-top: 3%;padding-right: 5%;">
                                <div class="info-box" >
                                    <span class="info-box-icon bg-green" ><i class="ion ion-ios-cart-outline"></i></span>
                                    <div class="info-box-content">
                                        <input class="form-control" type="hidden" name="total" id="total" value="<?php echo $total; ?>" readonly/>
                                        <span class="info-box-text" align="center">Total</span>
                                        <span class="info-box-number" align="center" type="text" name="texttotal" id="texttotal" value="<?php echo $total; ?>" style="font-size: 28px"><?php echo $texttotal; ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <!-- PERSONA COMPRADOR INICIO -->
                    <div class="box divpersonacomprador" id="divpersonacomprador" style="display:none">
                        <section class="">
                            <h3><?php echo $heading_title_cliente; ?></h3>
                        </section>
                        <div id="divcliente_insert">

                        </div>

                        <!-- BOTONES INICIO  -->
                        <div class="box-footer" style="text-align: -webkit-center;">
                            <button type="button" class="btn btn-primary" onclick="GuardarCliente();
                                    return false;" >Guardar</button>
                            <button type="button" class="btn btn-primary" onclick="PersonaComprador();
                                    return false;">Cancelar</button>
                        </div>
                        <!-- BOTONES FIN -->
                    </div>
                    <!-- PERSONA COMPRADOR FIN -->
                    <div class="box">
                        <?php include 'venta_grilla_articulos.tpl' ?>  
                    </div>
                </div>               

                <!-- BOTONES INICIO  -->
                <div class="box-footer" style="text-align: -webkit-center;">
                    <button type="button" class="btn btn-primary" onclick="PasarAFormasDePagos();
                            return false;" >Formas de pago</button>
                    <button type="button" class="btn btn-primary" onclick="location = '<?php echo $cancel; ?>'">Cancelar</button>
                </div>
                <!-- BOTONES FIN -->
            </div>
            <!-- DETALLE DE VENTA FIN -->           

            <!-- FORMAS DE PAGO INICIO -->
            <div class="divformasdepago" id="divformasdepago" style="display:none">
                <div class="box-body" id="box-body">
                    <div class="box">
                        <div class="row">  
                            <div class="col-md-3" style="padding-left: 1%;padding-top: 3%;padding-right: 1%;">
                                <div class="info-box" >
                                    <span class="info-box-icon bg-green" ><i class="ion ion-ios-cart-outline"></i></span>
                                    <div class="info-box-content">
                                        <input class="form-control" type="hidden" name="total2" id="total2" value="<?php echo $total; ?>" readonly/>
                                        <span class="info-box-text" align="center">Total de la Venta</span>
                                        <span class="info-box-number" align="center" type="text" name="texttotal2" id="texttotal2" value="<?php echo $total; ?>" style="font-size: 28px"><?php echo $texttotal; ?></span>
                                    </div>
                                </div>
                            </div> 
                            <div class="col-md-3" style="padding-left: 1%;padding-top: 3%;padding-right: 1%;">
                                <div class="info-box" >
                                    <span class="info-box-icon bg-red" ><i class="ion ion-cash"></i></span>
                                    <div class="info-box-content">
                                        <input class="form-control" type="hidden" name="totalapagar" id="totalapagar" value="0" readonly/>
                                        <span class="info-box-text" align="center">Total a pagar</span>
                                        <span class="info-box-number" align="center" type="text" name="texttotalapagar" id="texttotalapagar" value="<?php echo $texttotalapagar; ?>" style="font-size: 28px"><?php echo $texttotalapagar; ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3" style="padding-left: 1%;padding-top: 3%;padding-right: 1%;">
                                <div class="info-box" >
                                    <span class="info-box-icon bg-red" ><i class="ion ion-cash"></i></span>
                                    <div class="info-box-content">
                                        <input class="form-control" type="hidden" name="totalrecargofinanciacion" id="totalrecargofinanciacion" value="0" readonly/>
                                        <span class="info-box-text" align="center">Recargo por financiación</span>
                                        <span class="info-box-number" align="center" type="text" name="texttotalrecargofinanciacion" id="texttotalrecargofinanciacion" value="<?php echo $texttotalrecargofinanciacion; ?>" style="font-size: 28px"><?php echo $texttotalrecargofinanciacion; ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3" style="padding-left: 1%;padding-top: 3%;padding-right: 1%;">
                                <div class="info-box" >
                                    <span class="info-box-icon bg-red" ><i class="ion ion-cash"></i></span>
                                    <div class="info-box-content">
                                        <input class="form-control" type="hidden" name="totalrestoapagar" id="totalrestoapagar" value="0" readonly/>
                                        <span class="info-box-text" align="center">Resto a pagar</span>
                                        <span class="info-box-number" align="center" type="text" name="texttotalrestoapagar" id="texttotalrestoapagar" value="<?php echo $texttotalrestoapagar; ?>" style="font-size: 28px"><?php echo $texttotalrestoapagar; ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 

                    <div class="box">
                        <?php include 'venta_grilla_formaspago.tpl' ?>  
                    </div>
                </div>               

                <!-- BOTONES INICIO  -->
                <div class="box-footer" style="text-align: -webkit-center;">
                    <button type="button" class="btn btn-primary" onclick="PasarADetalleVenta();return false;" >Detalle Venta</button>
                    <?php if ($_REQUEST['action'] != 'consulta') { ?>  
                    <button type="submit" class="btn btn-primary" onclick="saveInsert();return false;" >Finalizar</button>
                    <?php } ?>   
                    <button type="button" class="btn btn-primary" onclick="location = '<?php echo $cancel; ?>'">Cancelar</button>

                </div>
                <!-- BOTONES FIN -->

            </div>
            <!-- FOMAS DE PAGO FIN -->
            
        </div>
        
    </form>
       
</section>

<script type="text/javascript">

    $(document).ready(function () {

        $("#auto_personacomprador").autocomplete({
            source: "<?php echo $script_busca_personacomprador; ?>",
            minLength: 3,
            select: function (event, ui) {
                $("#auto_personacomprador_venta").val(ui.item.id);
            }
        });

        $("#auto_articulo").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "<?php echo $script_busca_articulo; ?>",
                    data: {
                        term: $("#auto_articulo").val(),
                        listaprecio: $("#listaprecio").val()
                    },
                    dataType: "json",
                    type: "GET",
                    success: function (data) {
                        response(data);
                    }
                });
            },
            minLength: 3,
            select: function (event, ui) {
                $("#auto_articulo_venta").val(ui.item.id);
                CargaLineaCarga();
            }
        });

        $("#auto_notacredito").autocomplete({
             
            source: function (request, response) {
                $.ajax({
                    url: "<?php echo $script_busca_notacredito; ?>",
                    data: {
                        term: $("#auto_notacredito").val(),
                        puntovta: $("#puntovta").val()
                    },
                    dataType: "json",
                    type: "GET",
                    success: function (data) {
                       
                        response(data);
                    }
                });
            },
            minLength: 3,
            select: function (event, ui) {

                $("#auto_notacredito_venta").val(ui.item.id);
                CargaLineaCargaNC(ui.item.id);
            }
        });

        LimpiaValoresArticulo();
        LimpiaValoresFormaDePago(true);
    });

    function CargaLineaCargaNC(id) {

        if (id != '-1' && id != null) {
            $.ajax({
                type: 'GET',
                url: 'index.php?controller=ventas&action=getNotaCredito',
                data: 'term=' + id,
                async: false,
                success: function (data) {
                    if (data != 'error') {
                        
                        //obtengo json y lo convierto en un ojetejo
                        var obj = $.parseJSON(data);
                        
                        $("#importe").val(obj["totalnc"]);
                        $("#totalLFP").val(obj["totalnc"]);
                        $('#importe').attr('disabled', true);
                        
                        CalculaTotales();
                        CalculoTotalLineaFormaPago(null, null, false);
                    } else {
                        alert('');
                    }
                }
            });
        }
    }

    function CargaLineaCarga() {

        var articulo_vta = $("#auto_articulo_venta").val();
        var puntovta = $("#puntovta").val();

        var articulo = articulo_vta.split('_');
        var articuloid = articulo[0];
        var articulocodbarra = articulo[1];
        
        if (articulo_vta != '-1') {
            $.ajax({
                type: 'GET',
                url: 'index.php?controller=ventas&action=getArticulo',
                data: 'term=' + articulo_vta + '&articuloid=' + articuloid + '&codbarra=' + articulocodbarra + '&puntovta=' + puntovta,
                async: false,
                success: function (data) {
                    if (data != 'error') {
                        
                        //obtengo json y lo convierto en un ojetejo
                        var obj = $.parseJSON(data);
                        
                        $("#articulo").val(obj["articulo"]);
                        
                        $("#descmodelo").val(obj["descripcion"]);
                        $("#concoddebarra").val(obj["concoddebarra"]);
                        $("#codbarrainterno").val(obj["codbarrainterno"]);
                        $("#codbarra").val(obj["codbarra"]);
                        $("#porcDescuento").val(0);
                        $("#descuento").val(0);
                        
                        $("#cantidad_maxima").val(obj["cantidad"]);
                        
                        $("#precioventa").val(obj["preciovta"]);
                        $("#precioventa_descuento").val(obj["preciovta"]);

                        if (obj["concoddebarra"] == '1') {
                            
                            //$("#codbarrainterno").val('');
                        } else {

                            $("#codbarra").val('');
                        }


                    } else {
                        alert('');
                    }
                }
            });
        }
    }

    function PersonaComprador() {
        $div = $("#divpersonacomprador");
        if ($div.css('display') != 'none') {
            OcultarDIV(".divpersonacomprador");
            $('#cliente_insert').remove();
        } else {
            MostrarDIV(".divpersonacomprador");

            $.ajax({
                type: 'GET',
                url: 'index.php?controller=ventas&action=getFormCliente',
                async: false,
                success: function (data) {
                    
                    $('#divcliente_insert').append(data);

                    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
                    $("[data-mask]").inputmask();
                }
            });
        }
    }

    function GuardarCliente() {
        //hacer validaciones aca

        var persona = $("#persona").val();
        var apellido = $("#apellido").val();
        var nombre = $("#nombre").val();
        var tipocliente = $("#tipocliente").val();
        var fechanacimiento = $("#fechanacimiento").val();
        
        if (persona == '') {
            alert('Debe ingresar el número de documento.');
            return;
        } else if (persona.length < '7') {
            alert('No es un número de documento válido.');
            return;
        } else if (apellido == '') {
            alert('Debe ingresar el apellido.');
            return;
        } else if (nombre == '') {
            alert('Debe ingresar el nombre.');
            return;
        } else if (fechanacimiento == '') {
            alert('Debe seleccionar una fecha de nacimiento.');
            return;
        } else if (tipocliente == '-1') {
            alert('Debe seleccionar un tipo.');
            return;
        }

        $.ajax({
            type: 'GET',
            url: 'index.php?controller=ventas&action=insertCliente',
            data: $('#formCliente').serialize(),
            async: false,
            success: function (data) {
                
                var cliente = $.parseJSON(data);
                alert('Se agregó el cliente.');
                PersonaComprador();

                $("#auto_personacomprador_venta").val(cliente["id"]);
                $("#auto_personacomprador").val(cliente["value"]);
            }
        });
    }

    function PasarAFormasDePagos() {
        OcultarDIV(".detalleventa");
        MostrarDIV(".divformasdepago");
    }

    function PasarAFormasDePagos() {
        OcultarDIV(".detalleventa");
        MostrarDIV(".divformasdepago");
    }

    function PasarADetalleVenta() {
        OcultarDIV(".divformasdepago");
        MostrarDIV(".detalleventa");
    }

    function PasarAFormasDePagos2() {
        //hacer validaciones aca

        var tipofacturacion = $("#tipofacturacion").val();
        var tipocomprobante = $("#tipocomprobante").val();
        var cant = $('#articulos tr').size();
        var ptovta = $("#puntovta").val();
        //alert(cant);
        if ($('#articulos tr').size() <= '2') {
            alert('Debe ingresar al menos un articulo.');
        } else if (tipofacturacion == 0) {
            alert('Debe seleccionar un tipo de facturación.');
        } else if (tipocomprobante == 0) {
            alert('Debe seleccionar un estado de comprobante.');
        } else if (ptovta == '') {
            alert('El vendedor no tiene asignado un punto de venta por defecto.');
        } else {
            //hacer: validaciones si un producto con codigo de barras no fue vendido ya.
            //si un producto no tiene stock.
            //si no hay producto en ese almacen.

            //SI PASA TODAS LAS VALIDACIONES MUESTRA DIV DE FORMAS DE PAGO
        }
    }

    function saveInsert() {

        var ptovta = $("#puntovtaid").val();
        var vendedor = $("#vendedor").val();

        var table = document.getElementById("formasdepagos");
        var totalrestoapagar = $("#totalrestoapagar").val();

        if (table.rows.length <= 0) {
            alert('Debe agregar al menos una forma de pago.');
        } else if (vendedor == '-1') {
            alert('Debe seleccionar un vendedor.');
        } else if (CalculaTotalFaltaPagar() > 0) {
            alert('La sumatoria de las formas de pago debe ser igual al total de la venta.');
        } else if (ptovta == '') {
            alert('El vendedor no tiene asignado un punto de venta por defecto.');
        } else {
            //hacer: validaciones si un producto con codigo de barras no fue vendido ya.
            //si un producto no tiene stock.
            //si no hay cantidad suficiente del producto producto en ese almacen.
            //tengo que recorrer los productos y verificar que tengan stock en el punto de venta

            $.ajax({
                type: 'POST',
                url: 'index.php?controller=ventas&action=insert',
                data: $('#form').serialize(),
                beforeSend: function () {
                    $("body").addClass("loading");
                },
                success: function (data) {
                    //alert(data);
                    if (data == 'error') {
                        $('#form').html(data);
                    } else {
                        window.location.href = data;
                    }
                }
            });
        }
    }

    function AgregarArticulo() {

        var articulo = $("#articulo").val();
        var venta = $("#venta").val();
        var cantidad = $("#cantidad").val();
        var cantidad_maxima = parseInt($("#cantidad_maxima").val());
        var codbarra = $("#codbarra").val();
        var porcDescuento = $("#porcDescuento").val();
        var descuento = $("#descuento").val();
        var precioventa = $("#precioventa").val();
        var precioventa_descuento = $("#precioventa_descuento").val();
        var codbarrainterno = $("#codbarrainterno").val();
        var cantReg = $('#articulos tr').size();
        var ptovta = $("#puntovta").val();
        var total = $("#total").val();

        if (articulo == '-1') {
            alert('Antes de agregar un artículo debe seleccionar uno.');
            return;
        } else if ($("#concoddebarra").val() == '1' && codbarra == '') {
            alert('El artículo seleccionado necesita que se le cargue un código de barra.');
            return;
        } else if (cantidad == 0) {
            alert('Debe ingresar una cantidad.');
            return;
        } else if (cantidad < 0) {
            alert('La cantidad debe ser mayor a 0.');
            return;
        } else if (cantidad > cantidad_maxima) {
            alert('La cantidad NO debe ser mayor a ' + cantidad_maxima);
            $("#cantidad").val('1');
            return;
        } else if (precioventa == 0) {
            alert('Debe ingresar un precio de venta.');
            return;
        } else if (ptovta == '') {
            alert('El vendedor no tiene asignado un punto de venta por defecto.');
            return;
        } else {

            //antes de ir a buscarlo y agregar la linea
            //, validar que si es de codigo de barras ya no se encuentre asignado en la grilla
            
            var productoYaCargado = 'no';            
            $('input[id^="articulosvta[][codbarra]"]').each(function() {
                if (codbarra != '') {
                    var texto = $(this).val();
                    //alert(texto);
                    var n = texto.includes(codbarra);
                    if (n) {
                        productoYaCargado = 'si';
                    }
                }
            });          
                        
            if (productoYaCargado == 'si') {
                alert('El producto ya se encuentra cargado');
                return;
            }

            //var descuento = (precioventa * porcDescuento) / 100;
            $.ajax({
                type: 'GET',
                url: 'index.php?controller=ventas&action=AgregarArticulo',
                data: 'venta=' + venta + '&articulo=' + articulo + '&cantidad=' + cantidad + '&codbarra=' + codbarra + '&codbarrainterno=' + codbarrainterno + '&porcDescuento=' + porcDescuento + '&descuento=' + descuento + '&precioventa=' + precioventa + '&precioventa_descuento=' + precioventa_descuento + '&cantReg=' + cantReg,
                async: false,
                success: function (data) {
                    if (data != 'error') {

                        $('#articulos').append(data);

                        var tot = cantidad * precioventa_descuento;

                        var totalvta = Number(tot) + Number(total);
                        $("#total").val(totalvta);

                        LimpiaValoresArticulo();
                        LimpiaValoresFormaDePago(true);
                        CalculaTotales();
                        CalculoTotalLineaFormaPago(null, null, false);

                        $('#cantidad').attr('disabled', false);
                    } else {
                        alert('Surgió un error.');
                    }

                }
            });
             
        }
    }

    function removeArticulo(row, total_fila) {
        //controla que si hay forma de pago no deje eliminar
        var cantformadepago = $('#formasdepagos tr').size();
        if (cantformadepago > 2) {
            alert('No puede eliminarse ningun articulo, ya que hay formas de pagos asignadas.');
            return;
        }

        $('#' + row).remove();
        var cantarticulo = $('#articulos tr').size();

        var totalvta = $("#total").val() - total_fila;

        $("#total").val(totalvta);

        LimpiaValoresFormaDePago(true);
        CalculaTotales();

    }

    function AgregarFormaPago() {

        var formapago_vta = $("#formapago_vta").val();
        var venta = $("#venta").val();
        var cantReg = $('#formasdepagos tr').size();
        var tarjeta = $("#tarjeta").val();
        var banco = $("#banco").val();
        var nrotarjeta = $("#nrotarjeta").val();
        var cuotas = $("#cuotas").val();
        var importe = $("#importe").val();
        var totalLFP = $("#totalLFP").val();
        var recargo = $("#recargo").val();
        var financiacion = $("#financiacion").val();
        var notacredito = $("#auto_notacredito_venta").val();

        var total = $("#total").val();
        var totalformapago = $("#totalformapago").val();
        var totalapagar = $("#totalapagar").val();
        var totalPagos = Number(totalformapago) + Number(totalLFP);
        var restoapagar = $("#totalrestoapagar").val();

        //debugger;
        if (Number(total) == 0) {
            alert('Antes de agregar una forma de pago debe ingresar al menos un artículo.');
        } else if (formapago_vta == 'CHE' && (banco == '-1' || nrotarjeta == '')) {
            if (banco == '-1') {
                alert('Debe seleccionar un banco.');
            } else {
                if (nrotarjeta == '') {
                    alert('Debe ingresar un número de Cheque.');
                }
            }
        } else if (formapago_vta == 'TRA' && (banco == '-1' || nrotarjeta == '')) {
            if (banco == '-1') {
                alert('Debe seleccionar un banco.');
            } else {
                if (nrotarjeta == '') {
                    alert('Debe ingresar un número de Transferencia.');
                }
            }
        } else if (formapago_vta != 'NC' && formapago_vta != 'CHE' && formapago_vta != 'EFE' && formapago_vta != 'TRA' && (tarjeta == '-1' || banco == '-1' || nrotarjeta == '' || Number(cuotas) < 1)) {
            if (tarjeta == '-1') {
                alert('Debe seleccionar una tarjeta.');
            } else {
                if (banco == '-1') {
                    alert('Debe seleccionar un banco.');
                } else {
                    if (formapago_vta != 'TD' && nrotarjeta == '') {
                        alert('Debe ingresar el CPE.');
                    } else {
                        if (Number(cuotas) < 1) {
                            alert('Debe ingresar una cantidad valida de cuotas.');
                        }
                    }
                }
            }
        } else if (formapago_vta != 'NC' && formapago_vta != 'CHE' && formapago_vta != 'EFE' && formapago_vta != 'TRA' && (tarjeta == '-1' || banco == '-1' || nrotarjeta == '' || Number(cuotas) < 1)) {
            if (tarjeta == '-1') {
                alert('Debe seleccionar una tarjeta.');
            } else {
                if (banco == '-1') {
                    alert('Debe seleccionar un banco.');
                } else {
                    if (formapago_vta != 'TD' && nrotarjeta == '') {
                        alert('Debe ingresar el CPE.');
                    } else {
                        if (Number(cuotas) < 1) {
                            alert('Debe ingresar una cantidad valida de cuotas.');
                        }
                    }
                }
            }
        } else if (formapago_vta == 'NC' && notacredito == '') {
            alert('Debe seleccionar una NC.');
        } else if (totalLFP <= 0) {
            alert('Debe ingresar un importe.');
        } else if (totalLFP > 0 && restoapagar < 0) {

            alert('El total de las formas de pago no puede superar el total de lo que se tiene que pagar.');
        } else {

            //antes de ir a buscarloy agregar la linea, 
            //validar que si la forma de pago ya no se encuentre asignado en la grilla
                        
            var formaPagoYaCargado = 'no';            
            $('input[id^="ventaformasdepago[][ventaformapago]"]').each(function() {
                if (formapago_vta == 'EFE') {
                    var texto = $(this).val();
                    //alert(texto);
                    var n = texto.includes(formapago_vta);
                    if (n) {
                        formaPagoYaCargado = 'si';
                    }
                }
            });
            
            if (formaPagoYaCargado == 'si') {
                alert('La forma de pago efectivo ya se encuentra cargada.');
                return;
            }
            
            $.ajax({
                type: 'GET',
                url: 'index.php?controller=ventas&action=AgregarFormaPago',
                data: 'venta=' + venta + '&formapago=' + formapago_vta + '&financiacion=' + financiacion + '&tarjeta=' + tarjeta + '&banco=' + banco + '&nrotarjeta=' + nrotarjeta + '&notacredito=' + notacredito + '&cuotas=' + cuotas + '&importe=' + importe + '&recargo=' + recargo + '&totalLFP=' + totalLFP + '&cantReg=' + cantReg,
                async: false,
                success: function (data) {

                    if (data != 'error') {

                        $('#formasdepagos').append(data);

                        LimpiaValoresFormaDePago(true);

                    } else {
                        alert('Surgió un error.');
                    }
                }
            });            
        }
    }

    function removeVentaFormaPago(row) {
        $('#' + row).remove();

        CalculaTotales();
        LimpiaValoresFormaDePago(true);
        CalculoTotalLineaFormaPago(null, null, false);
    }

    function GetFinanciacion() {
        var financiacion = $("#financiacion").val();
        $.ajax({
            dataType: "html",
            type: "GET",
            url: "index.php?controller=ventas&action=getFinanciacion",
            data: "financiacion=" + financiacion,
            beforeSend: function (data)
            {

            },
            success: function (requestData)
            {
                //busco la financiacion
                //si tiene tarjeta, banco, cuotas, la precargo y las dejo desabilitadas
                //si la financiacion no tiene tarjeta cargo el combo de tarjeta y dejo que seleccione
                //si la financiacion no tiene banco cargo el combo de banco y dejo que seleccione
                //si la financiacion tiene cuotas precargar las cuotas sino dejar que cargue
                //si la financiacion tiene recargo cargarlo y calcular el importe si cargo

                //alert(idTipoproducto);
                if (financiacion != '-1') {
                    $('#financiacion').removeAttr('disabled');
                } else {
                    //dejo todo desabilitado hasta que no seleccione una forma de financiacion distinta a -1
                    LimpiaValoresFormaDePago(true);
                }

                $("#financiacion").html(requestData);  //Usando JQUERY, Cargamos las subcategorias
            },
            error: function (requestData, strError, strTipoError)
            {
                alert("Error " + strTipoError + ': ' + strError); //En caso de error mostraremos un alert
            },
            complete: function (requestData, exito) { }
        });
    }

    function GetFinanciaciones() {
        var formapago_vta = $("#formapago_vta").val();
        $.ajax({
            dataType: "html",
            type: "GET",
            url: "index.php?controller=ventas&action=getFinanciaciones",
            data: "formapago_vta=" + formapago_vta,
            beforeSend: function (data)
            {
                $("#financiacion").html('<option value="-1" selected="">Cargando...</option>');
            },
            success: function (requestData)
            {

                if (formapago_vta != '-1') {
                    $('#financiacion').removeAttr('disabled');
                    if (formapago_vta == 'EFE') {
                        LimpiaValoresFormaDePago(false);
                        CalculaTotales();

                        $('#importe').removeAttr('disabled');
                        //en financiacion seleccion efectivo y lo desabilito
                        $("#financiacion").html(requestData);
                        $('#financiacion').val('1');
                        $('input[name=financiacion]').val($('#financiacion').val());
                        $('#financiacion').attr('disabled', 'disabled');
                        
                        MostrarDIV(".codverificacion");
                        OcultarDIV(".nronotacredito");

                    } else if (formapago_vta == 'NC') {
                        LimpiaValoresFormaDePago(false);
                        CalculaTotales();

                        
                        //en financiacion seleccion efectivo y lo desabilito
                        $("#financiacion").html(requestData);
                        $('#financiacion').val('0');
                        $('input[name=financiacion]').val($('#financiacion').val());
                        $('#financiacion').attr('disabled', 'disabled');
                        
                        //tengo que habilitar el codigo de verificacion
                        $('#nronotacredito').removeAttr('disabled');
                        $('#auto_notacredito').removeAttr('disabled');
                        
                        $("#nronotacredito").val('');
                        
                        MostrarDIV(".nronotacredito");
                        OcultarDIV(".codverificacion");
        
                    } else {
                        $('#auto_notacredito').attr('disabled', 'disabled');
                        $('#nrotarjeta').attr('disabled', 'disabled');
                        
                        MostrarDIV(".codverificacion");                        
                        OcultarDIV(".nronotacredito");
                        
                        $('#importe').attr('disabled', 'disabled');
                        $("#financiacion").html(requestData);
                    }

                } else {
                    $('#financiacion').attr('disabled', 'disabled');                 
       
                    LimpiaValoresFormaDePago(false);
                    CalculaTotales();
                }

            },
            error: function (requestData, strError, strTipoError)
            {
                alert("Error " + strTipoError + ': ' + strError); //En caso de error mostraremos un alert
            },
            complete: function (requestData, exito) { }
        });
    }

    function calculaDescuento() {
        var precioventa = $("#precioventa").val();
        var porcDescuento = $("#porcDescuento").val();

        var descuento = (precioventa * porcDescuento) / 100;
        $("#descuento").val(descuento);

        var precioventaDesc = precioventa - descuento;
        $("#precioventa_descuento").val(precioventaDesc);
    }

    function CargaLineaCargaFinanciacion() {
        var financiacion = $("#financiacion").val();

        if (financiacion != '' && financiacion != '-1') {
            $.ajax({
                type: 'GET',
                url: 'index.php?controller=ventas&action=getFinanciacion',
                data: 'financiacion=' + financiacion,
                async: false,
                success: function (data) {
                    var obj = $.parseJSON(data); //obtengo json y lo convierto en un ojetejo
                    if (obj != null) {
                        //obtengo json y lo convierto en un ojetejo
                        if (obj["tarjeta"] > 0) {
                            $('#tarjeta').val(obj["tarjeta"]);
                            $('input[name=tarjeta]').val(obj["tarjeta"]);
                            $('#tarjeta').attr('disabled', 'disabled');
                            
                        } else {
                            $('#tarjeta').removeAttr('disabled');
                        }

                        if (obj["banco"] > 0) {
                            $('#banco').val(obj["banco"]);
                            $('input[name=banco]').val(obj["banco"]);
                            $('#banco').attr('disabled', 'disabled');
                        } else {
                            $('#banco').removeAttr('disabled');
                        }

                        $("#cuotas").val(obj["cuotas"]);
                        $("#textcuotas").val(obj["textcuotas"]);
                        $("#recargo").val(obj["recargo"]);
                        $('#importe').removeAttr('disabled');

                        if (obj["formapago"] == 'TAR' || obj["formapago"] == 'DEB') {
                            $('#nrotarjeta').removeAttr('disabled');
                        }

                        CalculoTotalLineaFormaPago(null, null, false);

                    } else {
                        //desabilito y blanqueo todo
                        LimpiaValoresFormaDePago(true);
                    }
                }
            });

        } else {
            //desabilito y blanqueo todo
            LimpiaValoresFormaDePago(true);

        }
    }

    function CalculoTotalLineaFormaPago(e, tipo, validaKey) {
        //consulto si me interesa valida o no la key de ingreso.
        if (validaKey) {
            if (tipo == 'int') {
                return ((e.keyCode == 9) || (e.keyCode == 8) || (e.keyCode > 32 && e.keyCode < 41) || (e.keyCode > 44 && e.keyCode < 58) || (e.keyCode > 94 && e.keyCode < 106));
            } else if (tipo == 'dec') {
                if (!((e.keyCode == 9) || (e.keyCode == 8) || (e.keyCode == 110 || e.keyCode == 190) || (e.keyCode > 32 && e.keyCode < 41) || (e.keyCode > 44 && e.keyCode < 58) || (e.keyCode > 94 && e.keyCode < 106)))
                {
                    return;
                }
            }
        }

        var recargo = $("#recargo").val();
        var importe = $("#importe").val();
        
        var cantidadDecimales =($("#importe").val() + "").split(".")[1];
        if (cantidadDecimales != undefined) {
            var cantidadDecimales = ($("#importe").val() + "").split(".")[1].length;
            if (cantidadDecimales > 2) {
                
                importe = Number($("#importe").val()).toFixed(2);
                $("#importe").val(importe);
            }
        }
        
        if (Number.isNaN(parseFloat(importe)) == true) {
            importe = 0.00;
        }
        if (Number.isNaN(parseFloat(recargo)) == true) {
            recargo = 0.00;
        }

        if (recargo != 0) {
            var totalrecargo = importe * (recargo / 100);
            var total = parseFloat(importe) + parseFloat(totalrecargo);
            $("#totalLFP").val(parseFloat(total).toFixed(2));
            //el total recargo se lo sumo a total a pagar, total recargo y resto a pagar
        } else
        {
            $("#totalLFP").val(parseFloat(importe).toFixed(2));
        }

        CalculaTotales();
    }

    function CalculaTotales() {

        //recorrer la grilla y recalcular el total
        var totalformadepago = 0;
        var table = document.getElementById("formasdepagos");
        var totalpaga = $("#totalLFP").val();
        var importe = $("#importe").val();
        var cuotas = $("#cuotas").val();
        var precioxcuota = totalpaga / cuotas;

        if (Number.isNaN(parseFloat(importe)) == true) {
            importe = 0.00;
        }
        var totalrecargoporfinanciacion = totalpaga - importe; //va la diferencia entre el importe y el totallfp del encabezado
        for (var i = 0; i < table.rows.length; i++) {

            var valohtml = table.rows[i].cells[6].innerHTML; //importe
            var recahtml = table.rows[i].cells[7].innerHTML; //recargo
            var totalfphtml = table.rows[i].cells[8].innerHTML; //total

            var valorimporte = valohtml.split(">");
            var valorrecargo = recahtml.split(">");
            var valortotal = totalfphtml.split(">");

            if (Number.isNaN(parseFloat(valortotal[1])) == false) {

                var tot = valortotal[1]; //importe

                totalformadepago = parseFloat(tot) + parseFloat(totalformadepago);
                var recargolfp = parseFloat(valortotal[1]) - parseFloat(valorimporte[1]);
                totalrecargoporfinanciacion = parseFloat(totalrecargoporfinanciacion) + parseFloat(recargolfp);
                totalpaga = parseFloat(totalpaga) + parseFloat(tot);

            }
        }

        var totalventabruta = $("#total").val();

        if (totalventabruta == '0.00') {
            return;
        }
       
        var totalventamasrecargo = parseFloat(totalventabruta) + parseFloat(totalrecargoporfinanciacion);
        var totfaltapagar = parseFloat(totalventamasrecargo) - parseFloat(totalpaga);
        
        //si el total es negativo setea el importe 0.00 y vuelve a calcular.
        
        if (totfaltapagar < 0) {
            $("#importe").val('0.00');
            CalculoTotalLineaFormaPago(null, null, false);
            return;
        }
        
        document.getElementById('texttotal').innerHTML = '$ ' + parseFloat(totalventabruta).toFixed(2);
        document.getElementById('texttotal2').innerHTML = '$ ' + parseFloat(totalventabruta).toFixed(2);
       
        $("#totalrestoapagar").val(totfaltapagar);
        document.getElementById('texttotalrestoapagar').innerHTML = '$ ' + parseFloat(totfaltapagar).toFixed(2);

        $("#totalrecargofinanciacion").val(totalrecargoporfinanciacion);
        document.getElementById('texttotalrecargofinanciacion').innerHTML = '$ ' + parseFloat(totalrecargoporfinanciacion).toFixed(2);

        $("#totalapagar").val(totalventamasrecargo);
        document.getElementById('texttotalapagar').innerHTML = '$ ' + parseFloat(totalventamasrecargo).toFixed(2);

        var textcuotas = cuotas + ' (' + parseFloat(precioxcuota).toFixed(2) + ')';
        $("#textcuotas").val(textcuotas);

    }

    function CalculaTotalFaltaPagar() {
        //recorrer la grilla y recalcular el total
        var table = document.getElementById("formasdepagos");

        var totalpaga = 0;
        var totalformadepago = 0;
        var totalrecargoporfinanciacion = 0
        
        for (var i = 0; i < table.rows.length; i++) {

            var valohtml = table.rows[i].cells[6].innerHTML; //importe
            var recahtml = table.rows[i].cells[7].innerHTML; //recargo
            var totalfphtml = table.rows[i].cells[8].innerHTML; //total

            var valorimporte = valohtml.split(">");
            var valorrecargo = recahtml.split(">");
            var valortotal = totalfphtml.split(">");

            if (Number.isNaN(parseFloat(valortotal[1])) == false) {

                var tot = valortotal[1]; //importe

                totalformadepago = parseFloat(tot) + parseFloat(totalformadepago);
                var recargolfp = parseFloat(valortotal[1]) - parseFloat(valorimporte[1]);
                totalrecargoporfinanciacion = parseFloat(totalrecargoporfinanciacion) + parseFloat(recargolfp);
                totalpaga = parseFloat(totalpaga) + parseFloat(tot);

            }
        }

        var totalventabruta = $("#total").val();

        var totalventamasrecargo = parseFloat(totalventabruta) + parseFloat(totalrecargoporfinanciacion);
        var totfaltapagar = parseFloat(totalventamasrecargo) - parseFloat(totalpaga);
        
        return totfaltapagar;
    }

    function LimpiaValoresArticulo() {

        $("#articulo").val('');
        $("#auto_articulo").val('');
        $("#auto_articulo_venta ").val('');
        $("#codbarra").val('');
        $("#codbarrainterno").val('');
        $("#cantidad").val('1');
        $("#cantidad_maxima").val('');
        $("#porcDescuento").val('0');
        $("#descuento").val('0');
        $("#precioventa").val('');
        $("#precioventa_descuento").val('');
    }

    function LimpiaValoresFormaDePago(limpiaFormaPago) {

        if (limpiaFormaPago) {
            $("#formapago_vta").val('EFE');
            $('#auto_notacredito').attr('disabled', true);
            
            //trae las financiaciones
            GetFinanciaciones();
        }
        
        $("#auto_notacredito").val('');
        $("#auto_notacredito_venta").val('');

        $("#tarjeta").val('-1');
        $("#banco").val('-1');
        $("#nrotarjeta").val('');
        $("#cuotas").val('1');
        $("#textcuotas").val('1');

        var totfaltapagar = CalculaTotalFaltaPagar();
        $("#importe").val(parseFloat(totfaltapagar).toFixed(2));
        $("#recargo").val('0.00');
        $("#totalLFP").val(parseFloat(totfaltapagar).toFixed(2));

        $('#tarjeta').attr('disabled', true);
        $('#banco').attr('disabled', true);
        $('#nrotarjeta').attr('disabled', true);
        
        $('#cuotas').attr('disabled', true);
        $('#textcuotas').attr('disabled', true);
        $('#importe').attr('disabled', true);
        
        MostrarDIV(".codverificacion");
        OcultarDIV(".nronotacredito");
        
    }

</script>



