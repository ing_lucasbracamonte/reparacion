<!-- Content Header (Page header) -->
<section class="content-header">
    <h3><?php echo $heading_title; ?></h3>
    <!-- Error -->
    <?php if ($error_texto_error != '') { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error_texto_error; ?>
    </div>
    <?php } ?> 
    <!-- Error -->
</section>

<!-- Main content -->
<section class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Registro</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <label><?php echo $entry_parametro; ?></label>
                    <input class="form-control" type="text" name="parametro" value="<?php echo $parametro; ?>" placeholder="Campo obligatorio" <?php if ($_REQUEST['action'] != 'insert') { ?> readonly <?php } ?> />
                </div>
                <div class="form-group">
                    <label><?php echo $entry_valor; ?></label>
                    <input class="form-control" type="text" name="valor" value="<?php echo $valor; ?>" placeholder="Campo obligatorio"/>
                </div>
                <div class="form-group">
                    <label><?php echo $entry_descripcion; ?></label>
                    <textarea class="form-control" rows="3" name="descripcion" id="descripcion" placeholder="Detalle del parametro." ><?php echo $descripcion; ?></textarea>
                </div>
            </div>
        </div>
        <!-- BOTONES INICIO -->
        <div class="box-footer" style="text-align: -webkit-center;">
            <?php if ($_REQUEST['action'] != 'consulta') { ?>  
            <button type="submit" class="btn btn-primary" onclick="document.getElementById('form').submit();">Guardar</button>
            <?php } ?>   
            <button type="button" class="btn btn-primary" onclick="location = '<?php echo $cancel; ?>'">Cancelar</button>
        </div>
        <!-- BOTONES FIN -->
    </form>
</section>

<script type="text/javascript">
    
    $(function () {
        $('.chosen-select').chosen();
        $('.chosen-select-deselect').chosen({allow_single_deselect: true});
    });

    function numeric(e, tipo)
    {
        if (tipo == 'int') {
            return ((e.keyCode == 9) || (e.keyCode == 8) || (e.keyCode > 32 && e.keyCode < 41) || (e.keyCode > 44 && e.keyCode < 58) || (e.keyCode > 94 && e.keyCode < 106));
        } else if (tipo == 'dec') {
            return ((e.keyCode == 9) || (e.keyCode == 8) || (e.keyCode == 110 || e.keyCode == 190) || (e.keyCode > 32 && e.keyCode < 41) || (e.keyCode > 44 && e.keyCode < 58) || (e.keyCode > 94 && e.keyCode < 106));
        }
    }
    
</script>