<!-- Content Header (Page header) -->
<section class="content-header">
    <h3><?php echo $heading_title; ?></h3>
    <!-- Error -->
    <?php if ($error_texto_error != '') { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error_texto_error; ?>
    </div>
    <?php } ?> 
    <!-- Error -->
</section>

<!-- Main content -->
<section class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Registro</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <input class="form-control" type="hidden" name="cliente" value="<?php echo $cliente; ?>" />
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">Empresa</a></li>
                        <li><a href="#tab_2" data-toggle="tab">Datos Facturación</a></li>
                        <li><a href="#tab_3" data-toggle="tab" >Otros Parametros</a></li>
                        
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="form-group">
                                <label><?php echo $entry_nombre; ?></label>
                                <input class="form-control" type="text" name="nombre" value="<?php echo $nombre; ?>"  placeholder="Campo obligatorio" focusablefirst="true" />
                            </div>
                            <div class="form-group">
                                <label><?php echo $entry_telefono; ?></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <input type="text" class="form-control" id="telefono" name="telefono" value="<?php echo $telefono; ?>" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label><?php echo $entry_localidad; ?></label>
                                <select class="form-control" name="localidad" id="localidad">
                                    <option value="-1" selected>Seleccione...</option>
                                    <?php foreach ($localidades as $loc) {       
                                    if ($loc['localidad'] == $localidad){ ?>
                                    <option value="<?php echo $loc['localidad']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                                    <?php } else{ ?>
                                    <option value="<?php echo $loc['localidad']; ?>"><?php echo $loc['descripcion']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label><?php echo $entry_domicilio; ?></label>
                                <input class="form-control" type="text" id="domicilio" name="domicilio" value="<?php echo $domicilio; ?>" />
                            </div>
                            
                            <div class="form-group">
                                <label><?php echo $entry_mail; ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <input type="text" class="form-control" id="mail" name="mail"   value="<?php echo $mail; ?>" placeholder="Email" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label><?php echo $entry_web; ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                                    <input type="text" class="form-control"  name="web"  value="<?php echo $web; ?>" placeholder="Web">
                                </div>
                            </div>  
                             
                            
                            
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                            
                            <div class="form-group">
                                <label><?php echo $entry_cuit; ?></label>
                                <input class="form-control" type="text" name="cuit" value="<?php echo $cuit; ?>"  placeholder="Campo obligatorio."/>
                            </div>
                            <div class="form-group">
                                <label><?php echo $entry_razonsocial; ?></label>
                                <input class="form-control" type="text" name="razonsocial" value="<?php echo $razonsocial; ?>"  placeholder="Campo obligatorio"/>
                            </div>
                            <div class="form-group">
                                <label><?php echo $entry_domiciliofiscal; ?></label>
                                <input class="form-control" type="text" id="domiciliofiscal" name="domiciliofiscal" value="<?php echo $domiciliofiscal; ?>"  placeholder="Campo obligatorio"/>
                            </div>
                            <div class="form-group">
                                <label><?php echo $entry_telefonofacturacion; ?></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <input type="text" class="form-control" id="telefonofacturacion" name="telefonofacturacion" value="<?php echo $telefonofacturacion; ?>" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label><?php echo $entry_tipoiva; ?></label>
                                <select class="form-control" name="tipoiva" id="tipoiva"  >
                                    <option value="-1" selected>Seleccione...</option>
                                    <?php foreach ($tiposiva as $loc) {       
                                    if ($loc['tipoiva'] == $tipoiva){ ?>
                                    <option value="<?php echo $loc['tipoiva']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                                    <?php } else{ ?>
                                    <option value="<?php echo $loc['tipoiva']; ?>"><?php echo $loc['descripcion']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label><?php echo $entry_mailfactura; ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <input type="text" class="form-control"  id="mailfactura" name="mailfactura"  value="<?php echo $mailfactura; ?>" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label><?php echo $entry_horarioatencion; ?></label>
                                <input class="form-control" type="text" name="horarioatencion" value="<?php echo $horarioatencion; ?>" />
                            </div>
                            
                            <!--------IMAGEN logofactura------------>
                <div class="form-group">
                    <label><?php echo $entry_logofactura; ?></label>
                    <?php if ($logofactura) { ?>
                    <div class="form-control"><a href="template/default/image/<?php echo basename($logofactura); ?>" onclick="mostrarImagen('logofactura','template/default/image/<?php echo basename($logofactura); ?>');
                            return false;">Ver imagen</a> | <a href="/" onclick="mostrarUpload('imagen-upload-logofactura');
                                    return false;">Cambiar imagen</a></div>
                    <?php } ?>
                    <input class="form-control" type="file" name="logofactura" id="imagen-upload-logofactura" style="width: 100%; <?php echo ($logofactura) ? 'display:none; margin-top: 5px; margin-bottom: 5px;' : ''; ?>" >
                </div> 
                        
                            
                <?php /* ?>            
                            <!--------ARCHIVO certificado_crt------------>
                <div class="form-group">
                    <label><?php echo $entry_certificado_crt; ?></label>
                    <?php if ($certificado_crt) { ?>
                    <div class="form-control"><a href="template/default/image/<?php echo basename($certificado_crt); ?>" onclick="mostrarImagen('certificado_crt','template/default/image/<?php echo basename($certificado_crt); ?>');
                            return false;">Ver imagen</a> | <a href="/" onclick="mostrarUpload('imagen-upload-certificado_crt');
                                    return false;">Cambiar imagen</a></div>
                    <?php } ?>
                    <input class="form-control" type="file" name="certificado_crt" id="imagen-upload-certificado_crt" style="width: 100%; <?php echo ($certificado_crt) ? 'display:none; margin-top: 5px; margin-bottom: 5px;' : ''; ?>" >
                </div> 
                            
                            <!--------ARCHIVO claveprivada_key------------>
                <div class="form-group">
                    <label><?php echo $entry_claveprivada_key; ?></label>
                    <?php if ($claveprivada_key) { ?>
                    <div class="form-control"><a href="template/default/image/<?php echo basename($claveprivada_key); ?>" onclick="mostrarImagen('claveprivada_key','template/default/image/<?php echo basename($claveprivada_key); ?>');
                            return false;">Ver imagen</a> | <a href="/" onclick="mostrarUpload('imagen-upload-claveprivada_key');
                                    return false;">Cambiar imagen</a></div>
                    <?php } ?>
                    <input class="form-control" type="file" name="claveprivada_key" id="imagen-upload-claveprivada_key" style="width: 100%; <?php echo ($claveprivada_key) ? 'display:none; margin-top: 5px; margin-bottom: 5px;' : ''; ?>" >
                </div> 
                            
                <?php */ ?>             
                            
                            
                            
                        </div>
                        <div class="tab-pane" id="tab_3">
                            <!--------IMAGEN logoempresa------------>
                <div class="form-group">
                    <label><?php echo $entry_logoempresa; ?></label>
                    <?php if ($logoempresa) { ?>
                    <div class="form-control"><a href="template/default/image/<?php echo basename($logoempresa); ?>" onclick="mostrarImagen('logoempresa','template/default/image/<?php echo basename($logoempresa); ?>');
                            return false;">Ver imagen</a> | <a href="/" onclick="mostrarUpload('imagen-upload-logoempresa');
                                    return false;">Cambiar imagen</a></div>
                    <?php } ?>
                    <input class="form-control" type="file" name="logoempresa" id="imagen-upload-logoempresa" style="width: 100%; <?php echo ($logoempresa) ? 'display:none; margin-top: 5px; margin-bottom: 5px;' : ''; ?>" >
                </div> 
                            
                            <!--------IMAGEN favicon------------>
                <div class="form-group">
                    <label><?php echo $entry_favicon; ?></label>
                    <?php if ($favicon) { ?>
                    <div class="form-control"><a href="template/default/image/<?php echo basename($favicon); ?>" onclick="mostrarImagen('favicon','template/default/image/<?php echo basename($favicon); ?>');
                            return false;">Ver imagen</a> | <a href="/" onclick="mostrarUpload('imagen-upload-favicon');
                                    return false;">Cambiar imagen</a></div>
                    <?php } ?>
                    <input class="form-control" type="file" name="favicon" id="imagen-upload-favicon" style="width: 100%; <?php echo ($favicon) ? 'display:none; margin-top: 5px; margin-bottom: 5px;' : ''; ?>" >
                </div> 
                            
                            <!--------IMAGEN encabezadoreporte------------>
                <div class="form-group">
                    <label><?php echo $entry_encabezadoreporte; ?></label>
                    <?php if ($encabezadoreporte) { ?>
                    <div class="form-control"><a href="template/default/image/<?php echo basename($encabezadoreporte); ?>" onclick="mostrarImagen('encabezadoreporte','template/default/image/<?php echo basename($encabezadoreporte); ?>');
                            return false;">Ver imagen</a> | <a href="/" onclick="mostrarUpload('imagen-upload-encabezadoreporte');
                                    return false;">Cambiar imagen</a></div>
                    <?php } ?>
                    <input class="form-control" type="file" name="encabezadoreporte" id="imagen-upload-encabezadoreporte" style="width: 100%; <?php echo ($encabezadoreporte) ? 'display:none; margin-top: 5px; margin-bottom: 5px;' : ''; ?>" >
                </div> 
                            
                            <!--------IMAGEN encabezadoinforme------------>
                <div class="form-group">
                    <label><?php echo $entry_encabezadoinforme; ?></label>
                    <?php if ($encabezadoinforme) { ?>
                    <div class="form-control"><a href="template/default/image/<?php echo basename($encabezadoinforme); ?>" onclick="mostrarImagen('encabezadoinforme','template/default/image/<?php echo basename($encabezadoinforme); ?>');
                            return false;">Ver imagen</a> | <a href="/" onclick="mostrarUpload('imagen-upload-encabezadoinforme');
                                    return false;">Cambiar imagen</a></div>
                    <?php } ?>
                    <input class="form-control" type="file" name="encabezadoinforme" id="imagen-upload-encabezadoinforme" style="width: 100%; <?php echo ($encabezadoinforme) ? 'display:none; margin-top: 5px; margin-bottom: 5px;' : ''; ?>" >
                </div> 
                    <!--------IMAGEN encabezadomail------------>        
                            <div class="form-group">
                    <label><?php echo $entry_encabezadomail; ?></label>
                    <?php if ($encabezadomail) { ?>
                    <div class="form-control"><a href="template/default/image/<?php echo basename($encabezadomail); ?>" onclick="mostrarImagen('encabezadomail','template/default/image/<?php echo basename($encabezadomail); ?>');
                            return false;">Ver imagen</a> | <a href="/" onclick="mostrarUpload('imagen-upload-encabezadomail');
                                    return false;">Cambiar imagen</a></div>
                    <?php } ?>
                    <input class="form-control" type="file" name="encabezadomail" id="imagen-upload-encabezadomail" style="width: 100%; <?php echo ($encabezadomail) ? 'display:none; margin-top: 5px; margin-bottom: 5px;' : ''; ?>" >
                </div> 
                            
                            
                
                        </div><!-- /.tab-pane -->
                       
                    </div><!-- /.tab-content -->
                </div><!-- nav-tabs-custom -->
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer" style="text-align: -webkit-center;">
            <?php if ($_REQUEST['action'] != 'consulta') { ?>  
            <button type="submit" class="btn btn-primary" onclick="document.getElementById('form').submit();">Guardar</button>
            <?php } ?>   
            <button type="button" class="btn btn-primary" onclick="location = '<?php echo $cancel; ?>'">Cancelar</button>
              
        </div>
    </form>
</section>

<script type="text/javascript">

    function mostrarUpload(id) {
        document.getElementById(id).style.display = "initial";
    }

    function mostrarImagen(id,src) {

        document.getElementById(id).src = src;
        msgBox('Ver imagen...', false, 850, 500, '', 'verImagen');
    }

    function mostrarUpload_old() {
        document.getElementById('imagen-upload').style.display = "initial";
    }

    function mostrarImagen_old() {

        document.getElementById('imagen').src = 'Archivos/tiposmaquina/<?php echo basename($imagen); ?>';
        msgBox('Ver imagen...', false, 850, 500, '', 'verImagen');
    }
</script>
