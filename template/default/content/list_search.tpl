<?php
// Agrego los CSS para condensar luego
if(isset($this->head_def)){
$this->head_def->setcss("/css/list.css");
} ?>

<?php if ($error) { ?>
<div class="warning"><?php echo $error; ?></div>
<?php } ?>
<?php if ($message) { ?>
<div class="message"><?php echo $message; ?></div>
<?php } ?>

<!-- List -->
<div class="block_top activity act">
    <div class="main_sidebar">
        <img src="template/default/image/img/gold_icons/LUGAR.png"></img>
        <h3><?php echo $heading_title; ?></h3>
    </div>
    <!-- List actions -->
    <div class="actions-table">
        <div class="actions">
            <a href="javascript:history.back()"><img src="template/default/image/img/light-blue_icons/back.png"></a> 
            <?php if (@$listar) { ?>
            <div  onclick="location = '<?php echo $list; ?>'"><img style="cursor: pointer" src="template/default/image/img/light-blue_icons/listado.png" class="first" alt="<?php echo $button_list; ?>"></div>
            <?php } ?>   
            <?php if (@$exportPdf) { ?>
            <!--<div class="enabled" onclick="location='<?php echo $exportpdf; ?>'"><img src="template/default/image/exportar.png" alt="<?php echo $button_exportar; ?>" class="png" /><?php echo $button_exportar; ?></div> -->
            <div class="enabled"><a href="<?php echo $export; ?>" title="<?php echo $titulo_ventana; ?>" rel="gb_page_fs[]" ><img style="cursor: pointer" src="template/default/image/img/light-blue_icons/pdf.png" class="first" alt="<?php echo $button_exportarpdf; ?>"></a></div>
            <?php } ?>
            <?php if (@$exportExcel) { ?>
            <div class="enabled"><a onclick="generarInforme()" href="<?php echo $exportExcel; ?>" title="<?php echo $button_exportar_descargar.$button_exportar_excel; ?>"><img src="template/default/image/excel.png" class="png"/></a><?php echo $button_exportar_excel; ?></div>
            <?php } ?>
            <?php if (@$listaMail) { ?>
            <div class="enabled"><a href="/" onclick="listaMail();
                return false;"><img src="template/default/image/img/light-blue_icons/iconos-12.png" title="Lista de Mails"></a></div>
            <?php } ?>
            <a href="/" onclick="MensajeModal();
                 return false;"><img src="template/default/image/img/light-blue_icons/iconos-20.png" title="Ayuda"></a>
            <a   class="search"><img  src="template/default/image/img/light-blue_icons/iconos-27.png"></a>
            <a href="#"><img src="template/default/image/img/light-blue_icons/home(1).png" style="width: 40px;margin-top: 5px;" class="second" title="Home"></a>
        </div>
    </div>

    <!-- filter -->
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <div class="filter">
            <div class="main_sidebar">
                <h4><?php echo $entry_search; ?></h4>
                <input type="text" value="<?php echo $search; ?>" id="search" name="search" placeholder="<?php echo $placeholder_buscar; ?>" style="width:75%">
                <div class="task1"> 
                    <div class="enabled" onclick="document.forms[0].submit();">();"><img  src="template/default/image/img/iconos-17.png" title="Buscar"></div>
                </div>
            </div>
        </div>
    </form>
    <input type="hidden" value="<?php echo $centro; ?>" name="centro" id="centro">
    <input type="hidden" value="<?php echo $club; ?>" name="club" id="club">
    <input type="hidden" value="<?php echo $unionid; ?>" name="unionid" id="unionid">
    <input type="hidden" value="<?php echo $region; ?>" name="region" id="region">
</div>

<!-- Activity table -->
<div class="block_bottom activity act">
    <div class="generated-table">
        <section class="section__main">
            <table class="table">
                <thead>
                    <tr>
                        <?php $bandera = true; ?>
                        <?php foreach ($cols as $col) { ?>
                        <?php if ($bandera == true) { ?>
                        <th style="border-left: none;">
                            <?php $bandera = false; ?>
                            <?php } else { ?>
                        <th>
                            <?php } ?>
                            <?php if (!isset($col['sort'])) { ?>
                            <?php if (isset($col['name'])) { ?>
                            <?php echo $col['name']; ?>
                            <?php } ?>
                            <?php } else { ?>
                            <?php if (isset($col['sort'])) { ?>
                            <form action="<?php echo $action; $actionPaginacion = $action; ?>" method="post" enctype="multipart/form-data" onclick="this.submit();">
                                <?php echo $col['name']; ?>
                                <?php if ($sort == $col['sort']) { ?>
                                <?php if ($order == 'asc') { ?>
                                &nbsp;<img src="template/default/image/asc.png" class="png" />
                                <?php } else { ?>
                                &nbsp;<img src="template/default/image/desc.png" class="png" />
                                <?php } ?>
                                <?php } ?>
                                <input type="hidden" name="sort" value="<?php echo $col['sort']; ?>" />
                            </form>
                            <?php } ?>
                            <?php } ?>
                        </th>
                        <?php } ?>
                    </tr>
                </thead>
                <?php $j = 1; ?>
                <?php foreach ($rows as $row) { ?>
                <?php if ($j != 1) { $j = 1; } else { $j = 0; } if ($j == 0) { $class = 'row1'; } elseif ($j == 1) { $class = 'row2'; } ?>
                <tbody>
                    <tr>
                        <?php $bandera = true; ?>
                        <?php foreach ($row['cell'] as $cell) { ?>
                        <?php if (isset($cell['value'])) { ?>
                        <?php if ($bandera == true) { ?>
                        <td style="border-left: none;" align="<?php echo $col['align']; ?>">
                            <?php $bandera = false; ?>
                            <?php } else { ?>
                        <td align="<?php echo $col['align']; ?>">
                            <?php } ?>
                            <?php echo $cell['value']; ?>
                            <?php if (@$cell['default']) { ?>
                            (<?php echo $text_default; ?>)
                            <?php } ?>
                        </td>
                        <?php } elseif (isset($cell['image'])) { ?>
                        <td align="<?php echo $col['align']; ?>"><img src="<?php echo $cell['image']; ?>" /></td>
                        <?php } elseif (isset($cell['path'])) { ?>
                        <td align="<?php echo $col['align']; ?>"><a href="<?php echo $cell['path']; ?>"><img src="template/default/image/<?php echo $cell['icon']; ?>" class="png" /></a></td>
                        <?php } elseif (isset($cell['icon'])) { ?>
                        <td align="<?php echo $col['align']; ?>"><?php if (isset($cell['href'])) { ?>
                            <a href="<?php echo $cell['href']; ?>"><img src="template/default/image/<?php echo $cell['icon']; ?>" class="png" /></a>
                            <?php } else { ?>
                            <img src="template/default/image/<?php echo $cell['icon']; ?>" class="png" />
                            <?php } ?>
                        </td>
                        <?php } elseif (isset($cell['action'])) { ?>      
                        <td align="<?php echo $col['align']; ?>">
                            <?php foreach ($cell['action'] as $action) {   
                            $html_action = '<a ';
                            foreach ($action['prop_a'] as $key => $value) {
                            $html_action .= $key.'="'.$value.'" '; }
                            $html_action .= '>';
                            if(isset($action['icon'])) {
                            $html_action .= '<img src="template/default/image/'.$action['icon'].'" alt="'.$action['text'].'" title="'.$action['text'].'" class="png" />';
                            } else { $html_action .= $action['text']; }
                            $html_action .= "</a>\n              ";
                            echo $html_action; } ?>
                        </td>
                        <?php } ?>
                        <?php } ?>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </section>
    </div>
</div>

<!-- Paginacion -->
<?php include 'paginacion.tpl' ?>

<div id="dialog-message" title="Ayuda">
    <p>
        <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
        <?php echo $textMessageAyuda ?>
    </p>
</div>

<script language="JavaScript">

    function MensajeModal()
    {
        $("#dialog-message").dialog({
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            }
        });
        $("#dialog-message").dialog('open');
    }

    function listaMail() {
        $('#popup').html('')
        $.ajax
                ({
                    type: "POST",
                    url: "index.php?controller=integrantesclubes&action=listaMail",
                    //data: "Actividad=" + Actividad,
                    async: false,
                    success: function (data) {
                        //alert(data);
                        if (data != 'ok') {
                            $('#popup').html(data);
                        } else {
                            alert(data);
                        }
                    }
                });
        estiloBotones();
        $('#popup').dialog('option', 'title', 'Lista de Mail de los inscriptos');
        $("#popup").dialog('open');
    }

    function estiloBotones() {
        $("button").each(function () {
            if ($(this).attr("primary") != undefined) {
                props = {icons:{primary: $(this).attr("primary")}}
            } else if ($(this).attr("secondary") != undefined) {
                props = {icons:{secondary: $(this).attr("secondary")}}
            } else {
                props = {};
            }
            $(this).button(props);
        });
    }

</script>