<!-- Content Header (Page header) -->
<div>
    <section class="content-header">

        <h3><?php echo $heading_title; ?></h3>

        <!-- Error -->
        <?php if ($error_texto_error != '') { ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Error!</h4>
            <?php echo $error_texto_error; ?>
        </div>
        <?php } ?> 
        <!-- Error -->
    </section>
</div>

<!-- Main content -->
<section class="content" style="padding-top: 55px !important">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Registro</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label>Código</label>
                                <input class="form-control" type="text" name="compra" value="<?php echo $compra; ?>" readonly/>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label><?php echo $entry_fecha; ?></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" value="<?php echo $fecha; ?>" id="fecha" name="fecha" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" placeholder="Campo obligatorio">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label><?php echo $entry_formapago; ?></label>
                                <select class="chosen-select" tabindex="2" name="formapago" id="formapago" onchange="habilitaNro();">
                                    <option value="-1" selected>Seleccione...</option>
                                    <?php foreach ($formaspago as $loc) { 
                                    if ($loc['formapago'] == $formapago){ ?>
                                    <option value="<?php echo $loc['formapago']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                                    <?php } else{ ?>
                                    <option value="<?php echo $loc['formapago']; ?>"><?php echo $loc['descripcion']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-3">
                            <div class="form-group">
                                <label><?php echo $entry_nrotarjeta; ?></label>
                                <input class="form-control" type="text" id="nrotarjeta" name="nrotarjeta" value="<?php echo $nrotarjeta; ?>" disabled/>
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label><?php echo $entry_proveedor; ?></label>
                                <select class="chosen-select" tabindex="2" name="proveedor" id="proveedor">
                                    <option value="-1" selected>Seleccione...</option>
                                    <?php foreach ($proveedores as $loc) {       
                                    if ($loc['proveedor'] == $proveedor){ ?>
                                    <option value="<?php echo $loc['proveedor']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                                    <?php } else{ ?>
                                    <option value="<?php echo $loc['proveedor']; ?>"><?php echo $loc['descripcion']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label><?php echo $entry_tipodocrelacionado; ?></label>
                                <select class="chosen-select" tabindex="2" name="tipodocrelacionado" id="tipodocrelacionado">
                                    <option value="-1" selected>Seleccione...</option>
                                    <?php foreach ($tipodocrelacionados as $loc) {       
                                    if ($loc['tipodocumento'] == $tipodocrelacionado){ ?>
                                    <option value="<?php echo $loc['tipodocumento']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                                    <?php } else{ ?>
                                    <option value="<?php echo $loc['tipodocumento']; ?>"><?php echo $loc['descripcion']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label><?php echo $entry_nrodocrelacionado; ?></label>
                                <input class="form-control" type="text" name="nrodocrelacionado" value="<?php echo $nrodocrelacionado; ?>" />
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label><?php echo $entry_observacion; ?></label>
                                <input class="form-control" type="text" name="observacion" value="<?php echo $observacion; ?>" />
                            </div>
                        </div>
                    </div>
                </div>
                <?php if ($_REQUEST['action'] == 'consulta') { ?>
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label><?php echo $entry_fechabaja; ?></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" value="<?php echo $fechabaja; ?>" id="fechabaja" name="fechabaja" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" >
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-9">
                            <div class="form-group">
                                <label><?php echo $entry_motivobaja; ?></label>
                                <input class="form-control" type="text" name="motivobaja" value="<?php echo $motivobaja; ?>" />
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="box">
                <?php include 'compra_grilla_repuestos.tpl' ?> 
            </div>
        </div>
        <!-- BOTONES INICIO  -->
        <div class="box-footer" style="text-align: -webkit-center;">
            <?php if ($_REQUEST['action'] != 'consulta') { ?>  
            <button type="submit" class="btn btn-primary" onclick="saveInsert();
                    return false;" onclick="document.getElementById('form').submit();">Guardar</button>
            <?php } ?>   
            <button type="button" class="btn btn-primary" onclick="location = '<?php echo $cancel; ?>'">Cancelar</button>
        </div>
        <!-- BOTONES FIN -->
    </form>
</section>

<script type="text/javascript">

    $(function () {
        $('.chosen-select').chosen();
        $('.chosen-select-deselect').chosen({allow_single_deselect: true});
    });

    $(function () {
        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
        //Money Euro
        $("[data-mask]").inputmask();
    });

    function habilitaNro() {
        var formapago = $("#formapago").val();
        // alert(formapago);
        if (formapago != '-1') {
            $('#nrotarjeta').attr('disabled', false);
        } else {
            $('#nrotarjeta').attr('disabled', true);
        }
    }

    function guardar(action) {
//hacer validaciones aca
        $.ajax({
            type: 'POST',
            url: 'index.php?controller=compras&action=' + action,
            data: $('#formABM').serialize(),
            async: false,
            success: function (data) {
//            alert(data);
                if (data != 'ok') {
                    $('#popup').html(data);
                } else {
                    location.reload(true);
                }
            }
        });

    }

    function saveInsert() {
//hacer validaciones aca
        var proveedor = $("#proveedor").val();
        //var entidadorigen_chosen  = $("#entidadorigen_chosen").val();
        var tipodocrelacionado = $("#tipodocrelacionado").val();

        //var estadodua  = $("#estadodua").val();
        var cant = $('#repuestos tr').size();
        //alert(cant);
        if (proveedor == '-1') {
            alert('Debe seleccionar un proveedor.');
        } else if ($('#repuestos tr').size() <= '2') {
            alert('Debe ingresar al menos un repuesto.');
        } else if (tipodocrelacionado == '-1') {
            alert('Debe seleccionar el tipo de documento relacionado.');
        } else {
            $.ajax({
                type: 'POST',
                url: 'index.php?controller=compras&action=insert',
                data: $('#form').serialize(),
                async: false,
                success: function (data) {
                    //alert(data);
                    if (data == 'error') {
                        $('#form').html(data);
                    } else {
                        //alert(data);
                        window.location.href = data;
                    }
                }
            });
        }
    }

    function AgregarRepuesto() {
        //alert($("#concoddebarra").val());
        var repuesto = $("#repuesto").val();
        //var descripcion  = $("#descripcion").val();
        var compra = $("#compra").val();
        var cantidad = $("#cantidad").val();
        var preciocompra = $("#preciocompra").val();
        var nroparte = $("#nroparte").val();
        var total = $("#total").val();
        var subtotal = $("#subtotal").val();
        var cantReg = $('#repuestos tr').size();


        //var minutocambio  = $("#minutocambio").val();
        //var entrenamiento = $('#entrenamiento').val();
        //alert('personaentra=' + personaentra + '&personasale='+ personasale + '&minuto='+ minutocambio + '&partido_id='+ partido_id +'&formulario=personas');

        if (repuesto == '-1') {
            alert('Antes de agregar un repuesto debe seleccionar uno.');
        } else if (cantidad == 0) {
            alert('Debe ingresar una cantidad.');
        } else if (preciocompra == 0) {
            alert('Debe ingresar un precio de compra.');
        } else {
            //alert('personaentra=' + personaentra + '&personasale='+ personasale + '&minutocambio='+ minutocambio + '&partido_id='+ partido_id +'&formulario=personas');
            //alert('2');
            $.ajax({
                type: 'GET',
                url: 'index.php?controller=compras&action=AgregarRepuesto',
                data: 'compra=' + compra + '&repuesto=' + repuesto + '&cantidad=' + cantidad + '&nroparte=' + nroparte + '&preciocompra=' + preciocompra + '&cantReg=' + cantReg,
                async: false,
                success: function (data) {
                    if (data != 'error') {
                        $('#repuestos').append(data);
                        //alert($('#articulos tr').size());
                        var cantidad = $("#cantidad").val();
                        var preciocompra = $("#preciocompra").val();
                        var tot = preciocompra * cantidad;

                        var totalcompra = Number(tot) + Number(total);
                        $("#total").val(totalcompra);
                        $("#texttotal").val('TOTAL: ' + totalcompra);

                        $("#nroparte").val('');
                        //$("#repuesto_compra").val('-1');
                        $("#preciocompra").val('0');
                        $("#subtotal").val('0');
                        $("#cantidad").val('1');
                        $("#repuesto_compra").val('-1').trigger("chosen:updated");


                    }
                    // else{
                    //   alert('En este artículo es obligatorio la carga del codigo de barras.');
                    // }
                }
            });
        }
    }

    function removeRepuesto(row) {
        //alert(row);
        $('#' + row).remove();
    }

    function numeric(e, tipo)
    {
        if (tipo == 'int') {
            return ((e.keyCode == 9) || (e.keyCode == 8) || (e.keyCode > 32 && e.keyCode < 41) || (e.keyCode > 44 && e.keyCode < 58) || (e.keyCode > 94 && e.keyCode < 106));
        } else if (tipo == 'dec') {
            return ((e.keyCode == 9) || (e.keyCode == 8) || (e.keyCode == 110 || e.keyCode == 190) || (e.keyCode > 32 && e.keyCode < 41) || (e.keyCode > 44 && e.keyCode < 58) || (e.keyCode > 94 && e.keyCode < 106));
        }
    }

    function IsNumeric(valor)
    {
        var log = valor.length;
        var sw = "S";
        for (x = 0; x < log; x++)
        {
            v1 = valor.substr(x, 1);
            v2 = parseInt(v1);
//Compruebo si es un valor numérico 
            if (isNaN(v2)) {
                sw = "N";
            }
        }
        if (sw == "S") {return true;} else {return false; }
    }

    var primerslap = false;
    var segundoslap = false;
    function formateafecha(fecha)
    {
        var long = fecha.length;
        var dia;
        var mes;
        var ano;

        if ((long >= 2) && (primerslap == false)) {
            dia = fecha.substr(0, 2);
            if ((IsNumeric(dia) == true) && (dia <= 31) && (dia != "00")) {
                fecha = fecha.substr(0, 2) + "/" + fecha.substr(3, 7);
                primerslap = true;
            } else {
                fecha = "";
                primerslap = false;
            }
        } else
        {
            dia = fecha.substr(0, 1);
            if (IsNumeric(dia) == false)
    {fecha="";}
            if ((long <= 2) && (primerslap = true))
    {fecha=fecha.substr(0,1); primerslap=false; }
        }
        if ((long >= 5) && (segundoslap == false))
        {
            mes = fecha.substr(3, 2);
            if ((IsNumeric(mes) == true) && (mes <= 12) && (mes != "00")) {
                fecha = fecha.substr(0, 5) + "/" + fecha.substr(6, 4);
                segundoslap = true;
            } else {
                fecha = fecha.substr(0, 3);
                ;
                segundoslap = false;
            }
        } else {
            if ((long <= 5) && (segundoslap = true)) {
                fecha = fecha.substr(0, 4);
                segundoslap = false;
            }
        }
        if (long >= 7)
        {
            ano = fecha.substr(6, 4);
            if (IsNumeric(ano) == false) {
                fecha = fecha.substr(0, 6);
            } else {
                if (long == 10) {
                    if ((ano == 0) || (ano < 1900) || (ano > 2100)) {
                        fecha = fecha.substr(0, 6);
                    }
                }
            }
        }

        if (long >= 10)
        {
            fecha = fecha.substr(0, 10);
            dia = fecha.substr(0, 2);
            mes = fecha.substr(3, 2);
            ano = fecha.substr(6, 4);
// Año no viciesto y es febrero y el dia es mayor a 28 
            if ((ano % 4 != 0) && (mes == 02) && (dia > 28)) {
                fecha = fecha.substr(0, 2) + "/";
            }
        }
        return (fecha);
    }

</script>