<!-- Content Header (Page header) -->
<section class="content-header">
    <h3><?php echo $heading_title; ?></h3>
    <!-- Error -->
    <?php if ($error_texto_error != '') { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error_texto_error; ?>
    </div>
    <?php } ?> 
    <!-- Error -->
</section>

<!-- Main content -->
<section class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Registro</h3>
            </div>
            <div class="box-body">
                <input type="hidden" value="<?php echo $subrubro; ?>" name="subrubro" id="subrubro">
                <div class="form-group">
                    <label><?php echo $entry_descripcion; ?></label>
                    <input class="form-control" type="text" name="descripcion" value="<?php echo $descripcion; ?>" />
                </div>
                <div class="form-group">
                    <label><?php echo $entry_rubro; ?></label>
                    <select class="chosen-select" tabindex="2" name="rubro" id="rubro">
                        <option value="-1" selected>Seleccione...</option>
                        <?php foreach ($rubroscaja as $loc) {       
                        if ($loc['rubro'] == $rubro){ ?>
                        <option value="<?php echo $loc['rubro']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                        <?php } else{ ?>
                        <option value="<?php echo $loc['rubro']; ?>"><?php echo $loc['descripcion']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <!-- BOTONES INICIO -->
        <div class="box-footer" style="text-align: -webkit-center;">
            <?php if ($_REQUEST['action'] != 'consulta') { ?>  
            <button type="submit" class="btn btn-primary" onclick="document.getElementById('form').submit();">Guardar</button>
            <?php } ?>   
            <button type="button" class="btn btn-primary" onclick="location = '<?php echo $cancel; ?>'">Cancelar</button>
        </div>
        <!-- BOTONES FIN -->
    </form>
</section>

<script>
    $(function () {
        $('.chosen-select').chosen();
        $('.chosen-select-deselect').chosen({allow_single_deselect: true});
    });
</script>