<section class="content-header">
    <h3><?php echo $heading_title; ?></h3>
    <!-- Error -->
    <?php if ($error_texto_error != '') { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error_texto_error; ?>
    </div>
    <?php } ?> 
    <!-- Error -->
</section>

<!-- Main content -->
<section class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Registro</h3>
            </div>
            <div class="box-body">
                <input type="hidden" value="<?php echo $proveedor; ?>" name="proveedor" id="proveedor">
                <div class="form-group">
                    <label><?php echo $entry_cuitcuil; ?></label>
                    <input class="form-control" type="text" name="cuitcuil" value="<?php echo $cuitcuil; ?>"  placeholder="Campo obligatorio"/>
                </div>
                <div class="form-group">
                    <label><?php echo $entry_nombre; ?></label>
                    <input class="form-control" type="text" name="nombre" value="<?php echo $nombre; ?>"  placeholder="Campo obligatorio"/>
                </div>
                <div class="form-group">
                    <label><?php echo $entry_tipoproveedor; ?></label>
                    <select class="form-control" tabindex="2" name="tipoproveedor" id="tipoproveedor">
                        <option value="-1" selected>Seleccione...</option>
                        <?php foreach ($tiposproveedor as $loc) {       
                        if ($loc['tipoproveedor'] == $tipoproveedor){ ?>
                        <option value="<?php echo $loc['tipoproveedor']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                        <?php } else{ ?>
                        <option value="<?php echo $loc['tipoproveedor']; ?>"><?php echo $loc['descripcion']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label><?php echo $entry_direccion; ?></label>
                    <div class="row">
                        <div class="col-xs-9"><input class="form-control" type="text" name="direccion" value="<?php echo $direccion; ?>" placeholder="Calle"/></div>
                        <div class="col-xs-1"><input class="form-control" type="text" name="numero" value="<?php echo $numero; ?>" placeholder="Nro"/></div>
                        <div class="col-xs-1"><input class="form-control" type="text" name="piso" value="<?php echo $piso; ?>"  placeholder="Piso"/></div>
                        <div class="col-xs-1"><input class="form-control" type="text" name="departamento" value="<?php echo $departamento; ?>"  placeholder="Dpto"/></div>
                    </div>
                </div>
                <div class="form-group">
                    <label><?php echo $entry_localidad; ?></label>
                    <input type="input" class="form-control" name="auto_localidad" id="auto_localidad" value="<?php echo $auto_localidad; ?>" placeholder="Ingrese 3 letras y seleccione una opción de la lista"/>
                    <input type="hidden" name="auto_localidad_proveedor" id="auto_localidad_proveedor" value="<?php echo $auto_localidad_proveedor; ?>" />                    
                </div> 
                <div class="form-group">
                    <label><?php echo $entry_telefono; ?></label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                        </div>
                        <input type="text" class="form-control" name="telefono" id="telefono" value="<?php echo $telefono; ?>" placeholder="Campo obligatorio">
                    </div>
                </div>
                <div class="form-group">
                    <label><?php echo $entry_mail; ?></label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        <input type="text" class="form-control" name="mail"  value="<?php echo $mail; ?>" placeholder="Campo obligatorio">
                    </div>
                </div>
            </div>
        </div>
        <!-- BOTONES INICIO -->
        <div class="box-footer" style="text-align: -webkit-center;">
            <?php if ($_REQUEST['action'] != 'consulta') { ?>  
            <button type="submit" class="btn btn-primary" onclick="document.getElementById('form').submit();">Guardar</button>
            <?php } ?>   
            <button type="button" class="btn btn-primary" onclick="location = '<?php echo $cancel; ?>'">Cancelar</button>
        </div>
        <!-- BOTONES FIN -->
    </form>
</section>

<script type="text/javascript">

    $(document).ready(function () {

        $("#auto_localidad").autocomplete({
            source: "<?php echo $script_busca_localidades; ?>",
            minLength: 3,
            select: function (event, ui) {
                $("#auto_localidad_proveedor").val(ui.item.id)
            }
        });
    });


</script>
