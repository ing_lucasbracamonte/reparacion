<!-- Content Header (Page header) -->
<section class="content-header">
    <h3><?php echo $heading_title; ?></h3>
    <!-- Error -->
    <?php if ($error_texto_error != '') { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error_texto_error; ?>
    </div>
    <?php } ?> 
    <!-- Error -->
</section>

<!-- Main content -->
<section class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Registro</h3>
            </div>
            <div class="box-body">
                <input type="hidden" value="<?php echo $financiacion; ?>" name="financiacion" id="financiacion">
                
                <div class="form-group">
                    <label><?php echo $entry_descripcion; ?></label>
                    <input class="form-control" type="text" name="descripcion" value="<?php echo $descripcion; ?>" placeholder="Campo Obligatorio"/>
                </div>
                <div class="form-group">
                    <label><?php echo $entry_formapago; ?></label>
                    <select class="form-control" tabindex="2" name="formapago" id="formapago">
                        <option value="-1" selected>Seleccione...</option>
                        <?php foreach ($formaspago as $loc) {       
                        if ($loc['formapago'] == $formapago){ ?>
                        <option value="<?php echo $loc['formapago']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                        <?php } else{ ?>
                        <option value="<?php echo $loc['formapago']; ?>"><?php echo $loc['descripcion']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label><?php echo $entry_tarjeta; ?></label>
                    <select class="form-control" tabindex="2" name="tarjeta" id="tarjeta">
                        <option value="-1" selected>Seleccione...</option>
                        <?php foreach ($tarjetas as $loc) {       
                        if ($loc['tarjeta'] == $tarjeta){ ?>
                        <option value="<?php echo $loc['tarjeta']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                        <?php } else{ ?>
                        <option value="<?php echo $loc['tarjeta']; ?>"><?php echo $loc['descripcion']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>

                <div class="form-group">
                    <label><?php echo $entry_banco; ?></label>
                    <select class="form-control" tabindex="2" name="banco" id="banco">
                        <option value="-1" selected>Seleccione...</option>
                        <?php foreach ($bancos as $loc) {       
                        if ($loc['banco'] == $banco){ ?>
                        <option value="<?php echo $loc['banco']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                        <?php } else{ ?>
                        <option value="<?php echo $loc['banco']; ?>"><?php echo $loc['descripcion']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                
                <div class="form-group">
                    <label><?php echo $entry_cuotas; ?></label>
                    <input class="form-control" type="text" id="cuotas" name="cuotas" value="<?php echo $cuotas; ?>" onkeydown="return numeric(event, 'int');" placeholder="Campo Obligatorio"/>
                </div>
                
                <div class="form-group">
                    <label><?php echo $entry_recargo; ?></label>
                    <input class="form-control" type="text" id="recargo" name="recargo" value="<?php echo $recargo; ?>" onkeydown="return numeric(event, 'dec');" placeholder="Campo Obligatorio"/>
                </div>
            </div>
        </div>
        <!-- BOTONES INICIO -->
        <div class="box-footer" style="text-align: -webkit-center;">
            <?php if ($_REQUEST['action'] != 'consulta') { ?>  
            <button type="submit" class="btn btn-primary" onclick="document.getElementById('form').submit();">Guardar</button>
            <?php } ?>   
            <button type="button" class="btn btn-primary" onclick="location = '<?php echo $cancel; ?>'">Cancelar</button>
        </div>
        <!-- BOTONES FIN -->
    </form>
</section>

<script type="text/javascript">
 function numeric(e, tipo)
    {
        if (tipo == 'int') {
            return ((e.keyCode == 9) || (e.keyCode == 8) || (e.keyCode > 32 && e.keyCode < 41) || (e.keyCode > 44 && e.keyCode < 58) || (e.keyCode > 94 && e.keyCode < 106));
        } else if (tipo == 'dec') {
            return ((e.keyCode == 9) || (e.keyCode == 8) || (e.keyCode == 110 || e.keyCode == 190) || (e.keyCode > 32 && e.keyCode < 41) || (e.keyCode > 44 && e.keyCode < 58) || (e.keyCode > 94 && e.keyCode < 106));
        }
    }
</script>