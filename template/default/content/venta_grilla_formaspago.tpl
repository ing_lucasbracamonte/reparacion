<table id="formasdepago_encabezado" name="formasdepago_encabezado" class="table table-bordered table-striped dataTable" aria-describedby="example1_info">
    <thead>
        <tr role="row" >
            <th  class="col-xs-2" role="columnheader" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">
                Forma de Pago
            </th>
             <th  class="col-xs-2" role="columnheader" tabindex="1" aria-controls="example2" rowspan="1" colspan="1">
                Financiación
            </th>
            <th  class="col-xs-1" role="columnheader" tabindex="1" aria-controls="example2" rowspan="1" colspan="1">
                Tarjeta
            </th>
            <th  class="col-xs-1"  role="columnheader" tabindex="2" aria-controls="example2" rowspan="1" colspan="1">
                Banco
            </th>
            <th  class="col-xs-1"  role="columnheader" tabindex="3" aria-controls="example2" rowspan="1" colspan="1">
                Cód Veri.
            </th>
            <th  class="col-xs-1"  role="columnheader" tabindex="4" aria-controls="example2" rowspan="1" colspan="1">
                Cuotas
            </th>
            <th  class="col-xs-1"  role="columnheader" tabindex="5" aria-controls="example2" rowspan="1" colspan="1">
                Importe $
            </th>
            <th  class="col-xs-1"  role="columnheader" tabindex="4" aria-controls="example2" rowspan="1" colspan="1">
                Recargo %
            </th>
            <th  class="col-xs-1"  role="columnheader" tabindex="5" aria-controls="example2" rowspan="1" colspan="1">
                Total $
            </th>
            <?php if ($_REQUEST['action'] == 'insert') { ?> 
            <th  class="col-xs-1"  role="columnheader" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" style="text-align: center">
                Acción
            </th>
            <?php } ?> 
        </tr>
    </thead>
    <?php if ($_REQUEST['action'] == 'insert') { ?> 
    
        <tr class="odd"  >

            <input class="form-control" type="hidden" name="formapago" id="formapago" value="<?php echo $formapago; ?>"  disabled/>

            <td class="col-xs-2">
                <select class="form-control" name="formapago_vta" id="formapago_vta" onchange="GetFinanciaciones();">
                    <option value="-1" selected>Seleccione...</option>
                    <?php foreach ($formaspagos as $loc) {       
                    if ($loc['formapago'] == $formapago_vta){ ?>
                    <option value="<?php echo $loc['formapago']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                    <?php } else{ ?>
                    <option value="<?php echo $loc['formapago']; ?>"><?php echo $loc['descripcion']; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select> 
            </td>
            <td class="col-xs-2">
                <select class="form-control" name="financiacion" id="financiacion" onchange="CargaLineaCargaFinanciacion();" disabled>
                    <?php foreach ($financiaciones as $loc) {       
                    if ($loc['financiacion'] == $financiacion){ ?>
                    <option value="<?php echo $loc['financiacion']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                    <?php } else{ ?>
                    <option value="<?php echo $loc['financiacion']; ?>"><?php echo $loc['descripcion']; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select> 
            </td>
            <td class="col-xs-1">
                <select class="form-control" name="tarjeta" id="tarjeta" disabled>
                    <option value="-1" selected>Seleccione...</option>
                    <?php foreach ($tarjetas as $loc) {       
                    if ($loc['tarjeta'] == $tarjeta){ ?>
                    <option value="<?php echo $loc['tarjeta']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                    <?php } else{ ?>
                    <option value="<?php echo $loc['tarjeta']; ?>"><?php echo $loc['descripcion']; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select> 
            </td>
            <td class="col-xs-1">
                <select class="form-control" name="banco" id="banco" disabled>
                    <option value="-1" selected>Seleccione...</option>
                    <?php foreach ($bancos as $loc) {       
                    if ($loc['banco'] == $banco){ ?>
                    <option value="<?php echo $loc['banco']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                    <?php } else{ ?>
                    <option value="<?php echo $loc['banco']; ?>"><?php echo $loc['descripcion']; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select> 
            </td>
            <td class="col-xs-1">
                <div class="codverificacion" id="codverificacion" name="codverificacion">
                    <input class="form-control" type="text" name="nrotarjeta" id="nrotarjeta" value="<?php echo $nrotarjeta; ?>"  disabled/>
                </div>
                <div class="nronotacredito" id="nronotacredito" name="nronotacredito" style="display:none">
                    <input type="input" class="form-control" name="auto_notacredito" id="auto_notacredito" value="<?php echo $auto_notacredito; ?>" placeholder="Ingrese 3 letras y seleccione una opción de la lista"/>
                    <input type="hidden" name="auto_notacredito_venta" id="auto_notacredito_venta" value="<?php echo $auto_notacredito_venta; ?>" />
                </div>
            </td>
            <td class="col-xs-1">
                <input class="form-control" type="hidden" name="cuotas" id="cuotas" value="<?php echo $cuotas; ?>"  onkeydown="return numeric(event, 'int');" disabled/>
                <input class="form-control" type="text" name="textcuotas" id="textcuotas" value="<?php echo $textcuotas; ?>" disabled/>
            </td>
            <td class="col-xs-1">
                <input class="form-control" type="text" name="importe" id="importe" value="<?php echo $importe; ?>" onkeydown="return numeric(event, 'dec');"  onkeyup="return CalculoTotalLineaFormaPago(event, 'dec', true);" />
            </td>
            <td class="col-xs-1">
                <input class="form-control" type="text" name="recargo" id="recargo" value="<?php echo $recargo; ?>"  onkeydown="return numeric(event, 'int');" disabled/>
            </td>
            <td class="col-xs-1">
                <input class="form-control" type="text" name="totalLFP" id="totalLFP" value="<?php echo $totalLFP; ?>"   disabled/>
            </td>          
            <td class="col-xs-1" align="center" style="padding: 12px 12px;">
                <?php if ($_REQUEST['action'] != 'update') { ?>
                    <input name="botonagregar" id="botonagregar" type="button" class="botones" value="+" onclick="AgregarFormaPago()" alt="<?php echo $button_insert; ?>" />
                <?php } ?>
            </td>           
        </tr>
    <?php } ?>
     
    </table>
    <table id="formasdepagos" name="formasdepagos" class="table table-bordered table-striped dataTable" aria-describedby="example1_info">
    
        <?php $i = 1; ?>
    <?php foreach ($ventaformasdepagoasignados as $ad) { ?>

        <tr id="vtaformapago_<?php echo $i; ?>" role="row">
            <input type="hidden" name="ventaformasdepago[][numero]" value="<?php echo $i; ?>"/>
            <input type="hidden" name="ventaformasdepago[][venta]" value="<?php echo $ad['venta']; ?>"/>

            <input type="hidden" name="ventaformasdepago[][ventaformapago]" value="<?php echo $ad['ventaformapago']; ?>"/>
            <td class="col-xs-2"><input type="hidden" name="ventaformasdepago[][descformapago]" value="<?php echo $ad['descformapago']; ?>"/><?php echo $ad['descformapago']; ?></td>

            <input type="hidden" name="ventaformasdepago[][financiacion]" value="<?php echo $ad['financiacion']; ?>"/>
            <td class="col-xs-2"><input type="hidden" name="ventaformasdepago[][descfinanciacion]" value="<?php echo $ad['descfinanciacion']; ?>"/><?php echo $ad['descfinanciacion']; ?></td>

            <input type="hidden" name="ventaformasdepago[][tarjeta]" value="<?php echo $ad['tarjeta']; ?>"/>
            <td class="col-xs-1"><input type="hidden" name="ventaformasdepago[][desctarjeta]" value="<?php echo $ad['desctarjeta']; ?>"/><?php echo $ad['desctarjeta']; ?></td>

            <input type="hidden" name="ventaformasdepago[][banco]" value="<?php echo $ad['banco']; ?>"/>
            <td class="col-xs-1"><input type="hidden" name="ventaformasdepago[][descbanco]" value="<?php echo $ad['descbanco']; ?>"/><?php echo $ad['descbanco']; ?></td>

            <td class="col-xs-1"><input type="hidden" name="ventaformasdepago[][nrotarjeta]" value="<?php echo $ad['nrotarjeta']; ?>"/><?php echo $ad['nrotarjeta']; ?></td>
            <input type="hidden" name="ventaformasdepago[][cuotas]" value="<?php echo $ad['cuotas']; ?>"/>
            <td class="col-xs-1"><input type="hidden" name="ventaformasdepago[][textcuotas]" value="<?php echo $ad['textcuotas']; ?>"/><?php echo $ad['cuotas']; ?></td>

            <td class="col-xs-1"><input type="hidden" name="ventaformasdepago[][importe]" value="<?php echo $ad['importe']; ?>" /><?php echo $ad['importe']; ?></td>
            <td class="col-xs-1"><input type="hidden" name="ventaformasdepago[][recargo]" value="<?php echo $ad['recargo']; ?>" /><?php echo $ad['recargo']; ?></td>
            <td class="col-xs-1"><input type="hidden" name="ventaformasdepago[][totalLFP]" value="<?php echo $ad['totalLFP']; ?>" /><?php echo $ad['totalpago']; ?></td>
            <?php if ($_REQUEST['action'] == 'insert') { ?>
            <td class="col-xs-1" align="center">
                 
                <a href="/" onclick="removeVentaFormaPago('vtaformapago_<?php echo $i; ?>');
                                    return false;"><i class="fa fa-fw fa-trash-o" alt="Borrar" title="Borrar"> </i></a>
                 
            </td>
            <?php } ?>
        </tr>
    <?php $i++; ?>
    <?php } ?>

   
</table>

