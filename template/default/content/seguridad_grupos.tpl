<!-- Content Header (Page header) -->
<section class="content-header">
    <h3><?php echo $heading_title; ?></h3>
    <!-- Error -->
    <?php if ($error_texto_error != '') { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error_texto_error; ?>
    </div>
    <?php } ?> 
    <!-- Error -->
</section>

<!-- Main content -->
<section class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Nuevo registro</h3>
            </div>
            <div class="box-body">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                        <li><a href="#tab_2" data-toggle="tab">Permisos</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="form-group">
                                <label><?php echo $entry_grupo; ?></label>
                                <?php if ($_REQUEST['action'] == 'update') { ?> 
                                <input class="form-control" type="text" name="grupo" value="<?php echo $grupo; ?>" readonly/>
                                <?php } else { ?>
                                <input class="form-control" type="text" name="grupo" maxlength="8" value="<?php echo $grupo; ?>" placeholder="Campo obligatorio"/>
                                <?php } ?> 
                            </div>
                            <div class="form-group">
                                <label><?php echo $entry_descripcion; ?></label>
                                <input class="form-control" type="text" name="descripcion" value="<?php echo $descripcion; ?>"  placeholder="Campo obligatorio"/>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <div class="form-group">
                                <div class="row" style="border-bottom: 1px solid gainsboro;">
                                    <div class="col-xs-3"><b>ALTA</b></div>
                                    <div class="col-xs-3"><b>BAJA</b></div>
                                    <div class="col-xs-3"><b>MODIFICACIÓN</b></div>
                                    <div class="col-xs-3"><b>CONSULTA</b></div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <?php foreach ($permissions as $permission) { ?>
                                        <?php if (!$permission['alta']) { ?>
                                        <input type="checkbox" name="altas[]" value="<?php echo $permission['name']; ?>" id="alta_<?php echo $permission['name']; ?>"><label for="alta_<?php echo $permission['name']; ?>"><?php echo $permission['name']; ?></label><br>  
                                        <?php } else { ?>
                                        <input type="checkbox" name="altas[]" value="<?php echo $permission['name']; ?>" id="alta_<?php echo $permission['name']; ?>" checked="checked"><label for="alta_<?php echo $permission['name']; ?>"><?php echo $permission['name']; ?></label><br> 
                                        <?php } ?>
                                        <?php } ?>
                                    </div>
                                    <div class="col-xs-3">
                                        <?php foreach ($permissions as $permission) { ?>
                                        <?php if (!$permission['baja']) { ?>
                                        <input type="checkbox" name="bajas[]" value="<?php echo $permission['name']; ?>" id="baja_<?php echo $permission['name']; ?>"><label for="baja_<?php echo $permission['name']; ?>"><?php echo $permission['name']; ?></label><br> 
                                        <?php } else { ?>
                                        <input type="checkbox" name="bajas[]" value="<?php echo $permission['name']; ?>" id="baja_<?php echo $permission['name']; ?>" checked="checked"><label for="baja_<?php echo $permission['name']; ?>"><?php echo $permission['name']; ?></label><br> 
                                        <?php } ?>
                                        <?php } ?>
                                    </div>
                                    <div class="col-xs-3">
                                        <?php foreach ($permissions as $permission) { ?>
                                        <?php if (!$permission['modificacion']) { ?>
                                        <input type="checkbox" name="modificaciones[]" value="<?php echo $permission['name']; ?>" id="mod_<?php echo $permission['name']; ?>"><label for="mod_<?php echo $permission['name']; ?>"><?php echo $permission['name']; ?></label><br>  
                                        <?php } else { ?>
                                        <input type="checkbox" name="modificaciones[]" value="<?php echo $permission['name']; ?>" id="mod_<?php echo $permission['name']; ?>" checked="checked"><label for="mod_<?php echo $permission['name']; ?>"><?php echo $permission['name']; ?></label><br>  
                                        <?php } ?>
                                        <?php } ?>
                                    </div>
                                    <div class="col-xs-3">
                                        <?php foreach ($permissions as $permission) { ?>
                                        <?php if (!$permission['consulta']) { ?>
                                        <input type="checkbox" name="consultas[]" value="<?php echo $permission['name']; ?>" id="con_<?php echo $permission['name']; ?>"><label for="con_<?php echo $permission['name']; ?>"><?php echo $permission['name']; ?></label><br> 
                                        <?php } else { ?>
                                        <input type="checkbox" name="consultas[]" value="<?php echo $permission['name']; ?>" id="con_<?php echo $permission['name']; ?>" checked="checked"><label for="con_<?php echo $permission['name']; ?>"><?php echo $permission['name']; ?></label><br> 
                                        <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer" style="text-align: -webkit-center;">
            <?php if ($_REQUEST['action'] != 'consulta') { ?>  
            <button type="submit" class="btn btn-primary" onclick="document.getElementById('form').submit();">Guardar</button>
            <?php } ?>   
            <button type="button" class="btn btn-primary" onclick="location = '<?php echo $cancel; ?>'">Cancelar</button>
        </div>
    </form>
</section>