<!-- Content Header (Page header) -->
<section class="content-header">
    <h3><?php echo $heading_title; ?></h3>
    <!-- Error -->
    <?php if ($error_texto_error != '') { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error_texto_error; ?>
    </div>
    <?php } ?> 
    <!-- Error -->
</section>

<!-- Main content -->
<section class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Registro</h3>
            </div>
            <div class="box-body">
                <input type="hidden" value="<?php echo $articulo; ?>" name="articulo" id="articulo">
                <div class="form-group">
                    <label><?php echo $entry_tipoproducto; ?></label>
                    <select class="form-control" tabindex="2" name="tipoproducto" id="tipoproducto">
                        <option value="-1" selected>Seleccione...</option>
                        <?php foreach ($tiposproducto as $loc) {       
                        if ($loc['tipoproducto'] == $tipoproducto){ ?>
                        <option value="<?php echo $loc['tipoproducto']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                        <?php } else{ ?>
                        <option value="<?php echo $loc['tipoproducto']; ?>"><?php echo $loc['descripcion']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>

                <div class="form-group">
                    <label><?php echo $entry_marca; ?></label>
                   <!-- <select class="chosen-select" tabindex="2" name="marca" id="marca" onchange="GetModelos();">
                        <option value="-1" selected>Seleccione...</option>
                        <?php foreach ($marcas as $i) {       
                        if ($i['marca'] == $marca){ ?>
                        <option value="<?php echo $i['marca']; ?>" selected><?php echo $i['descripcion']; ?></option>
                        <?php } else{ ?>
                        <option value="<?php echo $i['marca']; ?>"><?php echo $i['descripcion']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>-->
                   <input type="input" class="form-control" name="auto_marca" id="auto_marca" value="<?php echo $auto_marca; ?>"  placeholder="Ingrese 3 letras y seleccione una opción de la lista."/>
                    <input type="hidden" name="auto_marca_id" id="auto_marca_id" value="<?php echo $auto_marca_id; ?>" />
                </div>

                <div class="form-group">
                    <label><?php echo $entry_modelo; ?></label>
                  <!--  <?php if (!isset($modelo)){ ?>
                    <select class="chosen-select" tabindex="2" name="modelo" id="modelo" disabled>
                    <?php } else{ ?>
                    <select class="chosen-select" tabindex="2" name="modelo" id="modelo">
                    <?php } ?>                   
                        <option value="-1" selected>Seleccione...</option>
                        <?php foreach ($modelos as $i) {       
                        if ($i['modelo'] == $modelo){ ?>
                        <option value="<?php echo $i['modelo']; ?>" selected><?php echo $i['descripcion']; ?></option>
                        <?php } else{ ?>
                        <option value="<?php echo $i['modelo']; ?>"><?php echo $i['descripcion']; ?></option>
                        <?php } ?>
                        <?php } ?>

                    </select>
                  
                  <input type="input" class="form-control" name="auto_modelo" id="auto_modelo" value="<?php echo $auto_modelo; ?>"  placeholder="Ingrese 3 letras y seleccione una opción de la lista."/>
                    <input type="hidden" name="auto_modelo_id" id="auto_modelo_id" value="<?php echo $auto_modelo_id; ?>" />
                  -->
                    <input type="input" class="form-control" name="modelo" id="modelo" value="<?php echo $modelo; ?>"  placeholder="Ingrese la descripción del modelo."/>
                 </div>

                <div class="form-group">
                    <label><?php echo $entry_iva; ?></label>
                    <select class="form-control" tabindex="2" name="iva" id="iva">
                        <option value="-1" selected>Seleccione...</option>
                        <?php foreach ($ivas as $loc) {       
                        if ($loc['iva'] == $iva){ ?>
                        <option value="<?php echo $loc['iva']; ?>" selected><?php echo $loc['iva']; ?></option>
                        <?php } else{ ?>
                        <option value="<?php echo $loc['iva']; ?>"><?php echo $loc['iva']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                
                <div class="form-group">
                    <label><?php echo $entry_tipogama; ?></label>
                    <select class="form-control" tabindex="2" name="tipogama" id="tipogama">
                        <option value="-1" selected>Seleccione...</option>
                        <?php foreach ($tiposgama as $loc) {       
                        if ($loc['tipogama'] == $tipogama){ ?>
                        <option value="<?php echo $loc['tipogama']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                        <?php } else{ ?>
                        <option value="<?php echo $loc['tipogama']; ?>"><?php echo $loc['descripcion']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label><?php echo $entry_mnu; ?></label>
                    <input class="form-control" type="text" name="mnu" value="<?php echo $mnu; ?>" />
                </div>
                <div class="form-group">
                    <label><?php echo $entry_codbarrainterno; ?></label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-barcode"></i>
                        </div>
                        <input type="text" class="form-control" id="codbarrainterno" name="codbarrainterno" value="<?php echo $codbarrainterno; ?>" placeholder="Si no lo ingresa el sistema le asignara uno.">
                    </div>
                </div>
                <div  class="form-group">
                    <label>
                        <input type="checkbox" name="concoddebarra" value="concoddebarra" style="width: 20px;margin-top: -3px;" <?php echo ($concoddebarra == 1) ?  'checked' : '' ?>>
                               <?php echo $entry_concoddebarras; ?>
                    </label>
                </div>
            </div>
        </div>
        <!-- BOTONES INICIO -->
        <div class="box-footer" style="text-align: -webkit-center;">
            <?php if ($_REQUEST['action'] != 'consulta') { ?>  
            <button type="submit" class="btn btn-primary" onclick="document.getElementById('form').submit();">Guardar</button>
            <?php } ?>   
            <button type="button" class="btn btn-primary" onclick="location = '<?php echo $cancel; ?>'">Cancelar</button>
        </div>
        <!-- BOTONES FIN -->
    </form>
</section>

<script type="text/javascript">
$(document).ready(function () {

        $("#auto_marca").autocomplete({
            source: "<?php echo $script_busca_marca; ?>",
            minLength: 2,
            select: function (event, ui) {
                $("#auto_marca_id").val(ui.item.id);
            }
        });
        
        $("#auto_modelo").autocomplete({
            source: "<?php echo $script_busca_modelo; ?>",
            minLength: 2,
            select: function (event, ui) {
                $("#auto_modelo_id").val(ui.item.id);
            }
        });
    });
    //$(document).ready(function () {
        //GetModelos();
    //});

    function GetModelos()
    {
        var idMarca = $("#marca").val();
        $.ajax(
                {
                    dataType: "html",
                    type: "GET",
                    url: "index.php?controller=articulos&action=getModelos",
                    data: "idMarca=" + idMarca,
                    beforeSend: function (data)
                    {
                        $("#modelo").html('<option value="-1" selected="">Cargando...</option>');
                        $('#modelo').trigger('chosen:updated');
                    },
                    success: function (requestData)
                    {
                        if (idMarca != '-1') {
                            $('#modelo').removeAttr('disabled');
                        } else {
                            $('#modelo').attr('disabled', 'disabled');
                        }

                        $("#modelo").html(requestData);  //Usando JQUERY, Cargamos las subcategorias
                        $('#modelo').trigger('chosen:updated'); //Si no no funciona
                    },
                    error: function (requestData, strError, strTipoError)
                    {
                        alert("Error " + strTipoError + ': ' + strError); //En caso de error mostraremos un alert
                    },
                    complete: function (requestData, exito) { }
                });
    }

</script>