<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo $heading_title; ?>
    </h1>
    <div class="breadcrumb">
        <div class="btn-group-horizontal">
            <?php if (@$listar) { ?>
            <button  onclick="location = '<?php echo $list; ?>'"><img style="cursor: pointer" src="template/default/image/img/light-blue_icons/iconos-18.png" class="first" alt="<?php echo $button_list; ?>"></button>
            <?php } ?>   
            <?php if (@$insert) { ?>
            <button class="enabled" onclick="location = '<?php echo $insert; ?>'"><i class="fa fa-plus" style="font-size: 30px" alt="<?php echo $button_insert; ?>" title="Agregar"></i> </button>
            <?php } ?>
            <?php if (@$exportPdf) { ?>
            <button class="enabled"><a href="<?php echo $exportPdf; ?>" title="<?php echo $titulo_ventana; ?>" rel="gb_page_fs[]" ><img style="cursor: pointer" src="template/default/image/img/iconos-10.png" class="first" alt="<?php echo $button_exportarpdf; ?>"></a></button>
            <?php } ?>
            <?php if (@$exportExcel) { ?>
            <button class="enabled"><a onclick="generarInforme()" href="<?php echo $exportExcel; ?>" title="<?php echo $button_exportar_descargar.$button_exportar_excel; ?>"><img src="template/default/image/img/iconos-08.png" class="png"/></a><?php echo $button_exportar_excel; ?></button>
            <?php } ?>
            <button class="search" data-toggle="collapse" data-target="#filter" aria-expanded="false" aria-controls="filter"><i class="fa fa-search" style="font-size: 30px" alt="Buscar" title="Buscar"></i></button>
        </div>
    </div>
</section>
<!-- Main content -->

<section class="content">
    <!-- MENSAJE INICIO -->
    <?php if ($error) { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error; ?>
    </div> 
    <?php } ?>
    <?php if ($message) { ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> ÉXITO!</h4>
        <?php echo $message; ?>
    </div>
    <?php } ?>
    <!-- MENSAJE FIN -->

    <!-- filter -->
    <div class="filter collapse" id="filter" aria-expanded="false" style="height: 0px;">
        <div class="box" style="margin-bottom: 10px;">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form"  style="padding-bottom: 15px;padding-left: 10px;">
                 <div class="titulo">
                    Filtro
                    <button class="search" style="text-align: right; right: 5px; position: absolute;" onclick="limpiafiltro();"><i class="fa fa-search-minus" style="font-size: 18px" alt="Limpiar" title="Limpiar"></i></button>
                    <button class="search" style="text-align: right; right: 40px; position: absolute;" onclick="document.forms[0].submit();"><i class="fa fa-search-plus" style="font-size: 18px" alt="Buscar" title="Buscar"></i></button>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <input class="form-control" type="text" id="search" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $placeholder_buscar; ?>"/>
                    </div>
                </div>
            </form>
        </div>   
    </div>

    <!-- Grilla -->
    <?php include 'grilla.tpl' ?>

</section>

<script language="JavaScript">

    function ActionDelete(direccion) {
        var respuesta = confirm('¿Estás seguro de eliminar la Persona?');

        if (respuesta == true) {
            window.location = direccion;
        }
    }
function limpiafiltro() {
    $("#search").val('');
    document.forms[0].submit();
}
</script>