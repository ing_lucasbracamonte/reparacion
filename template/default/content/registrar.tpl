<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formABM" name="form">
    <div class="generated-table">
        <table class="gradient-style">
            <tr>
                <td width="220">
                    <span class="required">*</span> <?php echo $entry_persona; ?></td>
                <td>
                    <?php if ($_REQUEST['action'] == 'update') { ?> 
                    <input type="text" name="persona" value="<?php echo $persona; ?>" readonly/>
                    <?php } else { ?>
                    <input type="text" name="persona" maxlength="8" value="<?php echo $persona; ?>" onkeydown="return numeric(event)"/>
                    <?php } ?> 
                    <?php if ($error_persona) { ?>
                    <span class="error"><?php echo $error_persona; ?></span>
                    <?php } ?>
                </td>
            </tr>  
            <tr>
                <td><span class="required">*</span> <?php echo $entry_apellido; ?></td>
                <td>
                    <input type="text" name="apellido" value="<?php echo $apellido; ?>" size="50" />
                    <input type="hidden" name="accion_form" value="<?php echo $accion_form; ?>" />
                    <?php if ($error_apellido) { ?>
                    <span class="error"><?php echo $error_apellido; ?></span>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_nombre; ?></td>
                <td>
                    <input type="text" name="nombre" value="<?php echo $nombre; ?>" size="50" />
                    <?php if ($error_nombre) { ?>
                    <span class="error"><?php echo $error_nombre; ?></span>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td><?php echo $entry_fecha; ?></td>
                <td>
                    <input type="text" value="<?php echo $fechanacimiento; ?>" id="fechanacimiento" name="fechanacimiento" size="10" readonly="readonly" style="background-color: black;"/>
                    <?php if ($error_fecha) { ?>
                    <span class="error"><?php echo $error_fecha; ?></span>
                    <?php } ?>
                </td>
            </tr>         
            <tr>
                <td><?php echo $entry_localidad; ?></td>
                <td colspan="3">
                    <input type="input" name="localidad" value="<?php echo $localidad; ?>" id="localidad"/>
                    <?php if ($error_localidad) { ?>
                    <span class="error"><?php echo $error_localidad; ?></span>
                    <?php } ?>
                </td>
            </tr>           
            <tr>
                <td> <?php echo $entry_telefono; ?></td>
                <td><input type="text" name="telefono" value="<?php echo $telefono; ?>" size="50"/>
                    <?php if ($error_telefono) { ?>
                    <span class="error"><?php echo $error_telefono; ?></span>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_mail; ?></td>
                <td>
                    <input type="text" name="mail" value="<?php echo $mail; ?>" size="50"/>
                    <?php if ($error_mail) { ?>
                    <span class="error"><?php echo $error_mail; ?></span>
                    <?php } ?>              
                </td>
            </tr>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_remail; ?></td>
                <td>
                    <input type="text" name="remail" value="<?php echo $remail; ?>" size="50"/>
                    <?php if ($error_remail) { ?>
                    <span class="error"><?php echo $error_remail; ?></span>
                    <?php } ?>              
                </td>
            </tr>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_password; ?></td>
                <td>
                    <input type="password" name="password" value="<?php echo $password; ?>" size="50" />
                    <?php if ($error_password) { ?>
                    <span class="error"><?php echo $error_password; ?></span>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td><span class="required">*</span> <?php echo $entry_repassword; ?></td>
                <td>
                    <input type="password" name="repassword" value="<?php echo $repassword; ?>" size="50" />
                    <?php if ($error_repassword) { ?>
                    <span class="error"><?php echo $error_repassword; ?></span>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br>
                    <br>
                    <button id="btGuardar" onclick="guardarRegistro('<?php echo $actionBoton; ?>');
                            return false;" secondary="ui-icon ui-icon-triangle-1-e">Guardar</button>
                    <button id="btCerrar" onclick="$('#popup').dialog('close');
                            return false;" secondary="ui-icon-close">Cancelar</button>
                </td>
            </tr>            

        </table>
    </div> 
</form>

<script language="JavaScript">

    $(document).ready(function () {
        $("#fechanacimiento").datepicker({
            dateFormat: "dd/mm/yy",
            changeMonth: true,
            changeYear: true,
            yearRange: '<?php echo date("Y")-80; ?>:<?php echo date("Y"); ?>'
        });

    });

    function numeric(e)
    {
        return ((e.keyCode == 46) || (e.keyCode == 9) || (e.keyCode == 8) || (e.keyCode > 32 && e.keyCode < 41) || (e.keyCode > 47 && e.keyCode < 58) || (e.keyCode > 94 && e.keyCode < 106));
    }

</script>