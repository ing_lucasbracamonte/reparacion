<!-- Content Header (Page header) -->
<section class="content-header">
    <h3><?php echo $heading_title; ?></h3>
    <!-- Error -->
    <?php if ($error_texto_error != '') { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error_texto_error; ?>
    </div>
    <?php } ?> 
    <!-- Error -->
</section>

<!-- Main content -->
<section class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Registro</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label>Código</label>
                                <input class="form-control" type="text" name="compra" value="<?php echo $compra; ?>" readonly/>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label><?php echo $entry_fecha; ?></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" value="<?php echo $fecha; ?>" id="fecha" name="fecha" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label><?php echo $entry_formapago; ?></label>
                                <select class="chosen-select" tabindex="2" name="formapago" id="formapago" readonly>
                                    <option value="-1" selected>Seleccione...</option>
                                    <?php foreach ($formaspago as $loc) {       
                                    if ($loc['formapago'] == $formapago){ ?>
                                    <option value="<?php echo $loc['formapago']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                                    <?php } else{ ?>
                                    <option value="<?php echo $loc['formapago']; ?>"><?php echo $loc['descripcion']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label><?php echo $entry_nrotarjeta; ?></label>
                                <input class="form-control" type="text" id="nrotarjeta" name="nrotarjeta" value="<?php echo $nrotarjeta; ?>" disabled/>
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label><?php echo $entry_proveedor; ?></label>
                                <select class="chosen-select" tabindex="2" name="proveedor" id="proveedor" disabled>
                                    <option value="-1" selected>Seleccione...</option>
                                    <?php foreach ($proveedores as $loc) {       
                                    if ($loc['proveedor'] == $proveedor){ ?>
                                    <option value="<?php echo $loc['proveedor']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                                    <?php } else{ ?>
                                    <option value="<?php echo $loc['proveedor']; ?>"><?php echo $loc['descripcion']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label><?php echo $entry_tipodocrelacionado; ?></label>
                                <select class="chosen-select" tabindex="2" name="tipodocrelacionado" id="tipodocrelacionado" disabled>
                                    <option value="-1" selected>Seleccione...</option>
                                    <?php foreach ($tipodocrelacionados as $loc) {       
                                    if ($loc['tipodocumento'] == $tipodocrelacionado){ ?>
                                    <option value="<?php echo $loc['tipodocumento']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                                    <?php } else{ ?>
                                    <option value="<?php echo $loc['tipodocumento']; ?>"><?php echo $loc['descripcion']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label><?php echo $entry_nrodocrelacionado; ?></label>
                                <input class="form-control" type="text" name="nrodocrelacionado" value="<?php echo $nrodocrelacionado; ?>"  readonly/>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label><?php echo $entry_observacion; ?></label>
                                <input class="form-control" type="text" name="observacion" value="<?php echo $observacion; ?>" readonly/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">

                        <div class="col-xs-3">
                            <div class="form-group">
                                <label><?php echo $entry_fechabaja; ?></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" value="<?php echo $fechabaja; ?>" id="fechabaja" name="fechabaja" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" >
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-9">
                            <div class="form-group">
                                <label><?php echo $entry_motivobaja; ?></label>
                                <input class="form-control" type="text" name="motivobaja" value="<?php echo $motivobaja; ?>" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <table id="repuestos" name="repuestos" class="table table-bordered table-striped dataTable" aria-describedby="example1_info">
                    <thead>
                        <tr role="row" id="comprarepuesto_0">
                            <th  role="columnheader" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">
                                Repuesto
                            </th>
                            <th  role="columnheader" tabindex="2" aria-controls="example2" rowspan="1" colspan="1">
                                Descripción
                            </th>
                            <th  role="columnheader" tabindex="2" aria-controls="example2" rowspan="1" colspan="1">
                                Nro. Parte
                            </th>
                            <th  role="columnheader" tabindex="4" aria-controls="example2" rowspan="1" colspan="1">
                                Cantidad
                            </th>
                            <th  role="columnheader" tabindex="5" aria-controls="example2" rowspan="1" colspan="1">
                                $ Compra
                            </th>
                            <th  role="columnheader" tabindex="3" aria-controls="example2" rowspan="1" colspan="1">
                                Subtotal
                            </th>
                        </tr>
                    </thead>
                    <?php $i = 1; ?>
                    <?php foreach ($repuestosasignados as $ad) { ?>
                    <tr id="comprarepuesto_<?php echo $i; ?>" class="even" style="height: 25px;padding: 6px 12px;font-size: 14px;">
                    <input type="hidden" name="repuestoscompra[<?php echo $i; ?>][numero]" value="<?php echo $i; ?>"/>
                    <input type="hidden" name="repuestoscompra[<?php echo $i; ?>][compra]" value="<?php echo $ad['compra']; ?>"/>

                    <td class="col-xs-1"><input type="hidden" name="repuestoscompra[<?php echo $i; ?>][repuesto]" value="<?php echo $ad['repuesto']; ?>"/><?php echo $ad['repuesto']; ?></td>
                    <td class="col-xs-3"><input type="hidden" name="repuestoscompra[<?php echo $i; ?>][descripcion]" value="<?php echo $ad['descripcion']; ?>"/><?php echo $ad['descripcion']; ?></td>

                    <td class="col-xs-2"><input type="hidden" name="repuestoscompra[<?php echo $i; ?>][nroparte]" value="<?php echo $ad['nroparte']; ?>"/><?php echo $ad['nroparte']; ?></td>
                    <td class="col-xs-1"><input type="hidden" name="repuestoscompra[<?php echo $i; ?>][cantidad]" value="<?php echo $ad['cantidad']; ?>"/><?php echo $ad['cantidad']; ?></td>
                    <td class="col-xs-1"><input type="hidden" name="repuestoscompra[<?php echo $i; ?>][preciocompra]" value="<?php echo $ad['preciocompra']; ?>"/><?php echo $ad['preciocompra']; ?></td>
                    <td class="col-xs-1"><input type="hidden" name="repuestoscompra[<?php echo $i; ?>][subtotal]" value="<?php echo $ad['subtotal']; ?>"/><?php echo $ad['subtotal']; ?></td>
                    </tr>
                    <?php $i++; ?>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- BOTONES INICIO  -->
        <div class="box-footer" style="text-align: -webkit-center;">
            <button type="submit" class="btn btn-primary"  onclick="document.getElementById('form').submit();">Guardar</button>
            <button type="button" class="btn btn-primary" onclick="location = '<?php echo $cancel; ?>'">Cancelar</button>
        </div>
        <!-- BOTONES FIN -->
    </form>
</section>


<script type="text/javascript">

    $(function () {
        $('.chosen-select').chosen();
        $('.chosen-select-deselect').chosen({allow_single_deselect: true});
    });

    $(function () {
        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
        //Money Euro
        $("[data-mask]").inputmask();
    });

    function guardar() {
//hacer validaciones aca
        $.ajax({
            type: 'POST',
            url: 'index.php?controller=compras&action=cancelacion',
            data: $('#formABM').serialize(),
            async: false,
            success: function (data) {
//            alert(data);
                if (data != 'ok') {
                    $('#popup').html(data);
                } else {
                    location.reload(true);
                }
            }
        });

    }

    function numeric(e, tipo)
    {
        if (tipo == 'int') {
            return ((e.keyCode == 9) || (e.keyCode == 8) || (e.keyCode > 32 && e.keyCode < 41) || (e.keyCode > 44 && e.keyCode < 58) || (e.keyCode > 94 && e.keyCode < 106));
        } else if (tipo == 'dec') {
            return ((e.keyCode == 9) || (e.keyCode == 8) || (e.keyCode == 110 || e.keyCode == 190) || (e.keyCode > 32 && e.keyCode < 41) || (e.keyCode > 44 && e.keyCode < 58) || (e.keyCode > 94 && e.keyCode < 106));
        }
    }

    function IsNumeric(valor)
    {
        var log = valor.length;
        var sw = "S";
        for (x = 0; x < log; x++)
        {
            v1 = valor.substr(x, 1);
            v2 = parseInt(v1);
//Compruebo si es un valor numérico 
            if (isNaN(v2)) {
                sw = "N";
            }
        }
        if (sw == "S") {return true;} else {return false; }
    }

    var primerslap = false;
    var segundoslap = false;
    function formateafecha(fecha)
    {
        var long = fecha.length;
        var dia;
        var mes;
        var ano;

        if ((long >= 2) && (primerslap == false)) {
            dia = fecha.substr(0, 2);
            if ((IsNumeric(dia) == true) && (dia <= 31) && (dia != "00")) {
                fecha = fecha.substr(0, 2) + "/" + fecha.substr(3, 7);
                primerslap = true;
            } else {
                fecha = "";
                primerslap = false;
            }
        } else
        {
            dia = fecha.substr(0, 1);
            if (IsNumeric(dia) == false)
    {fecha="";}
            if ((long <= 2) && (primerslap = true))
    {fecha=fecha.substr(0,1); primerslap=false; }
        }
        if ((long >= 5) && (segundoslap == false))
        {
            mes = fecha.substr(3, 2);
            if ((IsNumeric(mes) == true) && (mes <= 12) && (mes != "00")) {
                fecha = fecha.substr(0, 5) + "/" + fecha.substr(6, 4);
                segundoslap = true;
            } else {
                fecha = fecha.substr(0, 3);
                ;
                segundoslap = false;
            }
        } else {
            if ((long <= 5) && (segundoslap = true)) {
                fecha = fecha.substr(0, 4);
                segundoslap = false;
            }
        }
        if (long >= 7)
        {
            ano = fecha.substr(6, 4);
            if (IsNumeric(ano) == false) {
                fecha = fecha.substr(0, 6);
            } else {
                if (long == 10) {
                    if ((ano == 0) || (ano < 1900) || (ano > 2100)) {
                        fecha = fecha.substr(0, 6);
                    }
                }
            }
        }

        if (long >= 10)
        {
            fecha = fecha.substr(0, 10);
            dia = fecha.substr(0, 2);
            mes = fecha.substr(3, 2);
            ano = fecha.substr(6, 4);
// Año no viciesto y es febrero y el dia es mayor a 28 
            if ((ano % 4 != 0) && (mes == 02) && (dia > 28)) {
                fecha = fecha.substr(0, 2) + "/";
            }
        }
        return (fecha);
    }

</script>