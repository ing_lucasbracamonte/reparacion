<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formABM" name="form">
    <div class="row">
        <div class="col-sm-12 col-lg-12" >
            <table class="display" style="border-collapse: inherit !important;">
                <tbody style="background-color: transparent; ">
                <input type="hidden" value="<?php echo $tipolocal; ?>" name="tipolocal" id="tipolocal">
                <tr>
                    <td><span class="required">*</span> <?php echo $entry_descripcion; ?></td>
                    <td><input type="text" name="descripcion" value="<?php echo $descripcion; ?>"/>
                        <?php if ($error_descripcion) { ?>
                        <span class="error"><?php echo $error_descripcion; ?></span>
                        <?php } ?>              
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="height: 15px"></td>
                </tr>   
                <tr>
                    <td colspan="2" style="text-align: end;border-top: 1px solid #F79C15; ">
                        <?php if ($_REQUEST['action'] != 'consulta') { ?>
                        <img src="template/default/image/img/iconos-25.png" class="png" id="btGuardar" onclick="ActionSave('<?php echo $actionBoton; ?>');" alt="Borrar fecha" title="Guardar" style="width: 7%;margin-right: 9px;cursor: pointer;" />
                        <?php } ?>
                        <img src="template/default/image/img/iconos-13.png" class="png" id="btCerrar" onclick="$('#popup').dialog('close');
                                return false;" alt="Cancelar" title="Borrar fecha" style="width: 7%;margin-right: 0%;margin-left: 20px;cursor: pointer;" />
                    </td>
                </tr>
                </tbody>
            </table>
        </div> 
    </div>
</form>