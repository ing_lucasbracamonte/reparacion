<?php
// Agrego los CSS para condensar luego
if(isset($this->head_def)){
$this->head_def->setcss("/css/tab.css");
$this->head_def->setcss("/css/list.css");
} ?>

<?php if ($error) { ?>
<div class="warning"><?php echo $error; ?></div>
<?php } ?>
<?php if ($message) { ?>
<div class="message"><?php echo $message; ?></div>
<?php } ?>

<div class="block_top activity" style="height: 10%;margin-bottom: 10px;">
    <div class="main_sidebar">
        <img src="template/default/image/img/gold_icons/NOTICIAS.png"></img>
        <h3><?php echo $heading_title; ?></h3>
    </div>
    <!-- List actions -->
    <div class="actions-table" style="height: 5%;">
        <div class="actions">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">Fecha: 
                <select name="search" id="search" style="background-color: #464545;" onchange="this.form.submit()">            
                    <?php foreach ($logs as $index => $loc) {
                    if ($loc <> '.' && $loc <> '..')
                    if ($index == $selectedindex){ ?>
                    <option value="<?php echo $index; ?>" selected><?php echo $loc; ?></option>
                    <?php } else{ ?>
                    <option value="<?php echo $index; ?>"><?php echo $loc; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select>
            </form>
        </div>
    </div>
</div>
<div style="height: 88%"> 
    <div class="generated-table" style="text-align: left !important; overflow-y: scroll; background-color: black; font-family: 'Courier New', Courier, monospace; font-size: 12pt;">
        <?php echo nl2br(htmlentities($log,ENT_QUOTES)); ?>
    </div>        
</div>
<?php if ($error) { ?>
<div class="warning"><?php echo $error; ?></div>
<?php } ?>
