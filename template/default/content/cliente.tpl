<!-- Content Header (Page header) -->
<section class="content-header">
    <h3><?php echo $heading_title; ?></h3>
    <!-- Error -->
    <?php if ($error_texto_error != '') { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error_texto_error; ?>
    </div>
    <?php } ?> 
    <!-- Error -->
</section>

<!-- Main content -->
<section class="content">

    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Registro</h3>
        </div>
        <div class="box-body">
            <?php include 'cliente_insert.tpl' ?>              
        </div>
    </div>
    <!-- BOTONES INICIO -->
    <div class="box-footer" style="text-align: -webkit-center;">
        <?php if ($_REQUEST['action'] != 'consulta') { ?>  
        <button type="submit" class="btn btn-primary" onclick="document.getElementById('formCliente').submit();">Guardar</button>
        <?php } ?>   
        <button type="button" class="btn btn-primary" onclick="location = '<?php echo $cancel; ?>'">Cancelar</button>
    </div>
    <!-- BOTONES FIN -->

</section>


