<!-- Content Header (Page header) -->
<section class="content-header">
    <h3><?php echo $heading_title; ?></h3>
    <!-- Error -->
    <?php if ($error_texto_error != '') { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error_texto_error; ?>
    </div>
    <?php } ?> 
    <!-- Error -->
</section>

<!-- Main content -->
<section class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Registro</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label>Código</label>
                                <input class="form-control" type="text" id="dua" name="dua" value="<?php echo $dua; ?>" readonly/>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label><?php echo $entry_fecha; ?></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <?php if ($_REQUEST['action'] == 'insert') { ?> 
                                    <input type="text" class="form-control" value="<?php echo $fecha; ?>" id="fecha" name="fecha" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" placeholder="Campo obligatorio">
                                    <?php } else { ?> 
                                    <input type="text" class="form-control" value="<?php echo $fecha; ?>" id="fecha" name="fecha" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" placeholder="Campo obligatorio" readonly>
                                    <?php } ?> 
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label><?php echo $entry_tipodua; ?></label>
                                <input class="form-control" type="text" id="tipodua" name="tipodua" value="<?php echo $tipodua; ?>" readonly/>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label><?php echo $entry_tipoarqueo; ?></label>
                                <?php if ($_REQUEST['action'] == 'insert') { ?> 
                                <select class="form-control" tabindex="2" name="tipoarqueo" id="tipoarqueo"  onchange="CambiarTipoArqueo();">
                                    <?php } else { ?>
                                    <select class="form-control" tabindex="2" name="tipodua" id="tipodua" onchange="CambiarTipoArqueo();" disabled>
                                        <?php } ?> 
                                        <option value="-1" selected>Seleccione...</option>
                                        <?php foreach ($tiposarqueo as $loc) {       
                                        if ($loc['tipoarqueo'] == $tipoarqueo){ ?>
                                        <option value="<?php echo $loc['tipoarqueo']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                                        <?php } else{ ?>
                                        <option value="<?php echo $loc['tipoarqueo']; ?>"><?php echo $loc['descripcion']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                            </div>
                        </div>
                        </div>
                    </div>
                
                <div class="form-group">
                    <div class="row">
                        
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label><?php echo $entry_entidadorigen; ?></label>
                                <?php if ($_REQUEST['action'] == 'insert') { ?> 
                                <input type="input" class="form-control" name="auto_entidadorigen" id="auto_entidadorigen" value="<?php echo $auto_entidadorigen; ?>" placeholder="Ingrese 3 letras y seleccione una opción de la lista" readonly/>
                                <?php } else { ?>
                                <input type="input" class="form-control" name="auto_entidadorigen" id="auto_entidadorigen" value="<?php echo $auto_entidadorigen; ?>" placeholder="Ingrese 3 letras y seleccione una opción de la lista" readonly/>
                                <?php } ?> 
                                <input type="hidden" name="auto_entidadorigen_dua" id="auto_entidadorigen_dua" value="<?php echo $auto_entidadorigen_dua; ?>" />
                            </div>
                        </div>
                        
                         <div class="col-xs-6">
                            <div class="form-group">
                                <label><?php echo $entry_observacion; ?></label>
                                <?php if ($_REQUEST['action'] == 'insert') { ?> 
                                <input class="form-control" type="text" id="observacion" name="observacion" value="<?php echo $observacion; ?>" />
                                <?php } else { ?>
                                <input class="form-control" type="text" id="observacion" name="observacion" value="<?php echo $observacion; ?>" readonly/>
                                <?php } ?> 
                            </div>
                        </div>
                    </div>
                </div>
               </div> 
            </div>
            <div class="box">
                <?php include 'duaarqueo_grilla_articulos.tpl' ?> 
            </div>
        </div>
        <!-- BOTONES INICIO  -->
        <div class="box-footer" style="text-align: -webkit-center;">
            <?php if ($_REQUEST['action'] == 'insert') { ?>  
            <button type="submit" class="btn btn-primary" onclick="document.getElementById('form').submit();">Guardar</button>
            <?php } ?>   
            <?php if ($_REQUEST['action'] == 'delete') { ?>  
            <button type="submit" class="btn btn-primary" onclick="document.getElementById('form').submit();">Guardar</button>
            <?php } ?>   
            <button type="button" class="btn btn-primary" onclick="location = '<?php echo $cancel; ?>'">Cancelar</button>
        </div>
        <!-- BOTONES FIN -->
    </form>
</section>

<script type="text/javascript">

    $(document).ready(function () {

        $("#auto_entidadorigen").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "<?php echo $script_busca_entidadorigen; ?>",
                    data: {
                        term: $("#auto_entidadorigen").val(),
                        //tipodua: $("#tipodua").val()
                    },
                    dataType: "json",
                    type: "GET",
                    success: function (data) {
                        response(data);
                    }
                });
            },
            minLength: 3,
            select: function (event, ui) {
                $("#auto_entidadorigen_dua").val(ui.item.id);


                //habilito el detalle.
                DeshabilitaHabilitaDetalle(false);
            }
        });

        

        //CambiarEstadoDua();
        //CambiarTipoDua();
        
        $("#auto_articulodua").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "index.php?controller=duasarqueo&action=getArticulo",
                    data: {
                        term: $("#auto_articulodua").val(),
                        //tipodua: $("#tipodua").val(),
                        entidadorigen: $("#auto_entidadorigen_dua").val()
                    },
                    dataType: "json",
                    type: "GET",
                    success: function (data) {
                        //alert(data);
                        response(data);
                    }
                });
            },
            minLength: 3,
            select: function (event, ui) {
                $("#auto_articulodua_dua").val(ui.item.id);

                //obtengo json y lo convierto en un objeto
                $("#articulo").val(ui.item.id);
                $("#descmodelo").val(ui.item.value);
                $("#codbarra").val(ui.item.codbarra);
                $("#concoddebarra").val(ui.item.concoddebarra);
                $("#codbarrainterno").val(ui.item.codbarrainterno);
                $("#cantidad").val(ui.item.cantidad);
                //if ($("#tipodua").val() == 'T') { //TRANSFERENCIA
                    $("#cantidad_maxima").val(ui.item.cantidad);
                //}
                //alert(ui.item.precioultcompra);
                //$("#preciocompra").val(ui.item.precioultcompra);


                if (ui.item.concoddebarra == '1') {
                    $('#codbarra').attr('disabled', true);
                    //$('#aduana').attr('disabled', false);
                    $('#codbarrainterno').attr('disabled', true);
                    $("#cantidad").val('1');
                    $('#cantidad').attr('disabled', true);
                    //$("#codbarrainterno").val('');
                } else {
                    $('#codbarra').attr('disabled', true);
                    //$('#aduana').attr('disabled', true);
                    $('#cantidad').attr('disabled', false);
                    $("#codbarra").val('');
                }
            }
        });
    });

    
    function DeshabilitarCabecera() {

        if ($('#articulos tr').size() > '2') {
           var valor = true;
            $('#tipodua').attr('readonly', 'readonly');
            $('#auto_entidadorigen').attr('readonly', 'readonly');
            $('#observacion').attr('readonly', 'readonly');
        } else {
            var valor = false;
            $('#tipodua').removeAttr('readonly', 'readonly');
            $('#auto_entidadorigen').removeAttr('readonly', 'readonly');
            
            
            $('#observacion').removeAttr('readonly', 'readonly');
        }

        //CambiarEstadoDua();
        //CambiarTipoDua();
    }

    function CambiarTipoArqueo() {

        var tipoarqueo = $("#tipoarqueo").val();
        if (tipoarqueo != '-1') {
            $('#auto_entidadorigen').removeAttr('readonly');
            
            $('#auto_entidadorigen').val('');
            $('#auto_entidadorigen_dua').val('');

        } else {
            $('#auto_entidadorigen').val('');
            $('#auto_entidadorigen_dua').val('');
            $('#auto_entidadorigen').attr('readonly', 'readonly');
            
            //deshabilito el detalle.
            DeshabilitaHabilitaDetalle(true);
        }


    }

    
    function AgregarArticulo() {
        var articulo = $("#auto_articulodua_dua").val();
        var dua = $("#dua").val();
        var tipodua = 'A';
        var cantidad = $("#cantidad").val();
        var cantidad_maxima = parseInt($("#cantidad_maxima").val());
        var codbarra = $("#codbarra").val();
        //var preciocompra = $("#preciocompra").val();
        var codbarrainterno = $("#codbarrainterno").val();
        var cantReg = $('#articulos tr').size();
        var concoddebarra =$("#concoddebarra").val();

        //debugger;
        if (articulo == '') {
            alert('Antes de agregar un articulo debe seleccionar uno.');
        } else if (concoddebarra == '1' && codbarra == '') {
            alert('El articulo seleccionado necesita que se le cargue un código de barra.');
        } else if (cantidad == 0) {
            alert('Debe ingresar una cantidad.');
        } else if (cantidad < 0) {
            alert('La cantidad debe ser mayor a 0.');
        } else if (tipodua == 'A' && cantidad > cantidad_maxima) {
            alert('La cantidad NO debe ser mayor a ' + cantidad_maxima);
            $("#cantidad").val(cantidad_maxima);
        } else {
             //antes de ir a buscarloy agregar la linea, validar que si es de codigo de barras ya no se encuentre asignado en la grilla

            var table = document.getElementById("articulos");
            var tableArr = [];
            //console.log(table);
            for (var i = 0; i < table.rows.length; i++) {
                var valo = table.rows[i].cells[2].innerHTML;
                //var arregloDeSubCadenas = valo.split(">");
                //alert(arregloDeSubCadenas[1]);

                tableArr.push({
                    articulo: table.rows[i].cells[0].innerHTML,
                    descripcion: table.rows[i].cells[1].innerHTML,
                    codbarra: table.rows[i].cells[2].innerHTML,
                    codbarrainterno: table.rows[i].cells[3].innerHTML,
                    cantidad: table.rows[i].cells[4].innerHTML,
                    precioventa: table.rows[i].cells[5].innerHTML
                });
            }

            //console.log(tableArr);

            var productoYaCargado = 'no';
            for (var i = 0; i < tableArr.length; i++) {

                if (codbarra != '') {
                    var texto = tableArr[i]['codbarra'];
                    var n = texto.includes(codbarra);
                    if (n) {
                        productoYaCargado = 'si';
                    }
                }
            }
            if (productoYaCargado == 'no') {

            $.ajax({
                type: 'GET',
                url: 'index.php?controller=duasarqueo&action=AgregarArticulo',
                data: 'dua=' + dua + '&articulo=' + articulo + '&cantidad=' + cantidad + '&codbarra=' + codbarra + '&codbarrainterno=' + codbarrainterno +  '&cantReg=' + cantReg,
                async: false,
                success: function (data) {
                    if (data != 'error') {
                        $('#articulos').append(data);

                        $("#codbarra").val('');
                        $("#codbarrainterno").val(codbarrainterno);
                        $("#cantidad").val('1');

                    } else {
                        alert('En este artículo es obligatorio la carga del codigo de barras.');
                    }
                }
            });

            var tipodua = $("#tipodua").val();
            if (tipodua == 'A') { //ARQUEO
                    
                    $('#hideANDseek3').hide();
            }
            else {
                    $('#hideANDseek3').show();
            }

            
            DeshabilitarCabecera();

            LimpiaDetalle(concoddebarra);
        } else {
                alert('El producto ya se encuentra cargado');
            }
            
        }
    }

    function LimpiaDetalle(concoddebarra) {
        
        //if (concoddebarra == 1) {
           // setTimeout(function () {
                //$('input[id="codbarra"]').focus();
            //}, 1000);
            //return;
        //}
        $("#auto_articulodua_dua").val('');
        $("#auto_articulodua").val('');

        $("#articulo").val('');
        $("#descmodelo").val('');
        $("#concoddebarra").val('');
        $("#codbarrainterno").val('');
        $("#cantidad").val('1');
        $("#cantidad_maxima").val('');
        //$("#preciocompra").val('');
    }

    function DeshabilitaHabilitaDetalle(valor) {
        if (valor == true) {
            $('#auto_articulodua').attr('readonly', 'readonly');
        } else {
            $('#auto_articulodua').removeAttr('readonly', 'readonly');
        }
        $("#descmodelo").prop('disabled', valor);
        $("#concoddebarra").prop('disabled', valor);
        $("#codbarrainterno").prop('disabled', valor);
        $("#cantidad").prop('disabled', valor);
        //$("#preciocompra").prop('disabled', valor);
    }

    function removeArticulo(row) {
        $('#' + row).remove();
        DeshabilitarCabecera();
    }


</script>