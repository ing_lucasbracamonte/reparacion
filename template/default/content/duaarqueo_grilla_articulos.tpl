<table id="articulos_encavezado" name="articulos_encavezado" class="table table-bordered table-striped dataTable" aria-describedby="example1_info">
    <thead>
        <tr role="row" >
            <th  role="columnheader" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">
                Artículo
            </th>
            <th  role="columnheader" tabindex="1" aria-controls="example2" rowspan="1" colspan="1">
                Descripción
            </th>
            <th  role="columnheader" tabindex="2" aria-controls="example2" rowspan="1" colspan="1">
                Cod. Barras
            </th>
            <th  role="columnheader" tabindex="3" aria-controls="example2" rowspan="1" colspan="1">
                Cod. Barras Interno
            </th>
            <th  role="columnheader" tabindex="4" aria-controls="example2" rowspan="1" colspan="1">
                Cantidad
            </th>
            
            <th  role="columnheader" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" style="text-align: center">
                Acción
            </th>
        </tr>
    </thead>
    <?php if ($_REQUEST['action'] == 'insert') { ?> 
    <tbody aria-live="polite" aria-relevant="all">
        <tr class="odd"  >
            <td class="col-xs-1">
                <input class="form-control" type="text" name="articulo" id="articulo" value="<?php echo $articulo; ?>"  disabled/>
            </td>
            <td class="col-xs-4">
                <input type="input" class="form-control" name="auto_articulodua" id="auto_articulodua" value="<?php echo $auto_articulodua; ?>" placeholder="Ingrese 3 letras y seleccione una opción de la lista" readonly/>
                <input type="hidden" name="auto_articulodua_dua" id="auto_articulodua_dua" value="<?php echo $auto_articulodua_dua; ?>" />                
            </td>
            <td class="col-xs-2">
                <input type="hidden" name="concoddebarra" id="concoddebarra" value="<?php echo $concoddebarra; ?>" /> 
                <input class="form-control" type="text" name="codbarra" id="codbarra" value="<?php echo $codbarra; ?>"  disabled />
            </td>
            <td class="col-xs-2">
                <input class="form-control" type="text" name="codbarrainterno" id="codbarrainterno" value="<?php echo $codbarrainterno; ?>"  disabled/>
            </td>
            <td class="col-xs-1">
                <input class="form-control" type="text" name="cantidad" id="cantidad" value="<?php echo $cantidad; ?>"  onkeydown="return numeric(event, 'int');"/>
                <input type="hidden" name="cantidad_maxima" id="cantidad_maxima" value="" />                
            </td>
           

            <td class="col-xs-1" align="center">
                <button class="enabled" onclick="AgregarArticulo();
                        return false;"><i class="fa fa-plus" style="font-size: 25px" alt="<?php echo $button_insert; ?>" title="Agregar"></i> </button>
            </td>
        </tr>
        <?php } ?>
        </tbody>
        </table>
        <table id="articulos" name="articulos" class="table table-bordered table-striped dataTable" aria-describedby="example1_info">
        <?php $i = 1; ?>
        <?php foreach ($articulosasignados as $ad) { ?>

        <tr id="duaarticulo_<?php echo $i; ?>" class="even" style="height: 25px;padding: 6px 12px;font-size: 14px;">
    <input type="hidden" name="articulosdua[<?php echo $i; ?>][numero]" value="<?php echo $i; ?>"/>
    <input type="hidden" name="articulosdua[<?php echo $i; ?>][dua]" value="<?php echo $ad['dua']; ?>"/>

    <td class="col-xs-1"><input type="hidden" name="articulosdua[<?php echo $i; ?>][articulo]" value="<?php echo $ad['articulo']; ?>"/><?php echo $ad['articulo']; ?></td>
    <td class="col-xs-4"><input type="hidden" name="articulosdua[<?php echo $i; ?>][descmodelo]" value="<?php echo $ad['descmodelo']; ?>"/><?php echo $ad['descmodelo']; ?></td>

    <td class="col-xs-2"><input type="hidden" name="articulosdua[<?php echo $i; ?>][codbarra]" value="<?php echo $ad['codbarra']; ?>"/><?php echo $ad['codbarra']; ?></td>
    <td class="col-xs-2"><input type="hidden" name="articulosdua[<?php echo $i; ?>][codbarrainterno]" value="<?php echo $ad['codbarrainterno']; ?>"/><?php echo $ad['codbarrainterno']; ?></td>
    <td class="col-xs-1"><input type="hidden" name="articulosdua[<?php echo $i; ?>][cantidad]" value="<?php echo $ad['cantidad']; ?>"/><?php echo $ad['cantidad']; ?></td>
    
    <td class="col-xs-1" align="center">
        <?php if ($_REQUEST['action'] == 'insert') { ?> 
        <a href="/" onclick="removeArticulo('duaarticulo_<?php echo $i; ?>');
                return false;"><i class="fa fa-fw fa-trash-o" alt="Borrar" title="Borrar" > </i></a>
        <?php } ?>
    </td>
</tr>
<?php $i++; ?>
<?php } ?>


</table>