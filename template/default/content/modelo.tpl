<!-- Content Header (Page header) -->
<section class="content-header">
    <h3><?php echo $heading_title; ?></h3>
    <!-- Error -->
    <?php if ($error_texto_error != '') { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error_texto_error; ?>
    </div>
    <?php } ?> 
    <!-- Error -->
</section>

<!-- Main content -->
<section class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Registro</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <label><?php echo $entry_modelo; ?></label>
                    <?php if ($_REQUEST['action'] == 'update'){ ?> 
                    <input class="form-control" type="text" name="modelo" value="<?php echo $modelo; ?>" readonly/>
                    <?php } else { ?>
                    <input class="form-control" type="text" name="modelo" value="<?php echo $modelo; ?>" maxlength="8"/>
                    <?php } ?> 
                </div>
                <div class="form-group">
                    <label><?php echo $entry_descripcion; ?></label>
                    <input class="form-control" type="text" name="descripcion" value="<?php echo $descripcion; ?>" />
                </div>
                <div class="form-group">
                    <label><?php echo $entry_marca; ?></label>
                    <select class="chosen-select" tabindex="2" name="marca" id="marca">
                        <option value="-1" selected>Seleccione...</option>
                        <?php foreach ($marcas as $loc) {       
                        if ($loc['marca'] == $marca){ ?>
                        <option value="<?php echo $loc['marca']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                        <?php } else{ ?>
                        <option value="<?php echo $loc['marca']; ?>"><?php echo $loc['descripcion']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <!-- BOTONES INICIO -->
        <div class="box-footer" style="text-align: -webkit-center;">
            <?php if ($_REQUEST['action'] != 'consulta') { ?>  
            <button type="submit" class="btn btn-primary" onclick="document.getElementById('form').submit();">Guardar</button>
            <?php } ?>   
            <button type="button" class="btn btn-primary" onclick="location = '<?php echo $cancel; ?>'">Cancelar</button>
        </div>
        <!-- BOTONES FIN -->
    </form>
</section>

<script type="text/javascript">

    $(function () {
        $('.chosen-select').chosen();
        $('.chosen-select-deselect').chosen({allow_single_deselect: true});
    });

    function numeric(e, tipo)
    {
        if (tipo == 'int') {
            return ((e.keyCode == 9) || (e.keyCode == 8) || (e.keyCode > 32 && e.keyCode < 41) || (e.keyCode > 44 && e.keyCode < 58) || (e.keyCode > 94 && e.keyCode < 106));
        } else if (tipo == 'dec') {
            return ((e.keyCode == 9) || (e.keyCode == 8) || (e.keyCode == 110 || e.keyCode == 190) || (e.keyCode > 32 && e.keyCode < 41) || (e.keyCode > 44 && e.keyCode < 58) || (e.keyCode > 94 && e.keyCode < 106));
        }
    }

</script>


