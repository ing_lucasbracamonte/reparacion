<!-- Content Header (Page header) -->
<section class="content-header">
    <h3><?php echo $heading_title; ?></h3>
    <!-- Error -->
    <?php if ($error_texto_error != '') { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error_texto_error; ?>
    </div>
    <?php } ?> 
    <!-- Error -->
</section>

<!-- Main content -->
<section class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Registro</h3>
            </div>
            <div class="box-body">
                <input type="hidden" value="<?php echo $localidad; ?>" name="localidad" id="localidad">
                <div class="form-group">
                    <label><?php echo $entry_descripcion; ?></label>
                    <input class="form-control" type="text" name="descripcion" value="<?php echo $descripcion; ?>" />
                </div>
                <div class="form-group">
                    <label><?php echo $entry_provincia; ?></label>
                    <input class="form-control" type="input" name="auto_provincia" id="auto_provincia" value="<?php echo $auto_provincia; ?>"/>
                    <input type="hidden" name="auto_provincia_localidad" id="auto_provincia_localidad" value="<?php echo $auto_provincia_localidad; ?>"/>
                </div>
            </div>
        </div>
        <!-- BOTONES INICIO -->
        <div class="box-footer" style="text-align: -webkit-center;">
            <?php if ($_REQUEST['action'] != 'consulta') { ?>  
            <button type="submit" class="btn btn-primary" onclick="document.getElementById('form').submit();">Guardar</button>
            <?php } ?>   
            <button type="button" class="btn btn-primary" onclick="location = '<?php echo $cancel; ?>'">Cancelar</button>
        </div>
        <!-- BOTONES FIN -->
    </form>
</section>

<script type="text/javascript">
    
    $(document).ready(function () {
        $("#auto_provincia").autocomplete({
            source: "<?php echo $script_busca_provincia; ?>",
            minLength: 3,
            select: function (event, ui) {
                $("#auto_provincia_localidad").val(ui.item.id)
            }
        });
    });
    
</script>