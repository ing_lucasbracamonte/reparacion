<section class="content">
    <!-- MENSAJE INICIO -->
    <?php if ($error) { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error; ?>
    </div> 
    <?php } ?>
    <?php if ($message) { ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> ÉXITO!</h4>
        <?php echo $message; ?>
    </div>
    <?php } ?>
    <!-- MENSAJE FIN -->
    <!-- filter -->
    <form action="<?php echo $exportExcel; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <div class="box" style="margin-bottom: 10px;">
            <div class="titulo">
                Filtro
            </div>
            <div class="box-body">
                <div class="form-group">
                    <label>Tipos de Productos</label>
                    <div class="form-control checkbox form-checklist"  >
                        <label><input type="checkbox" id="tiposproductos" onclick="marcarTiposProducto(this);" />Marcar/Desmarcar Todos</label><br>
                        <?php foreach ($tiposproducto as $tipo) { ?>
                        <?php if ($tipo['tipoproducto'] != '0') { ?>
                        <label for="tipo_<?php echo $tipo['tipoproducto']; ?>">
                            <input type="checkbox" name="tipos[]" value="<?php echo $tipo['tipoproducto']; ?>" id="tipo_<?php echo $tipo['tipoproducto']; ?>">
                            <?php echo $tipo['descripcion']; ?></label><br> 
                        <?php } else { ?>
                        <label for="tipo_<?php echo $tipo['tipoproducto']; ?>">
                            <input type="checkbox" name="tipos[]" value="<?php echo $tipo['tipoproducto']; ?>" id="tipo_<?php echo $tipo['tipoproducto']; ?>" checked="checked">
                            <?php echo $tipo['descripcion']; ?></label><br>
                        <?php } ?>
                        <?php } ?>
                    </div> 
                </div>
                <div class="form-group">
                    <label>Marcas</label>
                    <div class="form-control checkbox form-checklist"  >
                        <label><input type="checkbox" id="marcas" onclick="marcarMarcas(this);" />Marcar/Desmarcar Todos</label><br>
                        <?php foreach ($marcas as $marca) { ?>
                        <?php if ($marca['marca']  != '0') { ?>
                        <label for="marca_<?php echo $marca['marca']; ?>">
                            <input type="checkbox" name="marcas[]" value="<?php echo $marca['marca']; ?>" id="marca_<?php echo $marca['marca']; ?>">
                            <?php echo $marca['descripcion']; ?></label><br> 
                        <?php } else { ?>
                        <label for="marca_<?php echo $marca['marca']; ?>">
                            <input type="checkbox" name="marcas[]" value="<?php echo $marca['marca']; ?>" id="marca_<?php echo $marca['marca']; ?>" checked="checked">
                            <?php echo $marca['descripcion']; ?></label><br>
                        <?php } ?>
                        <?php } ?>
                    </div> 
                </div>
            </div>
        </div>
    </form>      
    <!-- BOTONES INICIO -->
    <div class="box-footer" style="text-align: -webkit-center;">
        <?php if (@$exportExcel) { ?>
        <button type="submit" class="btn btn-primary" onclick="document.getElementById('form').submit();">Exporter Lista</button>
        <?php } ?>
        <?php if (@$insertImport) { ?>
        <button type="submit" class="btn btn-primary" onclick="location = '<?php echo $insertImport; ?>'">Importar Precios</button>
        <?php }  ?>
        <button type="button" class="btn btn-primary" onclick="location = '<?php echo $cancel; ?>'">Cancelar</button>
    </div>
    <!-- BOTONES FIN -->
</section>

<style>
    .chosen-container
    {
        width: 100% !important;
    }
</style>

<script type="text/javascript">

    $(function () {
        $('.chosen-select').chosen();
        $('.chosen-select-deselect').chosen({allow_single_deselect: true});

    });

    function marcarTiposProducto(source)
    {
        checkboxes = document.form.getElementsByTagName('input'); //obtenemos todos los controles del tipo Input
        for (i = 0; i < checkboxes.length; i++) //recoremos todos los controles
        {
            if (checkboxes[i].type == "checkbox" && checkboxes[i].name == 'tipos[]') //solo si es un checkbox entramos
            {
                checkboxes[i].checked = source.checked; //si es un checkbox le damos el valor del checkbox que lo llamó (Marcar/Desmarcar Todos)
            }
        }
    }

    function marcarMarcas(source)
    {
        checkboxes = document.form.getElementsByTagName('input'); //obtenemos todos los controles del tipo Input
        for (i = 0; i < checkboxes.length; i++) //recoremos todos los controles
        {
            if (checkboxes[i].type == "checkbox" && checkboxes[i].name == 'marcas[]') //solo si es un checkbox entramos
            {
                checkboxes[i].checked = source.checked; //si es un checkbox le damos el valor del checkbox que lo llamó (Marcar/Desmarcar Todos)
            }
        }
    }

    function exportExcel() {

        $.ajax({
            type: 'POST',
            url: 'index.php?controller=listaprecios&action=exportExcel',
            data: $('#form').serialize(),
            async: false,
            success: function (data) {

                if (data != 'ok') {

                } else {
                    //location.reload(true);
                }
            }
        });
    }

</script>


