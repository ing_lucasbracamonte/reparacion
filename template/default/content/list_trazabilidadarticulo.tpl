<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo $heading_title; ?>
    </h1>
    <div class="breadcrumb">
        <div class="btn-group-horizontal">

            <?php if (@$exportCSV && $primeravez != 0 && $error == "") { ?>
            <button class="enabled" onclick="location = '<?php echo $exportXLS; ?>'"><i class="fa fa-upload" style="font-size: 30px" alt="Exportar XLS" title="Exportar XLS"></i> </button>
            <button class="enabled" onclick="location = '<?php echo $exportCSV; ?>'"><i class="fa fa-upload" style="font-size: 30px" alt="Exportar CSV" title="ExportarCSV"></i> </button>
            <?php } ?>
            <button class="search" id="search" data-toggle="collapse" data-target="#filter" aria-expanded="false" aria-controls="filter"><i class="fa fa-search" style="font-size: 30px" alt="Buscar" title="Buscar"></i></button>
        </div>
    </div>
</section>
<!-- Main content -->

<section class="content">
    <!-- MENSAJE INICIO -->
    <?php if ($error) { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error; ?>
    </div> 
    <?php } ?>
    <?php if ($message) { ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> ÉXITO!</h4>
        <?php echo $message; ?>
    </div>
    <?php } ?>
    <!-- MENSAJE FIN -->

    <!-- filter -->
    <div class="filter collapse" id="filter" aria-expanded="true" style="height: 0px;">
        <div class="box" style="margin-bottom: 10px;">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form"  style="padding-bottom: 15px;padding-left: 10px;">
                <div class="titulo">
                    Filtro
                    <button class="search" style="text-align: right; right: 5px; position: absolute;" onclick="document.getElementById('form').submit();"><i class="fa fa-search" style="font-size: 18px" alt="Buscar" title="Buscar"></i></button>
                </div>
                <div class="box-body">
                    
                    
                    <div class="form-group">
                        <label>Puntos de Venta</label>
                        <select class="form-control" tabindex="2" name="puntovta" id="puntovta">
                            <option value="-1" selected>Todos</option>
                            <?php foreach ($puntosdeventa as $loc) {       
                            if ($loc['puntovta'] == $puntovta){ ?>
                            <option value="<?php echo $loc['puntovta']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                            <?php } else{ ?>
                            <option value="<?php echo $loc['puntovta']; ?>"><?php echo $loc['descripcion']; ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Producto</label>
                        <input type="input" class="form-control" name="auto_articulo" id="auto_articulo" value="<?php echo $auto_articulo; ?>"  placeholder="Ingrese 3 letras y seleccione una opción de la lista."/>
                        <input type="hidden" name="auto_articulo_stock" id="auto_articulo_stock" value="<?php echo $auto_articulo_stock; ?>" />
                    </div>
                </div>
            </form>
        </div>
    </div>

    <?php if($primeravez != 0 && $error == "") { ?>
    <div class="row"> 
        <div class="col-md-4" style="padding-left: 5%;padding-top: 3%;padding-right: 5%;">
                                <div class="info-box" >
                                    <span class="info-box-icon bg-green" ><i class="ion ion-ios-cart-outline"></i></span>
                                    <div class="info-box-content">
                                        <input class="form-control" type="hidden" name="entrada" id="entrada" value="<?php echo $entrada; ?>" readonly/>
                                        <span class="info-box-text" align="center">Entrada</span>
                                        <span class="info-box-number" align="center" type="text" name="entrada" id="entrada" value="<?php echo $entrada; ?>" style="font-size: 28px"><?php echo $entrada; ?></span>
                                    </div>
                                </div>
                            </div>
        <div class="col-md-4" style="padding-left: 5%;padding-top: 3%;padding-right: 5%;">
                                <div class="info-box" >
                                    <span class="info-box-icon bg-red" ><i class="ion ion-ios-cart-outline"></i></span>
                                    <div class="info-box-content">
                                        <input class="form-control" type="hidden" name="salida" id="total" value="<?php echo $salida; ?>" readonly/>
                                        <span class="info-box-text" align="center">Salida</span>
                                        <span class="info-box-number" align="center" type="text" name="salida" id="salida" value="<?php echo $salida; ?>" style="font-size: 28px"><?php echo $salida; ?></span>
                                    </div>
                                </div>
                            </div>
        <div class="col-md-4" style="padding-left: 5%;padding-top: 3%;padding-right: 5%;">
                                <div class="info-box" >
                                    <span class="info-box-icon bg-blue" ><i class="ion ion-ios-cart-outline"></i></span>
                                    <div class="info-box-content">
                                        <input class="form-control" type="hidden" name="enstock" id="enstock" value="<?php echo $enstock; ?>" readonly/>
                                        <span class="info-box-text" align="center">En stock</span>
                                        <span class="info-box-number" align="center" type="text" name="texttotal" id="enstock" value="<?php echo $enstock; ?>" style="font-size: 28px"><?php echo $enstock; ?></span>
                                    </div>
                                </div>
                            </div>
    </div>
    <?php include 'grilla_sinpaginacion.tpl' ?>
    <?php } else{ ?>
    <?php if($primeravez != 0 && $error == "") { ?>
    <table id="example1" class="table table-bordered table-striped dataTable" aria-describedby="example1_info">
        <tbody>
            <tr class="even">
                <td colspan="<?php echo count($cols); ?>" class="sin_filas" style="text-align: center; "><b><?php echo $mensaje_sin_filas; ?> <?php echo $primeravez; ?></b></td>
            </tr>
        </tbody>
    </table>
    <?php } ?>
    <?php } ?>

</section>

<style>
    .chosen-container
    {
        width: 100% !important;
    }
</style>

<script language="JavaScript">
    $(document).ready(function () {

        $("#auto_articulo").autocomplete({
            source: "<?php echo $script_busca_articulo; ?>",
            minLength: 3,
            select: function (event, ui) {
                $("#auto_articulo_stock").val(ui.item.id);
            }
        });
        
        $('#search').click();
    });


</script>