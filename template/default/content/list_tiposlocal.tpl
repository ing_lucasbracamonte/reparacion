<?php
// Agrego los CSS para condensar luego
if(isset($this->head_def)){
$this->head_def->setcss("/css/table.css");
} ?>

<?php if ($error) { ?>
<div class="warning"><?php echo $error; ?></div>
<?php } ?>
<?php if ($message) { ?>
<div class="message"><?php echo $message; ?></div>
<?php } ?>

<!-- List -->
<div class="block_top">
    <div class="main_sidebar">
        <h3><?php echo $heading_title; ?></h3>
        <!-- List actions -->
        <div class="actions-table">
            <div class="actions">
                <?php if (@$listar) { ?>
                <div  onclick="location = '<?php echo $list; ?>'"><img style="cursor: pointer" src="template/default/image/img/light-blue_icons/iconos-18.png" class="first" alt="<?php echo $button_list; ?>"></div>
                <?php } ?>   
                <?php if (@$insert) { ?>
                <div class="enabled" onclick="ActionAdd()"><img style="cursor: pointer" src="template/default/image/img/iconos-16.png" class="first" alt="<?php echo $button_insert; ?>" title="Agregar"></div>
                <?php } ?>
                <?php if (@$exportPdf) { ?>
                <div class="enabled"><a href="<?php echo $export; ?>" title="<?php echo $titulo_ventana; ?>" rel="gb_page_fs[]" ><img style="cursor: pointer" src="template/default/image/img/iconos-10.png" class="first" alt="<?php echo $button_exportarpdf; ?>"></a></div>
                <?php } ?>
                <?php if (@$exportExcel) { ?>
                <div class="enabled"><a onclick="generarInforme()" href="<?php echo $exportExcel; ?>" title="<?php echo $button_exportar_descargar.$button_exportar_excel; ?>"><img src="template/default/image/img/iconos-08.png" class="png"/></a><?php echo $button_exportar_excel; ?></div>
                <?php } ?>
                <a class="search" data-toggle="collapse" data-target="#filter" aria-expanded="false" aria-controls="filter"><img  src="template/default/image/img/iconos-17.png" title="Buscar"></a>
            </div>
        </div>

        <!-- filter -->
        <div class="filter" class="filter" id="filter" aria-expanded="false">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
                <h4><?php echo $entry_search; ?></h4>
                <input type="text" value="<?php echo $search; ?>" id="search" name="search" placeholder="<?php echo $placeholder_buscar; ?>" style="width:65%">
                <div class="task1"> 
                    <div class="enabled" onclick="document.forms[0].submit();"><img  src="template/default/image/img/iconos-17.png" title="Buscar"></div>
                </div>
            </form>
        </div>
    </div>

    <!-- Grilla -->
    <?php include 'grilla.tpl' ?>

    <div id="popup" title=""></div>
    <div id="dialog-message" title="Ayuda">
        <p>
            <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
            <?php echo $textMessageAyuda ?>
        </p>
    </div>
</div>

<script language="JavaScript">

    function MensajeModal() {
        $("#dialog-message").dialog({
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            }
        });
        $("#dialog-message").dialog('open');
    }

    function confirmar(mensaje) {
        return confirm(mensaje);
    }

    function ActionDelete(direccion) {
        var respuesta = confirm('¿Estás seguro de eliminar el Tipo Local?');

        if (respuesta == true) {
            window.location = direccion;
        }
    }

    function ActionAdd() {

        $.ajax({
            type: 'GET',
            url: 'index.php?controller=tiposlocal&action=insert',
            //data: 	 '&cuestionario=' +  cuestionario,
            async: false,
            success: function (data) {
                //alert(data)
                if (data != 'error') {
                    $('#popup').html(data);
                } else {
                    alert(data);
                }
            }
        });

        estiloBotones();
        $('#popup').dialog('option', 'title', '<?php echo $entry_agregar; ?>');
        $("#popup").dialog('open');
    }

    function ActionUpdate(id) {

        $.ajax({
            type: 'GET',
            url: 'index.php?controller=tiposlocal&action=update',
            data: '&tipolocal=' + id,
            async: false,
            success: function (data) {
                //alert(data)
                if (data != 'error') {
                    $('#popup').html(data);
                } else {
                    alert(data);
                }
            }
        });

        estiloBotones();
        $('#popup').dialog('option', 'title', '<?php echo $entry_modificar; ?>');
        $("#popup").dialog('open');
    }

    function ActionConsult(id) {

        $.ajax({
            type: 'GET',
            url: 'index.php?controller=tiposlocal&action=consulta',
            data: 'tipolocal=' + id,
            async: false,
            success: function (data) {
                //alert(data)
                if (data != 'error') {
                    $('#popup').html(data);
                } else {
                    alert(data);
                }
            }
        });

        estiloBotones();
        $('#popup').dialog('option', 'title', '<?php echo $entry_consultar; ?>');
        $("#popup").dialog('open');

    }

    function ActionSave(action) {

        $.ajax({
            type: 'POST',
            url: 'index.php?controller=tiposlocal&action=' + action,
            data: $('#formABM').serialize(),
            async: false,
            success: function (data) {
                //            alert(data);
                if (data != 'ok') {
                    $('#popup').html(data);
                } else {
                    location.reload(true);
                }
            }
        });

        estiloBotones();
        $("#popup").dialog('open');
    }

    $(document).ready(function () {
        estiloBotones();

        $("#popup").dialog({
            height: 350,
            width: 600,
            autoOpen: false,
            modal: true
        });
    });

    function estiloBotones() {
        $("button").each(function () {
            if ($(this).attr("primary") != undefined) {
                props = {icons:{primary: $(this).attr("primary")}}
            } else if ($(this).attr("secondary") != undefined) {
                props = {icons:{secondary: $(this).attr("secondary")}}
            } else {
                props = {};
            }
            $(this).button(props);
        });
    }

</script>