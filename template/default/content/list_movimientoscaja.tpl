<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo $heading_title; ?>
    </h1>
    <div class="breadcrumb">
        <div class="btn-group-horizontal">
            <?php if (@$listar) { ?>
            <button  onclick="location = '<?php echo $list; ?>'"><img style="cursor: pointer" src="template/default/image/img/light-blue_icons/iconos-18.png" class="first" alt="<?php echo $button_list; ?>"></button>
            <?php } ?>   
            <?php if (@$insert) { ?>
            <button class="enabled" onclick="location = '<?php echo $insert; ?>'"><i class="fa fa-plus" style="font-size: 30px" alt="<?php echo $button_insert; ?>" title="Agregar"></i> </button>
            <?php } ?>
            <?php if (@$exportXls) { ?>
            <?php } ?>
            <?php if (@$exportPdf) { ?>
            <button class="enabled"><a href="<?php echo $exportPdf; ?>" title="<?php echo $titulo_ventana; ?>" rel="gb_page_fs[]" ><img style="cursor: pointer" src="template/default/image/img/iconos-10.png" class="first" alt="<?php echo $button_exportarpdf; ?>"></a></button>
            <?php } ?>
            <?php if (@$exportExcel) { ?>
            <button class="enabled"><a onclick="generarInforme()" href="<?php echo $exportExcel; ?>" title="<?php echo $button_exportar_descargar.$button_exportar_excel; ?>"><img src="template/default/image/img/iconos-08.png" class="png"/></a><?php echo $button_exportar_excel; ?></button>
            <?php } ?>
        </div>
    </div>
</section>
<!-- Main content -->

<section class="content">

    <!-- MENSAJE INICIO -->
    <?php if ($error) { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error; ?>
    </div> 
    <?php  } ?>
    <?php if ($message) { ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> ÉXITO!</h4>
        <?php echo $message; ?>
    </div>
    <?php } ?>
    <!-- MENSAJE FIN -->

    <!-- filter -->
    <div class="box" style="margin-bottom: 10px;">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form"  style="padding-bottom: 15px;padding-left: 10px;">
            <div class="titulo">
                Datos de la Caja
            </div>
            <div class="box-body">
                <input class="form-control" type="hidden" name="caja" value="<?php echo $caja; ?>" readonly/>
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label><?php echo $entry_fechaapertura; ?></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" value="<?php echo $fechaapertura; ?>" id="fechaapertura" name="fechaapertura" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" placeholder="Campo obligatorio" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label><?php echo $entry_fechacierre; ?></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" value="<?php echo $fecha; ?>" id="fecha" name="fecha" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" placeholder="" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label><?php echo $entry_montoapertura; ?></label>
                                <input class="form-control" type="text" name="montoapertura" value="<?php echo $montoapertura; ?>" readonly/>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label><?php echo $entry_saldo; ?></label>
                                <input class="form-control" type="text" name="saldo" value="<?php echo $saldo; ?>" readonly/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>   

    <!-- Grilla -->
    <?php include 'grilla.tpl' ?>

    <!-- BOTONES INICIO  -->
    <div class="box-footer" style="text-align: -webkit-center;">
        <a class="btn btn-primary" href="<?php echo $cancel; ?>">Volver</a>
    </div>
    <!-- BOTONES FIN -->
</section>

<style>
    .chosen-container
    {
        width: 100% !important;
    }
</style>

<script language="JavaScript">

    $(function () {
        $('.chosen-select').chosen();
        $('.chosen-select-deselect').chosen({allow_single_deselect: true});

    });

    function ActionDelete(direccion) {
        var respuesta = confirm('¿Estás seguro de eliminar el Movimiento de Caja?');

        if (respuesta == true) {
            window.location = direccion;
        }
    }

</script>