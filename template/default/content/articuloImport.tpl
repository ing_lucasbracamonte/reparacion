<!-- Content Header (Page header) -->
<section class="content-header">
    <h3><?php echo $heading_title; ?></h3>
    <!-- Error -->
    <?php if ($error_texto_error != '') { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error_texto_error; ?>
    </div>
    <?php } ?> 
    <!-- Error -->
</section>

<!-- Main content -->
<section class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <div class="box box-primary">
            <div class="box-header" style="float:right">
                <a href="<?php echo $exportXlsModelo; ?>"><i class="fa fa-cloud-download" style="font-size: 30px" alt="Modelo de carga" title="Modelo de carga"></i> 
                    Descargar modelo
                </a>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputFile">Cargar archivo</label>
                    <input id="archivoupload" type="file" name="archivoupload" >
                    <p class="help-block">La importacion se ejecutara seg&uacute;n el archivo modelo.</p>
                    <p class="help-block">Primero buscara si tiene id de celsis:</p>
                    <p class="help-block">* si tiene y lo encuentra, valida que el codigo interno no este siendo usado en otro art&iacute;culo, actualiza marca, modelo, tipo y c&oacute;digo interno.</p>
                    <p class="help-block">* si tiene y no lo encuentra lo creasi no tiene busca: si existe la marca, si existe el modelo, si existe el tipo de producto. si no alguno de los 3 no existe informa y no lo guarda.&nbsp; &nbsp; &nbsp;</p>
                    <p class="help-block">* si existe el tridente actualiza cod interno al articulo &nbsp; &nbsp;si no existe el tridente es porque no existe el articulo, lo crea.&nbsp;</p>
                </div>
                
            </div>
        </div>
        <!-- BOTONES INICIO -->
        <div class="box-footer" style="text-align: -webkit-center;">
            <?php if ($_REQUEST['action'] != 'consulta') { ?>  
            <button type="submit" class="btn btn-primary" onclick="document.getElementById('form').submit();">Importar</button>
            <?php } ?>   
            <button type="button" class="btn btn-primary" onclick="location = '<?php echo $cancel; ?>'">Cancelar</button>
        </div>
        <!-- BOTONES FIN -->
    </form>
</section>

<script type="text/javascript">

    $(function () {
        $('.chosen-select').chosen();
        $('.chosen-select-deselect').chosen({allow_single_deselect: true});
    });

    function numeric(e, tipo)
    {
        if (tipo == 'int') {
            return ((e.keyCode == 9) || (e.keyCode == 8) || (e.keyCode > 32 && e.keyCode < 41) || (e.keyCode > 44 && e.keyCode < 58) || (e.keyCode > 94 && e.keyCode < 106));
        } else if (tipo == 'dec') {
            return ((e.keyCode == 9) || (e.keyCode == 8) || (e.keyCode == 110 || e.keyCode == 190) || (e.keyCode > 32 && e.keyCode < 41) || (e.keyCode > 44 && e.keyCode < 58) || (e.keyCode > 94 && e.keyCode < 106));
        }
    }

</script>