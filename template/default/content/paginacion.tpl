<div class="box-footer clearfix">
    <div class="pagination pagination-sm no-margin pull-left"><?php echo $text_results; ?></div>
    <div class="pagination pagination-sm no-margin pull-right">
        <form action="<?php echo $actionPaginacion; ?>" method="post" enctype="multipart/form-data">     
            <?php echo $entry_page; ?>
            <input class="button_pages" name="first" type="button" onclick="this.form.page.value = <?php echo $pages_first; ?> ; this.form.submit()" value="<<"  <?php echo ($pages_first == 0)? 'disabled':''; ?>/>
                   <input class="button_pages" name="previous" type="button" onclick="this.form.page.value = <?php echo $pages_previous; ?> ; this.form.submit()" value="<"  <?php echo ($pages_previous == 0)? 'disabled':''; ?>/>
                   <select name="page" onchange="this.form.submit()">
                <?php foreach ($pages as $pages) { ?>
                <?php if ($pages['value'] == $page) { ?>
                <option value="<?php echo $pages['value']; ?>" selected><?php echo $pages['text']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $pages['value']; ?>"><?php echo $pages['text']; ?></option>
                <?php } ?>
                <?php } ?>
            </select>
            <input class="button_pages" name="next" type="button" onclick="this.form.page.value = <?php echo $pages_next; ?> ; this.form.submit()" value=">"  <?php echo ($pages_next == 0)? 'disabled':''; ?>/>
                   <input class="button_pages" name="last" type="button" onclick="this.form.page.value = <?php echo $pages_last; ?> ; this.form.submit()" value=">>" <?php echo ($pages_last == 0)? 'disabled':''; ?>/>
        </form>
    </div>
</div>