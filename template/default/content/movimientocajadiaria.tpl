<!-- Content Header (Page header) -->
<section class="content-header">
    <h3><?php echo $heading_title; ?></h3>
    <!-- Error -->
    <?php if ($error_texto_error != '') { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error_texto_error; ?>
    </div>
    <?php } ?> 
    <!-- Error -->
</section>

<!-- Main content -->
<section class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Registro</h3>
            </div>
            <div class="box-body">
                <input class="form-control" type="hidden" name="movimiento" value="<?php echo $movimiento; ?>" />
                <div class="form-group">
                    <label><?php echo $entry_fechamovimiento; ?></label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input class="form-control" type="hidden" name="cajadiaria" value="<?php echo $cajadiaria; ?>" />
                        <input type="text" class="form-control" value="<?php echo $fechamovimiento; ?>" id="fechamovimiento" name="fechamovimiento"  readonly >
                    </div>
                </div>
                <div class="form-group">
                    <label><?php echo $entry_tipomovimiento; ?></label>
                    <select class="form-control" tabindex="2" name="tipomovimiento" id="tipomovimiento" <?php if ($_REQUEST['action'] == 'update') echo readonly ?>>
                        <?php if ($tipomovimiento == 'CREDITO'){ ?>
                        <option value="-1" >Seleccione...</option>
                        <option value="DEBITO" >DEBITO</option>
                        <option value="CREDITO" selected>CREDITO</option>
                        <?php } else if($tipomovimiento == 'DEBITO'){ ?>
                        <option value="-1" >Seleccione...</option>
                        <option value="DEBITO" selected>DEBITO</option>
                        <option value="CREDITO" >CREDITO</option>
                        <?php } else{ ?>
                        <option value="-1" selected>Seleccione...</option>
                        <option value="DEBITO" >DEBITO</option>
                        <option value="CREDITO" >CREDITO</option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label><?php echo $entry_importe; ?></label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-usd"></i></span>
                        <input type="text" class="form-control" name="importe" id="importe"  value="<?php echo $importe; ?>" onkeydown="return numeric(event, 'dec');" placeholder="Campo Obligatorio" <?php if ($_REQUEST['action'] == 'update') echo readonly ?>>
                    </div>
                </div>
                <div class="form-group">
                                <label><?php echo $entry_rubro; ?></label>
                                <select class="form-control"  tabindex="2" name="rubro" id="rubro" onchange="getSubRubros();" >
                                    <?php if ($_REQUEST['action'] == 'insert'){ ?> <option value="-1">Seleccione...</option> <?php } ?>
                                        <?php foreach ($rubroscaja as $loc) {       
                                        if ($loc['rubro'] == $rubro){ ?>
                                        <option value="<?php echo $loc['rubro']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                                    <?php } else{ ?>
                                    <option value="<?php echo $loc['rubro']; ?>"><?php echo $loc['descripcion']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                <div class="form-group">
                                <label><?php echo $entry_subrubro; ?></label>
                                <select class="form-control"  tabindex="2" name="subrubro" id="subrubro" <?php if ($_REQUEST['action'] != 'update') echo disabled ?> >
                                   <?php if ($_REQUEST['action'] == 'insert'){ ?> <option value="-1">Seleccione...</option> <?php } ?>
                                        <?php foreach ($subrubroscaja as $loc) {       
                                        if ($loc['subrubro'] == $subrubro){ ?>
                                        <option value="<?php echo $loc['subrubro']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                                    <?php } else{ ?>
                                    <option value="<?php echo $loc['subrubro']; ?>"><?php echo $loc['descripcion']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                <div class="form-group">
                    <label><?php echo $entry_observacion; ?></label>
                    <textarea class="form-control" rows="3" name="observacion" id="observacion" placeholder="Detalle la observación del movimiento." ><?php echo $observacion; ?></textarea>
                </div>
            </div>
        </div>
        <!-- BOTONES INICIO -->
        <div class="box-footer" style="text-align: -webkit-center;">
            <?php if ($_REQUEST['action'] != 'consulta') { ?>  
            <button type="submit" class="btn btn-primary" onclick="document.getElementById('form').submit();">Guardar</button>
            <?php } ?>   
            <button type="button" class="btn btn-primary" onclick="location = '<?php echo $cancel; ?>'">Cancelar</button>
        </div>
        <!-- BOTONES FIN -->
    </form>
</section>

<script type="text/javascript">
    
    $(function () {
        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
        //Money Euro
        $("[data-mask]").inputmask();
    });

    $(function () {
        $('.chosen-select').chosen();
        $('.chosen-select-deselect').chosen({allow_single_deselect: true});
    });

    function numeric(e, tipo)
    {
        if (tipo == 'int') {
            return ((e.keyCode == 9) || (e.keyCode == 8) || (e.keyCode > 32 && e.keyCode < 41) || (e.keyCode > 44 && e.keyCode < 58) || (e.keyCode > 94 && e.keyCode < 106));
        } else if (tipo == 'dec') {
            return ((e.keyCode == 9) || (e.keyCode == 8) || (e.keyCode == 110) || (e.keyCode > 32 && e.keyCode < 41) || (e.keyCode > 44 && e.keyCode < 58) || (e.keyCode > 94 && e.keyCode < 106));
        }
    }
    
    function getSubRubros()
	{		
		var rubro= document.getElementById("rubro").value;
		//alert (rubro);
		
		$.ajax( 
		{	    
			dataType: "html",
			type: "GET",        
			url: "index.php?controller=movimientoscajadiaria&action=GetSubRubros", 
                        data: "rubro=" + rubro,
                        beforeSend: function(data)
				 { 
					$("#modelo").html('<option>Cargando...</option>');
					//Tambi�n puedes poner aqui el gif que indica cargando...
                 },        
                        success: function(requestData)
				 {  	//Llamada exitosa
					//alert(requestData);
                                        if (rubro != '-1') { 
                                        $('#subrubro').removeAttr('disabled'); 
                                        
                                        }  
                                        else{ 
                                            $('#subrubro').attr('disabled', true); 
                                        
                                        }
                                        
					$("#subrubro").html(requestData);  //Usando JQUERY, Cargamos las subcategorias
                 },         
                        error: function(requestData, strError, strTipoError)
				 {   
					alert("Error " + strTipoError +': ' + strError); //En caso de error mostraremos un alert
                 },
                        complete: function(requestData, exito)
				 {  //fin de la llamada ajax.          
					//armaNombre();
					//buscaCAIE();	
                 }                  
                                 
         });
	}
</script>
