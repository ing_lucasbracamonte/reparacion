<div class="box">
    <div class="box-header">
        <h3 class="box-title">Lista de registros</h3>
    </div>
    <div class="box-body">
        <div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">            
            <table id="example1" class="table table-bordered table-striped dataTable" aria-describedby="example1_info">
                <!-- TITULO COLUMNAS INICIO -->             
                <thead>
                    <tr role="row">
                        <?php foreach ($cols as $col) { ?>
                        <th>
                            <?php if (!isset($col['sort'])) { ?>
                            <?php if (isset($col['name'])) { ?>
                            <?php echo $col['name']; ?>
                            <?php } ?>
                            <?php } else { ?>
                            <?php if (isset($col['sort'])) { ?>
                            <form action="<?php echo $action; $actionPaginacion = $action; ?>" method="post" enctype="multipart/form-data" onclick="this.submit();">
                                <?php echo $col['name']; ?>
                                <?php if ($sort == $col['sort']) { ?>
                                <?php if ($order == 'ASC') { ?>
                                &nbsp;<img src="template/Theme/plugins/datatables/images/sort_asc.png" class="png" align="right"/>
                                <?php } else { ?>
                                &nbsp;<img src="template/Theme/plugins/datatables/images/sort_desc.png" class="png" align="right"/>
                                <?php } ?>
                                <?php } else{ ?>
                                &nbsp;<img src="template/Theme/plugins/datatables/images/sort_both.png" class="png" align="right"/>
                                <?php } ?>
                                <input type="hidden" name="sort" value="<?php echo $col['sort']; ?>" />
                            </form>
                            <?php } ?>
                            <?php } ?>
                        </th>
                        <?php } ?>
                    </tr>
                </thead>
                <!-- TITULO COLUMNAS FIN -->                  
                <tbody>
                    <?php if(count($rows) == 0) { ?>
                    <tr class="even">
                        <td colspan="<?php echo count($cols); ?>" class="sin_filas" style="text-align: center;"><b><?php echo $mensaje_sin_filas; ?></b></td>
                    </tr>
                    <?php }else{ ?>
                    <?php $j = 1; ?>
                    <?php foreach ($rows as $row) { ?>
                    <?php if ($j != 1) { $j = 1; } else { $j = 0; } if ($j == 0) { $class = 'odd'; } elseif ($j == 1) { $class = 'even'; } ?>
                    <tr class="odd">
                        <?php $bandera = true; ?>
                        <?php foreach ($row['cell'] as $cell) { ?>
                        <?php if (isset($cell['value'])) { ?>
                        <td align="<?php echo $cell['align']; ?>">
                            <?php echo $cell['value']; ?>
                            <?php if (@$cell['default']) { ?>
                            (<?php echo $text_default; ?>)
                            <?php } ?></td>
                        <?php } elseif (isset($cell['image'])) { ?>
                        <td align="<?php echo $col['align']; ?>"><img src="<?php echo $cell['image']; ?>" /></td>
                        <?php } elseif (isset($cell['path'])) { ?>
                        <td align="<?php echo $col['align']; ?>"><a href="<?php echo $cell['path']; ?>"><img src="template/default/image/<?php echo $cell['icon']; ?>" class="png" /></a></td>
                        <?php } elseif (isset($cell['icon'])) { ?>
                        <td align="<?php echo $col['align']; ?>"><?php if (isset($cell['href'])) { ?>
                            <a href="<?php echo $cell['href']; ?>"><img src="template/default/image/<?php echo $cell['icon']; ?>" class="png" /></a>
                            <?php } else { ?>
                            <img src="template/default/image/<?php echo $cell['icon']; ?>" class="png" />
                            <?php } ?>
                        </td>
                        <?php } elseif (isset($cell['action'])) { ?>
                        <td align="<?php echo $col['align']; ?>">
                            <div class="btn-group-horizontal">
                                <?php foreach ($cell['action'] as $action) {   
                                if(isset($action['prop_a'])){
                                $html_action = '<a ';
                                foreach (@$action['prop_a'] as $key => $value) {
                                $html_action .= $key.'="'.$value.'" '; }
                                $html_action .= '>';
                                if(isset($action['icon'])){
                                $html_action .= '<i  class="'.$action['class'].'" alt="'.$action['text'].'" title="'.$action['text'].'"> </i>';
                                }else{
                                $html_action .= $action['text']; }
                                $html_action .= "</a>\n              "; }
                                elseif(isset($action['icon'])){
                                $html_action = '<i  class="'.$action['class'].'" alt="'.$action['text'].'" title="'.$action['text'].'"> </i>';
                                }else{ $html_action = $action['text']; } echo $html_action; } ?>
                            </div>
                        </td>
                        <?php } ?>
                        <?php } ?>
                    </tr>
                    <?php } } ?>
                </tbody>
            </table>
            
        </div>
    </div>
</div>