<!-- Content Header (Page header) -->
<section class="content-header">
    <h3><?php echo $heading_title; ?></h3>
</section>

<!-- Main content -->
<section class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <div class="box box-primary"> <!-- REPARACION -->
            <div class="box-body">
                <input type="hidden" name="reparacion" id="reparacion" value="<?php echo $reparacion; ?>" readonly/>
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group" style='text-align: center;'>
                                <div class="box-header" >
                                    <h3 class="box-title" ><b><?php echo $titulo; ?></b></h3>
                                </div>
                                <?php echo $texto; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- FIN REPARACION-->
    </form>
    <!-- BOTONES INICIO  -->
    <div class="box-footer" style="text-align: -webkit-center;">
        <a class="btn btn-primary" href="<?php echo $base; ?>">Insight</a>
    </div>
    <!-- BOTONES FIN -->
</section>


