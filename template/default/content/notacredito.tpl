<!-- Content Header (Page header) -->
<section class="content-header">
    <?php if ($validaciones_caja != '') { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $validaciones_caja; ?>
        <?php } else{ ?>

        <h3><?php echo $heading_title; ?></h3>
        <!-- Error -->
        <?php if ($error_texto_error != '') { ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Error!</h4>
            <?php echo $error_texto_error; ?>
        </div>
        <?php } ?> 
        <!-- Error -->
        <?php } ?> 
</section>

<!-- Main content -->
<section class="content">


    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <!--<input class="form-control" type="hidden" id="venta" name="venta" value="<?php echo $venta; ?>" readonly/>
        -->
        <div class="box box-primary" <?php if ($validaciones_caja != '') { ?> style="display:none" <?php } ?> >

             <div class="box-header">

            </div>

            <!-- DETALLE DE VENTA INICIO -->
            <div class="detallenc" id="detallenc" >
                <div class="box-body" id="box-body">
                    <div class="box">
                        <div class="row">  
                            <div class="col-md-6">
                                <table id="encavezadonc" name="encavezadonc" class="table table-condensed" aria-describedby="example1_info">

                                    <thead>
                                        <tr role="row" >
                                            <th  role="columnheader" tabindex="0" aria-controls="example2" rowspan="1" colspan="2">
                                            </th>
                                            <th  role="columnheader" tabindex="1" aria-controls="example2" rowspan="1" colspan="2">
                                            </th>
                                            <th  role="columnheader" tabindex="2" aria-controls="example2" rowspan="1" colspan="2">
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody aria-live="polite" aria-relevant="all">

                                    <input class="form-control" type="hidden" name="notacredito" value="<?php echo $notacredito; ?>" readonly/>

                                    <tr class="even"  >
                                        <td class="col-xs-4">
                                            <label><?php echo $entry_fecha; ?></label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control" value="<?php echo $fecha; ?>" id="fecha" name="fecha" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" placeholder="Campo obligatorio">
                                            </div>
                                        </td>
                                        <td class="col-xs-4">
                                            <label><?php echo $entry_vendedor; ?></label>
                                            <select class="form-control" tabindex="2" name="vendedor" id="vendedor">  
                                                <option value="-1" selected>Seleccione...</option>
                                                <?php foreach ($vendedores as $loc) {       
                                                if ($loc['persona'] == $vendedor){ ?>
                                                <option value="<?php echo $loc['persona']; ?>" selected><?php echo $loc['apellido']; ?>, <?php echo $loc['nombre']; ?></option>
                                                <?php } else{ ?>
                                                <option value="<?php echo $loc['persona']; ?>"><?php echo $loc['apellido']; ?>, <?php echo $loc['nombre']; ?></option>
                                                <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </td>
                                        <td class="col-xs-4">
                                            <label><?php echo $entry_puntovta; ?></label>
                                            <input class="form-control" type="hidden" id="puntovta" name="puntovta" value="<?php echo $puntovta; ?>" readonly/>
                                            <input class="form-control" type="hidden" id="puntovtaid" name="puntovtaid" value="<?php echo $puntovta; ?>" readonly/>
                                            <input class="form-control" type="text" name="descpuntovta" value="<?php echo $descpuntovta; ?>" readonly/>
                                        </td>
                                    </tr>
                                    <tr class="even"  >			
                                        <td class="col-xs-4">
                                            <label><?php echo $entry_venta; ?></label>
                                            <input class="form-control" type="text" id="venta" name="venta" value="<?php echo $venta; ?>" readonly/>
                                        </td>

                                        <td class="col-xs-4">
                                            <label><?php echo $entry_personacomprador; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                            <input type="input" class="form-control" name="auto_personacomprador" id="auto_personacomprador" value="<?php echo $auto_personacomprador; ?>" placeholder="Ingrese 3 letras y seleccione una opción de la lista" readonly/>
                                            <input type="hidden" name="auto_personacomprador_venta" id="auto_personacomprador_venta" value="<?php echo $auto_personacomprador_venta; ?>" />

                                        </td>
                                        <td class="col-xs-4">
                                            <label><?php echo $entry_estado; ?></label>
                                            <input class="form-control" type="text" name="estado" value="<?php echo $estado; ?>" readonly/>
                                        </td>
                                    </tr>
                                    <tr class="even"  >			
                                        <td class="col-xs-12" colspan="3">
                                            <label><?php echo $entry_observacion; ?></label>
                                            <textarea class="form-control" type="text" id="observacion" name="observacion" /><?php echo $observacion; ?></textarea>
                                        </td>

                                    </tr>
                                    </tbody>
                                </table>
                            </div>   
                            <div class="col-md-5" style="padding-left: 5%;padding-top: 3%;padding-right: 5%;">
                                <div class="info-box" >
                                    <span class="info-box-icon bg-green" ><i class="ion ion-ios-cart-outline"></i></span>
                                    <div class="info-box-content">
                                        <input class="form-control" type="hidden" name="total" id="total" value="<?php echo $total; ?>" readonly/>
                                        <span class="info-box-text" align="center">Total</span>
                                        <span class="info-box-number" align="center" type="text" name="texttotal" id="texttotal" value="<?php echo $total; ?>" style="font-size: 28px"><?php echo $texttotal; ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 

                    <div class="box">
                        <?php include 'notacredito_grilla_articulos.tpl' ?>  
                    </div>
                </div>               

                <!-- BOTONES INICIO  -->
                <div class="box-footer" style="text-align: -webkit-center;">
                    <?php if ($_REQUEST['action'] != 'consulta') { ?>
                    <button type="submit" class="btn btn-primary" onclick="saveInsert();
                          return false;" >Finalizar</button> 
                    <?php } ?>
                    <button type="button" class="btn btn-primary" onclick="location = '<?php echo $cancel; ?>'">Cancelar</button>
                </div>
                <!-- BOTONES FIN -->
            </div>
            <!-- DETALLE DE VENTA FIN -->           

        </div>

    </form>

</section>

<script type="text/javascript">

    $(document).ready(function () {

        $("#auto_personacomprador").autocomplete({
            source: "<?php echo $script_busca_personacomprador; ?>",
            minLength: 3,
            select: function (event, ui) {
                $("#auto_personacomprador_venta").val(ui.item.id);
            }
        });

        $("#auto_articulo").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "<?php echo $script_busca_articulo; ?>",
                    data: {
                        term: $("#auto_articulo").val(),
                        venta: $("#venta").val()
                    },
                    dataType: "json",
                    type: "GET",
                    success: function (data) {

                        response(data);
                    }
                });
            },
            minLength: 3,
            select: function (event, ui) {
                $("#auto_articulo_nc").val(ui.item.id);

                CargaLineaCarga();
            }
        });

        LimpiaValoresArticulo();
    });

    function CargaLineaCarga() {

        var articulo_nc = $("#auto_articulo_nc").val();
        var puntovta = $("#puntovta").val();
        var venta = $("#venta").val();

        var articulo = articulo_nc.split('_');
        var articuloid = articulo[0];
        var articulocodbarra = articulo[1];

        if (articulo_nc != '-1') {
            $.ajax({
                type: 'GET',
                url: 'index.php?controller=notasdecredito&action=getArticulo',
                data: 'term=' + articulo_nc + '&articuloid=' + articuloid + '&codbarra=' + articulocodbarra + '&puntovta=' + puntovta + '&venta=' + venta,
                async: false,
                success: function (data) {
                    
                    if (data != 'error') {

                        //obtengo json y lo convierto en un ojetejo
                        var obj = $.parseJSON(data);
                        //alert(data);
                        $("#articulo").val(obj["articulo"]);

                        $("#descmodelo").val(obj["descripcion"]);
                        $("#concoddebarra").val(obj["concoddebarra"]);
                        $("#codbarrainterno").val(obj["codbarrainterno"]);
                        $("#codbarra").val(obj["codbarra"]);
                        //alert(obj["cantidad"]);
                        $("#cantidad").val(obj["cantidad"]);
                        $("#cantidad_maxima").val(obj["cantidad"]);

                        $("#precionc").val(obj["precionc"]);

                        if (obj["concoddebarra"] == '1') {

                            //$("#codbarrainterno").val('');
                        } else {

                            $("#codbarra").val('');
                        }


                    } else {
                        alert('');
                    }
                }
            });
        }
    }

    function saveInsert() {

        var ptovta = $("#puntovtaid").val();
        var vendedor = $("#vendedor").val();
        var venta = $("#venta").val();
        var notacredito = $("#notacredito").val();

        if (vendedor == '-1') {
            alert('Debe seleccionar un vendedor.');
        } else if (ptovta == '') {
            alert('El vendedor no tiene asignado un punto de venta por defecto.');
        } else {
            //hacer: validaciones si un producto con codigo de barras no fue vendido ya.
            //si un producto no tiene stock.
            //si no hay cantidad suficiente del producto producto en ese almacen.
            //tengo que recorrer los productos y verificar que tengan stock en el punto de venta

            $.ajax({
                type: 'POST',
                url: 'index.php?controller=notasdecredito&action=insert',
                data: $('#form').serialize(),
                beforeSend: function () {
                    $("body").addClass("loading");
                },
                success: function (data) {
                    //alert(data);
                    if (data == 'error') {
                        $('#form').html(data);
                    } else {
                        window.location.href = data;
                    }
                }
            });
        }
    }

    function AgregarArticulo() {

        var articulo = $("#articulo").val();
        var venta = $("#venta").val();
        var notacredito = $("#notacredito").val();
        var cantidad = $("#cantidad").val();
        var cantidad_maxima = parseInt($("#cantidad_maxima").val());
        var codbarra = $("#codbarra").val();
        var precionc = $("#precionc").val();
        var codbarrainterno = $("#codbarrainterno").val();
        var cantReg = $('#articulos tr').size();
        var ptovta = $("#puntovta").val();
        var total = $("#total").val();

        if (articulo == '-1') {
            alert('Antes de agregar un artículo debe seleccionar uno.');
        } else if ($("#concoddebarra").val() == '1' && codbarra == '') {
            alert('El artículo seleccionado necesita que se le cargue un código de barra.');
        } else if (cantidad == 0) {
            alert('Debe ingresar una cantidad.');
        } else if (cantidad < 0) {
            alert('La cantidad debe ser mayor a 0.');
        } else if (cantidad > cantidad_maxima) {
            alert('La cantidad NO debe ser mayor a ' + cantidad_maxima);
            $("#cantidad").val(cantidad_maxima);
        } else if (precionc == 0) {
            alert('Debe ingresar un precio de venta.');
        } else if (ptovta == '') {
            alert('El vendedor no tiene asignado un punto de venta por defecto.');
        } else {

            //antes de ir a buscarlo y agregar la linea
            //, validar que si es de codigo de barras ya no se encuentre asignado en la grilla

            var productoYaCargado = 'no';
            $('input[id^="articulosnc[][codbarra]"]').each(function () {
                if (codbarra != '') {
                    var texto = $(this).val();
                    //alert(texto);
                    var n = texto.includes(codbarra);
                    if (n) {
                        productoYaCargado = 'si';
                    }
                }
            });

            if (productoYaCargado == 'si') {
                alert('El producto ya se encuentra cargado');
                return;
            }

            //var descuento = (precioventa * porcDescuento) / 100;
            $.ajax({
                type: 'GET',
                url: 'index.php?controller=notasdecredito&action=AgregarArticulo',
                data: 'notacredito=' + notacredito + '&venta=' + venta + '&articulo=' + articulo + '&cantidad=' + cantidad + '&codbarra=' + codbarra + '&codbarrainterno=' + codbarrainterno + '&precionc=' + precionc + '&cantReg=' + cantReg,
                async: false,
                success: function (data) {
                    if (data != 'error') {

                        $('#articulos').append(data);
                        
                        var tot = cantidad * precionc;

                        var totalnc = Number(tot) + Number(total);
                        $("#total").val(totalnc);

                        LimpiaValoresArticulo();
                        CalculaTotales();

                        $('#cantidad').attr('disabled', false);
                    } else {
                        alert('Surgió un error.');
                    }

                }
            });

        }
    }

    function removeArticulo(row, total_fila) {

        $('#' + row).remove();

        LimpiaValoresArticulo();
        CalculaTotales();
    }

    function CalculaTotales() {

        var totalnc = 0;
        var table = document.getElementById("articulos");
        //debugger;
        if (table.rows.length > 0) {
            for (var i = 0; i < table.rows.length; i++) {

                var valohtml = table.rows[i].cells[5].innerHTML; //importe
                var valorimporte = valohtml.split(">");

                if (Number.isNaN(parseFloat(valorimporte[1])) == false) {
                    //alert(valorimporte[1]);
                    totalnc += parseFloat(valorimporte[1]); //importe
                    
                }
            }
        }

        if (totalnc == 0) {
            document.getElementById('texttotal').innerHTML = '$ 0.00';
            $("#total").val('0.00');
            return;
        }

        document.getElementById('texttotal').innerHTML = '$ ' + parseFloat(totalnc).toFixed(2);
        $("#total").val(totalnc);
    }

    function LimpiaValoresArticulo() {

        $("#articulo").val('');
        $("#auto_articulo").val('');
        $("#auto_articulo_venta ").val('');
        $("#codbarra").val('');
        $("#codbarrainterno").val('');
        $("#cantidad").val('1');
        $("#cantidad_maxima").val('');
        $("#precionc").val('');
    }

</script>



