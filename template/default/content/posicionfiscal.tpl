<!-- Content Header (Page header) -->
<section class="content-header">
    <h3><?php echo $heading_title; ?></h3>
    <!-- Error -->
    <?php if ($error_texto_error != '') { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error_texto_error; ?>
    </div>
    <?php } ?> 
    <!-- Error -->
</section>

<!-- Main content -->
<section class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Registro</h3>
            </div>
            <div class="box-body">
                <input type="hidden" value="<?php echo $posicionfiscal; ?>" name="posicionfiscal" id="posicionfiscal">
                
                <div class="form-group">
                    <label><?php echo $entry_puntovta; ?></label>
                    <select class="form-control" tabindex="2" name="puntovta" id="puntovta">
                        <option value="-1" selected>Seleccione...</option>
                        <?php foreach ($puntosdeventa as $loc) {       
                        if ($loc['puntovta'] == $puntovta){ ?>
                        <option value="<?php echo $loc['puntovta']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                        <?php } else{ ?>
                        <option value="<?php echo $loc['puntovta']; ?>"><?php echo $loc['descripcion']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label><?php echo $entry_tipocomprobante; ?></label>
                    <select class="form-control" tabindex="2" name="tipocomprobante" id="tipocomprobante">
                        <option value="-1" selected>Seleccione...</option>
                        <?php foreach ($tiposcomprobante as $loc) {       
                        if ($loc['tipocomprobante'] == $tipocomprobante){ ?>
                        <option value="<?php echo $loc['tipocomprobante']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                        <?php } else{ ?>
                        <option value="<?php echo $loc['tipocomprobante']; ?>"><?php echo $loc['descripcion']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>

                
                <div class="form-group">
                    <label><?php echo $entry_prefijo; ?></label>
                    <input class="form-control" type="text" id="prefijo" name="prefijo" value="<?php echo $prefijo; ?>" onkeydown="return numeric(event, 'int');" placeholder="Campo Obligatorio"/>
                </div>
                
                <div class="form-group" >
                        <label><?php echo $entry_wsafip; ?></label>
                        <div class="form-inline" >
                      <div class="radio">
                        <label>
                          <input type="radio" name="wsafip" id="reaparicionno" value="0" <?php echo ($wsafip == 0) ?  'checked' : '' ?> >
                          No.   
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="wsafip" id="reaparicionsi" value="1" <?php echo ($wsafip == 1) ?  'checked' : '' ?>>
                           Si.
                        </label>
                      </div>
                         </div>
                    </div>
            </div>
        </div>
        <!-- BOTONES INICIO -->
        <div class="box-footer" style="text-align: -webkit-center;">
            <?php if ($_REQUEST['action'] != 'consulta') { ?>  
            <button type="submit" class="btn btn-primary" onclick="document.getElementById('form').submit();">Guardar</button>
            <?php } ?>   
            <button type="button" class="btn btn-primary" onclick="location = '<?php echo $cancel; ?>'">Cancelar</button>
        </div>
        <!-- BOTONES FIN -->
    </form>
</section>

<script type="text/javascript">
 function numeric(e, tipo)
    {
        if (tipo == 'int') {
            return ((e.keyCode == 9) || (e.keyCode == 8) || (e.keyCode > 32 && e.keyCode < 41) || (e.keyCode > 44 && e.keyCode < 58) || (e.keyCode > 94 && e.keyCode < 106));
        } else if (tipo == 'dec') {
            return ((e.keyCode == 9) || (e.keyCode == 8) || (e.keyCode == 110 || e.keyCode == 190) || (e.keyCode > 32 && e.keyCode < 41) || (e.keyCode > 44 && e.keyCode < 58) || (e.keyCode > 94 && e.keyCode < 106));
        }
    }
</script>