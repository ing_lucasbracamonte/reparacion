<script language="JavaScript">
<!--   
    function confirmarMail() {
        var titulo = "Confirme el mail:";
        $.ajax
                ({
                    type: "POST",
                    url: "index.php?controller=confirmarmail&action=update",
                    data: "mail=" + $("#confirmar_mail").val() + "&mail_repetido=" + $('#confirmar_mail_repetido').val() + "&persona=" + $('#confirmar_persona').val() + "&destino=" + $('#confirmar_destino').val(),
                    beforeSend: function () {
                        msgBox('Enviando...', true, '350', '110', '<div align="center"><table width="100%" height="100" border="0"><tr><td style="font-size:14px; text-align: left;">Cargando datos, por favor espere...</td><td><img src="template/default/image/loading.gif"/></td></tr></table></div>');
                    },
                    error: function (requestData, strError, strTipoError)
                    {
                        msgBox('Ocurrio un error!', true, '350', '110', '<div align="center"><table width="100%" height="100" border="0"><tr><td style="font-size:14px; text-align: left;">Error ' + strTipoError + ': ' + strError + '.</td></tr><tr><td align="right"><input style=" background-color:#CCC;" type="button" id="aceptar" value="&nbsp;Aceptar&nbsp;" onclick="tb_remove()" /></td></tr></table></div>');
                    },
                    success: function (data)
                    {
                        if (data == 'true') {
                            location = '<?php echo urldecode($destino); ?>';
                        } else {
                            $("#TB_window").remove();
                            msgBox(titulo, true, '470', '270', data);
                        }
                    }
                });
    }
    //-->
</script>

<div class="heading"><?php echo $heading_title; ?></div>
<div class="description"><?php echo $heading_description; ?></div>
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form"><br />
    <table width="100%">
        <tr>
            <td><span class="required">*</span> <?php echo $entry_mail; ?></td>
            <td><input type="text" name="mail" value="<?php echo $mail; ?>" id="confirmar_mail"/>
                <?php if ($error_mail) { ?>
                <span class="error"><?php echo $error_mail; ?></span>
                <?php }else{ ?>
                <br /><br />
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td><span class="required">*</span> <?php echo $entry_mail_repetido; ?></td>
            <td><input type="text" name="mail_repetido" value="<?php echo $mail_repetido; ?>" id="confirmar_mail_repetido"/>
                <?php if ($error_mail_repetido) { ?>
                <span class="error"><?php echo $error_mail_repetido; ?></span>
                <?php }else{ ?>
                <br /><br />
                <?php } ?>    
                <input type="hidden" name="persona" value="<?php echo $persona; ?>" id="confirmar_persona"/>
                <input type="hidden" name="destino" value="<?php echo $destino; ?>" id="confirmar_destino"/>
            </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
            <td class="task" colspan="2">
                <div class="enabled" onclick="confirmarMail();"><img src="template/default/image/save_enabled.png" alt="<?php echo $button_save; ?>" class="png" /><?php echo $button_save; ?></div>
                <div class="enabled" onclick="tb_remove();"><img src="template/default/image/cancel_enabled.png" alt="<?php echo $button_cancel; ?>" class="png" /><?php echo $button_cancel; ?></div>
            </td>
        </tr>
    </table>
</form>