<table id="articulo_encavezado" name="articulo_encavezado" class="table table-bordered table-striped dataTable" aria-describedby="example1_info">
     <thead>
        <tr role="row">
            <th  class="col-xs-1"  role="columnheader" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">
                Artículo
            </th>
            <th  class="col-xs-3"  role="columnheader" tabindex="1" aria-controls="example2" rowspan="1" colspan="1">
                Descripción
            </th>
            <th  class="col-xs-2"  role="columnheader" tabindex="2" aria-controls="example2" rowspan="1" colspan="1">
                Cod. Barras
            </th>
            <th  class="col-xs-2"  role="columnheader" tabindex="3" aria-controls="example2" rowspan="1" colspan="1">
                Cod. Barras Interno
            </th>
            <th  class="col-xs-1"  role="columnheader" tabindex="4" aria-controls="example2" rowspan="1" colspan="1">
                Cantidad
            </th>
            <th  class="col-xs-1"  role="columnheader" tabindex="5" aria-controls="example2" rowspan="1" colspan="1">
                $ Venta
            </th>
             <?php if ($_REQUEST['action'] == 'insert') { ?> 
            <th  class="col-xs-1"  role="columnheader" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" style="text-align: center">
                Acción
            </th>
             <?php } ?> 
        </tr>
    </thead>
    <?php if ($_REQUEST['action'] == 'insert') { ?> 
    <tbody aria-live="polite" aria-relevant="all">
            <tr class="odd" >
                <td class="col-xs-1">
                    <input class="form-control" type="text" name="articulo" id="articulo" value="<?php echo $articulo; ?>"  disabled/>
                </td>
                <td class="col-xs-3">
                    <input type="input" class="form-control" name="auto_articulo" id="auto_articulo" value="<?php echo $auto_articulo; ?>"  placeholder="Ingrese 3 letras y seleccione una opción de la lista."/>
                    <input type="hidden" name="auto_articulo_nc" id="auto_articulo_nc" value="<?php echo $auto_articulo_nc; ?>" />
                </td>
                <td class="col-xs-2">
                    <input type="hidden" name="concoddebarra" id="concoddebarra" value="<?php echo $concoddebarra; ?>" /> 
                    <input class="form-control" type="text" name="codbarra" id="codbarra" value="<?php echo $codbarra; ?>" disabled/>
                </td>
                <td class="col-xs-2">
                    <input class="form-control" type="text" name="codbarrainterno" id="codbarrainterno" value="<?php echo $codbarrainterno; ?>" disabled/>
                </td>
                <td class="col-xs-1">
                    <input class="form-control" type="text" name="cantidad" id="cantidad" value="<?php echo $cantidad; ?>"  onkeydown="return numeric(event,'int');"/>
                    <input type="hidden" name="cantidad_maxima" id="cantidad_maxima" value="" />   
                </td>
                <td class="col-xs-1">
                    <input class="form-control" type="text" name="precionc" id="precionc" value="<?php echo $precionc; ?>"  disabled/>
                </td>
                             
                <td class="col-xs-1" align="center" style="padding: 12px 12px;">
                    <?php if ($_REQUEST['action'] != 'update') { ?>
                        <input type="button" class="botones" value="+" onclick="AgregarArticulo()" alt="<?php echo $button_insert; ?>" />
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>
     </table>
<table id="articulos" name="articulos" class="table table-bordered table-striped dataTable" aria-describedby="example1_info">
    <?php $i = 1; ?>
    <?php foreach ($articulosasignados as $ad) { ?>
       
        <tr id="ncarticulo_<?php echo $i; ?>" class="even" style="height: 25px;padding: 6px 12px;font-size: 14px;">
            <input type="hidden" name="articulosnc[][numero]" value="<?php echo $i; ?>"/>
            <input type="hidden" name="articulosnc[][venta]" value="<?php echo $ad['venta']; ?>"/>
            <input type="hidden" name="articulosnc[][notacredito]" value="<?php echo $ad['notacredito']; ?>"/>
            
            <td class="col-xs-1"><input type="hidden" name="articulosnc[][articulo]" value="<?php echo $ad['articulo']; ?>"/><?php echo $ad['articulo']; ?></td>
            <td class="col-xs-3"><input type="hidden" name="articulosnc[][descmodelo]" value="<?php echo $ad['descmodelo']; ?>"/><?php echo $ad['descmodelo']; ?></td>
            
            <td class="col-xs-2"><input type="hidden" name="articulosnc[][codbarra]" value="<?php echo $ad['codbarra']; ?>"/><?php echo $ad['codbarra']; ?></td>
            <td class="col-xs-2"><input type="hidden" name="articulosnc[][codbarrainterno]" value="<?php echo $ad['codbarrainterno']; ?>"/><?php echo $ad['codbarrainterno']; ?></td>
            <td class="col-xs-1"><input type="hidden" name="articulosnc[][cantidad]" value="<?php echo $ad['cantidad']; ?>"/><?php echo $ad['cantidad']; ?></td>
            <td class="col-xs-1"><input type="hidden" name="articulosnc[][precionc]" value="<?php echo $ad['precionc']; ?>"/><?php echo $ad['precionc']; ?></td>
            
        </tr>
    <?php $i++; ?>
    <?php } ?>
    
 </table>