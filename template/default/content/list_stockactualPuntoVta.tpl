<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo $heading_title; ?>
    </h1>
    <div class="breadcrumb">
        <div class="btn-group-horizontal">
            <?php if (@$exportCSV) { ?>
            <button class="enabled" onclick="location = '<?php echo $exportXLS; ?>'"><i class="fa fa-upload" style="font-size: 30px" alt="Exportar XLS" title="Exportar XLS"></i> </button>
            <button class="enabled" onclick="location = '<?php echo $exportCSV; ?>'"><i class="fa fa-upload" style="font-size: 30px" alt="Exportar CSV" title="ExportarCSV"></i> </button>
            <?php } ?>
            <button class="search" data-toggle="collapse" data-target="#filter" aria-expanded="false" aria-controls="filter"><i class="fa fa-search" style="font-size: 30px" alt="Buscar" title="Buscar"></i></button>
        </div>
    </div>
</section>
<!-- Main content -->

<section class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form"  style="padding-bottom: 15px;padding-left: 10px;">
               
    <!-- MENSAJE INICIO -->
    <?php if ($error) { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error; ?>
    </div> 
    <?php } ?>
    <?php if ($message) { ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> ÉXITO!</h4>
        <?php echo $message; ?>
    </div>
    <?php } ?>
    <!-- MENSAJE FIN -->

    <!-- filter -->
    <div class="filter" id="filter" aria-expanded="true" style="height: 0px;">
        <div class="box" style="margin-bottom: 10px;">
                <input type="hidden" id="primeravez" name="primeravez" value="<?php echo $primeravez; ?>" />
                <input type="hidden" id="puntovtaid" name="puntovtaid" value="<?php echo $puntovtaid; ?>" />
                <input type="hidden" id="agrupadopor" name="agrupadopor" value="<?php echo $agrupadopor; ?>" />
                <input type="hidden" id="vuelvea" name="vuelvea" value="<?php echo $vuelvea; ?>" />
                <input type="hidden" id="puntovta" name="puntovta" value="<?php echo $puntovta; ?>" />
                <input type="hidden" id="tipoproducto" name="tipoproducto" value="<?php echo $tipoproducto; ?>" />
                <input type="hidden" id="auto_articulo_stock" name="auto_articulo_stock" value="<?php echo $auto_articulo_stock; ?>" />
            
                <div class="titulo">
                    Filtro
                    <button class="search" style="text-align: right; right: 5px; position: absolute;" onclick="document.getElementById('form').submit();"><i class="fa fa-search" style="font-size: 18px" alt="Buscar" title="Buscar"></i></button>
                </div>
                
                <div class="box-body">
                    
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label><?php echo $entry_search; ?></label>
                                <input class="form-control" type="text" id="search" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $placeholder_buscar; ?>"/>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                
            </div>
                
                
                
            
        </div>
            <?php include 'grilla_sinpaginacion.tpl' ?>
        
        
    </div>

    
    
</form>
    
    <!-- BOTONES INICIO -->
        <div class="box-footer" style="text-align: -webkit-center;">
            
            <button type="button" class="btn btn-primary" onclick="location = '<?php echo $volver; ?>'">Volver</button>
        </div>
        <!-- BOTONES FIN -->
</section>

<style>
    .chosen-container
    {
        width: 100% !important;
    }
</style>

<script language="JavaScript">
 $(document).ready(function () {
    
        $("#auto_articulo").autocomplete({
            source: "<?php echo $script_busca_articulo; ?>",
            minLength: 3,
            select: function (event, ui) {
                $("#auto_articulo_stock").val(ui.item.id);
            }
        });
        
    });
    
    $(function () {
        $('.chosen-select').chosen();
        $('.chosen-select-deselect').chosen({allow_single_deselect: true});

    });

</script>