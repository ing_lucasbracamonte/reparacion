<!-- Content Header (Page header) -->
<section class="content-header">
    <h3><?php echo $heading_title; ?></h3>
    <!-- Error -->
    <?php if ($error_texto_error != '') { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error_texto_error; ?>
    </div>
    <?php } ?> 
    <!-- Error -->
</section>

<!-- Main content -->
<section class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Nuevo registro</h3>
            </div>
            <div class="box-body">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">Vendedor</a></li>
                        <li><a href="#tab_2" data-toggle="tab">Punto de Vta</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="form-group">
                                <label><?php echo $entry_persona; ?></label>
                                <?php if ($_REQUEST['action'] == 'update') { ?> 
                                <input class="form-control" type="text" name="persona" value="<?php echo $persona; ?>" readonly/>
                                <?php } else { ?>
                                <input class="form-control" type="text" name="persona" maxlength="8" value="<?php echo $persona; ?>" placeholder="Campo obligatorio"/>
                                <?php } ?> 
                            </div>
                            <div class="form-group">
                                <label><?php echo $entry_apellido; ?></label>
                                <input class="form-control" type="text" name="apellido" value="<?php echo $apellido; ?>"  placeholder="Campo obligatorio"/>
                                <input type="hidden" name="accion_form" value="<?php echo $accion_form; ?>" />
                            </div>
                            <div class="form-group">
                                <label><?php echo $entry_nombre; ?></label>
                                <input class="form-control" type="text" name="nombre" value="<?php echo $nombre; ?>"  placeholder="Campo obligatorio"/>
                            </div>
                            <div class="form-group">
                                <label><?php echo $entry_direccion; ?></label>
                                <div class="row">
                                    <div class="col-xs-9"><input class="form-control" type="text" name="direccion" value="<?php echo $direccion; ?>" placeholder="Calle"/></div>
                                    <div class="col-xs-1"><input class="form-control" type="text" name="numero" value="<?php echo $numero; ?>" placeholder="Nro"/></div>
                                    <div class="col-xs-1"><input class="form-control" type="text" name="piso" value="<?php echo $piso; ?>"  placeholder="Piso"/></div>
                                    <div class="col-xs-1"><input class="form-control" type="text" name="departamento" value="<?php echo $departamento; ?>"  placeholder="Dpto"/></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label><?php echo $entry_localidad; ?></label>
                                <input type="input" class="form-control" name="auto_localidad" id="auto_localidad" value="<?php echo $auto_localidad; ?>" placeholder="Ingrese 3 letras y seleccione una opción de la lista"/>
                                <input type="hidden" name="auto_localidad_vendedor" id="auto_localidad_vendedor" value="<?php echo $auto_localidad_vendedor; ?>" />                    
                            </div> 
                            <div class="form-group">
                                <label><?php echo $entry_fecha; ?></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" value="<?php echo $fechanacimiento; ?>" id="fechanacimiento" name="fechanacimiento" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" placeholder="Campo obligatorio">
                                </div>
                            </div>
                            <div class="form-group">
                                <label><?php echo $entry_celular; ?></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <input type="text" class="form-control" name="celular" value="<?php echo $celular; ?>" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label><?php echo $entry_mail; ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <input type="text" class="form-control"  name="mail" id="mail" value="<?php echo $mail; ?>" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label><?php echo $entry_tipoiva; ?></label>
                                <select class="chosen-select" tabindex="2" name="tipoiva" id="tipoiva">
                                    <option value="-1" selected>Seleccione...</option>
                                    <?php foreach ($tiposiva as $loc) {       
                                    if ($loc['tipoiva'] == $tipoiva){ ?>
                                    <option value="<?php echo $loc['tipoiva']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                                    <?php } else{ ?>
                                    <option value="<?php echo $loc['tipoiva']; ?>"><?php echo $loc['descripcion']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label><?php echo $entry_tipovendedor; ?></label>
                                <select class="chosen-select" tabindex="2" name="tipovendedor" id="tipovendedor">
                                    <option value="-1" selected>Seleccione...</option>
                                    <?php foreach ($tiposvendedor as $loc) {       
                                    if ($loc['tipovendedor'] == $tipovendedor){ ?>
                                    <option value="<?php echo $loc['tipovendedor']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                                    <?php } else{ ?>
                                    <option value="<?php echo $loc['tipovendedor']; ?>"><?php echo $loc['descripcion']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label><?php echo $entry_dependencia; ?></label>
                                <select class="chosen-select" tabindex="2" name="dependenciapersona" id="dependenciapersona">
                                    <option value="-1" selected>Seleccione...</option>
                                    <?php foreach ($vendedores as $loc) {       
                                    if ($loc['persona'] == $dependenciapersona){ ?>
                                    <option value="<?php echo $loc['persona']; ?>" selected><?php echo $loc['apellidonombre']; ?></option>
                                    <?php } else{ ?>
                                    <option value="<?php echo $loc['persona']; ?>"><?php echo $loc['apellidonombre']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <div  class="form-group">
                                <label>
                                    <input type="checkbox" name="comisiona" value="comisiona" style="width: 20px;margin-top: -3px;" <?php echo ($comisiona == 1) ?  'checked' : '' ?>>
                                           <?php echo $entry_comision; ?>
                                </label>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <div class="form-group">
                                <?php if ($error_puntovta) { ?>
                                <span class="error"><?php echo $error_puntovta; ?></span>
                                <?php } ?>
                                <?php foreach ($puntosdevta as $ptovta) { ?>
                                <div class="checkbox">
                                    <label>				
                                        <?php if (!$ptovta['checked']) { ?>
                                        <input type="checkbox" name="puntosdevta[]" value="<?php echo $ptovta['puntovta']; ?>"><?php echo $ptovta['descripcion']; ?><br>	
                                        <?php } else { ?>
                                        <input type="checkbox" name="puntosdevta[]" value="<?php echo $ptovta['puntovta']; ?>" checked><?php echo $ptovta['descripcion']; ?><br>	
                                        <?php } ?>
                                    </label>    
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="box-footer" style="text-align: -webkit-center;">
            <?php if ($_REQUEST['action'] != 'consulta') { ?>  
            <button type="submit" class="btn btn-primary" onclick="document.getElementById('form').submit();">Guardar</button>
            <?php } ?>   
            <button type="button" class="btn btn-primary" onclick="location = '<?php echo $cancel; ?>'">Cancelar</button>
        </div>
    </form>
</section>

<script type="text/javascript">

    $(document).ready(function () {

        $("#auto_localidad").autocomplete({
            source: "<?php echo $script_busca_localidades; ?>",
            minLength: 3,
            select: function (event, ui) {
                $("#auto_localidad_vendedor").val(ui.item.id)
            }
        });
    });

</script>