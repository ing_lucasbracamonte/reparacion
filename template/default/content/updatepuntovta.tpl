<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Sistema de Gestión | Seleccionar Punto Venta</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.2 -->
        <link href="template/Theme/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="template/Theme/dist/css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- iCheck -->
        <link href="template/Theme/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />
    </head>
    <body class="login-page">
        <div class="login-box">
            <div class="login-logo">
                <a><b>Sistema de Gestión</b></a>
            </div>
            <div class="login-box-body">
                
    <!-- Error -->
    <?php if ($error_texto_error != '') { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error_texto_error; ?>
    </div>
    <?php } ?> 
    <!-- Error -->

                <p class="login-box-msg">Selección de punto de venta.</p>
                <form action="<?php echo $action; ?>" method="post" id="login" enctype="multipart/form-data" name="login">
                    <div class="form-group has-feedback">
                        <label><?php echo $entry_puntovta; ?></label>
                            <select class="form-control" tabindex="2" name="puntovtaasignado" id="puntovtaasignado" >
                                <option value="-1" selected>Seleccione...</option>
                                <?php foreach ($puntosdeventa as $loc) {       
                                if ($loc['puntovta'] == $puntovtaasignado){ ?>
                                <option value="<?php echo $loc['puntovta']; ?>" selected><?php echo $loc['descpuntoventa']; ?></option>
                                <?php } else{ ?>
                                <option value="<?php echo $loc['puntovta']; ?>"><?php echo $loc['descpuntoventa']; ?></option>
                                <?php } ?>
                                <?php } ?>
                            </select>
                    </div>
                    
                    <div class="row">
                        <div class="col-xs-4">
                            
                        </div>
                        <div class="col-xs-4">
                           <button type="submit" class="btn btn-primary" onclick="document.getElementById('login').submit();">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- iCheck -->
        <script src="template/Theme/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
    </body>
</html>




