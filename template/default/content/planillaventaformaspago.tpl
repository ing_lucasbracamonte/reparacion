<!-- Content Header (Page header) -->
<section class="content-header">
    <h3><?php echo $heading_title; ?></h3>
    <!-- Error -->
    <?php if ($error_texto_error != '') { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $error_texto_error; ?>
    </div>
    <?php } ?> 
    <!-- Error -->
</section>

<!-- Main content -->
<section class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Filtros</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label><?php echo $entry_desde; ?></label>
                                <div class="input-group date MiDateTimePicker" data-date="" data-date-format="dd/mm/yyyy" data-link-field="desde" data-link-format="dd/mm/yyyy">
                                    <input class="form-control" size="16" type="text" value="" readonly>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                                <input type="hidden" id="desde" name="desde" value="<?php echo $desde; ?>" /><br/>
                            </div>
                        
                            
                            <div class="form-group">
                                <label><?php echo $entry_hasta; ?></label>
                                <div class="input-group date MiDateTimePicker" data-date="" data-date-format="dd/mm/yyyy" data-link-field="hasta" data-link-format="dd/mm/yyyy">
                                    <input class="form-control" size="16" type="text" value="" readonly>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                                <input type="hidden" id="hasta" name="hasta" value="<?php echo $hasta; ?>" /><br/>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label>Puntos de Venta</label>
                                <div class="form-control checkbox form-checklist"  >
                                    <label><input type="checkbox" id="puntosdeventa" onclick="marcarPuntosdeventa(this);" /><b>Marcar/Desmarcar Todos</b></label><br>
                                    <?php foreach ($puntosdeventa as $tipo) { ?>
                                    <?php if ($tipo['puntovta'] != '0') { ?>
                                    <label for="punto_<?php echo $tipo['puntovta']; ?>">
                                        <input type="checkbox" name="puntosvta[]" value="<?php echo $tipo['puntovta']; ?>" id="punto_<?php echo $tipo['puntovta']; ?>">
                                        <?php echo $tipo['descripcion']; ?></label><br> 
                                    <?php } else { ?>
                                    <label for="punto_<?php echo $tipo['puntovta']; ?>">
                                        <input type="checkbox" name="puntosvta[]" value="<?php echo $tipo['puntovta']; ?>" id="punto_<?php echo $tipo['puntovta']; ?>" checked="checked">
                                        <?php echo $tipo['descripcion']; ?></label><br>
                                    <?php } ?>
                                    <?php } ?>
                                </div> 
                            </div>
                        </div>
                        </div>
                        </div>
            </div>
            
            
        </div>
        <!-- BOTONES INICIO -->
        <div class="box-footer" style="text-align: -webkit-center;">
            <input type="hidden" value="<?php echo $tiporeporte; ?>"  id="tiporeporte" name="tiporeporte" />
            <?php if ($_REQUEST['action'] != 'consulta') { ?>  
            <button type="submit" class="btn btn-primary"  onclick="cambiaTipoReporte('CSV');">Exportar CSV</button>
            
            <button type="submit" class="btn btn-primary" onclick="cambiaTipoReporte('XLS');">Exportar XLS</button>
            <?php } ?>   
            <button type="button" class="btn btn-primary" onclick="location = '<?php echo $cancel; ?>'">Cancelar</button>
        </div>
        <!-- BOTONES FIN -->
    </form>
</section>

<script type="text/javascript">

   function marcarPuntosdeventa(source)
    {
        checkboxes = document.form.getElementsByTagName('input'); //obtenemos todos los controles del tipo Input
        for (i = 0; i < checkboxes.length; i++) //recoremos todos los controles
        {
            if (checkboxes[i].type == "checkbox" && checkboxes[i].name == 'puntosvta[]') //solo si es un checkbox entramos
            {
                checkboxes[i].checked = source.checked; //si es un checkbox le damos el valor del checkbox que lo llamó (Marcar/Desmarcar Todos)
            }
        }
    }
    
    function marcarVendedores(source)
    {
        checkboxes = document.form.getElementsByTagName('input'); //obtenemos todos los controles del tipo Input
        for (i = 0; i < checkboxes.length; i++) //recoremos todos los controles
        {
            if (checkboxes[i].type == "checkbox" && checkboxes[i].name == 'vendedores[]') //solo si es un checkbox entramos
            {
                checkboxes[i].checked = source.checked; //si es un checkbox le damos el valor del checkbox que lo llamó (Marcar/Desmarcar Todos)
            }
        }
    }
    
    function marcarTiposProducto(source)
    {
        checkboxes = document.form.getElementsByTagName('input'); //obtenemos todos los controles del tipo Input
        for (i = 0; i < checkboxes.length; i++) //recoremos todos los controles
        {
            if (checkboxes[i].type == "checkbox" && checkboxes[i].name == 'tipos[]') //solo si es un checkbox entramos
            {
                checkboxes[i].checked = source.checked; //si es un checkbox le damos el valor del checkbox que lo llamó (Marcar/Desmarcar Todos)
            }
        }
    }

    function marcarMarcas(source)
    {
        checkboxes = document.form.getElementsByTagName('input'); //obtenemos todos los controles del tipo Input
        for (i = 0; i < checkboxes.length; i++) //recoremos todos los controles
        {
            if (checkboxes[i].type == "checkbox" && checkboxes[i].name == 'marcas[]') //solo si es un checkbox entramos
            {
                checkboxes[i].checked = source.checked; //si es un checkbox le damos el valor del checkbox que lo llamó (Marcar/Desmarcar Todos)
            }
        }
    }

function marcarModelos(source)
    {
        checkboxes = document.form.getElementsByTagName('input'); //obtenemos todos los controles del tipo Input
        for (i = 0; i < checkboxes.length; i++) //recoremos todos los controles
        {
            if (checkboxes[i].type == "checkbox" && checkboxes[i].name == 'modelos[]') //solo si es un checkbox entramos
            {
                checkboxes[i].checked = source.checked; //si es un checkbox le damos el valor del checkbox que lo llamó (Marcar/Desmarcar Todos)
            }
        }
    }
    
    function exportExcel() {

        $.ajax({
            type: 'POST',
            url: 'index.php?controller=listaprecios&action=exportExcel',
            data: $('#form').serialize(),
            async: false,
            success: function (data) {

                if (data != 'ok') {

                } else {
                    //location.reload(true);
                }
            }
        });
    }
    
    function cambiaTipoReporte(tipo) {
        document.getElementById("tiporeporte").value = tipo; 
}

</script>