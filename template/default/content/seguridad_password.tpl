
<?php // Agrego los CSS para condensar luego
if(isset($this->head_def)) { $this->head_def->setcss("/css/tab.css"); } ?>

<?php if ($error) { ?>
<div class="warning"><?php echo $error; ?></div>
<?php } ?>
<?php if ($message) { ?>
<div class="message"><?php echo $message; ?></div>
<?php } ?>

<div class="block_top">
    <div class="main_sidebar">
        <h3><?php echo $heading_title; ?></h3>
        <!-- List actions -->
        <div class="actions-table">
            <div class="actions">
                <?php if ($_REQUEST['action'] != 'consulta') { ?>  
                <div class="enabled" onclick="document.getElementById('form').submit();"><img style="cursor: pointer" src="template/default/image/img/iconos-25.png" alt="<?php echo $button_save; ?>" class="second"></div>
                <?php } ?>   
                <div class="enabled" onclick="location = '<?php echo $cancel; ?>'"><img style="cursor: pointer" src="template/default/image/img/iconos-13.png" alt="<?php echo $button_cancel; ?>" class="second"></div>  
            </div>  
        </div>
    </div>
</div>

<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
    <div class="tab" id="tab">
        <div class="pages">
            <div class="page">
                <div class="pad">
                    <table class="personas">
                        <tr>
                            <td><?php echo $entry_newpassword; ?></td>
                            <td><input type="password" name="password" /></td>
                        </tr>
                        <tr>
                            <td><?php echo $entry_newpasswordagain; ?></td>
                            <td><input type="password" name="newpassword" /></td>
                        </tr>
                    </table>
                </div>        
            </div>
        </div>
    </div>  
</form>

