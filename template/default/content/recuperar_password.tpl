<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formABM" name="form">
    <div class="generated-table">
        <table class="gradient-style">    
            <tr>
                <td width="220">
                    <span class="required">*</span> <?php echo $entry_persona; ?></td>
                <td>
                    <?php if ($_REQUEST['action'] == 'update') { ?> 
                    <input type="text" name="persona" value="<?php echo $persona; ?>" readonly/>
                    <?php } else { ?>
                    <input type="text" name="persona" maxlength="8" value="<?php echo $persona; ?>" onkeydown="return numeric(event)"/>
                    <?php } ?> 
                    <?php if ($error_persona) { ?>
                    <span class="error"><?php echo $error_persona; ?></span>
                    <?php } ?>
                </td>
            </tr>                          
            <tr>
                <td><span class="required">*</span> <?php echo $entry_mail; ?></td>
                <td>
                    <input type="text" name="mail" value="<?php echo $mail; ?>" size="50"/>
                    <?php if ($error_mail) { ?>
                    <span class="error"><?php echo $error_mail; ?></span>
                    <?php } ?>              
                </td>
            </tr>
            <tr>                        
                <td colspan="2">
                    <br><br>
                    <button id="btGuardar" onclick="guardarRecuperar();
                        return false;" secondary="ui-icon ui-icon-triangle-1-e">Enviar</button>
                    <button id="btCerrar" onclick="$('#popup').dialog('close');
                        return false;" secondary="ui-icon-close">Cancelar</button>
                </td>
            </tr>
        </table>
    </div> 
</form>

<script language="JavaScript">

    function numeric(e)
    {
        return ((e.keyCode == 46) || (e.keyCode == 9) || (e.keyCode == 8) || (e.keyCode > 32 && e.keyCode < 41) || (e.keyCode > 47 && e.keyCode < 58) || (e.keyCode > 94 && e.keyCode < 106));
    }
    
</script>