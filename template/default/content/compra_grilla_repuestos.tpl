<table id="repuestos" name="repuestos" class="table table-bordered table-striped dataTable" aria-describedby="example1_info">
    <thead>
        <tr role="row" id="comprarepuesto_0">
            <th  role="columnheader" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">
                Repuesto
            </th>
            <th  role="columnheader" tabindex="2" aria-controls="example2" rowspan="1" colspan="1">
                Descripción
            </th>
            <th  role="columnheader" tabindex="2" aria-controls="example2" rowspan="1" colspan="1">
                Nro. Parte
            </th>
            <th  role="columnheader" tabindex="4" aria-controls="example2" rowspan="1" colspan="1">
                Cantidad
            </th>
            <th  role="columnheader" tabindex="5" aria-controls="example2" rowspan="1" colspan="1">
                $ Compra
            </th>
            <th  role="columnheader" tabindex="3" aria-controls="example2" rowspan="1" colspan="1">
                Subtotal
            </th>
            <th  role="columnheader" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" style="text-align: center">
                Acción
            </th>
        </tr>
    </thead>
    <?php if ($_REQUEST['action'] == 'insert') { ?> 
    <tbody aria-live="polite" aria-relevant="all">
        <tr class="odd" id="comprarepuesto_1" >
            <td class="col-xs-1">
                <input class="form-control" type="text" name="repuesto" id="repuesto" value="<?php echo $repuesto; ?>"  disabled/>
            </td>
            <td class="col-xs-3">
                <select class="chosen-select" name="repuesto_compra" id="repuesto_compra" onchange="CargaLineaCarga();">
                    <option value="-1" selected>Seleccione...</option>
                    <?php foreach ($repuestoss as $loc) {       
                    if ($loc['repuesto'] == $repuesto_compra){ ?>
                    <option value="<?php echo $loc['repuesto']; ?>" selected><?php echo $loc['descripcion']; ?></option>
                    <?php } else{ ?>
                    <option value="<?php echo $loc['repuesto']; ?>"><?php echo $loc['descripcion']; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select> 
            </td>
            <td class="col-xs-2">
                <input class="form-control" type="text" name="nroparte" id="nroparte" value="<?php echo $nroparte; ?>"  disabled/>
            </td>
            <td class="col-xs-1">
                <input class="form-control" type="text" name="cantidad" id="cantidad" value="<?php echo $cantidad; ?>"  onkeydown="return numeric(event, 'int');" onkeyup="ActualizaSubtotal()"/>
            </td>
            <td class="col-xs-1">
                <input class="form-control" type="text" name="preciocompra" id="preciocompra" value="<?php echo $preciocompra; ?>"  onkeydown="return numeric(event, 'dec');" onkeyup="ActualizaSubtotal()"/>
            </td>
            <td class="col-xs-1">
                <input class="form-control" type="text" name="subtotal" id="subtotal" value="<?php echo $subtotal; ?>"  disabled/>
            </td>
            <td class="col-xs-1" align="center">
                <button class="enabled" <?php if ($_REQUEST['action'] == 'update') { ?> style="display: none;" <?php } ?>  onclick="AgregarRepuesto();return false;"><i class="fa fa-plus" style="font-size: 25px" alt="<?php echo $button_insert; ?>" title="Agregar"></i> </button>
            </td>
        </tr>
        <?php } ?>
        <?php $i = 1; ?>
        <?php foreach ($repuestosasignados as $ad) { ?>

        <tr id="comprarepuesto_<?php echo $i; ?>" class="even" style="height: 25px;padding: 6px 12px;font-size: 14px;">
    <input type="hidden" name="repuestoscompra[<?php echo $i; ?>][numero]" value="<?php echo $i; ?>"/>
    <input type="hidden" name="repuestoscompra[<?php echo $i; ?>][compra]" value="<?php echo $ad['compra']; ?>"/>

    <td class="col-xs-1"><input type="hidden" name="repuestoscompra[<?php echo $i; ?>][repuesto]" value="<?php echo $ad['repuesto']; ?>"/><?php echo $ad['repuesto']; ?></td>
    <td class="col-xs-3"><input type="hidden" name="repuestoscompra[<?php echo $i; ?>][descripcion]" value="<?php echo $ad['descripcion']; ?>"/><?php echo $ad['descripcion']; ?></td>

    <td class="col-xs-2"><input type="hidden" name="repuestoscompra[<?php echo $i; ?>][nroparte]" value="<?php echo $ad['nroparte']; ?>"/><?php echo $ad['nroparte']; ?></td>
    <td class="col-xs-1"><input type="hidden" name="repuestoscompra[<?php echo $i; ?>][cantidad]" value="<?php echo $ad['cantidad']; ?>"/><?php echo $ad['cantidad']; ?></td>
    <td class="col-xs-1"><input type="hidden" name="repuestoscompra[<?php echo $i; ?>][preciocompra]" value="<?php echo $ad['preciocompra']; ?>"/><?php echo $ad['preciocompra']; ?></td>
    <td class="col-xs-1"><input type="hidden" name="repuestoscompra[<?php echo $i; ?>][subtotal]" value="<?php echo $ad['subtotal']; ?>"/><?php echo $ad['subtotal']; ?></td>
    <td class="col-xs-1" align="center">
        <a href="/" onclick="removeRepuesto('comprarepuesto_<?php echo $i; ?>');
                return false;"><i class="fa fa-fw fa-trash-o" alt="Borrar" title="Borrar"> </i></a>
    </td>
</tr>
<?php $i++; ?>
<?php } ?>

</tbody>
</table>
<div class="box-body">
    <div class="form-group">
        <input class="form-control" type="hidden" name="total" id="total" value="<?php echo $total; ?>" readonly/>
        <input type="text" class="form-control" name="texttotal" id="texttotal" style='text-align: center;font-weight: bold;' value="<?php echo $texttotal; ?>" >
    </div>
</div>                   

<script>
    function CargaLineaCarga() {

        var repuesto_compra = $("#repuesto_compra").val();

        //alert($("#repuesto_compra").val());
        //var minutocambio  = $("#minutocambio").val();
        //var entrenamiento = $('#entrenamiento').val();
        //alert('personaentra=' + personaentra + '&personasale='+ personasale + '&minuto='+ minutocambio + '&partido_id='+ partido_id +'&formulario=personas');

        if (repuesto_compra != '-1') {
            //alert('personaentra=' + personaentra + '&personasale='+ personasale + '&minutocambio='+ minutocambio + '&partido_id='+ partido_id +'&formulario=personas');
            $.ajax({
                type: 'GET',
                url: 'index.php?controller=compras&action=getRepuesto',
                data: 'term=' + repuesto_compra,
                async: false,
                success: function (data) {
                    if (data != 'error') {
                        //obtengo json y lo convierto en un ojetejo
                        var obj = $.parseJSON(data);
                        $("#repuesto").val(obj["repuesto"]);
                        $("#descripcion").val(obj["descripcion"]);
                        $("#nroparte").val(obj["nroparte"]);
                        // $( "#subtotal").val(obj["precioultcompra"]);
                        $("#preciocompra").val(obj["precioultcompra"]);
                        $("#subtotal").val(obj["precioultcompra"]);
                        // if (obj["concoddebarra"]=='1') {
                        //$('#codbarra').attr('disabled', false);
                        //$('#acomprana').attr('disabled', false);
                        //$( "#nroparte").val('');
                        //}
                        //else{
                        //$('#codbarra').attr('disabled', true);
                        //$('#acomprana').attr('disabled', true);
                        //$( "#codbarra").val('');
                        //  }
                        $("#repuesto_compra").val(obj["repuesto"]);

                    } else {
                        alert('');
                    }
                }
            });
        }
    }

    function ActualizaSubtotal() {

        var cantidad = $("#cantidad").val();
        var preciocompra = $("#preciocompra").val();

        $("#subtotal").val(preciocompra * cantidad);

    }

</script>