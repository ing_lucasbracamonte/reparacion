<!-- sidebar menu: : style can be found in sidebar.less -->
<div class="sidebar-menu">
    <ul>
        <li class="header">MENÚ DE NAVEGACIÓN</li>       
        <?php echo $menu ?>
    </ul>
</div>

<div id="popup" title=""></div>

<script language="JavaScript">

    $(document).ready(function () { // Script del Navegador
        $("ul.subnavegador").not('.selected').hide();
        $("a.desplegable").click(function (e) {
            var desplegable = $(this).parent().find("ul.subnavegador");
            $('.desplegable').parent().find("ul.subnavegador").not(desplegable).slideUp('slow');
            desplegable.slideToggle('slow');
            e.preventDefault();
        })
    });

</script>