<?php
// Heading
$_['heading_title']          = 'Localidades';
$_['heading_description']    = 'Ud. puede editar sus localidades aqu&iacute;.';

// Text
$_['text_message']           = 'Los datos han sido actualizados!';

// Column
$_['column_codpostal']       = 'C.P.';
$_['column_nombre']       	 = 'Nombre';
$_['column_provincia']       = 'Provincia';
$_['column_action']       	 = 'Acci&oacute;n';

?>

