<?php
// Heading
$_['heading_title']       = 'Modulo Encabezado';
$_['heading_description'] = 'Ud. puede editar los detalles del modulo encabezado aqu&iacute;.';

// Text
$_['text_message']        = 'Exito: Ud. ha actualizado el modulo encabezado!';

// Entry
$_['entry_status']        = 'Estado:';

// Error
$_['error_permission']    = 'Advertencia: Ud. no tiene permisos para modificar el modulo encabezado';
?>