<?php
// Heading
$_['heading_title']       = 'Usuario';
$_['heading_description'] = 'Alta de usuario.';

// Text
$_['text_message']        = 'Exito: Ud. ha actualizado sus usuarios!';

// Entry
$_['entry_tipodoc']       = 'Tipo de documento:';
$_['entry_numerodoc']     = 'Nro. de documento:';
$_['entry_email']	  = 'Email:';
$_['entry_password']      = 'Contrase&ntilde;a:';
$_['entry_confirm']       = 'Confirmar:';

// Error
$_['error_tipodoc']      = '* Debe seleccionar el tipo de documento';
$_['error_numerodoc']    = '* Debe ingresar su n&uacute;mero de documento';
$_['error_email']   	 = '* Debe ingresar su cuenta de correo electr&oacute;nico';
$_['error_password']      = '* Contrase&ntilde;a debe ser mayor a 3 caracteres y menor que 20!';
$_['error_confirm']       = '* La Contrase&ntilde;a y su confirmaci&oacute;n no coinciden!';
?>
