<?php
// Heading
$_['heading_title']       = 'Ingreso';

// Entry
$_['entry_tipodoc']       = 'Tipo documento:';
$_['entry_nrodoc']        = 'N&uacute;mero de documento:';
$_['entry_password']      = 'Contrase&ntilde;a';

// Button
$_['button_login']        = 'Ingresar';

// Error
$_['error_login']         = 'No se ha encontrado el usuario / la contrase&ntilde;a es incorrecta.';
?>
