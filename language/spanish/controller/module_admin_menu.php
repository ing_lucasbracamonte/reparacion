<?php
// Heading
$_['heading_title']       = 'Modulo Menu';
$_['heading_description'] = 'Ud. puede editar los detalles del modulo menu aqu&iacute;.';

// Text
$_['text_message']        = 'Exito: Ud. ha actualizado el modulo menu!';

// Entry
$_['entry_status']        = 'Estado:';

// Error
$_['error_permission']    = 'Advertencia: Ud. no tiene permisos para modificar el modulo menu';
?>