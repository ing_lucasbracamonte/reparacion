<?php
// Heading
$_['heading_title']       = 'Cambiar contrase&ntilde;a';

// Entry
$_['entry_newpassword']   = 'Ingrese la nueva contrase&ntilde;a:';
$_['entry_newpasswordagain'] = 'Reingrese la nueva contrase&ntilde;a:';

// Button
$_['button_aceptar']      = 'Cambiar';
$_['button_cancelar']     = 'Cancelar';

// Error
$_['error_password']      = 'Las contrase&ntilde;as ingresadas no coinciden';
?>
