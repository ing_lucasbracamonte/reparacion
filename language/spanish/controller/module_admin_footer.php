<?php
// Heading
$_['heading_title']       = 'Modulo Pie';
$_['heading_description'] = 'Ud. puede editar los detalles del modulo pie aqu&iacute;.';

// Text
$_['text_message']        = 'Exito: Ud. ha actualizado el modulo pie!';

// Entry
$_['entry_status']        = 'Estado:';

// Error
$_['error_permission']    = 'Advertencia: Ud. no tiene permisos para modificar el modulo pie';
?>