<?php
// Heading
$_['heading_title']       = 'Menu';
$_['heading_description'] = 'Menu de Administraci&oacute;n';

// Text
$_['text_sistema']        = 'Sistema';
$_['text_admin']          = 'Administraci&oacute;n';
$_['text_menu_procesos']  = 'Procesos';
$_['text_home']           = 'Inicio';
$_['text_configuration']  = 'Configuraci&oacute;n';
$_['text_setting']        = 'Par&aacute;metros';
$_['text_users']          = 'Usuarios';
$_['text_user']           = 'Usuario';
$_['text_user_group']     = 'Grupo de Usuario';
$_['text_paises']         = 'Pa&iacute;ses';
$_['text_provincia']      = 'Provincias';
$_['text_localidad']      = 'Localidades';
$_['text_valores'] 		  = 'Valores';
$_['text_estudios']       = 'Estudios Jur&iacute;dicos';
$_['text_juzgados']       = 'Juzgados';
$_['text_aseguradoras']   = 'Aseguradoras';
$_['text_productores']    = 'Productores';
$_['text_actores']        = 'Actores';
$_['text_tipos_proceso']  = 'Tipos de Proceso';
$_['text_documentacion']  = 'Documentaci&oacute;n';
$_['text_etapasproceso']  = 'Etapas de Procesos';
$_['text_tipoescrito']    = 'Tipos de Escritos';
$_['text_modelotipoescrito'] = 'Modelos de Escritos';
$_['text_proceso']        = 'Procesos';
$_['text_url_alias']      = 'URL Alias';
$_['text_backup']         = 'Backup / Restaurar';
$_['text_server_info']    = 'Server Info';
$_['text_logout']         = 'Cerrar Sesi&oacute;n';
?>
