<?php
// Locale
$_['code']              = 'en';
$_['charset']           = 'utf-8';
$_['direction']         = 'ltr';
$_['locale']            = 'es_ES.UTF-8,es_ES,Español,es_AR,es';
$_['date_format_short'] = 'j F Y';
$_['date_format_long']  = 'l dS F Y';
$_['time_format']       = 'h:i:s A';
$_['decimal_point']     = '.';
$_['thousand_point']    = ',';

// Text
$_['text_time']         = 'P&aacute;gina creada en %s segundos';
$_['text_yes']          = 'Si';
$_['text_no']           = 'No';
$_['text_plus']         = '+';
$_['text_minus']        = '-';
$_['text_enabled']      = 'Habilitado';
$_['text_disabled']     = 'Deshabilitado';
$_['text_none']         = ' --- Nada --- ';
$_['text_all_zones']    = 'Todas las Zonas';
$_['text_default']      = 'Defecto';
$_['text_previous']     = 'Previo';
$_['text_results']      = 'Resultado %s - %s de %s';
$_['text_pages']        = '%s de %s';
$_['text_january']      = 'Enero';
$_['text_february']     = 'Febrero';
$_['text_march']        = 'Marzo';
$_['text_april']        = 'Abril';
$_['text_may']          = 'Mayo';
$_['text_june']         = 'Junio';
$_['text_july']         = 'Julio';
$_['text_august']       = 'Agosto';
$_['text_september']    = 'Septiembre';
$_['text_october']      = 'Octubre';
$_['text_november']     = 'Noviembre';
$_['text_december']     = 'Diciembre';
$_['text_confirm']      = '�Est&aacute; seguro?';
$_['text_dtp_no_aplica'] = 'No aplica';
$_['text_dtp_entregada'] = 'Entregada';

// Entry
$_['entry_page']        = 'P&aacute;gina';
$_['entry_search']      = 'Buscar ';
$_['entry_description']      = 'Descripci&oacute;n: ';
$_['entry_user']      = 'Usuario: ';
$_['entry_password']      = 'Contrase&ntilde;a ';

// Button
$_['button_list']       = 'Lista';
$_['button_insert']     = 'Insertar';
$_['button_upload']     = 'Subir';
$_['button_download']   = 'Descargar';
$_['button_update']     = 'Actualizar';
$_['button_consult']     = 'Consultar';
$_['button_delete']     = 'Borrar';
$_['button_file']       = 'Archivos';
$_['button_class']       = 'Clases';
$_['button_site']       = 'Ir al sitio...';
$_['button_answer']       = 'Responder';
$_['button_answertoname']       = 'Responder a nombre de';
$_['button_uploadnotes']       = 'Cargar notas';
$_['button_chargepayments']       = 'Cargar pagos';
$_['button_class']       = 'Clases';
$_['button_condition']       = 'Condiciones';
$_['button_listinscript']       = 'Listado de inscriptos';
$_['button_updatecuestion']       = 'Actualizar preguntas';
$_['button_resetpassword']     = 'Reiniciar contrase&ntilde;a';
$_['button_save']       = 'Guardar';
$_['button_cancel']     = 'Cancelar';
$_['button_install']    = 'Instalar';
$_['button_uninstall']  = 'Desinstalar';
$_['button_configure']  = 'Configurar';
$_['button_send']       = 'Enviar E-Mail';
$_['button_search']		= 'Ir';
$_['button_select']		= 'Seleccionar';
$_['button_exportar']	= 'Listados';
$_['button_actaasistencia']	= 'Emitir acta para toma de asistencia';
$_['button_listasistencia']	= 'Listado de asistencia';
$_['button_register']	= 'Inscribirse';
$_['button_capacitor']	= 'Usted es capacitador de esta actividad';
$_['button_detail']	= 'Ver detalle';
$_['button_csv']       = 'Exportar CSV';
$_['button_constancy']       = 'Constancia';
$_['button_additem']       = 'Agregar item';
$_['button_addquestion']       = 'Agregar pregunta';
$_['button_updateitem']       = 'Modificar item';
$_['button_updatequestion']       = 'Modificar pregunta';
$_['button_deleteitem']       = 'Borrar item';
$_['button_deletequestion']       = 'Borrar pregunta';


// Tab
$_['tab_general']       = 'General';
$_['tab_texto']         = 'Texto';
$_['tab_data']          = 'Datos';
$_['tab_materias'] 		= 'Materias';
?>
