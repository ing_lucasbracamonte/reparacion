<?php

class ControllerReporteplanillaventacajadiaria extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'REPORTE');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $request = & $this->locator->get('request');
        $cache = & $this->locator->get('cache');
        // </editor-fold>
        //<editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('reporteplanillaventaformaspago.page'));

        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        $view->set('heading_title', 'PLANILLA DE VENTA SEGÚN FORMAS DE PAGO');
        $view->set('placeholder_buscar', 'BUSCA POR ID O MARCA O MODELO O TIPO');

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        //$view->set('consultaVacia', $url->ssl('consulta','consulta_vacia'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        //$view->set('button_consultavacia', "Vacía");
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('reporteplanillaventaformaspago.search'));
        $view->set('sort', $session->get('reporteplanillaventaformaspago.sort'));
        $view->set('order', $session->get('reporteplanillaventaformaspago.order'));
        $view->set('page', $session->get('reporteplanillaventaformaspago.page'));

        //$view->set('listaprecios.tipoproducto', '-1');
        $view->set('entry_desde', 'Fecha desde:');
        if ($request->has('desde', 'post')) {
            $view->set('desde', $request->get('desde', 'post'));
        } else {
            $view->set('desde', date('d/m/Y'));
        }
        
        $view->set('entry_hasta', 'Fecha hasta:');
        if ($request->has('hasta', 'post')) {
            $view->set('hasta', $request->get('hasta', 'post'));
        } else {
            $view->set('hasta', date('d/m/Y'));
        }
        
//        $view->set('puntosdeventa', $database->getRows("SELECT * FROM puntosdeventa ORDER BY descripcion ASC"));
//        
//        $view->set('vendedores', $database->getRows("SELECT * FROM vw_grilla_vendedores ORDER BY apellidonombre ASC"));
//        
//        $view->set('tiposproducto', $database->getRows("SELECT * FROM tiposproducto ORDER BY descripcion ASC"));
//
//        $view->set('marcas', $database->getRows("SELECT * FROM marcas ORDER BY descripcion ASC"));
//
//        $view->set('modelos', $database->getRows("SELECT * FROM modelos ORDER BY descripcion ASC"));

        $view->set('titulo_ventana', '::. Exportar');

        $view->set('entry_modificar', "Modificar");
        $view->set('entry_agregar', "Agregar");

        $view->set('modelo', '');
        $view->set('search', '');
        $view->set('reporteplanillaventaformaspago.search', '');
        $cache->delete('reporteplanillaventacajadiaria');

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('reporteplanillaventacajadiaria', 'exportxlsdataCSV'));
        //$view->set('list', $url->ssl('reporteplanillaventa'));
        $view->set('cancel', $url->ssl('reporteplanillaventacajadiaria'));

//        if ($user->hasPermisos($user->getPERSONA(), 'reporteplanillaventa', 'C')) {
//            $view->set('exportarXLS', $url->ssl('reporteplanillaventa', 'exportxlsdataXLS'));
//        }
//
//        if ($user->hasPermisos($user->getPERSONA(), 'reporteplanillaventa', 'C'))
//            $view->set('exportarCSV', $url->ssl('reporteplanillaventa', 'exportarCSV'));

        //$view->set('addPais', $url->ssl('listaprecios','insert'));
        // $view->set('updatePersona', $url->ssl('paciente','update', array('persona' => $result['persona'])));
        //$view->set('updatePais', $url->ssl('listaprecios','update'));
        // </editor-fold>

        return $view->fetch('content/planillaventacajadiaria.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">
//		if ($request->has('modelo', 'post')) {
//                    $session->set('listaprecios.pais',$request->get('modelo','post'));              
//                }

        if ($request->has('search', 'post')) {
            $session->set('listaprecios.search', $request->get('search', 'post'));
        }

        if ($request->has('tipoproducto', 'post')) {
            $session->set('listaprecios.tipoproducto', $request->get('tipoproducto', 'post'));
        }

        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('listaprecios.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('listaprecios.order', (($session->get('listaprecios.sort') == $request->get('sort', 'post')) && ($session->get('listaprecios.order') == 'asc')) ? 'desc' : 'asc');
        }

        if ($request->has('sort', 'post')) {
            $session->set('listaprecios.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('reporteplanillaventacajadiaria'));
    }

    
    function exportarXLS_old() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        // </editor-fold>

        set_time_limit(0);

        //** PHPExcel **//
        require_once (DIR_INCLUDES . '/library/phpexcel/PHPExcel.php');

        date_default_timezone_set('America/Argentina/Buenos_Aires');
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set properties of the file
        $objPHPExcel->getProperties()->setCreator("NombreEmpresa")
                ->setLastModifiedBy("NombreEmpresa")
                ->setTitle("Planilla web exportada: " . date('d-m-Y'))
                ->setSubject("Planilla web exportada: " . date('d-m-Y'));

        //genero las columnas del excel
        $letra = 'A';
        $fila_inicial = 2; //fila desde donde empiezo a cargar la tabla en el excel. Arriba va el encabezado
        // <editor-fold defaultstate="collapsed" desc="PROPIEDADES DE LAS COLUMNAS">
        $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //COLUMNA A
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'ID');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA B
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'COD INTERNO');
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA C
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(37);
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', 'MODELO');
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA D
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(22);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', 'PRECIO VTA');
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">

        $sql = "SELECT * FROM vw_grilla_articulos  ";
        $conca = " WHERE ";

        //OBTENEMOS TODOS LOS TIPOS MARCADOS
        $tiposdeproducto = $request->get('tipos', 'post', array());

        if (count($tiposdeproducto) > 0) {
            $conca2 = ' (';
            foreach ($tiposdeproducto as $tipo) {
                $sql .= $conca . $conca2 . "tipoproducto = '" . $tipo . "'  ";
                $conca = " OR ";
                $conca2 = '';
            }
            $sql .= ") ";
            $conca = ' AND ';
        }

        //OBTENEMOS TODOS LOS TIPOS MARCADOS
        $marcas = $request->get('marcas', 'post', array());

        if (count($marcas) > 0) {
            $conca2 = ' (';
            foreach ($marcas as $marca) {
                $sql .= $conca . $conca2 . " marca = '" . $marca . "'  ";
                $conca = " OR ";
            }
            $sql .= ") ";
        }

        $sql .= " ORDER BY marca, modelo ASC";


//		$consul = $database->splitQuery($database->parse($sql,'%' . $session->get('articulos.search') . '%','%' . $session->get('articulos.search') . '%'), $session->get('articulos.page'), $config->get('config_max_rows'));
//
//                $results = $database->getRows($consul);

        $results = $database->getRows($sql);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="LLANAR LOS DATOS">

        $num = $fila_inicial;

        foreach ($results as $result) {

            //COLUMNA A
            $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $num, $result['articulo']);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            //COLUMNA B
            $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $num, $result['codbarrainterno']);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            //COLUMNA C
            $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $num, $result['descmodelo']);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA D
            $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' . $num, $result['preciovta']);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $num++;
        }

        //SEGUIR EN COMPLETAR EL EXCEL
        //POR CADA INSCRIPTO IR BUSCANDO CADA RESPUESTA Y COMPLETAR SEGUN CORRESPONDA
        // </editor-fold>
        // Nombre de la hoja del libro
        $objPHPExcel->getActiveSheet()->setTitle('ExportPrecios');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Save Excel 2007 file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');

        // It will be called file.xls | esta linea hace que sea una descarga y no una pestaña aparte
        header('Content-Disposition: attachment; filename="ExportPrecios.xls"');

        header('Cache-Control: max-age=0');

        // Write file to the browser
        $objWriter->save('php://output');
    }

    function exportxlsdataXLS() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $common = & $this->locator->get('common');
        // </editor-fold>

        //$tabla = 'vw_grilla_clientecontactos_xls';
        $tabla = 'vw_grilla_ventaformasdepago_xls';
        set_time_limit(0);

        try {
            $msg = '';

            $TABLE_NAME = $tabla;

            // <editor-fold defaultstate="collapsed" desc="TABLA SELECCIONADA A EXCEL">
            //** PHPExcel **//
            require_once (DIR_INCLUDES . '/library/phpexcel/PHPExcel.php');

            date_default_timezone_set('America/Argentina/Buenos_Aires');
            // Create new PHPExcel object
            $objPHPExcel = new PHPExcel();

            // Set properties of the file
            $objPHPExcel->getProperties()->setCreator("Reparación")
                    ->setLastModifiedBy("Reparación")
                    ->setTitle("Planilla web exportada: " . date('d-m-Y'))
                    ->setSubject("Planilla web exportada: " . date('d-m-Y'));

            

            //genero las columnas del excel
            $letra = 'A';
            $fila_inicial = 4; //fila desde donde empiezo a cargar la tabla en el excel. Arriba va el encabezado
            
            $ptovtaasignado = $common->getPuntoVtaAsignado();
            //$local = 'Nombre del local';
            $fecha = 'DD/MM/YYYY';
            
            //COLUMNA A
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2', 'LOCAL');
            $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true)->setName('Verdana')->setSize(12);
            
            //COLUMNA B Y D
            $objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->mergeCells('B2:D2');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B2', $ptovtaasignado);
            $objPHPExcel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA E
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('E2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('E2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E2', 'FECHA');
            $objPHPExcel->getActiveSheet()->getStyle('E2')->getFont()->setBold(true)->setName('Verdana')->setSize(12);
            
            //COLUMNA F
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('F2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F2', $fecha);
            $objPHPExcel->getActiveSheet()->getStyle('F2')->getFont()->setBold(true)->setName('Verdana')->setSize(12);
            
             //genero las columnas del excel
            $letra = 'A';
            $fila_inicial = 5; //fila desde donde empiezo a cargar la tabla en el excel. Arriba va el encabezado
                        
            // <editor-fold defaultstate="collapsed" desc="PROPIEDADES DE LAS COLUMNAS">
                $objPHPExcel->getActiveSheet()->getStyle('A3:R3')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            
            //COLUMNA A
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A3', 'TIPO');
            $objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA B
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('B3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('B3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B3', 'Nº');
            $objPHPExcel->getActiveSheet()->getStyle('B3')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA C
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('C3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('C3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C3', 'VENDEDOR');
            $objPHPExcel->getActiveSheet()->getStyle('C3')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA D
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('D3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('D3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D3', 'TIPO DE VTA');
            $objPHPExcel->getActiveSheet()->getStyle('D3')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
             
            //COLUMNA E
            //$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('E3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('E3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E3', 'MODELO');
            $objPHPExcel->getActiveSheet()->getStyle('E3')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA F
            //$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('F3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('F3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F3', 'CLIENTE');
            $objPHPExcel->getActiveSheet()->getStyle('F3')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA G
            //$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('G3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('G3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G3', 'IMEI');
            $objPHPExcel->getActiveSheet()->getStyle('G3')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA H
            //$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('H3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('H3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H3', 'SIM');
            $objPHPExcel->getActiveSheet()->getStyle('H3')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA I
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('I3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('I3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I3', 'FICHA');
            $objPHPExcel->getActiveSheet()->getStyle('I3')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA J
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('J3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('J3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J3', 'LINEA');
            $objPHPExcel->getActiveSheet()->getStyle('J3')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA K
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('K3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('K3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K3', 'ENTRADA');
            $objPHPExcel->getActiveSheet()->getStyle('K3')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA L
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('L3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('L3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L3', 'SALIDA');
            $objPHPExcel->getActiveSheet()->getStyle('L3')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA M
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('M3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('M3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M3', 'SALDO');
            $objPHPExcel->getActiveSheet()->getStyle('M3')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA N
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('N3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('N3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N3', 'IMPORTE');
            $objPHPExcel->getActiveSheet()->getStyle('N3')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA O
            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('O3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('O3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('O3', 'TIPO');
            $objPHPExcel->getActiveSheet()->getStyle('O3')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA P
            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('P3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('P3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('P3', 'CODIGO');
            $objPHPExcel->getActiveSheet()->getStyle('P3')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA Q
            $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('Q3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('Q3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q3', 'TARJETA');
            $objPHPExcel->getActiveSheet()->getStyle('Q3')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA R
            $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('R3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('R3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('R3', 'VENDEDOR');
            $objPHPExcel->getActiveSheet()->getStyle('R3')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            
            //COLUMNA A
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('A4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A4', '-');
            $objPHPExcel->getActiveSheet()->getStyle('A4')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA B
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('B4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B4', '-');
            $objPHPExcel->getActiveSheet()->getStyle('B4')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA C
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('C4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('C4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C4', '-');
            $objPHPExcel->getActiveSheet()->getStyle('C4')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA D
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('D4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('D4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D4', '-');
            $objPHPExcel->getActiveSheet()->getStyle('D4')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
             
            //COLUMNA E
            //$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('E4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('E4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E4', '-');
            $objPHPExcel->getActiveSheet()->getStyle('E4')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA F
            //$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('F4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('F4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F4', '-');
            $objPHPExcel->getActiveSheet()->getStyle('F4')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA G
            //$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('G4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('G4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G4', '-');
            $objPHPExcel->getActiveSheet()->getStyle('G4')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA H
            //$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('H4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('H4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H4', '-');
            $objPHPExcel->getActiveSheet()->getStyle('H4')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA I
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('I4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('I4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I4', '-');
            $objPHPExcel->getActiveSheet()->getStyle('I4')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA J
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('J4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('J4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J4', '-');
            $objPHPExcel->getActiveSheet()->getStyle('J4')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA K Y L
            $objPHPExcel->getActiveSheet()->getStyle('K4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('K4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->mergeCells('K4:L4');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K4', 'Saldo Inicial');
            $objPHPExcel->getActiveSheet()->getStyle('K4')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA M
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('M4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('M4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M4', '-');
            $objPHPExcel->getActiveSheet()->getStyle('M4')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA N
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('N4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('N4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N4', '-');
            $objPHPExcel->getActiveSheet()->getStyle('N4')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA O
            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('O4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('O4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('O4', '-');
            $objPHPExcel->getActiveSheet()->getStyle('O4')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA P
            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('P4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('P4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('P4', '-');
            $objPHPExcel->getActiveSheet()->getStyle('P4')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA Q
            $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('Q4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('Q4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q4', '-');
            $objPHPExcel->getActiveSheet()->getStyle('Q4')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            //COLUMNA R
            $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getStyle('R4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('R4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('R4', '-');
            $objPHPExcel->getActiveSheet()->getStyle('R4')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="LLANAR LOS DATOS">
             // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
             set_time_limit(0);
                
              
              
                $sql = "SELECT * FROM vw_grilla_ventaformasdepago_xls WHERE PUNTOVTA = " . $ptovtaasignado['puntovtaasignado'];
                $conca = " AND " ;
                   
               // <editor-fold defaultstate="collapsed" desc="FECHA">
                $fecha_d = '';
                if ($request->get('desde', 'post') != '') {
                    $dated = explode('/', $request->get('desde', 'post'));
                    //$fecha_d = $dated[0] . '-' . $dated[1] . '-' . $dated[2];
                    $fecha_d = 	str_replace('/','-',$request->get('desde', 'post'));
                }
                if ($fecha_d != '') {
                    $sql .= $conca . " STR_TO_DATE(FECHA, '%d-%m-%Y') >= STR_TO_DATE('" . $fecha_d . "', '%d-%m-%Y') ";
                    $conca = " AND ";
                }

                $fecha_h = '';
                if ($request->get('hasta', 'post') != '') {
                    $dated = explode('/', $request->get('hasta', 'post'));
                    //$fecha_h = $dated[0] . '-' . $dated[1] . '-' . $dated[2];
                   $fecha_h = 	str_replace('/','-',$request->get('hasta', 'post'));
                    // $fecha_h = $request->get('hasta', 'post');
                }
                if ($fecha_h != '') {
                    $sql .= $conca . " STR_TO_DATE(FECHA, '%d-%m-%Y') <= STR_TO_DATE('" . $fecha_h . "', '%d-%m-%Y') ";
                    $conca = " AND ";
                }
                // </editor-fold>
                
                $results = $database->getRows($sql);
		
            // </editor-fold>
            
            $num = 5;

            foreach ($results as $result) {
//COLUMNA A
                $objPHPExcel->getActiveSheet()->getStyle('A'.$num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$num, '');
                $objPHPExcel->getActiveSheet()->getStyle('A'.$num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //COLUMNA B
                $objPHPExcel->getActiveSheet()->getStyle('B'.$num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$num, '');
                $objPHPExcel->getActiveSheet()->getStyle('B'.$num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                
                //COLUMNA C
                $objPHPExcel->getActiveSheet()->getStyle('C'.$num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$num, $result['T']);
                $objPHPExcel->getActiveSheet()->getStyle('C'.$num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                     
                //COLUMNA D
                $objPHPExcel->getActiveSheet()->getStyle('D'.$num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$num, $result['Drel']);
                $objPHPExcel->getActiveSheet()->getStyle('D'.$num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                
                //COLUMNA E
                $objPHPExcel->getActiveSheet()->getStyle('E'.$num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$num, $result['D_SHI']);
                $objPHPExcel->getActiveSheet()->getStyle('E'.$num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                
                //COLUMNA F
                $objPHPExcel->getActiveSheet()->getStyle('F'.$num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$num, $result['D_AccHI']);
                $objPHPExcel->getActiveSheet()->getStyle('F'.$num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

//                //COLUMNA G
                $objPHPExcel->getActiveSheet()->getStyle('G'.$num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$num, $result['D_DecHI']);
                $objPHPExcel->getActiveSheet()->getStyle('G'.$num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

                //COLUMNA H
                $objPHPExcel->getActiveSheet()->getStyle('H'.$num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$num, $result['D_MPHI']);
                $objPHPExcel->getActiveSheet()->getStyle('H'.$num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

                 //COLUMNA I
                $objPHPExcel->getActiveSheet()->getStyle('I'.$num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$num, $result['%D_SHI']);
                $objPHPExcel->getActiveSheet()->getStyle('I'.$num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

                 //COLUMNA J
                $objPHPExcel->getActiveSheet()->getStyle('J'.$num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$num, $result['%D_AccHI']);
                $objPHPExcel->getActiveSheet()->getStyle('J'.$num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

                 //COLUMNA K
                $objPHPExcel->getActiveSheet()->getStyle('K'.$num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$num, $result['%D_DecHI']);
                $objPHPExcel->getActiveSheet()->getStyle('K'.$num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

                 //COLUMNA H
                $objPHPExcel->getActiveSheet()->getStyle('L'.$num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$num, $result['%D_MPHI']);
                $objPHPExcel->getActiveSheet()->getStyle('L'.$num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

                 //COLUMNA M
                $objPHPExcel->getActiveSheet()->getStyle('M'.$num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$num, $result['Smax']);
                $objPHPExcel->getActiveSheet()->getStyle('M'.$num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

                 //COLUMNA N
                $objPHPExcel->getActiveSheet()->getStyle('N'.$num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$num, $result['Amax']);
                $objPHPExcel->getActiveSheet()->getStyle('N'.$num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

                 //COLUMNA O
                $objPHPExcel->getActiveSheet()->getStyle('O'.$num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.$num, $result['D_A1']);
                $objPHPExcel->getActiveSheet()->getStyle('O'.$num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

                //COLUMNA P
                $objPHPExcel->getActiveSheet()->getStyle('P'.$num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.$num, $result['D_A8']);
                $objPHPExcel->getActiveSheet()->getStyle('P'.$num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

                //COLUMNA O
                $objPHPExcel->getActiveSheet()->getStyle('Q'.$num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q'.$num, $result['D_MP5']);
                $objPHPExcel->getActiveSheet()->getStyle('Q'.$num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

                $num++;
            }

            //SEGUIR EN COMPLETAR EL EXCEL
            //POR CADA INSCRIPTO IR BUSCANDO CADA RESPUESTA Y COMPLETAR SEGUN CORRESPONDA
            // </editor-fold>
            //Cargo los datos celda por celda
            // Nombre de la hoja del libro
            $objPHPExcel->getActiveSheet()->setTitle($TABLE_NAME);

            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $objPHPExcel->setActiveSheetIndex(0);

            // Save Excel 2007 file
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

            // We'll be outputting an excel file
            header('Content-type: application/vnd.ms-excel');

            $nombre_archivo = $TABLE_NAME . '.xls';
            // It will be called file.xls | esta linea hace que sea una descarga y no una pestaña aparte
            header('Content-Disposition: attachment; filename=' . $nombre_archivo);

            header("Expires: 0");

            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

            // Write file to the browser
            //$objWriter->save($nombre_archivo); 
           // $objWriter->save('Archivos/exportatablaxls/' . $nombre_archivo);
            // Write file to the browser
            $objWriter->save('php://output'); 

            $common->log_msg($msg, 'excel_log');
            //$objPHPExcel->
            //$objWriter->close();
            //$objPHPExcel->disconnectWorksheets();
            //unset($objWriter, $objPHPExcel);
            // </editor-fold>
        } catch (Exception $e) {
            $msg .= date('c') . "\r\n \t CATCH:\t " . $e . " \r\n";
            $common->log_msg($msg, 'excel_log');
        }
        //$view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
    }

    function exportxlsdataCSV() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $common = & $this->locator->get('common');
        // </editor-fold>

        //$tabla = 'vw_grilla_clientecontactos_xls';
        $tabla = 'vw_grilla_ventaformasdepago_xls';
        set_time_limit(0);

        try {
            $msg = '';

            $TABLE_NAME = $tabla;

            // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">
            $sql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '" . $TABLE_NAME . "' AND table_schema = '" . DB_NAME . "'  ";
            $list_colum_name = $database->getRows($sql);
            $cant_columnas = count($list_colum_name);
            
            $nombre_de_las_columnas = array();
            for ($i = 0; $i < $cant_columnas ; $i++) {
                    $name = $list_colum_name[$i][COLUMN_NAME];
                    $nombre_de_las_columnas[] = $name;
            }
                
            // </editor-fold>
            
            // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
             set_time_limit(0);
                
//                if (!$session->get('reporteplanillaventa.search')) {
                $sql = "SELECT * FROM " . $TABLE_NAME . " WHERE PUNTOVTA != '0' ";
                $conca = " AND ";
//		} else {
//			$sql = "SELECT * FROM " . $TABLE_NAME; 
//                        $sql .= " WHERE (CLIENTE LIKE '?' or CONTACTO LIKE '?')  ";
//			$conca = " AND ";
//		}
                   
                // <editor-fold defaultstate="collapsed" desc="FECHA">
                $fecha_d = '';
                if ($request->get('desde', 'post') != '') {
                    $dated = explode('/', $request->get('desde', 'post'));
                   // $fecha_d = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
                    $fecha_d = 	str_replace('/','-',$request->get('desde', 'post'));
                }
                if ($fecha_d != '') {
                    $sql .= $conca . " STR_TO_DATE(FECHA, '%d-%m-%Y') >= STR_TO_DATE('" . $fecha_d . "', '%d-%m-%Y') ";
                    $conca = " AND ";
                }

                $fecha_h = '';
                if ($request->get('hasta', 'post') != '') {
                    $dated = explode('/', $request->get('hasta', 'post'));
                   // $fecha_h = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
                    $fecha_h = 	str_replace('/','-',$request->get('hasta', 'post'));
                    // $fecha_h = $request->get('hasta', 'post');
                }
                if ($fecha_h != '') {
                    $sql .= $conca . " STR_TO_DATE(FECHA, '%d-%m-%Y') <= STR_TO_DATE('" . $fecha_h . "', '%d-%m-%Y')";
                    $conca = " AND ";
                }
                // </editor-fold>
                                
                //$sql = "SELECT * FROM " . $TABLE_NAME;
                $results = $database->getRows($sql);
		
                //LE METO EN ELCABEZADO
                array_unshift($results, $nombre_de_las_columnas);
                
            // </editor-fold>
            
            // <editor-fold defaultstate="collapsed" desc="TABLA SELECCIONADA A CSV">
            //cabeceras para descarga
            header('Content-Type: application/octet-stream');
            header("Content-Transfer-Encoding: Binary"); 
            header("Content-disposition: attachment; filename=\"my_csv_file.csv\""); 

            //preparar el wrapper de salida
            $outputBuffer = fopen("php://output", 'w');

            //volcamos el contenido del array en formato csv
            foreach($results as $val) {
                //fputcsv($outputBuffer,$val );
                fputcsv($outputBuffer, $val, ";");
            }
            //cerramos el wrapper
            fclose($outputBuffer);
            exit;

            // </editor-fold>
            
        } catch (Exception $e) {
            $msg .= date('c') . "\r\n \t CATCH:\t " . $e . " \r\n";
            $common->log_msg($msg, 'excel_log');
        }
        //$view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
    }
    
    function devuelveLetra($num) {
        $letra = '';
        switch ($num) {
            case 1:$letra = 'A';
                break;
            case 2:$letra = 'B';
                break;
            case 3:$letra = 'C';
                break;
            case 4:$letra = 'D';
                break;
            case 5:$letra = 'E';
                break;
            case 6:$letra = 'F';
                break;
            case 7:$letra = 'G';
                break;
            case 8:$letra = 'H';
                break;
            case 9:$letra = 'I';
                break;
            case 10:$letra = 'J';
                break;
            case 11:$letra = 'K';
                break;
            case 12:$letra = 'L';
                break;
            case 13:$letra = 'M';
                break;
            case 14:$letra = 'N';
                break;
            case 15:$letra = 'O';
                break;
            case 16:$letra = 'P';
                break;
            case 17:$letra = 'Q';
                break;
            case 18:$letra = 'R';
                break;
            case 19:$letra = 'S';
                break;
            case 20:$letra = 'T';
                break;
            case 21:$letra = 'U';
                break;
            case 22:$letra = 'V';
                break;
            case 23:$letra = 'W';
                break;
            case 24:$letra = 'X';
                break;
            case 25:$letra = 'Y';
                break;
            case 26:$letra = 'Z';
                break;
            case 27:$letra = 'AA';
                break;
            case 28:$letra = 'AB';
                break;
            case 29:$letra = 'AC';
                break;
            case 30:$letra = 'AD';
                break;
            case 31:$letra = 'AE';
                break;
            case 32:$letra = 'AF';
                break;
            case 33:$letra = 'AG';
                break;
            case 34:$letra = 'AH';
                break;
            case 35:$letra = 'AI';
                break;
            case 36:$letra = 'AJ';
                break;
            case 37:$letra = 'AK';
                break;
            case 38:$letra = 'AL';
                break;
            case 39:$letra = 'AM';
                break;
            case 40:$letra = 'AN';
                break;
            case 41:$letra = 'AO';
                break;
            case 42:$letra = 'AP';
                break;
            case 43:$letra = 'AQ';
                break;
            case 44:$letra = 'AR';
                break;
            case 45:$letra = 'AS';
                break;
            case 46:$letra = 'AT';
                break;
            case 47:$letra = 'AU';
                break;
            case 48:$letra = 'AV';
                break;
            case 49:$letra = 'AW';
                break;
            case 50:$letra = 'AX';
                break;
            case 51:$letra = 'AY';
                break;
            case 52:$letra = 'AZ';
                break;

            case 53:$letra = 'BA';
                break;
            case 54:$letra = 'BB';
                break;
            case 55:$letra = 'BC';
                break;
            case 56:$letra = 'BD';
                break;
            case 57:$letra = 'BE';
                break;
            case 58:$letra = 'BF';
                break;
            case 59:$letra = 'BG';
                break;
            case 60:$letra = 'BH';
                break;
            case 61:$letra = 'BI';
                break;
            case 62:$letra = 'BJ';
                break;
            case 63:$letra = 'BK';
                break;
            case 64:$letra = 'BL';
                break;
            case 65:$letra = 'BM';
                break;
            case 66:$letra = 'BN';
                break;
            case 67:$letra = 'BO';
                break;
            case 68:$letra = 'BP';
                break;
            case 69:$letra = 'BQ';
                break;
            case 70:$letra = 'BR';
                break;
            case 71:$letra = 'BS';
                break;
            case 72:$letra = 'BT';
                break;
            case 73:$letra = 'BU';
                break;
            case 74:$letra = 'BV';
                break;
            case 75:$letra = 'BW';
                break;
            case 76:$letra = 'BX';
                break;
            case 77:$letra = 'BY';
                break;
            case 78:$letra = 'BZ';
                break;

            case 79:$letra = 'CA';
                break;
            case 80:$letra = 'CB';
                break;
            case 81:$letra = 'CC';
                break;
            case 82:$letra = 'CD';
                break;
            case 83:$letra = 'CE';
                break;
            case 84:$letra = 'CF';
                break;
            case 85:$letra = 'CG';
                break;
            case 86:$letra = 'CH';
                break;
            case 87:$letra = 'CI';
                break;
            case 88:$letra = 'CJ';
                break;
            case 89:$letra = 'CK';
                break;
            case 90:$letra = 'CL';
                break;
            case 91:$letra = 'CM';
                break;
            case 92:$letra = 'CN';
                break;
            case 93:$letra = 'CO';
                break;
            case 94:$letra = 'CP';
                break;
            case 95:$letra = 'CQ';
                break;
            case 96:$letra = 'CR';
                break;
            case 97:$letra = 'CS';
                break;
            case 98:$letra = 'CT';
                break;
            case 99:$letra = 'CU';
                break;
            case 100:$letra = 'CV';
                break;
            case 101:$letra = 'CW';
                break;
            case 102:$letra = 'CX';
                break;
            case 103:$letra = 'CY';
                break;
            case 104:$letra = 'CZ';
                break;

            case 105:$letra = 'DA';
                break;
            case 106:$letra = 'DB';
                break;
            case 107:$letra = 'DC';
                break;
            case 108:$letra = 'DD';
                break;
            case 109:$letra = 'DE';
                break;
            case 110:$letra = 'DF';
                break;
            case 111:$letra = 'DG';
                break;
            case 112:$letra = 'DH';
                break;
            case 113:$letra = 'DI';
                break;
            case 114:$letra = 'DJ';
                break;
            case 115:$letra = 'DK';
                break;
            case 116:$letra = 'DL';
                break;
            case 117:$letra = 'DM';
                break;
            case 118:$letra = 'DN';
                break;
            case 119:$letra = 'DO';
                break;
            case 120:$letra = 'DP';
                break;
            case 121:$letra = 'DQ';
                break;
            case 122:$letra = 'DR';
                break;
            case 123:$letra = 'DS';
                break;
            case 124:$letra = 'DT';
                break;
            case 125:$letra = 'DU';
                break;
            case 126:$letra = 'DV';
                break;
            case 127:$letra = 'DW';
                break;
            case 128:$letra = 'DX';
                break;
            case 129:$letra = 'DY';
                break;
            case 130:$letra = 'DZ';
                break;

            case 131:$letra = 'EA';
                break;
            case 132:$letra = 'EB';
                break;
            case 133:$letra = 'EC';
                break;
            case 134:$letra = 'ED';
                break;
            case 135:$letra = 'EE';
                break;
            case 136:$letra = 'EF';
                break;
            case 137:$letra = 'EG';
                break;
            case 138:$letra = 'EH';
                break;
            case 139:$letra = 'EI';
                break;
            case 140:$letra = 'EJ';
                break;
            case 141:$letra = 'EK';
                break;
            case 142:$letra = 'EL';
                break;
            case 143:$letra = 'EM';
                break;
            case 144:$letra = 'EN';
                break;
            case 145:$letra = 'EO';
                break;

            default:
                break;
        }
        return $letra;
    }
}
?>