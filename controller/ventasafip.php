<?php

class ControllerVentasafip extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'VENTAS');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
        $request = & $this->locator->get('request');
        $common = & $this->locator->get('common');
        // </editor-fold>
         
        // <editor-fold defaultstate="collapsed" desc="LIMPIA FILTRO Y VARIBLES DE SESSION">
        
        if ($request->get('filtra') == NULL) {

            $session->set('ventasafip.search', '');
            $session->set('ventasafip.fecha', '');
            $session->set('ventasafip.sort', '');
            $session->set('ventasafip.order', '');
            $session->set('ventasafip.page', '');

            $view->set('search', '');
            $view->set('ventasafip.search', '');
            $view->set('ventasafip.fecha', '');
            
            $cache->delete('ventasafip');
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">
        $cols = array();

        $cols[] = array(
            'name' => 'Venta',
            'sort' => 'venta',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Nro factura',
            'sort' => 'nrofactura',
            'align' => 'left'
        );
        
        $cols[] = array(
            'name' => 'Fecha',
            'sort' => 'fecha',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Facturación',
            'sort' => 'tipofacturacion',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Comprobante ',
            'sort' => 'tipocomprobante',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Pto Vta',
            'sort' => 'puntovta',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Vendedor',
            'sort' => 'au_usuario',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Comprador',
            'sort' => 'personacomprador',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Total',
            'sort' => 'total',
            'align' => 'left'
        );


        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'venta',
            'nrofactura',
            'fecha',
            'tipocomprobante',
            'tipofacturacion',
            'puntovta',
            'au_usuario',
            'personacomprador',
            'total'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">

        

        if (!$session->get('ventasafip.search')) {
            //SI ES EXENTO PODRIA ENCONTRAR CUALQUIER VENTA
            if ($user->getExento() == 1) {
               $sql = "SELECT * FROM vw_grilla_ventas WHERE tipofacturacion = 'V'  ";
                $conca = " AND ";
                } else {
                $sql = "SELECT * FROM vw_grilla_ventas WHERE tipofacturacion = 'V'  AND puntovta = '" . $user->getPuntovtaasignado() . "' ";
                $conca = " AND ";
            }
        } else {
            //SI ES EXENTO PODRIA ENCONTRAR CUALQUIER VENTA
            if ($user->getExento() == 1) {
                $sql = "SELECT * FROM vw_grilla_ventas WHERE tipofacturacion = 'V' AND (venta LIKE '" . '%' . $session->get('ventasafip.search') . '%' . "' OR nrofactura LIKE '" . '%' . $session->get('ventasafip.search') . '%' . "') ";
                $conca = " AND ";
            } else {
                $sql = "SELECT * FROM vw_grilla_ventas WHERE tipofacturacion = 'V' AND puntovta = '" . $user->getPuntovtaasignado() . "' AND venta LIKE '" . '%' . $session->get('ventasafip.search') . '%' . "'  ";
                $conca = " AND ";
            }
        }

        //filtro por fecha
        // <editor-fold defaultstate="collapsed" desc="FECHA">
                $fecha_d = '';
                
                if ($session->get('ventasafip.fecha')) {
                    $dated = explode('/', $session->get('ventasafip.fecha'));
                    //$fecha_d = $dated[0] . '-' . $dated[1] . '-' . $dated[2];
                    $fecha_d = 	str_replace('/','-',$session->get('ventasafip.fecha'));
                }
                else{
                    if (!$session->get('ventasafip.search')) {
                        $hoy = date("d-m-Y");
                        $fecha_d = $hoy;
                    }
                    
                }    
                
                if ($fecha_d != '') {
                    $sql .= $conca . " STR_TO_DATE(DATE_FORMAT(fecha, '%d-%m-%Y'), '%d-%m-%Y') = STR_TO_DATE('" . $fecha_d . "', '%d-%m-%Y') ";
                    $conca = " AND ";
                }
                

                
                // </editor-fold>
        

        if (in_array($session->get('ventasafip.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('ventasafip.sort') . " " . (($session->get('ventasafip.order') == 'DESC') ? 'DESC' : 'ASC');
        } else {
            $sql .= " ORDER BY venta DESC";
        }

        //$consult = $database->parse($sql, '%' . $session->get('ventasafip.search') . '%');
        $results = $database->getRows($sql);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['venta'],
                'align' => 'left',
                'default' => 0
            );
            $cell[] = array(
                'value' => @$result['nrofactura'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => date('d/m/Y', strtotime(@$result['fecha'])),
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['desctipofacturacion'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['desctipocomprobante'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['descpuntovta'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['nombreApellidoVendedor'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['nombreApellidoComprador'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['total'],
                'align' => 'left',
                'default' => 0
            );

            $action = array();


            if ($user->hasPermisos($user->getPERSONA(), 'ventasafip', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa fa-fw fa-pencil',
                    'text' => $language->get('button_update'),
                    'prop_a' => array('href' => $url->ssl('ventasafip', 'update', array('venta' => $result['venta'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'ventasafip', 'B')) {
                $action[] = array(
                    'icon' => 'img/iconos-11.png',
                    'text' => $language->get('button_delete'),
                    'class' => 'fa fa-fw fa-trash-o',
                    'prop_a' => array('href' => '/'
                        , 'onclick' => "ActionDelete('" . $result['venta'] . "'); return false;")
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'ventasafip', 'C')) {
                        $action[] = array(
                            'icon' => 'img/iconos-17.png',
                            'class' => 'fa fa-fw fa-search',
                            'text' => $language->get('button_consult'),
                            'prop_a' => array('href' => $url->ssl('ventasafip', 'consulta', array('venta' => $result['venta'])))
                        );

                        if ($user->hasPermisos($user->getPERSONA(), 'ventasafip', 'C')) {
                            $action[] = array(
                                'icon' => 'img/iconos-17.png',
                                'class' => 'fa fa-fw  fa-money',
                                'text' => 'Remito',
                                'target' => ' target="_blank" ',
                                'prop_a' => array('href' => $url->ssl('ventasafip', 'RemitoPDF', array('venta' => $result['venta'])))
                            );
                        }
                
                        if ($result['tipocomprobante'] == 'FAE') {
                           if ($user->hasPermisos($user->getPERSONA(), 'ventasafip', 'C') && $result['cae'] != '') {
                            $action[] = array(
                                'icon' => 'img/iconos-17.png',
                                'class' => 'fa fa-fw  fa-money',
                                'text' => 'Descargar factura',
                                'target' => ' target="_blank" ',
                                'prop_a' => array('href' => $url->ssl('ventasafip', 'ImprimirComprobanteA', array('venta' => $result['venta'])))
                            );
                          } 
                        }
                        if ($result['tipocomprobante'] == 'FBE') {
                           if ($user->hasPermisos($user->getPERSONA(), 'ventasafip', 'C') && $result['cae'] != '') {
                            $action[] = array(
                                'icon' => 'img/iconos-17.png',
                                'class' => 'fa fa-fw  fa-money',
                                'text' => 'Descargar factura',
                                'target' => ' target="_blank" ',
                                'prop_a' => array('href' => $url->ssl('ventasafip', 'ImprimirComprobanteB', array('venta' => $result['venta'])))
                            );
                          } 
                        }
                
                else{
                    
                    if ($result['tipocomprobante'] == 'FAE') {
                           if ($user->hasPermisos($user->getPERSONA(), 'ventasafip', 'C') && $result['cae'] != '') {
                           $action[] = array(
                            'icon' => 'img/iconos-17.png',
                            'class' => 'fa fa-fw  fa-money',
                            'text' => 'Reprocesar factura',
                            'target' => ' target="_blank" ',
                            'prop_a' => array('href' => $url->ssl('ventasafip', 'reprocesarComprobanteA', array('venta' => $result['venta'])))
                        );
                          } 
                        }
                        if ($result['tipocomprobante'] == 'FBE') {
                           if ($user->hasPermisos($user->getPERSONA(), 'ventasafip', 'C') && $result['cae'] != '') {
                            $action[] = array(
                        'icon' => 'img/iconos-17.png',
                        'class' => 'fa fa-fw  fa-money',
                        'text' => 'Reprocesar factura',
                        'target' => ' target="_blank" ',
                        'prop_a' => array('href' => $url->ssl('ventasafip', 'reprocesarComprobanteB', array('venta' => $result['venta'])))
                    );
                          } 
                        }
                        
                    
                }
            }

            if ($user->hasPermisos($user->getPERSONA(), 'notasdecredito', 'A')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa fa-fw fa-plus',
                    'text' => 'NC',
                    'prop_a' => array('href' => $url->ssl('notasdecredito', 'insert', array('venta' => $result['venta'])))
                );
            }

            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('ventasafip.page'));


        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        if ($session->get('ventasafip.venta')) {
            $view->set('venta', $session->get('ventasafip.venta'));
        } else {
            $view->set('venta', '');
        }
        
        if ($session->get('ventasafip.fecha')) {
            $view->set('fecha', $session->get('ventasafip.fecha'));
        } else {
            $view->set('fecha', '');
        }

//		if ($session->get('ventasafip.nombre')) {
//                    $view->set('nombre',$session->get('ventasafip.nombre'));
//		} else {
//                    $view->set('nombre', '');
//		}
//		if ($session->get('ventasafip.localidad')) {
//                    $view->set('localidad', $session->get('ventasafip.localidad'));
//		} else {
//                    $view->set('localidad', '-1');
//		}
        //$view->set('localidades', $database->getRows("SELECT localidad, descripcion FROM localidades ORDER BY descripcion"));

        $view->set('heading_title', 'VENTAS');
        $view->set('heading_title2', 'Filtrar');
        $view->set('heading_description', 'Gesti&oacute;n de VENTAS');

        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/NOTICIAS.png');
        $view->set('placeholder_buscar', 'BUSCA POR NRO VENTA o NRO FACTURA ');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('ventasafip.search'));
        $view->set('fecha', $session->get('ventasafip.fecha'));
        $view->set('sort', $session->get('ventasafip.sort'));
        $view->set('order', $session->get('ventasafip.order'));
        $view->set('page', $session->get('ventasafip.page'));

        //$view->set('search', $session->get('ventasafip.search'));

        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('ventasafip', 'page'));
        $view->set('list', $url->ssl('ventasafip'));
        if ($user->hasPermisos($user->getPERSONA(), 'ventasafip', 'A')) {
            $view->set('insert', $url->ssl('ventasafip', 'insert'));
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_ventas_afip.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">

        if ($request->has('search', 'post')) {
            $session->set('ventasafip.search', $request->get('search', 'post'));
        }
        if ($request->has('fecha', 'post')) {
            $session->set('ventasafip.fecha', $request->get('fecha', 'post'));
        }


        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('ventasafip.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('ventasafip.order', (($session->get('ventasafip.sort') == $request->get('sort', 'post')) && ($session->get('ventasafip.order') == 'ASC')) ? 'DESC' : 'ASC');
        }

        if ($request->has('sort', 'post')) {
            $session->set('ventasafip.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('ventasafip', 'index', array('filtra' => '1')));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $common = & $this->locator->get('common');
        $language = & $this->locator->get('language');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $view = $this->locator->create('template');
        $user = & $this->locator->get('user');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES Y VIEW VARIABLES">


        $view->set('heading_title_cliente', 'VENTA');
        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/user-8.png');

        $view->set('heading_description', 'Gesti&oacute;n de VENTAS');

        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        $view->set('tab_general', $language->get('tab_general'));

        // <editor-fold defaultstate="collapsed" desc="DETALLE DE VENTA">

        if (($request->get('venta')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM vw_grilla_ventas WHERE venta = '?'";
            @$objeto_info = $database->getRow($database->parse($consulta, $request->get('venta')));
        }

        if ($request->has('venta', 'post')) {
            $view->set('venta', $request->get('venta', 'post'));
            $objeto_id = $request->get('venta', 'post');
        } else {
            if ($request->has('venta', 'get')) {
                $objeto_id = @$objeto_info['venta'];
            } else {
                $objeto_id = $common->GetaaaaMMddhhmmssmm();
            }
            $view->set('venta', $objeto_id);
        }

        $view->set('heading_title', 'VENTA NRO: ' . $objeto_id);

        $view->set('entry_fecha', 'Fecha:');
        if ($request->has('fecha', 'post')) {
            $view->set('fecha', $request->get('fecha', 'post'));
        } else {
            if ($request->has('fecha', 'get')) {
                $view->set('fecha', date('d/m/Y', strtotime(@$objeto_info['fecha'])));
            } else {
                $view->set('fecha', date('d/m/Y'));
            }
        }

        $view->set('entry_tipocomprobante', 'Tipo comprobante:');
        if ($request->has('tipocomprobante', 'post')) {
            $view->set('tipocomprobante', $request->get('tipocomprobante', 'post'));
        } else {
            $tipocomprobante = '';
            if ($request->has('tipocomprobante', 'get')) {
                $tipocomprobante = @$objeto_info['tipocomprobante'];
            } else {
                $tipocomprobante = 'FBE';
            }
            $view->set('tipocomprobante', $tipocomprobante);
        }
        //SOLO VAN A APARECER LOS COMPROBANTES QUE TENGAN POSICIONES FISCALES DEL PUNTO DE VENTA LOGUIADO
        $view->set('tiposcomprobante', $database->getRows("SELECT tc.tipocomprobante, tc.descripcion FROM posicionesfiscales pf LEFT JOIN tiposcomprobante tc ON pf.tipocomprobante = tc.tipocomprobante WHERE tc.seveenventas = 1 AND pf.puntovta = '".$user->getPuntovtaasignado()."' ORDER BY tc.descripcion ASC"));

        $view->set('entry_tipofacturacion', 'Tipo facturación:');
        if ($request->has('tipofacturacion', 'post')) {
            $view->set('tipofacturacion', $request->get('tipofacturacion', 'post'));
        } else {
            $tipofacturacion = '';
            if ($request->has('tipofacturacion', 'get')) {
                $tipofacturacion = @$objeto_info['tipofacturacion'];
            } else {
                $tipofacturacion = 'V';
            }
            $view->set('tipofacturacion', $tipofacturacion);
        }
        $view->set('tiposfacturacion', $database->getRows("SELECT * FROM tiposfacturacion WHERE tipofacturacion ='V' ORDER BY descripcion ASC"));

        $view->set('entry_personacomprador', 'Cliente:');
        if ($request->has('auto_personacomprador', 'post')) {
            $view->set('auto_personacomprador', $request->get('auto_personacomprador', 'post'));
            $view->set('auto_personacomprador_venta', $request->get('auto_personacomprador_venta', 'post'));
        } else {
            if ($request->get('venta')) {
                $desccomprador = '';
                if ($objeto_info['personacomprador'] != '0') {
                    $desccomprador = '(' . $objeto_info['personacomprador'] . ')' . @$objeto_info['nombreApellidoComprador'];
                } else {
                    $desccomprador = @$objeto_info['nombreApellidoComprador'];
                }
                $view->set('auto_personacomprador', $desccomprador);
                $view->set('auto_personacomprador_venta', @$objeto_info['personacomprador']);
            } else {

                $view->set('auto_personacomprador', 'CONSUMIDOR FINAL');
                $view->set('auto_personacomprador_venta', 0);
            }
        }
        $view->set('script_busca_personacomprador', $url->rawssl('ventasafip', 'getPersonacomprador'));

        $ptovtaasignado = $database->getRow("SELECT p.puntovtaasignado, pv.descripcion as descpuntovta, pv.listaprecio "
                . "FROM personas p "
                . "LEFT JOIN puntosdeventa pv ON p.puntovtaasignado = pv.puntovta where p.persona = '" . $user->getPERSONA() . "' ");

        $view->set('entry_puntovta', 'Punto de Venta:');
        if ($request->has('puntovta', 'post')) {
            $view->set('puntovta', $request->get('puntovta', 'post'));
            $puntovta = $request->get('puntovta', 'post');
        } else {
            //va el punto de venta que tiene asignado el vendedor

            $view->set('puntovta', @$ptovtaasignado['puntovtaasignado']);
            $puntovta = @$ptovtaasignado['puntovtaasignado'];
        }

        if ($request->has('descpuntovta', 'post')) {
            $view->set('descpuntovta', $request->get('total', 'post'));
        } else {
            if ($ptovtaasignado) {
                $view->set('descpuntovta', $ptovtaasignado['descpuntovta']);
            } else {
                $view->set('descpuntovta', 'SIN PUNTO VENTA');
            }
        }

        $view->set('entry_vendedor', 'Vendedor:');
        if ($request->has('vendedor', 'post')) {
            $view->set('vendedor', $request->get('vendedor', 'post'));
        } else {
            if (@$objeto_info['vendedor']) {
                $view->set('vendedor', @$objeto_info['vendedor']);
            } else {
                $view->set('vendedor', '-1');
            }
        }

        if ($request->get('action', 'get') == 'insert') {
            $view->set('vendedores', $database->getRows("SELECT * FROM personas v LEFT JOIN vendedorespuntodeventa vp ON v.persona = vp.persona WHERE vp.puntovta = '" . $puntovta . "'  ORDER BY v.apellido ASC"));
        } else {
            $view->set('vendedores', $database->getRows("SELECT * FROM personas v LEFT JOIN vendedorespuntodeventa vp ON v.persona = vp.persona WHERE v.persona = '" . @$objeto_info['vendedor'] . "'  ORDER BY v.apellido ASC"));
        }

        $view->set('entry_total', 'TOTAL: $');
        if ($request->has('total', 'post')) {
            $view->set('total', $request->get('total', 'post'));
        } else {
            $view->set('total', 0);
        }

        $view->set('entry_totalrecargo', 'TOTAL RECARGO: $');
        if ($request->has('totalrecargo', 'post')) {
            $view->set('totalrecargo', $request->get('totalrecargo', 'post'));
        } else {
            $view->set('totalrecargo', @$objeto_info['totalrecargo']);
        }

        $view->set('entry_subtotal', 'SUB TOTAL: $');
        if ($request->has('subtotal', 'post')) {
            $view->set('subtotal', $request->get('subtotal', 'post'));
        } else {
            $view->set('subtotal', (@$objeto_info['total'] - @$objeto_info['totalrecargo']));
        }

        if ($request->has('texttotal', 'post')) {
            $view->set('texttotal', $request->get('texttotal', 'post'));
        } else {
            if (@$objeto_info['total']) {
                $view->set('texttotal', '$ ' . @$objeto_info['total']);
            } else {
                $view->set('texttotal', '$ 0.00');
            }
        }


        if ($request->has('texttotalformapago', 'post')) {
            $view->set('texttotalformapago', $request->get('texttotalformapago', 'post'));
        } else {
            if (@$objeto_info['total']) {
                $view->set('texttotalformapago', @$objeto_info['total']);
            } else {
                $view->set('texttotalformapago', '$ 0.00');
            }
        }

        //$vendedordepende = $database->getRow("SELECT concat(apellido,', ',nombre) AS apellidonombredependencia FROM personas WHERE persona = '".$persona_info['dependenciapersona']."'");
        $view->set('entry_articulo', 'Producto:');
        if ($request->has('articulo_vta', 'post')) {
            $view->set('articulo_vta', $request->get('articulo_vta', 'post'));
        } else {
            $view->set('articulo_vta', '-1');
        }

        $listaprecio = $ptovtaasignado['listaprecio'];
        $view->set('listaprecio', $listaprecio);

        $view->set('auto_articulo', $request->get('auto_articulo', 'post'));

        if ($request->has('auto_articulo_venta', 'post')) {
            $view->set('auto_articulo_venta', $request->get('auto_articulo_venta', 'post'));
        } else {
            $view->set('auto_articulo_venta', '');
        }
        $view->set('script_busca_articulo', $url->rawssl('ventasafip', 'getArticulos'));

        if ($request->has('preciovta', 'post')) {
            $view->set('preciovta', $request->get('preciovta', 'post'));
        } else {
            $view->set('preciovta', '0');
        }

        if ($request->has('codbarra', 'post')) {
            $view->set('codbarra', $request->get('codbarra', 'post'));
        } else {
            $view->set('codbarra', '');
        }

        $view->set('entry_cantidad', 'Cantidad:');
        if ($request->has('cantidad', 'post')) {
            $view->set('cantidad', $request->get('cantidad', 'post'));
        } else {
            $view->set('cantidad', '1');
        }

        $view->set('articulosasignados', $database->getRows("SELECT * FROM vw_grilla_ventaarticulos where venta = '" . $objeto_id . "'"));

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FORMAS DE PAGO">

        $view->set('entry_importe', 'TOTAL: $');
        if ($request->has('importe', 'post')) {
            $view->set('importe', $request->get('importe', 'post'));
        } else {
            $view->set('importe', @$objeto_info['total']);
        }


        if ($request->has('textimporte', 'post')) {
            $view->set('textimporte', $request->get('textimporte', 'post'));
        } else {
            if (@$objeto_info['total']) {
                $view->set('textimporte', @$objeto_info['total']);
            } else {
                $view->set('textimporte', '$ 0.00');
            }
        }

        if ($request->has('textsubtotal', 'post')) {
            $view->set('textsubtotal', $request->get('texttotal', 'post'));
        } else {
            if (@$objeto_info['total']) {
                $view->set('textsubtotal', '$ ' . (@$objeto_info['total'] - @$objeto_info['totalrecargo']));
            } else {
                $view->set('textsubtotal', '$ 0.00');
            }
        }

        if ($request->has('texttotalrecargo', 'post')) {
            $view->set('texttotalrecargo', $request->get('texttotalrecargo', 'post'));
        } else {
            if (@$objeto_info['totalrecargo']) {
                $view->set('texttotalrecargo', '$ ' . @$objeto_info['totalrecargo']);
            } else {
                $view->set('texttotalrecargo', '$ 0.00');
            }
        }

        //$view->set('entry_articulo', 'Producto:');
        if ($request->has('formapago_vta', 'post')) {
            $view->set('formapago_vta', $request->get('formapago_vta', 'post'));
        } else {
            $view->set('formapago_vta', 'EFE');
        }

        $view->set('entry_formapago', 'Forma de Pago:');
        if ($request->has('formapago', 'post')) {
            $view->set('formapago', $request->get('formapago', 'post'));
        } else {
            $view->set('formapago', 'EFE');
        }
        $view->set('formaspagos', $database->getRows("SELECT * FROM formaspago WHERE estado = 1 ORDER BY descripcion ASC"));

        $view->set('entry_financiacion', 'Financiación:');
        if ($request->has('financiacion', 'post')) {
            $view->set('financiacion', $request->get('financiacion', 'post'));
        } else {
            $view->set('financiacion', '1');
        }
        $view->set('financiaciones', $database->getRows("SELECT * FROM financiaciones WHERE estado = 'ACTIVO' ORDER BY descripcion ASC"));


        $view->set('entry_banco', 'Banco:');
        if ($request->has('banco', 'post')) {
            $view->set('banco', $request->get('banco', 'post'));
        } else {
            $view->set('banco', '-1');
        }
        $view->set('bancos', $database->getRows("SELECT * FROM bancos ORDER BY descripcion ASC"));

        $view->set('entry_tarjeta', 'Tarjeta:');
        if ($request->has('tarjeta', 'post')) {
            $view->set('tarjeta', $request->get('tarjeta', 'post'));
        } else {
            $view->set('tarjeta', '-1');
        }
        $view->set('tarjetas', $database->getRows("SELECT * FROM tarjetas ORDER BY descripcion ASC"));

        $view->set('entry_nrotarjeta', 'Nro Tarjeta:');
        if ($request->has('nrotarjeta', 'post')) {
            $view->set('nrotarjeta', $request->get('nrotarjeta', 'post'));
        } else {
            $view->set('nrotarjeta', '');
        }

        $view->set('auto_notacredito', $request->get('auto_notacredito', 'post'));

        if ($request->has('auto_notacredito_venta', 'post')) {
            $view->set('auto_notacredito_venta', $request->get('auto_notacredito_venta', 'post'));
        } else {
            $view->set('auto_notacredito_venta', '');
        }
        $view->set('script_busca_notacredito', $url->rawssl('ventasafip', 'getNotasdeCredito'));

        $view->set('entry_codseguridad', 'Código de seguridad:');
        if ($request->has('codseguridad', 'post')) {
            $view->set('codseguridad', $request->get('codseguridad', 'post'));
        } else {
            $view->set('codseguridad', '-1');
        }

        $view->set('entry_recargo', 'Recargo %:');
        if ($request->has('recargo', 'post')) {
            $view->set('recargo', $request->get('recargo', 'post'));
        } else {
            $view->set('recargo', '0.00');
        }

        $view->set('entry_cuotas', 'Cuotas:');
        if ($request->has('cuotas', 'post')) {
            $view->set('cuotas', $request->get('cuotas', 'post'));
        } else {
            $view->set('cuotas', '1');
        }

        if ($request->has('textcuotas', 'post')) {
            $view->set('textcuotas', $request->get('textcuotas', 'post'));
        } else {
            $view->set('textcuotas', '1');
        }

        $view->set('entry_importeformapago', 'Importe:');
        if ($request->has('importeformapago', 'post')) {
            $view->set('importeformapago', $request->get('importeformapago', 'post'));
        } else {
            $view->set('importeformapago', '0.00');
        }

        $view->set('ventaformasdepagoasignados', $database->getRows("SELECT * FROM vw_grilla_ventaformasdepago where venta = '" . $objeto_id . "'"));

        // <editor-fold defaultstate="collapsed" desc="IMPORTES">
        $view->set('entry_totalapagar', 'TOTAL: $');
        if ($request->has('totalapagar', 'post')) {
            $view->set('totalapagar', $request->get('totalapagar', 'post'));
        } else {
            $view->set('totalapagar', @$objeto_info['totalapagar']);
        }
        if ($request->has('texttotalapagar', 'post')) {
            $view->set('texttotalapagar', $request->get('texttotalapagar', 'post'));
        } else {
            if (@$objeto_info['totalapagar']) {
                $view->set('texttotalapagar', '$ ' . @$objeto_info['totalapagar']);
            } else {
                $view->set('texttotalapagar', '$ 0.00');
            }
        }

        $view->set('entry_totalrecargofinanciacion', 'TOTAL: $');
        if ($request->has('totalrecargofinanciacion', 'post')) {
            $view->set('totalrecargofinanciacion', $request->get('totalrecargofinanciacion', 'post'));
        } else {

            if (@$objeto_info['totalrecargofinanciacion']) {
                $view->set('totalrecargofinanciacion', @$objeto_info['totalrecargofinanciacion']);
            } else {
                $view->set('totalrecargofinanciacion', '0.00');
            }
        }
        if ($request->has('texttotalrecargofinanciacion', 'post')) {
            $view->set('texttotalrecargofinanciacion', $request->get('texttotalrecargofinanciacion', 'post'));
        } else {
            if (@$objeto_info['totalrecargofinanciacion']) {
                $view->set('texttotalrecargofinanciacion', '$ ' . @$objeto_info['totalrecargofinanciacion']);
            } else {
                $view->set('texttotalrecargofinanciacion', '$ 0.00');
            }
        }

        $view->set('entry_totalrestoapagar', 'TOTAL: $');
        if ($request->has('totalrestoapagar', 'post')) {
            $view->set('totalrestoapagar', $request->get('totalrestoapagar', 'post'));
        } else {
            $view->set('totalrestoapagar', @$objeto_info['totalrestoapagar']);
        }
        if ($request->has('texttotalrestoapagar', 'post')) {
            $view->set('texttotalrestoapagar', $request->get('texttotalrestoapagar', 'post'));
        } else {
            if (@$objeto_info['totalrestoapagar']) {
                $view->set('texttotalrestoapagar', '$ ' . @$objeto_info['totalrestoapagar']);
            } else {
                $view->set('texttotalrestoapagar', '$ 0.00');
            }
        }

        // </editor-fold>
        // </editor-fold>
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        $view->set('error_texto_error', @$this->error['texto_error']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $urlAction = $url->ssl('ventasafip', $request->get('action'), array('venta' => $request->get('venta')));
        $view->set('action', $urlAction);

        $view->set('accion_form', $request->get('action'));
        $view->set('cancel', $url->ssl('ventasafip'));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACION DE CAJA">
        // <editor-fold defaultstate="collapsed" desc="CAJA">
        //veo si tengo una caja abierta y no es de la fecha del movimiento la cierro
        //la cierro y abro otra con el saldo de la anterior
        //si esta abierta y es del dia tomo el id de la caja
        // <editor-fold defaultstate="collapsed" desc="FECHA">

        $solofecha = date('d/m/Y');
        $dated = explode('/', $solofecha);
        $fecha_d = $dated[2] . '-' . $dated[1] . '-' . $dated[0];


        // </editor-fold>
        $saldocaja = 0;
        $caja_id = '';
        $sql = "SELECT * FROM cajasdiaria WHERE estadocaja = 'ABIERTA' AND puntovta ='" . $user->getPuntovtaasignado() . "' AND STR_TO_DATE(fechaapertura, '%Y-%m-%d') = STR_TO_DATE('" . $fecha_d . "', '%Y-%m-%d')  ORDER BY cajadiaria desc LIMIT 1";
        $caja_info = $database->getRow($sql);
        if ($caja_info) {
            //$caja_id = $caja_info['cajadiaria'];
        } else {
            $sql = "SELECT * FROM cajasdiaria WHERE  puntovta ='" . $user->getPuntovtaasignado() . "'  ORDER BY cajadiaria desc LIMIT 1";
            $caja_info = $database->getRow($sql);

            $sql = "UPDATE cajasdiaria SET fechacierre=NOW(), estadocaja = 'CERRADA', au_usuario='?',au_accion='M',au_fecha_hora=NOW() WHERE estadocaja = 'ABIERTA' AND puntovta ='" . $user->getPuntovtaasignado() . "' ";
            $consulta = $database->parse($sql, $user->getPERSONA());
            $database->query($consulta);

            $sql = "INSERT IGNORE INTO cajasdiaria SET fechaapertura=now(), montoapertura = '?', estadocaja = 'ABIERTA', saldo='?',puntovta='?', au_usuario='?',au_accion='A',au_fecha_hora=NOW()";
            $consulta = $database->parse($sql, $caja_info['saldo'], $caja_info['saldo'], $user->getPuntovtaasignado(), $user->getPERSONA());
            $database->query($consulta);

            //$caja_id = $database->getLastId();
        }

        // </editor-fold>
//        if ($request->get('action') != 'consulta') {
//            $sql = "SELECT count(*) AS total FROM cajasdiaria WHERE estadocaja = 'ABIERTA' AND puntovta ='" . $puntovta . "' ";
//            $caja_valida = $database->getRow($sql);
//
//            $view->set('validaciones_caja', '');
//            if ($caja_valida['total'] == 0) {
//                $view->set('validaciones_caja', 'NO PUEDE REALIZAR LA VENTA YA QUE NO POSEE CAJA ABIERTA.');
//            } else {
//                $hoy = date("Y-m-d");
//                $sql = "SELECT count(*) AS total FROM cajasdiaria WHERE estadocaja = 'ABIERTA' AND puntovta ='" . $puntovta . "' AND STR_TO_DATE(fechaapertura, '%Y-%m-%d') = STR_TO_DATE('" . $hoy . "', '%Y-%m-%d') ";
//                $caja_valida = $database->getRow($sql);
//                if ($caja_valida['total'] == 0) {
//                    $view->set('validaciones_caja', 'NO PUEDE REALIZAR LA VENTA YA QUE LA CAJA QUE SE ENCUENTRA ABIERTA NO ES DE HOY.');
//                }
//            }
//        }
        // </editor-fold>

        
        return $view->fetch('content/venta.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $common = & $this->locator->get('common');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        //recorrer la lista de articulos y sumar las candidades de los mismos articulos
        //ver si lo que esta en stock en su punto de venta alcanza sino informar
        //ver que si es con codigo de barras y lo agrego 2 veces tiene que eliminar uno
        //validar que tenga ptovta seleccionado
        //validar que tenga un cliente seleccionado
        //validar que tenga al menos un producto
        //validar que cada producto agregado 
//        $articulos = $request->get('ventaarticulo', 'post', array());
//        foreach ($articulos as $art) {
//            
//        }

        //EL PUNTO DE VENTA SELECCIONADO TIENE QUE TENER ASIGNADO UNA POCISION FISCAL
        if ($request->get('puntovta', 'post') == '-1') {
            $errores .= 'Debe seleccionar una punto de venta. <br>';
            //$this->error['tab_general'] = '1';
        }
        else{
            $consulta_info = $database->getRow("SELECT COUNT(*) as total FROM posicionesfiscales WHERE puntovta = '" . $request->get('puntovta', 'post') . "' and tipocomprobante = '".$request->get('tipocomprobante', 'post')."' ");
            if (@$consulta_info['total'] == 0) {
            $errores .= 'El punto de venta no tiene una posición fiscal para el tipo de comprobante seleccionado. <br>';
            } 
        }

        $articulos = $request->get('articulosvta', 'post', array());
        if (count($articulos) == 0) {
            $errores .= 'Debe ingresar al menos un producto. <br>';
        }

        if ((strlen($request->get('fecha', 'post')) != 10)) {
            $errores = 'Debe ingresar una fecha. <br>';
        }

        if ($request->get('vendedor', 'post') == '-1') {
            $errores = 'Debe seleccionar un vendedor. <br>';
        }

        //SI SELECCIONO FACTURA A (FAI O FAE) EL CLIENTE NO PUEDE SER CONSUMIDOR FINAL
        
       
        
        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function validateFormUpdate() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $common = & $this->locator->get('common');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">


        if ((strlen($request->get('apellido', 'post')) < 2) || (strlen($request->get('apellido', 'post')) > 100)) {
            $this->error['apellido'] = 'Debe ingresar el apellido';
            $this->error['tab_general'] = '1';
        }

        if ((strlen($request->get('nombre', 'post')) < 2) || (strlen($request->get('nombre', 'post')) > 100)) {
            $this->error['nombre'] = 'Debe ingresar el nombre';
            $this->error['tab_general'] = '1';
        }

//		if ((strlen($request->get('domicilio', 'post')) == 0)) {
//			$this->error['domicilio'] = 'Debe ingresar el domicilio';
//                        $this->error['tab_general'] = '1';
//		}
//
//
//		if ((strlen($request->get('telefono', 'post')) == 0)) {
//			$this->error['telefono'] = 'Debe ingresar el tel&eacute;fono';
//                        $this->error['tab_general'] = '1';
//		}
//
        if ((strlen($request->get('celular', 'post')) == 0)) {
            $this->error['celular'] = 'Debe ingresar el celular';
            $this->error['tab_general'] = '1';
        }
//
//		if (!$mail->validarDuplicado($request->get('mail', 'post'),$database,$mensaje,$request->get('persona', 'post'))) {
//			$this->error['mail'] = $mensaje;
//                        $this->error['tab_general'] = '1';
//		}              
//		if (!$common->validarDuplicado($request->get('mail', 'post'),$database,$mensaje,$request->get('persona', 'post'))) {
//			$this->error['mail'] = $mensaje;
//                        $this->error['tab_general'] = '1';
//		}              
//                if ($request->get('remail', 'post') != $request->get('mail', 'post')) {
//			$this->error['remail'] = 'El mail y la confirmacion no coinciden';
//                        $this->error['tab_general'] = '1';
//                        
//		}

        if (count($request->get('puntosdevta', 'post', array())) == 0) {
            $this->error['puntovta'] = 'Debe seleccionar al menos un punto de venta';
            $this->error['tab_general'] = '1';
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $common = & $this->locator->get('common');
        //$neofactura = & $this->locator->get('neofactura');
        // </editor-fold>

        $template->set('title', 'VENTAS');

        if (($request->isPost()) && ($this->validateForm())) {
            
            $id_venta = $request->get('venta', 'post');

            //CONTROLA QUE NO SE ENCUENTRE INGRESADA LA VENTA
            $consulta_info = $database->getRow("SELECT COUNT(venta) as total FROM ventas WHERE venta = '" . $id_venta . "' ");
            if (@$consulta_info['total'] != 0) {

                $ur = $url->ssl('ventasafip');
                die($ur);
                return;
            }

            // <editor-fold defaultstate="collapsed" desc="ENCABEZADO VENTA">
            // <editor-fold defaultstate="collapsed" desc="FECHA">
            $fecha_d = '';
            if ($request->get('fecha', 'post') != '') {
                $dated = explode('/', $request->get('fecha', 'post'));
                $fecha_d = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
            } else {
                $fecha_d = NULL;
            }

            // </editor-fold>
            //$pt = $request->get('puntovtaid', 'post');
            
            $sql = "INSERT INTO ventas SET venta='?',fecha='?',tipofacturacion='?',tipocomprobante='?',personacomprador='?',puntovta='?',total='?',totalapagar='?',totalrecargofinanciacion='?',estado='OK',vendedor='?',au_usuario='?',au_accion='A',au_fecha_hora=NOW() ";
            $sql = $database->parse($sql, $id_venta, $fecha_d, $request->get('tipofacturacion', 'post'), $request->get('tipocomprobante', 'post'), $request->get('auto_personacomprador_venta', 'post'), $request->get('puntovtaid', 'post'), $request->get('total', 'post'), $request->get('totalapagar', 'post'), $request->get('totalrecargofinanciacion', 'post'), $request->get('vendedor', 'post'), $user->getPERSONA());
            $database->query($sql);
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="DETALLE DE VENTA">
            $articulos = $request->get('articulosvta', 'post', array());

            foreach ($articulos as $art) {
                $sql = "INSERT INTO ventadetalles SET venta='?', articulo='?', cantidad='?', porcDescuento='?', descuento='?', codbarra='?', preciocompra='?', preciovta='?', preciovta_descuento='?'";
                $sql = $database->parse($sql, $id_venta, $art['articulo'], $art['cantidad'], $art['porcDescuento'], $art['descuento'], $art['codbarra'], $art['precioultcompra'], $art['precioventa'], $art['precioventa_descuento']);
                $database->query($sql);

                if ($art['codbarra']) {
                    $sql = "UPDATE articulosbarra SET  estadoarticulo='ND', preciovta='?', au_usuario='?',au_accion='M',au_fecha_hora=NOW() "
                            . "WHERE articulo='?' AND codbarra='?'";
                    $sql = $database->parse($sql, $art['precioventa'], $user->getPERSONA(), $art['articulo'], $art['codbarra']);
                    $database->query($sql);
                }

                $sql = "UPDATE articulos SET cantidadstock= cantidadstock - '?',au_accion='V',au_fecha_hora=NOW() WHERE articulo='?'";
                $sql = $database->parse($sql, $art['cantidad'], $art['articulo']);
                $database->query($sql);


                $sql = "UPDATE articulospuntovta SET cantidad = cantidad - '?' WHERE articulo='?' AND puntovta = '?' ";
                $sql = $database->parse($sql, $art['cantidad'], $art['articulo'], $request->get('puntovtaid', 'post'));
                $database->query($sql);
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="CTA CTE">
            $sql = "SELECT * FROM puntosdeventa  WHERE puntovta = '?' ";
            $consulta = $database->parse($sql, $request->get('puntovtaid', 'post'));
            $obj_puntovta = $database->getRow($consulta);

            //SI ES UN LOCAL NO PROPIO SE GENERA UNA DEUDA DEL TOTAL DE LA VENTA MENOS EL PORCENTAJE ASIGNADO EN PARAMETROS
            $sql = "SELECT valor FROM parametrosconfiguracion  WHERE parametro = 'porcentajedescuentolocalnopropioaccesorios' ";
            $valor_parametro = $database->getRow($sql);

            $importe_ctate = $request->get('totalapagar', 'post');
            if ($obj_puntovta['puntovtapropio'] == '0' && $valor_parametro['valor'] != '0') {
                $valor_descuento = ($request->get('totalapagar', 'post') * $valor_parametro['valor']) / 100;
                $importe_ctate = $request->get('totalapagar', 'post') - $valor_descuento;
            }

            //SE GENERA LA CUENTA CORRIENTE
            $sql = "INSERT INTO ctascte SET ctacte='?', fecha=CURDATE(), puntovta='?',puntovtapropio='?', vendedor='?', rubro='VENTA', movimiento='D', importectacte='?', importepago=0, au_usuario='?', au_accion='A',au_fecha_hora=NOW() ";
            $sql = $database->parse($sql, $id_venta, $request->get('puntovtaid', 'post'), $obj_puntovta['puntovtapropio'], $user->getPERSONA(), $importe_ctate, $user->getPERSONA());
            $database->query($sql);
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="FORMAS DE PAGO">
            //SE REGISTRAN LAS FORMAS DE PAGO
            $ventaformasdepago = $request->get('ventaformasdepago', 'post', array());
            foreach ($ventaformasdepago as $itemformapago) {
                $financiacion = '';
                if ($itemformapago['financiacion'] != '-1') {
                    $financiacion = $itemformapago['financiacion'];
                }
                $banco = '';
                if ($itemformapago['banco'] != '-1') {
                    $banco = $itemformapago['banco'];
                }
                $tarjeta = '';
                if ($itemformapago['tarjeta'] != '-1') {
                    $tarjeta = $itemformapago['tarjeta'];
                }

                $nrotarjeta = '';
                if ($itemformapago['nrotarjeta'] != '-') {
                    $nrotarjeta = $itemformapago['nrotarjeta'];
                }
                $recargo = '';
                if ($itemformapago['recargo'] != '-1') {
                    $recargo = $itemformapago['recargo'];
                }
                $cuotas = '1';
                if ($itemformapago['cuotas'] != '-') {
                    $cuotas = $itemformapago['cuotas'];
                }

                $sql = "INSERT INTO ventaformasdepago SET venta='?', fecha=CURDATE(), formapago='?',financiacion='?', banco=NULLIF('?',''), tarjeta=NULLIF('?',''), codpagoelectronico=NULLIF('?',''),  cuotas='?', recargo=NULLIF('?','0'),importe='?',totalpago='?', au_usuario='?', au_accion='A',au_fecha_hora=NOW() ";
                $sql = $database->parse($sql, $id_venta, $itemformapago['ventaformapago'], $financiacion, $banco, $tarjeta, $nrotarjeta, $cuotas, $recargo, $itemformapago['importe'], $itemformapago['totalLFP'], $user->getPERSONA());
                $database->query($sql);

                //lo que es en efectovo genera un movimiento de caja
                if ($itemformapago['ventaformapago'] == 'EFE') {
                    //BUSCO LA CAJA ABIERTA
                    $sql = "SELECT * FROM cajasdiaria WHERE estadocaja = 'ABIERTA' AND puntovta ='" . $request->get('puntovtaid', 'post') . "' ";
                    $caja = $database->getRow($sql);

                    //Mauro agrego esto si es D resta el saldo y el tipomovimiento es DEBITO si es V suma el saldo y tipomovimiento es CREDITO
                    if ($request->get('tipofacturacion', 'post') == 'V') {
                        $sql = "INSERT IGNORE INTO cajadiariamovimientos SET cajadiaria='?', fechamovimiento=CURDATE(), importe = '?', tipomovimiento='CREDITO',rubro='2', subrubro='2',venta = '?',observacion = '?',  au_usuario='?',au_accion='A',au_fecha_hora=NOW()";
                        $consulta = $database->parse($sql, $caja['cajadiaria'], $itemformapago['totalLFP'], $id_venta, $id_venta, $user->getPERSONA());
                        $database->query($consulta);

                        //ACTUALIZO EL SALDO SEGUN EL TIPO DE MOVIMIENTO
                        $sql = "UPDATE cajasdiaria SET saldo=saldo + '?', au_usuario='?',au_accion='M',au_fecha_hora=NOW() WHERE cajadiaria = '?'";
                        $consulta = $database->parse($sql, $itemformapago['totalLFP'], $user->getPERSONA(), $caja['cajadiaria']);
                        $database->query($consulta);
                    } else if ($request->get('tipofacturacion', 'post') == 'D') {
                        $sql = "INSERT IGNORE INTO cajadiariamovimientos SET cajadiaria='?', fechamovimiento=CURDATE(), importe = '?', tipomovimiento='DEBITO',rubro='2', subrubro='2',venta = '?',observacion = '?',  au_usuario='?',au_accion='A',au_fecha_hora=NOW()";
                        $consulta = $database->parse($sql, $caja['cajadiaria'], $itemformapago['totalLFP'], $id_venta, $id_venta, $user->getPERSONA());
                        $database->query($consulta);

                        //ACTUALIZO EL SALDO SEGUN EL TIPO DE MOVIMIENTO
                        $sql = "UPDATE cajasdiaria SET saldo=saldo - '?', au_usuario='?',au_accion='M',au_fecha_hora=NOW() WHERE cajadiaria = '?'";
                        $consulta = $database->parse($sql, $itemformapago['totalLFP'], $user->getPERSONA(), $caja['cajadiaria']);
                        $database->query($consulta);
                    }
                }
                
                if ($itemformapago['ventaformapago'] == 'NC') {
                    
                        //ACTUALIZO EL ESTADO DE LA NOTA DE CREDITO
                        $sql = "UPDATE notasdecredito SET estado='NO DISPONIBLE', au_usuario='?',au_accion='M',au_fecha_hora=NOW() WHERE notacredito = '?'";
                        $consulta = $database->parse($sql,  $user->getPERSONA(), $itemformapago['nrotarjeta']);
                        $database->query($consulta);
                }
            }
            // </editor-fold>
            
            // <editor-fold defaultstate="collapsed" desc="IMPRIMIR AFIP FACTURA B">
            if ($request->get('tipocomprobante', 'post') == 'FBE') {
              
                include_once ('library/neofactura/wsfev1.php');
                include_once ('library/neofactura/wsaa.php');
                include_once ('library/neofactura/neopdf/pdf_voucher.php');

                $empresa_info = $database->getRow("SELECT e.*, ti.descripcion as desctipoiva FROM empresa e LEFT JOIN tiposiva ti ON e.tipoiva = ti.tipoiva limit 1 ");

                $CUIT = $empresa_info['cuit'];
                //const MODO_HOMOLOGACION = 0;
                //const MODO_PRODUCCION = 1;
                //$MODO = Wsaa::MODO_HOMOLOGACION;
                $MODO = Wsaa::MODO_PRODUCCION;
                //
                $crt = $empresa_info['certificado_crt'];
                //
                $key = $empresa_info['claveprivada_key'];
                
                $es = false;
                $afip = new Wsfev1($CUIT,$MODO,$crt,$key);
                $result = $afip->init();
                
                $venta = $request->has('venta', 'post');
                $error = '';
                if ($result["code"] === Wsfev1::RESULT_OK) {
                    $result = $afip->dummy();
                    if ($result["code"] === Wsfev1::RESULT_OK) {
                    // <editor-fold defaultstate="collapsed" desc="EJECUTA">    
                        // <editor-fold defaultstate="collapsed" desc="CONFIG IMPRIMIR AFIP">
                        //NEW CONFIG
                        //private $config = array();
                        $vconfig = array();
                        $vconfig["footer"] = false;
                        $vconfig["total_line"] = false;
                        $vconfig["header"] = false;
                        $vconfig["receiver"] = false;
                        $vconfig["footer"] = false;
                        $vconfig["TRADE_SOCIAL_REASON"] = $empresa_info['razonsocial'];
                        $vconfig["TRADE_CUIT"] = $CUIT;
                        $vconfig["TRADE_ADDRESS"] = $empresa_info['domiciliofiscal'];
                        $vconfig["TRADE_TAX_CONDITION"] = $empresa_info['desctipoiva'];
                        $vconfig["TRADE_INIT_ACTIVITY"] = $empresa_info['iniciosactividad'];
                        $vconfig["TYPE_CODE"] = 'codigo';
                        $vconfig["VOUCHER_OBSERVATION"] = false;
                        $vconfig["VOUCHER_CONFIG"] = false;
                        // </editor-fold>

                        // <editor-fold defaultstate="collapsed" desc="GENERAR VOUCHER"> 
                            
                            // <editor-fold defaultstate="collapsed" desc="PUNTO DE VENTA"> 
                            
                            //BUSCO EN POSICIONES FISCALES CON EL TIPO DE COMPROBANTE Y EL PUNTO DE VENTA
                            $puntovta_info = $database->getRow("SELECT * FROM posicionesfiscales where puntovta ='".$user->getPuntovtaasignado()."' AND tipocomprobante = 'FBE' ");
                            $puntovta = (int)$puntovta_info['prefijo'];
                            // </editor-fold>

                            // <editor-fold defaultstate="collapsed" desc="TIPO COMPROBANTE"> 
                            // Tipo de comprobante   TIPO_FACTURA_A = 1; TIPO_FACTURA_B = 6; TIPO_FACTURA_C = 11;
                            $tipocomprobante = 'FACTURA B';
                            $letra = 'B';
                            
                            //        001	FACTURAS A
                            //        002	NOTAS DE DEBITO A
                            //        003	NOTAS DE CREDITO A
                            //        004	RECIBOS A
                            //        005	NOTAS DE VENTA AL CONTADO A
                            //        006	FACTURAS B
                            //        007	NOTAS DE DEBITO B
                            //        008	NOTAS DE CREDITO B
                            //        009	RECIBOS B
                            //        010	NOTAS DE VENTA AL CONTADO B
                            //        011	FACTURAS C
                            //        012	NOTAS DE DEBITO C
                            //        013	NOTAS DE CREDITO C
                            $codigoTipoComprobante = '6';
                            // </editor-fold>

                            // <editor-fold defaultstate="collapsed" desc="CLIENTE">
                            // Tipo de documento del comprador 
                            // 80	CUIT
                            //86	CUIL
                            //87	CDI
                            //89	LE
                            //90	LC
                            //94	Pasaporte
                            //96	DNI

                            if ($request->get('auto_personacomprador_venta', 'post') == '0') {
                                $TipoDocumento = 'DNI';
                                $codigoTipoDocumento = '99';
                                // Numero de documento del comprador 0 para Consumidor Final (<$1000)
                                $numeroDocumento = '0';
                                $nombreCliente = 'Consumidor Final';
                                $domicilioCliente= '-';
                            }
                            else{
                                $sql = "SELECT * "
                                . "FROM vw_grilla_personas p "
                                . "WHERE (p.persona = '".$request->get('auto_personacomprador_venta', 'post')."' ) ";
                                $cliente_info = $database->getRow($sql);
                                
                                $TipoDocumento = $cliente_info['desctipodocumento'];
                                $codigoTipoDocumento = '99';
                                switch ($cliente_info['desctipodocumento']) {
                                    case 'DNI': $codigoTipoDocumento = '96';break;
                                    case 'CUIT': $codigoTipoDocumento = '80';break;
                                    case 'CUIL': $codigoTipoDocumento = '86';break;
                                    case 'PASAPORTE': $codigoTipoDocumento = '94';break;
                                    case 'LE': $codigoTipoDocumento = '89';break;
                                    case 'LC': $codigoTipoDocumento = '90';break;
                                    default:
                                        break;
                                }
                                
                                // Numero de documento del comprador 0 para Consumidor Final (<$1000)
                                $numeroDocumento = $cliente_info['persona'];
                                $nombreCliente = $cliente_info['apellidonombre'] ;
                                $domicilioCliente= $cliente_info['direccion'];
                            }
                            
                            
                            
                            // <editor-fold defaultstate="collapsed" desc="TIPO RESPONSABLE">
                            //tipo de responsable
                            //        Código	Descripción
                            //        1	IVA Responsable Inscripto
                            //        2	IVA Responsable no Inscripto
                            //        3	IVA no Responsable
                            //        4	IVA Sujeto Exento
                            //        5	Consumidor Final
                            //        6	Responsable Monotributo
                            //        7	Sujeto no Categorizado
                            //        8	Proveedor del Exterior
                            //        9	Cliente del Exterior
                            //        10	IVA Liberado – Ley Nº 19.640
                            //        11	IVA Responsable Inscripto – Agente de Percepción
                            //        12	Pequeño Contribuyente Eventual
                            //        13	Monotributista Social
                            //        14	Pequeño Contribuyente Eventual Social
                            $tipoResponsable = 'Consumidor Final';
                            if($cliente_info['desctipoiva'] != '' && $cliente_info['desctipoiva'] != '-1' && $cliente_info['desctipoiva'] !='IN') {
                                $tipoResponsable = $cliente_info['desctipoiva']; 
                            }
                            
                            // </editor-fold>
                            // </editor-fold>
                            
                            // <editor-fold defaultstate="collapsed" desc="CONDICION VENTA O FORMA DE PAGO">
                            $ventaformasdepago = $request->get('ventaformasdepago', 'post', array());
                            $descformapago = 'Contado';
                            
                            $coma = '';
                            foreach ($ventaformasdepago as $itemformapago) {
                                if ($itemformapago['descformapago'] != '-1') {
                                    $descformapago .= $coma . $itemformapago['descformapago'];
                                    $coma = ', ';
                                }
                            }
                            $CondicionVenta = $descformapago;
                            // </editor-fold>
                            
                            // <editor-fold defaultstate="collapsed" desc="UNIDADES DE MEDIDIA">
//                            00	SIN DESCRIPCION
                            //01	KILOGRAMO
                            //02	METROS
                            //03	METRO CUADRADO
                            //04	METRO CUBICO
                            //05	LITROS
                            //06	1000 KILOWATT HORA
                            //07	UNIDAD
                            //08	PAR
                            //09	DOCENA
                            //10	QUILATE
                            //11	MILLAR
                            //12	MEGA U. INTER. ACT. ANTIB
                            //13	UNIDAD INT. ACT. INMUNG
                            //14	GRAMO
                            //15	MILIMETRO
                            //16	MILIMETRO CUBICO
                            //17	KILOMETRO
                            //18	HECTOLITRO
                            //19	MEGA UNIDAD INT. ACT. INMUNG
                            //20	CENTIMETRO
                            //21	KILOGRAMO ACTIVO
                            //22	GRAMO ACTIVO
                            //23	GRAMO BASE
                            //24	UIACTHOR
                            //25	JGO.PQT. MAZO NAIPES
                            //26	MUIACTHOR
                            //27	CENTIMETRO CUBICO
                            //28	UIACTANT
                            //29	TONELADA
                            //30	DECAMETRO CUBICO
                            //31	HECTOMETRO CUBICO
                            //32	KILOMETRO CUBICO
                            //33	MICROGRAMO
                            //34	NANOGRAMO
                            //35	PICOGRAMO
                            //36	MUIACTANT
                            //37	UIACTIG
                            //41	MILIGRAMO
                            //47	MILILITRO
                            //48	CURIE
                            //49	MILICURIE
                            //50	MICROCURIE
                            //51	U.INTER. ACT. HORMONAL
                            //52	MEGA U. INTER. ACT. HOR.
                            //53	KILOGRAMO BASE
                            //54	GRUESA
                            //55	MUIACTIG
                            //61	KILOGRAMO BRUTO
                            //62	PACK
                            //63	HORMA
                            //97	SEÑAS/ANTICIPOS
                            //98	OTRAS UNIDADES
                            //99	BONIFICACION

                            $unidaddemedida = 'UN';
                            
                            // </editor-fold>
                            
                            $iva21 = 0;
                            $iva10 = 0;
                            $rows = array();
                            foreach ($articulos as $art) {
                               
                                if ($art['iva']=='21.00') {
                                    $iva21 += $art['precioventa_descuento'] / 1.21;
                                }
                                if ($art['iva']=='10.50') {
                                    $iva10 += $art['precioventa_descuento'] / 1.105;
                                }
                                
                                $rows[] = array(
                                            'scanner' 		=>$art['codbarra'],
                                            'codigo' 		=>$art['articulo'],
                                            'descripcion' 	=>$art['descripcion'],
                                            'cantidad' 		=>$art['cantidad'],
                                            'UnidadMedida' 	=>$unidaddemedida, //7 UN
                                            'precioUnitario' 	=>$art['precioventa'],
                                            'porcBonif' 	=>$art['porcDescuento'],
                                            'impBonif' 		=>$art['descuento'],
                                            'Alic'              =>3,//iva del articulo siempre
                                            'importeItem' 	=>$art['precioventa_descuento']
                                );
                                
                                
                            }
                            
                            if ($request->get('totalapagar', 'post') != '0') {
                            $baseimp = $request->get('totalapagar', 'post');
                            }
                            
                            $importeIva = 0;
                            if ($importeIva > 0) {
                            $detalleiva = array(
                                'AlicIva' => array(
                                    'codigo' => 5,
                                    'BaseImp' => $baseimp,
                                    'importe' => $renglon['importeiva']
                                )
                            );
                            }
                            // <editor-fold defaultstate="collapsed" desc="VOUCHER">
                            $voucher = array(
                                    'TipoComprobante' 		=>$tipocomprobante,     // Tipo de comprobante   TIPO_FACTURA_A = 1; TIPO_FACTURA_B = 6; TIPO_FACTURA_C = 11;
                                    'numeroComprobante'         =>1,                 // Numero de comprobante o numero del primer comprobante en caso de ser mas de uno
                                    'letra'                     =>$letra,                 // A o B    
                                    'numeroPuntoVenta' 		=>$puntovta,              // Punto de venta
                                    'fechaComprobante' 		=>intval(date('Ymd')),    // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
                                    'codigoTipoComprobante' 	=>$codigoTipoComprobante,// Tipo de comprobante (ver tipos disponibles)
                                    'codigoConcepto' 		=>1,                    // Concepto del Comprobante: (1)Productos, (2)Servicios, (3)Productos y Servicios
                                        'fechaDesde' 		=>NULL,
                                        'fechaHasta' 		=>NULL,
                                        'fechaVtoPago' 		=>NULL,
                                    'TipoDocumento' 		=>$TipoDocumento,                   // Tipo de documento del comprador 80: CUIT, 96: DNI, 99: Consumidor Final
                                    'codigoTipoDocumento'       =>$codigoTipoDocumento,                   // Tipo de documento del comprador 80: CUIT, 96: DNI, 99: Consumidor Final
                                    'numeroDocumento' 		=>$numeroDocumento,                    // Numero de documento del comprador 0 para Consumidor Final (<$1000)
                                    'nombreCliente' 		=>$nombreCliente,
                                    'tipoResponsable' 		=>$tipoResponsable,
                                    'domicilioCliente' 		=>$domicilioCliente,
                                    'CondicionVenta' 		=>$CondicionVenta,                         //formas de pago
                                    'items'                     =>$rows,
                                    'Tributos' => NULL, // array aca iria el recargo por financiacion
                                    'importeOtrosTributos' 	=>0,                // Importe neto no gravado
                                    'codigoMoneda' 		=>'PES',            //Tipo de moneda usada en el comprobante (ver tipos disponibles)('PES' para pesos argentinos) 
                                    'importeGravado'            =>0,              // Importe neto gravado
                                    'subtotivas' =>             $detalleiva,
                                    'importeIVA'                =>$importeIva,
                                    'importeNoGravado'          =>$request->get('totalapagar', 'post'),
                                    'importeExento'             =>0,
                                    'importeTotal' 		=>$request->get('totalapagar', 'post'),              // Importe total del comprobante
                                    'cae'                       =>NULL,             //SOLICITARLO
                                    'fechaVencimientoCAE' 	=>NULL,             //SOLICITARLO
                                    'cotizacionMoneda'          =>'1',                // Cotización de la moneda usada (1 para pesos argentinos)            
                                );  //$request->get('total', 'post'), 
                                //  $request->get('totalapagar', 'post'), 
                                //  $request->get('totalrecargofinanciacion', 'post')
                            // </editor-fold>
                            // </editor-fold> 
                            
                        // <editor-fold defaultstate="collapsed" desc="IMPACTO CONTRA LA AFIP">
                            //BUSCU CUAL ES EL ULTIMO NUMERO DE FACTURA
                            $Comprobante = $afip->consultarUltimoComprobanteAutorizado($voucher['numeroPuntoVenta'],$voucher['codigoTipoComprobante']);
                              
                            if ($Comprobante['code'] === Wsfev1::RESULT_OK) {
                                $numeroComprobante = $Comprobante['number'] + 1;
                                $voucher['numeroComprobante'] = $numeroComprobante;
                                
                                //PEGA CONTRA LA AFIP Y DEVUELVE EL CAE Y LA FECHA DE VENCIMIENTO
                               $cae_fechavencimientocae = $afip->emitirComprobante($voucher);
                               if($cae_fechavencimientocae['code'] === Wsfev1::RESULT_OK){
                                
                                $voucher['cae'] = $cae_fechavencimientocae['cae'];
                                $voucher['fechaVencimientoCAE'] = $cae_fechavencimientocae['fechaVencimientoCAE'];
                                        //MANDO A IMPRIMIR LA FACTURA
                                        ob_start();
                                        $logo_path ='template/default/image/logocomprobante.jpg';
                                        $pdf = new PDFVoucher($voucher, $vconfig);
                                        $pdf->emitirPDF($logo_path);
                                        //ob_end_clean();//rompimiento de pagina
                                        ob_clean();
                                        $pdf->Output("factura.pdf","D");
                                        
                                        // <editor-fold defaultstate="collapsed" desc="LOG WS">
                                        //$venta = $request->has('venta', 'post');
                                        //$venta = '2017102611322071';
                                        $nrofactura = str_pad($voucher["numeroPuntoVenta"], 4, "0", STR_PAD_LEFT) .  " - " . str_pad($voucher["numeroComprobante"], 8, "0", STR_PAD_LEFT);
                                        $sql = "INSERT INTO logwsafip SET tipocomprobante='?', nrocomprobante='?', nrooperacion='?', tipooperacion='?', descripcion='?', au_usuario='?', au_accion='A', au_fecha_hora=now()";
                                        $database->query($database->parse($sql, $tipocomprobante,$voucher['numeroComprobante'],$venta,'OK' ,'VENTA', $user->getPERSONA()));
                                        $id_log = $database->getLastId();
                                        
                                        $numfactura = str_pad($voucher["numeroPuntoVenta"], 4, "0", STR_PAD_LEFT) .  " - " . str_pad($voucher["numeroComprobante"], 8, "0", STR_PAD_LEFT);
                                        $sql = "UPDATE ventas SET logwsafip='?', nrofactura = '?', cae='?', fechavencimientocae='?' WHERE venta = '?'";
                                        $database->query($database->parse($sql,$id_log,$numfactura,$voucher['cae'],$voucher['fechaVencimientoCAE'], $venta));
                                        // </editor-fold>
                                }
                                else{
                                    $error .= $cae_fechavencimientocae["code"] . '<br>';
                                    $error .= $cae_fechavencimientocae["msg"] . '<br>';
                                }
                            }
                            else{
                                $error .= $Comprobante["code"] . '<br>';
                                $error .= $Comprobante["msg"] . '<br>';
                            }
                            
                            //guardo el log en la tabla logwsafip y en la venta
                        // </editor-fold>
                    // </editor-fold>
                    } else {
                        $error .= $result["code"] . '<br>';
                        $error .= $result["msg"] . '<br>';
                    }
                } else {
                    $error .= $result["code"] . '<br>';
                    $error .= $result["msg"] . '<br>';
                }

                if ($error != '') {
                    // <editor-fold defaultstate="collapsed" desc="LOG WS">
                    $common->log_msg($error, 'afip_log');
                    $sql = "INSERT INTO logwsafip SET tipocomprobante='?', nrocomprobante='?', nrooperacion='?', tipooperacion='?', descripcion='?', au_usuario='?', au_accion='A', au_fecha_hora=now()";
                    $database->query($database->parse($sql, 'FACTURA','NO SE GENERO',$venta,'ERROR', $error, $user->getPERSONA()));
                    $id_log = $database->getLastId();
                    
                   $sql = "UPDATE ventas SET logwsafip='?' WHERE venta = '?'";
                    $database->query($database->parse($sql,$id_log, $venta));
                    // </editor-fold>
                } 
        
            } 
        // </editor-fold>
            
            $cache->delete('ventasafip');

            $session->set('message', 'Se realizó la venta con éxito: ' . $id_venta);
            $ur = $url->ssl('ventasafip');
            die($ur);
            $response->redirect($url->ssl('ventasafip'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        $template->set('title', 'VENTAS');

        if (($request->isPost()) && ($this->validateFormUpdate())) {
            $persona_id = $request->get('persona', 'post');

            // <editor-fold defaultstate="collapsed" desc="FECHA">
            $fecha_d = '';
            if ($request->get('fechanacimiento', 'post') != '') {
                $dated = explode('/', $request->get('fechanacimiento', 'post'));
                $fecha_d = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
                $categoria = date('Y', strtotime($request->get('fechanacimiento', 'post')));
            } else {
                $fecha_d = NULL;
            }

            $fecha_i = '';

            // </editor-fold>

            if ($request->get('vendedorpropio', 'post') == 'vendedorpropio') {
                $vendedorpropio = 1;
            } else {
                $vendedorpropio = 0;
            }

            if ($request->get('comisiona', 'post') == 'comisiona') {
                $comision = 1;
            } else {
                $comision = 0;
            }
            $depende = $persona_id;
            if (strlen($request->get('auto_vendedor', 'post')) != 0) {
                $depende = $request->get('auto_vendedor_principal', 'post');
            }

            $puntosdeventa = $request->get('puntosdevta', 'post', array());

            foreach ($puntosdeventa as $puntovta) {
                $sql = "UPDATE vendedorespuntodeventa SET persona = '?', puntovta = '?', usuario = '?'";
                $database->query($database->parse($sql, $persona_id, $puntovta, $user->getPERSONA()));
            }

            $sql = "UPDATE personas SET nombre ='?', apellido ='?', fechanacimiento = NULLIF('?',''),celular ='?',  mail ='?',tipoiva='?',tipovendedor='?',zona='?',comisiona='?',dependenciapersona='?' ,au_usuario='?',au_accion='M',au_fecha_hora=NOW() WHERE persona = '?' ";
            $sql = $database->parse($sql, strtoupper($request->get('nombre', 'post')), strtoupper($request->get('apellido', 'post')), $fecha_d, $request->get('celular', 'post'), $request->get('mail', 'post'), $request->get('tipoiva', 'post'), $request->get('tipovendedor', 'post'), $request->get('zona', 'post'), $comision, $depende, $user->getPERSONA(), $persona_id);
            $database->query($sql);

            // Hago update para auditar el delete
            $sql = "UPDATE vendedorespuntodeventa SET au_usuario = '?' WHERE persona = '?'";
            $database->query($database->parse($sql, $user->getPERSONA(), $request->get('persona', 'post')));

            $sql = "DELETE FROM vendedorespuntodeventa WHERE persona = '?'";
            $database->query($database->parse($sql, $request->get('persona', 'post')));

            foreach ($puntosdeventa as $puntovta) {
                $sql = "INSERT IGNORE INTO vendedorespuntodeventa SET persona = '?', puntovta = '?'";
                $database->query($database->parse($sql, $request->get('persona', 'post'), $puntovta));
            }

            $cache->delete('ventasafip');

            $session->set('message', 'Se ha actualizado la informaci&oacute;n del vendedor');

            $response->redirect($url->ssl('ventasafip'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        $template->set('title', 'VENTAS');

        if (($request->isPost())) {

            $response->redirect($url->ssl('ventasafip'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        $template->set('title', 'VENTAS');

        //$persona = $request->get('persona');
        if (($request->get('ventasafip')) && ($this->validateDelete())) {


            $articulos = $database->getRows("SELECT * FROM ventadetalles where venta = '" . $request->get('venta', 'post') . "'");

            foreach ($articulos as $art) {

                if ($art['codbarra']) {
                    $sql = "UPDATE articulosbarra SET estadoarticulo='DI' WHERE articulo='?'";
                    $sql = $database->parse($sql, $art['articulo']);
                    $database->query($sql);
                }

                $sql = "UPDATE articulos SET cantidadstock= cantidadstock + '?' WHERE articulo='?'";
                $sql = $database->parse($sql, $art['cantidad'], $art['articulo']);
                $database->query($sql);


                $sql = "UPDATE articulospuntovta SET cantidad = cantidad + '?' WHERE articulo='?' AND puntovta";
                $sql = $database->parse($sql, $art['cantidad'], $art['articulo'], $request->get('puntovta', 'post'));
                $database->query($sql);
            }

            $sql = "UPDATE ventas SET estado='ANULADA' WHERE venta='?'";
            $sql = $database->parse($sql, $request->get('venta', 'post'));
            $database->query($sql);

            $cache->delete('ventasafip');

            //$session->set('message', 'Se ha eliminado el vendedor');
            $session->set('message', 'Se anuló la venta con éxito');

            $response->redirect($url->ssl('ventasafip'));
        }

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $config = & $this->locator->get('config');
        $language = & $this->locator->get('language');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function exportar() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $session = & $this->locator->get('session');
        // </editor-fold>

        set_time_limit(0);

        $filtro = 'Filtro:';

        $htmlTable = "<table border=0 width=180> 
			<tr><td colspan=6 align=center style=bold size=14>" . utf8_decode("xxxxxxxxx xxxxxxxxx xxxxxxx") . "</td></tr>
			<tr><td colspan=6 align=center style=bold size=12>SISTEMA DE xxxxxxxxxxx xxxxxxxxx</td></tr>
			<tr><td colspan=6 align=center style=bold size=10>Listado de personas</td></tr>
			<tr><td colspan=6 align=center></td></tr>
			<tr></tr>";

        if (!$session->get('ventasafip.persona') && !$session->get('ventasafip.nombre')) {
            $sql = "SELECT p.persona, p.nombre, p.domicilio, l.descripcion,  p.celular, p.mail FROM personas p LEFT JOIN localidades l ON p.localidad = l.localidad";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT p.persona, p.nombre, p.domicilio, l.descripcion,  p.celular, p.mail FROM personas p LEFT JOIN localidades l ON p.localidad = l.localidad WHERE p.persona LIKE '%" . $session->get('ventasafip.persona') . "%' AND p.nombre LIKE '%" . $session->get('ventasafip.nombre') . "%'";
            $conca = " AND ";
        }

        if ($session->get('ventasafip.localidad') != '-1' && $session->get('ventasafip.localidad') != '') {
            $sql .= $conca . "p.localidad = '" . $session->get('ventasafip.localidad') . "'";
            $conca = " AND ";
        }

        $sql .= " ORDER BY persona ASC";

        $reporte = $database->getRows($sql);

        foreach ($reporte as $estu) {

            $htmlTable .= "<tr><td colspan=6>______________________________________________________________________________________________________________</td></tr>
                                        <tr>
                                                <td align=left size=9>Documento</td>
                                                <td align=left size=9>" . utf8_decode($estu['persona']) . "</td>
                                        </tr>
                                        <tr>
                                                <td align=left size=9>Nombre</td>	
                                                <td align=left size=9 colspan= 5>" . utf8_decode($estu['nombre']) . "</td>
                                        </tr>
                                        <tr>
                                                <td align=left size=9>Domicilio<td>
                                                <td align=left size=9 colspan=2>" . utf8_decode($estu['domicilio']) . "</td>
                                                <td align=left size=9>Localidad</td>
                                                <td align=left size=9>" . utf8_decode($estu['descripcion']) . "</td>
                                        </tr>
                                        <tr>
                                                
                                                <td align=left size=9>Celular:</td>
                                                <td align=left size=9>" . utf8_decode($estu['celular']) . "</td>
                                                <td align=left size=9></td>
                                                <td align=left size=9></td>
                                        </tr>
                                        <tr>	
                                                <td align=left size=9>Email</td>
                                                <td align=left size=9 colspan=5>" . utf8_decode($estu['mail']) . "</td>
                                        </tr>";
        }

        $htmlTable .= "</table>";

        define('FPDF_FONTPATH', 'font/');
        require('library/pdf/pdftable.inc.php');
        //ob_end_clean();

        $p = new PDFTable();
        $p->AliasNbPages();
        $p->AddPage();
        $p->setfont('times', '', 6);
        $p->htmltable($htmlTable);
        $p->output('', 'I');
    }

    function reprocesarComprobanteA($venta) {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        // </editor-fold>

        set_time_limit(0);

        if ($request->has('venta', 'post')) {
            $venta_id = $request->get('venta', 'post');
        } else {
            if ($request->has('venta', 'get')) {
                $venta_id = $request->get('venta', 'get');
            }
        }


        $consulta = "SELECT DISTINCT * FROM vw_grilla_ventas WHERE venta = '?'";
        $venta_info = $database->getRow($database->parse($consulta, $venta_id));

        $consult = "SELECT * FROM vw_grilla_ventaarticulos WHERE venta='" . $venta_id . "' ORDER BY  articulo ASC";
        $detalleventa = $database->getRows($consult);

        // <editor-fold defaultstate="collapsed" desc="config PDF">
        // Include the main TCPDF library (search for installation path).
        // Include the main TCPDF library (search for installation path).
        define('FPDF_FONTPATH', 'font/');
        require('library/pdf/tcpdf/tcpdf.php');
        require_once('library/pdf/tcpdf/tcpdf_include.php');

        $con = PDF_PAGE_ORIENTATION;
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF8 sin BOM', false);


        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('STC');
        $pdf->SetTitle('');
        $pdf->SetSubject(' ');
        $pdf->SetKeywords(' ');

        // set default header data
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        //LA IMAGEN DEBE ESTAR EN template\default\image
        // $pdf->SetHeaderData('cabecera PDF-02.jpg', 173, '', '');
        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font - conjunto predeterminado fuentemonoespaciada
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetMargins(20, 10, 20);
        //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetHeaderMargin(5);
        $pdf->SetFooterMargin(15);

        // set auto page breaks - establecer saltos de página automático
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // establecemos la medida del interlineado
        //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        //$pdf->SetDefaultMonospacedFont(30);
        // ---------------------------------------------------------
        // set font
        $pdf->SetFont('helvetica', '', 9);


        //$pdf->Image('images/sistema.png', 50, 50, 100, '', '', 'http://www.tcpdf.org', '', false, 300);
// </editor-fold>  
        // add a page
        $pdf->AddPage();

        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">
        // $html = '<div style="text-align:center; " ><h1><b>UNIÓN DE RUGBY ARGENTINO</b></h1><br></div>';
        //$pdf->writeHTML($html, true, false, true, false, '');
        //$html = '<div style="text-align:center; font-size: 13pt;" ><b>CONSTANCIA DE ORDEN DE REPARACIÓN</b><br></div>';
        //$pdf->writeHTML($html, true, false, true, false, '');
//            $html = '<div style="text-align:center; font-size: 11pt;" ><br><b>ORDEN NRO: '.$reparacion_id.' </b><br></div>';
//            $pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO REMITO">
        // define barcode style
        $style = array(
            'position' => '',
            'align' => 'C',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

        // PRINT VARIOUS 1D BARCODES
        // CODE 39 EXTENDED + CHECKSUM
        //$pdf->Cell(0, 240, 'NÚMERO DE ORDEN', 0, 1);
        //public function write1DBarcode($code, $type, $x='', $y='', $w='', $h='', $xres='', $style='', $align='')
        //$pdf->write1DBarcode($reparacion_id, 'S25', '', '242', '', 18, 0.4, $style, 'N');
        //$pdf->Cell(110, 0, 'NÚMERO DE ORDEN', 0, 1);
        // Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)
        $pdf->Image('template/default/image/Remito.jpg', 95, 10, 20, 30, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
        $pdf->Image('template/default/image/Insight.jpg', 25, 10, 45, 20, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
        //$pdf->Text(55, 260, 'NÚMERO DE ORDEN');

        $fecha = date('d/m/Y', strtotime($venta_info['fecha']));
        $ptovta = $venta_info['descpuntovta'];
        $vendedorNombre = $venta_info['nombreApellidoVendedor'];

        $html = <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    <td width="100" style="text-align:left;"><b></b></td>
                    <td width="300" style="text-align:right;"><b></b></td>
                    <td width="300" style="text-align:left;" ><b></b></td>
                    
               </tr> 
                <tr >
                    <td width="100" style="text-align:left;" ><b></b></td>
                    <td width="300" style="text-align:right;"><b></b></td>
                    <td width="300" style="text-align:left;" ><b>REMITO NRO: $venta_id</b></td>
               </tr> 
                <tr >
                    <td width="100" style="text-align:left;" ><b></b></td>
                    <td width="300" style="text-align:right;"><b></b></td>
                    <td width="300" style="text-align:left;" ><b>FECHA: $fecha</b></td>
                    
               </tr>
            </table>
EOF;


        $pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="CLIENTE EQUIPO">
        //$html = '<div style="text-align:center; font-size: 11pt;" ><b>Datos del Cliente</b><hr /></div>';
        $html = <<<EOF
                 <br> <br><br><br><hr> 
EOF;
        $pdf->writeHTML($html, true, false, true, false, '');
        // <editor-fold defaultstate="collapsed" desc="STYLE">
        $html = <<<EOF
                <!-- EXAMPLE OF CSS STYLE -->
                <style>
                    /*
                    table.first {
                        color: black;
                        font-family: helvetica;
                        font-size: 8pt;
                        background-color: white;
                    }
                    td {
                        border: 0px solid black;
                        background-color: white;
                    }
                    */
                </style>
EOF;
        //$pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>

        $consulta = "SELECT DISTINCT * FROM vw_grilla_personas WHERE persona = '?'";
        $comprador_info = $database->getRow($database->parse($consulta, $venta_info['personacomprador']));

        $ducumento = $venta_info['personacomprador'];
        $apellidonombrecliente = $venta_info['nombreApellidoComprador'];
        $telefonocliente = $comprador_info['celular'];
        $mailcliente = $comprador_info['mail'];

//        $nroserie = $reparacion_info['nroserie'];
//        $tipoproducto = $reparacion_info['desctipoproducto'];
//        $modelo = $reparacion_info['descmarca'] . ' - ' . $reparacion_info['descmodelo'];
//        $engarantia = No;
//        if ($reparacion_info['engarantia'] == 1) {
//            $engarantia = Si;
//        }

        $html .= <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    <td width="300" style="text-align:left;"><b>DOCUMENTO: $ducumento</b></td>
                    <td width="50" style="text-align:left;" ></td>
                    <td width="300" style="text-align:left;" ><b>PTO VTA: $ptovta</b></td>
               </tr> 
                <tr >
                    <td width="300" style="text-align:left;"><b>APELLIDO Y NOMBRE: $apellidonombrecliente</b></td>
                    <td width="50" style="text-align:left;" ></td>
                    <td width="300" style="text-align:left;" ><b>VENDEDOR: $vendedorNombre</b></td>
               </tr>
                <tr >
                    <td width="300" style="text-align:left;"><b>TELÉFONO: $telefonocliente</b></td>
                    <td width="50" style="text-align:left;" ></td>
                    <td width="300" style="text-align:left;" ><b> </b></td>
               </tr>
                <tr >
                    <td width="300" style="text-align:left;"><b>MAIL: $mailcliente</b></td>
                    <td width="50" style="text-align:left;" ></td>
                    <td width="300" style="text-align:left;" ><b> </b></td>
               </tr>
                
            </table>
                <br><hr>
EOF;


        $pdf->writeHTML($html, true, false, true, false, '');

        // </editor-fold>
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="HARD SOFT">
        $html = '<div style="text-align:center; font-size: 11pt;" ><b>DETALLE</b><hr /></div>';
        $html .= <<<EOF
                   <br>
EOF;
        $pdf->writeHTML($html, true, false, true, false, '');
        // <editor-fold defaultstate="collapsed" desc="STYLE">
        $html = <<<EOF
                <!-- EXAMPLE OF CSS STYLE -->
                <style>
                    /*
                    table.first {
                        color: black;
                        font-family: helvetica;
                        font-size: 8pt;
                        background-color: white;
                    }
                    td {
                        border: 0px solid black;
                        background-color: white;
                    }
                    */
                </style>
EOF;
        //$pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>
        //lista de repuestos reparacion




        $html .= <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    <td width="80" style="text-align:left;"><b>Artículo</b></td>
                    <td width="240" style="text-align:left;"><b>Descripción</b></td>
                    <td width="60" style="text-align:center;"><b>Cantidad</b></td>
                    <td width="70" style="text-align:center;"><b>Precio U.</b></td>
                    <td width="70" style="text-align:center;"><b>Descuento</b></td>
                    <td width="80" style="text-align:right;"><b>Subtotal</b></td>
               </tr> 
                
            </table>
                <hr />
EOF;

        foreach ($detalleventa as $detalle) {
            $articulo = $detalle['codbarrainterno'];
            $descripcion = $detalle['descmodelo'];
            $cantidad = $detalle['cantidad'];
            $preciovta = $detalle['preciovta'];
            $descuento = $detalle['descuento'];
            $subtotal = ($preciovta * $cantidad) - $descuento;

            $html .= <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    <td width="80" style="text-align:left;">$articulo</td>
                    <td width="240" style="text-align:left;">$descripcion</td>
                    <td width="60" style="text-align:center;">$cantidad</td>
                    <td width="70" style="text-align:center;">$ $preciovta</td>
                    <td width="70" style="text-align:center;">$ $descuento</td>
                    <td width="80" style="text-align:right;">$ $subtotal</td>
               </tr> 
                
            </table>
EOF;
        }

        $pdf->writeHTML($html, true, false, true, false, '');

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SELLO">
        // Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)
        //  $pdf->Image('template/default/image/sello-entregado-2.png', 130, 180, 60, 50, 'png', '', '', true, 150, '', false, false, 0, false, false, false);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PIE">
        //$html = '<div style="text-align:center; font-size: 11pt;" ><b>Datos del Cliente</b><hr /></div>';
        $html = <<<EOF
                 <br> <br><br><br><hr> 
EOF;
        $pdf->writeHTML($html, true, false, true, false, '');
        // <editor-fold defaultstate="collapsed" desc="STYLE">
        $html = <<<EOF
                <!-- EXAMPLE OF CSS STYLE -->
                <style>
                    /*
                    table.first {
                        color: black;
                        font-family: helvetica;
                        font-size: 8pt;
                        background-color: white;
                    }
                    td {
                        border: 0px solid black;
                        background-color: white;
                    }
                    */
                </style>
EOF;
        //$pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>

        $total = $venta_info['total'];
        $html = <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    
                    <td width="600" style="text-align:center;" ><b>TOTAL $ $total</b></td>
               </tr> 
                
                
            </table>
                <br><hr>
EOF;


        $pdf->writeHTML($html, true, false, true, false, '');

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PIE">
        $pdf->Text(40, 255, '_____________________________________');
        $pdf->Text(45, 260, 'FIRMA Y ACLARACION DEL CLIENTE');
        // </editor-fold>
        $pdf->setPrintHeader(false);
        //ESTO HACE QUE ANDE EN EL SERVER DE LA DNNA
        ob_end_clean();
        //Close and output PDF document
        //D obliga la descarga
        //I abre en una ventana nueva
        $nombrePDF = 'RemitoNro' . $venta_id . '.pdf';
        $pdf->Output($nombrePDF, 'I');

        //$cache->delete('checkins');
        //die('ok');
        //============================================================+
        // END OF FILE
        //============================================================+
    }
    
     function reprocesarComprobanteB($venta) {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $user = & $this->locator->get('user');
        $common = & $this->locator->get('common');
        // </editor-fold>

        set_time_limit(0);

      
        $venta_id = $request->get('venta', 'get');
            
        $consulta = "SELECT DISTINCT * FROM vw_grilla_ventas WHERE venta = '?'";
        $venta_info = $database->getRow($database->parse($consulta, $venta_id));

        $consulta = "SELECT  * FROM vw_grilla_ventaarticulos WHERE venta = '?' ORDER BY  articulo ASC";
        $detalleventa = $database->getRows($database->parse($consulta, $venta_id));
        
        $consulta = "SELECT  * FROM vw_grilla_ventaformasdepago WHERE venta = '?'";
        $detalleformapago = $database->getRows($database->parse($consulta, $venta_id));
              

         // <editor-fold defaultstate="collapsed" desc="IMPRIMIR AFIP FACTURA B">
             
                               
                // <editor-fold defaultstate="collapsed" desc="EJECUTA">    
                        
                        
                            
                       // <editor-fold defaultstate="collapsed" desc="IMPRIMIR AFIP FACTURA B">
            
                include_once ('library/neofactura/wsfev1.php');
                include_once ('library/neofactura/wsaa.php');
                include_once ('library/neofactura/neopdf/pdf_voucher.php');

                $empresa_info = $database->getRow("SELECT e.*, ti.descripcion as desctipoiva FROM empresa e LEFT JOIN tiposiva ti ON e.tipoiva = ti.tipoiva limit 1 ");

                $CUIT = $empresa_info['cuit'];
                //const MODO_HOMOLOGACION = 0;
                //const MODO_PRODUCCION = 1;
                //$MODO = Wsaa::MODO_HOMOLOGACION;
                $MODO = Wsaa::MODO_PRODUCCION;
                //
                $crt = $empresa_info['certificado_crt'];
                //
                $key = $empresa_info['claveprivada_key'];
                
                $es = false;
                $afip = new Wsfev1($CUIT,$MODO,$crt,$key);
                $result = $afip->init();
                
                $venta = $request->has('venta', 'post');
                $error = '';
                if ($result["code"] === Wsfev1::RESULT_OK) {
                    $result = $afip->dummy();
                    if ($result["code"] === Wsfev1::RESULT_OK) {
                    // <editor-fold defaultstate="collapsed" desc="EJECUTA">    
                        // <editor-fold defaultstate="collapsed" desc="CONFIG IMPRIMIR AFIP">
                        //NEW CONFIG
                        //private $config = array();
                        $vconfig = array();
                        $vconfig["footer"] = false;
                        $vconfig["total_line"] = false;
                        $vconfig["header"] = false;
                        $vconfig["receiver"] = false;
                        $vconfig["footer"] = false;
                        $vconfig["TRADE_SOCIAL_REASON"] = $empresa_info['razonsocial'];
                        $vconfig["TRADE_CUIT"] = $CUIT;
                        $vconfig["TRADE_ADDRESS"] = $empresa_info['domiciliofiscal'];
                        $vconfig["TRADE_TAX_CONDITION"] = $empresa_info['desctipoiva'];
                        $vconfig["TRADE_INIT_ACTIVITY"] = $empresa_info['iniciosactividad'];
                        $vconfig["TYPE_CODE"] = 'codigo';
                        $vconfig["VOUCHER_OBSERVATION"] = false;
                        $vconfig["VOUCHER_CONFIG"] = false;
                        // </editor-fold>

                        // <editor-fold defaultstate="collapsed" desc="GENERAR VOUCHER"> 
                            
                            // <editor-fold defaultstate="collapsed" desc="PUNTO DE VENTA"> 
                            
                            //BUSCO EN POSICIONES FISCALES CON EL TIPO DE COMPROBANTE Y EL PUNTO DE VENTA
                            $puntovta_info = $database->getRow("SELECT * FROM posicionesfiscales where puntovta ='".$venta_info['puntovta']."' AND tipocomprobante = 'FBE' ");
                            $puntovta = (int)$puntovta_info['prefijo'];
                            // </editor-fold>

                            // <editor-fold defaultstate="collapsed" desc="TIPO COMPROBANTE"> 
                            // Tipo de comprobante   TIPO_FACTURA_A = 1; TIPO_FACTURA_B = 6; TIPO_FACTURA_C = 11;
                            $tipocomprobante = 'FACTURA B';
                            $letra = 'B';
                            
                            //        001	FACTURAS A
                            //        002	NOTAS DE DEBITO A
                            //        003	NOTAS DE CREDITO A
                            //        004	RECIBOS A
                            //        005	NOTAS DE VENTA AL CONTADO A
                            //        006	FACTURAS B
                            //        007	NOTAS DE DEBITO B
                            //        008	NOTAS DE CREDITO B
                            //        009	RECIBOS B
                            //        010	NOTAS DE VENTA AL CONTADO B
                            //        011	FACTURAS C
                            //        012	NOTAS DE DEBITO C
                            //        013	NOTAS DE CREDITO C
                            $codigoTipoComprobante = '6';
                            // </editor-fold>

                            // <editor-fold defaultstate="collapsed" desc="CLIENTE">
                            // Tipo de documento del comprador 
                            // 80	CUIT
                            //86	CUIL
                            //87	CDI
                            //89	LE
                            //90	LC
                            //94	Pasaporte
                            //96	DNI

                            if ($venta_info['personacomprador'] == '0') {
                                $TipoDocumento = 'DNI';
                                $codigoTipoDocumento = '99';
                                // Numero de documento del comprador 0 para Consumidor Final (<$1000)
                                $numeroDocumento = '0';
                                $nombreCliente = 'Consumidor Final';
                                $domicilioCliente= '-';
                            }
                            else{
                                $sql = "SELECT * "
                                . "FROM vw_grilla_personas p "
                                . "WHERE (p.persona = '".$venta_info['personacomprador']."' ) ";
                                $cliente_info = $database->getRow($sql);
                                
                                $TipoDocumento = $cliente_info['desctipodocumento'];
                                $codigoTipoDocumento = '99';
                                switch ($cliente_info['desctipodocumento']) {
                                    case 'DNI': $codigoTipoDocumento = '96';break;
                                    case 'CUIT': $codigoTipoDocumento = '80';break;
                                    case 'CUIL': $codigoTipoDocumento = '86';break;
                                    case 'PASAPORTE': $codigoTipoDocumento = '94';break;
                                    case 'LE': $codigoTipoDocumento = '89';break;
                                    case 'LC': $codigoTipoDocumento = '90';break;
                                    default:
                                        break;
                                }
                                
                                // Numero de documento del comprador 0 para Consumidor Final (<$1000)
                                $numeroDocumento = $cliente_info['persona'];
                                $nombreCliente = $cliente_info['apellidonombre'] ;
                                $domicilioCliente= $cliente_info['direccion'];
                            }
                            
                            
                            
                            // <editor-fold defaultstate="collapsed" desc="TIPO RESPONSABLE">
                            //tipo de responsable
                            //        Código	Descripción
                            //        1	IVA Responsable Inscripto
                            //        2	IVA Responsable no Inscripto
                            //        3	IVA no Responsable
                            //        4	IVA Sujeto Exento
                            //        5	Consumidor Final
                            //        6	Responsable Monotributo
                            //        7	Sujeto no Categorizado
                            //        8	Proveedor del Exterior
                            //        9	Cliente del Exterior
                            //        10	IVA Liberado – Ley Nº 19.640
                            //        11	IVA Responsable Inscripto – Agente de Percepción
                            //        12	Pequeño Contribuyente Eventual
                            //        13	Monotributista Social
                            //        14	Pequeño Contribuyente Eventual Social
                            $tipoResponsable = 'Consumidor Final';
                            if($cliente_info['desctipoiva'] != '' && $cliente_info['desctipoiva'] != '-1' && $cliente_info['desctipoiva'] !='IN') {
                                $tipoResponsable = $cliente_info['desctipoiva']; 
                            }
                            
                            // </editor-fold>
                            // </editor-fold>
                            
                            // <editor-fold defaultstate="collapsed" desc="CONDICION VENTA O FORMA DE PAGO">
                            
                            $descformapago = 'Contado';
                            $coma = '';
                            foreach ($detalleformapago as $itemformapago) {
                                if ($itemformapago['descformapago'] != '-1') {
                                    $descformapago .= $coma . $itemformapago['descformapago'];
                                    $coma = ', ';
                                }
                            }
                            $CondicionVenta = $descformapago;
                            // </editor-fold>
                            
                            // <editor-fold defaultstate="collapsed" desc="UNIDADES DE MEDIDIA">
//                            00	SIN DESCRIPCION
                            //01	KILOGRAMO
                            //02	METROS
                            //03	METRO CUADRADO
                            //04	METRO CUBICO
                            //05	LITROS
                            //06	1000 KILOWATT HORA
                            //07	UNIDAD
                            //08	PAR
                            //09	DOCENA
                            //10	QUILATE
                            //11	MILLAR
                            //12	MEGA U. INTER. ACT. ANTIB
                            //13	UNIDAD INT. ACT. INMUNG
                            //14	GRAMO
                            //15	MILIMETRO
                            //16	MILIMETRO CUBICO
                            //17	KILOMETRO
                            //18	HECTOLITRO
                            //19	MEGA UNIDAD INT. ACT. INMUNG
                            //20	CENTIMETRO
                            //21	KILOGRAMO ACTIVO
                            //22	GRAMO ACTIVO
                            //23	GRAMO BASE
                            //24	UIACTHOR
                            //25	JGO.PQT. MAZO NAIPES
                            //26	MUIACTHOR
                            //27	CENTIMETRO CUBICO
                            //28	UIACTANT
                            //29	TONELADA
                            //30	DECAMETRO CUBICO
                            //31	HECTOMETRO CUBICO
                            //32	KILOMETRO CUBICO
                            //33	MICROGRAMO
                            //34	NANOGRAMO
                            //35	PICOGRAMO
                            //36	MUIACTANT
                            //37	UIACTIG
                            //41	MILIGRAMO
                            //47	MILILITRO
                            //48	CURIE
                            //49	MILICURIE
                            //50	MICROCURIE
                            //51	U.INTER. ACT. HORMONAL
                            //52	MEGA U. INTER. ACT. HOR.
                            //53	KILOGRAMO BASE
                            //54	GRUESA
                            //55	MUIACTIG
                            //61	KILOGRAMO BRUTO
                            //62	PACK
                            //63	HORMA
                            //97	SEÑAS/ANTICIPOS
                            //98	OTRAS UNIDADES
                            //99	BONIFICACION

                            $unidaddemedida = 'UN';
                            
                            // </editor-fold>
                            
                            $rows = array();
                            foreach ($detalleventa as $art) {
                               //$cell = array();
                                $rows[] = array(
                                            'scanner' 		=>$art['codbarra'],
                                            'codigo' 		=>$art['articulo'],
                                            'descripcion' 	=>$art['descmodelo'],
                                            'cantidad' 		=>$art['cantidad'],
                                            'UnidadMedida' 	=>$unidaddemedida, //7 UN
                                            'precioUnitario' 	=>$art['preciovta'],
                                            'porcBonif' 	=>$art['porcDescuento'],
                                            'impBonif' 		=>$art['descuento'],
                                            'Alic'              =>3,
                                            'importeItem' 	=>$art['preciovta_descuento'] * $art['cantidad']
                                );
                                //$rows[] = array($cell);
                            }
                            //si hay un recargo por financiacion se le agrega un item
                            if ($venta_info['totalrecargofinanciacion'] > 0) {
                                //$cell = array();
                                $rows[] = array(
                                            'scanner' 		=>0,
                                            'codigo' 		=>0,
                                            'descripcion' 	=>'Recargo por financiación',
                                            'cantidad' 		=>1,
                                            'UnidadMedida' 	=>$unidaddemedida, //7 UN
                                            'precioUnitario' 	=>$venta_info['totalrecargofinanciacion'],
                                            'porcBonif' 	=>0,
                                            'impBonif' 		=>0,
                                            'Alic'              =>3,
                                            'importeItem' 	=>$venta_info['totalrecargofinanciacion']
                                );
                                //$rows[] = array($cell);
                            }
                            
                            // <editor-fold defaultstate="collapsed" desc="VOUCHER">
                            $voucher = array(
                                    'TipoComprobante' 		=>$tipocomprobante,     // Tipo de comprobante   TIPO_FACTURA_A = 1; TIPO_FACTURA_B = 6; TIPO_FACTURA_C = 11;
                                    'numeroComprobante'         =>1,                 // Numero de comprobante o numero del primer comprobante en caso de ser mas de uno
                                    'letra'                     =>$letra,                 // A o B    
                                    'numeroPuntoVenta' 		=>$puntovta,              // Punto de venta
                                    'fechaComprobante' 		=>intval(date('Ymd')),    // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
                                    'codigoTipoComprobante' 	=>$codigoTipoComprobante,// Tipo de comprobante (ver tipos disponibles)
                                    'codigoConcepto' 		=>1,                    // Concepto del Comprobante: (1)Productos, (2)Servicios, (3)Productos y Servicios
                                        'fechaDesde' 		=>NULL,
                                        'fechaHasta' 		=>NULL,
                                        'fechaVtoPago' 		=>NULL,
                                    'TipoDocumento' 		=>$TipoDocumento,                   // Tipo de documento del comprador 80: CUIT, 96: DNI, 99: Consumidor Final
                                    'codigoTipoDocumento'       =>$codigoTipoDocumento,                   // Tipo de documento del comprador 80: CUIT, 96: DNI, 99: Consumidor Final
                                    'numeroDocumento' 		=>$numeroDocumento,                    // Numero de documento del comprador 0 para Consumidor Final (<$1000)
                                    'nombreCliente' 		=>$nombreCliente,
                                    'tipoResponsable' 		=>$tipoResponsable,
                                    'domicilioCliente' 		=>$domicilioCliente,
                                    'CondicionVenta' 		=>$CondicionVenta,                         //formas de pago
                                    'items'                     =>$rows,
                                    'Tributos' => NULL, // array aca iria el recargo por financiacion
                                    'importeOtrosTributos' 	=>0,                // Importe neto no gravado
                                    'codigoMoneda' 		=>'PES',            //Tipo de moneda usada en el comprobante (ver tipos disponibles)('PES' para pesos argentinos) 
                                    'importeGravado'            =>0,              // Importe neto gravado
                                    'subtotivas' => array ( array (         // array Importe total de IVA
                                                            'codigo'    =>3,//3 es 0% de iva
                                                            'BaseImp' 	=>0, //aca que va el total de la factura
                                                            'importe' 	=>0 //va 0 porque es B y no se declara iva
                                    )),             
                                    'importeIVA'                =>0,
                                    'importeNoGravado'          =>$venta_info['totalapagar'],
                                    'importeExento'             =>0,
                                    'importeTotal' 		=>$venta_info['totalapagar'],              // Importe total del comprobante
                                    'cae'                       =>NULL,             //SOLICITARLO
                                    'fechaVencimientoCAE' 	=>NULL,             //SOLICITARLO
                                    'cotizacionMoneda'          =>'1',                // Cotización de la moneda usada (1 para pesos argentinos)            
                                );  //$request->get('total', 'post'), 
                                //  $request->get('totalapagar', 'post'), 
                                //  $request->get('totalrecargofinanciacion', 'post')
                            // </editor-fold>
                            // </editor-fold> 
                                                        
                        // <editor-fold defaultstate="collapsed" desc="IMPACTO CONTRA LA AFIP">
                            //BUSCU CUAL ES EL ULTIMO NUMERO DE FACTURA
                            $Comprobante = $afip->consultarUltimoComprobanteAutorizado($voucher['numeroPuntoVenta'],$voucher['codigoTipoComprobante']);
                              
                            if ($Comprobante['code'] === Wsfev1::RESULT_OK) {
                                $numeroComprobante = $Comprobante['number'] + 1;
                                $voucher['numeroComprobante'] = $numeroComprobante;
                                
                                //PEGA CONTRA LA AFIP Y DEVUELVE EL CAE Y LA FECHA DE VENCIMIENTO
                               $cae_fechavencimientocae = $afip->emitirComprobante($voucher);
                               if($cae_fechavencimientocae['code'] === Wsfev1::RESULT_OK){
                                
                                $voucher['cae'] = $cae_fechavencimientocae['cae'];
                                $voucher['fechaVencimientoCAE'] = $cae_fechavencimientocae['fechaVencimientoCAE'];
                                        //MANDO A IMPRIMIR LA FACTURA
                                        ob_start();
                                        $logo_path ='template/default/image/logocomprobante.jpg';
                                        $pdf = new PDFVoucher($voucher, $vconfig);
                                        $pdf->emitirPDF($logo_path);
                                        //ob_end_clean();//rompimiento de pagina
                                        ob_clean();
                                        $pdf->Output("factura.pdf","D");
                                        
                                        // <editor-fold defaultstate="collapsed" desc="LOG WS">
                                        //$venta = $request->has('venta', 'post');
                                        //$venta = '2017102611322071';
                                        $nrofactura = str_pad($voucher["numeroPuntoVenta"], 4, "0", STR_PAD_LEFT) .  " - " . str_pad($voucher["numeroComprobante"], 8, "0", STR_PAD_LEFT);
                                        $sql = "INSERT INTO logwsafip SET tipocomprobante='?', nrocomprobante='?', nrooperacion='?', tipooperacion='?', descripcion='?', au_usuario='?', au_accion='A', au_fecha_hora=now()";
                                        $database->query($database->parse($sql, $tipocomprobante,$voucher['numeroComprobante'],$venta,'OK' ,'VENTA', $user->getPERSONA()));
                                        $id_log = $database->getLastId();
                                        
                                        $numfactura = str_pad($voucher["numeroPuntoVenta"], 4, "0", STR_PAD_LEFT) .  " - " . str_pad($voucher["numeroComprobante"], 8, "0", STR_PAD_LEFT);
                                        $sql = "UPDATE ventas SET logwsafip='?', nrofactura = '?', cae='?', fechavencimientocae='?' WHERE venta = '?'";
                                        $database->query($database->parse($sql,$id_log,$numfactura,$voucher['cae'],$voucher['fechaVencimientoCAE'], $venta));
                                        // </editor-fold>
                                }
                                else{
                                    $error .= $cae_fechavencimientocae["code"] . '<br>';
                                    $error .= $cae_fechavencimientocae["msg"] . '<br>';
                                }
                            }
                            else{
                                $error .= $Comprobante["code"] . '<br>';
                                $error .= $Comprobante["msg"] . '<br>';
                            }
                            
                            //guardo el log en la tabla logwsafip y en la venta
                        // </editor-fold>
                    // </editor-fold>
                    } else {
                        $error .= $result["code"] . '<br>';
                        $error .= $result["msg"] . '<br>';
                    }
                } else {
                    $error .= $result["code"] . '<br>';
                    $error .= $result["msg"] . '<br>';
                }

                if ($error != '') {
                    // <editor-fold defaultstate="collapsed" desc="LOG WS">
                    $common->log_msg($error, 'afip_log');
                    $sql = "INSERT INTO logwsafip SET tipocomprobante='?', nrocomprobante='?', nrooperacion='?', tipooperacion='?', descripcion='?', au_usuario='?', au_accion='A', au_fecha_hora=now()";
                    $database->query($database->parse($sql, 'FACTURA','NO SE GENERO',$venta,'ERROR', $error, $user->getPERSONA()));
                    $id_log = $database->getLastId();
                    
                    $sql = "UPDATE ventas SET logwsafip='?' WHERE venta = '?'";
                    $database->query($database->parse($sql,$id_log, $venta));
                    // </editor-fold>
                } 
        
             
        // </editor-fold>
                    // </editor-fold>
                 
             
        // </editor-fold>
        
        
    }
    
    function ImprimirComprobanteA($venta) {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        // </editor-fold>

        set_time_limit(0);

        if ($request->has('venta', 'post')) {
            $venta_id = $request->get('venta', 'post');
        } else {
            if ($request->has('venta', 'get')) {
                $venta_id = $request->get('venta', 'get');
            }
        }


        $consulta = "SELECT DISTINCT * FROM vw_grilla_ventas WHERE venta = '?'";
        $venta_info = $database->getRow($database->parse($consulta, $venta_id));

        $consult = "SELECT * FROM vw_grilla_ventaarticulos WHERE venta='" . $venta_id . "' ORDER BY  articulo ASC";
        $detalleventa = $database->getRows($consult);

        // <editor-fold defaultstate="collapsed" desc="config PDF">
        // Include the main TCPDF library (search for installation path).
        // Include the main TCPDF library (search for installation path).
        define('FPDF_FONTPATH', 'font/');
        require('library/pdf/tcpdf/tcpdf.php');
        require_once('library/pdf/tcpdf/tcpdf_include.php');

        $con = PDF_PAGE_ORIENTATION;
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF8 sin BOM', false);


        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('STC');
        $pdf->SetTitle('');
        $pdf->SetSubject(' ');
        $pdf->SetKeywords(' ');

        // set default header data
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        //LA IMAGEN DEBE ESTAR EN template\default\image
        // $pdf->SetHeaderData('cabecera PDF-02.jpg', 173, '', '');
        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font - conjunto predeterminado fuentemonoespaciada
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetMargins(20, 10, 20);
        //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetHeaderMargin(5);
        $pdf->SetFooterMargin(15);

        // set auto page breaks - establecer saltos de página automático
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // establecemos la medida del interlineado
        //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        //$pdf->SetDefaultMonospacedFont(30);
        // ---------------------------------------------------------
        // set font
        $pdf->SetFont('helvetica', '', 9);


        //$pdf->Image('images/sistema.png', 50, 50, 100, '', '', 'http://www.tcpdf.org', '', false, 300);
// </editor-fold>  
        // add a page
        $pdf->AddPage();

        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">
        // $html = '<div style="text-align:center; " ><h1><b>UNIÓN DE RUGBY ARGENTINO</b></h1><br></div>';
        //$pdf->writeHTML($html, true, false, true, false, '');
        //$html = '<div style="text-align:center; font-size: 13pt;" ><b>CONSTANCIA DE ORDEN DE REPARACIÓN</b><br></div>';
        //$pdf->writeHTML($html, true, false, true, false, '');
//            $html = '<div style="text-align:center; font-size: 11pt;" ><br><b>ORDEN NRO: '.$reparacion_id.' </b><br></div>';
//            $pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO REMITO">
        // define barcode style
        $style = array(
            'position' => '',
            'align' => 'C',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

        // PRINT VARIOUS 1D BARCODES
        // CODE 39 EXTENDED + CHECKSUM
        //$pdf->Cell(0, 240, 'NÚMERO DE ORDEN', 0, 1);
        //public function write1DBarcode($code, $type, $x='', $y='', $w='', $h='', $xres='', $style='', $align='')
        //$pdf->write1DBarcode($reparacion_id, 'S25', '', '242', '', 18, 0.4, $style, 'N');
        //$pdf->Cell(110, 0, 'NÚMERO DE ORDEN', 0, 1);
        // Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)
        $pdf->Image('template/default/image/Remito.jpg', 95, 10, 20, 30, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
        $pdf->Image('template/default/image/Insight.jpg', 25, 10, 45, 20, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
        //$pdf->Text(55, 260, 'NÚMERO DE ORDEN');

        $fecha = date('d/m/Y', strtotime($venta_info['fecha']));
        $ptovta = $venta_info['descpuntovta'];
        $vendedorNombre = $venta_info['nombreApellidoVendedor'];

        $html = <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    <td width="100" style="text-align:left;"><b></b></td>
                    <td width="300" style="text-align:right;"><b></b></td>
                    <td width="300" style="text-align:left;" ><b></b></td>
                    
               </tr> 
                <tr >
                    <td width="100" style="text-align:left;" ><b></b></td>
                    <td width="300" style="text-align:right;"><b></b></td>
                    <td width="300" style="text-align:left;" ><b>REMITO NRO: $venta_id</b></td>
               </tr> 
                <tr >
                    <td width="100" style="text-align:left;" ><b></b></td>
                    <td width="300" style="text-align:right;"><b></b></td>
                    <td width="300" style="text-align:left;" ><b>FECHA: $fecha</b></td>
                    
               </tr>
            </table>
EOF;


        $pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="CLIENTE EQUIPO">
        //$html = '<div style="text-align:center; font-size: 11pt;" ><b>Datos del Cliente</b><hr /></div>';
        $html = <<<EOF
                 <br> <br><br><br><hr> 
EOF;
        $pdf->writeHTML($html, true, false, true, false, '');
        // <editor-fold defaultstate="collapsed" desc="STYLE">
        $html = <<<EOF
                <!-- EXAMPLE OF CSS STYLE -->
                <style>
                    /*
                    table.first {
                        color: black;
                        font-family: helvetica;
                        font-size: 8pt;
                        background-color: white;
                    }
                    td {
                        border: 0px solid black;
                        background-color: white;
                    }
                    */
                </style>
EOF;
        //$pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>

        $consulta = "SELECT DISTINCT * FROM vw_grilla_personas WHERE persona = '?'";
        $comprador_info = $database->getRow($database->parse($consulta, $venta_info['personacomprador']));

        $ducumento = $venta_info['personacomprador'];
        $apellidonombrecliente = $venta_info['nombreApellidoComprador'];
        $telefonocliente = $comprador_info['celular'];
        $mailcliente = $comprador_info['mail'];

//        $nroserie = $reparacion_info['nroserie'];
//        $tipoproducto = $reparacion_info['desctipoproducto'];
//        $modelo = $reparacion_info['descmarca'] . ' - ' . $reparacion_info['descmodelo'];
//        $engarantia = No;
//        if ($reparacion_info['engarantia'] == 1) {
//            $engarantia = Si;
//        }

        $html .= <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    <td width="300" style="text-align:left;"><b>DOCUMENTO: $ducumento</b></td>
                    <td width="50" style="text-align:left;" ></td>
                    <td width="300" style="text-align:left;" ><b>PTO VTA: $ptovta</b></td>
               </tr> 
                <tr >
                    <td width="300" style="text-align:left;"><b>APELLIDO Y NOMBRE: $apellidonombrecliente</b></td>
                    <td width="50" style="text-align:left;" ></td>
                    <td width="300" style="text-align:left;" ><b>VENDEDOR: $vendedorNombre</b></td>
               </tr>
                <tr >
                    <td width="300" style="text-align:left;"><b>TELÉFONO: $telefonocliente</b></td>
                    <td width="50" style="text-align:left;" ></td>
                    <td width="300" style="text-align:left;" ><b> </b></td>
               </tr>
                <tr >
                    <td width="300" style="text-align:left;"><b>MAIL: $mailcliente</b></td>
                    <td width="50" style="text-align:left;" ></td>
                    <td width="300" style="text-align:left;" ><b> </b></td>
               </tr>
                
            </table>
                <br><hr>
EOF;


        $pdf->writeHTML($html, true, false, true, false, '');

        // </editor-fold>
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="HARD SOFT">
        $html = '<div style="text-align:center; font-size: 11pt;" ><b>DETALLE</b><hr /></div>';
        $html .= <<<EOF
                   <br>
EOF;
        $pdf->writeHTML($html, true, false, true, false, '');
        // <editor-fold defaultstate="collapsed" desc="STYLE">
        $html = <<<EOF
                <!-- EXAMPLE OF CSS STYLE -->
                <style>
                    /*
                    table.first {
                        color: black;
                        font-family: helvetica;
                        font-size: 8pt;
                        background-color: white;
                    }
                    td {
                        border: 0px solid black;
                        background-color: white;
                    }
                    */
                </style>
EOF;
        //$pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>
        //lista de repuestos reparacion




        $html .= <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    <td width="80" style="text-align:left;"><b>Artículo</b></td>
                    <td width="240" style="text-align:left;"><b>Descripción</b></td>
                    <td width="60" style="text-align:center;"><b>Cantidad</b></td>
                    <td width="70" style="text-align:center;"><b>Precio U.</b></td>
                    <td width="70" style="text-align:center;"><b>Descuento</b></td>
                    <td width="80" style="text-align:right;"><b>Subtotal</b></td>
               </tr> 
                
            </table>
                <hr />
EOF;

        foreach ($detalleventa as $detalle) {
            $articulo = $detalle['codbarrainterno'];
            $descripcion = $detalle['descmodelo'];
            $cantidad = $detalle['cantidad'];
            $preciovta = $detalle['preciovta'];
            $descuento = $detalle['descuento'];
            $subtotal = ($preciovta * $cantidad) - $descuento;

            $html .= <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    <td width="80" style="text-align:left;">$articulo</td>
                    <td width="240" style="text-align:left;">$descripcion</td>
                    <td width="60" style="text-align:center;">$cantidad</td>
                    <td width="70" style="text-align:center;">$ $preciovta</td>
                    <td width="70" style="text-align:center;">$ $descuento</td>
                    <td width="80" style="text-align:right;">$ $subtotal</td>
               </tr> 
                
            </table>
EOF;
        }

        $pdf->writeHTML($html, true, false, true, false, '');

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SELLO">
        // Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)
        //  $pdf->Image('template/default/image/sello-entregado-2.png', 130, 180, 60, 50, 'png', '', '', true, 150, '', false, false, 0, false, false, false);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PIE">
        //$html = '<div style="text-align:center; font-size: 11pt;" ><b>Datos del Cliente</b><hr /></div>';
        $html = <<<EOF
                 <br> <br><br><br><hr> 
EOF;
        $pdf->writeHTML($html, true, false, true, false, '');
        // <editor-fold defaultstate="collapsed" desc="STYLE">
        $html = <<<EOF
                <!-- EXAMPLE OF CSS STYLE -->
                <style>
                    /*
                    table.first {
                        color: black;
                        font-family: helvetica;
                        font-size: 8pt;
                        background-color: white;
                    }
                    td {
                        border: 0px solid black;
                        background-color: white;
                    }
                    */
                </style>
EOF;
        //$pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>

        $total = $venta_info['total'];
        $html = <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    
                    <td width="600" style="text-align:center;" ><b>TOTAL $ $total</b></td>
               </tr> 
                
                
            </table>
                <br><hr>
EOF;


        $pdf->writeHTML($html, true, false, true, false, '');

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PIE">
        $pdf->Text(40, 255, '_____________________________________');
        $pdf->Text(45, 260, 'FIRMA Y ACLARACION DEL CLIENTE');
        // </editor-fold>
        $pdf->setPrintHeader(false);
        //ESTO HACE QUE ANDE EN EL SERVER DE LA DNNA
        ob_end_clean();
        //Close and output PDF document
        //D obliga la descarga
        //I abre en una ventana nueva
        $nombrePDF = 'RemitoNro' . $venta_id . '.pdf';
        $pdf->Output($nombrePDF, 'I');

        //$cache->delete('checkins');
        //die('ok');
        //============================================================+
        // END OF FILE
        //============================================================+
    }
    
    function ImprimirComprobanteB($venta) {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $user = & $this->locator->get('user');
        // </editor-fold>

        set_time_limit(0);

      
        $venta_id = $request->get('venta', 'get');
            
        


        $consulta = "SELECT DISTINCT * FROM vw_grilla_ventas WHERE venta = '?'";
        $venta_info = $database->getRow($database->parse($consulta, $venta_id));

        $consulta = "SELECT  * FROM vw_grilla_ventaarticulos WHERE venta = '?' ORDER BY  articulo ASC";
        $detalleventa = $database->getRows($database->parse($consulta, $venta_id));
        
        $consulta = "SELECT  * FROM vw_grilla_ventaformasdepago WHERE venta = '?'";
        $detalleformapago = $database->getRows($database->parse($consulta, $venta_id));
              

         // <editor-fold defaultstate="collapsed" desc="IMPRIMIR AFIP FACTURA B">
             
                include_once ('library/neofactura/wsfev1.php');
                include_once ('library/neofactura/wsaa.php');
                include_once ('library/neofactura/neopdf/pdf_voucher.php');

                $empresa_info = $database->getRow("SELECT e.*, ti.descripcion as desctipoiva FROM empresa e LEFT JOIN tiposiva ti ON e.tipoiva = ti.tipoiva limit 1 ");
    
                $CUIT = $empresa_info['cuit'];
                                 
                // <editor-fold defaultstate="collapsed" desc="EJECUTA">    
                        // <editor-fold defaultstate="collapsed" desc="CONFIG IMPRIMIR AFIP">
                        //NEW CONFIG
                        //private $config = array();
                        $vconfig = array();
                        $vconfig["footer"] = false;
                        $vconfig["total_line"] = false;
                        $vconfig["header"] = false;
                        $vconfig["receiver"] = false;
                        $vconfig["footer"] = false;
                        $vconfig["TRADE_SOCIAL_REASON"] = $empresa_info['razonsocial'];
                        $vconfig["TRADE_CUIT"] = $CUIT;
                        $vconfig["TRADE_ADDRESS"] = $empresa_info['domiciliofiscal'];
                        $vconfig["TRADE_TAX_CONDITION"] = $empresa_info['desctipoiva'];
                        $vconfig["TRADE_INIT_ACTIVITY"] = $empresa_info['iniciosactividad'];
                        $vconfig["TYPE_CODE"] = 'codigo';
                        $vconfig["VOUCHER_OBSERVATION"] = false;
                        $vconfig["VOUCHER_CONFIG"] = false;
                        // </editor-fold>

                        // <editor-fold defaultstate="collapsed" desc="GENERAR VOUCHER"> 
                            
                            // <editor-fold defaultstate="collapsed" desc="PUNTO DE VENTA"> 
                            
                            //BUSCO EN POSICIONES FISCALES CON EL TIPO DE COMPROBANTE Y EL PUNTO DE VENTA
                            $puntovta_info = $database->getRow("SELECT * FROM posicionesfiscales where puntovta ='".$user->getPuntovtaasignado()."' AND tipocomprobante = 'FBE' ");
                            $puntovta = (int)$puntovta_info['prefijo'];
                            // </editor-fold>

                            // <editor-fold defaultstate="collapsed" desc="TIPO COMPROBANTE"> 
                            // Tipo de comprobante   TIPO_FACTURA_A = 1; TIPO_FACTURA_B = 6; TIPO_FACTURA_C = 11;
                            $tipocomprobante = 'FACTURA B';
                            $letra = 'B';
                            
                            //        001	FACTURAS A
                            //        002	NOTAS DE DEBITO A
                            //        003	NOTAS DE CREDITO A
                            //        004	RECIBOS A
                            //        005	NOTAS DE VENTA AL CONTADO A
                            //        006	FACTURAS B
                            //        007	NOTAS DE DEBITO B
                            //        008	NOTAS DE CREDITO B
                            //        009	RECIBOS B
                            //        010	NOTAS DE VENTA AL CONTADO B
                            //        011	FACTURAS C
                            //        012	NOTAS DE DEBITO C
                            //        013	NOTAS DE CREDITO C
                            $codigoTipoComprobante = '6';
                            // </editor-fold>

                            // <editor-fold defaultstate="collapsed" desc="CLIENTE">
                            // Tipo de documento del comprador 
                            // 80	CUIT
                            //86	CUIL
                            //87	CDI
                            //89	LE
                            //90	LC
                            //94	Pasaporte
                            //96	DNI

                            if ($venta_info['personacomprador'] == '0') {
                                $TipoDocumento = 'DNI';
                                $codigoTipoDocumento = '99';
                                // Numero de documento del comprador 0 para Consumidor Final (<$1000)
                                $numeroDocumento = '0';
                                $nombreCliente = 'Consumidor Final';
                                $domicilioCliente= '-';
                            }
                            else{
                                $sql = "SELECT * "
                                . "FROM vw_grilla_personas p "
                                . "WHERE (p.persona = '".$venta_info['personacomprador']."' ) ";
                                $cliente_info = $database->getRow($sql);
                                
                                $TipoDocumento = $cliente_info['desctipodocumento'];
                                $codigoTipoDocumento = '99';
                                switch ($cliente_info['desctipodocumento']) {
                                    case 'DNI': $codigoTipoDocumento = '96';break;
                                    case 'CUIT': $codigoTipoDocumento = '80';break;
                                    case 'CUIL': $codigoTipoDocumento = '86';break;
                                    case 'PASAPORTE': $codigoTipoDocumento = '94';break;
                                    case 'LE': $codigoTipoDocumento = '89';break;
                                    case 'LC': $codigoTipoDocumento = '90';break;
                                    default:
                                        break;
                                }
                                
                                // Numero de documento del comprador 0 para Consumidor Final (<$1000)
                                $numeroDocumento = $cliente_info['persona'];
                                $nombreCliente = $cliente_info['apellidonombre'] ;
                                $domicilioCliente= $cliente_info['direccion'];
                            }
                            
                            
                            
                            // <editor-fold defaultstate="collapsed" desc="TIPO RESPONSABLE">
                            //tipo de responsable
                            //        Código	Descripción
                            //        1	IVA Responsable Inscripto
                            //        2	IVA Responsable no Inscripto
                            //        3	IVA no Responsable
                            //        4	IVA Sujeto Exento
                            //        5	Consumidor Final
                            //        6	Responsable Monotributo
                            //        7	Sujeto no Categorizado
                            //        8	Proveedor del Exterior
                            //        9	Cliente del Exterior
                            //        10	IVA Liberado – Ley Nº 19.640
                            //        11	IVA Responsable Inscripto – Agente de Percepción
                            //        12	Pequeño Contribuyente Eventual
                            //        13	Monotributista Social
                            //        14	Pequeño Contribuyente Eventual Social
                            $tipoResponsable = 'Consumidor Final';
                            if($cliente_info['desctipoiva'] != '' && $cliente_info['desctipoiva'] != '-1' && $cliente_info['desctipoiva'] !='IN') {
                                $tipoResponsable = $cliente_info['desctipoiva']; 
                            }
                            
                            // </editor-fold>
                            // </editor-fold>
                            
                            // <editor-fold defaultstate="collapsed" desc="CONDICION VENTA O FORMA DE PAGO">
                            
                            $descformapago = 'Contado';
                            $coma = '';
                            foreach ($detalleformapago as $itemformapago) {
                                if ($itemformapago['descformapago'] != '-1') {
                                    $descformapago .= $coma . $itemformapago['descformapago'];
                                    $coma = ', ';
                                }
                            }
                            $CondicionVenta = $descformapago;
                            // </editor-fold>
                            
                            // <editor-fold defaultstate="collapsed" desc="UNIDADES DE MEDIDIA">
//                            00	SIN DESCRIPCION
                            //01	KILOGRAMO
                            //02	METROS
                            //03	METRO CUADRADO
                            //04	METRO CUBICO
                            //05	LITROS
                            //06	1000 KILOWATT HORA
                            //07	UNIDAD
                            //08	PAR
                            //09	DOCENA
                            //10	QUILATE
                            //11	MILLAR
                            //12	MEGA U. INTER. ACT. ANTIB
                            //13	UNIDAD INT. ACT. INMUNG
                            //14	GRAMO
                            //15	MILIMETRO
                            //16	MILIMETRO CUBICO
                            //17	KILOMETRO
                            //18	HECTOLITRO
                            //19	MEGA UNIDAD INT. ACT. INMUNG
                            //20	CENTIMETRO
                            //21	KILOGRAMO ACTIVO
                            //22	GRAMO ACTIVO
                            //23	GRAMO BASE
                            //24	UIACTHOR
                            //25	JGO.PQT. MAZO NAIPES
                            //26	MUIACTHOR
                            //27	CENTIMETRO CUBICO
                            //28	UIACTANT
                            //29	TONELADA
                            //30	DECAMETRO CUBICO
                            //31	HECTOMETRO CUBICO
                            //32	KILOMETRO CUBICO
                            //33	MICROGRAMO
                            //34	NANOGRAMO
                            //35	PICOGRAMO
                            //36	MUIACTANT
                            //37	UIACTIG
                            //41	MILIGRAMO
                            //47	MILILITRO
                            //48	CURIE
                            //49	MILICURIE
                            //50	MICROCURIE
                            //51	U.INTER. ACT. HORMONAL
                            //52	MEGA U. INTER. ACT. HOR.
                            //53	KILOGRAMO BASE
                            //54	GRUESA
                            //55	MUIACTIG
                            //61	KILOGRAMO BRUTO
                            //62	PACK
                            //63	HORMA
                            //97	SEÑAS/ANTICIPOS
                            //98	OTRAS UNIDADES
                            //99	BONIFICACION

                            $unidaddemedida = 'UN';
                            
                            // </editor-fold>
                            
                            $rows = array();
                            foreach ($detalleventa as $art) {
                               //$cell = array();
                                $rows[] = array(
                                            'scanner' 		=>$art['codbarra'],
                                            'codigo' 		=>$art['articulo'],
                                            'descripcion' 	=>$art['descmodelo'],
                                            'cantidad' 		=>$art['cantidad'],
                                            'UnidadMedida' 	=>$unidaddemedida, //7 UN
                                            'precioUnitario' 	=>$art['preciovta'],
                                            'porcBonif' 	=>$art['porcDescuento'],
                                            'impBonif' 		=>$art['descuento'],
                                            'Alic'              =>3,
                                            'importeItem' 	=>$art['preciovta_descuento'] * $art['cantidad']
                                );
                                //$rows[] = array($cell);
                            }
                            //si hay un recargo por financiacion se le agrega un item
                            if ($venta_info['totalrecargofinanciacion'] > 0) {
                                //$cell = array();
                                $rows[] = array(
                                            'scanner' 		=>0,
                                            'codigo' 		=>0,
                                            'descripcion' 	=>'Recargo por financiación',
                                            'cantidad' 		=>1,
                                            'UnidadMedida' 	=>$unidaddemedida, //7 UN
                                            'precioUnitario' 	=>$venta_info['totalrecargofinanciacion'],
                                            'porcBonif' 	=>0,
                                            'impBonif' 		=>0,
                                            'Alic'              =>3,
                                            'importeItem' 	=>$venta_info['totalrecargofinanciacion']
                                );
                                //$rows[] = array($cell);
                            }
                            
                            // <editor-fold defaultstate="collapsed" desc="VOUCHER">
                            $voucher = array(
                                    'TipoComprobante' 		=>$tipocomprobante,     // Tipo de comprobante   TIPO_FACTURA_A = 1; TIPO_FACTURA_B = 6; TIPO_FACTURA_C = 11;
                                    'numeroComprobante'         =>$venta_info['nrofactura'],                 // Numero de comprobante o numero del primer comprobante en caso de ser mas de uno
                                    'letra'                     =>$letra,                 // A o B    
                                    'numeroPuntoVenta' 		=>$puntovta,              // Punto de venta
                                    'fechaComprobante' 		=>intval(date('Ymd')),    // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
                                    'codigoTipoComprobante' 	=>$codigoTipoComprobante,// Tipo de comprobante (ver tipos disponibles)
                                    'codigoConcepto' 		=>1,                    // Concepto del Comprobante: (1)Productos, (2)Servicios, (3)Productos y Servicios
                                        'fechaDesde' 		=>NULL,
                                        'fechaHasta' 		=>NULL,
                                        'fechaVtoPago' 		=>NULL,
                                    'TipoDocumento' 		=>$TipoDocumento,                   // Tipo de documento del comprador 80: CUIT, 96: DNI, 99: Consumidor Final
                                    'codigoTipoDocumento'       =>$codigoTipoDocumento,                   // Tipo de documento del comprador 80: CUIT, 96: DNI, 99: Consumidor Final
                                    'numeroDocumento' 		=>$numeroDocumento,                    // Numero de documento del comprador 0 para Consumidor Final (<$1000)
                                    'nombreCliente' 		=>$nombreCliente,
                                    'tipoResponsable' 		=>$tipoResponsable,
                                    'domicilioCliente' 		=>$domicilioCliente,
                                    'CondicionVenta' 		=>$CondicionVenta,                         //formas de pago
                                    'items'                     =>$rows,
                                    'Tributos' => NULL, // array aca iria el recargo por financiacion
                                    'importeOtrosTributos' 	=>0,                // Importe neto no gravado
                                    'codigoMoneda' 		=>'PES',            //Tipo de moneda usada en el comprobante (ver tipos disponibles)('PES' para pesos argentinos) 
                                    'importeGravado'            =>0,              // Importe neto gravado
                                    'subtotivas' => array ( array (         // array Importe total de IVA
                                                            'codigo'    =>3,//3 es 0% de iva
                                                            'BaseImp' 	=>0, //aca que va el total de la factura
                                                            'importe' 	=>0 //va 0 porque es B y no se declara iva
                                    )),             
                                    'importeIVA'                =>0,
                                    'importeNoGravado'          =>$venta_info['totalapagar'],
                                    'importeExento'             =>0,
                                    'importeTotal' 		=>$venta_info['totalapagar'],              // Importe total del comprobante
                                    'cae'                       =>$venta_info['cae'],             //SOLICITARLO
                                    'fechaVencimientoCAE' 	=>$venta_info['fechavencimientocae'],             //SOLICITARLO
                                    'cotizacionMoneda'          =>'1',                // Cotización de la moneda usada (1 para pesos argentinos)            
                                );  //$request->get('total', 'post'), 
                                //  $request->get('totalapagar', 'post'), 
                                //  $request->get('totalrecargofinanciacion', 'post')
                            // </editor-fold>
                            // </editor-fold> 
                            
                        // <editor-fold defaultstate="collapsed" desc="IMPRIMO PDF">
                            //MANDO A IMPRIMIR LA FACTURA
                                        ob_start();
                                        $logo_path ='template/default/image/logocomprobante.jpg';
                                        $pdf = new PDFVoucher($voucher, $vconfig);
                                        $pdf->emitirPDF($logo_path);
                                        //ob_end_clean();//rompimiento de pagina
                                        ob_clean();
                                        $pdf->Output("factura.pdf","D");
                            //guardo el log en la tabla logwsafip y en la venta
                        // </editor-fold>
                    // </editor-fold>
                 
             
        // </editor-fold>
        
        
    }
    
    function RemitoPDF() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        // </editor-fold>

        set_time_limit(0);

        if ($request->has('venta', 'post')) {
            $venta_id = $request->get('venta', 'post');
        } else {
            if ($request->has('venta', 'get')) {
                $venta_id = $request->get('venta', 'get');
            }
        }


        $consulta = "SELECT DISTINCT * FROM vw_grilla_ventas WHERE venta = '?'";
        $venta_info = $database->getRow($database->parse($consulta, $venta_id));

        $consult = "SELECT * FROM vw_grilla_ventaarticulos WHERE venta='" . $venta_id . "' ORDER BY  articulo ASC";
        $detalleventa = $database->getRows($consult);

        // <editor-fold defaultstate="collapsed" desc="config PDF">
        // Include the main TCPDF library (search for installation path).
        // Include the main TCPDF library (search for installation path).
        define('FPDF_FONTPATH', 'font/');
        require('library/pdf/tcpdf/tcpdf.php');
        require_once('library/pdf/tcpdf/tcpdf_include.php');

        $con = PDF_PAGE_ORIENTATION;
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF8 sin BOM', false);


        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('STC');
        $pdf->SetTitle('');
        $pdf->SetSubject(' ');
        $pdf->SetKeywords(' ');

        // set default header data
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        //LA IMAGEN DEBE ESTAR EN template\default\image
        // $pdf->SetHeaderData('cabecera PDF-02.jpg', 173, '', '');
        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font - conjunto predeterminado fuentemonoespaciada
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetMargins(20, 10, 20);
        //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetHeaderMargin(5);
        $pdf->SetFooterMargin(15);

        // set auto page breaks - establecer saltos de página automático
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // establecemos la medida del interlineado
        //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        //$pdf->SetDefaultMonospacedFont(30);
        // ---------------------------------------------------------
        // set font
        $pdf->SetFont('helvetica', '', 9);


        //$pdf->Image('images/sistema.png', 50, 50, 100, '', '', 'http://www.tcpdf.org', '', false, 300);
// </editor-fold>  
        // add a page
        $pdf->AddPage();

        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">
        // $html = '<div style="text-align:center; " ><h1><b>UNIÓN DE RUGBY ARGENTINO</b></h1><br></div>';
        //$pdf->writeHTML($html, true, false, true, false, '');
        //$html = '<div style="text-align:center; font-size: 13pt;" ><b>CONSTANCIA DE ORDEN DE REPARACIÓN</b><br></div>';
        //$pdf->writeHTML($html, true, false, true, false, '');
//            $html = '<div style="text-align:center; font-size: 11pt;" ><br><b>ORDEN NRO: '.$reparacion_id.' </b><br></div>';
//            $pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO REMITO">
        // define barcode style
        $style = array(
            'position' => '',
            'align' => 'C',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

        // PRINT VARIOUS 1D BARCODES
        // CODE 39 EXTENDED + CHECKSUM
        //$pdf->Cell(0, 240, 'NÚMERO DE ORDEN', 0, 1);
        //public function write1DBarcode($code, $type, $x='', $y='', $w='', $h='', $xres='', $style='', $align='')
        //$pdf->write1DBarcode($reparacion_id, 'S25', '', '242', '', 18, 0.4, $style, 'N');
        //$pdf->Cell(110, 0, 'NÚMERO DE ORDEN', 0, 1);
        // Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)
        $pdf->Image('template/default/image/Remito.jpg', 95, 10, 20, 30, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
        $pdf->Image('template/default/image/Insight.jpg', 25, 10, 45, 20, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
        //$pdf->Text(55, 260, 'NÚMERO DE ORDEN');

        $fecha = date('d/m/Y', strtotime($venta_info['fecha']));
        $ptovta = $venta_info['descpuntovta'];
        $vendedorNombre = $venta_info['nombreApellidoVendedor'];

        $html = <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    <td width="100" style="text-align:left;"><b></b></td>
                    <td width="300" style="text-align:right;"><b></b></td>
                    <td width="300" style="text-align:left;" ><b></b></td>
                    
               </tr> 
                <tr >
                    <td width="100" style="text-align:left;" ><b></b></td>
                    <td width="300" style="text-align:right;"><b></b></td>
                    <td width="300" style="text-align:left;" ><b>REMITO NRO: $venta_id</b></td>
               </tr> 
                <tr >
                    <td width="100" style="text-align:left;" ><b></b></td>
                    <td width="300" style="text-align:right;"><b></b></td>
                    <td width="300" style="text-align:left;" ><b>FECHA: $fecha</b></td>
                    
               </tr>
            </table>
EOF;


        $pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="CLIENTE EQUIPO">
        //$html = '<div style="text-align:center; font-size: 11pt;" ><b>Datos del Cliente</b><hr /></div>';
        $html = <<<EOF
                 <br> <br><br><br><hr> 
EOF;
        $pdf->writeHTML($html, true, false, true, false, '');
        // <editor-fold defaultstate="collapsed" desc="STYLE">
        $html = <<<EOF
                <!-- EXAMPLE OF CSS STYLE -->
                <style>
                    /*
                    table.first {
                        color: black;
                        font-family: helvetica;
                        font-size: 8pt;
                        background-color: white;
                    }
                    td {
                        border: 0px solid black;
                        background-color: white;
                    }
                    */
                </style>
EOF;
        //$pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>

        $consulta = "SELECT DISTINCT * FROM vw_grilla_personas WHERE persona = '?'";
        $comprador_info = $database->getRow($database->parse($consulta, $venta_info['personacomprador']));

        $ducumento = $venta_info['personacomprador'];
        $apellidonombrecliente = $venta_info['nombreApellidoComprador'];
        $telefonocliente = $comprador_info['celular'];
        $mailcliente = $comprador_info['mail'];

//        $nroserie = $reparacion_info['nroserie'];
//        $tipoproducto = $reparacion_info['desctipoproducto'];
//        $modelo = $reparacion_info['descmarca'] . ' - ' . $reparacion_info['descmodelo'];
//        $engarantia = No;
//        if ($reparacion_info['engarantia'] == 1) {
//            $engarantia = Si;
//        }

        $html .= <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    <td width="300" style="text-align:left;"><b>DOCUMENTO: $ducumento</b></td>
                    <td width="50" style="text-align:left;" ></td>
                    <td width="300" style="text-align:left;" ><b>PTO VTA: $ptovta</b></td>
               </tr> 
                <tr >
                    <td width="300" style="text-align:left;"><b>APELLIDO Y NOMBRE: $apellidonombrecliente</b></td>
                    <td width="50" style="text-align:left;" ></td>
                    <td width="300" style="text-align:left;" ><b>VENDEDOR: $vendedorNombre</b></td>
               </tr>
                <tr >
                    <td width="300" style="text-align:left;"><b>TELÉFONO: $telefonocliente</b></td>
                    <td width="50" style="text-align:left;" ></td>
                    <td width="300" style="text-align:left;" ><b> </b></td>
               </tr>
                <tr >
                    <td width="300" style="text-align:left;"><b>MAIL: $mailcliente</b></td>
                    <td width="50" style="text-align:left;" ></td>
                    <td width="300" style="text-align:left;" ><b> </b></td>
               </tr>
                
            </table>
                <br><hr>
EOF;


        $pdf->writeHTML($html, true, false, true, false, '');

        // </editor-fold>
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="HARD SOFT">
        $html = '<div style="text-align:center; font-size: 11pt;" ><b>DETALLE</b><hr /></div>';
        $html .= <<<EOF
                   <br>
EOF;
        $pdf->writeHTML($html, true, false, true, false, '');
        // <editor-fold defaultstate="collapsed" desc="STYLE">
        $html = <<<EOF
                <!-- EXAMPLE OF CSS STYLE -->
                <style>
                    /*
                    table.first {
                        color: black;
                        font-family: helvetica;
                        font-size: 8pt;
                        background-color: white;
                    }
                    td {
                        border: 0px solid black;
                        background-color: white;
                    }
                    */
                </style>
EOF;
        //$pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>
        //lista de repuestos reparacion




        $html .= <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    <td width="80" style="text-align:left;"><b>Artículo</b></td>
                    <td width="240" style="text-align:left;"><b>Descripción</b></td>
                    <td width="60" style="text-align:center;"><b>Cantidad</b></td>
                    <td width="70" style="text-align:center;"><b>Precio U.</b></td>
                    <td width="70" style="text-align:center;"><b>Descuento</b></td>
                    <td width="80" style="text-align:right;"><b>Subtotal</b></td>
               </tr> 
                
            </table>
                <hr />
EOF;

        foreach ($detalleventa as $detalle) {
            $articulo = $detalle['codbarrainterno'];
            $descripcion = $detalle['descmodelo'];
            $cantidad = $detalle['cantidad'];
            $preciovta = $detalle['preciovta'];
            $descuento = $detalle['descuento'];
            $subtotal = ($preciovta * $cantidad) - $descuento;

            $html .= <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    <td width="80" style="text-align:left;">$articulo</td>
                    <td width="240" style="text-align:left;">$descripcion</td>
                    <td width="60" style="text-align:center;">$cantidad</td>
                    <td width="70" style="text-align:center;">$ $preciovta</td>
                    <td width="70" style="text-align:center;">$ $descuento</td>
                    <td width="80" style="text-align:right;">$ $subtotal</td>
               </tr> 
                
            </table>
EOF;
        }

        $pdf->writeHTML($html, true, false, true, false, '');

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SELLO">
        // Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)
        //  $pdf->Image('template/default/image/sello-entregado-2.png', 130, 180, 60, 50, 'png', '', '', true, 150, '', false, false, 0, false, false, false);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PIE">
        //$html = '<div style="text-align:center; font-size: 11pt;" ><b>Datos del Cliente</b><hr /></div>';
        $html = <<<EOF
                 <br> <br><br><br><hr> 
EOF;
        $pdf->writeHTML($html, true, false, true, false, '');
        // <editor-fold defaultstate="collapsed" desc="STYLE">
        $html = <<<EOF
                <!-- EXAMPLE OF CSS STYLE -->
                <style>
                    /*
                    table.first {
                        color: black;
                        font-family: helvetica;
                        font-size: 8pt;
                        background-color: white;
                    }
                    td {
                        border: 0px solid black;
                        background-color: white;
                    }
                    */
                </style>
EOF;
        //$pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>

        $total = $venta_info['total'];
        $html = <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    
                    <td width="600" style="text-align:center;" ><b>TOTAL $ $total</b></td>
               </tr> 
                
                
            </table>
                <br><hr>
EOF;


        $pdf->writeHTML($html, true, false, true, false, '');

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PIE">
        $pdf->Text(40, 255, '_____________________________________');
        $pdf->Text(45, 260, 'FIRMA Y ACLARACION DEL CLIENTE');
        // </editor-fold>
        $pdf->setPrintHeader(false);
        //ESTO HACE QUE ANDE EN EL SERVER DE LA DNNA
        ob_end_clean();
        //Close and output PDF document
        //D obliga la descarga
        //I abre en una ventana nueva
        $nombrePDF = 'RemitoNro' . $venta_id . '.pdf';
        $pdf->Output($nombrePDF, 'I');

        //$cache->delete('checkins');
        //die('ok');
        //============================================================+
        // END OF FILE
        //============================================================+
    }

    function getPersonacomprador() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>		

        $miDescripcion = $request->get('term', 'get');

        $sql = "SELECT distinct p.persona AS id, IF(p.persona='0',p.apellidonombre,CONCAT('(',p.persona,') ',p.apellidonombre))  as value "
                . "FROM vw_grilla_personas p "
                . "WHERE (p.persona LIKE '?' OR p.nombre LIKE '?' OR p.apellido LIKE '?') "
                . "ORDER BY p.apellidonombre ASC "
                . "LIMIT 10";
        $consulta = $database->parse($sql, '%' . $miDescripcion . '%', '%' . $miDescripcion . '%', '%' . $miDescripcion . '%');

        $codigo = $database->getRows($consulta);

        echo json_encode($codigo);
    }

    function getArticulos() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $user = & $this->locator->get('user');
        // </editor-fold>

        $miDescripcion = $request->get('term', 'get');
        $listaprecio = $request->get('listaprecio', 'get');

        $sql = "SELECT DISTINCT if(a.codbarra!='',concat(a.articulo, '_',a.codbarra), a.articulo) as id, "
                . " concat(a.marca,' - ',a.descmodelo, ' ' ,if(a.codbarra is not null ,concat('Cod: ',a.codbarra), concat('Cod: ',a.codbarrainterno))) as value  "
                . "FROM articulospuntovta apv "
                . "LEFT JOIN vw_grilla_articulostodos a ON apv.articulo = a.articulo "
                . "LEFT JOIN listasdeprecioarticulos lp ON lp.articulo = a.articulo "
                . "WHERE apv.cantidad > 0 "
                . "AND apv.puntovta = '?' "
                . "AND (a.articulo LIKE '?' OR a.codbarrainterno LIKE '?' OR a.desctipoproducto LIKE '?' OR a.descmarca LIKE '?' OR a.descmodelo LIKE '?' OR a.codbarra like '?' ) "
                . "AND lp.listaprecio = '?' "
                . "AND a.estadoarticulo = 'DI' "
                . "LIMIT 10 ";
        $consulta = $database->parse($sql, $user->getPuntovtaasignado(),'%' . $miDescripcion . '%', '%' . $miDescripcion . '%', '%' . $miDescripcion . '%', '%' . $miDescripcion . '%', '%' . $miDescripcion . '%', '%' . $miDescripcion . '%', $listaprecio);
        $codigo = $database->getRows($consulta);

        $varia = utf8_decode(json_encode($codigo));

        echo $varia;
    }

    function getArticulo() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $user = & $this->locator->get('user');
        // </editor-fold>		

        $articulo = $request->get('articuloid', 'get');
        $codbarra = $request->get('codbarra', 'get');
        $puntovta = $request->get('puntovta', 'get');

        //si devuelvo error es porque el articulo esta tomada por alguna venta. 
        //ANTES DE DEVOLVER TENGO QUE BUSCAR EL PRECIO DE VENTA SEGUN LA LISTA DE PRECIO
        $sql = "SELECT listaprecio FROM puntosdeventa  WHERE puntovta = '?'  ";
        $consulta = $database->parse($sql, $puntovta);
        $listaprecio = $database->getRow($consulta);

        if ($codbarra != '' && $codbarra != 'undefined' && $codbarra != null) {
            $sql = "SELECT a.articulo, "
                    . "concat(a.marca,' - ',a.descmodelo, ' (cod bar:',a.codbarra,')') AS descripcion, "
                    . "'1' AS concoddebarra, "
                    . "a.codbarrainterno, "
                    . "a.codbarra, "
                    . "apv.cantidad AS cantidad, "
                    . "a.precioultcompra, "
                    . "ab.preciovta "
                    . "FROM articulospuntovta apv "
                    . "LEFT JOIN vw_grilla_articulostodos a ON apv.articulo = a.articulo "
                    . "LEFT JOIN listasdeprecioarticulos ab ON ab.articulo = a.articulo "
                    . "WHERE apv.cantidad > 0 "
                    . "AND apv.puntovta = '?' "
                    . "AND a.codbarra = '?' "
                    . "AND ab.listaprecio = '?' ";
            $consulta = $database->parse($sql, $user->getPuntovtaasignado(), $codbarra, $listaprecio['listaprecio']);
            $codigo = $database->getRow($consulta);
        } else {
            $sql = "SELECT a.articulo, "
                    . "concat(a.marca,' - ',a.descmodelo, ' (Cod: ',a.codbarrainterno,')') AS descripcion, "
                    . "a.concoddebarra, "
                    . "a.codbarrainterno, '' AS codbarra,"
                    . "apv.cantidad AS cantidad, "
                    . "a.precioultcompra, "
                    . "ab.preciovta "
                    . "FROM articulospuntovta apv "
                    . "LEFT JOIN vw_grilla_articulostodos a ON apv.articulo = a.articulo "
                    . "LEFT JOIN listasdeprecioarticulos ab ON ab.articulo = a.articulo "
                    . "WHERE apv.cantidad > 0 "
                    . "AND apv.puntovta = '?' "
                    . "AND apv.articulo = '?' "
                    . "AND ab.listaprecio = '?' ";
            $consulta = $database->parse($sql, $user->getPuntovtaasignado(), $articulo, $listaprecio['listaprecio']);
            $codigo = $database->getRow($consulta);
        }

        echo json_encode($codigo);
    }

    function ValidaAgregarLinea() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>		

        $miTerm = $request->get('term', 'get');

        $sql = "SELECT a.articulo , concat(a.marca,' - ',a.descmodelo) AS descripcion, a.concoddebarra, a.codbarrainterno, a.precioultcompra "
                . "FROM vw_grilla_articulos a "
                . "WHERE a.articulo = '?'";
        $consulta = $database->parse($sql, $miTerm);
        $codigo = $database->getRow($consulta);

        echo json_encode($codigo);
    }

    function AgregarArticulo() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $language = & $this->locator->get('language');
        $response = & $this->locator->get('response');
        $request = & $this->locator->get('request');
        $view = $this->locator->create('template');
        // </editor-fold>

        $view->set('button_add', $language->get('button_add'));
        $view->set('button_remove', $language->get('button_remove'));

        $articulo = $request->get('articulo');
        $cantidad = $request->get('cantidad');
        $porcDescuento = $request->get('porcDescuento');
        $descuento = $request->get('descuento');
        $codbarra = $request->get('codbarra');
        $codbarrainterno = $request->get('codbarrainterno');
        $precioventa = $request->get('precioventa');
        $precioventa_descuento = $request->get('precioventa_descuento');

        $sql = "SELECT a.articulo , concat(a.marca,' - ',a.descmodelo) AS descripcion, precioultcompra "
                . "FROM vw_grilla_articulos a "
                . "WHERE a.articulo = '?' ";
        $consulta = $database->parse($sql, $articulo);
        $art = $database->getRow($consulta);

        $total_fila = $cantidad * $precioventa_descuento;

        $descmodelo = $art['descripcion'];

        $view->set('cargo_venta', venta);
        $view->set('cargo_articulo', $articulo);
        $view->set('cargo_descmodelo', $descmodelo);
        $view->set('cargo_cantidad', $cantidad);
        $view->set('cargo_porcDescuento', $porcDescuento);
        $view->set('cargo_descuento', $descuento);
        $view->set('cargo_codbarra', $codbarra);
        $view->set('cargo_codbarrainterno', $codbarrainterno);
        $view->set('cargo_precioventa', $precioventa);
        $view->set('cargo_precioventa_descuento', $precioventa_descuento);
        $view->set('cargo_precioultcompra', $art['precioultcompra']);

        $view->set('cargo_id', $request->get('cantReg'));

        $view->set('nombre_fila', 'vtaarticulo_' . $request->get('cantReg'));

        $view->set('total_fila', $total_fila);

        $response->set($view->fetch('content/venta_articulo.tpl'));
    }

    function AgregarFormaPago() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $language = & $this->locator->get('language');
        $response = & $this->locator->get('response');
        $request = & $this->locator->get('request');
        $view = $this->locator->create('template');
        // </editor-fold>

        $view->set('button_add', $language->get('button_add'));
        $view->set('button_remove', $language->get('button_remove'));

        $formapago = $request->get('formapago');
        $venta = $request->get('venta');
        $tarjeta = $request->get('tarjeta');
        $banco = $request->get('banco');
        $nrotarjeta = $request->get('nrotarjeta');
        $notacredito = $request->get('notacredito');
        $cuotas = $request->get('cuotas');
        $importe = $request->get('importe');
        $financiacion = $request->get('financiacion');
        $totalLFP = $request->get('totalLFP');
        $recargo = $request->get('recargo');

        $sql = "SELECT formapago, descripcion FROM formaspago  WHERE formapago = '?' ";
        $consulta = $database->parse($sql, $formapago);
        $obj_formapago = $database->getRow($consulta);

        $descfinanciacion = '-';
        if ($financiacion != '-1') {
            $sql = "SELECT financiacion, descripcion FROM financiaciones  WHERE financiacion = '?' ";
            $consulta = $database->parse($sql, $financiacion);
            $obj_financiacion = $database->getRow($consulta);

            $descfinanciacion = $obj_financiacion['descripcion'];
            //$cuotas = $request->get('cuotas');
        }

        //$cuotas = '-';
        $desctarjeta = '-';
        if ($tarjeta != '-1') {
            $sql = "SELECT tarjeta, descripcion FROM tarjetas  WHERE tarjeta = '?' ";
            $consulta = $database->parse($sql, $tarjeta);
            $obj_tarjeta = $database->getRow($consulta);

            $desctarjeta = $obj_tarjeta['descripcion'];
            $cuotas = $request->get('cuotas');
        }

        $descbanco = '-';
        if ($banco != '-1') {
            $sql = "SELECT banco, descripcion FROM bancos  WHERE banco = '?' ";
            $consulta = $database->parse($sql, $banco);
            $obj_banco = $database->getRow($consulta);

            $descbanco = $obj_banco['descripcion'];
        }

        if ($nrotarjeta == '') {
            $nrotarjeta = '-';
        }

        if ($formapago == 'NC') {
            $nrotarjeta = $notacredito;
        }
        
        $descformapago = $obj_formapago['descripcion'];
        $precioxcuota = $totalLFP / $cuotas;

        $view->set('cargo_venta', $venta);
        $view->set('cargo_ventaformapago', $formapago);
        $view->set('cargo_descformapago', $obj_formapago['descripcion']);
        $view->set('cargo_tarjeta', $tarjeta);
        $view->set('cargo_desctarjeta', $desctarjeta);
        $view->set('cargo_banco', $banco);
        $view->set('cargo_descbanco', $descbanco);
        $view->set('cargo_nrotarjeta', $nrotarjeta);
        $view->set('cargo_cuotas', $cuotas);
        $view->set('cargo_textcuotas', $cuotas . ' de $' . $precioxcuota);
        $view->set('cargo_importe', $importe);
        $view->set('cargo_descfinanciacion', $descfinanciacion);
        $view->set('cargo_financiacion', $financiacion);
        $view->set('cargo_recargo', $recargo);
        $view->set('cargo_totalLFP', $totalLFP);
        //SI EL ENTRENAMIENTO ES 0 ES PORQUE ES UN ALTA
        $view->set('cargo_id', $request->get('cantReg'));

        $view->set('nombre_fila', 'vtaformapago_' . $request->get('cantReg'));

        $response->set($view->fetch('content/venta_formapago.tpl'));
    }

    function getFormCliente() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $language = & $this->locator->get('language');
        $response = & $this->locator->get('response');
        $request = & $this->locator->get('request');
        $url = & $this->locator->get('url');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('entry_persona', 'Documento:');
        $view->set('entry_apellido', 'Apellido:');
        $view->set('entry_nombre', 'Nombre:');
        $view->set('entry_celular', 'Celular:');
        $view->set('entry_mail', 'E-mail:');
        $view->set('entry_remail', 'Confirma E-mail:');
        $view->set('entry_fecha', 'Fecha nacimiento:');
        $view->set('entry_localidad', 'Localidad:');

        $view->set('entry_tipocliente', 'Tipo de cliente:');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('heading_title', 'Clientes');
        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/user-8.png');

        $view->set('heading_description', 'Gesti&oacute;n de clientes');

        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        $view->set('tab_general', $language->get('tab_general'));

        $view->set('persona', '');
        $view->set('apellido', '');
        $view->set('nombre', '');
        $view->set('tipocliente', '');
        $view->set('tiposcliente', $database->getRows("SELECT * FROM tiposcliente ORDER BY descripcion ASC"));
        $view->set('mail', '');
        $view->set('remail', '');
        $view->set('celular', '');
        $view->set('fechanacimiento', '');
        $view->set('auto_localidad', '');
        $view->set('auto_localidad', '');
        $view->set('auto_localidad_cliente', @$persona_info['localidad']);
        $view->set('script_busca_localidades', $url->rawssl('clientes', 'getLocalidad'));

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $view->set('error', $this->error['message']);
        //$session->delete('error');
        $view->set('error_persona', $this->error['persona']);
        $view->set('error_texto_error', $this->error['texto_error']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        // </editor-fold>

        $response->set($view->fetch('content/cliente_insert.tpl'));
    }

    function insertCliente() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $common = & $this->locator->get('common');
        // </editor-fold>

        $template->set('title', 'Clientes');

        $id = strtoupper($request->get('persona', 'get'));

        // <editor-fold defaultstate="collapsed" desc="FECHA">
        $fecha_d = '';
        if ($request->get('fechanacimiento', 'get') != '') {
            $dated = explode('/', $request->get('fechanacimiento', 'get'));
            $fecha_d = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
        } else {
            $fecha_d = NULL;
        }

        // </editor-fold>

        $clave = $common->getEcriptar($id);

        $sql = "INSERT INTO personas SET persona = '?',clave ='?', nombre ='?', apellido ='?', tipocliente='?', fechanacimiento = NULLIF('?',''), tipopersona='CL', direccion = '?', numero = '?',  piso='?', departamento='?', localidad='?',celular ='?',  mail ='?',  usuario = '?'";
        $sql = $database->parse($sql, $id, $clave, strtoupper($request->get('nombre', 'get')), strtoupper($request->get('apellido', 'get')), $request->get('tipocliente', 'get'), $fecha_d, $request->get('direccion', 'get'), $request->get('numero', 'get'), $request->get('piso', 'get'), $request->get('departamento', 'get'), $request->get('auto_localidad_cliente', 'get'), $request->get('celular', 'get'), $request->get('mail', 'get'), $user->getPERSONA());
        $database->query($sql);

        $sql = "INSERT IGNORE INTO personasgrupos SET persona = '?', grupo = 'CL', usuario = '?'";
        $database->query($database->parse($sql, $id, $user->getPERSONA()));

        $cache->delete('ventasafip');
        //$session->set('message', 'Se agreg&oacute; el cliente: ' . $id);

        $sql = "SELECT p.persona AS id, p.apellidonombre as value "
                . "FROM vw_grilla_personas p "
                . "WHERE p.persona = '?' "
                . "ORDER BY p.apellidonombre ASC ";
        $consulta = $database->parse($sql, $id);

        $codigo = $database->getRow($consulta);

        echo json_encode($codigo);
    }

    function getFinanciacion() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>		

        $financiacion = $request->get('financiacion', 'get');

        $sql = "SELECT *, cuotas AS textcuotas FROM financiaciones  WHERE financiacion = '?' ";

        $consulta = $database->parse($sql, $financiacion);
        $codigo = $database->getRow($consulta);


        echo json_encode($codigo);
    }

    function getFinanciaciones() {
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $response = & $this->locator->get('response');

        $view = $this->locator->create('template');

        $formapago_vta = $request->get('formapago_vta', 'get');

        $sql = "SELECT * FROM financiaciones WHERE formapago ='?' and estado = 'ACTIVO' ";
        $consulta = $database->parse($sql, $formapago_vta);
        $results = $database->getRows($consulta);

        $opts = "<option value='-1' checked>Seleccione uno...</option>";

        foreach ($results as $result):
            $opts .= "<option value='" . $result['financiacion'] . "'>" . $result['descripcion'] . "</option>";
        endforeach;

        echo $opts;
    }
    
    //SEGUIR EN HACER ESTOS DOS METODOS
    function getNotasdeCredito() {
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $response = & $this->locator->get('response');

        $puntovta = $request->get('puntovta', 'get');
        $notacredito = $request->get('term', 'get');

        $sql = "SELECT notacredito AS id, notacredito AS value FROM notasdecredito WHERE estado = 'OK' and notacredito LIKE '?' and puntovta = '?' ";
        $consulta = $database->parse($sql, '%' . $notacredito . '%' , $puntovta);
        $results = $database->getRows($consulta);

        $varia = utf8_decode(json_encode($results));

        echo $varia;
    }

    function getNotaCredito() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>		

        $notacredito = $request->get('term', 'get');

        $sql = "SELECT * FROM notasdecredito WHERE notacredito ='?' ";

        $consulta = $database->parse($sql, $notacredito);
        $codigo = $database->getRow($consulta);

        echo json_encode($codigo);
    }

}

?>
