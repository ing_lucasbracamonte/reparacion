<?php

class ControllerMisdatos extends Controller {

    var $error = array();

    function index() {
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        $user = & $this->locator->get('user');
        $url = & $this->locator->get('url');

        $template->set('title', 'Mis datos');

        $template->set('content', $this->getForm());

        $template->set($module->fetch());



        $response->set($template->fetch('layout.tpl'));
    }

    function getForm() {
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $user = & $this->locator->get('user');
        $session = & $this->locator->get('session');
        $template = & $this->locator->get('template');
        $view = $this->locator->create('template');

        $view->set('heading_title', 'Mis datos');

        if ($request->get('completardatos', 'post') == 'si') {
            $view->set('heading_description', 'Informaci&oacute;n Personal: Debe completar los datos faltantes antes de ingresar al sistema');
            $view->set('cancel', $url->ssl('logout'));
        } else {
            $view->set('heading_description', 'Complete o modifique sus datos personales');
            $view->set('cancel', $this->getDefault());
        }

        //$view->set('action', $url->ssl('misdatos', 'update'));

        $view->set('entry_persona', 'Documento:');
        $view->set('entry_apellido', 'Apellido:');
        $view->set('entry_nombre', 'Nombre:');
        $view->set('entry_domicilio', 'Domicilio:');
        $view->set('entry_localidad', 'Localidad:');
        $view->set('entry_telefono', 'T.E.:');
        $view->set('entry_celular', 'Celular:');
        $view->set('entry_mail', 'E-mail:');
        $view->set('entry_remail', 'Confirma E-mail:');
        $view->set('entry_fecha', 'Fecha nacimiento:');
        $view->set('entry_newpassword', 'Nueva contraseña:');
        $view->set('entry_newrepassword', 'Nueva Confirma Contraseña:');

        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        $template->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');
        $view->set('error_persona', @$this->error['persona']);
        $view->set('error_domicilio', @$this->error['domicilio']);
        $view->set('error_apellido', @$this->error['apellido']);
        $view->set('error_nombre', @$this->error['nombre']);
        $view->set('error_telefono', @$this->error['telefono']);
        $view->set('error_celular', @$this->error['celular']);
        $view->set('error_localidad', @$this->error['localidad']);
        $view->set('error_mail', @$this->error['mail']);
        $view->set('error_remail', @$this->error['remail']);
        $view->set('tab_general', $language->get('tab_general'));
        $view->set('error_newpassword', @$this->error['newpassword']);
        $view->set('error_newrepassword', @$this->error['newrepassword']);
        $view->set('error_texto_error', @$this->error['texto_error']);

        if (($user->getPERSONA()) && (!$request->isPost())) {
            $consulta = "SELECT  *  FROM personas  WHERE persona = '?'";
            $persona_info = $database->getRow($database->parse($consulta, $user->getPERSONA()));
        }
        if ($request->has('persona', 'post')) {
            $view->set('persona', $user->getPERSONA());
            $persona_id = $request->get('persona', 'post');
        } else {
            $view->set('persona', @$persona_info['persona']);
            $persona_id = @$persona_info['persona'];
        }

        if ($request->has('apellido', 'post')) {
            $view->set('apellido', $request->get('apellido', 'post'));
        } else {
            $view->set('apellido', $persona_info['apellido']);
        }

        if ($request->has('nombre', 'post')) {
            $view->set('nombre', $request->get('nombre', 'post'));
        } else {
            $view->set('nombre', $persona_info['nombre']);
        }

        $view->set('entry_puntovta', 'Punto de Venta:');
        if ($request->has('puntovtaasignado', 'post')) {
            $view->set('puntovtaasignado', $request->get('puntovtaasignado', 'post'));
            $puntovta = $request->get('puntovtaasignado', 'post');
        } else {
            //va el punto de venta que tiene asignado el vendedor
            //$ptovtaasignado = $database->getRow("SELECT puntovtaasignado FROM personas where persona = '".$user->getPERSONA()."' ");

            $view->set('puntovtaasignado', @$objeto_info['puntovtaasignado']);
            $puntovta = @$objeto_info['puntovtaasignado'];
        }

        $view->set('puntosdeventa', $database->getRows("SELECT vpv.persona, pv.puntovta, pv.descripcion AS descpuntoventa FROM vendedorespuntodeventa vpv LEFT JOIN puntosdeventa pv ON vpv.puntovta = pv.puntovta WHERE vpv.persona = '" . $user->getPERSONA() . "' ORDER BY descpuntoventa ASC"));


        if ($request->has('domicilio', 'post')) {
            $view->set('domicilio', $request->get('domicilio', 'post'));
        } else {
            $view->set('domicilio', @$persona_info['domicilio']);
        }

        if ($request->has('localidad', 'post')) {
            $view->set('localidad', $request->get('localidad', 'post'));
        } else {
            $view->set('localidad', @$persona_info['localidad']);
        }

        if ($request->has('celular', 'post')) {
            $view->set('celular', $request->get('celular', 'post'));
        } else {
            $view->set('celular', @$persona_info['celular']);
        }

        if ($request->has('mail', 'post')) {
            $view->set('mail', $request->get('mail', 'post'));
        } else {
            $view->set('mail', @$persona_info['mail']);
        }

        if ($request->has('remail', 'post')) {
            $view->set('remail', $request->get('remail', 'post'));
        } else {
            $view->set('remail', @$persona_info['mail']);
        }

        if ($request->has('fechanacimiento', 'post')) {
            $view->set('fechanacimiento', $request->get('fechanacimiento', 'post'));
        } else {
            if ($persona_info['fechanacimiento'] != '')
                $view->set('fechanacimiento', date('d/m/Y', strtotime(@$persona_info['fechanacimiento'])));
            else
                $view->set('fechanacimiento', '');
        }

        if ($request->has('newpassword', 'post')) {
            $view->set('newpassword', $request->get('newpassword', 'post'));
        } else {
            $view->set('newpassword', '');
        }

        if ($request->has('newrepassword', 'post')) {
            $view->set('newrepassword', $request->get('newrepassword', 'post'));
        } else {
            $view->set('newrepassword', '');
        }

        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('misdatos', 'update'));
        $view->set('actionBoton', $request->get('action'));
        $view->set('cancel', $url->ssl('home'));
        // </editor-fold>

        return $view->fetch('content/seguridad_mis_datos.tpl');
    }

    function validateForm() {
        $request = & $this->locator->get('request');
        $mail = & $this->locator->get('mail');
        $user = & $this->locator->get('user');
        $database = & $this->locator->get('database');
        $common = & $this->locator->get('common');
        $mensaje = '';

        $errores = '';
        
        if ((strlen($request->get('persona', 'post')) == 0)) {
            //$this->error['persona'] = 'Debe ingresar el n&uacute;mero de documento';
            $errores .= 'Debe ingresar el n&uacute;mero de documento. <br>';
        }

        if (strlen($request->get('persona', 'post')) < 7 || !is_numeric($request->get('persona', 'post'))) {
            //$this->error['persona'] = 'No es un n&uacute;mero de documento v&aacute;lido.';
            $errores .= 'No es un n&uacute;mero de documento v&aacute;lido.. <br>';
        }

        if ((strlen($request->get('apellido', 'post')) < 2) || (strlen($request->get('apellido', 'post')) > 100)) {
            //$this->error['apellido'] = 'Debe ingresar el apellido';
            $errores .= 'Debe ingresar el apellido. <br>';
        }

        if ((strlen($request->get('nombre', 'post')) < 2) || (strlen($request->get('nombre', 'post')) > 100)) {
            //$this->error['nombre'] = 'Debe ingresar el nombre';
            $errores .= 'Debe ingresar el nombre. <br>';
        }

//		if ((strlen($request->get('domicilio', 'post')) == 0)) {
//			$this->error['domicilio'] = 'Debe ingresar el domicilio';
//		}
//		if ($request->has('localidad', 'post')) {
//			$view->set('localidad', $request->get('localidad', 'post'));
//		} else {
//			$view->set('localidad', @$region_info['localidad']);
//		}

        if (!$request->has('fechanacimiento', 'post')) {
            //$this->error['fechanacimiento'] = 'Debe selccionar una fecha de nacimiento';
            $errores .= 'Debe selccionar una fecha de nacimiento. <br>';
        }

//		if ((strlen($request->get('telefono', 'post')) == 0)) {
//			$this->error['telefono'] = 'Debe ingresar el tel&eacute;fono';
//		}

        if ((strlen($request->get('mail', 'post')) > 0)) {
            if (!$common->validarDuplicado($request->get('mail', 'post'), $database, $mensaje, $user->getPERSONA())) {
                //$this->error['mail'] = $mensaje;
                $errores .= $mensaje . ' <br>';
            }

            if ($request->get('remail', 'post') != $request->get('mail', 'post')) {
                //$this->error['remail'] = 'El mail y la confirmaci&oacute;n no coinciden';
                $errores .= 'El mail y la confirmaci&oacute;n no coinciden. <br>';
            }
        }


        if ((strlen($request->get('newpassword', 'post')) > 0)) {
            if ((strlen($request->get('newrepassword', 'post')) == 0)) {
                //$this->error['newrepassword'] = 'Debe ingresar la confirmacion de la contraseña';
                $errores .= 'Debe ingresar la confirmacion de la contraseña. <br>';
            }
            if ($request->get('newrepassword', 'post') != $request->get('newpassword', 'post')) {
                //$this->error['newrepassword'] = 'La contraseña y la confirmacion no coinciden';
                $errores .= 'La contraseña y la confirmacion no coinciden. <br>';
            }
        }

        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        
        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function completardatos() {
        $response = & $this->locator->get('response');
        $request = & $this->locator->get('request');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        $user = & $this->locator->get('user');
        $url = & $this->locator->get('url');

        $template->set('title', 'Mis datos');

        $request->set('completardatos', 'si', 'post');

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $user = & $this->locator->get('user');
        $common = & $this->locator->get('common');
        // </editor-fold>
        $template->set('title', 'Mis datos');

        if (($request->isPost()) && ($this->validateForm())) {

            //VALIDO QUE SI EL MAIL CAMBIO DEBE VOLVER A VALIDAR
            $consulta = "SELECT mail FROM personas WHERE persona = '?'";
            $persona_mail = $database->getRow($database->parse($consulta, $request->get('persona', 'post')));
            $mail_confirmado = 0;
            if ($request->get('mail', 'post') == $persona_mail['mail']) {
                $mail_confirmado = 1;
            }
            // <editor-fold defaultstate="collapsed" desc="FECHA">
            $fecha_d = '';
            if ($request->get('fechanacimiento', 'post') != '') {
                $dated = explode('/', $request->get('fechanacimiento', 'post'));
                $fecha_d = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
            }
            // </editor-fold>

            if ($request->get('newpassword', 'post') != '') {
                $clave = $common->getEcriptar($request->get('newpassword', 'post'));
                $sql = "UPDATE personas SET clave = '?', nombre = '?',apellido = '?', fechanacimiento = NULLIF('?',''),  localidad =NULLIF('?',''), celular = '?',  mail ='?', mail_confirmado ='?',puntovtaasignado='?', usuario = '?' WHERE persona = '?'";
                $sql = $database->parse($sql, $clave, strtoupper($request->get('nombre', 'post')), strtoupper($request->get('apellido', 'post')), $fecha_d, $request->get('localidad', 'post'), $request->get('celular', 'post'), $request->get('mail', 'post'), $mail_confirmado, $request->get('puntovtaasignado', 'post'), $user->getPERSONA(), $user->getPERSONA());
                $database->query($sql);

                $cache->delete('persona');

                $session->set('message', 'Se han actualizado sus datos personales');

                //ENVIAR MAIL SI CAMBIO EL MAIL PARA VALIDAR

                $response->redirect($url->ssl('home'));
            } else {
                $sql = "UPDATE personas SET  nombre = '?',apellido = '?', fechanacimiento = NULLIF('?',''), localidad =NULLIF('?',''), celular = '?', mail ='?', mail_confirmado ='?',puntovtaasignado='?', usuario = '?' WHERE persona = '?'";
                $sql = $database->parse($sql, strtoupper($request->get('nombre', 'post')), strtoupper($request->get('apellido', 'post')), $fecha_d, $request->get('localidad', 'post'), $request->get('celular', 'post'), $request->get('mail', 'post'), $mail_confirmado, $request->get('puntovtaasignado', 'post'), $user->getPERSONA(), $user->getPERSONA());
                $database->query($sql);

                $cache->delete('persona');

                $session->set('message', 'Se han actualizado sus datos personales');

                //ENVIAR MAIL SI CAMBIO EL MAIL PARA VALIDAR

                $response->redirect($url->ssl('home'));
            }
        }
        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

}

?>
