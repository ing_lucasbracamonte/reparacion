<?php

class ControllerLogout extends Controller {

    function index() {
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $session = & $this->locator->get('session');

        $user->logout();
        $response->redirect($url->ssl(DEFAULT_HOMEPAGE));
    }

}

?>