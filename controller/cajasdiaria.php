<?php

class ControllerCajasdiaria extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'GESTIÓN DE CAJAS DIARIA');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
         $request = & $this->locator->get('request');
        // </editor-fold>
         // <editor-fold defaultstate="collapsed" desc="LIMPIA FILTRO Y VARIBLES DE SESSION">

        if ($request->get('filtra') == NULL) {

            $session->set('cajasdiaria.search', '');
            $session->set('cajasdiaria.sort', '');
            $session->set('cajasdiaria.order', '');
            $session->set('cajasdiaria.page', '');

            $view->set('search', '');
            $view->set('cajasdiaria.search', '');
            
            $view->set('estadocaja', '');
            $view->set('cajasdiaria.estadocaja', '');
            
            $cache->delete('cajasdiaria');
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">

        $cols = array();

        $cols[] = array(
            'name' => 'CAJA',
            'sort' => 'cajadiaria',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'APERTURA',
            'sort' => 'fechaapertura',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'CIERRE',
            'sort' => 'fechacierre',
            'align' => 'left'
        );
        $cols[] = array(
            'name' => '$ APERTURA',
            'sort' => 'montoapertura',
            'align' => 'left'
        );
        $cols[] = array(
            'name' => '$ SALDO',
            'sort' => 'saldo',
            'align' => 'left'
        );
        $cols[] = array(
            'name' => 'ESTADO',
            'sort' => 'estadocaja',
            'align' => 'left'
        );

                $cols[] = array(
			'name'  => 'PUNTO VTA',
			'sort'  => 'puntovta',
			'align' => 'left'
		);

        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'cajadiaria',
            'fechaapertura',
            'fechacierre',
            'montoapertura',
            'saldo',
            'puntovta',
            'estadocaja'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        set_time_limit(0);

        if (!$session->get('cajasdiaria.search')) {
            $sql = "SELECT * FROM vw_grilla_cajasdiaria  ";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT * FROM vw_grilla_cajasdiaria WHERE cajadiaria LIKE '%' . $session->get('cajasdiaria.search') . '%'  ";
            $conca = " AND ";
        }
        
        //si no es excento el del local abierto
        if ($user->getExento() != 1) {
                $sql .=  $conca." puntovta = '".$user->getPuntovtaasignado()."' ";
                $conca = " AND ";
        }
        
        $estado = $session->get('cajasdiaria.estadocaja');
        if ($session->get('cajasdiaria.estadocaja') != '-1' && $session->get('cajasdiaria.estadocaja') != '' ) {
            $sql .= $conca." estadocaja = '".$session->get('cajasdiaria.estadocaja')."' ";
            $conca = " AND ";
        }
        

        if (in_array($session->get('cajasdiaria.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('cajasdiaria.sort') . " " . (($session->get('cajasdiaria.order') == 'desc') ? 'desc' : 'asc');
        } else {
            $sql .= " ORDER BY estadocaja, fechaapertura DESC";
        }

//		$consul = $database->splitQuery($database->parse($sql,'%' . $session->get('cajasdiaria.search') . '%','%' . $session->get('cajasdiaria.search') . '%'), $session->get('cajasdiaria.page'), $config->get('config_max_rows'));
//
//                $results = $database->getRows($consul);

        //$consulta = $database->parse($sql,);
        $results = $database->getRows($sql);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['cajadiaria'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => date('d/m/Y H:i', strtotime(@$result['fechaapertura'])),
                'align' => 'left',
                'default' => 0
            );

            $fechacierre = '-';
            if (strtotime(@$result['fechacierre']) > 0) {
                $fechacierre = date('d/m/Y H:i', strtotime(@$result['fechacierre']));
            }
            $cell[] = array(
                'value' => $fechacierre,
                'align' => 'left',
                'default' => 0
            );


            $cell[] = array(
                'value' => @$result['montoapertura'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['saldo'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['estadocaja'],
                'align' => 'left',
                'default' => 0
            );
            
            $cell[] = array(
                'value' => @$result['descpuntovta'],
                'align' => 'left',
                'default' => 0
            );

            $action = array();

            
//            if ($user->hasPermisos($user->getPERSONA(), 'cajasdiaria', 'M') && $result['estadocaja'] == 'ABIERTA') {
//                $action[] = array(
//                    'icon' => 'img/iconos-01.png',
//                    'class' => 'fa  fa-unlock-alt',
//                    'text' => 'Cerrar caja',
//                    'prop_a' => array('href' => $url->ssl('cajasdiaria', 'update', array('cajadiaria' => $result['cajadiaria'])))
//                );
//            }

//            if ($user->hasPermisos($user->getPERSONA(), 'cajasdiaria', 'B')) {
//                $action[] = array(
//                    'icon' => 'img/iconos-11.png',
//                    'text' => $language->get('button_delete'),
//                    'class' => 'fa fa-fw fa-trash-o',
//                    'prop_a' => array('href' => "javascript:ActionDelete('" . $url->ssl('cajasdiaria', 'delete', array('cajadiaria' => $result['cajadiaria'])) . "');")
//                );
//            }

            if ($user->hasPermisos($user->getPERSONA(), 'movimientoscajadiaria', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw fa-wrench',
                    'text' => $language->get('button_consult'),
                    'prop_a' => array('href' => $url->ssl('movimientoscajadiaria', 'index', array('cajadiaria' => $result['cajadiaria'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'cajasdiaria', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw  fa-print',
                    'text' => 'Detalle de caja PDF',
                    'target' => ' target="_blank" ',
                    'prop_a' => array('href' => $url->ssl('cajasdiaria', 'exportarPDF', array('cajadiaria' => $result['cajadiaria'])))
                );
                
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw  fa-download',
                    'text' => 'Detalle de caja XLS con venta',
                    'prop_a' => array('href' => $url->ssl('cajasdiaria', 'exportXls', array('cajadiaria' => $result['cajadiaria'], 'tipofiltro' => 'CONVENTA')))
                );
                
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw  fa-download',
                    'text' => 'Detalle de caja XLS sin venta',
                    'prop_a' => array('href' => $url->ssl('cajasdiaria', 'exportXls', array('cajadiaria' => $result['cajadiaria'], 'tipofiltro' => 'SINVENTA')))
                );
            }

            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        //<editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('cajasdiaria.page'));

        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        $view->set('heading_title', 'CAJAS DIARIAS');
        $view->set('placeholder_buscar', 'BUSCA POR ID O ESTADO');

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        //$view->set('consultaVacia', $url->ssl('consulta','consulta_vacia'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        //$view->set('button_consultavacia', "Vacía");
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('cajasdiaria.search'));
        $view->set('estadocaja', $session->get('cajasdiaria.estadocaja'));
        $view->set('sort', $session->get('cajasdiaria.sort'));
        $view->set('order', $session->get('cajasdiaria.order'));
        $view->set('page', $session->get('cajasdiaria.page'));

        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $view->set('titulo_ventana', '::. Exportar');

        $view->set('entry_modificar', "Modificar");
        $view->set('entry_agregar', "Agregar");

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('cajasdiaria', 'page'));
        $view->set('list', $url->ssl('cajasdiaria'));

        if ($user->hasPermisos($user->getPERSONA(), 'cajasdiaria', 'A')) {
            $view->set('insert', $url->ssl('movimientoscajadiaria', 'insert'));
        }
        
//        if ($user->hasPermisos($user->getPERSONA(), 'cajasdiaria', 'A')) {
//            $view->set('insert', $url->ssl('cajasdiaria', 'insert'));
//        }
        if ($user->hasPermisos($user->getPERSONA(), 'cajasdiaria', 'A')) {
            $view->set('insertImport', $url->ssl('cajasdiaria', 'insertImport'));
        }

        if ($user->hasPermisos($user->getPERSONA(), 'cajasdiaria', 'C')) {
            $view->set('exportXls', $url->ssl('cajasdiaria', 'exportXls'));
        }

        //$view->set('addPais', $url->ssl('cajasdiaria','insert'));
        // $view->set('updatePersona', $url->ssl('paciente','update', array('persona' => $result['persona'])));
        //$view->set('updatePais', $url->ssl('cajasdiaria','update'));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_cajasdiaria.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">
		if ($request->has('estadocaja', 'post')) {
                    $session->set('cajasdiaria.estadocaja',$request->get('estadocaja','post'));              
                }

        if ($request->has('search', 'post')) {
            $session->set('cajasdiaria.search', $request->get('search', 'post'));
        }

        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('cajasdiaria.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('cajasdiaria.order', (($session->get('cajasdiaria.sort') == $request->get('sort', 'post')) && ($session->get('cajasdiaria.order') == 'asc')) ? 'desc' : 'asc');
        }

        if ($request->has('sort', 'post')) {
            $session->set('cajasdiaria.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('cajasdiaria', 'index', array('filtra' => '1')));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $template = & $this->locator->get('template');
        $view = $this->locator->create('template');
        $user = $this->locator->create('user');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('heading_title', 'DATOS DE LA CAJA');

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $template->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('tab_general', $language->get('tab_general'));

        $view->set('cajadiaria', $request->get('cajadiaria'));

        if (($request->get('cajadiaria')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM vw_grilla_cajasdiaria WHERE cajadiaria = '" . $request->get('cajadiaria') . "' ";
            $objeto_info = $database->getRow($consulta);
        }

        if ($request->has('cajadiaria', 'post')) {
            $view->set('cajadiaria', $request->get('cajadiaria', 'post'));
            $objeto_id = @$objeto_info['cajadiaria'];
        } else {
            $view->set('cajadiaria', $request->get('cajadiaria', 'get'));
            $objeto_id = @$objeto_info['cajadiaria'];
        }

        $view->set('entry_fechaapertura', 'Fecha Apertura:');
        if ($request->has('fechaapertura', 'post')) {
            $view->set('fechaapertura', $request->get('fechaapertura', 'post'));
        } else {
            if ($request->has('fechaapertura', 'get')) {
                $view->set('fechaapertura', date('d/m/Y', strtotime(@$objeto_info['fechaapertura'])));
            } else {
                $view->set('fechaapertura', date('d/m/Y'));
            }
        }


        $view->set('entry_montoapertura', 'Monto apertura:');
        if ($request->get('action') != 'insert') {
            if ($request->has('montoapertura', 'post')) {
            $view->set('montoapertura', $request->get('montoapertura', 'post'));
            } else {
                $view->set('montoapertura', @$objeto_info['montoapertura']);
            }
        }
        else {
            $sql = "SELECT saldo FROM cajasdiaria WHERE estadocaja = 'CERRADA' AND puntovta ='".$user->getPuntovtaasignado()."' ORDER BY fechaapertura desc LIMIT 1";
            $caja = $database->getRow($sql);
            if($caja['saldo']>0){
                $view->set('montoapertura', $caja['saldo']);
            }
            else{
                $view->set('montoapertura', '0');
            }    
        }
        
        $consulta = "SELECT DISTINCT * FROM puntosdeventa WHERE puntovta = '" . $user->getPuntovtaasignado() . "' ";
         $puntovta_info = $database->getRow($consulta);
            
        $view->set('entry_puntovta', 'Punto de Venta:');
        if ($request->has('puntovta', 'post')) {
            $view->set('puntovta', $request->get('puntovta', 'post'));
        } else {
            if (@$objeto_info['puntovta']) {
               $view->set('puntovta', @$objeto_info['puntovta']); 
            }
            else{
                $view->set('puntovta', $puntovta_info['puntovta']);
            }
        }
        
        if ($request->has('descpuntovta', 'post')) {
            $view->set('descpuntovta', $request->get('descpuntovta', 'post'));
        } else {
            if (@$objeto_info['descpuntovta']) {
               $view->set('descpuntovta', @$objeto_info['descpuntovta']);
            }
            else{
                $view->set('descpuntovta', $puntovta_info['descripcion']);
            }
            
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $template->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');
        $view->set('error_texto_error', @$this->error['texto_error']);
        //$view->set('error_titulo', @$this->error['titulo']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        //$view->set('action', $url->ssl('cajasdiaria', $request->get('action'), array('modelo' => $request->get('modelo'))));
        $view->set('actionBoton', $request->get('action'));
        $view->set('cancel', $url->ssl('cajasdiaria'));
        // </editor-fold>

        return $view->fetch('content/cajadiaria.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $user = & $this->locator->get('user');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        //SOLO PUEDE HABER ABIERTA UNA SOLA CAJA
        $pv = $request->get('puntovta', 'post');
        if ($request->get('puntovta', 'post')) {
            if ($request->get('action', 'get') == 'insert') {
                $sql = "SELECT count(*) as total FROM cajasdiaria WHERE estadocaja = 'ABIERTA' AND puntovta ='".$request->get('puntovta', 'post')."' ";
                $caja = $database->getRow($sql);

                if ($caja['total'] > 0) {
                    $errores .= 'Ya hay una caja abierta en el punto de vta. <br>';
                }
            }
        }
        else{
            $errores .= 'Debe tener un punto de vta asignado para abrir una caja. <br>'; 
        }
        
        if ((strlen($request->get('fechaapertura', 'post')) != 10)) {
            $errores .= 'Debe ingresar una fecha. <br>';
        }
//        if ((strlen($request->get('montoapertura', 'post')) == 0)) {
//            $errores .= 'Debe ingresar un monto. <br>';
//        }

        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';

        //SE VA A PODER ELIMINAR UNA CAJA SI NO TIENE MOVIMIENTOS HECHOS 
        $consulta_info = $database->getRow("SELECT COUNT(*) as total FROM vw_grilla_cajadiariamovimientos WHERE cajadiaria = '" . $request->get('cajadiaria') . "' ");
        if (@$consulta_info['total'] != 0) {
            $errores .= 'No es posible eliminar esta caja, tiene movimientos asignados.';
        }

        if ($errores != '') {
            $this->error['message'] = $errores;
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $user = & $this->locator->get('user');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'CAJAS');

        if (($request->isPost()) && ($this->validateForm())) {

            // <editor-fold defaultstate="collapsed" desc="FECHA">
           

            // </editor-fold>
            //$sql2 = "INSERT IGNORE INTO articulos SET tipoproducto='".$request->get('tipoproducto', 'post')."', modelo = '".$request->get('auto_modelo_marca', 'post')."', estadoarticulo = 'DI', tipogama='".$request->get('tipogama', 'post')."', mnu='".$request->get('tipogama', 'post')."', concoddebarra='".$request->get('concoddebarra', 'post')."', precio='".$request->get('precio', 'post')."', au_usuario='".$user->getPERSONA()."',au_accion='A',au_fecha_hora=NOW()";
            $sql = "INSERT IGNORE INTO cajasdiaria SET fechaapertura=now(), montoapertura = '?', estadocaja = 'ABIERTA', saldo='?',puntovta='?', au_usuario='?',au_accion='A',au_fecha_hora=NOW()";
            $consulta = $database->parse($sql, $request->get('montoapertura', 'post'), $request->get('montoapertura', 'post'), $request->get('puntovta', 'post'), $user->getPERSONA());
            $database->query($consulta);

            $id = $database->getLastId();
            $cache->delete('cajasdiaria');
            $session->set('message', 'Se agreg&oacute; la caja: ' . $id);

            $response->redirect($url->ssl('cajasdiaria'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $user = & $this->locator->get('user');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos de la caja');

        $sql = "SELECT * FROM cajasdiaria WHERE cajadiaria = '" . $request->get('cajadiaria', 'get') . "' ";
        $caja = $database->getRow($sql);

        $sql = "UPDATE cajasdiaria SET fechacierre=NOW(), estadocaja = 'CERRADA', au_usuario='?',au_accion='M',au_fecha_hora=NOW() WHERE cajadiaria='?' ";
        $consulta = $database->parse($sql, $user->getPERSONA(), $request->get('cajadiaria', 'get'));
        $database->query($consulta);

        $cache->delete('cajasdiaria');

        $session->set('message', "Se ha cerrado la caja número " . $request->get('cajadiaria', 'post') . ".");

        $response->redirect($url->ssl('cajasdiaria'));
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos de la caja');
        if (($request->isPost())) {

            $response->redirect($url->ssl('cajasdiaria'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        if (($request->get('cajadiaria')) && ($this->validateDelete())) {

            $id = $request->get('cajadiaria', 'get');

            $sql = "DELETE FROM cajasdiaria WHERE cajadiaria = '" . $id . "' ";
            $database->query($sql);

            $cache->delete('cajasdiaria');

            $session->set('message', 'Se ha eliminado la caja: ' . $id);

            $response->redirect($url->ssl('cajasdiaria'));
        }

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function exportXls() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        // </editor-fold>

        set_time_limit(0);

        //** PHPExcel **//
        require_once (DIR_INCLUDES . '/library/phpexcel/PHPExcel.php');

        date_default_timezone_set('America/Argentina/Buenos_Aires');
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set properties of the file
        $objPHPExcel->getProperties()->setCreator("Accesorios")
                ->setLastModifiedBy("Accesorios")
                ->setTitle("Planilla web exportada: " . date('d-m-Y'))
                ->setSubject("Planilla web exportada: " . date('d-m-Y'));

        
         if ($request->has('cajadiaria', 'post')) {
            $caja_id = $request->get('cajadiaria', 'post');
        } else {
            if ($request->has('cajadiaria', 'get')) {
                $caja_id = $request->get('cajadiaria', 'get');
            }
        }
        
        $tipofiltro = $request->get('tipofiltro', 'get');
         

        $consulta = "SELECT DISTINCT * FROM vw_grilla_cajasdiaria WHERE cajadiaria = '?'";
        $caja_info = $database->getRow($database->parse($consulta, $caja_id));

        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">
        

        $puntovta = $caja_info['descpuntovta'];
        $fechaapertura = date('d/m/Y H:i', strtotime($caja_info['fechaapertura']));
        $fechacierre = '-';
            if (strtotime(@$result['fechacierre']) > 0) {
                $fechacierre = date('d/m/Y H:i', strtotime($caja_info['fechacierre']));
            }
            
        $montoapertura = $caja_info['montoapertura'];
        $saldo = $caja_info['saldo'];
        $estado = $caja_info['estadocaja'];
        $nombreusuario = $caja_info['nombreusuario'];

        //COLUMNA A
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(11);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'CAJA NRO');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2', 'FECHA APERTURA');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        $objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A3', 'MONTO APERTURA');
        $objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        $objPHPExcel->getActiveSheet()->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('A4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A4', 'ESTADO');
        $objPHPExcel->getActiveSheet()->getStyle('A4')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

         //COLUMNA B
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(11);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', $caja_id);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        $objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B2', $fechaapertura);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        $objPHPExcel->getActiveSheet()->getStyle('B3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('B3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B3', $montoapertura);
        $objPHPExcel->getActiveSheet()->getStyle('B3')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        $objPHPExcel->getActiveSheet()->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('B4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B4', $estado);
        $objPHPExcel->getActiveSheet()->getStyle('B4')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        //COLUMNA D
         $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(11);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', 'PUNTO VTA');
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        
        $objPHPExcel->getActiveSheet()->getStyle('D2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('D2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D2', 'FECHA CIERRE');
        $objPHPExcel->getActiveSheet()->getStyle('D2')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        $objPHPExcel->getActiveSheet()->getStyle('D3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('D3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D3', 'SALDO');
        $objPHPExcel->getActiveSheet()->getStyle('D3')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        $objPHPExcel->getActiveSheet()->getStyle('D4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('D4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D4', 'USUARIO APERTURA');
        $objPHPExcel->getActiveSheet()->getStyle('D4')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

         //COLUMNA E
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(11);
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', $puntovta);
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        
        $objPHPExcel->getActiveSheet()->getStyle('E2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('E2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E2', $fechaapertura);
        $objPHPExcel->getActiveSheet()->getStyle('E2')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        $objPHPExcel->getActiveSheet()->getStyle('E3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('E3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E3', $$saldo);
        $objPHPExcel->getActiveSheet()->getStyle('E3')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        $objPHPExcel->getActiveSheet()->getStyle('E4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('E4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E4', $nombreusuario);
        $objPHPExcel->getActiveSheet()->getStyle('E4')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        
        //genero las columnas del excel
        $letra = 'A';
        $fila_inicial = 8; //fila desde donde empiezo a cargar la tabla en el excel. Arriba va el encabezado
        
        $objPHPExcel->getActiveSheet()->getStyle('A6:F7')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //COLUMNA A
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(11);
        $objPHPExcel->getActiveSheet()->getStyle('A6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A6', 'MOVIMIENTOS');
        $objPHPExcel->getActiveSheet()->getStyle('A6')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        $objPHPExcel->getActiveSheet()->mergeCells('A6:F6');
        
        //COLUMNA A
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(11);
        $objPHPExcel->getActiveSheet()->getStyle('A7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('A7')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A7', 'TIPO');
        $objPHPExcel->getActiveSheet()->getStyle('A7')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        //COLUMNA B
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getStyle('B7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('B7')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B7', 'MONTO');
        $objPHPExcel->getActiveSheet()->getStyle('B7')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA C
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(22);
        $objPHPExcel->getActiveSheet()->getStyle('C7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('C7')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C7', 'RUBRO');
        $objPHPExcel->getActiveSheet()->getStyle('C7')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA D
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
        $objPHPExcel->getActiveSheet()->getStyle('D7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('D7')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D7', 'SUBRUBRO');
        $objPHPExcel->getActiveSheet()->getStyle('D7')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA E
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getStyle('E7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('E7')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E7', 'OBSERVACION');
        $objPHPExcel->getActiveSheet()->getStyle('E7')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA F
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getStyle('F7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('F7')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F7', 'RESPONSABLE');
        $objPHPExcel->getActiveSheet()->getStyle('F7')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        
        // </editor-fold>
        
        //CARGA PRODUCTOS
        //// <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        set_time_limit(0);

        $consult = "SELECT * FROM vw_grilla_cajadiariamovimientos WHERE cajadiaria='" . $caja_id . "' ";
        
        
        if ($tipofiltro == 'SINVENTA') {
            $consult = "SELECT * FROM vw_grilla_cajadiariamovimientos WHERE cajadiaria='" . $caja_id . "' AND descrubro != 'VENTA' ";
        } else {
            $consult = "SELECT * FROM vw_grilla_cajadiariamovimientos WHERE cajadiaria='" . $caja_id . "' ";
        }
//
//        if ($session->get('cajasdiaria.tipoproducto') != '-1' && $session->get('cajasdiaria.tipoproducto') != '') {
//            $sql .= $conca . " tipoproducto = '" . $session->get('cajasdiaria.tipoproducto') . "'  ";
//        }

        $consult .= " ORDER BY  fechamovimiento ASC";

        $movimientoscaja = $database->getRows($consult);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="LLANAR LOS DATOS">

        $num = $fila_inicial;

        foreach ($movimientoscaja as $result) {

             // <editor-fold defaultstate="collapsed" desc="VARIABLES">
            $fecha = date('d/m/Y', strtotime($result['fechamovimiento']));
            $tipo = $result['tipomovimiento'];
            $monto = $result['importe'];
            $rubro = ($result['descrubro'] ? $result['descrubro'] : '-');
            $subrubro = ($result['descsubrubro'] ? $result['descsubrubro'] : '-');
            $observacion = $result['observacion'];
            $responsable = $result['nombreusuario'];

            // </editor-fold>

            //COLUMNA A
            $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $num, $tipo);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            //COLUMNA B
            $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $num, $monto);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            //COLUMNA C
            $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $num, $rubro);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA D
            $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' . $num, $subrubro);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA E
            $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' . $num, $observacion);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            //HACE QUE EL NUMERO DE VENTA SALGA EN FORMATO CORRECTO
            if (is_numeric ($observacion)) $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getNumberFormat()->setFormatCode('0000');
                    
            
            //COLUMNA F
            $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' . $num, $responsable);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
           
            $num++;
        }

        //SEGUIR EN COMPLETAR EL EXCEL
        //POR CADA INSCRIPTO IR BUSCANDO CADA RESPUESTA Y COMPLETAR SEGUN CORRESPONDA
        // </editor-fold>
        // Nombre de la hoja del libro
        $objPHPExcel->getActiveSheet()->setTitle('ExportArticulos');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Save Excel 2007 file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');

        // It will be called file.xls | esta linea hace que sea una descarga y no una pestaña aparte
        header('Content-Disposition: attachment; filename="DetalleCaja.xls"');

        header('Cache-Control: max-age=0');

        // Write file to the browser
        $objWriter->save('php://output');
    }

    function exportarPDF() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $request = & $this->locator->get('request');
        // </editor-fold>

        set_time_limit(0);

        if ($request->has('cajadiaria', 'post')) {
            $caja_id = $request->get('cajadiaria', 'post');
        } else {
            if ($request->has('cajadiaria', 'get')) {
                $caja_id = $request->get('cajadiaria', 'get');
            }
        }

        $consulta = "SELECT DISTINCT * FROM vw_grilla_cajasdiaria WHERE cajadiaria = '?'";
        $caja_info = $database->getRow($database->parse($consulta, $caja_id));

        $consult = "SELECT * FROM vw_grilla_cajadiariamovimientos WHERE cajadiaria='" . $caja_id . "' ORDER BY  fechamovimiento ASC";
        $movimientoscaja = $database->getRows($consult);

        // <editor-fold defaultstate="collapsed" desc="config PDF">
        // Include the main TCPDF library (search for installation path).
        // Include the main TCPDF library (search for installation path).
        define('FPDF_FONTPATH', 'font/');
        require('library/pdf/tcpdf/tcpdf.php');
        require_once('library/pdf/tcpdf/tcpdf_include.php');

        $con = PDF_PAGE_ORIENTATION;
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF8 sin BOM', false);


        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('STC');
        $pdf->SetTitle('');
        $pdf->SetSubject(' ');
        $pdf->SetKeywords(' ');

        // set default header data
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        //LA IMAGEN DEBE ESTAR EN template\default\image
        $pdf->SetHeaderData('cabecera PDF-reporte.jpg', 173, '', '');

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font - conjunto predeterminado fuentemonoespaciada
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetMargins(20, 22, 20);
        //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetHeaderMargin(5);
        $pdf->SetFooterMargin(15);

        // set auto page breaks - establecer saltos de página automático
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // establecemos la medida del interlineado
        //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        //$pdf->SetDefaultMonospacedFont(30);
        // ---------------------------------------------------------
        // set font
        $pdf->SetFont('helvetica', '', 9);


        //$pdf->Image('images/sistema.png', 50, 50, 100, '', '', 'http://www.tcpdf.org', '', false, 300);
// </editor-fold>  
        // add a page
        $pdf->AddPage();

        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">
        $html = '<div style="text-align:center; " ><h1><b>DETALLE DE CAJA DIARIA</b></h1></div>';
        $pdf->writeHTML($html, true, false, true, false, '');

        //$html = '<div style="text-align:center; font-size: 13pt;" ><b>CONSTANCIA DE ORDEN DE REPARACIÓN</b><br></div>';
        //$pdf->writeHTML($html, true, false, true, false, '');
//            $html = '<div style="text-align:center; font-size: 11pt;" ><b>CAJA NRO: '.$caja_id.' </b><br></div>';
//            $pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="CAJA">
        $html = '<div style="text-align:center; font-size: 11pt;" ><b>Datos de la caja nro ' . $caja_id . ' </b><hr /></div>';

        $pdf->writeHTML($html, true, false, true, false, '');
        // <editor-fold defaultstate="collapsed" desc="STYLE">
        $html = <<<EOF
                <!-- EXAMPLE OF CSS STYLE -->
                <style>
                    /*
                    table.first {
                        color: black;
                        font-family: helvetica;
                        font-size: 8pt;
                        background-color: white;
                    }
                    td {
                        border: 0px solid black;
                        background-color: white;
                    }
                    */
                </style>
EOF;
        //$pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>

        $puntovta = $caja_info['descpuntovta'];
        $fechaapertura = date('d/m/Y H:i', strtotime($caja_info['fechaapertura']));
        $fechacierre = '-';
            if (strtotime(@$result['fechacierre']) > 0) {
                $fechacierre = date('d/m/Y H:i', strtotime($caja_info['fechacierre']));
            }
            
        $montoapertura = $caja_info['montoapertura'];
        $saldo = $caja_info['saldo'];
        $estado = $caja_info['estadocaja'];
        $nombreusuario = $caja_info['nombreusuario'];

        $html .= <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    <td width="300" style="text-align:left;"><b>PUNTO VTA: </b>$puntovta</td>
                    <td width="300" style="text-align:left;"></td>
                    
               </tr> 
                <tr >
                    <td width="300" style="text-align:left;"><b>FECHA APERTURA: </b>$fechaapertura</td>
                    <td width="300" style="text-align:left;"><b>FECHA CIERRE: </b>$fechacierre</td>
                    
               </tr> 
                <tr >
                    <td width="300" style="text-align:left;" ><b>MONTO APERTURA: </b>$$montoapertura</td>
                    <td width="300" style="text-align:left;"><b>SALDO: </b>$$saldo</td>
               </tr> 
                <tr >
                    <td width="300" style="text-align:left;" ><b>ESTADO: </b>$estado</td>
                    <td width="300" style="text-align:left;"><b>USUARIO APERTURA: </b>$nombreusuario</td>
               </tr>
            </table>
EOF;

        $pdf->writeHTML($html, true, false, true, false, '');

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="MOVIMIENTOS">
        $html = '<div style="text-align:center; font-size: 11pt; " ><br><b>Movimientos</b><hr /></div>';

        $pdf->writeHTML($html, true, false, true, false, '');
        // <editor-fold defaultstate="collapsed" desc="STYLE">
        $html = <<<EOF
                <!-- EXAMPLE OF CSS STYLE -->
                <style>
                    
                    table.first {
                        color: black;
                        font-family: helvetica;
                        font-size: 8pt;
                        background-color: white;
                    }
                    td {
                        border: 1px solid black;
                        background-color: white;
                    }
                    
                </style>
EOF;
        $pdf->writeHTML($html, true, false, true, false, '');

        // </editor-fold>

        $html .= <<<EOF
                        
            <table style="width:100%;font-size: 8pt;">
            
                <tr>
                    <td width="65"><b>FECHA</b></td>
                    <td width="65"><b>TIPO</b></td>
                    <td width="55"><b>MONTO</b></td>
                    <td width="85"><b>RUBRO</b></td>
                   <td width="85"><b>SUB RUBRO</b></td>
                    <td width="150"><b>OBSERVACIÓN</b></td>
                    <td width="130"><b>RESPONSABLE</b></td>
               </tr> 
EOF;
        $totalsuma = 0;
        foreach ($movimientoscaja as $result) {

            // <editor-fold defaultstate="collapsed" desc="VARIABLES">
            $fecha = date('d/m/Y', strtotime($result['fechamovimiento']));
            $tipo = $result['tipomovimiento'];
            $monto = $result['importe'];
            $rubro = ($result['descrubro'] ? $result['descrubro'] : '-');
            $subrubro = ($result['descsubrubro'] ? $result['descsubrubro'] : '-');
            $observacion = $result['observacion'];
            $responsable = $result['nombreusuario'];

            // </editor-fold>

            $html .= <<<EOF
                
                <tr>
                    <td width="65">$fecha</td>
                    <td width="65">$tipo</td>
                    <td width="55">$$monto</td>
                    <td width="85">$rubro</td>
                    <td width="85">$subrubro</td>
                    <td width="150">$observacion</td>
                    <td width="130">$responsable</td>
                </tr>                  
                
EOF;
        }

        $html .= <<<EOF
                                 
                
</table>
EOF;

        $pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>
        //EJEMPLO
        //// print TEXT
        //$pdf->PrintChapter(1, 'LOREM IPSUM [TEXT]', 'data/chapter_demo_1.txt', false);
        // print HTML
        //$pdf->PrintChapter(2, 'LOREM IPSUM [HTML]', 'data/chapter_demo_2.txt', true);
        // output some RTL HTML content
        //$html = '<div style="text-align:right">ACOMPAÑA<br></div>';
        //$pdf->writeHTML($htmlTable, true, false, true, false, '');
        // reset pointer to the last page
        //$pdf->lastPage();
        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        // Print a table
        // remove default header
        $pdf->setPrintHeader(false);
        //ESTO HACE QUE ANDE EN EL SERVER DE LA DNNA
        ob_end_clean();
        //Close and output PDF document
        //D obliga la descarga
        //I abre en una ventana nueva
        $nombrePDF = 'DetalleCajadiaria_' . $caja_id . '.pdf';
        $pdf->Output($nombrePDF, 'I');

        //$cache->delete('checkins');
        //die('ok');
        //============================================================+
        // END OF FILE
        //============================================================+
    }

}

?>