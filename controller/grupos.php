<?php

class ControllerGrupos extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'Grupos de usuarios');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        $language = & $this->locator->get('language');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
        $request = & $this->locator->get('request');
        // </editor-fold>
         // <editor-fold defaultstate="collapsed" desc="LIMPIA FILTRO Y VARIBLES DE SESSION">

        if ($request->get('filtra') == NULL) {

            $session->set('grupos.search', '');
            $session->set('grupos.sort', '');
            $session->set('grupos.order', '');
            $session->set('grupos.page', '');

            $view->set('search', '');
            $view->set('grupos.search', '');
                        
            $cache->delete('grupos');
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">
        $cols = array();

        $cols[] = array(
            'name' => 'C&oacute;digo',
            'sort' => 'Grupo',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Descripci&oacute;n',
            'sort' => 'descripcion',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'Grupo',
            'descripcion'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        if (!$session->get('grupos.search')) {
            $sql = "SELECT Grupo, descripcion FROM grupos";
        } else {
            $sql = "SELECT Grupo, descripcion FROM grupos WHERE Grupo LIKE '?' OR descripcion LIKE '?'";
        }

        if (in_array($session->get('grupos.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('grupos.sort') . " " . (($session->get('grupos.order') == 'desc') ? 'desc' : 'asc');
        } else {
            $sql .= " ORDER BY grupo ASC";
        }

        $results = $database->getRows($database->splitQuery($database->parse($sql, '%' . $session->get('grupos.search') . '%', '%' . $session->get('grupos.search') . '%'), $session->get('grupos.page'), $config->get('config_max_rows')));

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => $result['Grupo'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => $result['descripcion'],
                'align' => 'left',
                'default' => 0
            );

            $action = array();

            if ($user->hasPermisos($user->getPERSONA(), 'grupos', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa fa-fw fa-pencil',
                    'text' => $language->get('button_update'),
                    'prop_a' => array('href' => $url->ssl('grupos', 'update', array('grupo' => $result['Grupo'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'grupos', 'B')) {
                $action[] = array(
                    'icon' => 'img/iconos-11.png',
                    'text' => $language->get('button_delete'),
                    'class' => 'fa fa-fw fa-trash-o',
                    'prop_a' => array('href' => "javascript:ActionDelete('" . $url->ssl('grupos', 'delete', array('grupo' => $result['grupo'])) . "');")
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'grupos', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw fa-search',
                    'text' => $language->get('button_consult'),
                    'prop_a' => array('href' => $url->ssl('grupos', 'consulta', array('grupo' => $result['Grupo'])))
                );
            }

            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        if ($session->get('grupos.grupo')) {
            $view->set('grupo', $session->get('grupos.grupo'));
        } else {
            $view->set('grupo', '');
        }

        $view->set('heading_title', 'GRUPOS');
        $view->set('heading_title2', 'Filtar');
        $view->set('heading_description', 'Gesti&oacute;n de grupos de usuarios');

        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/NOTICIAS.png');
        $view->set('placeholder_buscar', 'BUSCA POR DESCRIPCI&Oacute;N');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('grupos.search'));
        $view->set('sort', $session->get('grupos.sort'));
        $view->set('order', $session->get('grupos.order'));
        $view->set('page', $session->get('grupos.page'));

        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $view->set('titulo_ventana', '::. Exportar listado de grupos');

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('list', $url->ssl('grupos'));

        if ($user->hasPermisos($user->getPERSONA(), 'grupos', 'A')) {
            $view->set('insert', $url->ssl('grupos', 'insert'));
        }
        $view->set('action', $url->ssl('grupos', 'page'));
        $view->set('export', $url->ssl('grupos', 'exportar'));

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_grupos.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">

        if ($request->has('search', 'post')) {
            $session->set('grupos.search', $request->get('search', 'post'));
        }

        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('grupos.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('grupos.order', (($session->get('grupos.sort') == $request->get('sort', 'post')) && ($session->get('grupos.order') == 'asc')) ? 'desc' : 'asc');
        }

        if ($request->has('sort', 'post')) {
            $session->set('grupos.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('grupos', 'index', array('filtra' => '1')));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $template = & $this->locator->get('template');
        $view = $this->locator->create('template');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('entry_grupo', 'C&oacute;digo:');
        $view->set('entry_descripcion', 'Descripci&oacute;n:');
        $view->set('entry_alta', 'Alta');
        $view->set('entry_baja', 'Baja');
        $view->set('entry_modificacion', 'Modificaci&oacute;n');
        $view->set('entry_consulta', 'Consulta');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('heading_title', 'Grupos de usuarios');
        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/DIFUSION.png');

        $view->set('heading_description', 'Gesti&oacute;n de grupos de usuarios');

        $view->set('button_save', 'Guardar');
        $view->set('button_cancel', 'Cancelar');

        $view->set('tab_general', $language->get('tab_general'));

        if (($request->get('grupo')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM grupos WHERE grupo = '?'";
            $grupo_info = $database->getRow($database->parse($consulta, $request->get('grupo')));
        }

        if ($request->has('grupo', 'post')) {
            $view->set('grupo', $request->get('grupo', 'post'));
            $codigo_grupo = $request->get('grupo', 'post');
        } else {
            $view->set('grupo', @$grupo_info['grupo']);
            $codigo_grupo = @$grupo_info['grupo'];
        }

        if ($request->has('descripcion', 'post')) {
            $view->set('descripcion', $request->get('descripcion', 'post'));
        } else {
            $view->set('descripcion', @$grupo_info['descripcion']);
        }

        $permission_data = array();

        foreach (glob(DIR_CONTROLLER . '*') as $controller) {
            if (basename($controller, '.php') != 'module_admin_footer' && basename($controller, '.php') != 'module_admin_header' && basename($controller, '.php') != 'module_admin_menu') {
                $permisoA = $database->getRow("SELECT * FROM perfiles WHERE grupo = '" . $codigo_grupo . "' AND formulario = '" . basename($controller, '.php') . "' AND permiso='A'");
                $permisoB = $database->getRow("SELECT * FROM perfiles WHERE grupo = '" . $codigo_grupo . "' AND formulario = '" . basename($controller, '.php') . "' AND permiso='B'");
                $permisoM = $database->getRow("SELECT * FROM perfiles WHERE grupo = '" . $codigo_grupo . "' AND formulario = '" . basename($controller, '.php') . "' AND permiso='M'");
                $permisoC = $database->getRow("SELECT * FROM perfiles WHERE grupo = '" . $codigo_grupo . "' AND formulario = '" . basename($controller, '.php') . "' AND permiso='C'");

                $permission_data[] = array(
                    'name' => basename($controller, '.php'),
                    'alta' => (isset($permisoA) ? $permisoA : in_array('A', $request->get('permissions', 'post', array()))),
                    'baja' => (isset($permisoB) ? $permisoB : in_array('B', $request->get('permissions', 'post', array()))),
                    'modificacion' => (isset($permisoM) ? $permisoM : in_array('M', $request->get('permissions', 'post', array()))),
                    'consulta' => (isset($permisoC) ? $permisoC : in_array('C', $request->get('permissions', 'post', array()))),
                );
            }
        }
        $view->set('permissions', $permission_data);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');
        $view->set('error_grupo', @$this->error['grupo']);
        $view->set('error_texto_error', @$this->error['texto_error']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('grupos', $request->get('action'), array('grupos' => $request->get('grupos'))));

        $view->set('cancel', $url->ssl('grupos'));
        // </editor-fold>

        return $view->fetch('content/seguridad_grupos.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        if ((strlen($request->get('grupo', 'post')) == 0)) {
            $errores = 'Debe ingresar un c&oacute;digo de grupo. </br>';
        }

        if ((strlen($request->get('descripcion', 'post')) < 1) || (strlen($request->get('grupo', 'post')) > 50)) {
            $errores .= 'La descripci&oacute;n ingresada debe ser mayor que 0 caracteres y menor que 50. </br>';
        }

        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');

        // </editor-fold>

        $template->set('title', 'Grupos de usuarios');

        if (($request->isPost()) && ($this->validateForm())) {

            $sql = "INSERT INTO grupos SET grupo = '?', descripcion = '?', usuario = '?'";
            $database->query($database->parse($sql, $request->get('grupo', 'post'), $request->get('descripcion', 'post'), $user->getPERSONA()));

            $altas = $request->get('altas', 'post', array());
            $bajas = $request->get('bajas', 'post', array());
            $modificaciones = $request->get('modificaciones', 'post', array());
            $consultas = $request->get('consultas', 'post', array());

            foreach ($altas as $alta) {
                $sql = "INSERT INTO perfiles SET grupo = '?', formulario = '?', permiso = '?', usuario = '?'";
                $database->query($database->parse($sql, $request->get('grupo', 'post'), $alta, 'A', $user->getPERSONA()));
            }

            foreach ($bajas as $baja) {
                $sql = "INSERT INTO perfiles SET grupo = '?', formulario = '?', permiso = '?', usuario = '?'";
                $database->query($database->parse($sql, $request->get('grupo', 'post'), $baja, 'B', $user->getPERSONA()));
            }

            foreach ($modificaciones as $modificacion) {
                $sql = "INSERT INTO perfiles SET grupo = '?', formulario = '?', permiso = '?', usuario = '?'";
                $database->query($database->parse($sql, $request->get('grupo', 'post'), $modificacion, 'M', $user->getPERSONA()));
            }

            foreach ($consultas as $consulta) {
                $sql = "INSERT INTO perfiles SET grupo = '?', formulario = '?', permiso = '?', usuario = '?'";
                $database->query($database->parse($sql, $request->get('grupo', 'post'), $consulta, 'C', $user->getPERSONA()));
            }

            $id = $database->getLastId();
            $cache->delete('grupos');
            $session->delete('menu');
            $session->set('message', 'Se agreg&oacute; el grupo: ' . $id);

            $response->redirect($url->ssl('grupos'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $language = & $this->locator->get('language');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        $template->set('title', 'Grupos de usuarios');

        if (($request->isPost()) && ($this->validateForm())) {

            $id = $request->get('grupo', 'post');
            $sql = "UPDATE grupos SET descripcion = '?', usuario = '?' WHERE grupo = '?'";
            $database->query($database->parse($sql, $request->get('descripcion', 'post'), $user->getPERSONA(), $request->get('grupo', 'post')));

            $sql = "UPDATE perfiles SET usuario = '?' WHERE grupo = '?'";
            $database->query($database->parse($sql, $user->getPERSONA(), $id));

            $sql = "DELETE FROM perfiles WHERE grupo = '?'";
            $database->query($database->parse($sql, $id));

            $altas = $request->get('altas', 'post', array());
            $bajas = $request->get('bajas', 'post', array());
            $modificaciones = $request->get('modificaciones', 'post', array());
            $consultas = $request->get('consultas', 'post', array());

            foreach ($altas as $alta) {
                $sql = "INSERT INTO perfiles SET grupo = '?', formulario = '?', permiso = '?', usuario = '?'";
                $database->query($database->parse($sql, $id, $alta, 'A', $user->getPERSONA()));
            }

            foreach ($bajas as $baja) {
                $sql = "INSERT INTO perfiles SET grupo = '?', formulario = '?', permiso = '?', usuario = '?'";
                $database->query($database->parse($sql, $id, $baja, 'B', $user->getPERSONA()));
            }

            foreach ($modificaciones as $modificacion) {
                $sql = "INSERT INTO perfiles SET grupo = '?', formulario = '?', permiso = '?', usuario = '?'";
                $database->query($database->parse($sql, $id, $modificacion, 'M', $user->getPERSONA()));
            }

            foreach ($consultas as $consulta) {
                $sql = "INSERT INTO perfiles SET grupo = '?', formulario = '?', permiso = '?', usuario = '?'";
                $database->query($database->parse($sql, $id, $consulta, 'C', $user->getPERSONA()));
            }

            $cache->delete('grupos');
            $session->delete('menu');
            $session->set('message', 'Se actualiz&oacute; el grupo: ' . $id);

            $response->redirect($url->ssl('grupos'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Grupos de usuarios');
        if (($request->isPost())) {

            $response->redirect($url->ssl('grupos'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        if (($request->get('grupo')) && ($this->validateDelete())) {

            $id = $request->get('grupo', 'get');

            //TODO: FALTA PROGRAMAR LA ELIMINACION Y VALIDACION VER SI NO HAY RELACIONES
            $sql = "UPDATE grupos SET usuario = '?' WHERE grupo = '?'";
            $database->query($database->parse($sql, $user->getPERSONA(), $request->get('grupo', 'get')));

            $sql = "DELETE FROM grupos WHERE grupo = '?'";
            $database->query($database->parse($sql, $request->get('grupo', 'get')));

            $sql = "UPDATE perfiles SET usuario = '?' WHERE grupo = '?'";
            $database->query($database->parse($sql, $user->getPERSONA(), $request->get('grupo', 'get')));

            $sql = "DELETE FROM perfiles WHERE grupo = '?'";
            $database->query($database->parse($sql, $request->get('grupo', 'get')));

            $cache->delete('grupos');

            $session->set('message', 'Se ha eliminado el grupo: ' . $id);

            $response->redirect($url->ssl('grupos'));
        }

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';
        $sql = "SELECT COUNT(*) AS total FROM personasgrupos WHERE grupo = '?'";
        $personagrupos_info = $database->getRow($database->parse($sql, $request->get('grupo', 'get')));
        if ($personagrupos_info['total'] != 0) {
            $errores .= "No es posible eliminar el grupo, contiene: " . $personagrupos_info['total'] . " personas asociadas";
        }

        if ($errores != '') {
            $this->error['message'] = $errores;
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function exportar() {
        $database = & $this->locator->get('database');

        $consulta = "SELECT grupo,descripcion FROM grupos";

        $htmlTable = "<table border=0 align='center'>";
        $htmlTable.="<tr><td></td><td></td></tr>
                            <tr><td colspan=2 align ='center'>" . utf8_decode("XXXX XXXXXXX XXXXXXXX XXXX") . "</td></tr>
                            <tr><td colspan=2 align ='center'>SISTEMA DE XXXXXXXXXXXX</td></tr>
                            <tr><td colspan=2 align ='center'>Listado de Grupos</td></tr>
                            <tr><td colspan=2 align='center'>______________________________________________________________________________________________________________________</td></tr>";

        $campo_codigo = "Código";
        $campo_descripcion = "Descripción";
        $htmlTable .= "<tr><td>" . utf8_decode($campo_codigo) . "</td><td>" . utf8_decode($campo_descripcion) . "</td></tr>";
        $actividades = $database->getRows($consulta);
        foreach ($actividades as $act) {
            $htmlTable .= "<tr>
                                            <td>" . utf8_decode($act['grupo']) . "</td>
                                            <td>" . utf8_decode($act['descripcion']) . "</td>
                                       </tr>";
        }
        $htmlTable .="</table>";

        define('FPDF_FONTPATH', 'font/');
        require('library/pdf/pdftable.inc.php');
        //ob_end_clean();

        $p = new PDFTable();
        $p->AliasNbPages();
        $p->AddPage();
        $p->setfont('times', '', 9);
        $p->htmltable($htmlTable);
        $p->output();
    }

}

?>
