<?php

class ControllerReporteplanillaventadetalladaconcosto extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'REPORTE');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $request = & $this->locator->get('request');
        $cache = & $this->locator->get('cache');
        // </editor-fold>
        //<editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('listaprecios.page'));

        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        $view->set('heading_title', 'PLANILLA DE VENTA');
        $view->set('placeholder_buscar', 'BUSCA POR ID O MARCA O MODELO O TIPO');

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        //$view->set('consultaVacia', $url->ssl('consulta','consulta_vacia'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        //$view->set('button_consultavacia', "Vacía");
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('listaprecios.search'));
        $view->set('sort', $session->get('listaprecios.sort'));
        $view->set('order', $session->get('listaprecios.order'));
        $view->set('page', $session->get('listaprecios.page'));

        if ($request->has('tiporeporte', 'post')) {
            $view->set('tiporeporte', $request->get('tiporeporte', 'post'));
        } else {
            $view->set('tiporeporte', 'CSV');
        }

        //$view->set('listaprecios.tipoproducto', '-1');
        $view->set('entry_desde', 'Fecha desde:');
        if ($request->has('desde', 'post')) {
            $view->set('desde', $request->get('desde', 'post'));
        } else {
            $view->set('desde', date('d/m/Y'));
        }

        $view->set('entry_hasta', 'Fecha hasta:');
        if ($request->has('hasta', 'post')) {
            $view->set('hasta', $request->get('hasta', 'post'));
        } else {
            $view->set('hasta', date('d/m/Y'));
        }

        //SI ES EXENTO PUEDE FILTRAR POR TODOS LOS PUNTOS DE VENTA
        //SI NO ES EXCENTO SOLO VE LOS PUNTOS DE VENTA A LOS CUALES ESTA ASIGNADO
        if ($user->getExento() == 1) {
            $view->set('puntosdeventa', $database->getRows("SELECT * FROM puntosdeventa ORDER BY descripcion ASC"));
        } else {
            $view->set('puntosdeventa', $database->getRows("SELECT * FROM vendedorespuntodeventa vpv LEFT JOIN puntosdeventa pv ON vpv.puntovta = pv.puntovta WHERE vpv.persona = " . $user->getPERSONA() . " ORDER BY descripcion ASC"));
        }

        $view->set('vendedores', $database->getRows("SELECT * FROM vw_grilla_vendedores ORDER BY apellidonombre ASC"));

        $view->set('tiposproducto', $database->getRows("SELECT * FROM tiposproducto ORDER BY descripcion ASC"));

        $view->set('marcas', $database->getRows("SELECT * FROM marcas ORDER BY descripcion ASC"));

        $view->set('modelos', $database->getRows("SELECT * FROM modelos ORDER BY descripcion ASC"));

        //$view->set('tiposoperacion', $database->getRows("SELECT * FROM tiposoperacion ORDER BY descripcion ASC"));

        $view->set('titulo_ventana', '::. Exportar');

        $view->set('entry_modificar', "Modificar");
        $view->set('entry_agregar', "Agregar");

        //$view->set('modelo', '');
        $view->set('search', '');
        $view->set('reporteplanillaventa.search', '');
        $cache->delete('reporteplanillaventa');

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        //$view->set('action', $url->ssl('reporteplanillaventadetalladaconcosto', 'exportxlsdataCSV'));
        $view->set('action', $url->ssl('reporteplanillaventadetalladaconcosto', 'export'));
        //$view->set('exportxlsdataXLS', $url->ssl('reporteplanillaventadetalladaconcosto', 'exportxlsdataXLS'));
        //$view->set('list', $url->ssl('reporteplanillaventa'));
        $view->set('cancel', $url->ssl('reporteplanillaventadetalladaconcosto'));

//        if ($user->hasPermisos($user->getPERSONA(), 'reporteplanillaventa', 'C')) {
//            $view->set('exportarXLS', $url->ssl('reporteplanillaventa', 'exportxlsdataXLS'));
//        }
//
//        if ($user->hasPermisos($user->getPERSONA(), 'reporteplanillaventa', 'C'))
//            $view->set('exportarCSV', $url->ssl('reporteplanillaventa', 'exportarCSV'));
        //$view->set('addPais', $url->ssl('listaprecios','insert'));
        // $view->set('updatePersona', $url->ssl('paciente','update', array('persona' => $result['persona'])));
        //$view->set('updatePais', $url->ssl('listaprecios','update'));
        // </editor-fold>

        return $view->fetch('content/planillaventadetallada.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">

        if ($request->has('search', 'post')) {
            $session->set('listaprecios.search', $request->get('search', 'post'));
        }

        if ($request->has('tipoproducto', 'post')) {
            $session->set('listaprecios.tipoproducto', $request->get('tipoproducto', 'post'));
        }

        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('listaprecios.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('listaprecios.order', (($session->get('listaprecios.sort') == $request->get('sort', 'post')) && ($session->get('listaprecios.order') == 'asc')) ? 'desc' : 'asc');
        }

        if ($request->has('sort', 'post')) {
            $session->set('listaprecios.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('reporteplanillaventadetalladaconcosto'));
    }

    function export() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $common = & $this->locator->get('common');
        // </editor-fold>
        $tiporeporte = $request->get('tiporeporte', 'post');
        switch ($tiporeporte) {
            case 'CSV': $this->exportxlsdataCSV();
                break;
            case 'XLS': $this->exportxlsdataXLS();
                break;
            default:
                break;
        }
    }

    function exportarXLS_old() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        // </editor-fold>

        set_time_limit(0);

        //** PHPExcel **//
        require_once (DIR_INCLUDES . '/library/phpexcel/PHPExcel.php');

        date_default_timezone_set('America/Argentina/Buenos_Aires');
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set properties of the file
        $objPHPExcel->getProperties()->setCreator("NombreEmpresa")
                ->setLastModifiedBy("NombreEmpresa")
                ->setTitle("Planilla web exportada: " . date('d-m-Y'))
                ->setSubject("Planilla web exportada: " . date('d-m-Y'));

        //genero las columnas del excel
        $letra = 'A';
        $fila_inicial = 2; //fila desde donde empiezo a cargar la tabla en el excel. Arriba va el encabezado
        // <editor-fold defaultstate="collapsed" desc="PROPIEDADES DE LAS COLUMNAS">
        $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //COLUMNA A
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'ID');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA B
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'COD INTERNO');
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA C
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(37);
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', 'MODELO');
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA D
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(22);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', 'PRECIO VTA');
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">

        $sql = "SELECT * FROM vw_grilla_articulos  ";
        $conca = " WHERE ";

        //OBTENEMOS TODOS LOS TIPOS MARCADOS
        $tiposdeproducto = $request->get('tipos', 'post', array());

        if (count($tiposdeproducto) > 0) {
            $conca2 = ' (';
            foreach ($tiposdeproducto as $tipo) {
                $sql .= $conca . $conca2 . "tipoproducto = '" . $tipo . "'  ";
                $conca = " OR ";
                $conca2 = '';
            }
            $sql .= ") ";
            $conca = ' AND ';
        }

        //OBTENEMOS TODOS LOS TIPOS MARCADOS
        $marcas = $request->get('marcas', 'post', array());

        if (count($marcas) > 0) {
            $conca2 = ' (';
            foreach ($marcas as $marca) {
                $sql .= $conca . $conca2 . " marca = '" . $marca . "'  ";
                $conca = " OR ";
            }
            $sql .= ") ";
        }

        $sql .= " ORDER BY marca, modelo ASC";

        $results = $database->getRows($sql);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="LLANAR LOS DATOS">

        $num = $fila_inicial;

        foreach ($results as $result) {

            //COLUMNA A
            $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $num, $result['articulo']);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            //COLUMNA B
            $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $num, $result['codbarrainterno']);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            //COLUMNA C
            $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $num, $result['descmodelo']);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA D
            $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' . $num, $result['preciovta']);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $num++;
        }

        //SEGUIR EN COMPLETAR EL EXCEL
        //POR CADA INSCRIPTO IR BUSCANDO CADA RESPUESTA Y COMPLETAR SEGUN CORRESPONDA
        // </editor-fold>
        // Nombre de la hoja del libro
        $objPHPExcel->getActiveSheet()->setTitle('ExportPrecios');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Save Excel 2007 file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');

        // It will be called file.xls | esta linea hace que sea una descarga y no una pestaña aparte
        header('Content-Disposition: attachment; filename="ExportPrecios.xls"');

        header('Cache-Control: max-age=0');

        // Write file to the browser
        $objWriter->save('php://output');
    }

    function exportxlsdataXLS() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $common = & $this->locator->get('common');
        // </editor-fold>
        //$tabla = 'vw_grilla_clientecontactos_xls';
        $tabla = 'vw_lista_ventadetalladaconcosto_xls';
        set_time_limit(0);

        try {
            $msg = '';

            $TABLE_NAME = $tabla;

            // <editor-fold defaultstate="collapsed" desc="TABLA SELECCIONADA A EXCEL">
            //** PHPExcel **//
            require_once (DIR_INCLUDES . '/library/phpexcel/PHPExcel.php');

            date_default_timezone_set('America/Argentina/Buenos_Aires');
            // Create new PHPExcel object
            $objPHPExcel = new PHPExcel();

            // Set properties of the file
            $objPHPExcel->getProperties()->setCreator("Accesorios")
                    ->setLastModifiedBy("Accesorios")
                    ->setTitle("Planilla web exportada: " . date('d-m-Y'))
                    ->setSubject("Planilla web exportada: " . date('d-m-Y'));

            // http://localhost/NOB/?controller=exportatablaxls&TABLE_NAME=archivos&action=exportxls   
            //$TABLE_NAME = $request->get('TABLE_NAME','get');

            $sql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '" . $TABLE_NAME . "' AND table_schema = '" . DB_NAME . "'  ";
            $list_colum_name = $database->getRows($sql);
            $cant_columnas = count($list_colum_name);

            //genero las columnas del excel
            $letra = 'A';
            $letra_fin = $this->devuelveLetra($cant_columnas - 1);
            $fila_inicial = 2; //fila desde donde empiezo a cargar la tabla en el excel. Arriba va el encabezado
            //$letra_numero_fin = $letra_fin . $cant_columnas;
            // <editor-fold defaultstate="collapsed" desc="PROPIEDADES DE LAS COLUMNAS">
            //$funciona = 'A1:'.$letra_fin . '1';
            $objPHPExcel->getActiveSheet()->getStyle('A1:' . $letra_fin . '1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

            for ($i = 0; $i < $cant_columnas - 1; $i++) {
                if ($i == 99) {
                    $control = true;
                }
                $name = $list_colum_name[$i][COLUMN_NAME];

                $letra_i = $i + 1;
                $letra_actual = $this->devuelveLetra($letra_i);
                $letra_numero = $letra_actual . '1';
                $objPHPExcel->getActiveSheet()->getColumnDimension($letra_actual)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyle($letra_numero)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $objPHPExcel->getActiveSheet()->getStyle($letra_numero)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letra_numero, $name);
                $objPHPExcel->getActiveSheet()->getStyle($letra_numero)->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            }

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="LLANAR LOS DATOS">
            // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
            set_time_limit(0);


            $sql = "SELECT * FROM " . $TABLE_NAME;
            $conca = " WHERE ";

            // <editor-fold defaultstate="collapsed" desc="FECHA">
            $fecha_d = '';
            if ($request->get('desde', 'post') != '') {
                $dated = explode('/', $request->get('desde', 'post'));
                // $fecha_d = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
                $fecha_d = str_replace('/', '-', $request->get('desde', 'post'));
            }
            if ($fecha_d != '') {
                $sql .= $conca . " STR_TO_DATE(FECHA,'%d-%m-%Y') >= STR_TO_DATE('" . $fecha_d . "','%d-%m-%Y') ";
                $conca = " AND ";
            }

            $fecha_h = '';
            if ($request->get('hasta', 'post') != '') {
                $dated = explode('/', $request->get('hasta', 'post'));
                // $fecha_h = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
                $fecha_h = str_replace('/', '-', $request->get('hasta', 'post'));
                // $fecha_h = $request->get('hasta', 'post');
            }
            if ($fecha_h != '') {
                $sql .= $conca . " STR_TO_DATE(FECHA,'%d-%m-%Y') <= STR_TO_DATE('" . $fecha_h . "','%d-%m-%Y') ";
                $conca = " AND ";
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="VENDEDORES">
            $vendedores = $request->get('vendedores', 'post', array());
            $primero = "si";
            foreach ($vendedores as $vendedor) {
                if ($primero == "si") {
                    $conca .= 'VENDEDOR IN (';
                }
                $sql .= $conca . "'" . $vendedor . "'";
                $conca = ", ";
                $primero = "no";
            }
            if ($vendedores) {
                $sql .= ') ';
                $conca = " AND ";
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="PUNTOS DE VENTA">
            //$puntosdeventa = $request->get('puntosdeventa', 'post', array());
            $puntosdeventa = $request->get('puntosvta', 'post', array());
            $primero = "si";
            foreach ($puntosdeventa as $item) {
                if ($primero == "si") {
                    $conca .= 'PUNTOVTA IN (';
                }
                $sql .= $conca . "'" . $item . "'";
                $conca = ", ";
                $primero = "no";
            }
            if ($puntosdeventa) {
                $sql .= ') ';
                $conca = " AND ";
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="TIPOS DE PRODUCTO">
            $tipos = $request->get('tipos', 'post', array());
            $primero = "si";
            $parentesisabierto = ' ( ';
             $parentesisfin = ' ) ';
            foreach ($tipos as $tipoproducto) {
                if ($primero == "si") {
                    $conca .= $parentesisabierto . ' TIPO_PRODUCTO IN (';
                    $parentesisabierto = ' ';
                }
                $sql .= $conca . "'" . $tipoproducto . "'";
                $conca = ", ";
                $primero = "no";
            }
            if ($tipos) {
                $sql .= ') ';
                $conca = " OR ";
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="MARCAS">
            $marcas = $request->get('marcas', 'post', array());
            $primero = "si";
            foreach ($marcas as $item) {
                if ($primero == "si") {
                    $conca .= $parentesisabierto . ' MARCA IN (';
                    $parentesisabierto = ' ';
                }
                $sql .= $conca . "'" . $item . "'";
                $conca = ", ";
                $primero = "no";
            }
            if ($marcas) {
                $sql .= ') ';
                $conca = " OR ";
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="MODELOS">
            $modelos = $request->get('modelos', 'post', array());
            $primero = "si";
            foreach ($modelos as $modelo) {
                if ($primero == "si") {
                    $conca .= $parentesisabierto . ' MODELO IN (';
                    $parentesisabierto = ' ';
                }
                $sql .= $conca . "'" . utf8_encode($modelo) . "'";
                $conca = ", ";
                $primero = "no";
            }
            if ($modelos) {
                $sql .= ') ';
                $conca = " OR ";
            }
            if ($parentesisabierto == ' ') {
                $sql .= ' ) ';
            }
            // </editor-fold>
            //$sql = "SELECT * FROM " . $TABLE_NAME;
            $results = $database->getRows($sql);

            // </editor-fold>

            $num = $fila_inicial;

            foreach ($results as $result) {

                for ($i = 0; $i < $cant_columnas - 1; $i++) {
                    $name = $list_colum_name[$i][COLUMN_NAME];
                    $letra_i = $i + 1;
                    $letra_actual = $this->devuelveLetra($letra_i);
                    $letra_numero = $letra_actual . $num;

                    $objPHPExcel->getActiveSheet()->getStyle($letra_numero)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letra_numero, $result[$name]);
                    $objPHPExcel->getActiveSheet()->getStyle($letra_numero)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
               
                    //HACE QUE EL NUMERO DE VENTA SALGA EN FORMATO CORRECTO
                    if ($letra_actual == 'A') $objPHPExcel->getActiveSheet()->getStyle($letra_numero)->getNumberFormat()->setFormatCode('0000');
                    
                }
                $num++;
            }

            //SEGUIR EN COMPLETAR EL EXCEL
            //POR CADA INSCRIPTO IR BUSCANDO CADA RESPUESTA Y COMPLETAR SEGUN CORRESPONDA
            // </editor-fold>
            //Cargo los datos celda por celda
            // Nombre de la hoja del libro
            $objPHPExcel->getActiveSheet()->setTitle('Registros');

            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $objPHPExcel->setActiveSheetIndex(0);

            // Save Excel 2007 file
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

            // We'll be outputting an excel file
            header('Content-type: application/vnd.ms-excel');

            $fecha_d = date(" Y m d");
            $nombre_archivo = 'PlanillaVtaDetallada '.$fecha_d . '.xls';
            // It will be called file.xls | esta linea hace que sea una descarga y no una pestaña aparte
            header('Content-Disposition: attachment; filename=' . $nombre_archivo);

            header("Expires: 0");

            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

            // Write file to the browser
            //$objWriter->save($nombre_archivo); 
            // $objWriter->save('Archivos/exportatablaxls/' . $nombre_archivo);
            // Write file to the browser
            $objWriter->save('php://output');

            $common->log_msg($msg, 'excel_log');
            //$objPHPExcel->
            //$objWriter->close();
            //$objPHPExcel->disconnectWorksheets();
            //unset($objWriter, $objPHPExcel);
            // </editor-fold>
        } catch (Exception $e) {
            $msg .= date('c') . "\r\n \t CATCH:\t " . $e . " \r\n";
            $common->log_msg($msg, 'excel_log');
        }
        //$view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
    }

    function exportxlsdataCSV() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $common = & $this->locator->get('common');
        // </editor-fold>
        //$tabla = 'vw_grilla_clientecontactos_xls';
        $tabla = 'vw_lista_ventadetalladaconcosto_xls';

        set_time_limit(0);

        try {
            $msg = '';

            $TABLE_NAME = $tabla;

            // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">
            $sql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '" . $TABLE_NAME . "' AND table_schema = '" . DB_NAME . "'  ";
            $list_colum_name = $database->getRows($sql);
            $cant_columnas = count($list_colum_name);

            $nombre_de_las_columnas = array();
            for ($i = 0; $i < $cant_columnas; $i++) {
                $name = $list_colum_name[$i][COLUMN_NAME];
                $nombre_de_las_columnas[] = $name;
            }

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
            set_time_limit(0);

            $sql = "SELECT * FROM " . $TABLE_NAME;
            $conca = " WHERE ";

            // <editor-fold defaultstate="collapsed" desc="FECHA">
            $fecha_d = '';
            if ($request->get('desde', 'post') != '') {
                $dated = explode('/', $request->get('desde', 'post'));
                // $fecha_d = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
                $fecha_d = str_replace('/', '-', $request->get('desde', 'post'));
            }
            if ($fecha_d != '') {
                $sql .= $conca . " STR_TO_DATE(FECHA,'%d-%m-%Y') >= STR_TO_DATE('" . $fecha_d . "','%d-%m-%Y') ";
                $conca = " AND ";
            }

            $fecha_h = '';
            if ($request->get('hasta', 'post') != '') {
                $dated = explode('/', $request->get('hasta', 'post'));
                // $fecha_h = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
                $fecha_h = str_replace('/', '-', $request->get('hasta', 'post'));
                // $fecha_h = $request->get('hasta', 'post');
            }
            if ($fecha_h != '') {
                $sql .= $conca . " STR_TO_DATE(FECHA,'%d-%m-%Y') <= STR_TO_DATE('" . $fecha_h . "','%d-%m-%Y') ";
                $conca = " AND ";
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="VENDEDORES">
            $vendedores = $request->get('vendedores', 'post', array());
            $primero = "si";
            foreach ($vendedores as $vendedor) {
                if ($primero == "si") {
                    $conca .= 'VENDEDOR IN (';
                }
                $sql .= $conca . "'" . $vendedor . "'";
                $conca = ", ";
                $primero = "no";
            }
            if ($vendedores) {
                $sql .= ') ';
                $conca = " AND ";
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="PUNTOS DE VENTA">
            //$puntosdeventa = $request->get('puntosdeventa', 'post', array());
            $puntosdeventa = $request->get('puntosvta', 'post', array());
            $primero = "si";
            foreach ($puntosdeventa as $item) {
                if ($primero == "si") {
                    $conca .= 'PUNTOVTA IN (';
                }
                $sql .= $conca . "'" . $item . "'";
                $conca = ", ";
                $primero = "no";
            }
            if ($puntosdeventa) {
                $sql .= ') ';
                $conca = " AND ";
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="TIPOS DE PRODUCTO">
            $tipos = $request->get('tipos', 'post', array());
            $primero = "si";
            $parentesisabierto = ' ( ';
             $parentesisfin = ' ) ';
            foreach ($tipos as $tipoproducto) {
                if ($primero == "si") {
                    $conca .= $parentesisabierto . ' TIPO_PRODUCTO IN (';
                    $parentesisabierto = ' ';
                }
                $sql .= $conca . "'" . $tipoproducto . "'";
                $conca = ", ";
                $primero = "no";
            }
            if ($tipos) {
                $sql .= ') ';
                $conca = " OR ";
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="MARCAS">
            $marcas = $request->get('marcas', 'post', array());
            $primero = "si";
            foreach ($marcas as $item) {
                if ($primero == "si") {
                    $conca .= $parentesisabierto . ' MARCA IN (';
                    $parentesisabierto = ' ';
                }
                $sql .= $conca . "'" . $item . "'";
                $conca = ", ";
                $primero = "no";
            }
            if ($marcas) {
                $sql .= ') ';
                $conca = " OR ";
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="MODELOS">
            $modelos = $request->get('modelos', 'post', array());
            $primero = "si";
            foreach ($modelos as $modelo) {
                if ($primero == "si") {
                    $conca .= $parentesisabierto . ' MODELO IN (';
                    $parentesisabierto = ' ';
                }
                $sql .= $conca . "'" . utf8_encode($modelo) . "'";
                $conca = ", ";
                $primero = "no";
            }
            if ($modelos) {
                $sql .= ') ';
                $conca = " OR ";
            }
            if ($parentesisabierto == ' ') {
                $sql .= ' ) ';
            }
            // </editor-fold>
            //$sql = "SELECT * FROM " . $TABLE_NAME;
            $results = $database->getRows($sql);

            //LE METO EN ELCABEZADO
            array_unshift($results, $nombre_de_las_columnas);

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="TABLA SELECCIONADA A CSV">
            //cabeceras para descarga
            header('Content-Type: application/octet-stream');
            header("Content-Transfer-Encoding: Binary");
            header("Content-disposition: attachment; filename=\"my_csv_file.csv\"");

            //preparar el wrapper de salida
            $outputBuffer = fopen("php://output", 'w');

            //volcamos el contenido del array en formato csv
            foreach ($results as $val) {
                //fputcsv($outputBuffer, $val);
                fputcsv($outputBuffer, $val, ";");
            }
            //cerramos el wrapper
            fclose($outputBuffer);
            exit;

            // </editor-fold>
        } catch (Exception $e) {
            $msg .= date('c') . "\r\n \t CATCH:\t " . $e . " \r\n";
            $common->log_msg($msg, 'excel_log');
        }
        //$view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
    }

    function devuelveLetra($num) {
        $letra = '';
        switch ($num) {
            case 1:$letra = 'A';
                break;
            case 2:$letra = 'B';
                break;
            case 3:$letra = 'C';
                break;
            case 4:$letra = 'D';
                break;
            case 5:$letra = 'E';
                break;
            case 6:$letra = 'F';
                break;
            case 7:$letra = 'G';
                break;
            case 8:$letra = 'H';
                break;
            case 9:$letra = 'I';
                break;
            case 10:$letra = 'J';
                break;
            case 11:$letra = 'K';
                break;
            case 12:$letra = 'L';
                break;
            case 13:$letra = 'M';
                break;
            case 14:$letra = 'N';
                break;
            case 15:$letra = 'O';
                break;
            case 16:$letra = 'P';
                break;
            case 17:$letra = 'Q';
                break;
            case 18:$letra = 'R';
                break;
            case 19:$letra = 'S';
                break;
            case 20:$letra = 'T';
                break;
            case 21:$letra = 'U';
                break;
            case 22:$letra = 'V';
                break;
            case 23:$letra = 'W';
                break;
            case 24:$letra = 'X';
                break;
            case 25:$letra = 'Y';
                break;
            case 26:$letra = 'Z';
                break;
            case 27:$letra = 'AA';
                break;
            case 28:$letra = 'AB';
                break;
            case 29:$letra = 'AC';
                break;
            case 30:$letra = 'AD';
                break;
            case 31:$letra = 'AE';
                break;
            case 32:$letra = 'AF';
                break;
            case 33:$letra = 'AG';
                break;
            case 34:$letra = 'AH';
                break;
            case 35:$letra = 'AI';
                break;
            case 36:$letra = 'AJ';
                break;
            case 37:$letra = 'AK';
                break;
            case 38:$letra = 'AL';
                break;
            case 39:$letra = 'AM';
                break;
            case 40:$letra = 'AN';
                break;
            case 41:$letra = 'AO';
                break;
            case 42:$letra = 'AP';
                break;
            case 43:$letra = 'AQ';
                break;
            case 44:$letra = 'AR';
                break;
            case 45:$letra = 'AS';
                break;
            case 46:$letra = 'AT';
                break;
            case 47:$letra = 'AU';
                break;
            case 48:$letra = 'AV';
                break;
            case 49:$letra = 'AW';
                break;
            case 50:$letra = 'AX';
                break;
            case 51:$letra = 'AY';
                break;
            case 52:$letra = 'AZ';
                break;

            case 53:$letra = 'BA';
                break;
            case 54:$letra = 'BB';
                break;
            case 55:$letra = 'BC';
                break;
            case 56:$letra = 'BD';
                break;
            case 57:$letra = 'BE';
                break;
            case 58:$letra = 'BF';
                break;
            case 59:$letra = 'BG';
                break;
            case 60:$letra = 'BH';
                break;
            case 61:$letra = 'BI';
                break;
            case 62:$letra = 'BJ';
                break;
            case 63:$letra = 'BK';
                break;
            case 64:$letra = 'BL';
                break;
            case 65:$letra = 'BM';
                break;
            case 66:$letra = 'BN';
                break;
            case 67:$letra = 'BO';
                break;
            case 68:$letra = 'BP';
                break;
            case 69:$letra = 'BQ';
                break;
            case 70:$letra = 'BR';
                break;
            case 71:$letra = 'BS';
                break;
            case 72:$letra = 'BT';
                break;
            case 73:$letra = 'BU';
                break;
            case 74:$letra = 'BV';
                break;
            case 75:$letra = 'BW';
                break;
            case 76:$letra = 'BX';
                break;
            case 77:$letra = 'BY';
                break;
            case 78:$letra = 'BZ';
                break;

            case 79:$letra = 'CA';
                break;
            case 80:$letra = 'CB';
                break;
            case 81:$letra = 'CC';
                break;
            case 82:$letra = 'CD';
                break;
            case 83:$letra = 'CE';
                break;
            case 84:$letra = 'CF';
                break;
            case 85:$letra = 'CG';
                break;
            case 86:$letra = 'CH';
                break;
            case 87:$letra = 'CI';
                break;
            case 88:$letra = 'CJ';
                break;
            case 89:$letra = 'CK';
                break;
            case 90:$letra = 'CL';
                break;
            case 91:$letra = 'CM';
                break;
            case 92:$letra = 'CN';
                break;
            case 93:$letra = 'CO';
                break;
            case 94:$letra = 'CP';
                break;
            case 95:$letra = 'CQ';
                break;
            case 96:$letra = 'CR';
                break;
            case 97:$letra = 'CS';
                break;
            case 98:$letra = 'CT';
                break;
            case 99:$letra = 'CU';
                break;
            case 100:$letra = 'CV';
                break;
            case 101:$letra = 'CW';
                break;
            case 102:$letra = 'CX';
                break;
            case 103:$letra = 'CY';
                break;
            case 104:$letra = 'CZ';
                break;

            case 105:$letra = 'DA';
                break;
            case 106:$letra = 'DB';
                break;
            case 107:$letra = 'DC';
                break;
            case 108:$letra = 'DD';
                break;
            case 109:$letra = 'DE';
                break;
            case 110:$letra = 'DF';
                break;
            case 111:$letra = 'DG';
                break;
            case 112:$letra = 'DH';
                break;
            case 113:$letra = 'DI';
                break;
            case 114:$letra = 'DJ';
                break;
            case 115:$letra = 'DK';
                break;
            case 116:$letra = 'DL';
                break;
            case 117:$letra = 'DM';
                break;
            case 118:$letra = 'DN';
                break;
            case 119:$letra = 'DO';
                break;
            case 120:$letra = 'DP';
                break;
            case 121:$letra = 'DQ';
                break;
            case 122:$letra = 'DR';
                break;
            case 123:$letra = 'DS';
                break;
            case 124:$letra = 'DT';
                break;
            case 125:$letra = 'DU';
                break;
            case 126:$letra = 'DV';
                break;
            case 127:$letra = 'DW';
                break;
            case 128:$letra = 'DX';
                break;
            case 129:$letra = 'DY';
                break;
            case 130:$letra = 'DZ';
                break;

            case 131:$letra = 'EA';
                break;
            case 132:$letra = 'EB';
                break;
            case 133:$letra = 'EC';
                break;
            case 134:$letra = 'ED';
                break;
            case 135:$letra = 'EE';
                break;
            case 136:$letra = 'EF';
                break;
            case 137:$letra = 'EG';
                break;
            case 138:$letra = 'EH';
                break;
            case 139:$letra = 'EI';
                break;
            case 140:$letra = 'EJ';
                break;
            case 141:$letra = 'EK';
                break;
            case 142:$letra = 'EL';
                break;
            case 143:$letra = 'EM';
                break;
            case 144:$letra = 'EN';
                break;
            case 145:$letra = 'EO';
                break;

            default:
                break;
        }
        return $letra;
    }
}
?>