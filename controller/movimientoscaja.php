<?php

class ControllerMovimientoscaja extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'MOVIMIENTOS DE LA CAJA DE COMPRAS');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
        $request = & $this->locator->get('request');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">
        if ($request->has('caja', 'post')) {
            $view->set('caja', $request->get('caja', 'post'));
            $caja_id = $request->get('caja', 'post');
        } else {
            if ($request->has('caja', 'get')) {
                $view->set('caja', $request->get('caja', 'get'));
                $caja_id = $request->get('caja', 'get');
            }
        }

        if (($request->get('caja')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM cajas WHERE caja = '?'";
            @$caja_info = $database->getRow($database->parse($consulta, $caja_id));
        }
        $view->set('entry_fechaapertura', 'Fecha apertura');
        $view->set('fechaapertura', $caja_info['fechaapertura']);

        $view->set('entry_fechacierre', 'Fecha cierre');

        $view->set('fechacierre', $caja_info['fechaapertura']);

        $view->set('entry_montoapertura', 'Monto apertura');
        $view->set('montoapertura', $caja_info['fechaapertura']);

        $view->set('entry_saldo', 'Saldo');
        $view->set('saldo', $caja_info['saldo']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">

        $cols = array();

        $cols[] = array(
            'name' => 'Movimiento',
            'sort' => 'caja',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Fecha',
            'sort' => 'fechamovimiento',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Tipo',
            'sort' => 'tipomovimiento',
            'align' => 'left'
        );
        $cols[] = array(
            'name' => '$ Monto',
            'sort' => 'montoapertura',
            'align' => 'left'
        );


//                $cols[] = array(
//			'name'  => 'PRECIO VTA',
//			'sort'  => 'precio',
//			'align' => 'left'
//		);

        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'movimiento',
            'fechamovimiento',
            'tipomovimiento',
            'montomovimiento'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        set_time_limit(0);

//                  if (!$session->get('cajas.search')) {
        $sql = "SELECT * FROM movimientoscaja  WHERE caja=" . $caja_id;
//                        $conca = " WHERE ";
//		} else {
//			$sql = "SELECT * FROM movimientoscaja WHERE caja LIKE '?' OR estado LIKE '?' ";
//                        $conca = " AND ";      
//                }
//		if (in_array($session->get('cajas.sort'), $sort)) {
//			$sql .= " ORDER BY " . $session->get('cajas.sort') . " " . (($session->get('cajas.order') == 'desc') ? 'desc' : 'asc');
//		} else {
        $sql .= " ORDER BY fechamovimiento ASC";
//		}                
//		$consul = $database->splitQuery($database->parse($sql,'%' . $session->get('cajas.search') . '%','%' . $session->get('cajas.search') . '%'), $session->get('cajas.page'), $config->get('config_max_rows'));
//
//                $results = $database->getRows($consul);
        //$consulta = $database->parse($sql, '%'.$session->get('cajas.search').'%','%'.$session->get('cajas.search').'%');
        $results = $database->getRows($sql);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['movimiento'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => date('d/m/Y', strtotime(@$result['fechamovimiento'])),
                'align' => 'left',
                'default' => 0
            );


            $cell[] = array(
                'value' => @$result['tipomovimiento'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['montomovimiento'],
                'align' => 'left',
                'default' => 0
            );



//                        $precio = 0;
//                        if (@$result['preciovta']) {
//                            $precio = @$result['preciovta'];
//                        }
//                         $cell[] = array(
//				'value'   => $precio,
//				'align'   => 'left',
//				'default' => 0
//			);

            $action = array();

            if ($user->hasPermisos($user->getPERSONA(), 'movimientoscaja', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa fa-fw fa-pencil',
                    'text' => $language->get('button_update'),
                    'prop_a' => array('href' => $url->ssl('movimientoscaja', 'update', array('movimiento' => $result['movimiento'], 'caja' => $result['caja'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'movimientoscaja', 'B')) {
                $action[] = array(
                    'icon' => 'img/iconos-11.png',
                    'text' => $language->get('button_delete'),
                    'class' => 'fa fa-fw fa-trash-o',
                    'prop_a' => array('href' => "javascript:ActionDelete('" . $url->ssl('movimientoscaja', 'delete', array('movimiento' => $result['movimiento'], 'caja' => $result['caja'])) . "');")
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'movimientoscaja', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw fa-search',
                    'text' => $language->get('button_consult'),
                    'prop_a' => array('href' => $url->ssl('movimientoscaja', 'consulta', array('movimiento' => $result['movimiento'], 'caja' => $result['caja'])))
                );
            }

            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        //<editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('movimientoscaja.page'));

        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        $view->set('heading_title', 'MOVIMIENTOS CAJA');
        $view->set('placeholder_buscar', 'BUSCA POR ID O ESTADO');

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        //$view->set('consultaVacia', $url->ssl('consulta','consulta_vacia'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        //$view->set('button_consultavacia', "Vacía");
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('movimientoscaja.search'));
        $view->set('sort', $session->get('movimientoscaja.sort'));
        $view->set('order', $session->get('movimientoscaja.order'));
        $view->set('page', $session->get('movimientoscaja.page'));

        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $view->set('titulo_ventana', '::. Exportar');

        $view->set('entry_modificar', "Modificar");
        $view->set('entry_agregar', "Agregar");

        $view->set('movimientoscaja.search', '');
        $cache->delete('movimientoscaja');

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('movimientoscaja', 'page'));
        $view->set('list', $url->ssl('movimientoscaja'));

        $view->set('cancel', $url->ssl('cajas', 'index', array('caja' => $caja_id)));
        if (@$caja_info['estadocaja'] != 'CERRADA') {
            if ($user->hasPermisos($user->getPERSONA(), 'movimientoscaja', 'A')) {
                $view->set('insert', $url->ssl('movimientoscaja', 'insert', array('caja' => $caja_id)));
            }
            if ($user->hasPermisos($user->getPERSONA(), 'movimientoscaja', 'A')) {
                $view->set('insertImport', $url->ssl('movimientoscaja', 'insertImport'));
            }
        }

        if ($user->hasPermisos($user->getPERSONA(), 'movimientoscaja', 'C')) {
            $view->set('exportXls', $url->ssl('movimientoscaja', 'exportXls'));
        }

        //$view->set('addPais', $url->ssl('cajas','insert'));
        // $view->set('updatePersona', $url->ssl('paciente','update', array('persona' => $result['persona'])));
        //$view->set('updatePais', $url->ssl('cajas','update'));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">
//		$page_data = array();
//
//		for ($i = 1; $i <= $database->getPages(); $i++) {
//			$page_data[] = array(
//				'text'  => $language->get('text_pages', $i, $database->getPages()),
//				'value' => $i
//			);
//		}
//                
//                $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_movimientoscaja.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">
//		if ($request->has('modelo', 'post')) {
//                    $session->set('cajas.pais',$request->get('modelo','post'));              
//                }

        if ($request->has('search', 'post')) {
            $session->set('movimientoscaja.search', $request->get('search', 'post'));
        }

        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('movimientoscaja.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('movimientoscaja.order', (($session->get('movimientoscaja.sort') == $request->get('sort', 'post')) && ($session->get('movimientoscaja.order') == 'asc')) ? 'desc' : 'asc');
        }

        if ($request->has('sort', 'post')) {
            $session->set('movimientoscaja.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('movimientoscaja'));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $template = & $this->locator->get('template');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('heading_title', 'DATOS DEL MOVIMIENTO');

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $template->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('tab_general', $language->get('tab_general'));

        $view->set('caja', $request->get('caja'));

        if (($request->get('movimiento')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM movimientoscaja WHERE movimiento = '" . $request->get('movimiento') . "' ";
            $objeto_info = $database->getRow($consulta);
        }

        if ($request->has('movimiento', 'post')) {
            $view->set('movimiento', $request->get('movimiento', 'post'));
            $objeto_id = @$objeto_info['movimiento'];
        } else {
            $view->set('movimiento', $request->get('movimiento', 'get'));
            $objeto_id = @$objeto_info['movimiento'];
        }

        $caja_id = '';
        if ($request->has('caja', 'post')) {
            $view->set('caja', $request->get('caja', 'post'));
            $caja_id = $request->get('caja', 'post');
        } else {
            $view->set('caja', $request->get('caja', 'get'));
            $caja_id = $request->get('caja', 'get');
        }

        $view->set('entry_fechamovimiento', 'Fecha movimiento:');
        if ($request->has('fechamovimiento', 'post')) {
            $view->set('fechamovimiento', $request->get('fechamovimiento', 'post'));
        } else {
            if ($request->has('fechamovimiento', 'get')) {
                $view->set('fechamovimiento', date('d/m/Y', strtotime(@$objeto_info['fechamovimiento'])));
            } else {
                $view->set('fechamovimiento', date('d/m/Y'));
            }
        }

        $view->set('entry_tipomovimiento', 'Tipo movimiento');
        if ($request->has('tipomovimiento', 'post')) {
            $view->set('tipomovimiento', $request->get('tipomovimiento', 'post'));
        } else {
            $view->set('tipomovimiento', @$objeto_info['tipomovimiento']);
        }

        $view->set('entry_montomovimiento', 'Monto movimiento');
        if ($request->has('montomovimiento', 'post')) {
            $view->set('montomovimiento', $request->get('montomovimiento', 'post'));
        } else {
            $view->set('montomovimiento', @$objeto_info['montomovimiento']);
        }

        $view->set('entry_observacion', 'Observación');
        if ($request->has('observacion', 'post')) {
            $view->set('observacion', $request->get('observacion', 'post'));
        } else {
            $view->set('observacion', @$objeto_info['observacion']);
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $template->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');
        $view->set('error_texto_error', @$this->error['texto_error']);
        //$view->set('error_titulo', @$this->error['titulo']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        //$view->set('action', $url->ssl('cajas', $request->get('action'), array('modelo' => $request->get('modelo'))));
        $view->set('actionBoton', $request->get('action'));
        $view->set('cancel', $url->ssl('movimientoscaja', 'index', array('caja' => $caja_id)));
        // </editor-fold>

        return $view->fetch('content/movimientocaja.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        if ((strlen($request->get('montomovimiento', 'post')) != 0)) {
            $tipomovimiento = $request->get('tipomovimiento', 'post');
            if ($tipomovimiento == 'EGRESO') {
                $sql = "SELECT * FROM cajas WHERE caja = '" . $request->get('caja', 'post') . "' ";
                $caja = $database->getRow($sql);

                if ($caja['saldo'] < $request->get('montomovimiento', 'post')) {
                    $errores .= 'No existe en la caja el monto que desea retirar. <br>';
                }
            }
        }
        if ((strlen($request->get('fechamovimiento', 'post')) != 10)) {
            $errores .= 'Debe ingresar una fecha. <br>';
        }
        if ((strlen($request->get('montomovimiento', 'post')) == 0)) {
            $errores .= 'Debe ingresar un monto. <br>';
        }

        if ($request->get('tipomovimiento', 'post') == -1) {
            $errores .= 'Debe seleccionar un tipo de movimiento. <br>';
        }

        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';

        //UN MOVIMIENTO NO SE PUEDE ELIMINAR SI ESTA REFERENCIADO A UNA COMPRA
        $consulta_info = $database->getRow("SELECT * FROM movimientoscaja WHERE movimiento = '" . $request->get('movimiento') . "' ");
        if ($consulta_info['compra']) {
            $errores .= 'No es posible eliminar este movimiento, ya que fue generado por consecuencia de una compra. <br>';
        }

        if ($errores != '') {
            $this->error['message'] = $errores;
        }

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $user = & $this->locator->get('user');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'MOVIMIENTO CAJA');

        if (($request->isPost()) && ($this->validateForm())) {

            // <editor-fold defaultstate="collapsed" desc="FECHA">
            $fecha_d = '';
            if ($request->get('fechamovimiento', 'post') != '') {
                $dated = explode('/', $request->get('fechamovimiento', 'post'));
                $fecha_d = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
                
            } else {
                $fecha_d = NULL;
            }

            // </editor-fold>
            //$sql2 = "INSERT IGNORE INTO articulos SET tipoproducto='".$request->get('tipoproducto', 'post')."', modelo = '".$request->get('auto_modelo_marca', 'post')."', estadoarticulo = 'DI', tipogama='".$request->get('tipogama', 'post')."', mnu='".$request->get('tipogama', 'post')."', concoddebarra='".$request->get('concoddebarra', 'post')."', precio='".$request->get('precio', 'post')."', au_usuario='".$user->getPERSONA()."',au_accion='A',au_fecha_hora=NOW()";
            $sql = "INSERT IGNORE INTO movimientoscaja SET caja='?', fechamovimiento='?', montomovimiento = '?', tipomovimiento='?',observacion = '?',  au_usuario='?',au_accion='A',au_fecha_hora=NOW()";
            $consulta = $database->parse($sql, $request->get('caja', 'post'), $fecha_d, $request->get('montomovimiento', 'post'), $request->get('tipomovimiento', 'post'), $request->get('observacion', 'post'), $user->getPERSONA());
            $database->query($consulta);

            $tipomovimiento = $request->get('tipomovimiento', 'post');
            if ($tipomovimiento == 'INGRESO') {
                //ACTUALIZO EL SALDO SEGUN EL TIPO DE MOVIMIENTO
                $sql = "UPDATE cajas SET saldo=saldo + '?', au_usuario='?',au_accion='M',au_fecha_hora=NOW() WHERE caja = '?'";
                $consulta = $database->parse($sql, $request->get('montomovimiento', 'post'), $user->getPERSONA(), $request->get('caja', 'post'));
                $database->query($consulta);
            }
            if ($tipomovimiento == 'EGRESO') {
                //ACTUALIZO EL SALDO SEGUN EL TIPO DE MOVIMIENTO
                $sql = "UPDATE cajas SET saldo=saldo - '?', au_usuario='?',au_accion='M',au_fecha_hora=NOW() WHERE caja = '?'";
                $consulta = $database->parse($sql, $request->get('montomovimiento', 'post'), $user->getPERSONA(), $request->get('caja', 'post'));
                $database->query($consulta);
            }

            $id = $database->getLastId();
            $cache->delete('movimientoscaja');
            $session->set('message', 'Se agreg&oacute; el movimiento a la caja: ' . $id);

            $response->redirect($url->ssl('movimientoscaja', 'index', array('caja' => $request->get('caja', 'post'))));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $user = & $this->locator->get('user');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos del Articulo');

        if (($request->isPost()) && ($this->validateForm())) {

            $sql = "SELECT * FROM cajas WHERE caja = '" . $request->get('caja', 'get') . "' ";
            $caja = $database->getRow($sql);

            $sql = "UPDATE movimientoscaja SET observacion='?', au_usuario='?',au_accion='M',au_fecha_hora=NOW() WHERE movimiento='?' ";
            $consulta = $database->parse($sql, $request->get('observacion', 'post'), $user->getPERSONA(), $request->get('movimiento', 'post'));
            $database->query($consulta);

            $cache->delete('cajas');

            $session->set('message', "Se ah cerrado la caja número " . $request->get('caja', 'post') . ".");

            $response->redirect($url->ssl('movimientoscaja', 'index', array('caja' => $request->get('caja', 'post'))));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos del Movimiento');
        if (($request->isPost())) {
            $response->redirect($url->ssl('movimientoscaja', 'index', array('movimiento' => $request->get('movimiento', 'get'), 'caja' => $request->get('caja', 'get'))));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        if (($request->get('movimiento')) && ($this->validateDelete())) {

            $id = $request->get('movimiento', 'get');
            $id_caja = $request->get('caja', 'get');

            //TODO: FALTA PROGRAMAR LA ELIMINACION Y VALIDACION VER SI NO HAY RELACIONES
            $sql = "SELECT * FROM movimientoscaja WHERE movimiento = '" . $id . "' ";
            $movimientocaja = $database->getRow($sql);

            if ($movimientocaja['tipomovimiento'] == 'INGRESO') {
                //ACTUALIZO EL SALDO SEGUN EL TIPO DE MOVIMIENTO
                $sql = "UPDATE cajas SET saldo=saldo - '?', au_usuario='?',au_accion='M',au_fecha_hora=NOW() WHERE caja = '?'";
                $consulta = $database->parse($sql, $movimientocaja['montomovimiento'], $user->getPERSONA(), $id_caja);
                $database->query($consulta);
            }
            if ($movimientocaja['tipomovimiento'] == 'EGRESO') {
                //ACTUALIZO EL SALDO SEGUN EL TIPO DE MOVIMIENTO
                $sql = "UPDATE cajas SET saldo=saldo + '?', au_usuario='?',au_accion='M',au_fecha_hora=NOW() WHERE caja = '?'";
                $consulta = $database->parse($sql, $movimientocaja['montomovimiento'], $user->getPERSONA(), $id_caja);
                $database->query($consulta);
            }

            $sql = "DELETE FROM movimientoscaja WHERE movimiento = '" . $id . "' ";
            $database->query($sql);

            $cache->delete('movimientoscaja');

            $session->set('message', 'Se ha eliminado el movimiento de la caja: ' . $id_caja);

            $response->redirect($url->ssl('movimientoscaja', 'index', array('caja' => $id_caja)));
        }

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function exportXls() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        // </editor-fold>

        set_time_limit(0);

        //** PHPExcel **//
        require_once (DIR_INCLUDES . '/library/phpexcel/PHPExcel.php');

        date_default_timezone_set('America/Argentina/Buenos_Aires');
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set properties of the file
        $objPHPExcel->getProperties()->setCreator("NombreEmpresa")
                ->setLastModifiedBy("NombreEmpresa")
                ->setTitle("Planilla web exportada: " . date('d-m-Y'))
                ->setSubject("Planilla web exportada: " . date('d-m-Y'));

        //genero las columnas del excel
        $letra = 'A';
        $fila_inicial = 2; //fila desde donde empiezo a cargar la tabla en el excel. Arriba va el encabezado
        // <editor-fold defaultstate="collapsed" desc="PROPIEDADES DE LAS COLUMNAS">
        $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //COLUMNA A
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(11);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'ID Articulo');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        //COLUMNA B
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'MARCA');
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA C
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(22);
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', 'MODELO');
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA D
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', 'TIPO PRODUCTO');
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA E
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', 'GAMA');
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA F
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(14);
        $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', 'COD INTERNO');
        $objPHPExcel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        //COLUMNA G
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getStyle('G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('G1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1', 'COD DE BARRA');
        $objPHPExcel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        //COLUMNA H
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getStyle('H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('H1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1', 'PRECIO VTA');
        $objPHPExcel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        // </editor-fold>
        //CARGA PRODUCTOS
        //// <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        set_time_limit(0);

        if (!$session->get('cajas.search')) {
            $sql = "SELECT * FROM  vw_grilla_articulostodos  ";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT * FROM vw_grilla_articulostodos WHERE articulo LIKE '?' OR descestadoarticulo LIKE '?' OR desctipoproducto LIKE '?' OR descmodelo LIKE '?' OR descmarca LIKE '?' ";
            $conca = " AND ";
        }

        if ($session->get('cajas.tipoproducto') != '-1' && $session->get('cajas.tipoproducto') != '') {
            $sql .= $conca . " tipoproducto = '" . $session->get('cajas.tipoproducto') . "'  ";
        }

        $sql .= " ORDER BY marca, modelo ASC";

//		$consul = $database->splitQuery($database->parse($sql,'%' . $session->get('cajas.search') . '%','%' . $session->get('cajas.search') . '%'), $session->get('cajas.page'), $config->get('config_max_rows'));
//
//                $results = $database->getRows($consul);

        $consulta = $database->parse($sql, '%' . $session->get('cajas.search') . '%', '%' . $session->get('cajas.search') . '%', '%' . $session->get('cajas.search') . '%', '%' . $session->get('cajas.search') . '%', '%' . $session->get('cajas.search') . '%');
        $results = $database->getRows($consulta);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="LLANAR LOS DATOS">

        $num = $fila_inicial;

        foreach ($results as $result) {

            //COLUMNA A
            $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $num, $result['caja']);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            //COLUMNA B
            $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $num, $result['descmarca']);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            //COLUMNA C
            $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $num, $result['descmodelo']);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA D
            $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' . $num, $result['desctipoproducto']);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA E
            $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' . $num, $result['descgama']);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA F
            $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' . $num, $result['codbarrainterno']);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA G
            $objPHPExcel->getActiveSheet()->getStyle('G' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G' . $num, $result['codbarra']);
            $objPHPExcel->getActiveSheet()->getStyle('G' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA H
            $objPHPExcel->getActiveSheet()->getStyle('H' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H' . $num, '0');
            $objPHPExcel->getActiveSheet()->getStyle('H' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $num++;
        }

        //SEGUIR EN COMPLETAR EL EXCEL
        //POR CADA INSCRIPTO IR BUSCANDO CADA RESPUESTA Y COMPLETAR SEGUN CORRESPONDA
        // </editor-fold>
        // Nombre de la hoja del libro
        $objPHPExcel->getActiveSheet()->setTitle('ExportArticulos');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Save Excel 2007 file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');

        // It will be called file.xls | esta linea hace que sea una descarga y no una pestaña aparte
        header('Content-Disposition: attachment; filename="ExportArticulos.xls"');

        header('Cache-Control: max-age=0');

        // Write file to the browser
        $objWriter->save('php://output');
    }

}

?>