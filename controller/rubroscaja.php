<?php

class ControllerRubroscaja extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'GESTION DE RUBROS CAJA');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
        $request = & $this->locator->get('request');
        // </editor-fold>
         // <editor-fold defaultstate="collapsed" desc="LIMPIA FILTRO Y VARIBLES DE SESSION">

        if ($request->get('filtra') == NULL) {

            $session->set('rubroscaja.search', '');
            $session->set('rubroscaja.sort', '');
            $session->set('rubroscaja.order', '');
            $session->set('rubroscaja.page', '');

            $view->set('search', '');
            $view->set('rubroscaja.search', '');
                        
            $cache->delete('rubroscaja');
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">

        $cols = array();

        $cols[] = array(
            'name' => 'Rubro',
            'sort' => 'rubro',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Descripci&oacute;n',
            'sort' => 'descripcion',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'rubro',
            'descripcion'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        set_time_limit(0);

        if (!$session->get('rubroscaja.search')) {
            $sql = "SELECT * FROM rubroscaja  ";
        } else {
            $sql = "SELECT * FROM rubroscaja WHERE rubro LIKE '?' OR descripcion LIKE '?'";
        }

        if (in_array($session->get('rubroscaja.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('rubroscaja.sort') . " " . (($session->get('rubroscaja.order') == 'desc') ? 'desc' : 'asc');
        } else {
            $sql .= " ORDER BY descripcion ASC";
        }

//		$consul = $database->splitQuery($database->parse($sql,'%' . $session->get('rubroscaja.search') . '%','%' . $session->get('rubroscaja.search') . '%'), $session->get('rubroscaja.page'), $config->get('config_max_rows'));
//
//                $results = $database->getRows($consul);

        $consulta = $database->parse($sql, '%' . $session->get('rubroscaja.search') . '%', '%' . $session->get('rubroscaja.search') . '%');
        $results = $database->getRows($consulta);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['rubro'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['descripcion'],
                'align' => 'left',
                'default' => 0
            );


            $action = array();

            if ($user->hasPermisos($user->getPERSONA(), 'rubroscaja', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa fa-fw fa-pencil',
                    'text' => $language->get('button_update'),
                    'prop_a' => array('href' => $url->ssl('rubroscaja', 'update', array('rubro' => $result['rubro'])))
                );
            }


            if ($user->hasPermisos($user->getPERSONA(), 'rubroscaja', 'B')) {
                $action[] = array(
                    'icon' => 'img/iconos-11.png',
                    'text' => $language->get('button_delete'),
                    'class' => 'fa fa-fw fa-trash-o',
                    'prop_a' => array('href' => "javascript:ActionDelete('" . $url->ssl('rubroscaja', 'delete', array('rubro' => $result['rubro'])) . "');")
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'rubroscaja', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw fa-search',
                    'text' => $language->get('button_consult'),
                    'prop_a' => array('href' => $url->ssl('rubroscaja', 'consulta', array('rubro' => $result['rubro'])))
                );
            }



            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        //<editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('rubroscaja.page'));

        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        $view->set('heading_title', 'Rubros caja');
        $view->set('placeholder_buscar', 'BUSCA POR ID O DESCRIPCION');

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        //$view->set('consultaVacia', $url->ssl('consulta','consulta_vacia'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        //$view->set('button_consultavacia', "Vacía");
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('rubroscaja.search'));
        $view->set('sort', $session->get('rubroscaja.sort'));
        $view->set('order', $session->get('rubroscaja.order'));
        $view->set('page', $session->get('rubroscaja.page'));

        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $view->set('titulo_ventana', '::. Exportar');

        $view->set('entry_modificar', "Modificar");
        $view->set('entry_agregar', "Agregar");

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('rubroscaja', 'page'));
        $view->set('list', $url->ssl('rubroscaja'));

        if ($user->hasPermisos($user->getPERSONA(), 'rubroscaja', 'A')) {
            $view->set('insert', $url->ssl('rubroscaja', 'insert'));
        }

        if ($user->hasPermisos($user->getPERSONA(), 'rubroscaja', 'C'))
            $view->set('export', $url->ssl('rubroscaja', 'exportar'));

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_rubroscaja.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">
//		if ($request->has('rubro', 'post')) {
//                    $session->set('rubroscaja.pais',$request->get('rubro','post'));              
//                }

        if ($request->has('search', 'post')) {
            $session->set('rubroscaja.search', $request->get('search', 'post'));
        }


        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('rubroscaja.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('rubroscaja.order', (($session->get('rubroscaja.sort') == $request->get('sort', 'post')) && ($session->get('rubroscaja.order') == 'asc')) ? 'desc' : 'asc');
        }

        if ($request->has('sort', 'post')) {
            $session->set('rubroscaja.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('rubroscaja', 'index', array('filtra' => '1')));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $template = & $this->locator->get('template');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('heading_title', 'DATOS DEL RUBRO');
        $view->set('entry_descripcion', 'Descripci&oacute;n:');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('tab_general', $language->get('tab_general'));

        $view->set('rubro', $request->get('rubro'));

        if (($request->get('rubro')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM rubroscaja WHERE rubro = '" . $request->get('rubro') . "'";
            $objeto_info = $database->getRow($consulta);
        }

        if ($request->has('rubro', 'post')) {
            $view->set('rubro', $request->get('rubro', 'post'));
            $objeto_id = @$objeto_info['rubro'];
        } else {
            $view->set('rubro', $request->get('rubro', 'get'));
            $objeto_id = @$objeto_info['rubro'];
        }

        if ($request->has('descripcion', 'post')) {
            $view->set('descripcion', $request->get('descripcion', 'post'));
        } else {
            $view->set('descripcion', @$objeto_info['descripcion']);
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        $view->set('error_texto_error', @$this->error['texto_error']);
        //$view->set('error_titulo', @$this->error['titulo']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        //$view->set('action', $url->ssl('rubroscaja', $request->get('action'), array('rubro' => $request->get('rubro'))));
        $view->set('actionBoton', $request->get('action'));
        $view->set('cancel', $url->ssl('rubroscaja'));
        // </editor-fold>

        return $view->fetch('content/rubrocaja.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        if ((strlen($request->get('descripcion', 'post')) == 0)) {
            $errores = 'Debe ingresar la descripción';
        }

        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';
        $consulta_info = $database->getRow("SELECT COUNT(*) as total FROM subrubroscaja WHERE rubro = '" . $request->get('rubro') . "'");
        if (@$consulta_info['total'] != 0) {
            $errores .= 'No es posible eliminar el rubro, tiene subrubros asignados. <br>';
        }
        
        $consulta_info = $database->getRow("SELECT COUNT(*) as total FROM cajasdiaria WHERE rubro = '" . $request->get('rubro') . "'");
        if (@$consulta_info['total'] != 0) {
            $errores .= 'No es posible eliminar el rubro, tiene al menos un movimiento de caja asigado. <br>';
        }

        if ($errores != '') {
            $this->error['message'] = $errores;
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos del rubro');

        if (($request->isPost()) && ($this->validateForm())) {
            $sql = "INSERT IGNORE INTO rubroscaja SET descripcion = '?' ";
            $consulta = $database->parse($sql, $request->get('descripcion', 'post'));
            $database->query($consulta);

            $id = $database->getLastId();
            $cache->delete('rubroscaja');
            $session->set('message', 'Se agreg&oacute; el rubro: ' . $id);

            $response->redirect($url->ssl('rubroscaja'));
        }
        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos del rubro');
        if (($request->isPost()) && ($this->validateForm())) {

            $id = $request->get('rubro', 'post');
            $sql = "UPDATE rubroscaja SET descripcion ='?'  WHERE rubro='?' ";
            $consulta = $database->parse($sql, $request->get('descripcion', 'post'), $id);
            $database->query($consulta);

            $cache->delete('rubroscaja');

            $session->set('message', 'Se actualiz&oacute; el rubro: ' . $id);

            $response->redirect($url->ssl('rubroscaja'));
        }
        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos del rubro');
        if (($request->isPost())) {
            $response->redirect($url->ssl('rubroscaja'));
        }

        $response->set($this->getForm());
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        if (($request->get('rubro')) && ($this->validateDelete())) {

            $id = $request->get('rubro', 'get');

            //TODO: FALTA PROGRAMAR LA ELIMINACION Y VALIDACION VER SI NO HAY RELACIONES
            $sql = "DELETE FROM rubroscaja WHERE rubro = '" . $id . "' ";
            $database->query($sql);

            $cache->delete('rubroscaja');

            $session->set('message', 'Se ha eliminado el rubro: ' . $id);

            $response->redirect($url->ssl('rubroscaja'));
        }

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

}

?>