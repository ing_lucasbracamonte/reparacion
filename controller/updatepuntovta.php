<?php

class ControllerUpdatepuntovta extends Controller {

    var $error = array();

    function index() {
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        $user = & $this->locator->get('user');
        $url = & $this->locator->get('url');

        $template->set('title', 'Mis datos');

        $template->set('content', $this->getForm());

        $template->set($module->fetch());



        $response->set($template->fetch('layoutlogin.tpl'));
    }

    function getForm() {
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $user = & $this->locator->get('user');
        $session = & $this->locator->get('session');
        $template = & $this->locator->get('template');
        $view = $this->locator->create('template');

        $view->set('heading_title', 'Debe asignarse el punto de venta antes de ingresar al sistema.');

//        if ($request->get('completardatos', 'post') == 'si') {
        $view->set('heading_description', 'Informaci&oacute;n Personal: Debe asignarse el punto de venta antes de ingresar al sistema');
        $view->set('cancel', $url->ssl('logout'));
//        } else {
//            $view->set('heading_description', 'Complete o modifique sus datos personales');
//            $view->set('cancel', $this->getDefault());
//        }
        //$view->set('action', $url->ssl('misdatos', 'update'));



        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        $template->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');
        $view->set('error_persona', @$this->error['persona']);

        $view->set('error_texto_error', @$this->error['texto_error']);

        if (($user->getPERSONA()) && (!$request->isPost())) {
            $consulta = "SELECT  *  FROM personas  WHERE persona = '?'";
            $persona_info = $database->getRow($database->parse($consulta, $user->getPERSONA()));
        }
        if ($request->has('persona', 'post')) {
            $view->set('persona', $user->getPERSONA());
            $persona_id = $request->get('persona', 'post');
        } else {
            $view->set('persona', @$persona_info['persona']);
            $persona_id = @$persona_info['persona'];
        }


        $view->set('entry_puntovta', 'Punto de Venta:');
        if ($request->has('puntovtaasignado', 'post')) {
            $view->set('puntovtaasignado', $request->get('puntovtaasignado', 'post'));
            $puntovta = $request->get('puntovtaasignado', 'post');
        } else {
            //va el punto de venta que tiene asignado el vendedor
            //$ptovtaasignado = $database->getRow("SELECT puntovtaasignado FROM personas where persona = '".$user->getPERSONA()."' ");

            $view->set('puntovtaasignado', @$objeto_info['puntovtaasignado']);
            $puntovta = @$objeto_info['puntovtaasignado'];
        }

        $view->set('puntosdeventa', $database->getRows("SELECT vpv.persona, pv.puntovta, pv.descripcion AS descpuntoventa FROM vendedorespuntodeventa vpv LEFT JOIN puntosdeventa pv ON vpv.puntovta = pv.puntovta WHERE vpv.persona = '" . $user->getPERSONA() . "' ORDER BY descpuntoventa ASC"));


        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('updatepuntovta', 'update'));
        $view->set('actionBoton', $request->get('action'));
        $view->set('cancel', $url->ssl('home'));
        // </editor-fold>

        return $view->fetch('content/updatepuntovta.tpl');
    }

    function validateForm() {
        $request = & $this->locator->get('request');
        $mail = & $this->locator->get('mail');
        $user = & $this->locator->get('user');
        $database = & $this->locator->get('database');
        $common = & $this->locator->get('common');
        $mensaje = '';

        $errores = '';

//        if ((strlen($request->get('persona', 'post')) == 0)) {
//            //$this->error['persona'] = 'Debe ingresar el n&uacute;mero de documento';
//            $errores .= 'Debe ingresar el n&uacute;mero de documento. <br>';
//        }
//
//        if (strlen($request->get('persona', 'post')) < 7 || !is_numeric($request->get('persona', 'post'))) {
//            //$this->error['persona'] = 'No es un n&uacute;mero de documento v&aacute;lido.';
//            $errores .= 'No es un n&uacute;mero de documento v&aacute;lido.. <br>';
//        }
//
//        if ((strlen($request->get('apellido', 'post')) < 2) || (strlen($request->get('apellido', 'post')) > 100)) {
//            //$this->error['apellido'] = 'Debe ingresar el apellido';
//            $errores .= 'Debe ingresar el apellido. <br>';
//        }
//
//        if ((strlen($request->get('nombre', 'post')) < 2) || (strlen($request->get('nombre', 'post')) > 100)) {
//            //$this->error['nombre'] = 'Debe ingresar el nombre';
//            $errores .= 'Debe ingresar el nombre. <br>';
//        }
//		if ((strlen($request->get('domicilio', 'post')) == 0)) {
//			$this->error['domicilio'] = 'Debe ingresar el domicilio';
//		}
//		if ($request->has('localidad', 'post')) {
//			$view->set('localidad', $request->get('localidad', 'post'));
//		} else {
//			$view->set('localidad', @$region_info['localidad']);
//		}
//        if (!$request->has('fechanacimiento', 'post')) {
//            //$this->error['fechanacimiento'] = 'Debe selccionar una fecha de nacimiento';
//            $errores .= 'Debe selccionar una fecha de nacimiento. <br>';
//        }
//		if ((strlen($request->get('telefono', 'post')) == 0)) {
//			$this->error['telefono'] = 'Debe ingresar el tel&eacute;fono';
//		}
//        if ((strlen($request->get('mail', 'post')) > 0)) {
//            if (!$common->validarDuplicado($request->get('mail', 'post'), $database, $mensaje, $user->getPERSONA())) {
//                //$this->error['mail'] = $mensaje;
//                $errores .= $mensaje . ' <br>';
//            }
//
//            if ($request->get('remail', 'post') != $request->get('mail', 'post')) {
//                //$this->error['remail'] = 'El mail y la confirmaci&oacute;n no coinciden';
//                $errores .= 'El mail y la confirmaci&oacute;n no coinciden. <br>';
//            }
//        }
//
//
//        if ((strlen($request->get('newpassword', 'post')) > 0)) {
//            if ((strlen($request->get('newrepassword', 'post')) == 0)) {
//                //$this->error['newrepassword'] = 'Debe ingresar la confirmacion de la contraseña';
//                $errores .= 'Debe ingresar la confirmacion de la contraseña. <br>';
//            }
//            if ($request->get('newrepassword', 'post') != $request->get('newpassword', 'post')) {
//                //$this->error['newrepassword'] = 'La contraseña y la confirmacion no coinciden';
//                $errores .= 'La contraseña y la confirmacion no coinciden. <br>';
//            }
//        }

        if (($request->get('puntovtaasignado', 'post') == '-1')) {
            //$this->error['persona'] = 'Debe ingresar el n&uacute;mero de documento';
            $errores .= 'Debe seleccionar un punto de venta. <br>';
        }


        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function completardatos() {
        $response = & $this->locator->get('response');
        $request = & $this->locator->get('request');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        $user = & $this->locator->get('user');
        $url = & $this->locator->get('url');

        $template->set('title', 'Mis datos');

        $request->set('completardatos', 'si', 'post');

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $user = & $this->locator->get('user');
        $common = & $this->locator->get('common');
        // </editor-fold>
        $template->set('title', 'Mis datos');

        if (($request->isPost()) && ($this->validateForm())) {

            $id_puntovta = $request->get('puntovtaasignado', 'post');

            $puntovta_info = $database->getRow("SELECT * FROM  puntosdeventa WHERE puntovta = '" . $id_puntovta . "' ");
            $user->setDescpuntovtaasignado($id_puntovta, $puntovta_info['descripcion']); //  $session->set('descpuntovtaasignado', $puntovta_info['descripcion']);

            $sql = "UPDATE personas SET  puntovtaasignado='?' WHERE persona = '?'";
            $sql = $database->parse($sql, $request->get('puntovtaasignado', 'post'), $user->getPERSONA());
            $database->query($sql);

            $cache->delete('persona');

            $session->set('message', 'Se ha asignado el punto de venta de forma exitosa.');

            //ENVIAR MAIL SI CAMBIO EL MAIL PARA VALIDAR

            $response->redirect($url->ssl('home'));
        }
        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layoutlogin.tpl'));
    }

}

?>
