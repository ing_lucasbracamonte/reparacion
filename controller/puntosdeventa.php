<?php

class ControllerPuntosdeventa extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'GESTIÓN DE PTOS DE VTA');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
        $request = & $this->locator->get('request');
        // </editor-fold>
         // <editor-fold defaultstate="collapsed" desc="LIMPIA FILTRO Y VARIBLES DE SESSION">

        if ($request->get('filtra') == NULL) {

            $session->set('puntosdeventa.search', '');
            $session->set('puntosdeventa.sort', '');
            $session->set('puntosdeventa.order', '');
            $session->set('puntosdeventa.page', '');

            $view->set('search', '');
            $view->set('puntosdeventa.search', '');
                        
            $cache->delete('puntosdeventa');
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">

        $cols = array();

        $cols[] = array(
            'name' => 'CÓDIGO',
            'sort' => 'puntovta',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'DESCRIPCIÓN',
            'sort' => 'descripcion',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => '¿PROPIO?',
            'sort' => 'descripcion',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'LISTA DE PRECIO',
            'sort' => 'listaprecio',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'DIRECCIÓN',
            'sort' => 'direccion',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'puntovta',
            'descripcion'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        set_time_limit(0);

        if (!$session->get('puntosdeventa.search')) {
            $sql = "SELECT * FROM vw_grilla_puntosdeventa  ";
        } else {
            $sql = "SELECT * FROM vw_grilla_puntosdeventa WHERE puntovta LIKE '?' OR descripcion LIKE '?'  ";
        }

        if (in_array($session->get('puntosdeventa.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('puntosdeventa.sort') . " " . (($session->get('puntosdeventa.order') == 'desc') ? 'desc' : 'asc');
        } else {
            $sql .= " ORDER BY descripcion ASC";
        }

//		$consul = $database->splitQuery($database->parse($sql,'%' . $session->get('puntosdeventa.search') . '%','%' . $session->get('puntosdeventa.search') . '%'), $session->get('puntosdeventa.page'), $config->get('config_max_rows'));
//
//                $results = $database->getRows($consul);

        $consulta = $database->parse($sql, '%' . $session->get('puntosdeventa.search') . '%', '%' . $session->get('puntosdeventa.search') . '%');
        $results = $database->getRows($consulta);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['puntovta'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['descripcion'],
                'align' => 'left',
                'default' => 0
            );

            $propio = 'Si';
            if ($result['puntovtapropio'] == '0') {
                $propio = 'No';
            }
            $cell[] = array(
                'value' => $propio,
                'align' => 'left',
                'default' => 0
            );

            $desclista = 'SIN LISTA';
            if (@$result['desclistadeprecio'])
                $desclista = @$result['desclistadeprecio'];
            $cell[] = array(
                'value' => $desclista,
                'align' => 'left',
                'default' => 0
            );

            $dir = '-';
            if (@$result['direccion'])
                $dir = @$result['direccion'];
            $cell[] = array(
                'value' => $dir,
                'align' => 'left',
                'default' => 0
            );

            $action = array();

            if ($user->hasPermisos($user->getPERSONA(), 'puntosdeventa', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa fa-fw fa-pencil',
                    'text' => $language->get('button_update'),
                    'prop_a' => array('href' => $url->ssl('puntosdeventa', 'update', array('puntovta' => $result['puntovta'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'puntosdeventa', 'B')) {
                $action[] = array(
                    'icon' => 'img/iconos-11.png',
                    'text' => $language->get('button_delete'),
                    'class' => 'fa fa-fw fa-trash-o',
                    'prop_a' => array('href' => "javascript:ActionDelete('" . $url->ssl('puntosdeventa', 'delete', array('puntovta' => $result['puntovta'])) . "');")
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'puntosdeventa', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw fa-search',
                    'text' => $language->get('button_consult'),
                    'prop_a' => array('href' => $url->ssl('puntosdeventa', 'consulta', array('puntovta' => $result['puntovta'])))
                );
            }



//                            $action[] = array(
//                                     'icon' => 'img/light-blue_icons/iconos-16.png',
//                                    'text' => 'integrantes',
//                                    //'href' => $url->ssl('inscripcionesactividades', 'index', array('Actividad' => $result['Actividad']))
//                                    'prop_a' => array('href' => $url->ssl('integrantesclubes', 'index', array('modelo' => $result['modelo'])))
//                                        );
//			if ($user->hasPermisos($user->getPERSONA(),'encuesta','C')) {


            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        //<editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('puntosdeventa.page'));

        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        $view->set('heading_title', 'PUNTOS DE VENTA');
        $view->set('placeholder_buscar', 'BUSCA POR CÓDIGO O DESCRIPCIÓN');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        //$view->set('consultaVacia', $url->ssl('consulta','consulta_vacia'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        //$view->set('button_consultavacia', "Vacía");
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('puntosdeventa.search'));
        $view->set('sort', $session->get('puntosdeventa.sort'));
        $view->set('order', $session->get('puntosdeventa.order'));
        $view->set('page', $session->get('puntosdeventa.page'));

        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $view->set('titulo_ventana', '::. Exportar');

        $view->set('entry_modificar', "Modificar");
        $view->set('entry_agregar', "Agregar");

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('puntosdeventa', 'page'));
        $view->set('list', $url->ssl('puntosdeventa'));

        if ($user->hasPermisos($user->getPERSONA(), 'puntosdeventa', 'A')) {
            $view->set('insert', $url->ssl('puntosdeventa', 'insert'));
        }

        if ($user->hasPermisos($user->getPERSONA(), 'puntosdeventa', 'C'))
            $view->set('export', $url->ssl('puntosdeventa', 'exportar'));

        //$view->set('addPais', $url->ssl('puntosdeventa','insert'));
        // $view->set('updatePersona', $url->ssl('paciente','update', array('persona' => $result['persona'])));
        //$view->set('updatePais', $url->ssl('puntosdeventa','update'));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_puntosdeventa.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">
//		if ($request->has('modelo', 'post')) {
//                    $session->set('puntosdeventa.pais',$request->get('modelo','post'));              
//                }

        if ($request->has('search', 'post')) {
            $session->set('puntosdeventa.search', $request->get('search', 'post'));
        }


        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('puntosdeventa.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('puntosdeventa.order', (($session->get('puntosdeventa.sort') == $request->get('sort', 'post')) && ($session->get('puntosdeventa.order') == 'asc')) ? 'desc' : 'asc');
        }

        if ($request->has('sort', 'post')) {
            $session->set('puntosdeventa.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('puntosdeventa', 'index', array('filtra' => '1')));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $template = & $this->locator->get('template');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('heading_title', 'DATOS DEL PTO DE VTA');
        $view->set('entry_descripcion', 'Descripción:');
        $view->set('entry_empresa', 'Empresa:');
        $view->set('entry_direccion', 'Direccion:');
        $view->set('entry_numero', 'Nro');
        $view->set('entry_piso', 'piso');
        $view->set('entry_departamento', 'Dto:');
        $view->set('entry_localidad', 'Localidad:');
        $view->set('entry_zona', 'Zona:');
        $view->set('entry_listaprecio', 'Lista de precio:');
        $view->set('entry_puntovtapropio', 'Es un punto de venta propio?');
        $view->set('entry_fechabaja', 'Fecha baja:');
        $view->set('entry_motivobaja', 'motivo baja:');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('tab_general', $language->get('tab_general'));

        $view->set('puntovta', $request->get('puntovta'));

        if (($request->get('puntovta')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM vw_grilla_puntosdeventa WHERE puntovta = '" . $request->get('puntovta') . "' ";
            $objeto_info = $database->getRow($consulta);
        }

        if ($request->has('puntovta', 'post')) {
            $view->set('puntovta', $request->get('puntovta', 'post'));
            $objeto_id = @$objeto_info['puntovta'];
        } else {
            $view->set('puntovta', $request->get('puntovta', 'get'));
            $objeto_id = @$objeto_info['puntovta'];
        }

        if ($request->has('zona', 'post')) {
            $view->set('zona', $request->get('zona', 'post'));
        } else {
            $view->set('zona', @$objeto_info['zona']);
        }
        $view->set('zonas', $database->getRows("SELECT * FROM zonas ORDER BY descripcion ASC"));

        if ($request->has('empresa', 'post')) {
            $view->set('empresa', $request->get('empresa', 'post'));
        } else {
            $view->set('empresa', @$objeto_info['empresa']);
        }
        $view->set('empresas', $database->getRows("SELECT * FROM empresas ORDER BY descripcion ASC"));

        if ($request->has('direccion', 'post')) {
            $view->set('direccion', $request->get('direccion', 'post'));
        } else {
            $view->set('direccion', @$objeto_info['direccion']);
        }

        if ($request->has('numero', 'post')) {
            $view->set('numero', $request->get('numero', 'post'));
        } else {
            $view->set('numero', @$objeto_info['numero']);
        }

        if ($request->has('piso', 'post')) {
            $view->set('piso', $request->get('piso', 'post'));
        } else {
            $view->set('piso', @$objeto_info['piso']);
        }

        if ($request->has('departamento', 'post')) {
            $view->set('departamento', $request->get('departamento', 'post'));
        } else {
            $view->set('departamento', @$objeto_info['departamento']);
        }

        if ($request->has('auto_localidad', 'post')) {
            $view->set('auto_localidad', $request->get('auto_localidad', 'post'));
        } else {
            $consulta = "SELECT localidad  as id, desclocalidad as value "
                    . "FROM vw_grilla_localidades WHERE localidad = '?'";
            $localidad_info = $database->getRow($database->parse($consulta, @$objeto_info['localidad']));
            $view->set('auto_localidad', @$localidad_info['value']);
        }
        if ($request->has('auto_localidad_puntovta', 'post')) {
            $view->set('auto_localidad_puntovta', $request->get('auto_localidad_puntovta', 'post'));
        } else {
            $view->set('auto_localidad_puntovta', @$objeto_info['localidad']);
        }
        $view->set('script_busca_localidades', $url->rawssl('puntosdeventa', 'getLocalidad'));

        if ($request->has('descripcion', 'post')) {
            $view->set('descripcion', $request->get('descripcion', 'post'));
        } else {
            $view->set('descripcion', @$objeto_info['descripcion']);
        }

        if ($request->has('listaprecio', 'post')) {
            $view->set('listaprecio', $request->get('listaprecio', 'post'));
        } else {
            $view->set('listaprecio', @$objeto_info['listaprecio']);
        }
        $view->set('listasdeprecio', $database->getRows("SELECT * FROM listasdeprecio ORDER BY descripcion ASC"));

        if ($request->has('puntovtapropio', 'post')) {
            $view->set('puntovtapropio', $request->get('puntovtapropio', 'post'));
        } else {
            $view->set('puntovtapropio', @$objeto_info['puntovtapropio']);
        }

        if ($request->has('fechabaja', 'post')) {
            $view->set('fechabaja', $request->get('fechabaja', 'post'));
        } else {
            if (@$objeto_info['fechabaja'] != '')
                $view->set('fechabaja', date('d/m/Y', strtotime(@$objeto_info['fechabaja'])));
            else
                $view->set('fechabaja', '');
        }

        if ($request->has('motivobaja', 'post')) {
            $view->set('motivobaja', $request->get('motivobaja', 'post'));
        } else {
            $view->set('motivobaja', @$objeto_info['motivobaja']);
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');
        $view->set('error_texto_error', @$this->error['texto_error']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        //$view->set('action', $url->ssl('puntosdeventa', $request->get('action'), array('modelo' => $request->get('modelo'))));
        $view->set('actionBoton', $request->get('action'));
        $view->set('cancel', $url->ssl('puntosdeventa'));
        // </editor-fold>

        return $view->fetch('content/puntovta.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
//		if(@$this->error['puntovta'] == ''){
//                    $sql = "SELECT count(*) as total FROM puntosdeventa WHERE modelo and tipoproducto";
//                    $equipo = $database->getRow($database->parse($sql,$request->get('auto_modelo_marca', 'post'),$request->get('tipoproducto', 'post')));
//                
//                    if($equipo['total'] > 0 && $request->get('accion_form', 'post') == 'insert'){
//                            $this->error['id'] = 'Esta puntovta ya existe en el sistema';
//                    }                    
//                }
        if ((strlen($request->get('descripcion', 'post')) == 0)) {
            $errores = 'Debe ingresar una descripción. <br>';
        }
//                if($request->get('tipoproducto', 'post') == '-1'){
//                        $this->error['tipoproducto'] = 'Debe seleccionar un tipo de producto';
//                }
//                
//                if ((strlen($request->get('auto_modelo', 'post')) == 0)) {
//			$this->error['modelo'] = 'Debe ingresar un modelo';
//		}
        if ($request->get('listaprecio', 'post') == '-1') {
            $errores .= 'Debe seleccionar una lista de precio. <br>';
        }
        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>


        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';

        if ($errores != '') {
            $this->error['message'] = $errores;
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $user = & $this->locator->get('user');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'PTO DE VTA');

        if (($request->isPost()) && ($this->validateForm())) {
            // <editor-fold defaultstate="collapsed" desc="FECHA">
            $fecha_d = '';
            if ($request->get('fechabaja', 'post') != '') {
                $dated = explode('/', $request->get('fechabaja', 'post'));
                $fecha_d = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
            } else {
                $fecha_d = NULL;
            }

            // </editor-fold>

            $puntovtapropio = 0;
            if ($request->get('puntovtapropio', 'post') == 'puntovtapropio') {
                $puntovtapropio = 1;
            }

            //$sql2 = "INSERT IGNORE INTO puntosdeventa SET tipoproducto='".$request->get('tipoproducto', 'post')."', modelo = '".$request->get('auto_modelo_marca', 'post')."', estadopuntovta = 'DI', tipogama='".$request->get('tipogama', 'post')."', mnu='".$request->get('tipogama', 'post')."', concoddebarra='".$request->get('concoddebarra', 'post')."', precio='".$request->get('precio', 'post')."', au_usuario='".$user->getPERSONA()."',au_accion='A',au_fecha_hora=NOW()";
            $sql = "INSERT IGNORE INTO puntosdeventa SET descripcion='?', direccion = '?', numero = '?',  piso='?', departamento='?', localidad='?', zona='?',empresa='?',listaprecio='?',puntovtapropio='?',au_usuario='?',au_accion='A',au_fecha_hora=NOW()";
            $consulta = $database->parse($sql, $request->get('descripcion', 'post'), $request->get('direccion', 'post'), $request->get('numero', 'post'), $request->get('piso', 'post'), $request->get('departamento', 'post'), $request->get('auto_localidad_puntovta', 'post'), $request->get('zona', 'post'), $request->get('empresa', 'post'), $request->get('listaprecio', 'post'), $puntovtapropio, $user->getPERSONA());
            $database->query($consulta);

            $id = $database->getLastId();
            $cache->delete('puntosdeventa');
            $session->set('message', 'Se agreg&oacute; el punto de venta: ' . $id);

            $response->redirect($url->ssl('puntosdeventa'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $user = & $this->locator->get('user');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos del Punto de Vta');
        if (($request->isPost()) && ($this->validateForm())) {
            // <editor-fold defaultstate="collapsed" desc="FECHA">
            $fecha_d = '';
            if ($request->get('fechabaja', 'post') != '') {
                $dated = explode('/', $request->get('fechabaja', 'post'));
                $fecha_d = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
            } else {
                $fecha_d = NULL;
            }

            // </editor-fold>

            $puntovtapropio = 0;
            if ($request->get('puntovtapropio', 'post') == 'puntovtapropio') {
                $puntovtapropio = 1;
            }

            $id = $request->get('puntovta', 'post');
            $sql = "UPDATE puntosdeventa SET descripcion='?', direccion = '?', numero = '?',  piso='?', departamento='?', localidad='?', zona='?',empresa='?',listaprecio='?',puntovtapropio='?',au_usuario='?',au_accion='M',au_fecha_hora=NOW() WHERE puntovta='?' ";
            $consulta = $database->parse($sql, $request->get('descripcion', 'post'), $request->get('direccion', 'post'), $request->get('numero', 'post'), $request->get('piso', 'post'), $request->get('departamento', 'post'), $request->get('auto_localidad_puntovta', 'post'), $request->get('zona', 'post'), $request->get('empresa', 'post'), $request->get('listaprecio', 'post'), $puntovtapropio, $user->getPERSONA(), $id);
            $database->query($consulta);

            $cache->delete('puntosdeventa');

            $session->set('message', 'Se actualiz&oacute; el punto de venta: ' . $id);

            $response->redirect($url->ssl('puntosdeventa'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos del puntovta');
        if (($request->isPost())) {
            $response->redirect($url->ssl('puntosdeventa'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        if (($request->get('puntovta')) && ($this->validateDelete())) {

            $id = $request->get('puntovta', 'get');

            //TODO: FALTA PROGRAMAR LA ELIMINACION Y VALIDACION VER SI NO HAY RELACIONES
            //$sql = "DELETE FROM  WHERE  = '" . $id . "' ";
            //$database->query($sql);

            $cache->delete('puntosdeventa');

            $session->set('message', 'Se ha eliminado el punto de venta: ' . $id);

            $response->redirect($url->ssl('puntosdeventa'));
        }

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getLocalidad() {
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');

        $miDescripcion = $request->get('term', 'get');

        $sql = "SELECT localidad  as id, desclocalidad as value "
                . "FROM vw_grilla_localidades "
                . "WHERE descripcion LIKE '?' OR desclocalidad LIKE '?' OR descprovincia LIKE '?' OR cp LIKE '?'  "
                . "LIMIT 10";
        $consulta = $database->parse($sql, '%' . $miDescripcion . '%', '%' . $miDescripcion . '%', '%' . $miDescripcion . '%', '%' . $miDescripcion . '%');
        $codigo = $database->getRows($consulta);

        $varia = utf8_decode(json_encode($codigo));

        echo $varia;
    }


}

?>