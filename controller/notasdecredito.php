<?php

class ControllerNotasdecredito extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'NC');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
        $request = & $this->locator->get('request');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="LIMPIA FILTRO Y VARIBLES DE SESSION">

        if ($request->get('filtra') == NULL) {

            $session->set('notasdecredito.search', '');
            $session->set('notasdecredito.puntovta', '-1');
            $session->set('notasdecredito.desde', '');
            $session->set('notasdecredito.hasta', '');
        
            $session->set('notasdecredito.sort', '');
            $session->set('notasdecredito.order', '');
            $session->set('notasdecredito.page', '');

            $view->set('search', '');
            $view->set('notasdecredito.search', '');
            $view->set('notasdecredito.puntovta', '-1');
            $view->set('notasdecredito.desde', '');
            $view->set('notasdecredito.hasta', '');
            
            $session->set('notasdecredito.search', '');
            $session->set('notasdecredito.sort', '');
            $session->set('notasdecredito.order', '');
            $session->set('notasdecredito.page', '');

            $cache->delete('notasdecredito');
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">
        $cols = array();

        $cols[] = array(
            'name' => 'NC',
            'sort' => 'notacredito',
            'align' => 'left'
        );
        $cols[] = array(
            'name' => 'VENTA',
            'sort' => 'venta',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Fecha',
            'sort' => 'fecha',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Pto Vta',
            'sort' => 'puntovta',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Vendedor',
            'sort' => 'au_usuario',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Comprador',
            'sort' => 'personacomprador',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Total',
            'sort' => 'total',
            'align' => 'left'
        );


        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'notacredito',
            'venta',
            'fecha',
            'puntovta',
            'au_usuario',
            'personacomprador',
            'totalnc'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">

//        if (!$session->get('notasdecredito.search')) {
//            $sql = "SELECT * FROM vw_grilla_notasdecredito WHERE STR_TO_DATE(fecha, '%Y-%m-%d') = STR_TO_DATE(NOW(), '%Y-%m-%d') AND puntovta = '" . $user->getPuntovtaasignado() . "' ";
//            $conca = " AND ";
//        } else {
//            //SI ES EXENTO PODRIA ENCONTRAR CUALQUIER VENTA
//            if ($user->getExento() == 1) {
//                $sql = "SELECT * FROM vw_grilla_notasdecredito WHERE notacredito LIKE '" . '%' . $session->get('notasdecredito.search') . '%' . "' OR venta LIKE '" . '%' . $session->get('notasdecredito.search') . '%' . "'  ";
//                $conca = " AND ";
//            } else {
//                $sql = "SELECT * FROM vw_grilla_notasdecredito WHERE STR_TO_DATE(fecha, '%Y-%m-%dY') = STR_TO_DATE(NOW(), '%Y-%m-%d') AND puntovta = '" . $user->getPuntovtaasignado() . "' AND ( notacredito LIKE '" . '%' . $session->get('notasdecredito.search') . '%' . "' OR  venta LIKE '" . '%' . $session->get('notasdecredito.search') . '%' . "')  ";
//                $conca = " AND ";
//            }
//        }

        if (!$session->get('notasdecredito.search')) {
            $sql = "SELECT * FROM vw_grilla_notasdecredito   ";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT * FROM vw_grilla_notasdecredito  WHERE  ( notacredito LIKE '" . '%' . $session->get('notasdecredito.search') . '%' . "' OR  venta LIKE '" . '%' . $session->get('notasdecredito.search') . '%' . "')   ";
            $conca = " AND ";
        }
         //filtro por fecha
        // <editor-fold defaultstate="collapsed" desc="DESDE">
                $fecha_d = '';
                
                if ($session->get('notasdecredito.desde')) {
                    $dated = explode('/', $session->get('notasdecredito.desde'));
                    //$fecha_d = $dated[0] . '-' . $dated[1] . '-' . $dated[2];
                    $fecha_d = 	str_replace('/','-',$session->get('notasdecredito.desde'));
                }
                   
                
                if ($fecha_d != '') {
                    $sql .= $conca . " STR_TO_DATE(DATE_FORMAT(fecha, '%d-%m-%Y'), '%d-%m-%Y') >= STR_TO_DATE('" . $fecha_d . "', '%d-%m-%Y') ";
                    $conca = " AND ";
                }
                

                
                // </editor-fold>
            
        // <editor-fold defaultstate="collapsed" desc="HASTA">
                $fecha_h = '';
                
                if ($session->get('notasdecredito.hasta')) {
                    $dated = explode('/', $session->get('notasdecredito.hasta'));
                    //$fecha_d = $dated[0] . '-' . $dated[1] . '-' . $dated[2];
                    $fecha_h = 	str_replace('/','-',$session->get('notasdecredito.hasta'));
                }
                   
                
                if ($fecha_h != '') {
                    $sql .= $conca . " STR_TO_DATE(DATE_FORMAT(fecha, '%d-%m-%Y'), '%d-%m-%Y') <= STR_TO_DATE('" . $fecha_h . "', '%d-%m-%Y') ";
                    $conca = " AND ";
                }
                

                
                // </editor-fold>
            
        //SE FIJA SI ES EXCENTO MIRA SI SELECCIONO UN PUNTO DE VENTA
        //SI NO LO ES FIJA SI TIENE SELECCIONADO ALGUN PUNTO DE VENTA DE LOS QUE ESTA ASIGNADO
        //SINO BUSCA EN LOS PUNTO DE VENTA QUE ESTA ASIGNADO
        if ($user->getExento() == 1) {
            if ($session->get('notasdecredito.puntovta') != '-1') {
            $sql .= $conca . " puntovta = '" . $session->get('notasdecredito.puntovta') . "'  ";
            }
        } else {
            if ($session->get('notasdecredito.puntovta') != '-1') {
                $sql .= $conca . " puntovta = '" . $session->get('notasdecredito.puntovta') . "'  ";
            }
            else{
                $sql .= $conca . " puntovta = '" . $user->getPuntovtaasignado() . "'  ";
            }            
        }

        if (in_array($session->get('notasdecredito.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('notasdecredito.sort') . " " . (($session->get('notasdecredito.order') == 'DESC') ? 'DESC' : 'ASC');
        } else {
            $sql .= " ORDER BY notacredito DESC";
        }

        $results = $database->getRows($sql);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['notacredito'],
                'align' => 'left',
                'default' => 0
            );
            $cell[] = array(
                'value' => @$result['venta'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => date('d/m/Y', strtotime(@$result['fecha'])),
                'align' => 'left',
                'default' => 0
            );


            $cell[] = array(
                'value' => @$result['descpuntovta'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['nombreApellidoVendedor'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['nombreApellidoComprador'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['total'],
                'align' => 'left',
                'default' => 0
            );

            $action = array();

            if ($user->hasPermisos($user->getPERSONA(), 'notasdecredito', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw fa-search',
                    'text' => $language->get('button_consult'),
                    'prop_a' => array('href' => $url->ssl('notasdecredito', 'consulta', array('notacredito' => $result['notacredito'])))
                );

                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw  fa-money',
                    'text' => 'Comprobante',
                    'target' => ' target="_blank" ',
                    'prop_a' => array('href' => $url->ssl('notasdecredito', 'RemitoPDF', array('notacredito' => $result['notacredito'])))
                );
            }

            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('notasdecredito.page'));

        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        if ($session->get('notasdecredito.notacredito')) {
            $view->set('notacredito', $session->get('notasdecredito.notacredito'));
        } else {
            $view->set('notacredito', '');
        }

        $view->set('heading_title', 'NOTAS DE CREDITO');
        $view->set('heading_title2', 'Filtrar');
        $view->set('heading_description', 'Gesti&oacute;n de NOTAS DE CRÉDITO');

        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/NOTICIAS.png');
        $view->set('placeholder_buscar', 'BUSCA POR NRO O VTA');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('notasdecredito.search'));
        $view->set('puntovta', $session->get('notasdecredito.puntovta'));
        $view->set('desde', $session->get('notasdecredito.desde'));
        $view->set('hasta', $session->get('notasdecredito.hasta'));
        $view->set('sort', $session->get('notasdecredito.sort'));
        $view->set('order', $session->get('notasdecredito.order'));
        $view->set('page', $session->get('notasdecredito.page'));

        $view->set('search', $session->get('notasdecredito.search'));

        $view->set('cols', $cols);
        $view->set('rows', $rows);

        if ($user->getExento() == 1) {
            $view->set('puntosdeventa', $database->getRows("SELECT * FROM puntosdeventa ORDER BY descripcion ASC"));
        }
        else{
            $view->set('puntosdeventa', $database->getRows("SELECT * FROM vendedorespuntodeventa vpv LEFT JOIN puntosdeventa pv ON vpv.puntovta = pv.puntovta WHERE vpv.persona = ".$user->getPERSONA()." ORDER BY descripcion ASC"));
        }
        
        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('notasdecredito', 'page'));
        $view->set('list', $url->ssl('notasdecredito'));
        if ($user->hasPermisos($user->getPERSONA(), 'notasdecredito', 'A')) {
            $view->set('insert', $url->ssl('notasdecredito', 'insert'));
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_notasdecredito.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">

        if ($request->has('search', 'post')) {
            $session->set('notasdecredito.search', $request->get('search', 'post'));
        }
        
         if ($request->has('puntovta', 'post')) {
            $session->set('notasdecredito.puntovta', $request->get('puntovta', 'post'));
        }
        
        if ($request->has('desde', 'post')) {
            $session->set('notasdecredito.desde', $request->get('desde', 'post'));
        }
        
        if ($request->has('hasta', 'post')) {
            $session->set('notasdecredito.hasta', $request->get('hasta', 'post'));
        }

        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('notasdecredito.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('notasdecredito.order', (($session->get('notasdecredito.sort') == $request->get('sort', 'post')) && ($session->get('notasdecredito.order') == 'ASC')) ? 'DESC' : 'ASC');
        }

        if ($request->has('sort', 'post')) {
            $session->set('notasdecredito.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('notasdecredito', 'index', array('filtra' => '1')));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $common = & $this->locator->get('common');
        $language = & $this->locator->get('language');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $view = $this->locator->create('template');
        $user = & $this->locator->get('user');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES Y VIEW VARIABLES">

        $view->set('heading_title_cliente', 'NOTAS DE CRÉDITO');
        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/user-8.png');

        $view->set('heading_description', 'Gesti&oacute;n de NOTAS DE CRÉDITO');

        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        $view->set('tab_general', $language->get('tab_general'));

        // <editor-fold defaultstate="collapsed" desc="DETALLE DE NC">

        if (($request->get('notacredito')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM vw_grilla_notasdecredito WHERE notacredito = '?'";
            @$objeto_info = $database->getRow($database->parse($consulta, $request->get('notacredito')));
        }

        if ($request->has('notacredito', 'post')) {
            $view->set('notacredito', $request->get('notacredito', 'post'));
            $objeto_id = $request->get('notacredito', 'post');
        } else {
            if ($request->has('notacredito', 'get')) {
                $objeto_id = @$objeto_info['notacredito'];
            } else {
                $objeto_id = $common->GetaaaaMMddhhmmssmm();
            }
            $view->set('notacredito', $objeto_id);
        }

        $view->set('heading_title', 'NC NRO: ' . $objeto_id);

        $view->set('entry_fecha', 'Fecha:');
        if ($request->has('fecha', 'post')) {
            $view->set('fecha', $request->get('fecha', 'post'));
        } else {
            if ($request->has('fecha', 'get')) {
                $view->set('fecha', date('d/m/Y', strtotime(@$objeto_info['fecha'])));
            } else {
                $view->set('fecha', date('d/m/Y'));
            }
        }

        $view->set('entry_venta', 'Venta Nro:');
        if ($request->has('venta', 'post')) {
            $view->set('venta', $request->get('venta', 'post'));
        } else {
            if ($request->get('action') == 'insert') {
                $view->set('venta', $request->get('venta', 'get'));
            } else {
                $view->set('venta', @$objeto_info['venta']);
            }
        }

        //ESTE LO TRAIGO DE LA VENTA

        $view->set('entry_personacomprador', 'Cliente:');

        if (($request->get('venta')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM vw_grilla_ventas WHERE venta = '?'";
            @$venta_info = $database->getRow($database->parse($consulta, $request->get('venta')));
        }

        if ($request->has('auto_personacomprador', 'post')) {
            $view->set('auto_personacomprador', $request->get('auto_personacomprador', 'post'));
            $view->set('auto_personacomprador_venta', $request->get('auto_personacomprador_venta', 'post'));
        } else {
            if ($request->get('action') == 'insert') {
                $desccomprador = '';
                if ($venta_info['personacomprador'] != '0') {
                    $desccomprador = '(' . $venta_info['personacomprador'] . ')' . $venta_info['nombreApellidoComprador'];
                } else {
                    $desccomprador = $venta_info['nombreApellidoComprador'];
                }
                $view->set('auto_personacomprador', $desccomprador);
                $view->set('auto_personacomprador_venta', $venta_info['personacomprador']);
            } else {
                $desccomprador = '';
                if ($objeto_info['personacomprador'] != '0') {
                    $desccomprador = '(' . $objeto_info['personacomprador'] . ')' . @$objeto_info['nombreApellidoComprador'];
                } else {
                    $desccomprador = @$objeto_info['nombreApellidoComprador'];
                }
                $view->set('auto_personacomprador', $desccomprador);
                $view->set('auto_personacomprador_venta', @$objeto_info['personacomprador']);
            }
        }
        $view->set('script_busca_personacomprador', $url->rawssl('notasdecredito', 'getPersonacomprador'));

        $ptovtaasignado = $database->getRow("SELECT p.puntovtaasignado, pv.descripcion as descpuntovta, pv.listaprecio "
                . "FROM personas p "
                . "LEFT JOIN puntosdeventa pv ON p.puntovtaasignado = pv.puntovta where p.persona = '" . $user->getPERSONA() . "' ");

        $view->set('entry_puntovta', 'Punto de Venta:');
        if ($request->has('puntovta', 'post')) {
            $view->set('puntovta', $request->get('puntovta', 'post'));
            $puntovta = $request->get('puntovta', 'post');
        } else {
            //va el punto de venta que tiene asignado el vendedor

            $view->set('puntovta', @$ptovtaasignado['puntovtaasignado']);
            $puntovta = @$ptovtaasignado['puntovtaasignado'];
        }

        if ($request->has('descpuntovta', 'post')) {
            $view->set('descpuntovta', $request->get('total', 'post'));
        } else {
            if ($ptovtaasignado) {
                $view->set('descpuntovta', $ptovtaasignado['descpuntovta']);
            } else {
                $view->set('descpuntovta', 'SIN PUNTO VENTA');
            }
        }

        $view->set('entry_vendedor', 'Vendedor:');
        if ($request->has('vendedor', 'post')) {
            $view->set('vendedor', $request->get('vendedor', 'post'));
        } else {
            if (@$objeto_info['vendedor']) {
                $view->set('vendedor', @$objeto_info['vendedor']);
            } else {
                $view->set('vendedor', '-1');
            }
        }

        if ($request->get('action', 'get') == 'insert') {
            $view->set('vendedores', $database->getRows("SELECT * FROM personas v LEFT JOIN vendedorespuntodeventa vp ON v.persona = vp.persona WHERE vp.puntovta = '" . $puntovta . "'  ORDER BY v.apellido ASC"));
        } else {
            $view->set('vendedores', $database->getRows("SELECT * FROM personas v LEFT JOIN vendedorespuntodeventa vp ON v.persona = vp.persona WHERE v.persona = '" . @$objeto_info['vendedor'] . "'  ORDER BY v.apellido ASC"));
        }

        $view->set('entry_totalnc', 'TOTAL: $');
        if ($request->has('totalnc', 'post')) {
            $view->set('totalnc', $request->get('totalnc', 'post'));
        } else {
            $view->set('totalnc', 0);
        }

        $view->set('entry_estado', 'Estado: ');
        if ($request->has('estado', 'post')) {
            $view->set('estado', $request->get('estado', 'post'));
        } else {
            if (@$objeto_info['estado']) {
                $view->set('estado', @$objeto_info['estado']);
            } else {
                $view->set('estado', 'INICIANDO');
            }
        }

        $view->set('entry_observacion', 'Observación: ');
        if ($request->has('observacion', 'post')) {
            $view->set('observacion', $request->get('observacion', 'post'));
        } else {
            $view->set('observacion', @$objeto_info['observacion']);
        }

        $view->set('entry_subtotal', 'SUB TOTAL: $');
        if ($request->has('subtotal', 'post')) {
            $view->set('subtotal', $request->get('subtotal', 'post'));
        } else {
            $view->set('subtotal', (@$objeto_info['total']));
        }

        if ($request->has('texttotal', 'post')) {
            $view->set('texttotal', $request->get('texttotal', 'post'));
        } else {
            if (@$objeto_info['total']) {
                $view->set('texttotal', '$ ' . @$objeto_info['total']);
            } else {
                $view->set('texttotal', '$ 0.00');
            }
        }

        $view->set('entry_articulo', 'Producto:');
        if ($request->has('articulo_nc', 'post')) {
            $view->set('articulo_nc', $request->get('articulo_vta', 'post'));
        } else {
            $view->set('articulo_nc', '-1');
        }

        $view->set('auto_articulo', $request->get('auto_articulo', 'post'));

        if ($request->has('auto_articulo_nc', 'post')) {
            $view->set('auto_articulo_nc', $request->get('auto_articulo_nc', 'post'));
        } else {
            $view->set('auto_articulo_nc', '');
        }
        $view->set('script_busca_articulo', $url->rawssl('notasdecredito', 'getArticulos'));


        if ($request->has('codbarra', 'post')) {
            $view->set('codbarra', $request->get('codbarra', 'post'));
        } else {
            $view->set('codbarra', '');
        }

        $view->set('entry_cantidad', 'Cantidad:');
        if ($request->has('cantidad', 'post')) {
            $view->set('cantidad', $request->get('cantidad', 'post'));
        } else {
            $view->set('cantidad', '0');
        }

        $view->set('articulosasignados', $database->getRows("SELECT * FROM vw_grilla_articulosnc where notacredito = '" . $objeto_id . "'"));

        // </editor-fold>
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        $view->set('error_texto_error', @$this->error['texto_error']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $urlAction = $url->ssl('notasdecredito', $request->get('action'), array('notacredito' => $request->get('notacredito')));
        $view->set('action', $urlAction);

        $view->set('accion_form', $request->get('action'));
        $view->set('cancel', $url->ssl('notasdecredito'));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACION DE CAJA">
        if ($request->get('action') != 'consulta') {
            $sql = "SELECT count(*) AS total FROM cajasdiaria WHERE estadocaja = 'ABIERTA' AND puntovta ='" . $puntovta . "' ";
            $caja_valida = $database->getRow($sql);

            $view->set('validaciones_caja', '');
            if ($caja_valida['total'] == 0) {
                $view->set('validaciones_caja', 'NO PUEDE REALIZAR LA NC YA QUE NO POSEE CAJA ABIERTA.');
            } else {
                $hoy = date("Y-m-d");
                $sql = "SELECT count(*) AS total FROM cajasdiaria WHERE estadocaja = 'ABIERTA' AND puntovta ='" . $puntovta . "' AND STR_TO_DATE(fechaapertura, '%Y-%m-%d') = STR_TO_DATE('" . $hoy . "', '%Y-%m-%d') ";
                $caja_valida = $database->getRow($sql);
                if ($caja_valida['total'] == 0) {
                    $view->set('validaciones_caja', 'NO PUEDE REALIZAR LA NC YA QUE LA CAJA QUE SE ENCUENTRA ABIERTA NO ES DE HOY.');
                }
            }
        }

        // </editor-fold>

        return $view->fetch('content/notacredito.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $common = & $this->locator->get('common');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $articulos = $request->get('articulosnc', 'post', array());
        if (count($articulos) == 0) {
            $errores .= 'Debe ingresar al menos un producto. <br>';
        }

        foreach ($articulos as $art) {
            $sql = "SELECT SUM(cantidad) as total FROM ventadetalles WHERE articulo ='" . $art['articulo'] . "' and venta = '" . $request->get('venta', 'post') . "'";
            $totalArticuloVta = $database->getRow($sql);

            $sql = "SELECT SUM(ncd.cantidad) as total FROM notacreditodetalles ncd left join notasdecredito nc ON ncd.notacredito = nc.notacredito WHERE ncd.articulo ='" . $art['articulo'] . "' and nc.venta = '" . $request->get('venta', 'post') . "'";
            $totalArticuloYaDevuelto = $database->getRow($sql);

            $total = $totalArticuloYaDevuelto['total'] + $totalArticuloVta['total'];
            if ($total < $art['cantidad']) {
                $errores .= 'La cantidad del artículo ' . $art['descmodelo'] . ' . que se desea devolver supera a la vendida. Puede también ya haber devuelto una cantidad del producto relacionada con esta venta.<br>';
            }
        }

        if ($request->get('vendedor', 'post') == '-1') {
            $errores = 'Debe seleccionar un vendedor. <br>';
        }

        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $common = & $this->locator->get('common');
        // </editor-fold>

        $template->set('title', 'NC');

        if (($request->isPost()) && ($this->validateForm())) {

            $id_NC = $request->get('notacredito', 'post');

            //CONTROLA QUE NO SE ENCUENTRE INGRESADA LA NOTA DE CREDITO
            $consulta_info = $database->getRow("SELECT COUNT(notacredito) as total FROM notasdecredito WHERE notacredito = '" . $id_NC . "' ");
            if (@$consulta_info['total'] != 0) {

                $ur = $url->ssl('notasdecredito');
                die($ur);
                return;
            }

            // <editor-fold defaultstate="collapsed" desc="ENCABEZADO NC">
            // <editor-fold defaultstate="collapsed" desc="FECHA">
            $fecha_d = '';
            if ($request->get('fecha', 'post') != '') {
                $dated = explode('/', $request->get('fecha', 'post'));
                $fecha_d = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
            } else {
                $fecha_d = NULL;
            }

            // </editor-fold>

            $sql = "INSERT INTO notasdecredito SET notacredito ='?', venta='?',fecha='?',personacomprador='?',puntovta='?',totalnc='?',observacion='?',estado='OK',vendedor='?',au_usuario='?',au_accion='A',au_fecha_hora=NOW() ";
            $sql = $database->parse($sql, $id_NC, $request->get('venta', 'post'), $fecha_d, $request->get('auto_personacomprador_venta', 'post'), $request->get('puntovtaid', 'post'), $request->get('total', 'post'), $request->get('observacion', 'post'), $request->get('vendedor', 'post'), $user->getPERSONA());
            $database->query($sql);
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="DETALLE DE NC">
            $articulos = $request->get('articulosnc', 'post', array());

            foreach ($articulos as $art) {
                $sql = "INSERT INTO notacreditodetalles SET notacredito='?', articulo='?', cantidad='?', codbarra='?',  precionc='?' ";
                $sql = $database->parse($sql, $id_NC, $art['articulo'], $art['cantidad'], $art['codbarra'], $art['precionc']);
                $database->query($sql);

                if ($art['codbarra']) {
                    $sql = "UPDATE articulosbarra SET  estadoarticulo='DI',  au_usuario='?',au_accion='M',au_fecha_hora=NOW() "
                            . "WHERE articulo='?' AND codbarra='?'";
                    $sql = $database->parse($sql, $user->getPERSONA(), $art['articulo'], $art['codbarra']);
                    $database->query($sql);
                }

                $sql = "UPDATE articulos SET cantidadstock= cantidadstock + '?',au_accion='V',au_fecha_hora=NOW() WHERE articulo='?'";
                $sql = $database->parse($sql, $art['cantidad'], $art['articulo']);
                $database->query($sql);


                $sql = "UPDATE articulospuntovta SET cantidad = cantidad + '?' WHERE articulo='?' AND puntovta = '?' ";
                $sql = $database->parse($sql, $art['cantidad'], $art['articulo'], $request->get('puntovtaid', 'post'));
                $database->query($sql);
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="CTA CTE">
            $sql = "SELECT * FROM puntosdeventa  WHERE puntovta = '?' ";
            $consulta = $database->parse($sql, $request->get('puntovtaid', 'post'));
            $obj_puntovta = $database->getRow($consulta);

            $importe_ctate = $request->get('totalnc', 'post');

            //SE GENERA LA CUENTA CORRIENTE
            $sql = "INSERT INTO ctascte SET ctacte='?', fecha=CURDATE(), puntovta='?',puntovtapropio='?', vendedor='?', rubro='NC', movimiento='C', importectacte='?', importepago=0, au_usuario='?', au_accion='A',au_fecha_hora=NOW() ";
            $sql = $database->parse($sql, $id_NC, $request->get('puntovtaid', 'post'), $obj_puntovta['puntovtapropio'], $user->getPERSONA(), $importe_ctate, $user->getPERSONA());
            $database->query($sql);
            // </editor-fold>

            $cache->delete('notasdecredito');

            $session->set('message', 'Se realizó la NC con éxito: ' . $id_NC);
            $ur = $url->ssl('notasdecredito');
            die($ur);
            $response->redirect($url->ssl('notasdecredito'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        $template->set('title', 'NC');

        if (($request->isPost())) {

            $response->redirect($url->ssl('notasdecredito'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $config = & $this->locator->get('config');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        $errores = '';

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function exportar() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $session = & $this->locator->get('session');
        // </editor-fold>
    }

    function RemitoPDF() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        // </editor-fold>

        set_time_limit(0);

        if ($request->has('notacredito', 'post')) {
            $notacredito_id = $request->get('notacredito', 'post');
        } else {
            if ($request->has('notacredito', 'get')) {
                $notacredito_id = $request->get('notacredito', 'get');
            }
        }

        $consulta = "SELECT DISTINCT * FROM vw_grilla_notasdecredito WHERE notacredito = '?'";
        $venta_info = $database->getRow($database->parse($consulta, $notacredito_id));

        $consult = "SELECT * FROM vw_grilla_articulosnc WHERE notacredito='" . $notacredito_id . "' ORDER BY  articulo ASC";
        $detalleventa = $database->getRows($consult);

        // <editor-fold defaultstate="collapsed" desc="config PDF">
        // Include the main TCPDF library (search for installation path).
        // Include the main TCPDF library (search for installation path).
        define('FPDF_FONTPATH', 'font/');
        require('library/pdf/tcpdf/tcpdf.php');
        require_once('library/pdf/tcpdf/tcpdf_include.php');

        $con = PDF_PAGE_ORIENTATION;
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF8 sin BOM', false);


        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('STC');
        $pdf->SetTitle('');
        $pdf->SetSubject(' ');
        $pdf->SetKeywords(' ');

        // set default header data
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        //LA IMAGEN DEBE ESTAR EN template\default\image
        // $pdf->SetHeaderData('cabecera PDF-02.jpg', 173, '', '');
        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font - conjunto predeterminado fuentemonoespaciada
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetMargins(20, 10, 20);
        //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetHeaderMargin(5);
        $pdf->SetFooterMargin(15);

        // set auto page breaks - establecer saltos de página automático
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // establecemos la medida del interlineado
        //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        //$pdf->SetDefaultMonospacedFont(30);
        // ---------------------------------------------------------
        // set font
        $pdf->SetFont('helvetica', '', 9);


        //$pdf->Image('images/sistema.png', 50, 50, 100, '', '', 'http://www.tcpdf.org', '', false, 300);
// </editor-fold>  
        // add a page
        $pdf->AddPage();

        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">
        // $html = '<div style="text-align:center; " ><h1><b>UNIÓN DE RUGBY ARGENTINO</b></h1><br></div>';
        //$pdf->writeHTML($html, true, false, true, false, '');
        //$html = '<div style="text-align:center; font-size: 13pt;" ><b>CONSTANCIA DE ORDEN DE REPARACIÓN</b><br></div>';
        //$pdf->writeHTML($html, true, false, true, false, '');
//            $html = '<div style="text-align:center; font-size: 11pt;" ><br><b>ORDEN NRO: '.$reparacion_id.' </b><br></div>';
//            $pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO REMITO">
        // define barcode style
        $style = array(
            'position' => '',
            'align' => 'C',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

        // PRINT VARIOUS 1D BARCODES
        // CODE 39 EXTENDED + CHECKSUM
        //$pdf->Cell(0, 240, 'NÚMERO DE ORDEN', 0, 1);
        //public function write1DBarcode($code, $type, $x='', $y='', $w='', $h='', $xres='', $style='', $align='')
        //$pdf->write1DBarcode($reparacion_id, 'S25', '', '242', '', 18, 0.4, $style, 'N');
        //$pdf->Cell(110, 0, 'NÚMERO DE ORDEN', 0, 1);
        // Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)
        $pdf->Image('template/default/image/Remito.jpg', 95, 10, 20, 30, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
        $pdf->Image('template/default/image/Insight.jpg', 25, 10, 45, 20, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
        //$pdf->Text(55, 260, 'NÚMERO DE ORDEN');

        $fecha = date('d/m/Y', strtotime($venta_info['fecha']));
        $ptovta = $venta_info['descpuntovta'];
        $vendedorNombre = $venta_info['nombreApellidoVendedor'];

        $html = <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    <td width="100" style="text-align:left;"><b></b></td>
                    <td width="300" style="text-align:right;"><b></b></td>
                    <td width="300" style="text-align:left;" ><b></b></td>
                    
               </tr> 
                <tr >
                    <td width="100" style="text-align:left;" ><b></b></td>
                    <td width="300" style="text-align:right;"><b></b></td>
                    <td width="300" style="text-align:left;" ><b>NC NRO: $notacredito_id</b></td>
               </tr> 
                <tr >
                    <td width="100" style="text-align:left;" ><b></b></td>
                    <td width="300" style="text-align:right;"><b></b></td>
                    <td width="300" style="text-align:left;" ><b>FECHA: $fecha</b></td>
                    
               </tr>
            </table>
EOF;


        $pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="CLIENTE EQUIPO">
        //$html = '<div style="text-align:center; font-size: 11pt;" ><b>Datos del Cliente</b><hr /></div>';
        $html = <<<EOF
                 <br> <br><br><br><hr> 
EOF;
        $pdf->writeHTML($html, true, false, true, false, '');
        // <editor-fold defaultstate="collapsed" desc="STYLE">
        $html = <<<EOF
                <!-- EXAMPLE OF CSS STYLE -->
                <style>
                    /*
                    table.first {
                        color: black;
                        font-family: helvetica;
                        font-size: 8pt;
                        background-color: white;
                    }
                    td {
                        border: 0px solid black;
                        background-color: white;
                    }
                    */
                </style>
EOF;
        //$pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>

        $consulta = "SELECT DISTINCT * FROM vw_grilla_personas WHERE persona = '?'";
        $comprador_info = $database->getRow($database->parse($consulta, $venta_info['personacomprador']));

        $ducumento = $venta_info['personacomprador'];
        $apellidonombrecliente = $venta_info['nombreApellidoComprador'];
        $telefonocliente = $comprador_info['celular'];
        $mailcliente = $comprador_info['mail'];
        $venta_id = $venta_info['venta'];
//        $nroserie = $reparacion_info['nroserie'];
//        $tipoproducto = $reparacion_info['desctipoproducto'];
//        $modelo = $reparacion_info['descmarca'] . ' - ' . $reparacion_info['descmodelo'];
//        $engarantia = No;
//        if ($reparacion_info['engarantia'] == 1) {
//            $engarantia = Si;
//        }

        $html .= <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    <td width="300" style="text-align:left;"><b>DOCUMENTO: $ducumento</b></td>
                    <td width="50" style="text-align:left;" ></td>
                    <td width="300" style="text-align:left;" ><b>PTO VTA: $ptovta</b></td>
               </tr> 
                <tr >
                    <td width="300" style="text-align:left;"><b>APELLIDO Y NOMBRE: $apellidonombrecliente</b></td>
                    <td width="50" style="text-align:left;" ></td>
                    <td width="300" style="text-align:left;" ><b>VENDEDOR: $vendedorNombre</b></td>
               </tr>
                <tr >
                    <td width="300" style="text-align:left;"><b>TELÉFONO: $telefonocliente</b></td>
                    <td width="50" style="text-align:left;" ></td>
                    <td width="300" style="text-align:left;" ><b>VTA REL: $venta_id</b></td>
               </tr>
                <tr >
                    <td width="300" style="text-align:left;"><b>MAIL: $mailcliente</b></td>
                    <td width="50" style="text-align:left;" ></td>
                    <td width="300" style="text-align:left;" ><b> </b></td>
               </tr>
                
            </table>
                <br><hr>
EOF;


        $pdf->writeHTML($html, true, false, true, false, '');

        // </editor-fold>
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="HARD SOFT">
        $html = '<div style="text-align:center; font-size: 11pt;" ><b>DETALLE</b><hr /></div>';
        $html .= <<<EOF
                   <br>
EOF;
        $pdf->writeHTML($html, true, false, true, false, '');
        // <editor-fold defaultstate="collapsed" desc="STYLE">
        $html = <<<EOF
                <!-- EXAMPLE OF CSS STYLE -->
                <style>
                    /*
                    table.first {
                        color: black;
                        font-family: helvetica;
                        font-size: 8pt;
                        background-color: white;
                    }
                    td {
                        border: 0px solid black;
                        background-color: white;
                    }
                    */
                </style>
EOF;
        //$pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>
        //lista de repuestos reparacion

        $html .= <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    <td width="80" style="text-align:left;"><b>Artículo</b></td>
                    <td width="240" style="text-align:left;"><b>Descripción</b></td>
                    <td width="75" style="text-align:center;"><b>Cantidad</b></td>
                    <td width="125" style="text-align:right;"><b>Precio U.</b></td>
                    <td width="80" style="text-align:right;"><b>Subtotal</b></td>
               </tr> 
                
            </table>
                <hr />
EOF;

        foreach ($detalleventa as $detalle) {
            $articulo = $detalle['codbarrainterno'];
            $descripcion = $detalle['descmodelo'];
            $cantidad = $detalle['cantidad'];
            $precionc = $detalle['precionc'];
            $subtotal = ($precionc * $cantidad);

            $html .= <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    <td width="80" style="text-align:left;">$articulo</td>
                    <td width="255" style="text-align:left;">$descripcion</td>
                    <td width="50" style="text-align:center;">$cantidad</td>
                    <td width="130" style="text-align:right;">$ $precionc</td>
                    <td width="80" style="text-align:right;">$ $subtotal</td>
               </tr> 
                
            </table>
EOF;
        }

        $pdf->writeHTML($html, true, false, true, false, '');

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SELLO">
        // Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)
        //  $pdf->Image('template/default/image/sello-entregado-2.png', 130, 180, 60, 50, 'png', '', '', true, 150, '', false, false, 0, false, false, false);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PIE">
        //$html = '<div style="text-align:center; font-size: 11pt;" ><b>Datos del Cliente</b><hr /></div>';
        $html = <<<EOF
                 <br> <br><br><br><hr> 
EOF;
        $pdf->writeHTML($html, true, false, true, false, '');
        // <editor-fold defaultstate="collapsed" desc="STYLE">
        $html = <<<EOF
                <!-- EXAMPLE OF CSS STYLE -->
                <style>
                    /*
                    table.first {
                        color: black;
                        font-family: helvetica;
                        font-size: 8pt;
                        background-color: white;
                    }
                    td {
                        border: 0px solid black;
                        background-color: white;
                    }
                    */
                </style>
EOF;
        //$pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>

        $total = $venta_info['total'];
        $html = <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    
                    <td width="600" style="text-align:center;" ><b>TOTAL $ $total</b></td>
               </tr> 
                
                
            </table>
                <br><hr>
EOF;


        $pdf->writeHTML($html, true, false, true, false, '');

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PIE">
        $pdf->Text(40, 255, '_____________________________________');
        $pdf->Text(45, 260, 'FIRMA Y ACLARACION DEL CLIENTE');
        // </editor-fold>
        $pdf->setPrintHeader(false);
        //ESTO HACE QUE ANDE EN EL SERVER DE LA DNNA
        ob_end_clean();
        //Close and output PDF document
        //D obliga la descarga
        //I abre en una ventana nueva
        $nombrePDF = 'NCNro' . $notacredito_id . '.pdf';
        $pdf->Output($nombrePDF, 'I');

        //$cache->delete('checkins');
        //die('ok');
        //============================================================+
        // END OF FILE
        //============================================================+
    }

    function getPersonacomprador() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>		

        $miDescripcion = $request->get('term', 'get');

        $sql = "SELECT distinct p.persona AS id, IF(p.persona='0',p.apellidonombre,CONCAT('(',p.persona,') ',p.apellidonombre))  as value "
                . "FROM vw_grilla_personas p "
                . "WHERE (p.persona LIKE '?' OR p.nombre LIKE '?' OR p.apellido LIKE '?') "
                . "ORDER BY p.apellidonombre ASC "
                . "LIMIT 10";
        $consulta = $database->parse($sql, '%' . $miDescripcion . '%', '%' . $miDescripcion . '%', '%' . $miDescripcion . '%');

        $codigo = $database->getRows($consulta);

        echo json_encode($codigo);
    }

    function getArticulos() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>

        $miDescripcion = $request->get('term', 'get');
        $venta = $request->get('venta', 'get');

        $sql = "SELECT DISTINCT if(a.codbarra!='',concat(a.articulo, '_',a.codbarra), a.articulo) as id, "
                . " concat(a.marca,' - ',a.descmodelo, ' ' ,if(a.codbarra is not null ,concat('Cod: ',a.codbarra), concat('Cod: ',a.codbarrainterno))) as value  "
                . "FROM vw_lista_ventadetallada_xls dv "
                . "LEFT JOIN vw_grilla_articulostodos a ON dv.articulo = a.articulo "
                . "WHERE dv.venta = '" . $venta . "'"
                . "AND (a.articulo LIKE '?' OR a.codbarrainterno LIKE '?' OR a.desctipoproducto LIKE '?' OR a.descmarca LIKE '?' OR a.descmodelo LIKE '?' OR a.codbarra like '?' ) "
                . "LIMIT 10 ";
        $consulta = $database->parse($sql, '%' . $miDescripcion . '%', '%' . $miDescripcion . '%', '%' . $miDescripcion . '%', '%' . $miDescripcion . '%', '%' . $miDescripcion . '%', '%' . $miDescripcion . '%');
        $codigo = $database->getRows($consulta);

        $varia = utf8_decode(json_encode($codigo));

        echo $varia;
    }

    function getArticulo() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>		

        $articulo = $request->get('articuloid', 'get');
        $codbarra = $request->get('codbarra', 'get');
        $puntovta = $request->get('puntovta', 'get');
        $venta = $request->get('venta', 'get');

        if ($codbarra != '' && $codbarra != 'undefined' && $codbarra != null) {
            $sql = "SELECT a.articulo, "
                    . "concat(a.marca,' - ',a.descmodelo, ' (cod bar:',a.codbarra,')') AS descripcion, "
                    . "'1' AS concoddebarra, "
                    . "a.codbarrainterno, "
                    . "a.codbarra, "
                    . "apv.cantidad AS cantidad, "
                    . "apv.PRECIO_VTA AS precionc "
                    . "FROM vw_lista_ventadetallada_xls apv "
                    . "LEFT JOIN vw_grilla_articulostodos a ON apv.articulo = a.articulo "
                    . "WHERE a.codbarra = '?' "
                    . "and apv.venta = '?' ";
            $consulta = $database->parse($sql, $codbarra, $venta);
            $codigo = $database->getRow($consulta);
        } else {
            $sql = "SELECT a.articulo, "
                    . "concat(a.marca,' - ',a.descmodelo, ' (Cod: ',a.codbarrainterno,')') AS descripcion, "
                    . "a.concoddebarra, "
                    . "a.codbarrainterno, '' AS codbarra,"
                    . "apv.cantidad AS cantidad, "
                    . "apv.PRECIO_VTA AS precionc "
                    . "FROM vw_lista_ventadetallada_xls apv "
                    . "LEFT JOIN vw_grilla_articulostodos a ON apv.articulo = a.articulo "
                    . "WHERE apv.articulo = '?' "
                    . "and apv.venta = '?' ";
            $consulta = $database->parse($sql, $articulo, $venta);
            $codigo = $database->getRow($consulta);

            //tengo que restarle la cantidad que ya devolvio de ese articulo. todo: MAURO
            $sql = "SELECT SUM(ncd.cantidad) AS cantidad "
                    . "FROM notasdecredito nc "
                    . "LEFT JOIN notacreditodetalles ncd ON nc.notacredito = ncd.notacredito "
                    . "WHERE ncd.articulo = '?' "
                    . "and nc.venta = '?' ";
            $consulta = $database->parse($sql, $articulo, $venta);
            $devolvio = $database->getRow($consulta);
            
            $codigo['cantidad'] = $codigo['cantidad'] - $devolvio['cantidad'];
        }

        echo json_encode($codigo);
    }

    function ValidaAgregarLinea() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>		

        $miTerm = $request->get('term', 'get');

        $sql = "SELECT a.articulo , concat(a.marca,' - ',a.descmodelo) AS descripcion, a.concoddebarra, a.codbarrainterno, a.precioultcompra "
                . "FROM vw_grilla_articulos a "
                . "WHERE a.articulo = '?'";
        $consulta = $database->parse($sql, $miTerm);
        $codigo = $database->getRow($consulta);

        echo json_encode($codigo);
    }

    function AgregarArticulo() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $language = & $this->locator->get('language');
        $response = & $this->locator->get('response');
        $request = & $this->locator->get('request');
        $view = $this->locator->create('template');
        // </editor-fold>

        $view->set('button_add', $language->get('button_add'));
        $view->set('button_remove', $language->get('button_remove'));

        $articulo = $request->get('articulo');
        $cantidad = $request->get('cantidad');
        $codbarra = $request->get('codbarra');
        $codbarrainterno = $request->get('codbarrainterno');
        $precionc = $request->get('precionc');

        $sql = "SELECT a.articulo , concat(a.marca,' - ',a.descmodelo) AS descripcion, precioultcompra "
                . "FROM vw_grilla_articulos a "
                . "WHERE a.articulo = '?' ";
        $consulta = $database->parse($sql, $articulo);
        $art = $database->getRow($consulta);

        $total_fila = $cantidad * $precionc;

        $descmodelo = $art['descripcion'];

        $view->set('cargo_venta', venta);
        $view->set('cargo_articulo', $articulo);
        $view->set('cargo_descmodelo', $descmodelo);
        $view->set('cargo_cantidad', $cantidad);
        $view->set('cargo_codbarra', $codbarra);
        $view->set('cargo_codbarrainterno', $codbarrainterno);
        $view->set('cargo_precionc', $precionc);

        $view->set('cargo_id', $request->get('cantReg'));

        $view->set('nombre_fila', 'ncarticulo_' . $request->get('cantReg'));

        $view->set('total_fila', $total_fila);

        $response->set($view->fetch('content/notacredito_articulo.tpl'));
    }

}

?>
