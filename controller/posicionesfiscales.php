<?php

class ControllerPosicionesfiscales extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'Posiciones fiscales');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
        $request = & $this->locator->get('request');
        // </editor-fold>
         // <editor-fold defaultstate="collapsed" desc="LIMPIA FILTRO Y VARIBLES DE SESSION">

        if ($request->get('filtra') == NULL) {

            $session->set('posicionesfiscales.search', '');
            $session->set('posicionesfiscales.sort', '');
            $session->set('posicionesfiscales.order', '');
            $session->set('posicionesfiscales.page', '');

            $view->set('search', '');
            $view->set('posicionesfiscales.search', '');
                        
            $cache->delete('posicionesfiscales');
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">
        $cols = array();
       
        
        $cols[] = array(
            'name' => 'Punto Vta',
            'sort' => 'descpuntovta',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Tipo factura',
            'sort' => 'desctipocomprobante',
            'align' => 'left'
        );


        $cols[] = array(
            'name' => 'Prefijo',
            'sort' => 'prefijo',
            'align' => 'left'
        );
        
        $cols[] = array(
            'name' => 'WS AFIP',
            'sort' => 'wsafip',
            'align' => 'left'
        );

        
        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'descpuntovta',
            'desctipocomprobante',
            'prefijo',
            'wsafip'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        if (!$session->get('posicionesfiscales.search')) {
            $sql = "SELECT * FROM vw_list_posicionesfiscales  ";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT * FROM vw_list_posicionesfiscales WHERE descpuntovta LIKE '?' OR desctipocomprobante LIKE '?'  ";
            $conca = " AND ";
        }

        if (in_array($session->get('posicionesfiscales.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('posicionesfiscales.sort') . " " . (($session->get('posicionesfiscales.order') == 'DESC') ? 'DESC' : 'ASC');
        } else {
            $sql .= " ORDER BY descpuntovta ASC";
        }

        $consult = $database->parse($sql, '%' . $session->get('posicionesfiscales.search') . '%', '%' . $session->get('posicionesfiscales.search') . '%');
        $results = $database->getRows($consult);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['descpuntovta'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['desctipocomprobante'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['prefijo'],
                'align' => 'left',
                'default' => 0
            );
            
            $wsafip = "No";
            if (@$result['wsafip'] == '1')
                $wsafip = 'Si';
            $cell[] = array(
                'value' => $wsafip,
                'align' => 'left',
                'default' => 0
            );


            
            $action = array();

            if ($user->hasPermisos($user->getPERSONA(), 'posicionesfiscales', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa fa-fw fa-pencil',
                    'text' => $language->get('button_update'),
                    'prop_a' => array('href' => $url->ssl('posicionesfiscales', 'update', array('posicionfiscal' => $result['posicionfiscal'])))
                );
            }
      
           if ($user->hasPermisos($user->getPERSONA(),'posicionesfiscales','B')) {
                $action[] = array(
                        'icon' => 'img/iconos-11.png',
                        'text' => $language->get('button_delete'),
                        'class' => 'fa fa-fw fa-trash-o',
                        'prop_a' => array('href' => $url->ssl('posicionesfiscales', 'delete', array('posicionfiscal' => $result['posicionfiscal'])))
                );
            }
                        
                        
            if ($user->hasPermisos($user->getPERSONA(), 'posicionesfiscales', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw fa-search',
                    'text' => $language->get('button_consult'),
                    'prop_a' => array('href' => $url->ssl('posicionesfiscales', 'consulta', array('posicionfiscal' => $result['posicionfiscal'])))
                );
            }

            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('posicionesfiscales.page'));

        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>


        $view->set('heading_title', 'POSICIONES FISCALES');
        $view->set('heading_title2', 'Filtrar');
        $view->set('heading_description', 'Gesti&oacute;n de Posiciones fiscales');

        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/NOTICIAS.png');
        $view->set('placeholder_buscar', 'BUSCA PUNTO DE VENTA O TIPO DE FACTURA');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('posicionesfiscales.search'));
        $view->set('sort', $session->get('posicionesfiscales.sort'));
        $view->set('order', $session->get('posicionesfiscales.order'));
        $view->set('page', $session->get('posicionesfiscales.page'));

        $view->set('cols', $cols);
        $view->set('rows', $rows);
        
        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('posicionesfiscales', 'page'));
        $view->set('list', $url->ssl('posicionesfiscales'));
        if ($user->hasPermisos($user->getPERSONA(), 'posicionesfiscales', 'A')) {
            $view->set('insert', $url->ssl('posicionesfiscales', 'insert'));
        }
        //$view->set('listaMail', $url->ssl('posicionesfiscales', 'listaMail'));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_posicionesfiscales.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">

        if ($request->has('search', 'post')) {
            $session->set('posicionesfiscales.search', $request->get('search', 'post'));
        }

        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('posicionesfiscales.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('posicionesfiscales.order', (($session->get('posicionesfiscales.sort') == $request->get('sort', 'post')) && ($session->get('posicionesfiscales.order') == 'ASC')) ? 'DESC' : 'ASC');
        }

        if ($request->has('sort', 'post')) {
            $session->set('posicionesfiscales.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('posicionesfiscales', 'index', array('filtra' => '1')));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $language = & $this->locator->get('language');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $view = $this->locator->create('template');
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('heading_title', 'Posición fiscal');
        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/user-8.png');

        $view->set('heading_description', 'Gesti&oacute;n de posiciones fiscales');

        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        $view->set('tab_general', $language->get('tab_general'));

        if (($request->get('posicionfiscal')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM vw_list_posicionesfiscales WHERE posicionfiscal = '?'";
            $objeto_info = $database->getRow($database->parse($consulta, $request->get('posicionfiscal')));
        }

        if ($request->has('posicionfiscal', 'post')) {
            $view->set('posicionfiscal', $request->get('posicionfiscal', 'post'));
            $objeto_id = $request->get('posicionfiscal', 'post');
        } else {
            $view->set('posicionfiscal', @$objeto_info['posicionfiscal']);
            $objeto_id = @$objeto_info['posicionfiscal'];
        }

        $view->set('entry_puntovta', 'Punto de venta:');
        if ($request->has('puntovta', 'post')) {
            $view->set('puntovta', $request->get('puntovta', 'post'));
        } else {
            $view->set('puntovta', @$objeto_info['puntovta']);
        }
        $view->set('puntosdeventa', $database->getRows("SELECT * FROM puntosdeventa ORDER BY descripcion  ASC"));
        
        $view->set('entry_tipocomprobante', 'Tipo de factura:');
        if ($request->has('tipocomprobante', 'post')) {
            $view->set('tipocomprobante', $request->get('tipocomprobante', 'post'));
        } else {
            $view->set('tipocomprobante', @$objeto_info['tipocomprobante']);
        }
        $view->set('tiposcomprobante', $database->getRows("SELECT * FROM tiposcomprobante ORDER BY descripcion  ASC"));
        
        $view->set('entry_prefijo', 'Prefijo:');
        if ($request->has('prefijo', 'post')) {
            $view->set('prefijo', $request->get('prefijo', 'post'));
        } else {
            $view->set('prefijo', @$objeto_info['prefijo']);
        }
        
        $view->set('entry_wsafip', 'WS AFIP:');
        if ($request->has('wsafip', 'post')) {
            $view->set('wsafip', $request->get('wsafip', 'post'));
        } else {
            $view->set('wsafip', @$objeto_info['wsafip']);
        }

        
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');
        $view->set('error_financiacion', @$this->error['posicionfiscal']);

        $view->set('error_texto_error', @$this->error['texto_error']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $urlAction = $url->ssl('posicionesfiscales', $request->get('action'), array('posicionfiscal' => $request->get('posicionfiscal')));
        $view->set('action', $urlAction);
        $view->set('accion_form', $request->get('action'));
        $view->set('cancel', $url->ssl('posicionesfiscales'));

        // </editor-fold>

        return $view->fetch('content/posicionfiscal.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $common = & $this->locator->get('common');
        $upload = & $this->locator->get('upload');
        //$neofactura = & $this->locator->get('neofactura');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        
        if ($request->get('puntovta', 'post') == '-1') {
            $errores .= 'Debe seleccionar un punto de venta. <br>';
        }
        if ($request->get('tipocomprobante', 'post') == '-1') {
            $errores .= 'Debe seleccionar un tipo de factura. <br>';
        }
        if ((strlen($request->get('prefijo', 'post')) == 0)) {
            $errores .= 'Debe ingresar un prefijo. <br>';
        }

        if ($request->get('action') == 'update') {
            $consulta_info = $database->getRow("SELECT COUNT(*) as total FROM posicionesfiscales WHERE puntovta = '" . $request->get('puntovta', 'post') . "' and tipocomprobante = '".$request->get('tipocomprobante', 'post')."' and posicionfiscal != '".$request->get('posicionfiscal', 'post')."' ");
            if (@$consulta_info['total'] > 0) {
                $errores .= 'El punto de venta y el tipo de comprobante ya tienen asignado una posición fiscal. <br>';
            }
        }
        
        if ($request->get('action') == 'insert') {
            $consulta_info = $database->getRow("SELECT COUNT(*) as total FROM posicionesfiscales WHERE puntovta = '" . $request->get('puntovta', 'post') . "' and tipocomprobante = '".$request->get('tipocomprobante', 'post')."' ");
            if (@$consulta_info['total'] > 0) {
                $errores .= 'El punto de venta y el tipo de comprobante ya tienen asignado una posición fiscal. <br>';
            }
        }
        
        //HACER: PRODRIAMOS VALIDAR CONTRA LA API DE LA AFIP QUE EXISTA EL PUNTO DE VENTA
        //VALIDAR EL PUNTO DE VENTA SI EXISTE CONTRA LA API DE LA AFIP
            
        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $common = & $this->locator->get('common');
        
        // </editor-fold>

        $template->set('title', 'Posiciones fiscales');

        if (($request->isPost()) && ($this->validateForm())) {
            $id = strtoupper($request->get('persona', 'post'));

            $sql = "INSERT INTO posicionesfiscales SET puntovta = '?',tipocomprobante ='?',prefijo ='?', wsafip ='?',  au_usuario='?',au_accion='A',au_fecha_hora=NOW()";
            $sql = $database->parse($sql, $request->get('puntovta', 'post'), $request->get('tipocomprobante', 'post'),$request->get('prefijo', 'post'), $request->get('wsafip', 'post'), $user->getPERSONA());
            $database->query($sql);
            $id = $database->getLastId();

            $cache->delete('posicionesfiscales');
            $session->set('message', 'Se agreg&oacute; la posición fiscal: ' . $id);

            $response->redirect($url->ssl('posicionesfiscales'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $upload = & $this->locator->get('upload');
        // </editor-fold>

        $template->set('title', 'Posiciones fiscales');

        if (($request->isPost()) && ($this->validateForm())) {

            $id = $request->get('posicionfiscal', 'post');

            $sql = "UPDATE posicionesfiscales SET puntovta = '?',tipocomprobante ='?',prefijo ='?', wsafip ='?',  au_usuario='?',au_accion='M',au_fecha_hora=NOW() WHERE posicionfiscal = '?'";
            $sql = $database->parse($sql, $request->get('puntovta', 'post'), $request->get('tipocomprobante', 'post'),$request->get('prefijo', 'post'), $request->get('wsafip', 'post'), $user->getPERSONA(),$id);
            $database->query($sql);
            
            $cache->delete('posicionesfiscales');
            $session->set('message', 'Se actualiz&oacute; la posición fiscal: ' . $id);

            $response->redirect($url->ssl('posicionesfiscales'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        $template->set('title', 'Posición fiscal');

        if (($request->isPost())) {

            $response->redirect($url->ssl('posicionesfiscales'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        if (($request->get('posicionfiscal')) && ($this->validateDelete())) {

            $id = $request->get('posicionfiscal', 'get');

            
            $sql = "DELETE FROM posicionesfiscales WHERE posicionfiscal = '?'";
            $database->query($database->parse($sql, $id));
           

            $cache->delete('posicionesfiscales');

            $session->set('message', 'Se ha eliminado la posición fiscal: ' . $id);

            $response->redirect($url->ssl('posicionesfiscales'));
        }

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';

        $errores = 'no esta programada las validaciones para el delete';
//        $consulta_info = $database->getRow("SELECT COUNT(*) as total FROM ventaformasdepago WHERE financiacion = '" . $request->get('posicionfiscal') . "'");
//        if (@$consulta_info['total'] != 0) {
//            $errores .= 'No es posible eliminar la financiación, ya fue asignado como mínimo a una venta. <br>';
//        }
        
        if ($errores != '') {
            $this->error['message'] = $errores;
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
   
    function exportar() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $session = & $this->locator->get('session');
        // </editor-fold>

        set_time_limit(0);

        $filtro = 'Filtro:';

        $htmlTable = "<table border=0 width=180> 
			<tr><td colspan=6 align=center style=bold size=14>" . utf8_decode("xxxxxxxxx xxxxxxxxx xxxxxxx") . "</td></tr>
			<tr><td colspan=6 align=center style=bold size=12>SISTEMA DE xxxxxxxxxxx xxxxxxxxx</td></tr>
			<tr><td colspan=6 align=center style=bold size=10>Listado de personas</td></tr>
			<tr><td colspan=6 align=center></td></tr>
			<tr></tr>";

        if (!$session->get('posicionesfiscales.persona') && !$session->get('posicionesfiscales.nombre')) {
            $sql = "SELECT p.persona, p.nombre, p.domicilio, l.descripcion,  p.celular, p.mail FROM personas p LEFT JOIN localidades l ON p.localidad = l.localidad";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT p.persona, p.nombre, p.domicilio, l.descripcion,  p.celular, p.mail FROM personas p LEFT JOIN localidades l ON p.localidad = l.localidad WHERE p.persona LIKE '%" . $session->get('posicionesfiscales.persona') . "%' AND p.nombre LIKE '%" . $session->get('posicionesfiscales.nombre') . "%'";
            $conca = " AND ";
        }

        if ($session->get('posicionesfiscales.localidad') != '-1' && $session->get('posicionesfiscales.localidad') != '') {
            $sql .= $conca . "p.localidad = '" . $session->get('posicionesfiscales.localidad') . "'";
            $conca = " AND ";
        }

        $sql .= " ORDER BY persona ASC";

        $reporte = $database->getRows($sql);

        foreach ($reporte as $estu) {

            $htmlTable .= "<tr><td colspan=6>______________________________________________________________________________________________________________</td></tr>
                                        <tr>
                                                <td align=left size=9>Documento</td>
                                                <td align=left size=9>" . utf8_decode($estu['persona']) . "</td>
                                        </tr>
                                        <tr>
                                                <td align=left size=9>Nombre</td>	
                                                <td align=left size=9 colspan= 5>" . utf8_decode($estu['nombre']) . "</td>
                                        </tr>
                                        <tr>
                                                <td align=left size=9>Domicilio<td>
                                                <td align=left size=9 colspan=2>" . utf8_decode($estu['domicilio']) . "</td>
                                                <td align=left size=9>Localidad</td>
                                                <td align=left size=9>" . utf8_decode($estu['descripcion']) . "</td>
                                        </tr>
                                        <tr>
                                                
                                                <td align=left size=9>Celular:</td>
                                                <td align=left size=9>" . utf8_decode($estu['celular']) . "</td>
                                                <td align=left size=9></td>
                                                <td align=left size=9></td>
                                        </tr>
                                        <tr>	
                                                <td align=left size=9>Email</td>
                                                <td align=left size=9 colspan=5>" . utf8_decode($estu['mail']) . "</td>
                                        </tr>";
        }

        $htmlTable .= "</table>";

        define('FPDF_FONTPATH', 'font/');
        require('library/pdf/pdftable.inc.php');
        //ob_end_clean();

        $p = new PDFTable();
        $p->AliasNbPages();
        $p->AddPage();
        $p->setfont('times', '', 6);
        $p->htmltable($htmlTable);
        $p->output('', 'I');
    }

}

?>