<?php

class ControllerEquipos extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'GESTIÓN DE EQUIPOS');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
       $cache = & $this->locator->get('cache');
        $request = & $this->locator->get('request');
        // </editor-fold>
         // <editor-fold defaultstate="collapsed" desc="LIMPIA FILTRO Y VARIBLES DE SESSION">

        if ($request->get('filtra') == NULL) {

            $session->set('equipos.search', '');
            $session->set('equipos.sort', '');
            $session->set('equipos.order', '');
            $session->set('equipos.page', '');

            $view->set('search', '');
            $view->set('equipos.search', '');
                        
            $cache->delete('equipos');
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">

        $cols = array();

        $cols[] = array(
            'name' => 'NRO SERIE',
            'sort' => 'modelo',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'TIPO',
            'sort' => 'tipoproducto',
            'align' => 'left'
        );
        $cols[] = array(
            'name' => 'MARCA',
            'sort' => 'descmodelo',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'MODELO',
            'sort' => 'descmarca',
            'align' => 'left'
        );


        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'nroserie',
            'tipoproducto',
            'descmarca',
            'descmodelo'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        set_time_limit(0);

        if (!$session->get('equipos.search')) {
            $sql = "SELECT * FROM vw_list_equipos  ";
        } else {
            $sql = "SELECT * FROM vw_list_equipos WHERE equipo LIKE '?' OR nroserie LIKE '?' OR desctipoproducto LIKE '?' OR descmodelo LIKE '?' OR descmarca LIKE '?' ";
        }

        if (in_array($session->get('equipos.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('equipos.sort') . " " . (($session->get('equipos.order') == 'desc') ? 'desc' : 'asc');
        } else {
            $sql .= " ORDER BY nroserie ASC";
        }

//		$consul = $database->splitQuery($database->parse($sql,'%' . $session->get('equipos.search') . '%','%' . $session->get('equipos.search') . '%'), $session->get('equipos.page'), $config->get('config_max_rows'));
//
//                $results = $database->getRows($consul);

        $consulta = $database->parse($sql, '%' . $session->get('equipos.search') . '%', '%' . $session->get('equipos.search') . '%', '%' . $session->get('equipos.search') . '%', '%' . $session->get('equipos.search') . '%', '%' . $session->get('equipos.search') . '%');
        $results = $database->getRows($consulta);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['nroserie'],
                'align' => 'left',
                'default' => 0
            );


            $cell[] = array(
                'value' => @$result['desctipoproducto'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['descmarca'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['descmodelo'],
                'align' => 'left',
                'default' => 0
            );

            $action = array();

            if ($user->hasPermisos($user->getPERSONA(), 'equipos', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa fa-fw fa-pencil',
                    'text' => $language->get('button_update'),
                    'prop_a' => array('href' => $url->ssl('equipos', 'update', array('equipo' => $result['equipo'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'equipos', 'B')) {
                $action[] = array(
                    'icon' => 'img/iconos-11.png',
                    'text' => $language->get('button_delete'),
                    'class' => 'fa fa-fw fa-trash-o',
                    'prop_a' => array('href' => "javascript:ActionDelete('" . $url->ssl('equipos', 'delete', array('equipo' => $result['equipo'])) . "');")
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'equipos', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw fa-search',
                    'text' => $language->get('button_consult'),
                    'prop_a' => array('href' => $url->ssl('equipos', 'consulta', array('equipo' => $result['equipo'])))
                );
            }

            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        //<editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('equipos.page'));

        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        $view->set('heading_title', 'Equipos');
        $view->set('placeholder_buscar', 'BUSCA POR NRO SERIE O DESCRIPCION O MARCA O MODELO O TIPO');

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        //$view->set('consultaVacia', $url->ssl('consulta','consulta_vacia'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        //$view->set('button_consultavacia', "Vacía");
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('equipos.search'));
        $view->set('sort', $session->get('equipos.sort'));
        $view->set('order', $session->get('equipos.order'));
        $view->set('page', $session->get('equipos.page'));

        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $view->set('titulo_ventana', '::. Exportar');

        $view->set('entry_modificar', "Modificar");
        $view->set('entry_agregar', "Agregar");

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('equipos', 'page'));
        $view->set('list', $url->ssl('equipos'));

        if ($user->hasPermisos($user->getPERSONA(), 'equipos', 'A')) {
            $view->set('insert', $url->ssl('equipos', 'insert'));
        }

        if ($user->hasPermisos($user->getPERSONA(), 'equipos', 'C'))
            $view->set('export', $url->ssl('equipos', 'exportar'));

        $view->set('addPais', $url->ssl('equipos', 'insert'));
        // $view->set('updatePersona', $url->ssl('paciente','update', array('persona' => $result['persona'])));
        $view->set('updatePais', $url->ssl('equipos', 'update'));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_equipos.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">
//		if ($request->has('modelo', 'post')) {
//                    $session->set('equipos.pais',$request->get('modelo','post'));              
//                }

        if ($request->has('search', 'post')) {
            $session->set('equipos.search', $request->get('search', 'post'));
        }


        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('equipos.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('equipos.order', (($session->get('equipos.sort') == $request->get('sort', 'post')) && ($session->get('equipos.order') == 'asc')) ? 'desc' : 'asc');
        }

        if ($request->has('sort', 'post')) {
            $session->set('equipos.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('equipos', 'index', array('filtra' => '1')));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $template = & $this->locator->get('template');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('heading_title', 'DATOS DEL EQUIPO');
        $view->set('entry_marca', 'Marca:');
        $view->set('entry_modelo', 'Modelo:');
        $view->set('entry_nroserie', 'Nro Serie:');
        $view->set('entry_tipoproducto', 'Tipo de producto:');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $template->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('tab_general', $language->get('tab_general'));

        if (($request->get('equipo')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM equipos WHERE equipo = '" . $request->get('equipo') . "' ";
            $objeto_info = $database->getRow($consulta);
        }

        if ($request->has('equipo', 'post')) {
            $view->set('equipo', $request->get('equipo', 'post'));
            $objeto_id = @$objeto_info['equipo'];
        } else {
            $view->set('equipo', $request->get('equipo', 'get'));
            $objeto_id = @$objeto_info['equipo'];
        }


        if ($request->has('tipoproducto', 'post')) {
            $view->set('tipoproducto', $request->get('tipoproducto', 'post'));
        } else {
            $view->set('tipoproducto', @$objeto_info['tipoproducto']);
        }
        $view->set('tiposproducto', $database->getRows("SELECT * FROM tiposproducto ORDER BY descripcion ASC"));

        if ($request->has('modelo', 'post')) {
            $view->set('modelo', $request->get('modelo', 'post'));
        } else {
            $view->set('modelo', @$objeto_info['modelo']);
        }
        $view->set('modelos', $database->getRows("SELECT modelo, CONCAT(descmarca,' - ',descripcion) as descripcion FROM vw_list_modelos ORDER BY descripcion ASC"));


        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $template->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');
        $view->set('error_texto_error', @$this->error['texto_error']);
        //$view->set('error_titulo', @$this->error['titulo']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        //$view->set('action', $url->ssl('equipos', $request->get('action'), array('modelo' => $request->get('modelo'))));
        $view->set('actionBoton', $request->get('action'));
        $view->set('cancel', $url->ssl('equipos'));
        // </editor-fold>

        return $view->fetch('content/equipo.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        $errores = '';

        if ($request->get('tipoproducto', 'post') == '-1') {
            $errores .= 'Debe seleccionar un tipo de producto. <br>';
        }

        if ((strlen($request->get('nroserie', 'post')) == 0)) {
            $errores .= 'Debe ingresar un número de serie. <br>';
        }

        if ($request->get('accion_form', 'post') == 'insert') {
            if ((strlen($request->get('nroserie', 'post')) > 0) && $request->get('tipoproducto', 'post') != '-1') {
                
            }
            $sql = "SELECT count(*) as total FROM equipos WHERE nroserie ='?' and tipoproducto='?' ";
            $equipo = $database->getRow($database->parse($sql, $request->get('nroserie', 'post'), $request->get('tipoproducto', 'post')));

            if ($equipo['total'] > 0) {
                $errores .= 'Este nro de serie con ese tipo de producto ya existe en el sistema. <br>';
            }
        }

        if ((strlen($request->get('nroserie', 'post')) > 0) && $request->get('tipoproducto', 'post') != '-1' && $request->get('accion_form', 'post') == 'update') {
            $sql = "SELECT count(*) as total FROM equipos WHERE nroserie ='?' and tipoproducto='?' and equipo!='?' ";
            $equipo = $database->getRow($database->parse($sql, $request->get('nroserie', 'post'), $request->get('tipoproducto', 'post'), $request->get('equipo', 'post')));

            if ($equipo['total'] > 0) {
                $errores .= 'Este nro de serie con ese tipo de producto ya existe en el sistema. <br>';
            }
        }

        if ($request->get('modelo', 'post') == '-1') {
            $errores .= 'Debe ingresar un modelo. <br>';
        }

        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';

        $consulta_info = $database->getRow("SELECT COUNT(*) as total FROM reparaciones WHERE equipo = '" . $request->get('equipo') . "' ");
        if (@$consulta_info['total'] != 0) {
            $errores .= 'No es posible eliminar el equipo, tiene reparaciones asigadas. <br>';
        }

        if ($errores != '') {
            $this->error['message'] = $errores;
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Equipos');

        if (($request->isPost()) && ($this->validateForm())) {
            $sql = "INSERT IGNORE INTO equipos SET nroserie='?',tipoproducto='?', modelo = '?'";
            $consulta = $database->parse($sql, $request->get('nroserie', 'post'), $request->get('tipoproducto', 'post'), $request->get('modelo', 'post'));
            $database->query($consulta);

            $id = $database->getLastId();
            $cache->delete('equipos');
            $session->set('message', 'Se agreg&oacute; el equipo: ' . $id);

            $response->redirect($url->ssl('equipos'));
        }
        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos del equipo');
        if (($request->isPost()) && ($this->validateForm())) {

            $id = $request->get('equipo', 'post');
            $sql = "UPDATE modelos SET nroserie='?',tipoproducto='?', modelo = '?'  WHERE equipo='?'  ";
            $consulta = $database->parse($sql, $request->get('nroserie', 'post'), $request->get('tipoproducto', 'post'), $request->get('modelo', 'post'), $id);
            $database->query($consulta);

            $cache->delete('equipos');

            $session->set('message', 'Se actualiz&oacute; el equipo: ' . $id);

            die('ok');
        }

        $response->set($this->getForm());
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos del equipo');
        if (($request->isPost())) {

            die('ok');
        }

        $response->set($this->getForm());
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        if (($request->get('equipo')) && ($this->validateDelete())) {

            $id = $request->get('equipo', 'get');

            //TODO: FALTA PROGRAMAR LA ELIMINACION Y VALIDACION VER SI NO HAY RELACIONES
            $sql = "DELETE FROM equipos WHERE equipo  = '" . $id . "' ";
            $database->query($sql);

            $cache->delete('equipos');

            $session->set('message', 'Se ha eliminado el equipo: ' . $id);

            $response->redirect($url->ssl('equipos'));
        }

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getModelo() {
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');

        $miDescripcion = $request->get('term', 'get');
        //$miPersona = "lucas";
        // $codigo = $database->getRows("select concat(apellido,', ', nombre) as value, persona_id  as id from personas where persona_id ='" . $miPersona . "'");		

        $sql = "SELECT modelo as id, CONCAT(descmarca,' - ',descripcion) as value "
                . "FROM vw_list_modelos "
                . "WHERE descripcion LIKE '?' OR descmarca LIKE '?' "
                . "LIMIT 10";
        $consulta = $database->parse($sql, '%' . $miDescripcion . '%', '%' . $miDescripcion . '%');
        $codigo = $database->getRows($consulta);

        $varia = utf8_decode(json_encode($codigo));


        echo $varia;
    }

}

?>