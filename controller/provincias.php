<?php

class ControllerProvincias extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'GESTION DE PROVINCIAS');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
       $cache = & $this->locator->get('cache');
        $request = & $this->locator->get('request');
        // </editor-fold>
         // <editor-fold defaultstate="collapsed" desc="LIMPIA FILTRO Y VARIBLES DE SESSION">

        if ($request->get('filtra') == NULL) {

            $session->set('provincias.search', '');
            $session->set('provincias.sort', '');
            $session->set('provincias.order', '');
            $session->set('provincias.page', '');

            $view->set('search', '');
            $view->set('provincias.search', '');
                        
            $cache->delete('provincias');
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">




        $cols = array();

        $cols[] = array(
            'name' => 'N&uacute;mero',
            'sort' => 'provincia',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Descripci&oacute;n',
            'sort' => 'descripcion',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Pais',
            'sort' => 'descpais',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'provincia',
            'descripcion'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        set_time_limit(0);

        if (!$session->get('provincias.search')) {
            $sql = "SELECT * FROM vw_grilla_provincias  ";
        } else {
            $sql = "SELECT * FROM vw_grilla_provincias  WHERE provincia LIKE '?' OR descripcion LIKE '?' OR descpais LIKE '?'";
        }

        if (in_array($session->get('provincias.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('provincias.sort') . " " . (($session->get('provincias.order') == 'desc') ? 'desc' : 'asc');
        } else {
            $sql .= " ORDER BY descripcion ASC";
        }

//		$consul = $database->splitQuery($database->parse($sql,'%' . $session->get('provincias.search') . '%','%' . $session->get('provincias.search') . '%'), $session->get('provincias.page'), $config->get('config_max_rows'));
//
//                $results = $database->getRows($consul);

        $consulta = $database->parse($sql, '%' . $session->get('provincias.search') . '%', '%' . $session->get('provincias.search') . '%', '%' . $session->get('provincias.search') . '%');
        $results = $database->getRows($consulta);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['provincia'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['descripcion'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['descpais'],
                'align' => 'left',
                'default' => 0
            );

            $action = array();

            if ($user->hasPermisos($user->getPERSONA(), 'provincias', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa fa-fw fa-pencil',
                    'text' => $language->get('button_update'),
                    'prop_a' => array('href' => $url->ssl('provincias', 'update', array('provincia' => $result['provincia'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'provincias', 'B')) {
                $action[] = array(
                    'icon' => 'img/iconos-11.png',
                    'text' => $language->get('button_delete'),
                    'class' => 'fa fa-fw fa-trash-o',
                    'prop_a' => array('href' => "javascript:ActionDelete('" . $url->ssl('provincias', 'delete', array('provincia' => $result['provincia'])) . "');")
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'provincias', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw fa-search',
                    'text' => $language->get('button_consult'),
                    'prop_a' => array('href' => $url->ssl('provincias', 'consulta', array('provincia' => $result['provincia'])))
                );
            }

            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        //<editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('provincias.page'));

        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        $view->set('heading_title', 'Provincias');
        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/NOTICIAS.png');
        $view->set('placeholder_buscar', 'BUSCA POR NRO O DESCRIPCION O PAIS');

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        //$view->set('consultaVacia', $url->ssl('consulta','consulta_vacia'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        //$view->set('button_consultavacia', "Vacía");
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('provincias.search'));
        $view->set('sort', $session->get('provincias.sort'));
        $view->set('order', $session->get('provincias.order'));
        $view->set('page', $session->get('provincias.page'));

        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $view->set('titulo_ventana', '::. Exportar Provincia');

        $view->set('entry_modificar', "Modificar provincia");
        $view->set('entry_agregar', "Agregar provincia");

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('provincias', 'page'));
        $view->set('list', $url->ssl('provincias'));

        if ($user->hasPermisos($user->getPERSONA(), 'provincias', 'A')) {
            $view->set('insert', $url->ssl('provincias', 'insert'));
        }

        if ($user->hasPermisos($user->getPERSONA(), 'provincias', 'C'))
            $view->set('export', $url->ssl('provincias', 'exportar'));

        $view->set('addProvincia', $url->ssl('provincias', 'insert'));
        // $view->set('updatePersona', $url->ssl('paciente','update', array('persona' => $result['persona'])));
        $view->set('updateProvincia', $url->ssl('provincias', 'update'));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_provincias.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">
//		if ($request->has('provincia', 'post')) {
//                    $session->set('provincias.provincia',$request->get('provincia','post'));              
//                }

        if ($request->has('search', 'post')) {
            $session->set('provincias.search', $request->get('search', 'post'));
        }

        if ($request->has('liga', 'post')) {
            $session->set('provincias.liga', $request->get('liga', 'post'));
        }

        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('provincias.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('provincias.order', (($session->get('provincias.sort') == $request->get('sort', 'post')) && ($session->get('provincias.order') == 'asc')) ? 'desc' : 'asc');
        }

        if ($request->has('sort', 'post')) {
            $session->set('provincias.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('provincias', 'index', array('filtra' => '1')));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $template = & $this->locator->get('template');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('heading_title', 'DATOS DE LA PROVINCIA');
        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/NOTICIAS.png');

        $view->set('entry_descripcion', 'Descripci&oacute;n:');
        $view->set('entry_pais', 'País:');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('tab_general', $language->get('tab_general'));

        $view->set('provincia', $request->get('provincia'));

        if (($request->get('provincia')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM vw_list_Provincias WHERE provincia = '" . $request->get('provincia') . "'";
            $provincia_info = $database->getRow($consulta);
        }

        if ($request->has('provincia', 'post')) {
            $view->set('provincia', $request->get('provincia', 'post'));
            $provincia_id = @$provincia_info['provincia'];
        } else {
            $view->set('provincia', $request->get('provincia', 'get'));
            $provincia_id = @$provincia_info['provincia'];
        }

        if ($request->has('descripcion', 'post')) {
            $view->set('descripcion', $request->get('descripcion', 'post'));
        } else {
            $view->set('descripcion', @$provincia_info['descripcion']);
        }

        // <editor-fold defaultstate="collapsed" desc="LOCALIDAD PROVINCIA PAIS">

        if ($request->has('auto_pais', 'post')) {
            $view->set('auto_pais', $request->get('auto_pais', 'post'));
        } else {
            $view->set('auto_pais', @$provincia_info['descpais']);
        }
        if ($request->has('auto_pais_provincia', 'post')) {
            $view->set('auto_pais_provincia', $request->get('auto_pais_provincia', 'post'));
        } else {
            $view->set('auto_pais_provincia', @$provincia_info['pais']);
        }
        $view->set('script_busca_pais', $url->rawssl('provincias', 'getPais'));
        // </editor-fold>
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        $view->set('error_texto_error', @$this->error['texto_error']);
        //$view->set('error_titulo', @$this->error['titulo']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        //$view->set('action', $url->ssl('provincias', $request->get('action'), array('provincia' => $request->get('provincia'))));
        $view->set('actionBoton', $request->get('action'));
        $view->set('cancel', $url->ssl('provincias'));
        // </editor-fold>

        return $view->fetch('content/provincia.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        if ((strlen($request->get('descripcion', 'post')) == 0)) {
            $errores = 'Debe ingresar la descripción. <br>';
        }

        if ((strlen($request->get('auto_pais', 'post')) == 0)) {
            $errores .= 'Debe seleccionar un país. <br>';
        }

        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';

        $consulta_info = $database->getRow("SELECT COUNT(*) as total FROM localidades WHERE provincia = '" . $request->get('provincia') . "'");
        if (@$consulta_info['total'] != 0) {
            $errores .= 'No es posible eliminar la provincia, tiene localidades asignadas. <br>';
        }

        if ($errores != '') {
            $this->error['message'] = $errores;
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Provincias');

        if (($request->isPost()) && ($this->validateForm())) {

            $pais = $request->get('auto_pais_provincia', 'post');
            $sql = "INSERT IGNORE INTO Provincias SET descripcion = '?', pais = '?' ";
            $consulta = $database->parse($sql, ucwords($request->get('descripcion', 'post')), $pais);
            $database->query($consulta);

            $id = $database->getLastId();
            $cache->delete('provincias');
            $session->set('message', 'Se agreg&oacute; la provincia: ' . $id);

            $response->redirect($url->ssl('provincias'));
        }
        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos de la provincia');
        if (($request->isPost()) && ($this->validateForm())) {

            $id = $request->get('provincia', 'post');
            $pais = $request->get('auto_pais_provincia', 'post');
            $sql = "UPDATE Provincias SET descripcion ='?', pais = '?'   WHERE provincia='?' ";
            $consulta = $database->parse($sql, ucwords($request->get('descripcion', 'post')), $id);
            $database->query($consulta);

            $cache->delete('provincias');

            $session->set('message', 'Se actualiz&oacute; la provincia: ' . $id);

            $response->redirect($url->ssl('provincias'));
        }
        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos de la provincia');
        if (($request->isPost())) {
            $response->redirect($url->ssl('provincias'));
        }

        $response->set($this->getForm());
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        if (($request->get('provincia')) && ($this->validateDelete())) {

            $id = $request->get('provincia', 'get');

            //TODO: FALTA PROGRAMAR LA ELIMINACION Y VALIDACION VER SI NO HAY RELACIONES
            $sql = "DELETE FROM provincias WHERE provincia = '" . $id . "' ";
            $database->query($sql);

            $cache->delete('provincias');

            $session->set('message', 'Se ha eliminado la provincia: ' . $id);

            $response->redirect($url->ssl('provincias'));
        }

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getPais() {
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');

        $miDescripcion = $request->get('term', 'get');

        $sql = "SELECT pais  as id, descripcion as value "
                . "FROM paises "
                . "WHERE descripcion LIKE '?' "
                . "LIMIT 10";
        $consulta = $database->parse($sql, '%' . $miDescripcion . '%');
        $codigo = $database->getRows($consulta);

        $varia = utf8_decode(json_encode($codigo));


        echo $varia;
    }

}

?>