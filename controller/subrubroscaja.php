<?php

class ControllerSubrubroscaja extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'GESTIÓN DE SUB RUBROS DE CAJA');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
       $request = & $this->locator->get('request');
        // </editor-fold>
         // <editor-fold defaultstate="collapsed" desc="LIMPIA FILTRO Y VARIBLES DE SESSION">

        if ($request->get('filtra') == NULL) {

            $session->set('subrubroscaja.search', '');
            $session->set('subrubroscaja.sort', '');
            $session->set('subrubroscaja.order', '');
            $session->set('subrubroscaja.page', '');

            $view->set('search', '');
            $view->set('subrubroscaja.search', '');
                        
            $cache->delete('subrubroscaja');
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">

        $cols = array();

        $cols[] = array(
            'name' => 'Sub rubro',
            'sort' => 'subrubro',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Descripci&oacute;n',
            'sort' => 'descripcion',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Rubro',
            'sort' => 'rubro',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'subrubro',
            'descripcion'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        set_time_limit(0);

        if (!$session->get('subrubroscaja.search')) {
            $sql = "SELECT * FROM vw_list_subrubroscaja  ";
        } else {
            $sql = "SELECT * FROM vw_list_subrubroscaja WHERE subrubro LIKE '?' OR descripcion LIKE '?' OR rubro LIKE '?' OR descrubro LIKE '?' ";
        }

        if (in_array($session->get('subrubroscaja.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('subrubroscaja.sort') . " " . (($session->get('subrubroscaja.order') == 'desc') ? 'desc' : 'asc');
        } else {
            $sql .= " ORDER BY descripcion ASC";
        }

//		$consul = $database->splitQuery($database->parse($sql,'%' . $session->get('subrubroscaja.search') . '%','%' . $session->get('subrubroscaja.search') . '%'), $session->get('subrubroscaja.page'), $config->get('config_max_rows'));
//
//                $results = $database->getRows($consul);

        $consulta = $database->parse($sql, '%' . $session->get('subrubroscaja.search') . '%', '%' . $session->get('subrubroscaja.search') . '%', '%' . $session->get('subrubroscaja.search') . '%', '%' . $session->get('subrubroscaja.search') . '%');
        $results = $database->getRows($consulta);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['subrubro'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['descripcion'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['descrubro'],
                'align' => 'left',
                'default' => 0
            );

            $action = array();

            if ($user->hasPermisos($user->getPERSONA(), 'subrubroscaja', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa fa-fw fa-pencil',
                    'text' => $language->get('button_update'),
                    'prop_a' => array('href' => $url->ssl('subrubroscaja', 'update', array('subrubro' => $result['subrubro'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'subrubroscaja', 'B')) {
                $action[] = array(
                    'icon' => 'img/iconos-11.png',
                    'text' => $language->get('button_delete'),
                    'class' => 'fa fa-fw fa-trash-o',
                    'prop_a' => array('href' => "javascript:ActionDelete('" . $url->ssl('subrubroscaja', 'delete', array('subrubro' => $result['subrubro'])) . "');")
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'subrubroscaja', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw fa-search',
                    'text' => $language->get('button_consult'),
                    'prop_a' => array('href' => $url->ssl('subrubroscaja', 'consulta', array('subrubro' => $result['subrubro'])))
                );
            }

            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        //<editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('subrubroscaja.page'));

        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        $view->set('heading_title', 'Sub Rubros de caja');
        $view->set('placeholder_buscar', 'BUSCA POR ID O DESCRIPCION O RUBRO');

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        //$view->set('consultaVacia', $url->ssl('consulta','consulta_vacia'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        //$view->set('button_consultavacia', "Vacía");
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('subrubroscaja.search'));
        $view->set('sort', $session->get('subrubroscaja.sort'));
        $view->set('order', $session->get('subrubroscaja.order'));
        $view->set('page', $session->get('subrubroscaja.page'));

        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $view->set('titulo_ventana', '::. Exportar');

        $view->set('entry_modificar', "Modificar");
        $view->set('entry_agregar', "Agregar");

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('subrubroscaja', 'page'));
        $view->set('list', $url->ssl('subrubroscaja'));

        if ($user->hasPermisos($user->getPERSONA(), 'subrubroscaja', 'A')) {
            $view->set('insert', $url->ssl('subrubroscaja', 'insert'));
        }

        if ($user->hasPermisos($user->getPERSONA(), 'subrubroscaja', 'C'))
            $view->set('export', $url->ssl('subrubroscaja', 'exportar'));

        $view->set('addPais', $url->ssl('subrubroscaja', 'insert'));
        // $view->set('updatePersona', $url->ssl('paciente','update', array('persona' => $result['persona'])));
        $view->set('updatePais', $url->ssl('subrubroscaja', 'update'));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_subrubroscaja.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">
//		if ($request->has('subrubro', 'post')) {
//                    $session->set('subrubroscaja.pais',$request->get('subrubro','post'));              
//                }

        if ($request->has('search', 'post')) {
            $session->set('subrubroscaja.search', $request->get('search', 'post'));
        }


        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('subrubroscaja.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('subrubroscaja.order', (($session->get('subrubroscaja.sort') == $request->get('sort', 'post')) && ($session->get('subrubroscaja.order') == 'asc')) ? 'desc' : 'asc');
        }

        if ($request->has('sort', 'post')) {
            $session->set('subrubroscaja.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('subrubroscaja', 'index', array('filtra' => '1')));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $template = & $this->locator->get('template');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('heading_title', 'DATOS DEL SUB RUBRO DE CAJA');
        $view->set('entry_rubro', 'Rubro:');
        $view->set('entry_subrubro', 'Sub rubro:');
        $view->set('entry_descripcion', 'Descripci&oacute;n:');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('tab_general', $language->get('tab_general'));

        $view->set('subrubro', $request->get('subrubro'));

        if (($request->get('subrubro')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM subrubroscaja WHERE subrubro = '" . $request->get('subrubro') . "'";
            $objeto_info = $database->getRow($consulta);
        }

        if ($request->has('subrubro', 'post')) {
            $view->set('subrubro', $request->get('subrubro', 'post'));
            $objeto_id = @$objeto_info['subrubro'];
        } else {
            $view->set('subrubro', $request->get('subrubro', 'get'));
            $objeto_id = @$objeto_info['subrubro'];
        }

        if ($request->has('descripcion', 'post')) {
            $view->set('descripcion', $request->get('descripcion', 'post'));
        } else {
            $view->set('descripcion', @$objeto_info['descripcion']);
        }

        if ($request->has('rubro', 'post')) {
            $view->set('rubro', $request->get('rubro', 'post'));
        } else {
            $view->set('rubro', @$objeto_info['rubro']);
        }
        $view->set('rubroscaja', $database->getRows("SELECT * FROM rubroscaja ORDER BY descripcion ASC"));


        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        $view->set('error_texto_error', @$this->error['texto_error']);
        //$view->set('error_titulo', @$this->error['titulo']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        //$view->set('action', $url->ssl('subrubroscaja', $request->get('action'), array('subrubro' => $request->get('subrubro'))));
        $view->set('actionBoton', $request->get('action'));
        $view->set('cancel', $url->ssl('subrubroscaja'));
        // </editor-fold>

        return $view->fetch('content/subrubrocaja.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        if (@$this->error['subrubro'] == '') {
            $sql = "SELECT count(subrubro) as total FROM subrubroscaja WHERE descripcion ='?'";
            $falla = $database->getRow($database->parse($sql, $request->get('descripcion', 'post')));

            if ($falla['total'] > 0 && $request->get('accion_form', 'post') == 'insert') {
                $errores = 'Esta subrubro ya existe en el sistema.<br>';
            }
        }

//                if ((strlen($request->get('subrubro', 'post')) == 0)) {
//			$errores .= 'Debe ingresar una falla.<br>';
//		}

        if ((strlen($request->get('descripcion', 'post')) == 0)) {
            $errores .= 'Debe ingresar la descripción.<br>';
        }

        $FAILL = $request->get('rubro', 'post');
        if ($request->get('rubro', 'post') == '-1') {
            $errores .= 'Debe seleccionar un rubro.<br>';
        }

        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';

        $consulta_info = $database->getRow("SELECT COUNT(*) as total FROM cajasdiaria WHERE subrubro = '" . $request->get('subrubro') . "'");
        if (@$consulta_info['total'] != 0) {
            $errores .= 'No es posible eliminar el sub rubro, tiene al menos un movimiento de caja asigado. <br>';
        }

        if ($errores != '') {
            $this->error['message'] = $errores;
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Tipos Falla');

        if (($request->isPost()) && ($this->validateForm())) {
            $sql = "INSERT IGNORE INTO subrubroscaja SET  descripcion = '?',  rubro = '?'";
            $consulta = $database->parse($sql, $request->get('descripcion', 'post'), $request->get('rubro', 'post'));
            $database->query($consulta);

            $id = $database->getLastId();
            $cache->delete('subrubroscaja');
            $session->set('message', 'Se agreg&oacute; el sub rubro de caja: ' . $id);

            $response->redirect($url->ssl('subrubroscaja'));
        }
        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos del sub rubro de caja');
        if (($request->isPost()) && ($this->validateForm())) {

            $id = $request->get('subrubro', 'post');
            $sql = "UPDATE subrubroscaja SET descripcion ='?',rubro='?'  WHERE subrubro='?' ";
            $consulta = $database->parse($sql, $request->get('descripcion', 'post'), $request->get('rubro', 'post'), $id);
            $database->query($consulta);

            $cache->delete('subrubroscaja');

            $session->set('message', 'Se actualiz&oacute; el sub rubro de caja: ' . $id);

            $response->redirect($url->ssl('subrubroscaja'));
        }
        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos del sub rubro de caja');
        if (($request->isPost())) {
            $response->redirect($url->ssl('subrubroscaja'));
        }

        $response->set($this->getForm());
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        if (($request->get('subrubro')) && ($this->validateDelete())) {

            $id = $request->get('subrubro', 'get');

            //TODO: FALTA PROGRAMAR LA ELIMINACION Y VALIDACION VER SI NO HAY RELACIONES
            $sql = "DELETE FROM subrubroscaja WHERE subrubro = '" . $id . "' ";
            $database->query($sql);

            $cache->delete('subrubroscaja');

            $session->set('message', 'Se ha eliminado el sub rubro de caja: ' . $id);

            $response->redirect($url->ssl('subrubroscaja'));
        }

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

}

?>