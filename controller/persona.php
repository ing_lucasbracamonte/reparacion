<?php

class ControllerPersona extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'Usuarios');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
        $request = & $this->locator->get('request');
        // </editor-fold>
         // <editor-fold defaultstate="collapsed" desc="LIMPIA FILTRO Y VARIBLES DE SESSION">

        if ($request->get('filtra') == NULL) {

            $session->set('persona.search', '');
            $session->set('persona.sort', '');
            $session->set('persona.order', '');
            $session->set('persona.page', '');

            $view->set('search', '');
            $view->set('persona.search', '');
                        
            $cache->delete('persona');
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">
        $cols = array();

        $cols[] = array(
            'name' => 'Persona',
            'sort' => 'persona',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Nombre',
            'sort' => 'apellido, nombre',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Grupos',
            'sort' => 'descgrupo',
            'align' => 'left'
        );


        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'persona',
            'apellido, nombre',
            'descgrupo'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        if (!$session->get('persona.search')) {
            $sql = "SELECT * FROM vw_grilla_personas_2  ";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT * FROM vw_grilla_personas_2  WHERE persona LIKE '?' OR nombre LIKE '?' OR apellido LIKE '?' ";
            $conca = " AND ";
        }

        if (in_array($session->get('persona.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('persona.sort') . " " . (($session->get('persona.order') == 'DESC') ? 'DESC' : 'ASC');
        } else {
            $sql .= " ORDER BY apellido, nombre ASC";
        }

        $consult = $database->parse($sql, '%' . $session->get('persona.search') . '%', '%' . $session->get('persona.search') . '%', '%' . $session->get('persona.search') . '%');
        $results = $database->getRows($consult);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['persona'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['apellido'] . ", " . @$result['nombre'],
                'align' => 'left',
                'default' => 0
            );

            $descgrupo = '-';
            if (@$result['grupos']) {
                $descgrupo = @$result['grupos'];
            }
            $cell[] = array(
                'value' => $descgrupo,
                'align' => 'left',
                'default' => 0
            );



            $action = array();

            if ($user->hasPermisos($user->getPERSONA(), 'persona', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa fa-fw fa-pencil',
                    'text' => $language->get('button_update'),
                    'prop_a' => array('href' => $url->ssl('persona', 'update', array('persona' => $result['persona'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'persona', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-24.png',
                    'class' => 'fa fa-fw fa-unlock',
                    'text' => $language->get('button_resetpassword'),
                    'prop_a' => array('href' => $url->ssl('persona', 'resetPassword', array('persona' => $result['persona'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'persona', 'B')) {
                $action[] = array(
                    'icon' => 'img/iconos-11.png',
                    'text' => $language->get('button_delete'),
                    'class' => 'fa fa-fw fa-trash-o',
                    'prop_a' => array('href' => "javascript:ActionDelete('" . $url->ssl('persona', 'delete', array('persona' => $result['persona'])) . "');")
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'persona', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw fa-search',
                    'text' => $language->get('button_consult'),
                    'prop_a' => array('href' => $url->ssl('persona', 'consulta', array('persona' => $result['persona'])))
                );
            }

            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('persona.page'));


        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        if ($session->get('persona.persona')) {
            $view->set('persona', $session->get('persona.persona'));
        } else {
            $view->set('persona', '');
        }

//		if ($session->get('persona.nombre')) {
//                    $view->set('nombre',$session->get('persona.nombre'));
//		} else {
//                    $view->set('nombre', '');
//		}
//		if ($session->get('persona.localidad')) {
//                    $view->set('localidad', $session->get('persona.localidad'));
//		} else {
//                    $view->set('localidad', '-1');
//		}
        //$view->set('localidades', $database->getRows("SELECT localidad, descripcion FROM localidades ORDER BY descripcion"));

        $view->set('heading_title', 'USUARIOS');
        $view->set('heading_title2', 'Filtrar');
        $view->set('heading_description', 'Gesti&oacute;n de Usuarios');

        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/NOTICIAS.png');
        $view->set('placeholder_buscar', 'BUSCA POR DNI, APELLIDO, NOMBRE, GRUPO');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('persona.search'));
        $view->set('sort', $session->get('persona.sort'));
        $view->set('order', $session->get('persona.order'));
        $view->set('page', $session->get('persona.page'));
        
        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('persona', 'page'));
        $view->set('list', $url->ssl('persona'));
        if ($user->hasPermisos($user->getPERSONA(), 'persona', 'A')) {
            $view->set('insert', $url->ssl('persona', 'insert'));
        }
        $view->set('listaMail', $url->ssl('persona', 'listaMail'));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_persona.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">

        if ($request->has('search', 'post')) {
            $session->set('persona.search', $request->get('search', 'post'));
        }

        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('persona.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('persona.order', (($session->get('persona.sort') == $request->get('sort', 'post')) && ($session->get('persona.order') == 'ASC')) ? 'DESC' : 'ASC');
        }

        if ($request->has('sort', 'post')) {
            $session->set('persona.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('persona', 'index', array('filtra' => '1')));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $language = & $this->locator->get('language');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('entry_persona', 'Documento:');
        $view->set('entry_apellido', 'Apellido:');
        $view->set('entry_nombre', 'Nombre:');
        $view->set('entry_celular', 'Celular:');
        $view->set('entry_mail', 'E-mail:');
        $view->set('entry_remail', 'Confirma E-mail:');
        $view->set('entry_fecha', 'Fecha nacimiento:');
        $view->set('entry_direccion', 'Direccion:');
        $view->set('entry_numero', 'Nro');
        $view->set('entry_piso', 'piso');
        $view->set('entry_departamento', 'Dto:');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('heading_title', 'Personas');
        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/user-8.png');

        $view->set('heading_description', 'Gesti&oacute;n de personas');

        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        $view->set('tab_general', $language->get('tab_general'));

        if (($request->get('persona')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM personas WHERE persona = '?'";
            $persona_info = $database->getRow($database->parse($consulta, $request->get('persona')));
        }

        if ($request->has('persona', 'post')) {
            $view->set('persona', $request->get('persona', 'post'));
            $persona_id = $request->get('persona', 'post');
        } else {
            $view->set('persona', @$persona_info['persona']);
            $persona_id = @$persona_info['persona'];
        }

        if ($request->has('apellido', 'post')) {
            $view->set('apellido', $request->get('apellido', 'post'));
        } else {
            $view->set('apellido', @$persona_info['apellido']);
        }

        if ($request->has('nombre', 'post')) {
            $view->set('nombre', $request->get('nombre', 'post'));
        } else {
            $view->set('nombre', @$persona_info['nombre']);
        }

        if ($request->has('mail', 'post')) {
            $view->set('mail', $request->get('mail', 'post'));
        } else {
            $view->set('mail', @$persona_info['mail']);
        }

        if ($request->has('direccion', 'post')) {
            $view->set('direccion', $request->get('direccion', 'post'));
        } else {
            $view->set('direccion', @$persona_info['direccion']);
        }

        if ($request->has('numero', 'post')) {
            $view->set('numero', $request->get('numero', 'post'));
        } else {
            $view->set('numero', @$persona_info['numero']);
        }

        if ($request->has('piso', 'post')) {
            $view->set('piso', $request->get('piso', 'post'));
        } else {
            $view->set('piso', @$persona_info['piso']);
        }

        if ($request->has('departamento', 'post')) {
            $view->set('departamento', $request->get('departamento', 'post'));
        } else {
            $view->set('departamento', @$persona_info['departamento']);
        }

        // <editor-fold defaultstate="collapsed" desc="LOCALIDAD PROVINCIA PAIS">

        if ($request->has('auto_localidad', 'post')) {
            $view->set('auto_localidad', $request->get('auto_localidad', 'post'));
        } else {
            $consulta = "SELECT localidad  as id, desclocalidad as value "
                    . "FROM vw_grilla_localidades WHERE localidad = '?'";
            $localidad_info = $database->getRow($database->parse($consulta, @$persona_info['localidad']));
            $view->set('auto_localidad', @$localidad_info['value']);
        }
        if ($request->has('auto_localidad_persona', 'post')) {
            $view->set('auto_localidad_persona', $request->get('auto_localidad_persona', 'post'));
        } else {
            $view->set('auto_localidad_persona', @$persona_info['localidad']);
        }
        $view->set('script_busca_localidades', $url->rawssl('persona', 'getLocalidad'));
        // </editor-fold>

        if ($request->has('celular', 'post')) {
            $view->set('celular', $request->get('celular', 'post'));
        } else {
            $view->set('celular', @$persona_info['celular']);
        }

        if ($request->has('remail', 'post')) {
            $view->set('remail', $request->get('remail', 'post'));
        } else {
            $view->set('remail', @$persona_info['mail']);
        }

        if ($request->has('fechanacimiento', 'post')) {
            $view->set('fechanacimiento', $request->get('fechanacimiento', 'post'));
        } else {
            if ($persona_info['fechanacimiento'] != '')
                $view->set('fechanacimiento', date('d/m/Y', strtotime(@$persona_info['fechanacimiento'])));
            else
                $view->set('fechanacimiento', '');
        }

        $grupo_data = array();

        $grupos = $user->getGrupos();

        $sql = "SELECT grupo, descripcion FROM grupos";

        $esSU = false;

        foreach ($grupos as $grupo) {
            if ($grupo['Grupo'] == 'SU') {
                $esSU = true;
            }
        }

        if (!$esSU) {
            $sql .= " WHERE grupo <> 'SU' and grupo <> 'SUUAR'";
        }

        $results = $database->getRows($sql);

        if ($request->has('grupos', 'post')) {
            foreach ($results as $result) {

                $grupo_data[] = array(
                    'grupo' => $result['grupo'],
                    'descripcion' => $result['descripcion'],
                    'persona' => (in_array($result['grupo'], $request->get('grupos', 'post', array())) ? true : false )
                );
            }
        } else {
            foreach ($results as $result) {
                $sql = "SELECT * FROM personasgrupos WHERE persona = '?' AND grupo = '?'";
                $user_group = $database->getRow($database->parse($sql, $persona_id, $result['grupo']));

                $grupo_data[] = array(
                    'grupo' => $result['grupo'],
                    'descripcion' => $result['descripcion'],
                    'persona' => (isset($user_group) ? $user_group : in_array($result['grupo'], $request->get('grupos', 'post', array())))
                );
            }
        }

        $view->set('grupos', $grupo_data);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $template->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');
        $view->set('error_persona', @$this->error['persona']);
        $view->set('error_domicilio', @$this->error['domicilio']);
        $view->set('error_apellido', @$this->error['apellido']);
        $view->set('error_nombre', @$this->error['nombre']);
        $view->set('error_localidad', @$this->error['localidad']);
        $view->set('error_mail', @$this->error['mail']);
        $view->set('error_remail', @$this->error['remail']);
        $view->set('error_grupo', @$this->error['grupo']);

        $view->set('error_texto_error', @$this->error['texto_error']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $urlAction = $url->ssl('persona', $request->get('action'), array('persona' => $request->get('persona')));
        $view->set('action', $urlAction);

        $view->set('accion_form', $request->get('action'));
        $view->set('cancel', $url->ssl('persona'));
        // </editor-fold>

        return $view->fetch('content/seguridad_persona.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $common = & $this->locator->get('common');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        if ((strlen($request->get('persona', 'post')) == 0)) {
            $errores = 'Debe ingresar el n&uacute;mero de documento. <br>';
            $this->error['persona'] = 'Debe ingresar el n&uacute;mero de documento. <br>';
        }

        if (strlen($request->get('persona', 'post')) < 7 || !is_numeric($request->get('persona', 'post'))) {
            $errores .= 'No es un n&uacute;mero de documento v&aacute;lido. <br>';
            $this->error['persona'] = 'Debe ingresar el n&uacute;mero de documento. <br>';
        }

        if (@$this->error['persona'] == '') {
            $sql = "SELECT count(persona) as total FROM personas WHERE persona ='?'";
            $persona = $database->getRow($database->parse($sql, $request->get('persona', 'post')));

            if ($persona['total'] > 0 && $request->get('accion_form', 'post') == 'insert') {
                $errores .= 'Esta persona ya existe en el sistema. <br>';
            }
        }

        if ((strlen($request->get('apellido', 'post')) < 2) || (strlen($request->get('apellido', 'post')) > 100)) {
            $errores .= 'Debe ingresar el apellido. <br>';
        }

        if ((strlen($request->get('nombre', 'post')) < 2) || (strlen($request->get('nombre', 'post')) > 100)) {
            $errores .= 'Debe ingresar el nombre. <br>';
        }

//		if ((strlen($request->get('domicilio', 'post')) == 0)) {
//			$this->error['domicilio'] = 'Debe ingresar el domicilio';
//                        $this->error['tab_general'] = '1';
//		}
//
//
//		if ((strlen($request->get('telefono', 'post')) == 0)) {
//			$this->error['telefono'] = 'Debe ingresar el tel&eacute;fono';
//                        $this->error['tab_general'] = '1';
//		}
//
//		if ((strlen($request->get('celular', 'post')) == 0)) {
//			$this->error['celular'] = 'Debe ingresar el celular';
//                        $this->error['tab_general'] = '1';
//		}
//
//		if (!$mail->validarDuplicado($request->get('mail', 'post'),$database,$mensaje,$request->get('persona', 'post'))) {
//			$this->error['mail'] = $mensaje;
//                        $this->error['tab_general'] = '1';
//		}              
//		if (!$common->validarDuplicado($request->get('mail', 'post'),$database,$mensaje,$request->get('persona', 'post'))) {
//			$this->error['mail'] = $mensaje;
//                        $this->error['tab_general'] = '1';
//		}              


        if (count($request->get('grupos', 'post', array())) == 0) {
            $errores .= 'Debe seleccionar al menos un grupo. <br>';
        }

        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function validateFormUpdate() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $common = & $this->locator->get('common');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';
        if ((strlen($request->get('apellido', 'post')) < 2) || (strlen($request->get('apellido', 'post')) > 100)) {
            $errores .= 'Debe ingresar el apellido. <br>';
            $this->error['tab_general'] = '1';
        }

        if ((strlen($request->get('nombre', 'post')) < 2) || (strlen($request->get('nombre', 'post')) > 100)) {
            $errores .= 'Debe ingresar el nombre. <br>';
            $this->error['tab_general'] = '1';
        }

        if (count($request->get('grupos', 'post', array())) == 0) {
            $errores .= 'Debe seleccionar al menos un grupo. <br>';
            $this->error['tab_general'] = '1';
        }

        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $common = & $this->locator->get('common');
        // </editor-fold>

        $template->set('title', 'Personas');

        if (($request->isPost()) && ($this->validateForm())) {
            $id = $request->get('persona', 'post');

            // <editor-fold defaultstate="collapsed" desc="FECHA">
            $fecha_d = '';

            if ($request->get('fechanacimiento', 'post') != '') {
                $dated = explode('/', $request->get('fechanacimiento', 'post'));
                $fecha_d = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
                $categoria = date('Y', strtotime($request->get('fechanacimiento', 'post')));
            } else {
                $fecha_d = NULL;
            }

            $fecha_i = '';



            // </editor-fold>

            $clave = $common->getEcriptar($id);

            $sql = "INSERT INTO personas SET persona = '?',clave ='?', nombre ='?', apellido ='?', fechanacimiento = NULLIF('?',''),celular ='?',  mail ='?', direccion = '?', numero = '?',  piso='?', departamento='?', localidad='?',usuario = '?'";
            $sql = $database->parse($sql, $id, $clave, strtoupper($request->get('nombre', 'post')), strtoupper($request->get('apellido', 'post')), $fecha_d, $request->get('celular', 'post'), $request->get('mail', 'post'), $request->get('direccion', 'post'), $request->get('numero', 'post'), $request->get('piso', 'post'), $request->get('departamento', 'post'), $request->get('auto_localidad_persona', 'post'), $user->getPERSONA());
            $database->query($sql);

            $Grupos = $request->get('grupos', 'post', array());
            foreach ($Grupos as $Grupo) {

                $sql = "INSERT IGNORE INTO personasgrupos SET persona = '?', grupo = '?', usuario = '?'";
                $database->query($database->parse($sql, $id, $Grupo, $user->getPERSONA()));

                if ($Grupo == 'VE') {
                    $sql = "UPDATE personas SET tipopersona = 'V',tipovendedor ='4' WHERE persona = '?'";
                    $sql = $database->parse($sql, $request->get('persona', 'post'));
                    $database->query($sql);
                }
            }

            $cache->delete('persona');
            $session->set('message', 'Se agreg&oacute; la persona: ' . $id);

            $response->redirect($url->ssl('persona'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        $template->set('title', 'Personas');

        if (($request->isPost()) && ($this->validateFormUpdate())) {

            $id = $request->get('persona', 'post');
            $mail = $request->get('mail', 'post');

            // <editor-fold defaultstate="collapsed" desc="FECHA">
            $fecha_d = '';
            if ($request->get('fechanacimiento', 'post') != '') {
                $dated = explode('/', $request->get('fechanacimiento', 'post'));
                $fecha_d = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
                $categoria = date('Y', strtotime($request->get('fechanacimiento', 'post')));
            } else {
                $fecha_d = NULL;
            }

            $fecha_i = '';

            // </editor-fold>
            //VALIDO QUE SI EL MAIL CAMBIO DEBE VOLVER A VALIDAR
            $consulta = "SELECT mail FROM personas WHERE persona = '?'";
            $persona_mail = $database->getRow($database->parse($consulta, $id));
            $mail_confirmado = 0;
            if ($request->get('mail', 'post') == $persona_mail['mail']) {
                $mail_confirmado = 1;
            }

            $sql = "UPDATE personas SET nombre = '?',apellido ='?', fechanacimiento=NULLIF('?',''), celular = '?',  mail ='?', mail_confirmado = '?', direccion = '?', numero = '?',  piso='?', departamento='?', localidad='?',usuario = '?' WHERE persona = '?'";
            $sql = $database->parse($sql, strtoupper($request->get('nombre', 'post')), strtoupper($request->get('apellido', 'post')), $fecha_d, $request->get('celular', 'post'), $request->get('mail', 'post'), $mail_confirmado, $request->get('direccion', 'post'), $request->get('numero', 'post'), $request->get('piso', 'post'), $request->get('departamento', 'post'), $request->get('auto_localidad_persona', 'post'), $user->getPERSONA(), $id);
            $database->query($sql);

            // Hago update para auditar el delete
            $sql = "UPDATE personasgrupos SET usuario = '?' WHERE persona = '?'";
            $database->query($database->parse($sql, $user->getPERSONA(), $id));

            $sql = "DELETE FROM personasgrupos WHERE persona = '?'";
            $database->query($database->parse($sql, $id));

            $Grupos = $request->get('grupos', 'post', array());

            foreach ($Grupos as $Grupo) {
                $sql = "INSERT IGNORE INTO personasgrupos SET persona = '?', grupo = '?', usuario = '?'";
                $database->query($database->parse($sql, $id, $Grupo, $user->getPERSONA()));

                if ($Grupo == 'VE') {
                    $sql = "UPDATE personas SET tipopersona = 'V',tipovendedor ='4' WHERE persona = '?'";
                    $sql = $database->parse($sql, $id);
                    $database->query($sql);
                }
            }

            $cache->delete('persona');
            $session->set('message', 'Se actualiz&oacute; la persona: ' . $id);

            $response->redirect($url->ssl('persona'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        $template->set('title', 'Personas');

        if (($request->isPost())) {

            $response->redirect($url->ssl('persona'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        if (($request->get('persona')) && ($this->validateDelete())) {

            $id = $request->get('persona', 'get');

            //TODO: FALTA PROGRAMAR LA ELIMINACION Y VALIDACION VER SI NO HAY RELACIONES
            $sql = "UPDATE personas SET usuario = '?' WHERE persona = '?'";
            $database->query($database->parse($sql, $user->getPERSONA(), $id));

            $sql = "DELETE FROM personas WHERE persona = '?'";
            $database->query($database->parse($sql, $id));
            // Hago update para auditar el delete
            $sql = "UPDATE personasgrupos SET usuario = '?' WHERE persona = '?'";
            $database->query($database->parse($sql, $user->getPERSONA(), $id));

            $sql = "DELETE FROM personasgrupos WHERE persona = '?'";
            $database->query($database->parse($sql, $id));

            $cache->delete('persona');

            $session->set('message', 'Se ha eliminado la persona: ' . $id);

            $response->redirect($url->ssl('persona'));
        }

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';

        if ($errores != '') {
            $this->error['message'] = $errores;
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function exportar() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $session = & $this->locator->get('session');
        // </editor-fold>

        set_time_limit(0);

        $filtro = 'Filtro:';

        $htmlTable = "<table border=0 width=180> 
			<tr><td colspan=6 align=center style=bold size=14>" . utf8_decode("xxxxxxxxx xxxxxxxxx xxxxxxx") . "</td></tr>
			<tr><td colspan=6 align=center style=bold size=12>SISTEMA DE xxxxxxxxxxx xxxxxxxxx</td></tr>
			<tr><td colspan=6 align=center style=bold size=10>Listado de personas</td></tr>
			<tr><td colspan=6 align=center></td></tr>
			<tr></tr>";

        if (!$session->get('persona.persona') && !$session->get('persona.nombre')) {
            $sql = "SELECT p.persona, p.nombre, p.domicilio, l.descripcion,  p.celular, p.mail FROM personas p LEFT JOIN localidades l ON p.localidad = l.localidad";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT p.persona, p.nombre, p.domicilio, l.descripcion,  p.celular, p.mail FROM personas p LEFT JOIN localidades l ON p.localidad = l.localidad WHERE p.persona LIKE '%" . $session->get('persona.persona') . "%' AND p.nombre LIKE '%" . $session->get('persona.nombre') . "%'";
            $conca = " AND ";
        }

        if ($session->get('persona.localidad') != '-1' && $session->get('persona.localidad') != '') {
            $sql .= $conca . "p.localidad = '" . $session->get('persona.localidad') . "'";
            $conca = " AND ";
        }

        $sql .= " ORDER BY persona ASC";

        $reporte = $database->getRows($sql);

        foreach ($reporte as $estu) {

            $htmlTable .= "<tr><td colspan=6>______________________________________________________________________________________________________________</td></tr>
                                        <tr>
                                                <td align=left size=9>Documento</td>
                                                <td align=left size=9>" . utf8_decode($estu['persona']) . "</td>
                                        </tr>
                                        <tr>
                                                <td align=left size=9>Nombre</td>	
                                                <td align=left size=9 colspan= 5>" . utf8_decode($estu['nombre']) . "</td>
                                        </tr>
                                        <tr>
                                                <td align=left size=9>Domicilio<td>
                                                <td align=left size=9 colspan=2>" . utf8_decode($estu['domicilio']) . "</td>
                                                <td align=left size=9>Localidad</td>
                                                <td align=left size=9>" . utf8_decode($estu['descripcion']) . "</td>
                                        </tr>
                                        <tr>
                                                
                                                <td align=left size=9>Celular:</td>
                                                <td align=left size=9>" . utf8_decode($estu['celular']) . "</td>
                                                <td align=left size=9></td>
                                                <td align=left size=9></td>
                                        </tr>
                                        <tr>	
                                                <td align=left size=9>Email</td>
                                                <td align=left size=9 colspan=5>" . utf8_decode($estu['mail']) . "</td>
                                        </tr>";
        }

        $htmlTable .= "</table>";

        define('FPDF_FONTPATH', 'font/');
        require('library/pdf/pdftable.inc.php');
        //ob_end_clean();

        $p = new PDFTable();
        $p->AliasNbPages();
        $p->AddPage();
        $p->setfont('times', '', 6);
        $p->htmltable($htmlTable);
        $p->output('', 'I');
    }

    function resetPassword() {
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $user = & $this->locator->get('user');
        $common = & $this->locator->get('common');

        $template->set('title', $language->get('heading_title'));

        if ($user->hasPermisos($user->getPERSONA(), 'persona', 'M')) {

            $clave = $common->getEcriptar($request->get('persona', 'get'));
            $sql = "UPDATE personas SET clave = '?', usuario = '?' WHERE persona = '?'";
            $database->query($database->parse($sql, $clave, $user->getPERSONA(), $request->get('persona', 'get')));

            $session->set('message', "Se ha restablecido la contrase&ntilde;a");

            $response->redirect($url->ssl('persona'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function listaMail() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        if (!$session->get('persona.search')) {
            $sql = "SELECT persona, nombre, apellido,domicilio,telefono, mail, descripcion FROM vw_list_personas ";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT persona, nombre, apellido,domicilio,telefono, mail, descripcion FROM vw_list_personas WHERE persona LIKE '?' OR nombre LIKE '?' OR apellido LIKE '?' OR descripcion LIKE '?'  ";
            $conca = " AND ";
        }

        $sql .= " ORDER BY apellido, nombre ASC";

        $consult = $database->parse($sql, '%' . $session->get('persona.search') . '%', '%' . $session->get('persona.search') . '%', '%' . $session->get('persona.search') . '%', '%' . $session->get('persona.search') . '%');
        $results = $database->getRows($consult);

        // </editor-fold>

        $concat = '; ';
        foreach ($results as $integrante) {
            if ($integrante['mail'] != '') {
                $lista_de_mails .= $integrante['mail'] . $concat;
                $concat = '; ';
            }
        }
        if (strlen($lista_de_mails) < 4) {
            $lista_de_mails = "Sin inscriptos.";
        }

        $view = $this->locator->create('template');
        $view->set('lista_de_mails', $lista_de_mails);
        $response->set($view->fetch('content/listademail.tpl'));
    }

    function getLocalidad() {
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');

        $miDescripcion = $request->get('term', 'get');

        $sql = "SELECT localidad  as id, desclocalidad as value "
                . "FROM vw_grilla_localidades "
                . "WHERE descripcion LIKE '?' OR desclocalidad LIKE '?' OR descprovincia LIKE '?' OR cp LIKE '?'  "
                . "LIMIT 10";
        $consulta = $database->parse($sql, '%' . $miDescripcion . '%', '%' . $miDescripcion . '%', '%' . $miDescripcion . '%', '%' . $miDescripcion . '%');
        $codigo = $database->getRows($consulta);

        $varia = utf8_decode(json_encode($codigo));

        echo $varia;
    }
}

?>