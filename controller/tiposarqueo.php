<?php

class ControllerTiposarqueo extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'GESTIÓN DE TIPOS DE ARQUEO');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
         $cache = & $this->locator->get('cache');
       $request = & $this->locator->get('request');
        // </editor-fold>
         // <editor-fold defaultstate="collapsed" desc="LIMPIA FILTRO Y VARIBLES DE SESSION">

        if ($request->get('filtra') == NULL) {

            $session->set('tiposarqueo.search', '');
            $session->set('tiposarqueo.sort', '');
            $session->set('tiposarqueo.order', '');
            $session->set('tiposarqueo.page', '');

            $view->set('search', '');
            $view->set('tiposarqueo.search', '');
                        
            $cache->delete('tiposarqueo');
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">

        $cols = array();

        $cols[] = array(
            'name' => 'Tipo',
            'sort' => 'tipo',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Descripci&oacute;n',
            'sort' => 'descripcion',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'tipo',
            'descripcion'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        set_time_limit(0);

        if (!$session->get('tiposarqueo.search')) {
            $sql = "SELECT * FROM tiposarqueo  ";
        } else {
            $sql = "SELECT * FROM tiposarqueo WHERE tipoarqueo LIKE '?' OR descripcion LIKE '?'";
        }

        if (in_array($session->get('tiposarqueo.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('tiposarqueo.sort') . " " . (($session->get('tiposarqueo.order') == 'desc') ? 'desc' : 'asc');
        } else {
            $sql .= " ORDER BY descripcion ASC";
        }

//		$consul = $database->splitQuery($database->parse($sql,'%' . $session->get('tiposarqueo.search') . '%','%' . $session->get('tiposarqueo.search') . '%'), $session->get('tiposarqueo.page'), $config->get('config_max_rows'));
//
//                $results = $database->getRows($consul);

        $consulta = $database->parse($sql, '%' . $session->get('tiposarqueo.search') . '%', '%' . $session->get('tiposarqueo.search') . '%');
        $results = $database->getRows($consulta);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['tipoarqueo'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['descripcion'],
                'align' => 'left',
                'default' => 0
            );

            $action = array();

            if ($user->hasPermisos($user->getPERSONA(), 'tiposarqueo', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa fa-fw fa-pencil',
                    'text' => $language->get('button_update'),
                    'prop_a' => array('href' => $url->ssl('tiposarqueo', 'update', array('tipoarqueo' => $result['tipoarqueo'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'tiposarqueo', 'B')) {
                $action[] = array(
                    'icon' => 'img/iconos-11.png',
                    'text' => $language->get('button_delete'),
                    'class' => 'fa fa-fw fa-trash-o',
                    'prop_a' => array('href' => "javascript:ActionDelete('" . $url->ssl('tiposarqueo', 'delete', array('tipoarqueo' => $result['tipoarqueo'])) . "');")
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'tiposarqueo', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw fa-search',
                    'text' => $language->get('button_consult'),
                    'prop_a' => array('href' => $url->ssl('tiposarqueo', 'consulta', array('tipoarqueo' => $result['tipoarqueo'])))
                );
            }

            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        //<editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('tiposarqueo.page'));

        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        $view->set('heading_title', 'Tipos de Arqueo');
        $view->set('placeholder_buscar', 'BUSCA POR ID O DESCRIPCION');

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        //$view->set('consultaVacia', $url->ssl('consulta','consulta_vacia'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        //$view->set('button_consultavacia', "Vacía");
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('tiposarqueo.search'));
        $view->set('sort', $session->get('tiposarqueo.sort'));
        $view->set('order', $session->get('tiposarqueo.order'));
        $view->set('page', $session->get('tiposarqueo.page'));

        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $view->set('titulo_ventana', '::. Exportar');

        $view->set('entry_modificar', "Modificar");
        $view->set('entry_agregar', "Agregar");

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('tiposarqueo', 'page'));
        $view->set('list', $url->ssl('tiposarqueo'));

        if ($user->hasPermisos($user->getPERSONA(), 'tiposarqueo', 'A')) {
            $view->set('insert', $url->ssl('tiposarqueo', 'insert'));
        }

        if ($user->hasPermisos($user->getPERSONA(), 'tiposarqueo', 'C'))
            $view->set('export', $url->ssl('tiposarqueo', 'exportar'));

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_tiposarqueo.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">
//		if ($request->has('tipoarqueo', 'post')) {
//                    $session->set('tiposarqueo.pais',$request->get('tipoarqueo','post'));              
//                }

        if ($request->has('search', 'post')) {
            $session->set('tiposarqueo.search', $request->get('search', 'post'));
        }


        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('tiposarqueo.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('tiposarqueo.order', (($session->get('tiposarqueo.sort') == $request->get('sort', 'post')) && ($session->get('tiposarqueo.order') == 'asc')) ? 'desc' : 'asc');
        }

        if ($request->has('sort', 'post')) {
            $session->set('tiposarqueo.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('tiposarqueo', 'index', array('filtra' => '1')));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $template = & $this->locator->get('template');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('heading_title', 'DATOS DE LA TIPO DE ARQUEO');
        $view->set('entry_descripcion', 'Descripci&oacute;n:');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('tab_general', $language->get('tab_general'));

        $view->set('tipoarqueo', $request->get('tipoarqueo'));

        if (($request->get('tipoarqueo')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM tiposarqueo WHERE tipoarqueo = '" . $request->get('tipoarqueo') . "'";
            $objeto_info = $database->getRow($consulta);
        }

        if ($request->has('tipoarqueo', 'post')) {
            $view->set('tipoarqueo', $request->get('tipoarqueo', 'post'));
            $objeto_id = @$objeto_info['tipoarqueo'];
        } else {
            $view->set('tipoarqueo', $request->get('tipoarqueo', 'get'));
            $objeto_id = @$objeto_info['tipoarqueo'];
        }

        if ($request->has('descripcion', 'post')) {
            $view->set('descripcion', $request->get('descripcion', 'post'));
        } else {
            $view->set('descripcion', @$objeto_info['descripcion']);
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        $view->set('error_texto_error', @$this->error['texto_error']);
        //$view->set('error_titulo', @$this->error['titulo']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        //$view->set('action', $url->ssl('tiposarqueo', $request->get('action'), array('tipoarqueo' => $request->get('tipoarqueo'))));
        $view->set('actionBoton', $request->get('action'));
        $view->set('cancel', $url->ssl('tiposarqueo'));
        // </editor-fold>

        return $view->fetch('content/tipoarqueo.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        
        if ((strlen($request->get('descripcion', 'post')) == 0)) {
            $errores = 'Debe ingresar la descripción.';
        }

        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';

        $consulta_info = $database->getRow("SELECT COUNT(*) as total FROM duas WHERE tipoarqueo = '" . $request->get('tipoarqueo') . "'");
        if (@$consulta_info['total'] != 0) {
            $errores .= 'No es posible eliminar la tipo de arqueo, tiene al menos un DUA asignado. <br>';
        }

        if ($errores != '') {
            $this->error['message'] = $errores;
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Tipos de Arqueo');

        if (($request->isPost()) && ($this->validateForm())) {
            $sql = "INSERT IGNORE INTO tiposarqueo SET descripcion = '?' ";
            $consulta = $database->parse($sql, $request->get('descripcion', 'post'));
            $database->query($consulta);

            $id = $database->getLastId();
            $cache->delete('tiposarqueo');
            $session->set('message', 'Se agreg&oacute; el tipo de arqueo: ' . $id);

            $response->redirect($url->ssl('tiposarqueo'));
        }
        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos del tipo de arqueo');
        if (($request->isPost()) && ($this->validateForm())) {

            $id = $request->get('tipoarqueo', 'post');
            $sql = "UPDATE tiposarqueo SET descripcion ='?'  WHERE tipoarqueo='?' ";
            $consulta = $database->parse($sql, $request->get('descripcion', 'post'), $id);
            $database->query($consulta);

            $cache->delete('tiposarqueo');

            $session->set('message', 'Se actualiz&oacute; el tipo de arqueo: ' . $id);

            $response->redirect($url->ssl('tiposarqueo'));
        }
        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos de la tipo de arqueo');
        if (($request->isPost())) {
            $response->redirect($url->ssl('tiposarqueo'));
        }

        $response->set($this->getForm());
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        if (($request->get('tipoarqueo')) && ($this->validateDelete())) {

            $id = $request->get('tipoarqueo', 'get');

            //TODO: FALTA PROGRAMAR LA ELIMINACION Y VALIDACION VER SI NO HAY RELACIONES
            $sql = "DELETE FROM tiposarqueo WHERE tipoarqueo = '" . $id . "' ";
            $database->query($sql);

            $cache->delete('tiposarqueo');

            $session->set('message', 'Se ha eliminado el parametro: ' . $id);

            $response->redirect($url->ssl('tiposarqueo'));
        }

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

}

?>