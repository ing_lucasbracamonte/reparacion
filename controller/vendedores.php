<?php

class ControllerVendedores extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'VENDEDORES');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
       $request = & $this->locator->get('request');
        // </editor-fold>
         // <editor-fold defaultstate="collapsed" desc="LIMPIA FILTRO Y VARIBLES DE SESSION">

        if ($request->get('filtra') == NULL) {

            $session->set('vendedores.search', '');
            $session->set('vendedores.sort', '');
            $session->set('vendedores.order', '');
            $session->set('vendedores.page', '');

            $view->set('search', '');
            $view->set('vendedores.search', '');
                        
            $cache->delete('vendedores');
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">
        $cols = array();

        $cols[] = array(
            'name' => 'Persona',
            'sort' => 'persona',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Nombre',
            'sort' => 'apellido, nombre',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Tipo ',
            'sort' => 'desctipovendedor',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Depende de ',
            'sort' => 'apellidonombredependencia',
            'align' => 'left'
        );
        
        $cols[] = array(
            'name' => 'Punto Vta',
            'sort' => 'puntovta',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'persona',
            'apellido, nombre',
            'desctipovendedor',
            'apellidonombredependencia'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        if (!$session->get('vendedores.search')) {
            $sql = "SELECT * FROM vw_grilla_vendedores  ";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT * FROM vw_grilla_vendedores  WHERE persona LIKE '?' OR nombre LIKE '?' OR apellido LIKE '?' ";
            $conca = " AND ";
        }

        // $sql .= $conca ." grupo = 'VE'";

        if (in_array($session->get('vendedores.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('vendedores.sort') . " " . (($session->get('vendedores.order') == 'DESC') ? 'DESC' : 'ASC');
        } else {
            $sql .= " ORDER BY apellido, nombre ASC";
        }

        $consult = $database->parse($sql, '%' . $session->get('vendedores.search') . '%', '%' . $session->get('vendedores.search') . '%', '%' . $session->get('vendedores.search') . '%');
        $results = $database->getRows($consult);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['persona'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['apellido'] . ", " . @$result['nombre'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['desctipovendedor'],
                'align' => 'left',
                'default' => 0
            );

            $apellidonombredependencia = "-";
            if (@$result['apellidonombredependencia'])
                $apellidonombredependencia = @$result['apellidonombredependencia'];
            $cell[] = array(
                'value' => $apellidonombredependencia,
                'align' => 'left',
                'default' => 0
            );
            
            $puntosdeventas = "-";
            if (@$result['puntosdeventas'])
                $puntosdeventas = @$result['puntosdeventas'];
            $cell[] = array(
                'value' => $puntosdeventas,
                'align' => 'left',
                'default' => 0
            );

            $action = array();

            if ($user->hasPermisos($user->getPERSONA(), 'vendedores', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa fa-fw fa-pencil',
                    'text' => $language->get('button_update'),
                    'prop_a' => array('href' => $url->ssl('vendedores', 'update', array('persona' => $result['persona'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'vendedores', 'B')) {
                $action[] = array(
                    'icon' => 'img/iconos-11.png',
                    'text' => $language->get('button_delete'),
                    'class' => 'fa fa-fw fa-trash-o',
                    'prop_a' => array('href' => "javascript:ActionDelete('" . $url->ssl('vendedores', 'delete', array('persona' => $result['persona'])) . "');")
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'vendedores', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw fa-search',
                    'text' => $language->get('button_consult'),
                    'prop_a' => array('href' => $url->ssl('vendedores', 'consulta', array('persona' => $result['persona'])))
                );
            }

            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('vendedores.page'));


        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        if ($session->get('vendedores.persona')) {
            $view->set('persona', $session->get('vendedores.persona'));
        } else {
            $view->set('persona', '');
        }

//		if ($session->get('vendedores.nombre')) {
//                    $view->set('nombre',$session->get('vendedores.nombre'));
//		} else {
//                    $view->set('nombre', '');
//		}
//		if ($session->get('vendedores.localidad')) {
//                    $view->set('localidad', $session->get('vendedores.localidad'));
//		} else {
//                    $view->set('localidad', '-1');
//		}
        //$view->set('localidades', $database->getRows("SELECT localidad, descripcion FROM localidades ORDER BY descripcion"));

        $view->set('heading_title', 'VENDEDORES');
        $view->set('heading_title2', 'Filtrar');
        $view->set('heading_description', 'Gesti&oacute;n de Vendedores');

        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/NOTICIAS.png');
        $view->set('placeholder_buscar', 'BUSCA POR DNI O APELLIDO O NOMBRE ');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('vendedores.search'));
        $view->set('sort', $session->get('vendedores.sort'));
        $view->set('order', $session->get('vendedores.order'));
        $view->set('page', $session->get('vendedores.page'));
        $view->set('search', $session->get('vendedores.search'));
       
        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('vendedores', 'page'));
        $view->set('list', $url->ssl('vendedores'));
        if ($user->hasPermisos($user->getPERSONA(), 'vendedores', 'A')) {
            $view->set('insert', $url->ssl('vendedores', 'insert'));
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_vendedores.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">

        if ($request->has('search', 'post')) {
            $session->set('vendedores.search', $request->get('search', 'post'));
        }

        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('vendedores.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('vendedores.order', (($session->get('vendedores.sort') == $request->get('sort', 'post')) && ($session->get('vendedores.order') == 'ASC')) ? 'DESC' : 'ASC');
        }

        if ($request->has('sort', 'post')) {
            $session->set('vendedores.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('vendedores', 'index', array('filtra' => '1')));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $language = & $this->locator->get('language');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('entry_persona', 'Documento:');
        $view->set('entry_apellido', 'Apellido:');
        $view->set('entry_nombre', 'Nombre:');
        $view->set('entry_celular', 'Celular:');
        $view->set('entry_mail', 'E-mail:');
        $view->set('entry_remail', 'Confirma E-mail:');
        $view->set('entry_fecha', 'Fecha nacimiento:');

        $view->set('entry_dependencia', 'Depende de:');
        $view->set('entry_tipovendedor', 'Tipo Vendedor:');
        $view->set('entry_tipoiva', 'Tipo IVA:');

        $view->set('entry_zona', 'Zona:');

        $view->set('entry_vendedorpropio', 'Vendedor propio?:');
        $view->set('entry_comision', '¿Comisiona?:');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('heading_title', 'Vendedores');
        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/user-8.png');

        $view->set('heading_description', 'Gesti&oacute;n de vendedores');

        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        $view->set('tab_general', $language->get('tab_general'));

        if (($request->get('persona')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM personas WHERE persona = '?'";
            $persona_info = $database->getRow($database->parse($consulta, $request->get('persona')));
        }

        if ($request->has('persona', 'post')) {
            $view->set('persona', $request->get('persona', 'post'));
            $persona_id = $request->get('persona', 'post');
        } else {
            $view->set('persona', @$persona_info['persona']);
            $persona_id = @$persona_info['persona'];
        }

        if ($request->has('apellido', 'post')) {
            $view->set('apellido', $request->get('apellido', 'post'));
        } else {
            $view->set('apellido', @$persona_info['apellido']);
        }

        if ($request->has('nombre', 'post')) {
            $view->set('nombre', $request->get('nombre', 'post'));
        } else {
            $view->set('nombre', @$persona_info['nombre']);
        }

        $view->set('entry_direccion', 'Direccion:');
        $view->set('entry_numero', 'Nro');
        $view->set('entry_piso', 'piso');
        $view->set('entry_departamento', 'Dto:');
        $view->set('entry_localidad', 'Localidad:');

        if ($request->has('direccion', 'post')) {
            $view->set('direccion', $request->get('direccion', 'post'));
        } else {
            $view->set('direccion', @$persona_info['direccion']);
        }

        if ($request->has('numero', 'post')) {
            $view->set('numero', $request->get('numero', 'post'));
        } else {
            $view->set('numero', @$persona_info['numero']);
        }

        if ($request->has('piso', 'post')) {
            $view->set('piso', $request->get('piso', 'post'));
        } else {
            $view->set('piso', @$persona_info['piso']);
        }

        if ($request->has('departamento', 'post')) {
            $view->set('departamento', $request->get('departamento', 'post'));
        } else {
            $view->set('departamento', @$persona_info['departamento']);
        }

        if ($request->has('auto_localidad', 'post')) {
            $view->set('auto_localidad', $request->get('auto_localidad', 'post'));
        } else {
            $consulta = "SELECT localidad  as id, desclocalidad as value "
                    . "FROM vw_grilla_localidades WHERE localidad = '?'";
            $localidad_info = $database->getRow($database->parse($consulta, @$persona_info['localidad']));
            $view->set('auto_localidad', @$localidad_info['value']);
        }
        if ($request->has('auto_localidad_vendedor', 'post')) {
            $view->set('auto_localidad_vendedor', $request->get('auto_localidad_vendedor', 'post'));
        } else {
            $view->set('auto_localidad_vendedor', @$persona_info['localidad']);
        }
        $view->set('script_busca_localidades', $url->rawssl('vendedores', 'getLocalidad'));

        if ($request->has('celular', 'post')) {
            $view->set('celular', $request->get('celular', 'post'));
        } else {
            $view->set('celular', @$persona_info['celular']);
        }

        if ($request->has('comisiona', 'post')) {
            $view->set('comisiona', $request->get('comisiona', 'post'));
        } else {
            $view->set('comisiona', @$persona_info['comisiona']);
        }

        if ($request->has('mail', 'post')) {
            $view->set('mail', $request->get('mail', 'post'));
        } else {
            $view->set('mail', @$persona_info['mail']);
        }

        if ($request->has('remail', 'post')) {
            $view->set('remail', $request->get('remail', 'post'));
        } else {
            $view->set('remail', @$persona_info['mail']);
        }

        if ($request->has('fechanacimiento', 'post')) {
            $view->set('fechanacimiento', $request->get('fechanacimiento', 'post'));
        } else {
            if ($persona_info['fechanacimiento'] != '')
                $view->set('fechanacimiento', date('d/m/Y', strtotime(@$persona_info['fechanacimiento'])));
            else
                $view->set('fechanacimiento', '');
        }

        if ($request->has('tipovendedor', 'post')) {
            $view->set('tipovendedor', $request->get('tipovendedor', 'post'));
        } else {
            $view->set('tipovendedor', @$persona_info['tipovendedor']);
        }
        $view->set('tiposvendedor', $database->getRows("SELECT * FROM tiposvendedor ORDER BY descripcion ASC"));

        if ($request->has('tipoiva', 'post')) {
            $view->set('tipoiva', $request->get('tipoiva', 'post'));
        } else {
            $view->set('tipoiva', @$persona_info['tipoiva']);
        }
        $view->set('tiposiva', $database->getRows("SELECT * FROM tiposiva ORDER BY descripcion ASC"));


        if ($request->has('dependenciapersona', 'post')) {
            $view->set('dependenciapersona', $request->get('dependenciapersona', 'post'));
        } else {
            $view->set('dependenciapersona', @$persona_info['dependenciapersona']);
        }
        $view->set('vendedores', $database->getRows("SELECT persona, apellidonombre FROM vw_grilla_personas WHERE grupo = 'VE'"));

        $puntosdeventa_data = array();

        $sql = "SELECT persona FROM vendedorespuntodeventa WHERE persona = '?'";
        $puntosdevtadelvendedor = $database->getRows($database->parse($sql, @$persona_id));
        //$mailsdelgrupo == $grupos

        $sql = "SELECT puntovta, descripcion FROM puntosdeventa";
        $todoslospuntosdevta = $database->getRows($sql);
        //$results == $mails
        //ARMAR UN ARRAY NUEVO PARA DIBUJAR LOS CHECKEADOS Y LOS NO CHECKEADOS
        //RECORRO 
        if ($request->has('puntosdevta', 'post')) {
            foreach ($todoslospuntosdevta as $result) {

                $puntosdeventa_data[] = array(
                    'puntovta' => $result['puntovta'],
                    'descripcion' => $result['descripcion'],
                    'checked' => (in_array($result['puntovta'], $request->get('puntosdevta', 'post', array())) ? true : false )//esto hace que si esta en el array lo chequea
                );
            }
        } else {
            foreach ($todoslospuntosdevta as $result) {
                $sql = "SELECT * FROM vendedorespuntodeventa WHERE puntovta = '?' AND persona = '?'";
                $vendedor_ptovta = $database->getRow($database->parse($sql, $result['puntovta'], @$persona_id));

                $puntosdeventa_data[] = array(
                    'puntovta' => $result['puntovta'],
                    'descripcion' => $result['descripcion'],
                    'checked' => (isset($vendedor_ptovta) ? $vendedor_ptovta : in_array($result['puntovta'], $request->get('puntosdevta', 'post', array())))
                );
            }
        }

        $view->set('puntosdevta', $puntosdeventa_data);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $template->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');
        $view->set('error_persona', @$this->error['persona']);
        $view->set('error_texto_error', @$this->error['texto_error']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $urlAction = $url->ssl('vendedores', $request->get('action'), array('persona' => $request->get('persona')));
        $view->set('action', $urlAction);

        $view->set('accion_form', $request->get('action'));
        $view->set('cancel', $url->ssl('vendedores'));
        // </editor-fold>

        return $view->fetch('content/vendedor.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $common = & $this->locator->get('common');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        if ((strlen($request->get('persona', 'post')) == 0)) {
            $this->error['persona'] = 'Debe ingresar el n&uacute;mero de documento';
            $errores .= 'Debe ingresar el n&uacute;mero de documento. <br>';
        }

        if (strlen($request->get('persona', 'post')) < 7 || !is_numeric($request->get('persona', 'post'))) {
            $this->error['persona'] = 'No es un n&uacute;mero de documento v&aacute;lido.';
            $errores .= 'No es un n&uacute;mero de documento v&aacute;lido. <br>';
        }

        if (@$this->error['persona'] == '') {
            $sql = "SELECT count(persona) as total FROM personas WHERE persona ='?'";
            $persona = $database->getRow($database->parse($sql, $request->get('persona', 'post')));

            if ($persona['total'] > 0 && $request->get('accion_form', 'post') == 'insert') {
                $errores .= 'Esta persona ya existe en el sistema. <br>';
            }
        }

        if ((strlen($request->get('apellido', 'post')) < 2) || (strlen($request->get('apellido', 'post')) > 100)) {
            $errores .= 'Debe ingresar el apellido. <br>';
        }

        if ((strlen($request->get('nombre', 'post')) < 2) || (strlen($request->get('nombre', 'post')) > 100)) {
            $errores .= 'Debe ingresar el nombre. <br>';
        }

//		if ((strlen($request->get('domicilio', 'post')) == 0)) {
//			$this->error['domicilio'] = 'Debe ingresar el domicilio';
//                        $this->error['tab_general'] = '1';
//		}
//
//
//		if ((strlen($request->get('telefono', 'post')) == 0)) {
//			$this->error['telefono'] = 'Debe ingresar el tel&eacute;fono';
//                        $this->error['tab_general'] = '1';
//		}
//
        if ((strlen($request->get('celular', 'post')) == 0)) {
            $errores .= 'Debe ingresar el celular. <br>';
        }
//
//		if (!$mail->validarDuplicado($request->get('mail', 'post'),$database,$mensaje,$request->get('persona', 'post'))) {
//			$this->error['mail'] = $mensaje;
//                        $this->error['tab_general'] = '1';
//		}              
//		if (!$common->validarDuplicado($request->get('mail', 'post'),$database,$mensaje,$request->get('persona', 'post'))) {
//			$this->error['mail'] = $mensaje;
//                        $this->error['tab_general'] = '1';
//		}              


        if (count($request->get('puntosdevta', 'post', array())) == 0) {
            $errores .= 'Debe seleccionar al menos un punto de venta. <br>';
        }

        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function validateFormUpdate() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $common = & $this->locator->get('common');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        if ((strlen($request->get('apellido', 'post')) < 2) || (strlen($request->get('apellido', 'post')) > 100)) {
            $errores .= 'Debe ingresar el apellido. <br>';
        }

        if ((strlen($request->get('nombre', 'post')) < 2) || (strlen($request->get('nombre', 'post')) > 100)) {
            $errores .= 'Debe ingresar el nombre. <br>';
        }

//		if ((strlen($request->get('domicilio', 'post')) == 0)) {
//			$this->error['domicilio'] = 'Debe ingresar el domicilio';
//                        $this->error['tab_general'] = '1';
//		}
//
//
//		if ((strlen($request->get('telefono', 'post')) == 0)) {
//			$this->error['telefono'] = 'Debe ingresar el tel&eacute;fono';
//                        $this->error['tab_general'] = '1';
//		}
//
//		if ((strlen($request->get('celular', 'post')) == 0)) {
//			$this->error['celular'] = 'Debe ingresar el celular';
//                        $this->error['tab_general'] = '1';
//		}
//
//		if (!$mail->validarDuplicado($request->get('mail', 'post'),$database,$mensaje,$request->get('persona', 'post'))) {
//			$this->error['mail'] = $mensaje;
//                        $this->error['tab_general'] = '1';
//		}              
//		if (!$common->validarDuplicado($request->get('mail', 'post'),$database,$mensaje,$request->get('persona', 'post'))) {
//			$this->error['mail'] = $mensaje;
//                        $this->error['tab_general'] = '1';
//		}              
//                if ($request->get('remail', 'post') != $request->get('mail', 'post')) {
//			$this->error['remail'] = 'El mail y la confirmacion no coinciden';
//                        $this->error['tab_general'] = '1';
//                        
//		}

        if (count($request->get('puntosdevta', 'post', array())) == 0) {
            $errores .= 'Debe seleccionar al menos un punto de venta. <br>';
        }

        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $common = & $this->locator->get('common');
        // </editor-fold>

        $template->set('title', 'Vendedores');

        if (($request->isPost()) && ($this->validateForm())) {
            $id = $request->get('persona', 'post');

            // <editor-fold defaultstate="collapsed" desc="FECHA">
            $fecha_d = '';

            if ($request->get('fechanacimiento', 'post') != '') {
                $dated = explode('/', $request->get('fechanacimiento', 'post'));
                $fecha_d = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
            } else {
                $fecha_d = NULL;
            }

            // </editor-fold>

            $clave = $common->getEcriptar($id);

            if ($request->get('vendedorpropio', 'post') == 'vendedorpropio') {
                $vendedorpropio = 1;
            } else {
                $vendedorpropio = 0;
            }

            if ($request->get('comisiona', 'post') == 'comisiona') {
                $comision = 1;
            } else {
                $comision = 0;
            }
            $depende = $id;
            if ($request->get('dependenciapersona') != '-1') {
                $depende = $request->get('dependenciapersona', 'post');
            }

            $sql = "INSERT INTO personas SET persona = '?',clave ='?', nombre ='?', apellido ='?', direccion = '?', numero = '?',  piso='?', departamento='?', localidad='?', fechanacimiento = NULLIF('?',''),celular ='?',  mail ='?', tipopersona='V',tipoiva='?',tipovendedor='?',zona='?',comisiona='?',dependenciapersona='?' ,au_usuario='?',au_accion='A',au_fecha_hora=NOW() ";
            $sql = $database->parse($sql, $id, $clave, strtoupper($request->get('nombre', 'post')), strtoupper($request->get('apellido', 'post')), $request->get('direccion', 'post'), $request->get('numero', 'post'), $request->get('piso', 'post'), $request->get('departamento', 'post'), $request->get('auto_localidad_vendedor', 'post'), $fecha_d, $request->get('celular', 'post'), $request->get('mail', 'post'), $request->get('tipoiva', 'post'), $request->get('tipovendedor', 'post'), $request->get('zona', 'post'), $comision, $depende, $user->getPERSONA());
            $database->query($sql);

            //PUNTOS DE VENTA
            $puntosdeventa = $request->get('puntosdevta', 'post', array());

            foreach ($puntosdeventa as $puntovta) {
                $sql = "INSERT IGNORE INTO vendedorespuntodeventa SET persona = '?', puntovta = '?'";
                $database->query($database->parse($sql, $id, $puntovta));
            }

            //GRUPO
            $sql = "INSERT IGNORE INTO personasgrupos SET persona = '?', grupo = 'VE', usuario = '?'";
            $database->query($database->parse($sql, $id, $user->getPERSONA()));

            $cache->delete('vendedores');
            $session->set('message', 'Se agreg&oacute; el vendedor: ' . $id);

            $response->redirect($url->ssl('vendedores'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        $template->set('title', 'Personas');

        if (($request->isPost()) && ($this->validateFormUpdate())) {
            $id = $request->get('persona', 'post');

            // <editor-fold defaultstate="collapsed" desc="FECHA">
            $fecha_d = '';
            if ($request->get('fechanacimiento', 'post') != '') {
                $dated = explode('/', $request->get('fechanacimiento', 'post'));
                $fecha_d = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
                $categoria = date('Y', strtotime($request->get('fechanacimiento', 'post')));
            } else {
                $fecha_d = NULL;
            }

            $fecha_i = '';

            // </editor-fold>

            if ($request->get('vendedorpropio', 'post') == 'vendedorpropio') {
                $vendedorpropio = 1;
            } else {
                $vendedorpropio = 0;
            }

            if ($request->get('comisiona', 'post') == 'comisiona') {
                $comision = 1;
            } else {
                $comision = 0;
            }
            $depende = $id;
            if ($request->get('dependenciapersona', 'post') != '-1') {
                $depende = $request->get('dependenciapersona', 'post');
            }

            $puntosdeventa = $request->get('puntosdevta', 'post', array());

            $sql = "UPDATE personas SET nombre ='?', apellido ='?', direccion = '?', numero = '?',  piso='?', departamento='?', localidad='?', fechanacimiento = NULLIF('?',''),celular ='?',  mail ='?',tipoiva='?',tipovendedor='?',zona='?',comisiona='?',dependenciapersona='?' ,au_usuario='?',au_accion='M',au_fecha_hora=NOW() WHERE persona = '?' ";
            $sql = $database->parse($sql, strtoupper($request->get('nombre', 'post')), strtoupper($request->get('apellido', 'post')), $request->get('direccion', 'post'), $request->get('numero', 'post'), $request->get('piso', 'post'), $request->get('departamento', 'post'), $request->get('auto_localidad_vendedor', 'post'), $fecha_d, $request->get('celular', 'post'), $request->get('mail', 'post'), $request->get('tipoiva', 'post'), $request->get('tipovendedor', 'post'), $request->get('zona', 'post'), $comision, $depende, $user->getPERSONA(), $id);
            $database->query($sql);

            $sql = "DELETE FROM vendedorespuntodeventa WHERE persona = '?'";
            $database->query($database->parse($sql, $id));

            foreach ($puntosdeventa as $puntovta) {
                $sql = "INSERT IGNORE INTO vendedorespuntodeventa SET persona = '?', puntovta = '?'";
                $database->query($database->parse($sql, $id, $puntovta));
            }

            $cache->delete('vendedores');
            $session->set('message', 'Se actualiz&oacute; el vendedor: ' . $id);

            $response->redirect($url->ssl('vendedores'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        $template->set('title', 'Vendedores');

        if (($request->isPost())) {

            $response->redirect($url->ssl('vendedores'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        if (($request->get('persona')) && ($this->validateDelete())) {

            $id = $request->get('persona', 'get');

            //TODO: FALTA PROGRAMAR LA ELIMINACION Y VALIDACION VER SI NO HAY RELACIONES
            //$sql = "DELETE FROM  WHERE  = '" . $id . "' ";
            //$database->query($sql);

            $cache->delete('vendedores');

            $session->set('message', 'Se ha eliminado el vendedor: ' . $id);

            $response->redirect($url->ssl('vendedores'));
        }

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';

        if ($errores != '') {
            $this->error['message'] = $errores;
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function exportar() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $session = & $this->locator->get('session');
        // </editor-fold>

        set_time_limit(0);

        $filtro = 'Filtro:';

        $htmlTable = "<table border=0 width=180> 
			<tr><td colspan=6 align=center style=bold size=14>" . utf8_decode("xxxxxxxxx xxxxxxxxx xxxxxxx") . "</td></tr>
			<tr><td colspan=6 align=center style=bold size=12>SISTEMA DE xxxxxxxxxxx xxxxxxxxx</td></tr>
			<tr><td colspan=6 align=center style=bold size=10>Listado de personas</td></tr>
			<tr><td colspan=6 align=center></td></tr>
			<tr></tr>";

        if (!$session->get('vendedores.persona') && !$session->get('vendedores.nombre')) {
            $sql = "SELECT p.persona, p.nombre, p.domicilio, l.descripcion,  p.celular, p.mail FROM personas p LEFT JOIN localidades l ON p.localidad = l.localidad";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT p.persona, p.nombre, p.domicilio, l.descripcion,  p.celular, p.mail FROM personas p LEFT JOIN localidades l ON p.localidad = l.localidad WHERE p.persona LIKE '%" . $session->get('vendedores.persona') . "%' AND p.nombre LIKE '%" . $session->get('vendedores.nombre') . "%'";
            $conca = " AND ";
        }

        if ($session->get('vendedores.localidad') != '-1' && $session->get('vendedores.localidad') != '') {
            $sql .= $conca . "p.localidad = '" . $session->get('vendedores.localidad') . "'";
            $conca = " AND ";
        }

        $sql .= " ORDER BY persona ASC";

        $reporte = $database->getRows($sql);

        foreach ($reporte as $estu) {

            $htmlTable .= "<tr><td colspan=6>______________________________________________________________________________________________________________</td></tr>
                                        <tr>
                                                <td align=left size=9>Documento</td>
                                                <td align=left size=9>" . utf8_decode($estu['persona']) . "</td>
                                        </tr>
                                        <tr>
                                                <td align=left size=9>Nombre</td>	
                                                <td align=left size=9 colspan= 5>" . utf8_decode($estu['nombre']) . "</td>
                                        </tr>
                                        <tr>
                                                <td align=left size=9>Domicilio<td>
                                                <td align=left size=9 colspan=2>" . utf8_decode($estu['domicilio']) . "</td>
                                                <td align=left size=9>Localidad</td>
                                                <td align=left size=9>" . utf8_decode($estu['descripcion']) . "</td>
                                        </tr>
                                        <tr>
                                                
                                                <td align=left size=9>Celular:</td>
                                                <td align=left size=9>" . utf8_decode($estu['celular']) . "</td>
                                                <td align=left size=9></td>
                                                <td align=left size=9></td>
                                        </tr>
                                        <tr>	
                                                <td align=left size=9>Email</td>
                                                <td align=left size=9 colspan=5>" . utf8_decode($estu['mail']) . "</td>
                                        </tr>";
        }

        $htmlTable .= "</table>";

        define('FPDF_FONTPATH', 'font/');
        require('library/pdf/pdftable.inc.php');
        //ob_end_clean();

        $p = new PDFTable();
        $p->AliasNbPages();
        $p->AddPage();
        $p->setfont('times', '', 6);
        $p->htmltable($htmlTable);
        $p->output('', 'I');
    }

    function getVendedor() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>		

        $miPersona = $request->get('term', 'get');

        $sql = "SELECT p.persona as id, p.nombre AS value "
                . "FROM personas p "
                . "WHERE (p.persona LIKE '?' OR p.nombre LIKE '?') "
                . "AND p.tipopersona = 'V'  ";
        $consulta = $database->parse($sql, '%' . $miPersona . '%', '%' . $miPersona . '%');

        $codigo = $database->getRows($consulta);

        echo json_encode($codigo);
    }

    function getLocalidad() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
// </editor-fold>

        $miDescripcion = $request->get('term', 'get');

        $sql = "SELECT localidad  as id, desclocalidad as value "
                . "FROM vw_grilla_localidades "
                . "WHERE descripcion LIKE '?' "
                . "LIMIT 10";
        $consulta = $database->parse($sql, '%' . $miDescripcion . '%');
        $codigo = $database->getRows($consulta);

        $varia = utf8_decode(json_encode($codigo));

        echo $varia;
    }

}

?>