<?php

class ControllerHome extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>					
        $template->set('title', 'Home');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        $language = & $this->locator->get('language');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $request = & $this->locator->get('request');
        $template = & $this->locator->get('template');
        $common = & $this->locator->get('common');
        // </editor-fold>

        $template->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        $template->set('message', $session->get('message'));
        $session->delete('message');

        $usuario = array('nombre' => $user->getNombreCompleto(),
            'descripcionGrupo' => $user->getNombresGrupos(),
            'puntovtaasignado' => $user->getPuntovtaasignado(),
            'descpuntovtaasignado' =>  $user->getDescpuntovtaasignado()
        );
        $template->set('usuario', $usuario);

        return $view->fetch('content/home.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">
        if ($request->has('search', 'post')) {
            $session->set('home.search', $request->get('search', 'post'));
        }

        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('home.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('home.order', (($session->get('home.sort') == $request->get('sort', 'post')) && ($session->get('home.order') == 'asc')) ? 'desc' : 'asc');
        }

        if ($request->has('sort', 'post')) {
            $session->set('home.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('home'));
    }

}

?>
