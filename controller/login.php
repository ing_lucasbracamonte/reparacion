<?php

class ControllerLogin extends Controller {

    var $error = array();

    function index() {
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        $user = & $this->locator->get('user');
        $session = & $this->locator->get('session');
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');

        $view = $this->locator->create('template');

        $language->load('controller/login.php');

        $template->set('title', $language->get('heading_title'));

        if ($user->isLogged()) {
            //VALIDACION QUE PIDE ACTUALIZAR PUNTO DE VENTA
            //es vendedor y es la primera vez que se loguea en el dia
                $consulta = "SELECT  count(*) as total  FROM personas p "
                        . "LEFT JOIN personasgrupos pg ON p.persona = pg.persona  "
                        . "LEFT JOIN vendedorespuntodeventa vpv ON vpv.persona = p.persona "
                        . "WHERE p.persona = '?' AND pg.grupo = 'VE' ";
                $valida = $database->getRow($database->parse($consulta, $user->getPERSONA()));
                
                
                if ($valida['total'] >= 2 || $user->getPuntovtaasignado() == '-1' || $user->getPuntovtaasignado() == null || $user->getPuntovtaasignado() == '') {
                    $response->redirect($url->ssl('updatepuntovta'));
                }
                else {
                     $response->redirect($url->ssl(DEFAULT_HOMEPAGE));
                }  
           
        }

        if ($request->isPost() && $this->validate()) {

            if ($session->has('nexturl')) {
                $redirect = $session->get('nexturl');
                $session->delete('nexturl');
            } else {
                $redirect = $url->ssl(DEFAULT_HOMEPAGE);
            }

            $response->redirect($redirect);
        }

        // $view = $this->locator->create('template');

        $view->set('heading_title', $language->get('heading_title'));
        $view->set('heading_description', $language->get('heading_description'));

        $view->set('entry_user', $language->get('entry_user'));
        $view->set('entry_password', $language->get('entry_password'));

        $view->set('button_login', $language->get('button_login'));

        $template->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        $template->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('action', $url->ssl('login'));

        $template->set('content', $view->fetch('content/seguridad_login.tpl'));

        $template->set($module->fetch());

        $response->set($template->fetch('layoutlogin.tpl'));
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">
        if ($request->has('search', 'post')) {
            $session->set('login.search', $request->get('search', 'post'));
        }

        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('login.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('login.order', (($session->get('login.sort') == $request->get('sort', 'post')) && ($session->get('login.order') == 'asc')) ? 'desc' : 'asc');
        }

        if ($request->has('sort', 'post')) {
            $session->set('login.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('login'));
    }

    function validate() {
        $request = & $this->locator->get('request');
        $language = & $this->locator->get('language');
        $user = & $this->locator->get('user');

        if (!$user->login($request->get('username', 'post'), $request->get('password', 'post'))) {
            $this->error['message'] = $language->get('error_login');
        }

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function isLogged() {
        $user = & $this->locator->get('user');
        $url = & $this->locator->get('url');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $database = & $this->locator->get('database');

        $publico = false;

//                if($session->has('paginasPublicas')){
//                    $paginasPublicas = $session->get('paginasPublicas');
//                }else{
//                    $sql = 'SELECT * FROM paginaspublicas WHERE activa = 1';
//                    $paginasPublicas = $database->getRows($sql);
//                }                    
//                foreach($paginasPublicas as $pagPublica){
//                    if($pagPublica['controladora'] == $request->get('controller', 'get')){
//                        $publico = true;
//                    }
//                }

        $esSU = false;
        $grupos = $user->getGrupos();
        foreach ($grupos as $unGrupo) {
            if ($unGrupo['Grupo'] == 'SU') {
                $esSU = true;
            }
        }

        if (!$esSU) {
            if (!$publico) {
                if ($request->has('controller')) {
                    $controladora = $request->get('controller');
                } else {
                    $controladora = DEFAULT_HOMEPAGE;
                }

                if ($request->has('action')) {
                    $metodo = strtolower($request->get('action'));
                } else {
                    $metodo = 'index';
                }

                switch ($metodo) {
                    case 'insert':
                        $permiso = 'A';
                        break;
                    case 'update':
                        $permiso = 'M';
                        break;
                    case 'delete':
                        $permiso = 'B';
                        break;
                    default:
                        $permiso = 'C';
                        break;
                }

                if ($user->isLogged()) {
                    if ($user->hasPermisos($user->getPersona(), $controladora, $permiso) == false) {
                        $session->set('error', 'No tiene permisos suficientes para realizar esa acci&oacute;n.');
                        return $this->forward(DEFAULT_HOMEPAGE, 'index');
                    }
                } else {
                    $tex = $url->get_server() . '?' . $_SERVER['QUERY_STRING'];
                    $session->set('nexturl', $url->get_server() . '?' . $_SERVER['QUERY_STRING']);
                    return $this->forward('login', 'index');
                }
            }
        }
    }

    function recuperarPassword() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $common = & $this->locator->get('common');

        // </editor-fold>

        if ($request->isPost()) {
            $persona_id = strtoupper(trim($request->get('persona', 'post')));
            $sql = "SELECT persona, nombre, apellido, creacion, mail FROM personas WHERE persona = '?'";
            $results = $database->getRow($database->parse($sql, $persona_id));

            if (trim(strtolower($results['mail'])) == trim(strtolower($request->get('mail', 'post')))) {
                $url_recuperar = $url->rawssl('login', 'updatePassword', array('persona' => $persona_id, 'token' => sha1($persona_id . $results['creacion'])));
                $common->enviarMailRecuperarPassword($results['nombre'], $request->get('mail', 'post'), $url_recuperar);
                $session->set('message', 'Se ha enviado un e-mail con instrucciones para reestablecer la contrase&ntilde;a a la cuenta ingresada en el registro de la plataforma. Si no llega en unos minutos, revisa el correo no deseado.');
            } else {
                $session->set('error', 'No existen registros para ese documento y mail.');
            }

            die('ok');
        }

        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('entry_persona', 'Documento:');
        $view->set('entry_mail', 'E-mail:');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">

        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $template->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');
        $view->set('error_persona', @$this->error['persona']);
        $view->set('error_mail', @$this->error['mail']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', '#');
        // </editor-fold>

        $response->set($view->fetch('content/recuperar_password.tpl'));
    }

    function updatePassword() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $common = & $this->locator->get('common');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Recuperar Contrase&ntilde;a');

        if ($request->has('persona', 'get') && $request->has('token', 'get')) {

            $persona_id = strtoupper($request->get('persona', 'get'));

            $sql = "SELECT persona, nombre, creacion, mail_confirmado FROM personas WHERE persona = '?'";
            $results = $database->getRow($database->parse($sql, $persona_id));

            if (sha1($results['persona'] . $results['creacion']) == $request->get('token', 'get')) {
                $sql = "UPDATE personas SET clave = '?', mail_confirmado = 1, usuario = '?' WHERE persona = '?'";
                $database->query($database->parse($sql, $common->getEcriptar(substr(md5($results['nombre']), 0, 8)), $persona_id, $persona_id));
                $session->set('message', '¡Se cambió la contrase&ntilde;a! Podés cambiarla modificando tu perfil.');
            } elseif ($results['mail_confirmado'] == 1) {
                $session->set('message', 'Usted ya valid&oacute; su cuenta!');
            } else {
                $session->set('error', 'Los datos son inv&aacute;lidos!');
            }
        }

        $response->redirect($url->ssl(DEFAULT_HOMEPAGE));
    }

}

?>