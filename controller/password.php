<?php

class ControllerPassword extends Controller {

    var $error = array();

    function index() {
        $response = & $this->locator->get('response');
        $language = & $this->locator->get('language');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');

        $language->load('controller/password.php');

        $template->set('title', $language->get('heading_title'));

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $user = & $this->locator->get('user');
        $common = & $this->locator->get('common');
        $language->load('controller/password.php');

        $template->set('title', $language->get('heading_title'));

        if (($request->isPost()) && ($this->validateForm())) {
            $clave = $common->getEcriptar($request->get('password', 'post'));
            $sql = "UPDATE personas SET clave = '?', usuario = '?' WHERE persona = '?'";
            $database->query($database->parse($sql, $clave, $user->getPERSONA(), $request->get('nro_doc', 'get')));

            $cache->delete('password');

            $session->set('message', "Se ha actualizado la contrase&ntilde;a");

            $response->redirect($url->ssl('home'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $template = & $this->locator->get('template');
        $user = & $this->locator->get('user');
        $session = & $this->locator->get('session');

        $language->load('controller/password.php');
        $template = & $this->locator->get('template');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('entry_newpassword', $language->get('entry_newpassword'));
        $view->set('entry_newpasswordagain', $language->get('entry_newpasswordagain'));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $template->set('title', $language->get('heading_title'));

        $view->set('heading_title', $language->get('heading_title'));
        $view->set('heading_description', 'Ingrese su nueva contrase&ntilde;a');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $template->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('button_aceptar', $language->get('button_aceptar'));
        $view->set('button_cancelar', $language->get('button_cancelar'));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $template->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('cancel', $url->ssl(DEFAULT_HOMEPAGE));
        $view->set('action', $url->ssl('password', 'update', array('nro_doc' => $user->getPERSONA())));
        // </editor-fold>

        return $view->fetch('content/seguridad_password.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        if ((strlen($request->get('password', 'post')) < 1)) {
            $this->error['message'] = "Debe ingresar una contrase&ntilde;a";
        }

        if (strlen($request->get('password', 'post')) != strlen($request->get('newpassword', 'post'))) {
            $this->error['message'] = "Las contrase&ntilde;as no coinciden";
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

?>
