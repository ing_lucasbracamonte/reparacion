<?php

class ControllerFinanciaciones extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'Financiaciones');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
        $request = & $this->locator->get('request');
        // </editor-fold>
         // <editor-fold defaultstate="collapsed" desc="LIMPIA FILTRO Y VARIBLES DE SESSION">

        if ($request->get('filtra') == NULL) {

            $session->set('financiaciones.search', '');
            $session->set('financiaciones.sort', '');
            $session->set('financiaciones.order', '');
            $session->set('financiaciones.page', '');

            $view->set('search', '');
            $view->set('financiaciones.search', '');
                        
            $cache->delete('financiaciones');
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">
        $cols = array();

        $cols[] = array(
            'name' => 'Financiación',
            'sort' => 'persona',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Descripción',
            'sort' => 'descripcion',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Forma de pago',
            'sort' => 'descformapago',
            'align' => 'left'
        );
        
        $cols[] = array(
            'name' => 'Tarjeta',
            'sort' => 'desctarjeta',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Banco',
            'sort' => 'descbanco',
            'align' => 'left'
        );


        $cols[] = array(
            'name' => 'Cuotas',
            'sort' => 'cuotas',
            'align' => 'left'
        );
        
        $cols[] = array(
            'name' => 'Recargo %',
            'sort' => 'recargo',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Estado',
            'sort' => 'estado',
            'align' => 'left'
        );
        
        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'descripcion',
            'descformapago',
            'desctarjeta',
            'descbanco',
            'recargo',
            'cuotas'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        if (!$session->get('financiaciones.search')) {
            $sql = "SELECT * FROM vw_list_financiaciones  ";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT * FROM vw_list_financiaciones WHERE descripcion LIKE '?' OR desctarjeta LIKE '?' OR descbanco LIKE '?' OR descformapago LIKE '?' ";
            $conca = " AND ";
        }

        if (in_array($session->get('financiaciones.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('financiaciones.sort') . " " . (($session->get('financiaciones.order') == 'DESC') ? 'DESC' : 'ASC');
        } else {
            $sql .= " ORDER BY descripcion ASC";
        }

        $consult = $database->parse($sql, '%' . $session->get('financiaciones.search') . '%', '%' . $session->get('financiaciones.search') . '%', '%' . $session->get('financiaciones.search') . '%', '%' . $session->get('financiaciones.search') . '%');
        $results = $database->getRows($consult);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['financiacion'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['descripcion'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['descformapago'],
                'align' => 'left',
                'default' => 0
            );
            
            $desctarjeta = "-";
            if (@$result['desctarjeta'])
                $desctarjeta = @$result['desctarjeta'];
            $cell[] = array(
                'value' => $desctarjeta,
                'align' => 'left',
                'default' => 0
            );

            $descbanco = "-";
            if (@$result['descbanco'])
                $descbanco = @$result['descbanco'];
            $cell[] = array(
                'value' => $descbanco,
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['cuotas'],
                'align' => 'left',
                'default' => 0
            );
            
            $cell[] = array(
                'value' => @$result['recargo'],
                'align' => 'left',
                'default' => 0
            );
            
            $estado = "ACTIVO";
            if(@$result['estado'])
                $estado = @$result['estado'];
            $cell[] = array(
                    'value'   => $estado,
                    'align'   => 'left',
                    'default' => 0
            );
            
            $action = array();

            if ($user->hasPermisos($user->getPERSONA(), 'financiaciones', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa fa-fw fa-pencil',
                    'text' => $language->get('button_update'),
                    'prop_a' => array('href' => $url->ssl('financiaciones', 'update', array('financiacion' => $result['financiacion'])))
                );
            }

            if ($estado == "ACTIVO" )
                        {
                           if ($user->hasPermisos($user->getPERSONA(),'financiaciones','B')) {
                            $action[] = array(
                                    'icon' => 'img/iconos-11.png',
                                    'text' => 'Desactivar',
                                    'class' => 'fa fa-fw fa-thumbs-o-down',
                                    'prop_a' => array('href' => $url->ssl('financiaciones', 'deleteLogico', array('financiacion' => $result['financiacion'])))
                            );
                            }
                        }
                        else {
                            if ($user->hasPermisos($user->getPERSONA(),'financiaciones','B')) {
                            $action[] = array(
                                    'icon' => 'img/iconos-11.png',
                                    'text' => 'Reactivar',
                                    'class' => 'fa fa-fw fa-thumbs-o-up',
                                    'prop_a' => array('href' => $url->ssl('financiaciones', 'reactivarLogico', array('financiacion' => $result['financiacion'])))
                            );
                            }
                        }
            $consulta_info = $database->getRow("SELECT COUNT(*) as total FROM ventaformasdepago WHERE financiacion = '" . $request->get('financiacion') . "'");
        
             if ($user->getExento() == 1 && @$consulta_info['total'] == 0) 
                        {
                           if ($user->hasPermisos($user->getPERSONA(),'financiaciones','B')) {
                            $action[] = array(
                                    'icon' => 'img/iconos-11.png',
                                    'text' => $language->get('button_delete'),
                                    'class' => 'fa fa-fw fa-trash-o',
                                    'prop_a' => array('href' => $url->ssl('financiaciones', 'delete', array('financiacion' => $result['financiacion'])))
                            );
                            }
                        }
                        
            if ($user->hasPermisos($user->getPERSONA(), 'financiaciones', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw fa-search',
                    'text' => $language->get('button_consult'),
                    'prop_a' => array('href' => $url->ssl('financiaciones', 'consulta', array('financiacion' => $result['financiacion'])))
                );
            }

            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('financiaciones.page'));

        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        if ($session->get('financiaciones.financiacion')) {
            $view->set('persona', $session->get('financiaciones.financiacion'));
        } else {
            $view->set('persona', '');
        }

        $view->set('heading_title', 'FINANCIACIONES');
        $view->set('heading_title2', 'Filtrar');
        $view->set('heading_description', 'Gesti&oacute;n de Financiaciones');

        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/NOTICIAS.png');
        $view->set('placeholder_buscar', 'BUSCA POR DESCRIPCIÓN O FORMA DE PAGO O TARJETA O BANCO');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('financiaciones.search'));
        $view->set('sort', $session->get('financiaciones.sort'));
        $view->set('order', $session->get('financiaciones.order'));
        $view->set('page', $session->get('financiaciones.page'));

        $view->set('cols', $cols);
        $view->set('rows', $rows);
        
        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('financiaciones', 'page'));
        $view->set('list', $url->ssl('financiaciones'));
        if ($user->hasPermisos($user->getPERSONA(), 'financiaciones', 'A')) {
            $view->set('insert', $url->ssl('financiaciones', 'insert'));
        }
        //$view->set('listaMail', $url->ssl('financiaciones', 'listaMail'));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_financiaciones.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">

        if ($request->has('search', 'post')) {
            $session->set('financiaciones.search', $request->get('search', 'post'));
        }

        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('financiaciones.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('financiaciones.order', (($session->get('financiaciones.sort') == $request->get('sort', 'post')) && ($session->get('financiaciones.order') == 'ASC')) ? 'DESC' : 'ASC');
        }

        if ($request->has('sort', 'post')) {
            $session->set('financiaciones.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('financiaciones', 'index', array('filtra' => '1')));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $language = & $this->locator->get('language');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('entry_descripcion', 'Descripción:');
        $view->set('entry_tarjeta', 'Tarjeta:');
        $view->set('entry_banco', 'Banco:');
        $view->set('entry_cuotas', 'Cuota:');
        $view->set('entry_recargo', 'Recargo:');
        $view->set('entry_formapago', 'Forma de pago:');
       
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('heading_title', 'Financiaciones');
        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/user-8.png');

        $view->set('heading_description', 'Gesti&oacute;n de financiación');

        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        $view->set('tab_general', $language->get('tab_general'));

        if (($request->get('financiacion')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM financiaciones WHERE financiacion = '?'";
            $objeto_info = $database->getRow($database->parse($consulta, $request->get('financiacion')));
        }

        if ($request->has('financiacion', 'post')) {
            $view->set('financiacion', $request->get('financiacion', 'post'));
            $objeto_id = $request->get('financiacion', 'post');
        } else {
            $view->set('financiacion', @$objeto_info['financiacion']);
            $objeto_id = @$objeto_info['financiacion'];
        }

        if ($request->has('descripcion', 'post')) {
            $view->set('descripcion', $request->get('descripcion', 'post'));
        } else {
            $view->set('descripcion', @$objeto_info['descripcion']);
        }

        if ($request->has('cuotas', 'post')) {
            $view->set('cuotas', $request->get('cuotas', 'post'));
        } else {
            $view->set('cuotas', @$objeto_info['cuotas']);
        }
        
        if ($request->has('recargo', 'post')) {
            $view->set('recargo', $request->get('recargo', 'post'));
        } else {
            $view->set('recargo', @$objeto_info['recargo']);
        }

        if ($request->has('formapago', 'post')) {
            $view->set('formapago', $request->get('formapago', 'post'));
        } else {
            $view->set('formapago', @$objeto_info['formapago']);
        }
        $view->set('formaspago', $database->getRows("SELECT * FROM formaspago WHERE estado = 1 ORDER BY descripcion  ASC"));
        
        if ($request->has('tarjeta', 'post')) {
            $view->set('tarjeta', $request->get('tarjeta', 'post'));
        } else {
            $view->set('tarjeta', @$objeto_info['tarjeta']);
        }
        $view->set('tarjetas', $database->getRows("SELECT * FROM tarjetas ORDER BY descripcion ASC"));


         if ($request->has('banco', 'post')) {
            $view->set('banco', $request->get('banco', 'post'));
        } else {
            $view->set('banco', @$objeto_info['banco']);
        }
        $view->set('bancos', $database->getRows("SELECT * FROM bancos ORDER BY descripcion ASC"));
        
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');
        $view->set('error_financiacion', @$this->error['financiacion']);

        $view->set('error_texto_error', @$this->error['texto_error']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $urlAction = $url->ssl('financiaciones', $request->get('action'), array('financiacion' => $request->get('financiacion')));
        $view->set('action', $urlAction);
        $view->set('accion_form', $request->get('action'));
        $view->set('cancel', $url->ssl('financiaciones'));

        // </editor-fold>

        return $view->fetch('content/financiacion.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $common = & $this->locator->get('common');
        $upload = & $this->locator->get('upload');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        if ((strlen($request->get('descripcion', 'post')) == 0)) {
            $errores .= 'Debe ingresar la descripci&oacute;n. <br>';
        }

        if ((strlen($request->get('recargo', 'post')) == 0)) {
            $errores .= 'Debe ingresar el recargo. <br>';
        }

        if ((strlen($request->get('cuotas', 'post')) == 0)) {
            $errores .= 'Debe ingresar la cantidad de cuotas. <br>';
        }
   
        if ($request->get('formapago', 'post') == '-1') {
            $errores .= 'Debe seleccionar una forma de pago. <br>';
        }

        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $common = & $this->locator->get('common');
        
        // </editor-fold>

        $template->set('title', 'Financiaciones');

        if (($request->isPost()) && ($this->validateForm())) {
            $id = strtoupper($request->get('persona', 'post'));

            $sql = "INSERT INTO financiaciones SET descripcion = '?',formapago ='?',tarjeta ='?', banco ='?', cuotas ='?', recargo='?', estado = 'ACTIVO',  au_usuario='?',au_accion='A',au_fecha_hora=NOW()";
            $sql = $database->parse($sql, strtoupper($request->get('descripcion', 'post')), $request->get('formapago', 'post'),$request->get('tarjeta', 'post'), $request->get('banco', 'post'), $request->get('cuotas', 'post'), $request->get('recargo', 'post'), $user->getPERSONA());
            $database->query($sql);
            $id = $database->getLastId();

            $cache->delete('financiaciones');
            $session->set('message', 'Se agreg&oacute; la financiación: ' . $id);

            $response->redirect($url->ssl('financiaciones'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $upload = & $this->locator->get('upload');
        // </editor-fold>

        $template->set('title', 'Financiaciones');

        if (($request->isPost()) && ($this->validateForm())) {

            $id = $request->get('financiacion', 'post');

            $sql = "UPDATE financiaciones SET descripcion = '?',formapago='?',tarjeta ='?', banco ='?', cuotas ='?', recargo='?', estado = 'ACTIVO',  au_usuario='?',au_accion='M',au_fecha_hora=NOW() WHERE financiacion = '?'";
            $consult = $database->parse($sql, strtoupper($request->get('descripcion', 'post')), $request->get('formapago', 'post'),$request->get('tarjeta', 'post'), $request->get('banco', 'post'), $request->get('cuotas', 'post'), $request->get('recargo', 'post'), $user->getPERSONA(), $id);
            $database->query($consult);

            $cache->delete('financiaciones');
            $session->set('message', 'Se actualiz&oacute; la financiación: ' . $id);

            $response->redirect($url->ssl('financiaciones'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        $template->set('title', 'Clientes');

        if (($request->isPost())) {

            $response->redirect($url->ssl('financiaciones'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        if (($request->get('financiacion')) && ($this->validateDelete())) {

            $id = $request->get('financiacion', 'get');

            
            $sql = "DELETE FROM financiaciones WHERE financiacion = '?'";
            $database->query($database->parse($sql, $id));
           

            $cache->delete('financiaciones');

            $session->set('message', 'Se ha eliminado la financiación: ' . $id);

            $response->redirect($url->ssl('financiaciones'));
        }

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';

        $errores = '';
        $consulta_info = $database->getRow("SELECT COUNT(*) as total FROM ventaformasdepago WHERE financiacion = '" . $request->get('financiacion') . "'");
        if (@$consulta_info['total'] != 0) {
            $errores .= 'No es posible eliminar la financiación, ya fue asignado como mínimo a una venta. <br>';
        }
        
        if ($errores != '') {
            $this->error['message'] = $errores;
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function deleteLogico() {
            // <editor-fold defaultstate="collapsed" desc="INCLUDE">
		$request  =& $this->locator->get('request');
		$response =& $this->locator->get('response');
		$database =& $this->locator->get('database');
		$url      =& $this->locator->get('url');
		$user     =& $this->locator->get('user');
		$template =& $this->locator->get('template');
		$session  =& $this->locator->get('session');
		$module   =& $this->locator->get('module');
		$cache    =& $this->locator->get('cache');
            // </editor-fold>
		
		$template->set('title', 'Financiacion');

                //$persona = $request->get('persona');
		
                        // Hago update para auditar el delete
                        $sql = "UPDATE financiaciones SET estado = 'INACTIVO', au_usuario='?',au_fecha_hora=now(),au_accion='BL' WHERE financiacion = '?'";
			$sql = $database->parse($sql,$user->getPERSONA(),$request->get('financiacion','get'));
                        $database->query($sql);
                        
			$cache->delete('financiaciones');
			
			$session->set('message', 'Se ha dado de baja la financiación.');
			
			$response->redirect($url->ssl('financiaciones'));
		

		$template->set('content', $this->getList());

		$template->set($module->fetch());

		$response->set($template->fetch('layout.tpl'));
	}
        
    function reactivarLogico() {
            // <editor-fold defaultstate="collapsed" desc="INCLUDE">
		$request  =& $this->locator->get('request');
		$response =& $this->locator->get('response');
		$database =& $this->locator->get('database');
		$url      =& $this->locator->get('url');
		$user     =& $this->locator->get('user');
		$template =& $this->locator->get('template');
		$session  =& $this->locator->get('session');
		$module   =& $this->locator->get('module');
		$cache    =& $this->locator->get('cache');
                $common   =& $this->locator->get('common');
            // </editor-fold>
		
		$template->set('title', 'financiaciones');

                //$persona = $request->get('persona');
		
                        // Hago update para auditar el delete
               // $clave = $common->getEcriptar($request->get('persona','get'));
                        $sql = "UPDATE financiaciones SET estado = 'ACTIVO', au_usuario='?',au_fecha_hora=now(),au_accion='RL' WHERE financiacion = '?'";
			$sql = $database->parse($sql,$user->getPERSONA(),$request->get('financiacion','get'));
                        $database->query($sql);
                        
			$cache->delete('financiaciones');
			
			$session->set('message', 'Se ha reactivado la financiación.');
			
			$response->redirect($url->ssl('financiaciones'));
		

		$template->set('content', $this->getList());

		$template->set($module->fetch());

		$response->set($template->fetch('layout.tpl'));
	}
        
    function exportar() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $session = & $this->locator->get('session');
        // </editor-fold>

        set_time_limit(0);

        $filtro = 'Filtro:';

        $htmlTable = "<table border=0 width=180> 
			<tr><td colspan=6 align=center style=bold size=14>" . utf8_decode("xxxxxxxxx xxxxxxxxx xxxxxxx") . "</td></tr>
			<tr><td colspan=6 align=center style=bold size=12>SISTEMA DE xxxxxxxxxxx xxxxxxxxx</td></tr>
			<tr><td colspan=6 align=center style=bold size=10>Listado de personas</td></tr>
			<tr><td colspan=6 align=center></td></tr>
			<tr></tr>";

        if (!$session->get('financiaciones.persona') && !$session->get('financiaciones.nombre')) {
            $sql = "SELECT p.persona, p.nombre, p.domicilio, l.descripcion,  p.celular, p.mail FROM personas p LEFT JOIN localidades l ON p.localidad = l.localidad";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT p.persona, p.nombre, p.domicilio, l.descripcion,  p.celular, p.mail FROM personas p LEFT JOIN localidades l ON p.localidad = l.localidad WHERE p.persona LIKE '%" . $session->get('financiaciones.persona') . "%' AND p.nombre LIKE '%" . $session->get('financiaciones.nombre') . "%'";
            $conca = " AND ";
        }

        if ($session->get('financiaciones.localidad') != '-1' && $session->get('financiaciones.localidad') != '') {
            $sql .= $conca . "p.localidad = '" . $session->get('financiaciones.localidad') . "'";
            $conca = " AND ";
        }

        $sql .= " ORDER BY persona ASC";

        $reporte = $database->getRows($sql);

        foreach ($reporte as $estu) {

            $htmlTable .= "<tr><td colspan=6>______________________________________________________________________________________________________________</td></tr>
                                        <tr>
                                                <td align=left size=9>Documento</td>
                                                <td align=left size=9>" . utf8_decode($estu['persona']) . "</td>
                                        </tr>
                                        <tr>
                                                <td align=left size=9>Nombre</td>	
                                                <td align=left size=9 colspan= 5>" . utf8_decode($estu['nombre']) . "</td>
                                        </tr>
                                        <tr>
                                                <td align=left size=9>Domicilio<td>
                                                <td align=left size=9 colspan=2>" . utf8_decode($estu['domicilio']) . "</td>
                                                <td align=left size=9>Localidad</td>
                                                <td align=left size=9>" . utf8_decode($estu['descripcion']) . "</td>
                                        </tr>
                                        <tr>
                                                
                                                <td align=left size=9>Celular:</td>
                                                <td align=left size=9>" . utf8_decode($estu['celular']) . "</td>
                                                <td align=left size=9></td>
                                                <td align=left size=9></td>
                                        </tr>
                                        <tr>	
                                                <td align=left size=9>Email</td>
                                                <td align=left size=9 colspan=5>" . utf8_decode($estu['mail']) . "</td>
                                        </tr>";
        }

        $htmlTable .= "</table>";

        define('FPDF_FONTPATH', 'font/');
        require('library/pdf/pdftable.inc.php');
        //ob_end_clean();

        $p = new PDFTable();
        $p->AliasNbPages();
        $p->AddPage();
        $p->setfont('times', '', 6);
        $p->htmltable($htmlTable);
        $p->output('', 'I');
    }

    function listaMail() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        if (!$session->get('financiaciones.search')) {
            $sql = "SELECT persona, nombre, apellido,domicilio,telefono, mail, descripcion FROM vw_list_personas ";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT persona, nombre, apellido,domicilio,telefono, mail, descripcion FROM vw_list_personas WHERE persona LIKE '?' OR nombre LIKE '?' OR apellido LIKE '?' OR descripcion LIKE '?'  ";
            $conca = " AND ";
        }

        $sql .= " ORDER BY apellido, nombre ASC";

        $consult = $database->parse($sql, '%' . $session->get('financiaciones.search') . '%', '%' . $session->get('financiaciones.search') . '%', '%' . $session->get('financiaciones.search') . '%', '%' . $session->get('financiaciones.search') . '%');
        $results = $database->getRows($consult);

        // </editor-fold>

        $concat = '; ';
        foreach ($results as $integrante) {
            if ($integrante['mail'] != '') {
                $lista_de_mails .= $integrante['mail'] . $concat;
                $concat = '; ';
            }
        }
        if (strlen($lista_de_mails) < 4) {
            $lista_de_mails = "Sin inscriptos.";
        }


        $view = $this->locator->create('template');
        $view->set('lista_de_mails', $lista_de_mails);
        $response->set($view->fetch('content/listademail.tpl'));
    }

    function getFinanciacion() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>		

        $financiacion = $request->get('financiacion', 'get');

            $sql = "SELECT * FROM articulospuntovta  WHERE financiacion = '?' ";
            
            $consulta = $database->parse($sql, $financiacion);
            $codigo = $database->getRow($consulta);
        

        echo json_encode($codigo);
    }
    
//    function getFinanciaciones() {
//        $request = & $this->locator->get('request');
//        $database = & $this->locator->get('database');
//        $config = & $this->locator->get('config');
//        $url = & $this->locator->get('url');
//        $language = & $this->locator->get('language');
//        $response = & $this->locator->get('response');
//
//        $view = $this->locator->create('template');
//
//        $formapago_vta = $request->get('formapago_vta', 'get');
//
//        $sql = "SELECT ma.marca as id, ma.descripcion as value FROM articulos a left join marcas ma ON a.marca = ma.marca WHERE a.tipoproducto ='?' and ma.marca is not null  GROUP BY a.marca ";
//        $consulta = $database->parse($sql, $formapago_vta);
//        $results = $database->getRows($consulta);
//        
////        $sql = "SELECT a.marca as id, ma.descripcion as value FROM equipos e LEFT JOIN articulos a ON e.articulo = a.articulo left join marcas ma ON a.marca = ma.marca WHERE a.tipoproducto ='?' and e.equipo ='?' GROUP BY a.marca ";
////        $consulta = $database->parse($sql, $tipoproducto, $equipo);
////        $marca_equipo = $database->getRow($consulta);
//        
//        $varia = utf8_decode(json_encode($results));
//
//        echo $varia;
//        //echo $sql; 
//    }
    
    function getFinanciaciones() {
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $response = & $this->locator->get('response');

        $view = $this->locator->create('template');

        $formapago_vta = $request->get('formapago_vta', 'get');

         $sql = "SELECT ma.marca as id, ma.descripcion as value FROM articulos a left join marcas ma ON a.marca = ma.marca WHERE a.tipoproducto ='?' and ma.marca is not null  GROUP BY a.marca ";
        $consulta = $database->parse($sql, $formapago_vta);
        $results = $database->getRows($consulta);

        $opts = "<option value='-1' checked>Seleccione uno...</option>";

        foreach ($results as $result):
            $opts .= "<option value='" . $result['financiacion'] . "'>" . $result['descripcion'] . "</option>";
        endforeach;

        echo $opts;
        //echo $sql; 
    }
    
    function getLocalidad() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
// </editor-fold>

        $miDescripcion = $request->get('term', 'get');

        $sql = "SELECT localidad  as id, desclocalidad as value "
                . "FROM vw_grilla_localidades "
                . "WHERE descripcion LIKE '?' "
                . "LIMIT 10";
        $consulta = $database->parse($sql, '%' . $miDescripcion . '%');
        $codigo = $database->getRows($consulta);

        $varia = utf8_decode(json_encode($codigo));

        echo $varia;
    }

}

?>