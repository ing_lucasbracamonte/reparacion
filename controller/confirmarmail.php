<?php

class ControllerConfirmarMail extends Controller {

    var $error = array();

    function index() {
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $user = & $this->locator->get('user');
        $url = & $this->locator->get('url');

        if (!$user->isLogged())
            $response->redirect($url->ssl('login'));

        $template->set('title', 'Confirmar Mail');

        $response->set($this->getForm());
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');

        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('entry_mail', 'E-mail:');
        $view->set('entry_mail_repetido', 'Repetir e-mail:');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('heading_title', 'Confirmar E-mail');
        $view->set('heading_description', 'Es obligatorio contar con una &uacute;nica dirección de e-mail personal y verificada para el uso del sistema. Por favor, confirme la direcci&oacute;n de e-mail del asistente para continuar, esto se realiza por &uacute;nica vez por persona.');

        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        $view->set('tab_general', $language->get('tab_general'));

        if (($request->has('persona', 'post')) && ($request->isPost())) {
            $consulta = "SELECT DISTINCT mail, mail_confirmado, nombre FROM personas WHERE persona = '" . $request->get('persona', 'post') . "'";
            $persona_info = $database->getRow($consulta);
        }

        if ($request->has('persona', 'post')) {
            $view->set('persona', $request->get('persona', 'post'));
        } else {
            $view->set('persona', @$persona_info['persona']);
        }

        if ($request->has('mail', 'post')) {
            $view->set('mail', $request->get('mail', 'post'));
        } else {
            $view->set('mail', @$persona_info['mail']);
        }

        if ($request->has('mail_repetido', 'post')) {
            $view->set('mail_repetido', $request->get('mail_repetido', 'post'));
        } else {
            $view->set('mail_repetido', '');
        }

        if ($request->has('nombre', 'post')) {
            $view->set('nombre', $request->get('nombre', 'post'));
        } else {
            $view->set('nombre', @$persona_info['nombre']);
        }

        if ($request->has('destino', 'post')) {
            $view->set('destino', urlencode($request->get('destino', 'post')));
        } else {
            $view->set('destino', 'dex');
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $template->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');
        $view->set('error_mail', @$this->error['mail']);
        $view->set('error_mail_repetido', @$this->error['mail_repetido']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('confirmarmail', 'update'));

        $view->set('cancel', $url->ssl('confirmarmail'));
        // </editor-fold>

        return $view->fetch('content/confirmar_email.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $mail = & $this->locator->get('mail');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        $mensaje = '';

        if (!$mail->validarDuplicado($request->get('mail', 'post'), $database, $mensaje, $request->get('persona', 'post'))) {
            $this->error['mail'] = $mensaje;
        }

        if (trim($request->get('mail', 'post')) != trim($request->get('mail_repetido', 'post'))) {
            $this->error['mail_repetido'] = 'Los e-mails no concuerdan.';
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $user = & $this->locator->get('user');
        // </editor-fold>

        if (($request->isPost()) && ($this->validateForm())) {
            $sql = "UPDATE personas SET mail = '?', mail_confirmado = '?', usuario = '?' WHERE persona = '?'";
            $database->query($database->parse($sql, $request->get('mail', 'post'), '1', $user->getPERSONA(), $request->get('persona', 'post')));

            $response->set('true');
        } else {

            $response->set($this->getForm());
        }
    }

}

?>