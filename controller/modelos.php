<?php

class ControllerModelos extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'GESTIÓN DE MODELOS');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
        $request = & $this->locator->get('request');
        // </editor-fold>
         // <editor-fold defaultstate="collapsed" desc="LIMPIA FILTRO Y VARIBLES DE SESSION">

        if ($request->get('filtra') == NULL) {

            $session->set('modelos.search', '');
            $session->set('modelos.sort', '');
            $session->set('modelos.order', '');
            $session->set('modelos.page', '');

            $view->set('search', '');
            $view->set('modelos.search', '');
                        
            $cache->delete('modelos');
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">

        $cols = array();

        $cols[] = array(
            'name' => 'Modelo',
            'sort' => 'modelo',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Descripci&oacute;n',
            'sort' => 'descripcion',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Marca',
            'sort' => 'modelo',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'modelo',
            'descripcion'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        set_time_limit(0);

        if (!$session->get('modelos.search')) {
            $sql = "SELECT * FROM vw_list_modelos  ";
        } else {
            $sql = "SELECT * FROM vw_list_modelos WHERE modelo LIKE '?' OR descripcion LIKE '?' OR marca LIKE '?' OR descmarca LIKE '?' ";
        }

        if (in_array($session->get('modelos.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('modelos.sort') . " " . (($session->get('modelos.order') == 'desc') ? 'desc' : 'asc');
        } else {
            $sql .= " ORDER BY descripcion ASC";
        }

//		$consul = $database->splitQuery($database->parse($sql,'%' . $session->get('modelos.search') . '%','%' . $session->get('modelos.search') . '%'), $session->get('modelos.page'), $config->get('config_max_rows'));
//
//                $results = $database->getRows($consul);

        $consulta = $database->parse($sql, '%' . $session->get('modelos.search') . '%', '%' . $session->get('modelos.search') . '%', '%' . $session->get('modelos.search') . '%', '%' . $session->get('modelos.search') . '%');
        $results = $database->getRows($consulta);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['modelo'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['descripcion'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['descmarca'],
                'align' => 'left',
                'default' => 0
            );

            $action = array();

            if ($user->hasPermisos($user->getPERSONA(), 'modelos', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa fa-fw fa-pencil',
                    'text' => $language->get('button_update'),
                    'prop_a' => array('href' => $url->ssl('modelos', 'update', array('modelo' => $result['modelo'])))
                );
            }


            if ($user->hasPermisos($user->getPERSONA(), 'modelos', 'B')) {
                $action[] = array(
                    'icon' => 'img/iconos-11.png',
                    'text' => $language->get('button_delete'),
                    'class' => 'fa fa-fw fa-trash-o',
                    'prop_a' => array('href' => "javascript:ActionDelete('" . $url->ssl('modelos', 'delete', array('modelo' => $result['modelo'])) . "');")
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'modelos', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw fa-search',
                    'text' => $language->get('button_consult'),
                    'prop_a' => array('href' => $url->ssl('modelos', 'consulta', array('modelo' => $result['modelo'])))
                );
            }



            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        //<editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('modelos.page'));

        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        $view->set('heading_title', 'MODELOS');
        $view->set('placeholder_buscar', 'BUSCA POR ID O DESCRIPCION O MODELOS');

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        //$view->set('consultaVacia', $url->ssl('consulta','consulta_vacia'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        //$view->set('button_consultavacia', "Vacía");
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('modelos.search'));
        $view->set('sort', $session->get('modelos.sort'));
        $view->set('order', $session->get('modelos.order'));
        $view->set('page', $session->get('modelos.page'));

        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $view->set('titulo_ventana', '::. Exportar');

        $view->set('entry_modificar', "Modificar");
        $view->set('entry_agregar', "Agregar");

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('modelos', 'page'));
        $view->set('list', $url->ssl('modelos'));

        if ($user->hasPermisos($user->getPERSONA(), 'modelos', 'A')) {
            $view->set('insert', $url->ssl('modelos', 'insert'));
        }

        if ($user->hasPermisos($user->getPERSONA(), 'modelos', 'C'))
            $view->set('export', $url->ssl('modelos', 'exportar'));

        $view->set('addPais', $url->ssl('modelos', 'insert'));
        // $view->set('updatePersona', $url->ssl('paciente','update', array('persona' => $result['persona'])));
        $view->set('updatePais', $url->ssl('modelos', 'update'));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_modelos.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">
//		if ($request->has('modelo', 'post')) {
//                    $session->set('modelos.pais',$request->get('modelo','post'));              
//                }

        if ($request->has('search', 'post')) {
            $session->set('modelos.search', $request->get('search', 'post'));
        }


        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('modelos.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('modelos.order', (($session->get('modelos.sort') == $request->get('sort', 'post')) && ($session->get('modelos.order') == 'asc')) ? 'desc' : 'asc');
        }

        if ($request->has('sort', 'post')) {
            $session->set('modelos.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('modelos', 'index', array('filtra' => '1')));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $template = & $this->locator->get('template');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('heading_title', 'DATOS DE LA MODELO');
        $view->set('entry_marca', 'Marca:');
        $view->set('entry_modelo', 'Modelo:');
        $view->set('entry_descripcion', 'Descripci&oacute;n:');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $template->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('tab_general', $language->get('tab_general'));

        $view->set('modelo', $request->get('modelo'));

        if (($request->get('modelo')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM modelos WHERE modelo = '" . $request->get('modelo') . "'";
            $objeto_info = $database->getRow($consulta);
        }

        if ($request->has('modelo', 'post')) {
            $view->set('modelo', $request->get('modelo', 'post'));
            $objeto_id = @$objeto_info['modelo'];
        } else {
            $view->set('modelo', $request->get('modelo', 'get'));
            $objeto_id = @$objeto_info['modelo'];
        }

        if ($request->has('descripcion', 'post')) {
            $view->set('descripcion', $request->get('descripcion', 'post'));
        } else {
            $view->set('descripcion', @$objeto_info['descripcion']);
        }

        if ($request->has('marca', 'post')) {
            $view->set('marca', $request->get('marca', 'post'));
        } else {
            $view->set('marca', @$objeto_info['marca']);
        }
        $view->set('marcas', $database->getRows("SELECT * FROM marcas ORDER BY descripcion ASC"));


        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');
        $view->set('error_modelo', @$this->error['modelo']);
        $view->set('error_descripcion', @$this->error['descripcion']);
        $view->set('error_marca', @$this->error['marca']);
        $view->set('error_texto_error', @$this->error['texto_error']);
        //$view->set('error_titulo', @$this->error['titulo']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        //$view->set('action', $url->ssl('modelos', $request->get('action'), array('modelo' => $request->get('modelo'))));
        $view->set('actionBoton', $request->get('action'));
        $view->set('cancel', $url->ssl('modelos'));
        // </editor-fold>

        return $view->fetch('content/modelo.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        if (@$this->error['modelo'] == '') {
            $sql = "SELECT count(modelo) as total FROM modelos WHERE modelo ='?'";
            $marca = $database->getRow($database->parse($sql, $request->get('modelo', 'post')));

            if ($marca['total'] > 0 && $request->get('accion_form', 'post') == 'insert') {
                $errores .= 'Esta modelo ya existe en el sistema. <br>';
            }
        }

        if ((strlen($request->get('modelo', 'post')) == 0)) {
            $errores .= 'Debe ingresar un modelo. <br>';
        }

        if ((strlen($request->get('descripcion', 'post')) == 0)) {
            $errores .= 'Debe ingresar la descripción. <br>';
        }

        if ($request->get('marca', 'post') == '-1') {
            $errores .= 'Debe seleccionar una marca. <br>';
        }

        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';
        $consulta_info = $database->getRow("SELECT COUNT(*) as total FROM equipos WHERE modelo = '" . $request->get('modelo') . "'");
        if (@$consulta_info['total'] != 0) {
            $errores .= 'No es posible eliminar el modelo, tiene equipos asigados. <br>';
        }

        $consulta_info = $database->getRow("SELECT COUNT(*) as total FROM accesorios WHERE modelo = '" . $request->get('modelo') . "'");
        if (@$consulta_info['total'] != 0) {
            $errores .= 'No es posible eliminar el modelo, tiene accesorios asigados. <br>';
        }

        if ($errores != '') {
            $this->error['message'] = $errores;
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Modelos');

        if (($request->isPost()) && ($this->validateForm())) {
            $sql = "INSERT IGNORE INTO modelos SET modelo = '?', descripcion = '?',  marca = '?'";
            $consulta = $database->parse($sql, $request->get('modelo', 'post'), $request->get('descripcion', 'post'), $request->get('marca', 'post'));
            $database->query($consulta);

            //$id = $database->getLastId();
            $id = $request->get('modelo', 'post');
            $cache->delete('modelos');
            $session->set('message', 'Se agreg&oacute; la modelo: ' . $id);

            $response->redirect($url->ssl('modelos'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos de la modelo');
        if (($request->isPost()) && ($this->validateForm())) {

            $id = $request->get('modelo', 'post');
            $sql = "UPDATE modelos SET descripcion ='?',marca='?'  WHERE modelo='?' ";
            $consulta = $database->parse($sql, $request->get('descripcion', 'post'), $request->get('marca', 'post'), $id);
            $database->query($consulta);

            $cache->delete('modelos');

            $session->set('message', 'Se actualiz&oacute; el modelo: ' . $id);

            $response->redirect($url->ssl('modelos'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos de la modelo');
        if (($request->isPost())) {
            $response->redirect($url->ssl('modelos'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        if (($request->get('modelo')) && ($this->validateDelete())) {

            $id = $request->get('modelo', 'get');

            //TODO: FALTA PROGRAMAR LA ELIMINACION Y VALIDACION VER SI NO HAY RELACIONES
            $sql = "DELETE FROM modelos WHERE modelo = '" . $id . "' ";
            $database->query($sql);

            $cache->delete('modelos');

            $session->set('message', 'Se ha eliminado el modelo: ' . $id);

            $response->redirect($url->ssl('modelos'));
        }

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

}

?>