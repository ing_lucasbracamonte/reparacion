<?php

class ControllerProveedores extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'PROVEEDORES');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
         $cache = & $this->locator->get('cache');
        $request = & $this->locator->get('request');
        // </editor-fold>
         // <editor-fold defaultstate="collapsed" desc="LIMPIA FILTRO Y VARIBLES DE SESSION">

        if ($request->get('filtra') == NULL) {

            $session->set('proveedores.search', '');
            $session->set('proveedores.sort', '');
            $session->set('proveedores.order', '');
            $session->set('proveedores.page', '');

            $view->set('search', '');
            $view->set('proveedores.search', '');
                        
            $cache->delete('proveedores');
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">
        $cols = array();

        $cols[] = array(
            'name' => 'Cuit/Cuil',
            'sort' => 'cuitcuil',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Nombre',
            'sort' => 'nombre',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Tipo',
            'sort' => 'desctipovendedor',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'telefono',
            'sort' => 'telefono'
        );

        $cols[] = array(
            'name' => 'mail',
            'sort' => 'mail'
        );

        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'cuitcuil',
            'nombre',
            'desctipovendedor'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        if (!$session->get('proveedores.search')) {
            $sql = "SELECT * FROM vw_grilla_proveedores  ";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT * FROM vw_grilla_proveedores  WHERE cuitcuil LIKE '?' OR nombre LIKE '?' ";
            $conca = " AND ";
        }

        if (in_array($session->get('proveedores.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('proveedores.sort') . " " . (($session->get('proveedores.order') == 'DESC') ? 'DESC' : 'ASC');
        } else {
            $sql .= " ORDER BY nombre ASC";
        }

        $consult = $database->parse($sql, '%' . $session->get('proveedores.search') . '%', '%' . $session->get('proveedores.search') . '%', '%' . $session->get('proveedores.search') . '%');
        $results = $database->getRows($consult);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['cuitcuil'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => $result['nombre'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['desctipoproveedor'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['telefono'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['mail'],
                'align' => 'left',
                'default' => 0
            );

            $action = array();

            if ($user->hasPermisos($user->getPERSONA(), 'proveedores', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa fa-fw fa-pencil',
                    'text' => $language->get('button_update'),
                    'prop_a' => array('href' => $url->ssl('proveedores', 'update', array('proveedor' => $result['proveedor'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'proveedores', 'B')) {
                $action[] = array(
                    'icon' => 'img/iconos-11.png',
                    'text' => $language->get('button_delete'),
                    'class' => 'fa fa-fw fa-trash-o',
                    'prop_a' => array('href' => "javascript:ActionDelete('" . $url->ssl('proveedores', 'delete', array('proveedor' => $result['proveedor'])) . "');")
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'proveedores', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw fa-search',
                    'text' => $language->get('button_consult'),
                    'prop_a' => array('href' => $url->ssl('proveedores', 'consulta', array('proveedor' => $result['proveedor'])))
                );
            }

            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('proveedores.page'));


        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        if ($session->get('proveedores.search')) {
            $view->set('search', $session->get('proveedores.search'));
        } else {
            $view->set('search', '');
        }

//		if ($session->get('proveedores.nombre')) {
//                    $view->set('nombre',$session->get('proveedores.nombre'));
//		} else {
//                    $view->set('nombre', '');
//		}
//		if ($session->get('proveedores.localidad')) {
//                    $view->set('localidad', $session->get('proveedores.localidad'));
//		} else {
//                    $view->set('localidad', '-1');
//		}
        //$view->set('localidades', $database->getRows("SELECT localidad, descripcion FROM localidades ORDER BY descripcion"));

        $view->set('heading_title', 'PROVEEDORES');
        $view->set('heading_title2', 'Filtrar');
        $view->set('heading_description', 'Gesti&oacute;n de Proveedores');

        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/NOTICIAS.png');
        $view->set('placeholder_buscar', 'BUSCA POR CUIT/CUIL O NOMBRE ');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('proveedores.search'));
        $view->set('sort', $session->get('proveedores.sort'));
        $view->set('order', $session->get('proveedores.order'));
        $view->set('page', $session->get('proveedores.page'));

        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('proveedores', 'page'));
        $view->set('list', $url->ssl('proveedores'));
        if ($user->hasPermisos($user->getPERSONA(), 'proveedores', 'A')) {
            $view->set('insert', $url->ssl('proveedores', 'insert'));
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_proveedores.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">

        if ($request->has('search', 'post')) {
            $session->set('proveedores.search', $request->get('search', 'post'));
        }

        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('proveedores.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('proveedores.order', (($session->get('proveedores.sort') == $request->get('sort', 'post')) && ($session->get('proveedores.order') == 'ASC')) ? 'DESC' : 'ASC');
        }

        if ($request->has('sort', 'post')) {
            $session->set('proveedores.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('proveedores', 'index', array('filtra' => '1')));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $language = & $this->locator->get('language');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VIEW VARIABLES">
        $view->set('heading_title', 'Proveedores');
        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/user-8.png');

        $view->set('heading_description', 'Gesti&oacute;n de proveedores');

        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        $view->set('tab_general', $language->get('tab_general'));

        if (($request->get('proveedor')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM proveedores WHERE proveedor = '?'";
            $objeto_info = $database->getRow($database->parse($consulta, $request->get('proveedor')));
        }

        if ($request->has('proveedor', 'post')) {
            $view->set('proveedor', $request->get('proveedor', 'post'));
            $objeto_id = $request->get('proveedor', 'post');
        } else {
            $view->set('proveedor', @$objeto_info['proveedor']);
            $objeto_id = @$objeto_info['proveedor'];
        }

        $view->set('entry_cuitcuil', 'Cuit/Cuil:');
        if ($request->has('cuitcuil', 'post')) {
            $view->set('cuitcuil', $request->get('cuitcuil', 'post'));
        } else {
            $view->set('cuitcuil', @$objeto_info['cuitcuil']);
        }

        $view->set('entry_nombre', 'Nombre:');
        if ($request->has('nombre', 'post')) {
            $view->set('nombre', $request->get('nombre', 'post'));
        } else {
            $view->set('nombre', @$objeto_info['nombre']);
        }

        $view->set('entry_tipoproveedor', 'Tipo proveedor:');
        if ($request->has('tipoproveedor', 'post')) {
            $view->set('tipoproveedor', $request->get('tipoproveedor', 'post'));
        } else {
            $view->set('tipoproveedor', @$objeto_info['tipoproveedor']);
        }
        $view->set('tiposproveedor', $database->getRows("SELECT * FROM tiposproveedor ORDER BY descripcion ASC"));

        $view->set('entry_direccion', 'Direccion:');
        $view->set('entry_numero', 'Nro');
        $view->set('entry_piso', 'piso');
        $view->set('entry_departamento', 'Dto:');
        $view->set('entry_localidad', 'Localidad:');

        if ($request->has('direccion', 'post')) {
            $view->set('direccion', $request->get('direccion', 'post'));
        } else {
            $view->set('direccion', @$objeto_info['direccion']);
        }

        if ($request->has('numero', 'post')) {
            $view->set('numero', $request->get('numero', 'post'));
        } else {
            $view->set('numero', @$objeto_info['numero']);
        }

        if ($request->has('piso', 'post')) {
            $view->set('piso', $request->get('piso', 'post'));
        } else {
            $view->set('piso', @$objeto_info['piso']);
        }

        if ($request->has('departamento', 'post')) {
            $view->set('departamento', $request->get('departamento', 'post'));
        } else {
            $view->set('departamento', @$objeto_info['departamento']);
        }

        if ($request->has('auto_localidad', 'post')) {
            $view->set('auto_localidad', $request->get('auto_localidad', 'post'));
        } else {
            $consulta = "SELECT localidad  as id, desclocalidad as value "
                    . "FROM vw_grilla_localidades WHERE localidad = '?'";
            $localidad_info = $database->getRow($database->parse($consulta, @$objeto_info['localidad']));
            $view->set('auto_localidad', @$localidad_info['value']);
        }
        if ($request->has('auto_localidad_proveedor', 'post')) {
            $view->set('auto_localidad_proveedor', $request->get('auto_localidad_proveedor', 'post'));
        } else {
            $view->set('auto_localidad_proveedor', @$objeto_info['localidad']);
        }
        $view->set('script_busca_localidades', $url->rawssl('proveedores', 'getLocalidad'));

        $view->set('entry_telefono', 'Telefono:');
        if ($request->has('telefono', 'post')) {
            $view->set('telefono', $request->get('telefono', 'post'));
        } else {
            $view->set('telefono', @$objeto_info['telefono']);
        }

        $view->set('entry_mail', 'E-mail:');
        if ($request->has('mail', 'post')) {
            $view->set('mail', $request->get('mail', 'post'));
        } else {
            $view->set('mail', @$objeto_info['mail']);
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $template->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');
        $view->set('error_cuitcuil', @$this->error['cuitcuil']);
        $view->set('error_texto_error', @$this->error['texto_error']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $urlAction = $url->ssl('proveedores', $request->get('action'), array('proveedor' => $request->get('proveedor')));
        $view->set('action', $urlAction);

        $view->set('accion_form', $request->get('action'));
        $view->set('cancel', $url->ssl('proveedores'));
        // </editor-fold>

        return $view->fetch('content/proveedor.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $mail = & $this->locator->get('mail');
        $common = & $this->locator->get('common');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        if ((strlen($request->get('cuitcuil', 'post')) == 0)) {
            $this->error['cuitcuil'] = 'Debe ingresar el n&uacute;mero de CUIT/CUIL.';
            $errores .= 'Debe ingresar el n&uacute;mero de CUIT/CUIL. <br>';
        }

//                if(strlen($request->get('cuitcuil', 'post')) < 12){
//                        $this->error['persona'] = 'No es un n&uacute;mero de CUIT/CUIL v&aacute;lido.';
//                        $errores .= 'No es un n&uacute;mero de CUIT/CUIL v&aacute;lido. <br>';                       
//                }

        if (@$this->error['cuitcuil'] == '') {
            $sql = "SELECT count(cuitcuil) as total FROM proveedores WHERE cuitcuil ='?'";
            $proveedor = $database->getRow($database->parse($sql, $request->get('cuitcuil', 'post')));

            if ($proveedor['total'] > 0 && $request->get('accion_form', 'post') == 'insert') {
                $errores .= 'El CUIT/CUIL ya existe en el sistema. <br>';
            }
        }

        if ((strlen($request->get('nombre', 'post')) < 2) || (strlen($request->get('nombre', 'post')) > 100)) {
            $errores .= 'Debe ingresar el nombre. <br>';
        }

        if ($request->get('tipoproveedor', 'post') == -1) {
            $errores .= 'Debe seleccionar un tipo de proveedor. <br>';
        }
//
        if ((strlen($request->get('telefono', 'post')) == 0)) {
            $errores .= 'Debe ingresar el teléfono. <br>';
        }
//
        if (!$common->validarMail($request->get('mail', 'post'))) {
            $errores .= 'El e-mail ingresado es inv&aacute;lido. <br>';
        }

        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $common = & $this->locator->get('common');
        // </editor-fold>

        $template->set('title', 'Proveedores');

        if (($request->isPost()) && ($this->validateForm())) {

            $sql = "INSERT INTO proveedores SET cuitcuil ='?', nombre ='?', direccion = '?', numero = '?',  piso='?', departamento='?', localidad='?', telefono ='?', mail ='?', tipoproveedor='?' ,au_usuario='?',au_accion='A',au_fecha_hora=NOW() ";
            $sql = $database->parse($sql, $request->get('cuitcuil', 'post'), strtoupper($request->get('nombre', 'post')), $request->get('direccion', 'post'), $request->get('numero', 'post'), $request->get('piso', 'post'), $request->get('departamento', 'post'), $request->get('auto_localidad_proveedor', 'post'), $request->get('telefono', 'post'), $request->get('mail', 'post'), $request->get('tipoproveedor', 'post'), $user->getPERSONA());
            $database->query($sql);

            $id = $database->getLastId();
            $cache->delete('proveedores');
            $session->set('message', 'Se agreg&oacute; el proveedor: ' . $request->get('cuitcuil', 'post'));

            $response->redirect($url->ssl('proveedores'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        $template->set('title', 'Proveedor');

        if (($request->isPost()) && ($this->validateForm())) {
            $id = $request->get('proveedor', 'post');

            $sql = "UPDATE proveedores SET cuitcuil ='?', nombre ='?', direccion = '?', numero = '?',  piso='?', departamento='?', localidad='?', telefono ='?',  mail ='?', tipoproveedor='?' ,au_usuario='?',au_accion='M',au_fecha_hora=NOW() WHERE proveedor = '?' ";
            $sql = $database->parse($sql, $request->get('cuitcuil', 'post'), strtoupper($request->get('nombre', 'post')), $request->get('direccion', 'post'), $request->get('numero', 'post'), $request->get('piso', 'post'), $request->get('departamento', 'post'), $request->get('auto_localidad_proveedor', 'post'), $request->get('telefono', 'post'), $request->get('mail', 'post'), $request->get('tipoproveedor', 'post'), $user->getPERSONA(), $id);
            $database->query($sql);

            $cache->delete('proveedores');
            $session->set('message', 'Se actualiz&oacute; el proveedor: ' . $request->get('cuitcuil', 'post'));

            $response->redirect($url->ssl('proveedores'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        $template->set('title', 'Proveedores');

        if (($request->isPost())) {

            $response->redirect($url->ssl('proveedores'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        if (($request->get('proveedor')) && ($this->validateDelete())) {

            $id = $request->get('proveedor', 'get');

            //TODO: FALTA PROGRAMAR LA ELIMINACION Y VALIDACION VER SI NO HAY RELACIONES
            //$sql = "DELETE FROM  WHERE  = '" . $id . "' ";
            //$database->query($sql);

            $cache->delete('proveedores');

            $session->set('message', 'Se ha eliminado el proveedor: ' . $id);

            $response->redirect($url->ssl('proveedores'));
        }

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';

        if ($errores != '') {
            $this->error['message'] = $errores;
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function exportar() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $session = & $this->locator->get('session');
        // </editor-fold>

        set_time_limit(0);

        $filtro = 'Filtro:';

        $htmlTable = "<table border=0 width=180> 
			<tr><td colspan=6 align=center style=bold size=14>" . utf8_decode("xxxxxxxxx xxxxxxxxx xxxxxxx") . "</td></tr>
			<tr><td colspan=6 align=center style=bold size=12>SISTEMA DE xxxxxxxxxxx xxxxxxxxx</td></tr>
			<tr><td colspan=6 align=center style=bold size=10>Listado de personas</td></tr>
			<tr><td colspan=6 align=center></td></tr>
			<tr></tr>";

        if (!$session->get('proveedores.persona') && !$session->get('proveedores.nombre')) {
            $sql = "SELECT p.persona, p.nombre, p.domicilio, l.descripcion,  p.celular, p.mail FROM personas p LEFT JOIN localidades l ON p.localidad = l.localidad";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT p.persona, p.nombre, p.domicilio, l.descripcion,  p.celular, p.mail FROM personas p LEFT JOIN localidades l ON p.localidad = l.localidad WHERE p.persona LIKE '%" . $session->get('proveedores.persona') . "%' AND p.nombre LIKE '%" . $session->get('proveedores.nombre') . "%'";
            $conca = " AND ";
        }

        if ($session->get('proveedores.localidad') != '-1' && $session->get('proveedores.localidad') != '') {
            $sql .= $conca . "p.localidad = '" . $session->get('proveedores.localidad') . "'";
            $conca = " AND ";
        }

        $sql .= " ORDER BY persona ASC";

        $reporte = $database->getRows($sql);

        foreach ($reporte as $estu) {

            $htmlTable .= "<tr><td colspan=6>______________________________________________________________________________________________________________</td></tr>
                                        <tr>
                                                <td align=left size=9>Documento</td>
                                                <td align=left size=9>" . utf8_decode($estu['persona']) . "</td>
                                        </tr>
                                        <tr>
                                                <td align=left size=9>Nombre</td>	
                                                <td align=left size=9 colspan= 5>" . utf8_decode($estu['nombre']) . "</td>
                                        </tr>
                                        <tr>
                                                <td align=left size=9>Domicilio<td>
                                                <td align=left size=9 colspan=2>" . utf8_decode($estu['domicilio']) . "</td>
                                                <td align=left size=9>Localidad</td>
                                                <td align=left size=9>" . utf8_decode($estu['descripcion']) . "</td>
                                        </tr>
                                        <tr>
                                                
                                                <td align=left size=9>Celular:</td>
                                                <td align=left size=9>" . utf8_decode($estu['celular']) . "</td>
                                                <td align=left size=9></td>
                                                <td align=left size=9></td>
                                        </tr>
                                        <tr>	
                                                <td align=left size=9>Email</td>
                                                <td align=left size=9 colspan=5>" . utf8_decode($estu['mail']) . "</td>
                                        </tr>";
        }

        $htmlTable .= "</table>";

        define('FPDF_FONTPATH', 'font/');
        require('library/pdf/pdftable.inc.php');
        //ob_end_clean();

        $p = new PDFTable();
        $p->AliasNbPages();
        $p->AddPage();
        $p->setfont('times', '', 6);
        $p->htmltable($htmlTable);
        $p->output('', 'I');
    }

    function getLocalidad() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
// </editor-fold>

        $miDescripcion = $request->get('term', 'get');

        $sql = "SELECT localidad  as id, desclocalidad as value "
                . "FROM vw_grilla_localidades "
                . "WHERE descripcion LIKE '?' "
                . "LIMIT 10";
        $consulta = $database->parse($sql, '%' . $miDescripcion . '%');
        $codigo = $database->getRows($consulta);

        $varia = utf8_decode(json_encode($codigo));

        echo $varia;
    }

}

?>