<?php

class ControllerArticulos extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        $session = & $this->locator->get('session');
        // </editor-fold>

        $template->set('title', 'GESTIÓN DE ARTICULOS');
        
        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
        $request = & $this->locator->get('request');
        // </editor-fold>
        
         // <editor-fold defaultstate="collapsed" desc="LIMPIA FILTRO Y VARIBLES DE SESSION">

        if ($request->get('filtra') == NULL) {

            $session->set('articulos.search', '');
            $session->set('articulos.sort', '');
            $session->set('articulos.order', '');
            $session->set('articulos.page', '');

            $view->set('search', '');
            $view->set('articulos.search', '');
            $view->set('tipoproducto', '-1');
            $view->set('articulos.tipoproducto', '-1');
            
            $cache->delete('articulos');
        }

        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">

        $cols = array();

        $cols[] = array(
            'name' => 'ARTICULO',
            'sort' => 'articulo',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'TIPO',
            'sort' => 'desctipoproducto',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'MARCA',
            'sort' => 'descmarca',
            'align' => 'left'
        );
        $cols[] = array(
            'name' => 'MODELO',
            'sort' => 'descmodelo',
            'align' => 'left'
        );
        $cols[] = array(
            'name' => 'COD INTERNO',
            'sort' => 'codbarrainterno',
            'align' => 'left'
        );
        
        $cols[] = array(
            'name' => '% IVA',
            'sort' => 'codbarrainterno',
            'align' => 'left'
        );
        
        $cols[] = array(
            'name' => 'ESTADO',
            'sort' => 'descestadoarticulo',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'GAMA',
            'sort' => 'desctipogama',
            'align' => 'left'
        );
//                $cols[] = array(
//			'name'  => 'PRECIO VTA',
//			'sort'  => 'precio',
//			'align' => 'left'
//		);

        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'articulo',
            'desctipoproducto',
            'descmarca',
            'descmodelo',
            'descestado',
            'precio',
            'descgama'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        set_time_limit(0);
        //$art = $session->get('articulos.search');
        //$cat = strlen($session->get('articulos.search'));
        if (strlen($session->get('articulos.search')) == 0 ) {
            $sql = "SELECT * FROM vw_grilla_articulos  ";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT * FROM vw_grilla_articulos WHERE articulo LIKE '?' OR codbarrainterno LIKE '?' OR descestadoarticulo LIKE '?' OR desctipoproducto LIKE '?' OR descmodelo LIKE '?' OR descmarca LIKE '?' ";
            $conca = " AND ";
        }

        if ($session->get('articulos.tipoproducto') != '-1' && $session->get('articulos.tipoproducto') != '') {
            $sql .= $conca . " tipoproducto = '" . $session->get('articulos.tipoproducto') . "'  ";
        }

        if (in_array($session->get('articulos.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('articulos.sort') . " " . (($session->get('articulos.order') == 'desc') ? 'desc' : 'asc');
        } else {
            $sql .= " ORDER BY marca, modelo ASC";
        }

//		$consul = $database->splitQuery($database->parse($sql,'%' . $session->get('articulos.search') . '%','%' . $session->get('articulos.search') . '%'), $session->get('articulos.page'), $config->get('config_max_rows'));
//
//                $results = $database->getRows($consul);

        $consulta = $database->parse($sql, '%' . $session->get('articulos.search') . '%', '%' . $session->get('articulos.search') . '%', '%' . $session->get('articulos.search') . '%', '%' . $session->get('articulos.search') . '%', '%' . $session->get('articulos.search') . '%', '%' . $session->get('articulos.search') . '%');
        $results = $database->getRows($consulta);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['articulo'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['desctipoproducto'],
                'align' => 'left',
                'default' => 0
            );
            
            $descmarca = '-';
            if (@$result['descmarca']) {
                $descmarca = $result['descmarca'];
            }
            $cell[] = array(
                'value' => $descmarca,
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['descmodelo'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['codbarrainterno'],
                'align' => 'left',
                'default' => 0
            );
            
            $cell[] = array(
                'value' => @$result['iva'],
                'align' => 'left',
                'default' => 0
            );
            
            $desc = "-";
            if (@$result['descestadoarticulo']) {
                $desc = $result['descestadoarticulo'];
            }
            $cell[] = array(
                'value' => $desc,
                'align' => 'left',
                'default' => 0
            );

            $desc = "-";
            if (@$result['desctipogama']) {
                $desc = $result['desctipogama'];
            }
            $cell[] = array(
                'value' => $desc,
                'align' => 'left',
                'default' => 0
            );

            $action = array();

            if ($user->hasPermisos($user->getPERSONA(), 'articulos', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa fa-fw fa-pencil',
                    'text' => $language->get('button_update'),
                    'prop_a' => array('href' => $url->ssl('articulos', 'update', array('articulo' => $result['articulo'])))
                );
            }


            if ($user->hasPermisos($user->getPERSONA(), 'articulos', 'B')) {
                $action[] = array(
                    'icon' => 'img/iconos-11.png',
                    'text' => $language->get('button_delete'),
                    'class' => 'fa fa-fw fa-trash-o',
                    'prop_a' => array('href' => "javascript:ActionDelete('" . $url->ssl('articulos', 'delete', array('articulo' => $result['articulo'])) . "');")
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'articulos', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw fa-search',
                    'text' => $language->get('button_consult'),
                    'prop_a' => array('href' => $url->ssl('articulos', 'consulta', array('articulo' => $result['articulo'])))
                );
            }

            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        //<editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('articulos.page'));

        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        $view->set('heading_title', 'ARTICULOS');
        $view->set('placeholder_buscar', 'BUSCA POR ID O MARCA O MODELO O TIPO');

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        //$view->set('consultaVacia', $url->ssl('consulta','consulta_vacia'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        //$view->set('button_consultavacia', "Vacía");
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('articulos.search'));
        $view->set('tipoproducto', $session->get('articulos.tipoproducto'));
        $view->set('sort', $session->get('articulos.sort'));
        $view->set('order', $session->get('articulos.order'));
        $view->set('page', $session->get('articulos.page'));

        //$view->set('articulos.tipoproducto', '-1');
        $view->set('tiposproducto', $database->getRows("SELECT * FROM tiposproducto ORDER BY descripcion ASC"));

        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $view->set('titulo_ventana', '::. Exportar');

        $view->set('entry_modificar', "Modificar");
        $view->set('entry_agregar', "Agregar");
 
        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('articulos', 'page'));
        $view->set('list', $url->ssl('articulos'));

        if ($user->hasPermisos($user->getPERSONA(), 'articulos', 'A')) {
            $view->set('insert', $url->ssl('articulos', 'insert'));
        }
        if ($user->hasPermisos($user->getPERSONA(), 'articulos', 'A')) {
            $view->set('insertImport', $url->ssl('articulos', 'insertImport'));
        }

        if ($user->hasPermisos($user->getPERSONA(), 'articulos', 'C')) {
            $view->set('exportXls', $url->ssl('articulos', 'exportXls'));
        }

        //$view->set('addPais', $url->ssl('articulos','insert'));
        // $view->set('updatePersona', $url->ssl('paciente','update', array('persona' => $result['persona'])));
        //$view->set('updatePais', $url->ssl('articulos','update'));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_articulos.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">

        if ($request->has('search', 'post')) {
            $session->set('articulos.search', $request->get('search', 'post'));
        }

        if ($request->has('tipoproducto', 'post')) {
            $session->set('articulos.tipoproducto', $request->get('tipoproducto', 'post'));
        }

        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('articulos.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('articulos.order', (($session->get('articulos.sort') == $request->get('sort', 'post')) && ($session->get('articulos.order') == 'asc')) ? 'desc' : 'asc');
        }

        if ($request->has('sort', 'post')) {
            $session->set('articulos.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>
        
        $response->redirect($url->ssl('articulos', 'index', array('filtra' => '1')));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $template = & $this->locator->get('template');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('heading_title', 'DATOS DEL ARTICULO');
        $view->set('entry_marca', 'Marca');
        $view->set('entry_modelo', 'Modelo');
        $view->set('entry_mnu', 'MNU');
        $view->set('entry_precio', 'Precio de venta');
        $view->set('entry_iva', 'IVA');
        $view->set('entry_tipogama', 'Gama');
        $view->set('entry_tipoproducto', 'Tipo de producto');
        $view->set('entry_concoddebarras', 'Tiene código de barras');
        $view->set('entry_codbarrainterno', 'Cod. Barra interno');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $template->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('tab_general', $language->get('tab_general'));

        $view->set('articulo', $request->get('articulo'));

        if (($request->get('articulo')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM vw_grilla_articulos WHERE articulo = '" . $request->get('articulo') . "' ";
            $objeto_info = $database->getRow($consulta);
        }

        if ($request->has('articulo', 'post')) {
            $view->set('articulo', $request->get('articulo', 'post'));
            $objeto_id = @$objeto_info['articulo'];
        } else {
            $view->set('id', $request->get('articulo', 'get'));
            $objeto_id = @$objeto_info['articulo'];
        }

        if ($request->has('tipoproducto', 'post')) {
            $view->set('tipoproducto', $request->get('tipoproducto', 'post'));
        } else {
            $view->set('tipoproducto', @$objeto_info['tipoproducto']);
        }
        $view->set('tiposproducto', $database->getRows("SELECT * FROM tiposproducto ORDER BY descripcion ASC"));

        if ($request->has('modelo', 'post')) {
            $view->set('modelo', $request->get('modelo', 'post'));
        } else {
            $view->set('modelo', @$objeto_info['modelo']);
        }
//        $view->set('modelos', $database->getRows("SELECT modelo, descripcion FROM modelos ORDER BY descripcion ASC"));
//
//        if ($request->has('marca', 'post')) {
//            $view->set('marca', $request->get('marca', 'post'));
//        } else {
//            $view->set('marca', @$objeto_info['marca']);
//        }
//        $view->set('marcas', $database->getRows("SELECT marca, descripcion FROM marcas ORDER BY descripcion ASC"));

         // <editor-fold defaultstate="collapsed" desc="MARCA">

        if ($request->has('auto_marca', 'post')) {
            $view->set('auto_marca', $request->get('auto_marca', 'post'));
        } else {
            $consulta = "SELECT DISTINCT * FROM marcas WHERE marca = '?'";
            $marca_info = $database->getRow($database->parse($consulta, $objeto_info['marca']));
            $view->set('auto_marca', @$marca_info['descripcion']);
        }
        if ($request->has('auto_marca_id', 'post')) {
            $view->set('auto_marca_id', $request->get('auto_marca_id', 'post'));
        } else {
            $view->set('auto_marca_id', @$objeto_info['marca']);
        }
        $view->set('script_busca_marca', $url->rawssl('articulos', 'getMarcas'));
        // </editor-fold>
        
         // <editor-fold defaultstate="collapsed" desc="MODELO">

//        if ($request->has('auto_modelo', 'post')) {
//            $view->set('auto_modelo', $request->get('auto_modelo', 'post'));
//        } else {
//            $consulta = "SELECT DISTINCT * FROM modelos WHERE modelo = '?'";
//            $modelo_info = $database->getRow($database->parse($consulta, $objeto_info['modelo']));
//            $view->set('auto_modelo', @$modelo_info['descripcion']);
//        }
//        if ($request->has('auto_modelo_id', 'post')) {
//            $view->set('auto_modelo_id', $request->get('auto_modelo_id', 'post'));
//        } else {
//            $view->set('auto_modelo_id', @$objeto_info['modelo']);
//        }
//        $view->set('script_busca_modelo', $url->rawssl('articulos', 'getModelos'));
        // </editor-fold>
        
        if ($request->has('iva', 'post')) {
            $view->set('iva', $request->get('iva', 'post'));
        } else {
            $view->set('iva', @$objeto_info['iva']);
        }
        $view->set('ivas', $database->getRows("SELECT * FROM ivas ORDER BY iva ASC"));

        
        if ($request->has('tipogama', 'post')) {
            $view->set('tipogama', $request->get('tipogama', 'post'));
        } else {
            $view->set('tipogama', @$objeto_info['tipogama']);
        }
        $view->set('tiposgama', $database->getRows("SELECT * FROM tiposgama ORDER BY descripcion ASC"));

        if ($request->has('mnu', 'post')) {
            $view->set('mnu', $request->get('mnu', 'post'));
        } else {
            $view->set('mnu', @$objeto_info['mnu']);
        }

        if ($request->has('concoddebarra', 'post')) {
            $view->set('concoddebarra', $request->get('concoddebarra', 'post'));
        } else {
            $view->set('concoddebarra', @$objeto_info['concoddebarra']);
        }

        if ($request->has('codbarrainterno', 'post')) {
            $view->set('codbarrainterno', $request->get('codbarrainterno', 'post'));
        } else {
            $view->set('codbarrainterno', @$objeto_info['codbarrainterno']);
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $template->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');
        $view->set('error_modelo', @$this->error['modelo']);
        //$view->set('error_descripcion', @$this->error['descripcion']);
        $view->set('error_marca', @$this->error['marca']);
        //$view->set('error_id', @$this->error['id']);
        //$view->set('error_tipoid', @$this->error['tipoid']);
        $view->set('error_tipoproducto', @$this->error['tipoproducto']);
        $view->set('error_texto_error', @$this->error['texto_error']);
        //$view->set('error_titulo', @$this->error['titulo']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        //$view->set('action', $url->ssl('articulos', $request->get('action'), array('modelo' => $request->get('modelo'))));
        $view->set('actionBoton', $request->get('action'));
        $view->set('cancel', $url->ssl('articulos'));
        // </editor-fold>

        return $view->fetch('content/articulo.tpl');
    }

    function getFormImport() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $template = & $this->locator->get('template');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('heading_title', 'IMPORTAR ARTICULOS');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $template->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('tab_general', $language->get('tab_general'));

        $view->set('articulo', $request->get('articulo'));

        if (($request->get('articulo')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM vw_grilla_articulos WHERE articulo = '" . $request->get('articulo') . "' ";
            $objeto_info = $database->getRow($consulta);
        }

        if ($request->has('articulo', 'post')) {
            $view->set('articulo', $request->get('articulo', 'post'));
            $objeto_id = @$objeto_info['articulo'];
        } else {
            $view->set('id', $request->get('articulo', 'get'));
            $objeto_id = @$objeto_info['articulo'];
        }

        if ($request->has('tipoproducto', 'post')) {
            $view->set('tipoproducto', $request->get('tipoproducto', 'post'));
        } else {
            $view->set('tipoproducto', @$objeto_info['tipoproducto']);
        }
        $view->set('tiposproducto', $database->getRows("SELECT * FROM tiposproducto ORDER BY descripcion ASC"));

        if ($request->has('modelo', 'post')) {
            $view->set('modelo', $request->get('modelo', 'post'));
        } else {
            $view->set('modelo', @$objeto_info['modelo']);
        }
        $view->set('modelos', $database->getRows("SELECT modelo, descripcion FROM modelos ORDER BY descripcion ASC"));

        if ($request->has('marca', 'post')) {
            $view->set('marca', $request->get('marca', 'post'));
        } else {
            $view->set('marca', @$objeto_info['marca']);
        }
        $view->set('marcas', $database->getRows("SELECT marca, descripcion FROM marcas ORDER BY descripcion ASC"));
        
        if ($request->has('tipogama', 'post')) {
            $view->set('tipogama', $request->get('tipogama', 'post'));
        } else {
            $view->set('tipogama', @$objeto_info['tipogama']);
        }
        $view->set('tiposgama', $database->getRows("SELECT * FROM tiposgama ORDER BY descripcion ASC"));

        if ($request->has('precio', 'post')) {
            $view->set('precio', $request->get('precio', 'post'));
        } else {
            $view->set('precio', @$objeto_info['precio']);
        }

        if ($request->has('nmu', 'post')) {
            $view->set('nmu', $request->get('nmu', 'post'));
        } else {
            $view->set('nmu', @$objeto_info['nmu']);
        }

        if ($request->has('concoddebarra', 'post')) {
            $view->set('concoddebarra', $request->get('concoddebarra', 'post'));
        } else {
            $view->set('concoddebarra', @$objeto_info['concoddebarra']);
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');
        $view->set('error_modelo', @$this->error['modelo']);
        //$view->set('error_descripcion', @$this->error['descripcion']);
        $view->set('error_marca', @$this->error['marca']);
        //$view->set('error_id', @$this->error['id']);
        //$view->set('error_tipoid', @$this->error['tipoid']);
        $view->set('error_tipoproducto', @$this->error['tipoproducto']);
        $view->set('error_texto_error', @$this->error['texto_error']);
        //$view->set('error_titulo', @$this->error['titulo']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        //$view->set('action', $url->ssl('articulos', $request->get('action'), array('modelo' => $request->get('modelo'))));
        $view->set('actionBoton', $request->get('action'));
        $view->set('cancel', $url->ssl('articulos'));

        $view->set('exportXlsModelo', $url->ssl('articulos', 'exportXlsModelo'));
        // </editor-fold>

        return $view->fetch('content/articuloImport.tpl');
    }

    function insertImport() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $user = & $this->locator->get('user');
        $url = & $this->locator->get('url');
        $upload = & $this->locator->get('upload');
        $common = & $this->locator->get('common');
        // </editor-fold>

        $template->set('title', 'ARTICULOS');

        if (($request->isPost()) && ($this->validateFormImport())) {
            
            set_time_limit(0);
            
            $nombreDirectorio = '';
            $nombreArchivo = '';

            // <editor-fold defaultstate="collapsed" desc="XLS">
            //Cargo la imágen1     
            $objeto_id = $common->GetaaaaMMddhhmmssmm();
            if ($upload->has('archivoupload')) {
                $nombreArchivo = $objeto_id . '.' . end(explode(".", $upload->getName('archivoupload')));
                $nombreDirectorio = 'Archivos/Importar/';
                $upload->save('archivoupload', $nombreDirectorio . $nombreArchivo);

                $nombreCompleto = realpath($nombreDirectorio . $nombreArchivo);
                //ARMAR EL ARREGLO DESDE EL EXCEL
                require_once 'library/Excel/reader.php';
                $data = new Spreadsheet_Excel_Reader();
                $data->read($nombreCompleto);

                $errores = '';
                $articulosXls = array();
                $saltodelinea = '';

                for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
                    $Linea = $data->sheets[0]['cells'][$i];
                    if ($Linea != null) {
                                                
                        $ResultadoValidacion = $this->ValidaLinea($Linea);
                        if ($ResultadoValidacion == 'ok') {
                            //MARCA colA
                            $colA = $data->sheets[0]['cells'][$i][1];
                            if ($colA == '') {
                                $colA = 'DES';
                            }

                            //MODELO colB
                            $colB = $data->sheets[0]['cells'][$i][2];

                            //TIPO colC
                            $colC = $data->sheets[0]['cells'][$i][3];

                            //GAMA colD
                            $colD = $data->sheets[0]['cells'][$i][4];

                            //COD INTERNO colE
                            $colE = $data->sheets[0]['cells'][$i][5];

                            //CON COD DE BARRA colF
                            $colF = $data->sheets[0]['cells'][$i][6];

                            //IVA colG
                            $colG = str_replace(",", ".", $data->sheets[0]['cells'][$i][7]);
                            
//                                //CON precio vta colG
//                                $colG = $data->sheets[0]['cells'][$i][7];
//                                $flo = str_replace(",", ".", $colG);
//                                $colG = $flo;

                            $colH = $data->sheets[0]['cells'][$i][8];

                            $articulosXls[] = array(
                                'marca' => $colA,
                                'modelo' => $colB,
                                'tipoproducto' => $colC,
                                'tipogama' => $colD,
                                'codinterno' => $colE,
                                'concodigodebarra' => $colF,
                                'iva'   => $colG,
                                'idcelsis' => $colH
                            );
                        } else {
                            //meto en un array de errores
                            $errores .= $saltodelinea . 'Linea: ' . $i . ' (' . $ResultadoValidacion . ')';
                            $saltodelinea = '<br>';
                        }
                    }
                }

                if ($errores != '') {

                    //CARGO UNA VARIABLE CON LOS ERRORES Y LOS MUESTRO EN ROJO SIN SUBIR NADA

                    $this->error['texto_error'] = $errores;
                } else {
                    if ($upload->getError('archivoupload') > 0) {
                        $this->error['archivo'] = $upload->getErrorMsg($upload->getError('archivoupload'));
                    } else {
                        //HAGO LOS INSERT ACA
                        foreach ($articulosXls as $art) {
                            
                        // <editor-fold defaultstate="collapsed" desc="VARIABLES MARCA MODELO TIPO GAMA">
                            $descmarca = str_replace(array( "\'", '´', ',', '.', '{', '}', '+', '´', '\'','*', '¨', '[', ']', '%', '&', '/', '%', '\$', '#', '"', '!', '?', '¡'), '', utf8_encode($art['marca']));
                            $sqlquery = "select * from marcas where marca ='" . $descmarca . "' OR descripcion = '" . $descmarca . "'";
                            //$marca = $database->getRow("select * from marcas where marca ='" . $art['marca'] . "' OR descripcion = '" . $art['marca'] . "'");
                            $marca = $database->getRow($sqlquery);
                            $marcaid = 'DES';
                            if ($art['marca'] != '' && $art['marca'] != 'DESCONOCIDO') {
                                $marca_info = $database->getRow("select * from marcas where marca ='" . $descmarca . "' OR descripcion = '" . $descmarca . "'");
                                $marcaid = $marca_info['marca'];
                            }
                            
                            $modeloid = '';
                            $descmodlelo = str_replace(array( "\'", '´', ',', '.', '{', '}', '+', '´', '\'','*', '¨', '[', ']', '%', '&', '/', '%', '\$', '#', '"', '!', '?', '¡'), '', utf8_encode($art['modelo']));
                            $sqlquery = "select * from modelos where modelo ='" . $descmodlelo . "' OR descripcion = '" . $descmodlelo . "'";
                            $modelo = $database->getRow($sqlquery);
                            if (!$modelo) {
                                $id = str_replace(array( "\'", '´', ',', '.', '{', '}', '+', '´', '\'','*', '¨', '[', ']', '%', '&', '/', '%', '\$', '#', '"', '!', '?', '¡'), '', utf8_decode($art['modelo']));
                                //$id2=str_repalce(' ','_',$id);
                                if (strlen($id) > 50) {
                                    $id = substr($id, 0, 49);
                                }

                                $sql = "INSERT IGNORE INTO modelos SET modelo = '?', descripcion = '?',  marca = '?'";
                                $consulta = $database->parse($sql, $id, $descmodlelo, utf8_encode($marca['marca']));
                                $database->query($consulta);
                                $modeloid = $id;
                            } else {
                                $modeloid = $modelo['modelo'];
                            }

                            $tipoproducto = '5';
                            if ($art['tipoproducto'] != '' && $art['tipoproducto'] != 'ACCESORIO') {
                                $tipoproducto_info = $database->getRow("select * from tiposproducto where descripcion ='" . $art['tipoproducto'] . "' ");
                                $tipoproducto = $tipoproducto_info['tipoproducto'];
                            }

                            $iva = '21';
                            if ($art['iva'] != '' && $art['iva'] != 'SIN IVA') {
                                //$tipogama_info = $database->getRow("select * from tiposgama where descripcion ='" . $art['tipogama'] . "' ");
                                $iva = $art['iva'];
                            }
                            
                            $tipogama = 'SG';
                            if ($art['tipogama'] != '' && $art['tipogama'] != 'SIN GAMA') {
                                $tipogama_info = $database->getRow("select * from tiposgama where descripcion ='" . $art['tipogama'] . "' ");
                                $tipogama = $tipogama_info['tipogama'];
                            }
                            
                            $codinterno = $art['codinterno'];
                            if ($art['codinterno'] == '') {
                                $codinterno = $common->generaCodInternoProducto($tipoproducto);
                            }
                            // </editor-fold>
                            
                        //primero tengo que buscar el articulo por idcelsis
                        //si esta va a un update
                        
                        //si no esta
                        //busco por marca modelo tipo
                        //si esta va a un update
//                            $encontrado = 'no';
//                        if ($art['idcelsis'] == '4985' || $art['codbarrainterno'] == 'aicr-001615') {
//                            $encontrado = 'si';
//                        }
                        //si no esta va al insert
                            $existe = 'no';
                            if ($art['idcelsis'] != '') {
                                $verifica = $database->getRow("select count(*) AS verifica from articulos where idcelsis = '".$art['idcelsis']."' AND idcelsis != 0 ");
                                if ($verifica['verifica'] > 0) {
                                    $existe = 'si';
                                }
                            }
                            if ($existe == 'si') {
                                
                              // <editor-fold defaultstate="collapsed" desc="UPDATE">
                                $sql = "UPDATE articulos SET  tipoproducto='?', marca='?', modelo = '?',iva = '?',  tipogama='?', codbarrainterno='?', au_usuario='?',au_accion='M',au_fecha_hora=NOW() WHERE idcelsis ='?' ";
                                $consulta = $database->parse($sql, $tipoproducto, $marcaid,$modeloid, $iva,$tipogama, $codinterno, $user->getPERSONA(), $art['idcelsis']);
                                $database->query($consulta);
                              // </editor-fold>
                            }
                            else{
                                     
                                $verifica = $database->getRow("select count(*) AS verifica from articulos where tipoproducto='".$tipoproducto."' AND marca='".$marcaid."' AND modelo = '".$modeloid."' ");
                                if ($verifica['verifica'] > 0) {
                                    $existe = 'si';
                                }
                                if ($existe == 'si') {
                                
                                // <editor-fold defaultstate="collapsed" desc="UPDATE">
                                $sql = "UPDATE articulos SET iva='?',idcelsis ='?', tipogama='?', codbarrainterno='?', au_usuario='?',au_accion='M',au_fecha_hora=NOW() WHERE tipoproducto='?' AND marca='?' AND modelo = '?' ";
                                $consulta = $database->parse($sql, $iva,$art['idcelsis'],$tipogama, $codinterno, $user->getPERSONA(), $tipoproducto, $marcaid,$modeloid);
                                $database->query($consulta);
                              // </editor-fold>
                                }
                                else{
                                  // <editor-fold defaultstate="collapsed" desc="INSERT">
                                    $concoddebarra = 0;
                                    if ($art['concodigodebarra'] != '') {
                                        $concoddebarra = 1;
                                    }

                                    $valida = $database->getRow("select count(*) AS valida from articulos where modelo ='" . $modeloid . "' AND tipoproducto = '" . $tipoproducto . "'");
                                    if ($valida['valida'] == 0) {
                                                        
                                        //$sql2 = "INSERT IGNORE INTO articulos SET tipoproducto='".$request->get('tipoproducto', 'post')."', modelo = '".$request->get('auto_modelo_marca', 'post')."', estadoarticulo = 'DI', tipogama='".$request->get('tipogama', 'post')."', mnu='".$request->get('tipogama', 'post')."', concoddebarra='".$request->get('concoddebarra', 'post')."', precio='".$request->get('precio', 'post')."', au_usuario='".$user->getPERSONA()."',au_accion='A',au_fecha_hora=NOW()";
                                        $sql = "INSERT IGNORE INTO articulos SET iva='?',tipoproducto='?', marca='?', modelo = '?', estadoarticulo = 'DI', tipogama='?', concoddebarra='?', codbarrainterno='?',idcelsis='?',precioultcompra='0', au_usuario='?',au_accion='A',au_fecha_hora=NOW()";
                                        $consulta = $database->parse($sql, $iva,$tipoproducto, $marcaid,$modeloid, $tipogama, $concoddebarra, $codinterno, $art['idcelsis'], $user->getPERSONA());
                                        $database->query($consulta);
                                    }
                                  // </editor-fold>    
                                }
                            }    
                            
                        }

                        $cache->delete('articulos');

                        $session->set('message', 'Se importaron de forma correcta todos los artículos.');

                        $response->redirect($url->ssl('articulos'));
                    }
                }
            }

            // </editor-fold>
        }

        $template->set('content', $this->getFormImport());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function exportXlsModelo() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        // </editor-fold>

        set_time_limit(0);

        //** PHPExcel **//
        require_once (DIR_INCLUDES . '/library/phpexcel/PHPExcel.php');

        date_default_timezone_set('America/Argentina/Buenos_Aires');
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set properties of the file
        $objPHPExcel->getProperties()->setCreator("NombreEmpresa")
                ->setLastModifiedBy("NombreEmpresa")
                ->setTitle("Planilla web exportada: " . date('d-m-Y'))
                ->setSubject("Planilla web exportada: " . date('d-m-Y'));

        //genero las columnas del excel
        $letra = 'A';
        $fila_inicial = 1; //fila desde donde empiezo a cargar la tabla en el excel. Arriba va el encabezado
        // <editor-fold defaultstate="collapsed" desc="PROPIEDADES DE LAS COLUMNAS">
        $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //COLUMNA A
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'MARCA (Default: DESCONOCIDO)');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA B
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'MODELO');
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA C
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(37);
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', 'TIPO PRODUCTO(Defecto: ACCESORIO)');
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA D
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(26);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', 'GAMA (Defecto: SIN GAMA)');
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA E
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(14);
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', 'COD INTERNO (Auto genera)');
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        //COLUMNA F
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(37);
        $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', 'CON COD BARRA (Defecto: Sin código)');
        $objPHPExcel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        //COLUMNA G
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getStyle('G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('G1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1', 'IVA (Defecto: 21,00)');
        $objPHPExcel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        //COLUMNA H
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getStyle('H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('H1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1', 'ID Celsis');
        $objPHPExcel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        // </editor-fold>
        // Nombre de la hoja del libro
        $objPHPExcel->getActiveSheet()->setTitle('ModeloImportArticulos');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Save Excel 2007 file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');

        // It will be called file.xls | esta linea hace que sea una descarga y no una pestaña aparte
        header('Content-Disposition: attachment; filename="ModeloImportArticulos.xls"');

        header('Cache-Control: max-age=0');

        // Write file to the browser
        $objWriter->save('php://output');
    }

    function validateFormImport() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $upload = & $this->locator->get('upload');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        if ($upload->has('archivoupload')) {
            $tiposPermitidos = array(
                'application/vnd.ms-excel',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'application/vnd.ms-excel'
            );

            if (!in_array($upload->getType('archivoupload'), $tiposPermitidos)) {
                $errores = 'No es un documento v&aacute;lido para cargar (' . $upload->getType('archivoupload') . ')';
            }
        } elseif ($upload->hasError('archivoupload')) {
            $errores = $upload->getErrorMsg($upload->getError('archivoupload'));
        } else {
            $errores = 'Debe elegir un documento excel para cargar.';
        }


        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function ValidaLinea(array $Linea) {
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $upload = & $this->locator->get('upload');
        $common = & $this->locator->get('common');
        $user = & $this->locator->get('user');


        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        //valida que la marca exista
        //MARCA colA
        $colA = $Linea[1];
        if ($colA != null && $colA != '') {
            $verifica = $database->getRow("select count(marca) as verifica from marcas where marca = '" . utf8_encode($colA) . "' OR descripcion = '" . utf8_encode($colA) . "' ");
            if ($verifica["verifica"] == 0) {
                return 'Marca no existe.';
            }
        }

        //valida que haya ingresado un modelo
        $colB = $Linea[2];
        if ($colB == null && $colB == '') {
            return 'Debe ingresar un modelo.';
        }

        $colC = $Linea[3];
        if ($colC != '' && $colC != 'ACCESORIO') {
            $verifica = $database->getRow("select count(*) AS verifica from tiposproducto where descripcion ='" . $colC . "' ");
            if ($verifica['verifica'] == 0) {
                return 'Tipo de Producto no existe.';
            }
        }

        $colD = $Linea[4];
        if ($colD != '' && $colD != 'SIN GAMA') {
            $verifica = $database->getRow("select count(*) AS verifica from tiposgama where descripcion ='" . $colD . "' ");
            if ($verifica['verifica'] == 0) {
                return 'Tipo de Gama no existe.';
            }
        }

        $colE = $Linea[5];
        
        $colH = $Linea[8];
        
        if ($colE != '') {
            $verifica = $database->getRow("select count(*) AS verifica from articulos where codbarrainterno ='" . $colE . "' AND idcelsis != '".$colH."' ");
            if ($verifica['verifica'] > 0) {
                return "El código interno " . $colE . " ya se encuentra asignado a un artículo.";
            }
        }

        $colG = $Linea[7];
        $colG = str_replace(",", ".", $colG);
        if ($colG != null && $colG != '') {
            $verifica = $database->getRow("select count(*) AS verifica from ivas where iva ='" . $colG . "'  ");
            if ($verifica['verifica'] == 0) {
                return "IVA no existe.";
            }
        }
//                $colG = $Linea[7];
//                if ($colG != null) {
//                           if (!is_numeric($colG)) {
//                                return 'Debe ingresar un número valido en la columna G';
//                            }
//                        }
        // </editor-fold>

        return 'ok';
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        
        if ($request->get('articulo', 'post') == '') {
            $sql = "SELECT count(*) as total FROM vw_grilla_articulos WHERE marca = '?' and modelo = '?' and tipoproducto='?' ";
            $sql = $database->parse($sql, $request->get('auto_marca_id', 'post'), $request->get('modelo', 'post'), $request->get('tipoproducto', 'post'));
            $equipo = $database->getRow($sql);

            if ($equipo['total'] > 0) {
                $errores .= 'Esta articulo ya existe en el sistema. <br>';
            }
        }
        
        if ($request->get('articulo', 'post') != '') {
            $sql = "SELECT count(*) as total FROM vw_grilla_articulos WHERE marca = '?' and modelo = '?' and tipoproducto='?' and articulo != '?'";
            $sql = $database->parse($sql, $request->get('auto_marca_id', 'post'), $request->get('modelo', 'post'), $request->get('tipoproducto', 'post'), $request->get('articulo', 'post'));
            $equipo = $database->getRow($sql);

            if ($equipo['total'] > 0) {
                $errores .= 'Esta articulo ya existe en el sistema. <br>';
            }
        }

        if ($request->get('iva', 'post') == '-1') {
            $errores .= 'Debe seleccionar un IVA. <br>';
        }
        
        if ($request->get('tipoproducto', 'post') == '-1') {
            $errores .= 'Debe seleccionar un tipo de producto. <br>';
        }

        if (strlen($request->get('auto_marca_id', 'post')) == 0) {
            $errores .= 'Debe ingresar una marca. <br>';
        }

        if (strlen($request->get('modelo', 'post')) == 0) {
            $errores .= 'Debe ingresar un modelo. <br>';
        }

        if ($request->get('action') == 'update') {
            if (strlen($request->get('codbarrainterno', 'post')) > 0) {
             $verifica = $database->getRow("select count(*) AS verifica from articulos where codbarrainterno ='" . $request->get('codbarrainterno', 'post') . "' and articulo != '" . $request->get('articulo', 'get') ."' ");
            if ($verifica['verifica'] > 0) {
                $errores .= "El código interno " . $request->get('codbarrainterno', 'post') . " ya se encuentra asignado a un artículo.";
            }
            }
        }
        else{
           if (strlen($request->get('codbarrainterno', 'post')) > 0) {
             $verifica = $database->getRow("select count(*) AS verifica from articulos where codbarrainterno ='" . $request->get('codbarrainterno', 'post') . "'  ");
            if ($verifica['verifica'] > 0) {
                $errores .= "El código interno " . $request->get('codbarrainterno', 'post') . " ya se encuentra asignado a un artículo.";
            }
            } 
        }
        
        
            
        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';

        //VERIFICAR QUE NO ESTE EN UN DUA
        $sql = "SELECT count(*) as total FROM duadetalles WHERE articulo = '?' ";
        $verifica_duadetalle = $database->getRow($database->parse($sql, $request->get('articulo', 'get')));
        if ($verifica_duadetalle['total'] > 0) {
            $errores .= 'No es posible eliminar este artículo ya que se encuentra asignado a un DUA. <br>';
        }
        
        //VERIFICAR QUE NO ESTE EN UNA VENTA
        $sql = "SELECT count(*) as total FROM ventadetalles WHERE articulo = '?' ";
        $verifica_ventadetalle = $database->getRow($database->parse($sql, $request->get('articulo', 'get')));
        if ($verifica_ventadetalle['total'] > 0) {
            $errores .= 'No es posible eliminar este artículo ya que se encuentra asignado a una venta. <br>';
        }
           
        //VERIFICAR QUE NO ESTE EN UN EQUIPO DE REPARACION
        $sql = "SELECT count(*) as total FROM equipos WHERE articulo = '?' ";
        $sql = $database->parse($sql, $request->get('articulo', 'get'));
        $verifica_equipos = $database->getRow($sql);
        if ($verifica_equipos['total'] > 0) {
            $errores .= 'No es posible eliminar este artículo ya que se encuentra asignado a un equipo. <br>';
        }
        
        //VERIFICAR QUE NO ESTE EN UNA LISTA DE PRECIO
        $sql = "SELECT count(*) as total FROM listasdeprecioarticulos WHERE articulo = '?' ";
        $verifica_listasdeprecioarticulos = $database->getRow($database->parse($sql, $request->get('articulo', 'get')));
        if ($verifica_listasdeprecioarticulos['total'] > 0) {
            $errores .= 'No es posible eliminar este artículo ya que se encuentra asignado a una lista de precio. <br>';
        }
        
        if ($errores != '') {
            $this->error['message'] = $errores;
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $user = & $this->locator->get('user');
        $url = & $this->locator->get('url');
        $common = & $this->locator->get('common');
        // </editor-fold>

        $template->set('title', 'ARTICULOS');

        if (($request->isPost()) && ($this->validateForm())) {
            $concoddebarra = 0;
            if ($request->get('concoddebarra', 'post') == 'concoddebarra') {
                $concoddebarra = 1;
            }
            //$sql2 = "INSERT IGNORE INTO articulos SET tipoproducto='".$request->get('tipoproducto', 'post')."', modelo = '".$request->get('auto_modelo_marca', 'post')."', estadoarticulo = 'DI', tipogama='".$request->get('tipogama', 'post')."', mnu='".$request->get('tipogama', 'post')."', concoddebarra='".$request->get('concoddebarra', 'post')."', precio='".$request->get('precio', 'post')."', au_usuario='".$user->getPERSONA()."',au_accion='A',au_fecha_hora=NOW()";
           
             $sql = "INSERT IGNORE INTO modelos SET modelo = UPPER('?'), descripcion = UPPER('?'),  marca = '?'";
            $consulta = $database->parse($sql, $request->get('modelo', 'post'), $request->get('modelo', 'post'), $request->get('auto_marca_id', 'post'));
            $database->query($consulta);
            
            $cod = $request->get('tipoproducto', 'post');
            $codinterno = $request->get('codbarrainterno', 'post');
            if (strlen($request->get('codbarrainterno', 'post')) == 0) {
                $codinterno = $common->generaCodInternoProducto($request->get('tipoproducto', 'post'));
            }
            $sql = "INSERT IGNORE INTO articulos SET tipoproducto='?', marca='?', modelo = UPPER('?'),iva = '?', estadoarticulo = 'DI', tipogama='?', mnu='?', concoddebarra='?', codbarrainterno='?', au_usuario='?',au_accion='A',au_fecha_hora=NOW()";
            $consulta = $database->parse($sql, $request->get('tipoproducto', 'post'), $request->get('auto_marca_id', 'post'),$request->get('modelo', 'post'),$request->get('iva', 'post'), $request->get('tipogama', 'post'), $request->get('mnu', 'post'), $concoddebarra, $codinterno, $user->getPERSONA());
            $database->query($consulta);

            $id = $database->getLastId();
            $cache->delete('articulos');
            $session->set('message', 'Se agreg&oacute; el articulo: ' . $id);

            $response->redirect($url->ssl('articulos'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $user = & $this->locator->get('user');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos del Articulo');
        if (($request->isPost()) && ($this->validateForm())) {

            $id = $request->get('articulo', 'post');

            $concoddebarra = 0;
            if ($request->get('concoddebarra', 'post') == 'concoddebarra') {
                $concoddebarra = 1;
            }

            $sql = "INSERT IGNORE INTO modelos SET modelo = '?', descripcion = '?',  marca = '?'";
            $consulta = $database->parse($sql, $request->get('modelo', 'post'), $request->get('modelo', 'post'), $request->get('auto_marca_id', 'post'));
            $database->query($consulta);
            
            $sql = "UPDATE articulos SET tipoproducto='?', marca='?' ,modelo = '?',iva = '?', tipogama='?', mnu='?', concoddebarra='?',codbarrainterno='?', au_usuario='?',au_accion='M',au_fecha_hora=NOW() WHERE articulo='?' ";
            $consulta = $database->parse($sql, $request->get('tipoproducto', 'post'), $request->get('auto_marca_id', 'post'),$request->get('modelo', 'post'),$request->get('iva', 'post'), $request->get('tipogama', 'post'), $request->get('mnu', 'post'), $concoddebarra, $request->get('codbarrainterno', 'post'), $user->getPERSONA(), $id);
            $database->query($consulta);

            $cache->delete('articulos');

            $session->set('message', 'Se actualiz&oacute; el articulo: ' . $id);

            $response->redirect($url->ssl('articulos'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos del articulo');
        if (($request->isPost())) {

            $response->redirect($url->ssl('articulos'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        if (($request->get('articulo')) && ($this->validateDelete())) {

            $id = $request->get('articulo', 'get');

            $sql = "DELETE FROM articulos WHERE articulo = '" . $id . "' ";
            $database->query($sql);
            
            $cache->delete('articulos');

            $session->set('message', 'Se ha eliminado el articulo: ' . $id);

            $response->redirect($url->ssl('articulos'));
        }

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function exportXls() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        // </editor-fold>

        set_time_limit(0);

        //** PHPExcel **//
        require_once (DIR_INCLUDES . '/library/phpexcel/PHPExcel.php');

        date_default_timezone_set('America/Argentina/Buenos_Aires');
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set properties of the file
        $objPHPExcel->getProperties()->setCreator("NombreEmpresa")
                ->setLastModifiedBy("NombreEmpresa")
                ->setTitle("Planilla web exportada: " . date('d-m-Y'))
                ->setSubject("Planilla web exportada: " . date('d-m-Y'));

        //genero las columnas del excel
        $letra = 'A';
        $fila_inicial = 2; //fila desde donde empiezo a cargar la tabla en el excel. Arriba va el encabezado
        // <editor-fold defaultstate="collapsed" desc="PROPIEDADES DE LAS COLUMNAS">
        $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //COLUMNA A
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(11);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'ID Articulo');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        //COLUMNA B
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'MARCA');
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA C
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(22);
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', 'MODELO');
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA D
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', 'TIPO PRODUCTO');
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA E
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', 'GAMA');
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA F
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(14);
        $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', 'COD INTERNO');
        $objPHPExcel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        //COLUMNA G
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getStyle('G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('G1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1', 'COD DE BARRA');
        $objPHPExcel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        //COLUMNA H
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getStyle('H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('H1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1', 'PRECIO VTA');
        $objPHPExcel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        //COLUMNA I
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getStyle('I1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('I1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I1', 'IVA');
        $objPHPExcel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        // </editor-fold>
        //CARGA PRODUCTOS
        //// <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        set_time_limit(0);

        if (!$session->get('articulos.search')) {
            $sql = "SELECT * FROM  vw_grilla_articulostodos  ";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT * FROM vw_grilla_articulostodos WHERE articulo LIKE '?' OR descestadoarticulo LIKE '?' OR desctipoproducto LIKE '?' OR descmodelo LIKE '?' OR descmarca LIKE '?' ";
            $conca = " AND ";
        }

        if ($session->get('articulos.tipoproducto') != '-1' && $session->get('articulos.tipoproducto') != '') {
            $sql .= $conca . " tipoproducto = '" . $session->get('articulos.tipoproducto') . "'  ";
        }

        $sql .= " ORDER BY marca, modelo ASC";

//		$consul = $database->splitQuery($database->parse($sql,'%' . $session->get('articulos.search') . '%','%' . $session->get('articulos.search') . '%'), $session->get('articulos.page'), $config->get('config_max_rows'));
//
//                $results = $database->getRows($consul);

        $consulta = $database->parse($sql, '%' . $session->get('articulos.search') . '%', '%' . $session->get('articulos.search') . '%', '%' . $session->get('articulos.search') . '%', '%' . $session->get('articulos.search') . '%', '%' . $session->get('articulos.search') . '%');
        $results = $database->getRows($consulta);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="LLANAR LOS DATOS">

        $num = $fila_inicial;

        foreach ($results as $result) {

            //COLUMNA A
            $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $num, $result['articulo']);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            //COLUMNA B
            $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $num, $result['descmarca']);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            //COLUMNA C
            $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $num, $result['descmodelo']);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA D
            $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' . $num, $result['desctipoproducto']);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA E
            $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' . $num, $result['descgama']);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA F
            $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' . $num, $result['codbarrainterno']);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA G
            $objPHPExcel->getActiveSheet()->getStyle('G' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G' . $num, $result['codbarra']);
            $objPHPExcel->getActiveSheet()->getStyle('G' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            //HACE QUE EL NUMERO DE VENTA SALGA EN FORMATO CORRECTO
             $objPHPExcel->getActiveSheet()->getStyle('G' . $num)->getNumberFormat()->setFormatCode('0000');
                   
            //COLUMNA H
            $objPHPExcel->getActiveSheet()->getStyle('H' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H' . $num, '0');
            $objPHPExcel->getActiveSheet()->getStyle('H' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA I
            $objPHPExcel->getActiveSheet()->getStyle('I' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I' . $num, $result['iva']);
            $objPHPExcel->getActiveSheet()->getStyle('I' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $num++;
        }

        //SEGUIR EN COMPLETAR EL EXCEL
        //POR CADA INSCRIPTO IR BUSCANDO CADA RESPUESTA Y COMPLETAR SEGUN CORRESPONDA
        // </editor-fold>
        // Nombre de la hoja del libro
        $objPHPExcel->getActiveSheet()->setTitle('ExportArticulos');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Save Excel 2007 file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');

        // It will be called file.xls | esta linea hace que sea una descarga y no una pestaña aparte
        header('Content-Disposition: attachment; filename="ExportArticulos.xls"');

        header('Cache-Control: max-age=0');

        // Write file to the browser
        $objWriter->save('php://output');
    }

    function getMarcas() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        // </editor-fold>

        $miDescripcion = $request->get('term', 'get');

        $sql = "SELECT marca as id, descripcion as value FROM marcas WHERE marca like '?' or descripcion like '?'";
        $consulta = $database->parse($sql, '%' . $miDescripcion . '%', '%' . $miDescripcion . '%');
        $result = $database->getRows($consulta);

        $varia = utf8_decode(json_encode($result));

        echo $varia;
    }
    
    function getModelos() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        // </editor-fold>

        $miDescripcion = $request->get('term', 'get');

        $sql = "SELECT modelo as id, descripcion as value FROM modelos WHERE modelo like '?' or descripcion like '?'";
        $consulta = $database->parse($sql, '%' . $miDescripcion . '%', '%' . $miDescripcion . '%');
        $result = $database->getRows($consulta);

        $varia = utf8_decode(json_encode($result));

        echo $varia;
    }
    
//    function getModelos() {
//        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
//        $database = & $this->locator->get('database');
//        $request = & $this->locator->get('request');
//        $session = & $this->locator->get('session');
//        $user = & $this->locator->get('user');
//        // </editor-fold>
//
//        $idMarca = $request->get('idMarca', 'get');
//        try {
//            $sql = "SELECT modelo as modelo, descripcion FROM vw_list_modelos WHERE marca = '?'";
//            $consulta = $database->parse($sql, $idMarca);
//            $result = $database->getRows($consulta);
//        } catch (Exception $e) {
//            echo "<option value='0' selected>" . $e->getMessage() . "</option>";
//            return;
//        }
//
//        $opt .= "<option value='-1' selected>Seleccione...</option>";
//        foreach ($result as $item):
//            $opt .= "<option value='" . $item['modelo'] . "'>" . $item['descripcion'] . "</option>";
//        endforeach;
//
//        echo $opt;
//    }

    function getModelo() {
        $request = & $this->locator->get('request');
//		$response =& $this->locator->get('response');
        $database = & $this->locator->get('database');
//                $view = $this->locator->create('template');

        $miDescripcion = $request->get('term', 'get');
        //$miPersona = "lucas";
        // $codigo = $database->getRows("select concat(apellido,', ', nombre) as value, persona_id  as id from personas where persona_id ='" . $miPersona . "'");		

        $sql = "SELECT modelo  as id, CONCAT(descmarca,' - ',descripcion) as value FROM vw_list_modelos WHERE descripcion LIKE '?' OR descmarca LIKE '?'";
        $consulta = $database->parse($sql, '%' . $miDescripcion . '%', '%' . $miDescripcion . '%');
        $codigo = $database->getRows($consulta);

        $varia = utf8_decode(json_encode($codigo));


        echo $varia;
    }

}

?>