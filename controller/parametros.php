<?php

class ControllerParametros extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'GESTIÓN DE PARAMETROS');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
        $request = & $this->locator->get('request');
        // </editor-fold>
         // <editor-fold defaultstate="collapsed" desc="LIMPIA FILTRO Y VARIBLES DE SESSION">

        if ($request->get('filtra') == NULL) {

            $session->set('parametros.search', '');
            $session->set('parametros.sort', '');
            $session->set('parametros.order', '');
            $session->set('parametros.page', '');

            $view->set('search', '');
            $view->set('parametros.search', '');
                        
            $cache->delete('parametros');
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">

        $cols = array();

        $cols[] = array(
            'name' => 'Parametro',
            'sort' => 'parametro',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Valor',
            'sort' => 'valor',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Descripción',
            'sort' => 'descripcion',
            'align' => 'left'
        );


//                $cols[] = array(
//			'name'  => 'PRECIO VTA',
//			'sort'  => 'precio',
//			'align' => 'left'
//		);

        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'parametro',
            'valor',
            'descripcion'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        set_time_limit(0);

        if (!$session->get('parametros.search')) {
            $sql = "SELECT * FROM parametrosconfiguracion  ";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT * FROM parametrosconfiguracion WHERE parametro LIKE '?' OR valor LIKE '?' OR descripcion LIKE '?'  ";
            $conca = " AND ";
        }

        if (in_array($session->get('parametros.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('parametros.sort') . " " . (($session->get('parametros.order') == 'desc') ? 'desc' : 'asc');
        } else {
            $sql .= " ORDER BY parametro ASC";
        }

//		$consul = $database->splitQuery($database->parse($sql,'%' . $session->get('parametros.search') . '%','%' . $session->get('parametros.search') . '%'), $session->get('parametros.page'), $config->get('config_max_rows'));
//
//                $results = $database->getRows($consul);

        $consulta = $database->parse($sql, '%' . $session->get('parametros.search') . '%', '%' . $session->get('parametros.search') . '%', '%' . $session->get('parametros.search') . '%');
        $results = $database->getRows($consulta);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['parametro'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['valor'],
                'align' => 'left',
                'default' => 0
            );
            $cell[] = array(
                'value' => @$result['descripcion'],
                'align' => 'left',
                'default' => 0
            );

//                        $precio = 0;
//                        if (@$result['preciovta']) {
//                            $precio = @$result['preciovta'];
//                        }
//                         $cell[] = array(
//				'value'   => $precio,
//				'align'   => 'left',
//				'default' => 0
//			);

            $action = array();

            if ($user->hasPermisos($user->getPERSONA(), 'parametros', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa fa-fw fa-pencil',
                    'text' => $language->get('button_update'),
                    'prop_a' => array('href' => $url->ssl('parametros', 'update', array('parametro' => $result['parametro'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'parametros', 'B')) {
                $action[] = array(
                    'icon' => 'img/iconos-11.png',
                    'text' => $language->get('button_delete'),
                    'class' => 'fa fa-fw fa-trash-o',
                    'prop_a' => array('href' => "javascript:ActionDelete('" . $url->ssl('parametros', 'delete', array('parametro' => $result['parametro'])) . "');")
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'parametros', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw fa-search',
                    'text' => $language->get('button_consult'),
                    'prop_a' => array('href' => $url->ssl('parametros', 'consulta', array('parametro' => $result['parametro'])))
                );
            }

            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        //<editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('parametros.page'));

        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        $view->set('heading_title', 'PARAMETROS');
        $view->set('placeholder_buscar', 'BUSCA POR PARAMETRO O VALOR O DESCRIPCION');

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        //$view->set('consultaVacia', $url->ssl('consulta','consulta_vacia'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        //$view->set('button_consultavacia', "Vacía");
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        $view->set('error_texto_error', @$this->error['texto_error']);
        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $ver = $session->get('message');
        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('parametros.search'));
        $view->set('sort', $session->get('parametros.sort'));
        $view->set('order', $session->get('parametros.order'));
        $view->set('page', $session->get('parametros.page'));

        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $view->set('titulo_ventana', '::. Exportar');

        $view->set('entry_modificar', "Modificar");
        $view->set('entry_agregar', "Agregar");

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('parametros', 'page'));
        $view->set('list', $url->ssl('parametros'));

        if ($user->hasPermisos($user->getPERSONA(), 'parametros', 'A')) {
            $view->set('insert', $url->ssl('parametros', 'insert'));
        }

        //$view->set('addPais', $url->ssl('parametros','insert'));
        // $view->set('updatePersona', $url->ssl('paciente','update', array('persona' => $result['persona'])));
        //$view->set('updatePais', $url->ssl('parametros','update'));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_parametros.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">
//		if ($request->has('modelo', 'post')) {
//                    $session->set('parametros.pais',$request->get('modelo','post'));              
//                }

        if ($request->has('search', 'post')) {
            $session->set('parametros.search', $request->get('search', 'post'));
        }

        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('parametros.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('parametros.order', (($session->get('parametros.sort') == $request->get('sort', 'post')) && ($session->get('parametros.order') == 'asc')) ? 'desc' : 'asc');
        }

        if ($request->has('sort', 'post')) {
            $session->set('parametros.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('parametros', 'index', array('filtra' => '1')));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $template = & $this->locator->get('template');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('heading_title', 'DATOS DEL PARAMETRO');
        $view->set('entry_parametro', 'Parametro');
        $view->set('entry_descripcion', 'Descripción');
        $view->set('entry_valor', 'Valor');

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $template->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('tab_general', $language->get('tab_general'));

        $view->set('parametro', $request->get('parametro'));

        if (($request->get('parametro')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM parametrosconfiguracion WHERE parametro = '" . $request->get('parametro') . "' ";
            $objeto_info = $database->getRow($consulta);
        }

        if ($request->has('parametro', 'post')) {
            $view->set('parametro', $request->get('parametro', 'post'));
            $objeto_id = @$objeto_info['parametro'];
        } else {
            $view->set('id', $request->get('parametro', 'get'));
            $objeto_id = @$objeto_info['parametro'];
        }

        if ($request->has('parametro', 'post')) {
            $view->set('parametro', $request->get('parametro', 'post'));
        } else {
            $view->set('parametro', @$objeto_info['parametro']);
        }

        if ($request->has('descripcion', 'post')) {
            $view->set('descripcion', $request->get('descripcion', 'post'));
        } else {
            $view->set('descripcion', @$objeto_info['descripcion']);
        }

        if ($request->has('valor', 'post')) {
            $view->set('valor', $request->get('valor', 'post'));
        } else {
            $view->set('valor', @$objeto_info['valor']);
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $template->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        $view->set('error_texto_error', @$this->error['texto_error']);
        //$view->set('error_titulo', @$this->error['titulo']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        //$view->set('action', $url->ssl('parametros', $request->get('action'), array('modelo' => $request->get('modelo'))));
        $view->set('actionBoton', $request->get('action'));
        $view->set('cancel', $url->ssl('parametros'));
        // </editor-fold>

        return $view->fetch('content/parametro.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        if ($request->get('accion_form', 'post') == 'insert') {
            if ((strlen($request->get('parametro', 'post')) == 0)) {
                $errores .= 'Debe ingresar un parametro. <br>';
            } else {
                $sql = "SELECT count(*) as total FROM parametros WHERE parametro = '?'";
                $valida = $database->getRow($database->parse($sql, $request->get('parametro', 'post')));

                if ($valida['total'] != 0) {
                    $errores .= 'Este parametro ya existe en el sistema. <br>';
                }
            }
        }

        if ((strlen($request->get('valor', 'post')) == 0)) {
            $errores .= 'Debe ingresar un valor. <br>';
        }

        if ((strlen($request->get('descripcion', 'post')) == 0)) {
            $errores .= 'Debe ingresar la descripción. <br>';
        }

        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';

        if ($errores != '') {
            $this->error['message'] = $errores;
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $user = & $this->locator->get('user');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'PARAMETROS');

        if (($request->isPost()) && ($this->validateForm())) {
            $id = $request->get('parametro', 'post');

            $sql = "INSERT IGNORE INTO parametrosconfiguracion SET parametro='?', descripcion = '?', valor = '?'";
            $consulta = $database->parse($sql, $id, $request->get('descripcion', 'post'), $request->get('valor', 'post'));
            $database->query($consulta);

            $cache->delete('parametros');
            $session->set('message', 'Se agreg&oacute; el parametro: ' . $id);

            $response->redirect($url->ssl('parametros'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $user = & $this->locator->get('user');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos del Parametro');
        if (($request->isPost()) && ($this->validateForm())) {

            $id = $request->get('parametro', 'post');
            $sql = "UPDATE parametrosconfiguracion SET valor='?', descripcion = '?' WHERE parametro='?' ";
            $consulta = $database->parse($sql, $request->get('valor', 'post'), $request->get('descripcion', 'post'), $id);
            $database->query($consulta);

            $cache->delete('parametros');

            $session->set('message', 'Se actualiz&oacute; el parametro: ' . $id);

            $response->redirect($url->ssl('parametros'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos del Parametro');
        if (($request->isPost())) {

            $response->redirect($url->ssl('parametros'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        if (($request->get('parametro')) && ($this->validateDelete())) {

            $id = $request->get('parametro', 'get');

            //TODO: FALTA PROGRAMAR LA ELIMINACION Y VALIDACION VER SI NO HAY RELACIONES
            $sql = "DELETE FROM parametrosconfiguracion  WHERE parametro = '" . $id . "' ";
            $database->query($sql);

            $cache->delete('parametros');

            $session->set('message', 'Se ha eliminado el parametro: ' . $id);

            $response->redirect($url->ssl('parametros'));
        }

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

}

?>