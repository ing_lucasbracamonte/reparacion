<?php

class ControllerListasdeprecio extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'GESTIÓN DE LISTAS DE PRECIO');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">

        $cols = array();

        $cols[] = array(
            'name' => 'Lista de precio',
            'sort' => 'listaprecio',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Descripci&oacute;n',
            'sort' => 'descripcion',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'listaprecio',
            'descripcion'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        set_time_limit(0);

        if (!$session->get('listasdeprecio.search')) {
            $sql = "SELECT * FROM listasdeprecio  ";
        } else {
            $sql = "SELECT * FROM listasdeprecio WHERE listaprecio LIKE '?' OR descripcion LIKE '?'";
        }

        if (in_array($session->get('listasdeprecio.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('listasdeprecio.sort') . " " . (($session->get('listasdeprecio.order') == 'desc') ? 'desc' : 'asc');
        } else {
            $sql .= " ORDER BY descripcion ASC";
        }

//		$consul = $database->splitQuery($database->parse($sql,'%' . $session->get('listasdeprecio.search') . '%','%' . $session->get('listasdeprecio.search') . '%'), $session->get('listasdeprecio.page'), $config->get('config_max_rows'));
//
//                $results = $database->getRows($consul);

        $consulta = $database->parse($sql, '%' . $session->get('listasdeprecio.search') . '%', '%' . $session->get('listasdeprecio.search') . '%');
        $results = $database->getRows($consulta);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['listaprecio'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['descripcion'],
                'align' => 'left',
                'default' => 0
            );


            $action = array();

            if ($user->hasPermisos($user->getPERSONA(), 'listasdeprecio', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa fa-fw fa-pencil',
                    'text' => $language->get('button_update'),
                    'prop_a' => array('href' => $url->ssl('listasdeprecio', 'update', array('listaprecio' => $result['listaprecio'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'listasdeprecio', 'A')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa fa-download',
                    'text' => 'Importar y actualizar',
                    'prop_a' => array('href' => $url->ssl('listasdeprecio', 'insertImport', array('listaprecio' => $result['listaprecio'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'listasdeprecio', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa fa-upload',
                    'text' => 'Exportar',
                    'prop_a' => array('href' => $url->ssl('listasdeprecio', 'exportXlsArticulos', array('listaprecio' => $result['listaprecio'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'listasdeprecio', 'B')) {
                $action[] = array(
                    'icon' => 'img/iconos-11.png',
                    'text' => $language->get('button_delete'),
                    'class' => 'fa fa-fw fa-trash-o',
                    'prop_a' => array('href' => "javascript:ActionDelete('" . $url->ssl('listasdeprecio', 'delete', array('listaprecio' => $result['listaprecio'])) . "');")
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'listasdeprecio', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw fa-search',
                    'text' => $language->get('button_consult'),
                    'prop_a' => array('href' => $url->ssl('listasdeprecio', 'consulta', array('listaprecio' => $result['listaprecio'])))
                );
            }



            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        //<editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('listasdeprecio.page'));

        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        $view->set('heading_title', 'Lista de precio');
        $view->set('placeholder_buscar', 'BUSCA POR ID O DESCRIPCION');

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        //$view->set('consultaVacia', $url->ssl('consulta','consulta_vacia'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        //$view->set('button_consultavacia', "Vacía");
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('listasdeprecio.search'));
        $view->set('sort', $session->get('listasdeprecio.sort'));
        $view->set('order', $session->get('listasdeprecio.order'));
        $view->set('page', $session->get('listasdeprecio.page'));

        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $view->set('titulo_ventana', '::. Exportar');

        $view->set('entry_modificar', "Modificar");
        $view->set('entry_agregar', "Agregar");

        $view->set('listaprecio', '');
        $view->set('search', '');
        $view->set('listasdeprecio.search', '');
        $cache->delete('listasdeprecio');

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('listasdeprecio', 'page'));
        $view->set('list', $url->ssl('listasdeprecio'));

        if ($user->hasPermisos($user->getPERSONA(), 'listasdeprecio', 'A')) {
            $view->set('insert', $url->ssl('listasdeprecio', 'insert'));
        }

        if ($user->hasPermisos($user->getPERSONA(), 'listasdeprecio', 'C'))
            $view->set('export', $url->ssl('listasdeprecio', 'exportar'));

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_listasdeprecio.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">
//		if ($request->has('listaprecio', 'post')) {
//                    $session->set('listasdeprecio.pais',$request->get('listaprecio','post'));              
//                }

        if ($request->has('search', 'post')) {
            $session->set('listasdeprecio.search', $request->get('search', 'post'));
        }


        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('listasdeprecio.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('listasdeprecio.order', (($session->get('listasdeprecio.sort') == $request->get('sort', 'post')) && ($session->get('listasdeprecio.order') == 'asc')) ? 'desc' : 'asc');
        }

        if ($request->has('sort', 'post')) {
            $session->set('listasdeprecio.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('listasdeprecio'));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $template = & $this->locator->get('template');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('heading_title', 'DATOS DE LA LISTA DE PRECIO');
        $view->set('entry_descripcion', 'Descripci&oacute;n:');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('tab_general', $language->get('tab_general'));

        $view->set('listaprecio', $request->get('listaprecio'));

        if (($request->get('listaprecio')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM listasdeprecio WHERE listaprecio = '" . $request->get('listaprecio') . "'";
            $objeto_info = $database->getRow($consulta);
        }

        if ($request->has('listaprecio', 'post')) {
            $view->set('listaprecio', $request->get('listaprecio', 'post'));
            $objeto_id = @$objeto_info['listaprecio'];
        } else {
            $view->set('listaprecio', $request->get('listaprecio', 'get'));
            $objeto_id = @$objeto_info['listaprecio'];
        }

        if ($request->has('descripcion', 'post')) {
            $view->set('descripcion', $request->get('descripcion', 'post'));
        } else {
            $view->set('descripcion', @$objeto_info['descripcion']);
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        $view->set('error_texto_error', @$this->error['texto_error']);
        //$view->set('error_titulo', @$this->error['titulo']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        //$view->set('action', $url->ssl('listasdeprecio', $request->get('action'), array('listaprecio' => $request->get('listaprecio'))));
        $view->set('actionBoton', $request->get('action'));
        $view->set('cancel', $url->ssl('listasdeprecio'));
        // </editor-fold>

        return $view->fetch('content/listaprecio.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        if ((strlen($request->get('descripcion', 'post')) == 0)) {
            $errores = 'Debe ingresar la descripción';
        }

        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';
        $consulta_info = $database->getRow("SELECT COUNT(*) as total FROM puntosdeventa WHERE listaprecio = '" . $request->get('listaprecio') . "'");
        if (@$consulta_info['total'] != 0) {
            $errores .= 'No es posible eliminar la lista de precio, esta asignada como mínimo a un punto de venta. <br>';
        }

        if ($errores != '') {
            $this->error['message'] = $errores;
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Lista de precio');

        if (($request->isPost()) && ($this->validateForm())) {
            $sql = "INSERT IGNORE INTO listasdeprecio SET descripcion = '?' ";
            $consulta = $database->parse($sql, $request->get('descripcion', 'post'));
            $database->query($consulta);

            $id = $database->getLastId();
            $cache->delete('listasdeprecio');
            $session->set('message', 'Se agreg&oacute; la lista de precio: ' . $id);

            $response->redirect($url->ssl('listasdeprecio'));
        }
        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos de la lista de precio');
        if (($request->isPost()) && ($this->validateForm())) {

            $id = $request->get('listaprecio', 'post');
            $sql = "UPDATE listasdeprecio SET descripcion ='?'  WHERE listaprecio='?' ";
            $consulta = $database->parse($sql, $request->get('descripcion', 'post'), $id);
            $database->query($consulta);

            $cache->delete('listasdeprecio');

            $session->set('message', 'Se actualiz&oacute; la lista de precio: ' . $id);

            $response->redirect($url->ssl('listasdeprecio'));
        }
        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos de la lista de precio');
        if (($request->isPost())) {
            $response->redirect($url->ssl('listasdeprecio'));
        }

        $response->set($this->getForm());
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        if (($request->get('listaprecio')) && ($this->validateDelete())) {

            $id = $request->get('listaprecio', 'get');

            //TODO: FALTA PROGRAMAR LA ELIMINACION Y VALIDACION VER SI NO HAY RELACIONES
            $sql = "DELETE FROM listasdeprecioarticulos WHERE listaprecio = '" . $id . "'  ";
            $database->query($sql);

            $sql = "DELETE FROM listasdeprecio WHERE listaprecio = '" . $id . "'  ";
            $database->query($sql);

            $cache->delete('listasdeprecio');

            $session->set('message', 'Se ha eliminado la lista de precio: ' . $id);

            $response->redirect($url->ssl('listasdeprecio'));
        }

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function exportXlsArticulos() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        // </editor-fold>

        set_time_limit(0);

        //** PHPExcel **//
        require_once (DIR_INCLUDES . '/library/phpexcel/PHPExcel.php');

        date_default_timezone_set('America/Argentina/Buenos_Aires');
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set properties of the file
        $objPHPExcel->getProperties()->setCreator("NombreEmpresa")
                ->setLastModifiedBy("NombreEmpresa")
                ->setTitle("Planilla web exportada: " . date('d-m-Y'))
                ->setSubject("Planilla web exportada: " . date('d-m-Y'));

        //genero las columnas del excel
        $letra = 'A';
        $fila_inicial = 2; //fila desde donde empiezo a cargar la tabla en el excel. Arriba va el encabezado
        // <editor-fold defaultstate="collapsed" desc="PROPIEDADES DE LAS COLUMNAS">
        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //COLUMNA A
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(11);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'ID Articulo');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        //COLUMNA B
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'MARCA');
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA C
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(22);
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', 'MODELO');
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA D
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', 'TIPO PRODUCTO');
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA E
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(14);
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', 'COD INTERNO');
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        //COLUMNA F
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', 'PRECIO VTA');
        $objPHPExcel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        // </editor-fold>
        //CARGA PRODUCTOS
        //// <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        set_time_limit(0);

        $sql = "SELECT *, lp.preciovta AS preciovtalista  FROM listasdeprecioarticulos lp LEFT JOIN vw_grilla_articulos vw ON lp.articulo = vw.articulo  WHERE lp.listaprecio = '" . $request->get('listaprecio', 'get') . "' ";

        $sql .= "GROUP BY lp.articulo ORDER BY marca, modelo ASC";

//		$consul = $database->splitQuery($database->parse($sql,'%' . $session->get('articulos.search') . '%','%' . $session->get('articulos.search') . '%'), $session->get('articulos.page'), $config->get('config_max_rows'));
//
//                $results = $database->getRows($consul);

        $results = $database->getRows($sql);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="LLANAR LOS DATOS">

        $num = $fila_inicial;

        foreach ($results as $result) {

            //COLUMNA A
            $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $num, $result['articulo']);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            //COLUMNA B
            $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $num, $result['descmarca']);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            //COLUMNA C
            $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $num, $result['descmodelo']);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA D
            $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' . $num, $result['desctipoproducto']);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA E
            $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' . $num, $result['codbarrainterno']);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA F
            $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' . $num, $result['preciovtalista']);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $num++;
        }

        //SEGUIR EN COMPLETAR EL EXCEL
        //POR CADA INSCRIPTO IR BUSCANDO CADA RESPUESTA Y COMPLETAR SEGUN CORRESPONDA
        // </editor-fold>
        // Nombre de la hoja del libro
        $objPHPExcel->getActiveSheet()->setTitle('ModeloImportArticulos');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Save Excel 2007 file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');

        // It will be called file.xls | esta linea hace que sea una descarga y no una pestaña aparte
        header('Content-Disposition: attachment; filename="ModeloImportArticulos.xls"');

        header('Cache-Control: max-age=0');

        // Write file to the browser
        $objWriter->save('php://output');
    }

    // <editor-fold defaultstate="collapsed" desc="Import y Update">
    function getFormImport() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $template = & $this->locator->get('template');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('heading_title', 'IMPORTAR Y ACTUALIZAR ARTÍCULOS DE LA LISTA DE PRECIOS');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('tab_general', $language->get('tab_general'));

        $view->set('listaprecio', $request->get('listaprecio'));
        if ($request->has('listaprecio', 'post')) {
            $view->set('listaprecio', $request->get('listaprecio', 'post'));
            $objeto_id = $request->get('listaprecio', 'post');
        } else {
            $view->set('listaprecio', $request->get('listaprecio', 'get'));
            $objeto_id = $request->get('listaprecio', 'get');
        }


        if (($request->get('listaprecio')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM listasdeprecio WHERE listaprecio = '" . $objeto_id . "' ";
            $objeto_info = $database->getRow($consulta);
        }

        $view->set('heading_title', 'IMPORTAR Y ACTUALIZAR ARTÍCULOS DE LA LISTA DE PRECIOS: ' . $objeto_info['descripcion']);
//                
//                if ($request->has('articulo', 'post')) {
//			$view->set('articulo', $request->get('articulo', 'post'));
//			$objeto_id = @$objeto_info['articulo'];
//		} else {
//			$view->set('id', $request->get('articulo', 'get'));
//			$objeto_id =@$objeto_info['articulo'];
//		}
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        $view->set('error_texto_error', @$this->error['texto_error']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        //$view->set('action', $url->ssl('articulos', $request->get('action'), array('modelo' => $request->get('modelo'))));
        $view->set('actionBoton', $request->get('action'));
        $view->set('cancel', $url->ssl('listasdeprecio'));

        $view->set('exportXlsModelo', $url->ssl('listasdeprecio', 'exportXlsModelo', array('listaprecio' => $objeto_id)));
        // </editor-fold>

        return $view->fetch('content/listadePrecioImport.tpl');
    }

    function insertImport() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $user = & $this->locator->get('user');
        $url = & $this->locator->get('url');
        $upload = & $this->locator->get('upload');
        $common = & $this->locator->get('common');
        // </editor-fold>

        $template->set('title', 'LISTA DE PRECIOS');

        if (($request->isPost()) && ($this->validateFormImport())) {
            $nombreDirectorio = '';
            $nombreArchivo = '';

            // <editor-fold defaultstate="collapsed" desc="XLS">
            //Cargo la imágen1     
            $objeto_id = $common->GetaaaaMMddhhmmssmm();
            if ($upload->has('archivoupload')) {
                $nombreArchivo = $objeto_id . '.' . end(explode(".", $upload->getName('archivoupload')));
                $nombreDirectorio = 'Archivos/Importar/';
                $upload->save('archivoupload', $nombreDirectorio . $nombreArchivo);

                $nombreCompleto = realpath($nombreDirectorio . $nombreArchivo);
                //ARMAR EL ARREGLO DESDE EL EXCEL
                require_once 'library/Excel/reader.php';
                $data = new Spreadsheet_Excel_Reader();
                $data->read($nombreCompleto);

                $errores = '';
                $articulosXls = array();
                $saltodelinea = '';

                for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
                    $Linea = $data->sheets[0]['cells'][$i];
                    if ($Linea != null) {
                        $ResultadoValidacion = $this->ValidaLinea($Linea);
                        if ($ResultadoValidacion == 'ok') {
                            //ARTICULO colA
                            $colA = $data->sheets[0]['cells'][$i][1];
                            
                            //COD DE BARRA interno colE
                            $colE = $data->sheets[0]['cells'][$i][5];

                            //PRECIO DE VTA colF
                            $colF = $data->sheets[0]['cells'][$i][6];
                            $flo = str_replace(",", ".", $colF);
                            $colF = $flo;

                            if ($colA == '' && $colA == null && $colE != '' && $colE != null) {
                                $sql = "select articulo from articulos WHERE codbarrainterno = '?' ";
                                $consulta = $database->parse($sql, $colE);
                                 $arti = $database->getRow($consulta);
                                 $colA = $arti['articulo'];
                            }
                            
                            $articulosXls[] = array(
                                'articulo' => $colA,
                                'codbarrainterno' => $colE,
                                'preciovta' => $colF
                            );
                        } else {
                            //meto en un array de errores
                            $errores .= $saltodelinea . 'Linea: ' . $i . ' (' . $ResultadoValidacion . ')';
                            $saltodelinea = '<br>';
                        }
                    }
                }

                if ($errores != '') {

                    //CARGO UNA VARIABLE CON LOS ERRORES Y LOS MUESTRO EN ROJO SIN SUBIR NADA

                    $this->error['texto_error'] = $errores;
                } else {
                    if ($upload->getError('archivoupload') > 0) {
                        $this->error['archivo'] = $upload->getErrorMsg($upload->getError('archivoupload'));
                    } else {
                        //HAGO LOS INSERT ACA
                        foreach ($articulosXls as $art) {
//                            if ($art['articulo'] == null) {
//                                $sql = "SELECT count(*) as verifica  FROM listasdeprecioarticulos lp LEFT JOIN vw_grilla_articulos vw ON lp.articulo = vw.articulo  WHERE lp.listaprecio = '" . $request->get('listaprecio', 'get') . "' and codbarrainterno = '".$art['codbarrainterno']."'";
//                            }
//                            else{
                                $sql = "select count(*) as verifica from listasdeprecioarticulos where articulo ='" . $art['articulo'] . "' and listaprecio='" . $request->get('listaprecio') . "' ";
//                            }
                            //ver si ya esta guardado actualizar precio sino meter uno nuevo 
                            
                            $verifica = $database->getRow($sql);
                            if ($verifica['verifica'] != 0 && $art['articulo'] != null) {
                                //$sql2 = "INSERT IGNORE INTO articulos SET tipoproducto='".$request->get('tipoproducto', 'post')."', modelo = '".$request->get('auto_modelo_marca', 'post')."', estadoarticulo = 'DI', tipogama='".$request->get('tipogama', 'post')."', mnu='".$request->get('tipogama', 'post')."', concoddebarra='".$request->get('concoddebarra', 'post')."', precio='".$request->get('precio', 'post')."', au_usuario='".$user->getPERSONA()."',au_accion='A',au_fecha_hora=NOW()";
                                $sql = "UPDATE listasdeprecioarticulos SET preciovta='?' WHERE articulo ='?'  and listaprecio = '?'";
                                $consulta = $database->parse($sql, $art['preciovta'], $art['articulo'],  $request->get('listaprecio'));
                                $database->query($consulta);
                            } else {
                                $sql = "SELECT count(*) as verifica FROM listasdeprecioarticulos lp LEFT JOIN vw_grilla_articulos vw ON lp.articulo = vw.articulo  WHERE vw.codbarrainterno='" . $art['codbarrainterno'] . "' and lp.listaprecio = '" . $request->get('listaprecio', 'get') . "' ";
                                //$sql = "select count(*) as verifica from listasdeprecioarticulos where  codbarrainterno='" . $art['codbarrainterno'] . "' and listaprecio='" . $request->get('listaprecio') . "' ";
                                $verifica = $database->getRow($sql);
                            
                                if ($verifica['verifica'] != 0) {
                                //$sql2 = "INSERT IGNORE INTO articulos SET tipoproducto='".$request->get('tipoproducto', 'post')."', modelo = '".$request->get('auto_modelo_marca', 'post')."', estadoarticulo = 'DI', tipogama='".$request->get('tipogama', 'post')."', mnu='".$request->get('tipogama', 'post')."', concoddebarra='".$request->get('concoddebarra', 'post')."', precio='".$request->get('precio', 'post')."', au_usuario='".$user->getPERSONA()."',au_accion='A',au_fecha_hora=NOW()";
                                    $sql = "UPDATE listasdeprecioarticulos SET preciovta='?' WHERE articulo ='?'  and listaprecio = '?'";
                                    $consulta = $database->parse($sql, $art['preciovta'], $art['articulo'],  $request->get('listaprecio'));
                                    $database->query($consulta);
                                } else {
                                    
                                    //$sql2 = "INSERT IGNORE INTO articulos SET tipoproducto='".$request->get('tipoproducto', 'post')."', modelo = '".$request->get('auto_modelo_marca', 'post')."', estadoarticulo = 'DI', tipogama='".$request->get('tipogama', 'post')."', mnu='".$request->get('tipogama', 'post')."', concoddebarra='".$request->get('concoddebarra', 'post')."', precio='".$request->get('precio', 'post')."', au_usuario='".$user->getPERSONA()."',au_accion='A',au_fecha_hora=NOW()";
                                    $sql = "INSERT IGNORE INTO listasdeprecioarticulos SET listaprecio='?', articulo = '?', codbarra = '?', preciovta='?'";
                                    $consulta = $database->parse($sql, $request->get('listaprecio'), $art['articulo'], $art['codbarra'], $art['preciovta']);
                                    $database->query($consulta);
                                }
                            }
                        }

                        $cache->delete('listasdeprecio');

                        $session->set('message', 'Se importaron de forma correcta todos los artículos en la lista de precio.');

                        $response->redirect($url->ssl('listasdeprecio'));
                    }
                }
            }

            // </editor-fold>
        }

        $template->set('content', $this->getFormImport());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function exportXlsModelo() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        // </editor-fold>

        set_time_limit(0);

        //** PHPExcel **//
        require_once (DIR_INCLUDES . '/library/phpexcel/PHPExcel.php');

        date_default_timezone_set('America/Argentina/Buenos_Aires');
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set properties of the file
        $objPHPExcel->getProperties()->setCreator("NombreEmpresa")
                ->setLastModifiedBy("NombreEmpresa")
                ->setTitle("Planilla web exportada: " . date('d-m-Y'))
                ->setSubject("Planilla web exportada: " . date('d-m-Y'));

        //genero las columnas del excel
        $letra = 'A';
        $fila_inicial = 2; //fila desde donde empiezo a cargar la tabla en el excel. Arriba va el encabezado
        // <editor-fold defaultstate="collapsed" desc="PROPIEDADES DE LAS COLUMNAS">
        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //COLUMNA A
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(11);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'ID ARTICULO');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA B
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'MARCA');
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA C
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(22);
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', 'MODELO');
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA D
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', 'TIPO PRODUCTO');
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA E
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', 'COD INTERNO');
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        //COLUMNA F
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', 'PRECIO VTA');
        $objPHPExcel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        // </editor-fold>
        
        //CARGA PRODUCTOS
        //// <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        set_time_limit(0);

        $sql = "SELECT *, lp.preciovta AS preciovtalista  FROM listasdeprecioarticulos lp LEFT JOIN vw_grilla_articulos vw ON lp.articulo = vw.articulo  WHERE lp.listaprecio = '" . $request->get('listaprecio', 'get') . "' ";

        $sql .= "GROUP BY lp.articulo ORDER BY marca, modelo ASC ";

//		$consul = $database->splitQuery($database->parse($sql,'%' . $session->get('articulos.search') . '%','%' . $session->get('articulos.search') . '%'), $session->get('articulos.page'), $config->get('config_max_rows'));
//
//                $results = $database->getRows($consul);

        $results = $database->getRows($sql);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="LLANAR LOS DATOS">

        $num = $fila_inicial;

        foreach ($results as $result) {

            //COLUMNA A
            $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $num, $result['articulo']);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            //COLUMNA B
            $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $num, $result['descmarca']);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            //COLUMNA C
            $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $num, $result['descmodelo']);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA D
            $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' . $num, $result['desctipoproducto']);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA E
            $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' . $num, $result['codbarrainterno']);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA F
            $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' . $num, $result['preciovtalista']);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $num++;
        }

        //SEGUIR EN COMPLETAR EL EXCEL
        //POR CADA INSCRIPTO IR BUSCANDO CADA RESPUESTA Y COMPLETAR SEGUN CORRESPONDA
        // </editor-fold>
        
        // Nombre de la hoja del libro
        $objPHPExcel->getActiveSheet()->setTitle('ModeloImportListaPrecio');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Save Excel 2007 file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');

        // It will be called file.xls | esta linea hace que sea una descarga y no una pestaña aparte
        header('Content-Disposition: attachment; filename="ModeloImportListaPrecio.xls"');

        header('Cache-Control: max-age=0');

        // Write file to the browser
        $objWriter->save('php://output');
    }

    function validateFormImport() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $upload = & $this->locator->get('upload');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        if ($upload->has('archivoupload')) {
            $tiposPermitidos = array(
                'application/vnd.ms-excel',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'application/vnd.ms-excel'
            );

            if (!in_array($upload->getType('archivoupload'), $tiposPermitidos)) {
                $errores = 'No es un documento v&aacute;lido para cargar (' . $upload->getType('archivoupload') . ')';
            }
        } elseif ($upload->hasError('archivoupload')) {
            $errores = $upload->getErrorMsg($upload->getError('archivoupload'));
        } else {
            $errores = 'Debe elegir un documento excel para cargar.';
        }


        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function ValidaLinea(array $Linea) {
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $upload = & $this->locator->get('upload');
        $common = & $this->locator->get('common');
        $user = & $this->locator->get('user');


        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        //valida que el articulo exista.
        //MARCA colA
        $colA = $Linea[1];
        $colE = $Linea[5];
        $articulo = null;

        if ($colA != null && $colA != '') {
            $sql = "select * from articulos where articulo ='" . $colA . "' or codbarrainterno ='" . $colE . "'";
            $articulo = $database->getRow($sql);
            if (!$articulo) {
              //  return 'Artículo no existe.';
            }
        } else {
            if ($colE != null && $colE != '') {
                $sql = "select * from articulos where articulo ='" . $colA . "' or codbarrainterno ='" . $colE . "'";
                $articulo = $database->getRow($sql);
                if (!$articulo) {
                //    return 'Artículo no existe.';
                }
            } else {
               // return 'Articulo no existe.';
            }
        }

        

////valida que haya ingresado un modelo
//                $colB = $Linea[2];
//                if ($colB == null && $colA == '') {
//                   return 'Debe ingresar un modelo.'; 
//                }
//                                
//                $colC = $Linea[3];
//                if ($colC != '' && $colC != 'ACCESORIO') {
//                    $verifica = $database->getRow("select count(*) AS verifica from tiposproducto where descripcion ='" . $colC . "' ");
//                    if ($verifica['verifica'] == 0) {
//                        return 'Tipo de Producto no existe.';
//                    }
//                }         
//                
//                $colD = $Linea[4];
//                if ($colD != '' && $colD != 'SIN GAMA') {
//                    $verifica = $database->getRow("select count(*) AS verifica from tiposgama where descripcion ='" . $colD . "' ");
//                    if ($verifica['verifica'] == 0) {
//                        return 'Tipo de Gama no existe.';
//                    }
//                }
//                  
//                $colE = $Linea[5];
//                if ($colE != '') {
//                    $verifica = $database->getRow("select count(*) AS verifica from articulos where codbarrainterno ='" . $colE . "' ");
//                    if ($verifica['verifica'] > 0) {
//                        return "El código interno " .$colE. " ya se encuentra asignado a un artículo.";
//                    }
//                }
        $colF = $Linea[6];
        $colF = str_replace(",", "", $colF);
        if ($colF != null && $colF != '') {
            if (!is_numeric($colF)) {
                return 'Debe ingresar un número valido en la columna E';
            }
        }
        
//        $colF = $Linea[6];
//        if ($colF != null) {
//            if (!is_numeric($colF)) {
//                return 'Debe ingresar un número valido en la columna F';
//            }
//        }

        // </editor-fold>

        return 'ok';
    }

    // </editor-fold>
}

?>