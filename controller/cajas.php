<?php

class ControllerCajas extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'GESTIÓN DE CAJAS DE COMPRAS');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
        $request = & $this->locator->get('request');
        // </editor-fold>
         // <editor-fold defaultstate="collapsed" desc="LIMPIA FILTRO Y VARIBLES DE SESSION">

        if ($request->get('filtra') == NULL) {

            $session->set('cajas.search', '');
            $session->set('cajas.sort', '');
            $session->set('cajas.order', '');
            $session->set('cajas.page', '');

            $view->set('search', '');
            $view->set('cajas.search', '');
            
            $cache->delete('cajas');
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">

        $cols = array();

        $cols[] = array(
            'name' => 'CAJA',
            'sort' => 'caja',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'APERTURA',
            'sort' => 'fechaapertura',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'CIERRE',
            'sort' => 'fechacierre',
            'align' => 'left'
        );
        $cols[] = array(
            'name' => '$ APERTURA',
            'sort' => 'montoapertura',
            'align' => 'left'
        );
        $cols[] = array(
            'name' => '$ SALDO',
            'sort' => 'saldo',
            'align' => 'left'
        );
        $cols[] = array(
            'name' => 'ESTADO',
            'sort' => 'estadocaja',
            'align' => 'left'
        );

//                $cols[] = array(
//			'name'  => 'PRECIO VTA',
//			'sort'  => 'precio',
//			'align' => 'left'
//		);

        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'caja',
            'fechaapertura',
            'fechacierre',
            'montoapertura',
            'saldo',
            'estadocaja'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        set_time_limit(0);

        if (!$session->get('cajas.search')) {
            $sql = "SELECT * FROM cajas  ";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT * FROM cajas WHERE caja LIKE '?' OR estado LIKE '?' ";
            $conca = " AND ";
        }


        if (in_array($session->get('cajas.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('cajas.sort') . " " . (($session->get('cajas.order') == 'desc') ? 'desc' : 'asc');
        } else {
            $sql .= " ORDER BY estadocaja, fechaapertura ASC";
        }

//		$consul = $database->splitQuery($database->parse($sql,'%' . $session->get('cajas.search') . '%','%' . $session->get('cajas.search') . '%'), $session->get('cajas.page'), $config->get('config_max_rows'));
//
//                $results = $database->getRows($consul);

        $consulta = $database->parse($sql, '%' . $session->get('cajas.search') . '%', '%' . $session->get('cajas.search') . '%');
        $results = $database->getRows($consulta);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['caja'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => date('d/m/Y', strtotime(@$result['fechaapertura'])),
                'align' => 'left',
                'default' => 0
            );

            $fechacierre = '-';
            if (strtotime(@$result['fechacierre']) > 0) {
                $fechacierre = strtotime(@$result['fechacierre']);
            }
            $cell[] = array(
                'value' => $fechacierre,
                'align' => 'left',
                'default' => 0
            );


            $cell[] = array(
                'value' => @$result['montoapertura'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['saldo'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['estadocaja'],
                'align' => 'left',
                'default' => 0
            );

            $action = array();

            if ($user->hasPermisos($user->getPERSONA(), 'cajas', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa  fa-unlock-alt',
                    'text' => 'Cerrar caja',
                    'prop_a' => array('href' => $url->ssl('cajas', 'update', array('caja' => $result['caja'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'cajas', 'B')) {
                $action[] = array(
                    'icon' => 'img/iconos-11.png',
                    'text' => $language->get('button_delete'),
                    'class' => 'fa fa-fw fa-trash-o',
                    'prop_a' => array('href' => "javascript:ActionDelete('" . $url->ssl('cajas', 'delete', array('caja' => $result['caja'])) . "');")
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'movimientoscaja', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw fa-wrench',
                    'text' => $language->get('button_consult'),
                    'prop_a' => array('href' => $url->ssl('movimientoscaja', 'index', array('caja' => $result['caja'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'cajas', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw  fa-print',
                    'text' => 'Detalle de caja',
                    'prop_a' => array('href' => $url->ssl('cajas', 'exportarPDF', array('caja' => $result['caja'])))
                );
            }

            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        //<editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('cajas.page'));

        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        $view->set('heading_title', 'CAJAS');
        $view->set('placeholder_buscar', 'BUSCA POR ID O ESTADO');

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        //$view->set('consultaVacia', $url->ssl('consulta','consulta_vacia'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        //$view->set('button_consultavacia', "Vacía");
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('cajas.search'));
        $view->set('sort', $session->get('cajas.sort'));
        $view->set('order', $session->get('cajas.order'));
        $view->set('page', $session->get('cajas.page'));

        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $view->set('titulo_ventana', '::. Exportar');

        $view->set('entry_modificar', "Modificar");
        $view->set('entry_agregar', "Agregar");

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('cajas', 'page'));
        $view->set('list', $url->ssl('cajas'));

        if ($user->hasPermisos($user->getPERSONA(), 'cajas', 'A')) {
            $view->set('insert', $url->ssl('cajas', 'insert'));
        }
        if ($user->hasPermisos($user->getPERSONA(), 'cajas', 'A')) {
            $view->set('insertImport', $url->ssl('cajas', 'insertImport'));
        }

        if ($user->hasPermisos($user->getPERSONA(), 'cajas', 'C')) {
            $view->set('exportXls', $url->ssl('cajas', 'exportXls'));
        }

        //$view->set('addPais', $url->ssl('cajas','insert'));
        // $view->set('updatePersona', $url->ssl('paciente','update', array('persona' => $result['persona'])));
        //$view->set('updatePais', $url->ssl('cajas','update'));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_cajas.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">
//		if ($request->has('modelo', 'post')) {
//                    $session->set('cajas.pais',$request->get('modelo','post'));              
//                }

        if ($request->has('search', 'post')) {
            $session->set('cajas.search', $request->get('search', 'post'));
        }

        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('cajas.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('cajas.order', (($session->get('cajas.sort') == $request->get('sort', 'post')) && ($session->get('cajas.order') == 'asc')) ? 'desc' : 'asc');
        }

        if ($request->has('sort', 'post')) {
            $session->set('cajas.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('cajas', 'index', array('filtra' => '1')));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $template = & $this->locator->get('template');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('heading_title', 'DATOS DE LA CAJA');

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $template->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('tab_general', $language->get('tab_general'));

        $view->set('caja', $request->get('caja'));

        if (($request->get('caja')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM cajas WHERE caja = '" . $request->get('caja') . "' ";
            $objeto_info = $database->getRow($consulta);
        }

        if ($request->has('caja', 'post')) {
            $view->set('caja', $request->get('caja', 'post'));
            $objeto_id = @$objeto_info['caja'];
        } else {
            $view->set('caja', $request->get('caja', 'get'));
            $objeto_id = @$objeto_info['caja'];
        }

        $view->set('entry_fechaapertura', 'Fecha Apertura:');
        if ($request->has('fechaapertura', 'post')) {
            $view->set('fechaapertura', $request->get('fechaapertura', 'post'));
        } else {
            if ($request->has('fechaapertura', 'get')) {
                $view->set('fechaapertura', date('d/m/Y', strtotime(@$objeto_info['fechaapertura'])));
            } else {
                $view->set('fechaapertura', date('d/m/Y'));
            }
        }


        $view->set('entry_montoapertura', 'Monto apertura:');
        if ($request->has('montoapertura', 'post')) {
            $view->set('montoapertura', $request->get('montoapertura', 'post'));
        } else {
            $view->set('montoapertura', @$objeto_info['montoapertura']);
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $template->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');
        $view->set('error_texto_error', @$this->error['texto_error']);
        //$view->set('error_titulo', @$this->error['titulo']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        //$view->set('action', $url->ssl('cajas', $request->get('action'), array('modelo' => $request->get('modelo'))));
        $view->set('actionBoton', $request->get('action'));
        $view->set('cancel', $url->ssl('cajas'));
        // </editor-fold>

        return $view->fetch('content/caja.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        //SOLO PUEDE HABER ABIERTA UNA SOLA CAJA
        if ($request->get('action', 'get') == 'insert') {
            $sql = "SELECT count(*) as total FROM cajas WHERE estadocaja = 'ABIERTA' ";
            $caja = $database->getRow($sql);

            if ($caja['total'] > 0) {
                $errores .= 'Ya hay una caja abierta. <br>';
            }
        }
        if ((strlen($request->get('fechaapertura', 'post')) != 10)) {
            $errores .= 'Debe ingresar una fecha. <br>';
        }
        if ((strlen($request->get('montoapertura', 'post')) == 0)) {
            $errores .= 'Debe ingresar un monto. <br>';
        }

        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';

        //SE VA A PODER ELIMINAR UNA CAJA SI NO TIENE MOVIMIENTOS HECHOS 
        $consulta_info = $database->getRow("SELECT COUNT(*) as total FROM movimientoscaja WHERE caja = '" . $request->get('caja') . "' ");
        if (@$consulta_info['total'] != 0) {
            $errores .= 'No es posible eliminar esta caja, tiene movimientos asignados.';
        }

        if ($errores != '') {
            $this->error['message'] = $errores;
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $user = & $this->locator->get('user');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'CAJAS');

        if (($request->isPost()) && ($this->validateForm())) {

            // <editor-fold defaultstate="collapsed" desc="FECHA">
            $fecha_d = '';
            if ($request->get('fechaapertura', 'post') != '') {
                $dated = explode('/', $request->get('fechaapertura', 'post'));
                $fecha_d = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
                
            } else {
                $fecha_d = NULL;
            }

            // </editor-fold>
            //$sql2 = "INSERT IGNORE INTO articulos SET tipoproducto='".$request->get('tipoproducto', 'post')."', modelo = '".$request->get('auto_modelo_marca', 'post')."', estadoarticulo = 'DI', tipogama='".$request->get('tipogama', 'post')."', mnu='".$request->get('tipogama', 'post')."', concoddebarra='".$request->get('concoddebarra', 'post')."', precio='".$request->get('precio', 'post')."', au_usuario='".$user->getPERSONA()."',au_accion='A',au_fecha_hora=NOW()";
            $sql = "INSERT IGNORE INTO cajas SET fechaapertura='?', montoapertura = '?', estadocaja = 'ABIERTA', saldo='?', au_usuario='?',au_accion='A',au_fecha_hora=NOW()";
            $consulta = $database->parse($sql, $fecha_d, $request->get('montoapertura', 'post'), $request->get('montoapertura', 'post'), $user->getPERSONA());
            $database->query($consulta);

            $id = $database->getLastId();
            $cache->delete('cajas');
            $session->set('message', 'Se agreg&oacute; la caja: ' . $id);

            $response->redirect($url->ssl('cajas'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $user = & $this->locator->get('user');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos del Articulo');

        $sql = "SELECT * FROM cajas WHERE caja = '" . $request->get('caja', 'get') . "' ";
        $caja = $database->getRow($sql);

        $sql = "UPDATE cajas SET fechacierre=NOW(), estadocaja = 'CERRADA', au_usuario='?',au_accion='M',au_fecha_hora=NOW() WHERE caja='?' ";
        $consulta = $database->parse($sql, $user->getPERSONA(), $request->get('caja', 'get'));
        $database->query($consulta);

        $cache->delete('cajas');

        $session->set('message', "Se ah cerrado la caja número " . $request->get('caja', 'post') . ".");

        $response->redirect($url->ssl('cajas'));
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos del articulo');
        if (($request->isPost())) {

            $response->redirect($url->ssl('cajas'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        if (($request->get('caja')) && ($this->validateDelete())) {

            $id = $request->get('caja', 'get');

            $sql = "DELETE FROM cajas WHERE caja = '" . $id . "' ";
            $database->query($sql);

            $cache->delete('cajas');

            $session->set('message', 'Se ha eliminado la caja: ' . $id);

            $response->redirect($url->ssl('cajas'));
        }

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function exportXls() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        // </editor-fold>

        set_time_limit(0);

        //** PHPExcel **//
        require_once (DIR_INCLUDES . '/library/phpexcel/PHPExcel.php');

        date_default_timezone_set('America/Argentina/Buenos_Aires');
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set properties of the file
        $objPHPExcel->getProperties()->setCreator("NombreEmpresa")
                ->setLastModifiedBy("NombreEmpresa")
                ->setTitle("Planilla web exportada: " . date('d-m-Y'))
                ->setSubject("Planilla web exportada: " . date('d-m-Y'));

        //genero las columnas del excel
        $letra = 'A';
        $fila_inicial = 2; //fila desde donde empiezo a cargar la tabla en el excel. Arriba va el encabezado
        // <editor-fold defaultstate="collapsed" desc="PROPIEDADES DE LAS COLUMNAS">
        $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //COLUMNA A
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(11);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'ID Articulo');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        //COLUMNA B
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'MARCA');
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA C
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(22);
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', 'MODELO');
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA D
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', 'TIPO PRODUCTO');
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA E
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', 'GAMA');
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA F
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(14);
        $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', 'COD INTERNO');
        $objPHPExcel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        //COLUMNA G
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getStyle('G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('G1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1', 'COD DE BARRA');
        $objPHPExcel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        //COLUMNA H
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getStyle('H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('H1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1', 'PRECIO VTA');
        $objPHPExcel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        // </editor-fold>
        //CARGA PRODUCTOS
        //// <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        set_time_limit(0);

        if (!$session->get('cajas.search')) {
            $sql = "SELECT * FROM  vw_grilla_articulostodos  ";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT * FROM vw_grilla_articulostodos WHERE articulo LIKE '?' OR descestadoarticulo LIKE '?' OR desctipoproducto LIKE '?' OR descmodelo LIKE '?' OR descmarca LIKE '?' ";
            $conca = " AND ";
        }

        if ($session->get('cajas.tipoproducto') != '-1' && $session->get('cajas.tipoproducto') != '') {
            $sql .= $conca . " tipoproducto = '" . $session->get('cajas.tipoproducto') . "'  ";
        }

        $sql .= " ORDER BY marca, modelo ASC";

//		$consul = $database->splitQuery($database->parse($sql,'%' . $session->get('cajas.search') . '%','%' . $session->get('cajas.search') . '%'), $session->get('cajas.page'), $config->get('config_max_rows'));
//
//                $results = $database->getRows($consul);

        $consulta = $database->parse($sql, '%' . $session->get('cajas.search') . '%', '%' . $session->get('cajas.search') . '%', '%' . $session->get('cajas.search') . '%', '%' . $session->get('cajas.search') . '%', '%' . $session->get('cajas.search') . '%');
        $results = $database->getRows($consulta);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="LLANAR LOS DATOS">

        $num = $fila_inicial;

        foreach ($results as $result) {

            //COLUMNA A
            $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $num, $result['caja']);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            //COLUMNA B
            $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $num, $result['descmarca']);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            //COLUMNA C
            $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $num, $result['descmodelo']);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA D
            $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' . $num, $result['desctipoproducto']);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA E
            $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' . $num, $result['descgama']);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA F
            $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' . $num, $result['codbarrainterno']);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA G
            $objPHPExcel->getActiveSheet()->getStyle('G' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G' . $num, $result['codbarra']);
            $objPHPExcel->getActiveSheet()->getStyle('G' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA H
            $objPHPExcel->getActiveSheet()->getStyle('H' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H' . $num, '0');
            $objPHPExcel->getActiveSheet()->getStyle('H' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $num++;
        }

        //SEGUIR EN COMPLETAR EL EXCEL
        //POR CADA INSCRIPTO IR BUSCANDO CADA RESPUESTA Y COMPLETAR SEGUN CORRESPONDA
        // </editor-fold>
        // Nombre de la hoja del libro
        $objPHPExcel->getActiveSheet()->setTitle('ExportArticulos');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Save Excel 2007 file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');

        // It will be called file.xls | esta linea hace que sea una descarga y no una pestaña aparte
        header('Content-Disposition: attachment; filename="ExportArticulos.xls"');

        header('Cache-Control: max-age=0');

        // Write file to the browser
        $objWriter->save('php://output');
    }

    function exportarPDF() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $request = & $this->locator->get('request');
        // </editor-fold>

        set_time_limit(0);

        if ($request->has('caja', 'post')) {
            $caja_id = $request->get('caja', 'post');
        } else {
            if ($request->has('caja', 'get')) {
                $caja_id = $request->get('caja', 'get');
            }
        }

        $consulta = "SELECT DISTINCT * FROM vw_grilla_cajas WHERE caja = '?'";
        $caja_info = $database->getRow($database->parse($consulta, $caja_id));

        $consult = "SELECT * FROM vw_grilla_movimientoscaja WHERE caja='" . $caja_id . "' ORDER BY  fechamovimiento ASC";
        $movimientoscaja = $database->getRows($consult);

        // <editor-fold defaultstate="collapsed" desc="config PDF">
        // Include the main TCPDF library (search for installation path).
        // Include the main TCPDF library (search for installation path).
        define('FPDF_FONTPATH', 'font/');
        require('library/pdf/tcpdf/tcpdf.php');
        require_once('library/pdf/tcpdf/tcpdf_include.php');

        $con = PDF_PAGE_ORIENTATION;
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF8 sin BOM', false);


        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('STC');
        $pdf->SetTitle('');
        $pdf->SetSubject(' ');
        $pdf->SetKeywords(' ');

        // set default header data
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        //LA IMAGEN DEBE ESTAR EN template\default\image
        $pdf->SetHeaderData('cabecera PDF-reporte.jpg', 173, '', '');

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font - conjunto predeterminado fuentemonoespaciada
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetMargins(20, 22, 20);
        //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetHeaderMargin(5);
        $pdf->SetFooterMargin(15);

        // set auto page breaks - establecer saltos de página automático
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // establecemos la medida del interlineado
        //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        //$pdf->SetDefaultMonospacedFont(30);
        // ---------------------------------------------------------
        // set font
        $pdf->SetFont('helvetica', '', 9);


        //$pdf->Image('images/sistema.png', 50, 50, 100, '', '', 'http://www.tcpdf.org', '', false, 300);
// </editor-fold>  
        // add a page
        $pdf->AddPage();

        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">
        $html = '<div style="text-align:center; " ><h1><b>DETALLE DE CAJA</b></h1></div>';
        $pdf->writeHTML($html, true, false, true, false, '');

        //$html = '<div style="text-align:center; font-size: 13pt;" ><b>CONSTANCIA DE ORDEN DE REPARACIÓN</b><br></div>';
        //$pdf->writeHTML($html, true, false, true, false, '');
//            $html = '<div style="text-align:center; font-size: 11pt;" ><b>CAJA NRO: '.$caja_id.' </b><br></div>';
//            $pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="CAJA">
        $html = '<div style="text-align:center; font-size: 11pt;" ><b>Datos de la caja nro ' . $caja_id . ' </b><hr /></div>';

        $pdf->writeHTML($html, true, false, true, false, '');
        // <editor-fold defaultstate="collapsed" desc="STYLE">
        $html = <<<EOF
                <!-- EXAMPLE OF CSS STYLE -->
                <style>
                    /*
                    table.first {
                        color: black;
                        font-family: helvetica;
                        font-size: 8pt;
                        background-color: white;
                    }
                    td {
                        border: 0px solid black;
                        background-color: white;
                    }
                    */
                </style>
EOF;
        //$pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>

        $fechaapertura = date('d/m/Y', strtotime($caja_info['fechaapertura']));
        $fechacierre = date('d/m/Y', strtotime($caja_info['fechacierre']));
        $montoapertura = $caja_info['montoapertura'];
        $saldo = $caja_info['saldo'];
        $estado = $caja_info['estadocaja'];
        $nombreusuario = $caja_info['nombreusuario'];

        $html .= <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    <td width="300" style="text-align:left;"><b>FECHA APERTURA: </b>$fechaapertura</td>
                    <td width="300" style="text-align:left;"><b>FECHA CIERRE: </b>$fechacierre</td>
                    
               </tr> 
                <tr >
                    <td width="300" style="text-align:left;" ><b>MONTO APERTURA: </b>$$montoapertura</td>
                    <td width="300" style="text-align:left;"><b>SALDO: </b>$$saldo</td>
               </tr> 
                <tr >
                    <td width="300" style="text-align:left;" ><b>ESTADO: </b>$estado</td>
                    <td width="300" style="text-align:left;"><b>USUARIO APERTURA: </b>$nombreusuario</td>
               </tr>
            </table>
EOF;

        $pdf->writeHTML($html, true, false, true, false, '');

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="MOVIMIENTOS">
        $html = '<div style="text-align:center; font-size: 11pt; " ><br><b>Movimientos</b><hr /></div>';

        $pdf->writeHTML($html, true, false, true, false, '');
        // <editor-fold defaultstate="collapsed" desc="STYLE">
        $html = <<<EOF
                <!-- EXAMPLE OF CSS STYLE -->
                <style>
                    
                    table.first {
                        color: black;
                        font-family: helvetica;
                        font-size: 8pt;
                        background-color: white;
                    }
                    td {
                        border: 1px solid black;
                        background-color: white;
                    }
                    
                </style>
EOF;
        $pdf->writeHTML($html, true, false, true, false, '');

        // </editor-fold>

        $html .= <<<EOF
                        
            <table style="width:100%;font-size: 8pt;">
            
                <tr>
                    <td width="65"><b>FECHA</b></td>
                    <td width="65"><b>TIPO</b></td>
                    <td width="55"><b>MONTO</b></td>
                    <td width="85"><b>COMPRA</b></td>
                    <td width="180"><b>OBSERVACIÓN</b></td>
                    <td width="140"><b>RESPONSABLE</b></td>
               </tr> 
EOF;
        $totalsuma = 0;
        foreach ($movimientoscaja as $result) {

            // <editor-fold defaultstate="collapsed" desc="VARIABLES">
            $fecha = date('d/m/Y', strtotime($result['fechamovimiento']));
            $tipo = $result['tipomovimiento'];
            $monto = $result['montomovimiento'];
            $compra = ($result['compra'] ? $result['compra'] : '-');
            $observacion = $result['observacion'];
            $responsable = $result['nombreusuario'];

            // </editor-fold>

            $html .= <<<EOF
                
                <tr>
                    <td width="65">$fecha</td>
                    <td width="65">$tipo</td>
                    <td width="55">$$monto</td>
                    <td width="85">$compra</td>
                    <td width="180">$observacion</td>
                    <td width="140">$responsable</td>
                </tr>                  
                
EOF;
        }

        $html .= <<<EOF
                                 
                
</table>
EOF;

        $pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>
        //EJEMPLO
        //// print TEXT
        //$pdf->PrintChapter(1, 'LOREM IPSUM [TEXT]', 'data/chapter_demo_1.txt', false);
        // print HTML
        //$pdf->PrintChapter(2, 'LOREM IPSUM [HTML]', 'data/chapter_demo_2.txt', true);
        // output some RTL HTML content
        //$html = '<div style="text-align:right">ACOMPAÑA<br></div>';
        //$pdf->writeHTML($htmlTable, true, false, true, false, '');
        // reset pointer to the last page
        //$pdf->lastPage();
        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        // Print a table
        // remove default header
        $pdf->setPrintHeader(false);
        //ESTO HACE QUE ANDE EN EL SERVER DE LA DNNA
        ob_end_clean();
        //Close and output PDF document
        //D obliga la descarga
        //I abre en una ventana nueva
        $nombrePDF = 'DetalleCaja_' . $caja_id . '.pdf';
        $pdf->Output($nombrePDF, 'I');

        //$cache->delete('checkins');
        //die('ok');
        //============================================================+
        // END OF FILE
        //============================================================+
    }

}

?>