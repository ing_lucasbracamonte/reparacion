<?php

class ControllerContacto extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'Contacto');
        $template->set('content', $this->getForm());
        $template->set($module->fetch());
        $response->set($template->fetch('layout.tpl'));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $template = & $this->locator->get('template');
        $view = $this->locator->create('template');
        $user = & $this->locator->get('user');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('entry_persona', 'Documento:');
        $view->set('entry_nombre', 'Nombre y Apellido:');
        $view->set('entry_localidad', 'Localidad:');
        $view->set('entry_domicilio', 'Domicilio:');
        $view->set('entry_telefono', 'Tel&eacute;fono:');
        $view->set('entry_mail', 'E-mail:');
        $view->set('entry_consulta', 'Consulta:');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('heading_title', 'CONTACTO');
        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/NOTICIAS.png');

        $view->set('heading_description', 'Complete el formulario para realizar una consulta.');

        $view->set('tab_general', 'Consulta');

        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', 'Enviar');
        $view->set('button_cancel', $language->get('button_cancel'));

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $template->set('message', $session->get('message'));
        $session->delete('message');

        //PREGUNTO SI ESTA LOGUEADO
        //SI ESTA AL FORMULARIO LO CARGO CON LOS DATOS QUE TIENE EN LA BASE 

        if ($user->isLogged()) {
            $consulta = "SELECT DISTINCT * FROM personas WHERE persona = '?'";
            $persona_info = $database->getRow($database->parse($consulta, $user->getPERSONA()));
        }


        if ($request->has('persona', 'post')) {
            $view->set('persona', $request->get('persona', 'post'));
        } else {
            $view->set('persona', @$persona_info['persona']);
        }

        if ($request->has('nombre', 'post')) {
            $view->set('nombre', $request->get('nombre', 'post'));
        } else {
            $view->set('nombre', @$persona_info['apellido'] . '  ' . @$persona_info['nombre']);
        }

        if ($request->has('localidad', 'post')) {
            $view->set('localidad', $request->get('localidad', 'post'));
        } else {
            $view->set('localidad', @$persona_info['localidad']);
        }

        if ($request->has('telefono', 'post')) {
            $view->set('telefono', $request->get('telefono', 'post'));
        } else {
            $view->set('telefono', @$persona_info['telefono']);
        }

        if ($request->has('mail', 'post')) {
            $view->set('mail', $request->get('mail', 'post'));
        } else {
            $view->set('mail', @$persona_info['mail']);
        }

        if ($request->has('consulta', 'post')) {
            $view->set('consulta', $request->get('consulta', 'post'));
        } else {
            $view->set('consulta', '');
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $view->set('error_persona', @$this->error['persona']);
        $view->set('error_domicilio', @$this->error['domicilio']);
        $view->set('error_nombre', @$this->error['nombre']);
        $view->set('error_telefono', @$this->error['telefono']);
        $view->set('error_celular', @$this->error['celular']);
        $view->set('error_localidad', @$this->error['localidad']);
        $view->set('error_localidad', @$this->error['domicilio']);
        $view->set('error_mail', @$this->error['mail']);
        $view->set('error_consulta', @$this->error['consulta']);

        $view->set('error', $session->get('error'));
        $session->delete('error');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('cancel', $url->ssl(DEFAULT_HOMEPAGE));
        $view->set('action', $url->ssl('contacto', 'enviarContacto'));
        // </editor-fold>

        return $view->fetch('content/contacto.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        if ((strlen($request->get('persona', 'post')) == 0)) {
            $this->error['persona'] = 'Debe ingresar el n&uacute;mero de documento';
        }
        if ((strlen($request->get('nombre', 'post')) < 3) || (strlen($request->get('nombre', 'post')) > 100)) {
            $this->error['nombre'] = 'Debe ingresar el nombre y apellido';
        }



        if ((strlen($request->get('telefono', 'post')) == 0)) {
            $this->error['telefono'] = 'Debe ingresar el tel&eacute;fono de contacto';
        }

        if ((strlen($request->get('mail', 'post')) == 0)) {
            $this->error['mail'] = 'Debe ingresar el e-mail de contacto';
        }

        if ((strlen($request->get('mail', 'post')) > 100) || (!preg_match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^', $request->get('mail', 'post')))) {
            $this->error['mail'] = 'El email ingresado es incorrecto';
        }

        if ((strlen($request->get('consulta', 'post')) == 0)) {
            $this->error['consulta'] = 'No ha escrito ninguna consulta';
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function enviarContacto() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $common = & $this->locator->get('common');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        $template->set('title', 'Contacto');

        if (($request->isPost()) && ($this->validateForm())) {
            date_default_timezone_set('America/Rosario');
            $sql = "SELECT * FROM localidades WHERE localidad = '?'";
            $localidad = $database->getRow($database->parse($sql, $request->get('localidad', 'post')));

            $cabeceras = "MIME-Version: 1.0\r\n";
            $cabeceras .= "Content-type: text/html; charset=iso-8859-1\r\n";
            $cabeceras .= "From: <noresponder@xxxxxxx.com.ar> \r\n";
            $cabeceras .= 'Reply-To:' . stripslashes($request->get('mail', 'post')) . "\r\n";
            $cabeceras .= 'X-Mailer: PHP/' . phpversion();

            $mensaje = '<h2>Contacto desde Sistema XXX</h2>';
            $mensaje .= '<p><b>Nombre y Apellido:</b> ' . utf8_decode($request->get('nombre', 'post')) . '<br />';
            $mensaje .= '<b>Documento:</b> ' . utf8_decode($request->get('persona', 'post')) . '<br />';
            $mensaje .= '<b>Localidad:</b> ' . utf8_decode($localidad['Descripcion']) . '<br />';
            $mensaje .= '<b>Domicilio:</b> ' . utf8_decode($request->get('domicilio', 'post')) . '<br />';
            $mensaje .= '<b>Tel&eacute;fono:</b> ' . utf8_decode($request->get('telefono', 'post')) . '<br />';
            $mensaje .= '<b>E-Mail:</b> ' . utf8_decode($request->get('mail', 'post')) . '<br />';
            $mensaje .= '<b>Consulta:</b> </p>';
            $mensaje .= '<p style="margin-left: 40px; width: 80%;">' . nl2br(htmlentities(utf8_decode($request->get('consulta', 'post')), ENT_QUOTES)) . '</p>';
            $mensaje .= '<br /><br /><br />';
            $mensaje .= '------------------------------------------------------------------------------------------------------ <br />';
            $mensaje .= 'Enviado autom&aacute;ticamente por Sistema XXX. <br >';
            $mensaje .= date('d-m-Y | h:m:s', time()) . '<br />';
            $mensaje .= '------------------------------------------------------------------------------------------------------';


            if (mail(MAIL_CONTACTO, 'Contacto desde Sistema XXX', $mensaje, $cabeceras)) {
                $session->set('message', 'Se ha enviado su consulta, nos pondremos en contacto con usted a la brevedad.');
                //$response->redirect($url->ssl('contacto'));
            } else {
                $session->set('error', 'Ha ocurrido un error al procesar la consulta. Intente nuevamente m&aacute;s tarde.');
            }

            $cache->delete('contacto');

            $session->set('message', 'El registro fue satisfactorio, le llegara un mail de validación');

            //ENVIAR MAIL DE VALIDACION QUE CUANDO HAGA CLICK EN UN LINK SE ACTUALICE A mail_confirmado = 1

            die('ok');
        }
        $response->set($this->getForm());
    }

}

?>