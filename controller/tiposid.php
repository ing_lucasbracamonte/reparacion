<?php

class ControllerTiposid extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'GESTIÓN DE TIPOS DE ID');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">

        $cols = array();

        $cols[] = array(
            'name' => 'Tipo',
            'sort' => 'tipo',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Descripci&oacute;n',
            'sort' => 'descripcion',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'tipo',
            'descripcion'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        set_time_limit(0);

        if (!$session->get('tiposid.search')) {
            $sql = "SELECT * FROM tiposid  ";
        } else {
            $sql = "SELECT * FROM tiposid WHERE tipoid LIKE '?' OR descripcion LIKE '?'";
        }

        if (in_array($session->get('tiposid.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('tiposid.sort') . " " . (($session->get('tiposid.order') == 'desc') ? 'desc' : 'asc');
        } else {
            $sql .= " ORDER BY descripcion ASC";
        }

//		$consul = $database->splitQuery($database->parse($sql,'%' . $session->get('tiposid.search') . '%','%' . $session->get('tiposid.search') . '%'), $session->get('tiposid.page'), $config->get('config_max_rows'));
//
//                $results = $database->getRows($consul);

        $consulta = $database->parse($sql, '%' . $session->get('tiposid.search') . '%', '%' . $session->get('tiposid.search') . '%');
        $results = $database->getRows($consulta);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['tipoid'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['descripcion'],
                'align' => 'left',
                'default' => 0
            );


            $action = array();

            if ($user->hasPermisos($user->getPERSONA(), 'tiposid', 'M')) {

                //$per = $result['encuesta'];
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'text' => $language->get('button_update'),
                    //'href' => "javascript:modificarClub(".$result['tipoid'].");"
                    //'href' => $url->ssl('tiposid', 'update', array('tipoid' => $result['tipoid']))
                    'prop_a' => array('href' => '/'
                        , 'onclick' => "ActionUpdate('" . $result['tipoid'] . "'); return false;")
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'tiposid', 'B')) {
                $action[] = array(
                    'icon' => 'img/iconos-11.png',
                    'text' => $language->get('button_delete'),
                    'prop_a' => array('href' => "javascript:ActionDelete('" . $url->ssl('tiposid', 'delete', array('tipoid' => $result['tipoid'])) . "');")
                );
            }
            if ($user->hasPermisos($user->getPERSONA(), 'tiposid', 'C')) {

                //$per = $result['encuesta'];
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'text' => $language->get('button_consult'),
                    //'href' => "javascript:modificarClub(".$result['tipoid'].");"
                    //'href' => $url->ssl('tiposid', 'update', array('tipoid' => $result['tipoid']))
                    'prop_a' => array('href' => '/'
                        , 'onclick' => "ActionConsult('" . $result['tipoid'] . "'); return false;")
                );

//                            $action[] = array(
//                                     'icon' => 'img/light-blue_icons/iconos-16.png',
//                                    'text' => 'integrantes',
//                                    //'href' => $url->ssl('inscripcionesactividades', 'index', array('Actividad' => $result['Actividad']))
//                                    'prop_a' => array('href' => $url->ssl('integrantesclubes', 'index', array('tipoid' => $result['tipoid'])))
//                                        );
            }
//			if ($user->hasPermisos($user->getPERSONA(),'encuesta','C')) {


            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        //<editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('tiposid.page'));

        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        $view->set('heading_title', 'Tipos de ID');
        $view->set('placeholder_buscar', 'BUSCA POR ID O DESCRIPCION');

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        //$view->set('consultaVacia', $url->ssl('consulta','consulta_vacia'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        //$view->set('button_consultavacia', "Vacía");
        $view->set('button_exportar', $language->get('button_exportar'));

        $template->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $template->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('tiposid.search'));
        $view->set('sort', $session->get('tiposid.sort'));
        $view->set('order', $session->get('tiposid.order'));
        $view->set('page', $session->get('tiposid.page'));

        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $view->set('titulo_ventana', '::. Exportar');

        $view->set('entry_modificar', "Modificar");
        $view->set('entry_agregar', "Agregar");

        $view->set('tipoid', '');
        $view->set('search', '');
        $view->set('tiposid.search', '');
        $session->set('tiposid.search', '');
        $cache->delete('tiposid');

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('tiposid', 'page'));
        $view->set('list', $url->ssl('tiposid'));

        if ($user->hasPermisos($user->getPERSONA(), 'tiposid', 'A')) {
            $view->set('insert', $url->ssl('tiposid', 'insert'));
        }

        if ($user->hasPermisos($user->getPERSONA(), 'tiposid', 'C'))
            $view->set('export', $url->ssl('tiposid', 'exportar'));

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_tiposid.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">
//		if ($request->has('tipoid', 'post')) {
//                    $session->set('tiposid.pais',$request->get('tipoid','post'));              
//                }

        if ($request->has('search', 'post')) {
            $session->set('tiposid.search', $request->get('search', 'post'));
        }


        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('tiposid.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('tiposid.order', (($session->get('tiposid.sort') == $request->get('sort', 'post')) && ($session->get('tiposid.order') == 'asc')) ? 'desc' : 'asc');
        }

        if ($request->has('sort', 'post')) {
            $session->set('tiposid.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('tiposid'));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $template = & $this->locator->get('template');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('heading_title', 'DATOS DE LA TIPO DE ID');
        $view->set('entry_descripcion', 'Descripci&oacute;n:');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $template->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('tab_general', $language->get('tab_general'));

        $view->set('tipoid', $request->get('tipoid'));

        if (($request->get('tipoid')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM tiposid WHERE tipoid = '" . $request->get('tipoid') . "'";
            $objeto_info = $database->getRow($consulta);
        }

        if ($request->has('tipoid', 'post')) {
            $view->set('tipoid', $request->get('tipoid', 'post'));
            $objeto_id = @$objeto_info['tipoid'];
        } else {
            $view->set('tipoid', $request->get('tipoid', 'get'));
            $objeto_id = @$objeto_info['tipoid'];
        }

        if ($request->has('descripcion', 'post')) {
            $view->set('descripcion', $request->get('descripcion', 'post'));
        } else {
            $view->set('descripcion', @$objeto_info['descripcion']);
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $template->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');
        $view->set('error_descripcion', @$this->error['descripcion']);
        //$view->set('error_titulo', @$this->error['titulo']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        //$view->set('action', $url->ssl('tiposid', $request->get('action'), array('tipoid' => $request->get('tipoid'))));
        $view->set('actionBoton', $request->get('action'));
        $view->set('cancel', $url->ssl('tiposid'));
        // </editor-fold>

        return $view->fetch('content/tipoid.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        if ((strlen($request->get('descripcion', 'post')) == 0)) {
            $this->error['descripcion'] = 'Debe ingresar la descripción';
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';

        if ($errores != '') {
            $this->error['message'] = $errores;
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Tipos de ID');

        if (($request->isPost()) && ($this->validateForm())) {
            $sql = "INSERT IGNORE INTO tiposid SET descripcion = '?' ";
            $consulta = $database->parse($sql, $request->get('descripcion', 'post'));
            $database->query($consulta);

            $id = $database->getLastId();
            $cache->delete('tiposid');
            $session->set('message', 'Se agreg&oacute; el tipo de id: ' . $id);

            die('ok');
        }
        $response->set($this->getForm());
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos del tipo de id');
        if (($request->isPost()) && ($this->validateForm())) {

            $id = $request->get('tipoid', 'post');
            $sql = "UPDATE tiposid SET descripcion ='?'  WHERE tipoid='?' ";
            $consulta = $database->parse($sql, $request->get('descripcion', 'post'), $id);
            $database->query($consulta);

            $cache->delete('tiposid');

            $session->set('message', 'Se actualiz&oacute; el tipo de id: ' . $id);

            die('ok');
        }

        $response->set($this->getForm());
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos de la tipo de id');
        if (($request->isPost())) {

            die('ok');
        }

        $response->set($this->getForm());
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        if (($request->get('tipoid')) && ($this->validateDelete())) {

            $id = $request->get('tipoid', 'get');

            //TODO: FALTA PROGRAMAR LA ELIMINACION Y VALIDACION VER SI NO HAY RELACIONES
            //$sql = "DELETE FROM tiposid WHERE tipoid = '" . $id . "' ";
            //$database->query($sql);

            $cache->delete('tiposid');

            $session->set('message', 'Se ha eliminado el tipo de id: ' . $id);

            $response->redirect($url->ssl('tiposid'));
        }

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

}

?>