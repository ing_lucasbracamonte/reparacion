<?php

class ControllerRegistrar extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'Usuarios');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">
        $cols = array();

        $cols[] = array(
            'name' => 'Persona',
            'sort' => 'persona',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Apellido',
            'sort' => 'apellido',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Nombre',
            'sort' => 'nombre',
            'align' => 'left'
        );
        $cols[] = array(
            'name' => 'Perfil',
            'sort' => 'perfil',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Estado',
            'sort' => 'estado',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'persona',
            'nombre',
            'descripcion'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        if (!$session->get('registro.persona')) {
            $sql = "SELECT persona, nombre, apellido,estado FROM registros ";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT persona, nombre, apellido,estado FROM registros  WHERE persona LIKE '?' OR apellido LIKE '?' OR nombre LIKE '?' OR estado LIKE '?'";
            $conca = " AND ";
        }

//		if ($session->get('persona.localidad') != '-1' && $session->get('persona.localidad') != '') {
//			$sql .= $conca."a.localidad = '".$session->get('persona.localidad')."'";
//			$conca = " AND ";
//		}

        if (in_array($session->get('registro.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('registro.sort') . " " . (($session->get('registro.order') == 'desc') ? 'DESC' : 'ASC');
        } else {
            $sql .= " ORDER BY persona ASC";
        }

        $results = $database->getRows($database->splitQuery($database->parse($sql, '%' . $session->get('registro.persona') . '%', '%' . $session->get('registro.persona') . '%'), $session->get('registro.page'), $config->get('config_max_rows')));

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">

        $rows = array();

        foreach ($results as $result) {

            //Armo la lista de grupos para mostrarla en la columna de perfil
            $consulta = "SELECT descripcion FROM registrosgrupos rg INNER JOIN grupos g ON rg.grupo = g.grupo WHERE rg.persona = '?'";
            $listaperfiles = $database->getRows($database->parse($consulta, $result['persona']));

            foreach ($listaperfiles as $grupo) {
                $perfiles += ', ' . $grupo['descripcion'];
            }

            $cell = array();

            $cell[] = array(
                'value' => @$result['persona'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['apellido'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['nombre'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['estado'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$perfiles,
                'align' => 'left',
                'default' => 0
            );

            $action = array();


            if ($user->hasPermisos($user->getPERSONA(), 'registro', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'text' => $language->get('button_update'),
                    'prop_a' => array('href' => $url->ssl('registro', 'update', array('persona' => $result['persona'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'registro', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-24.png',
                    'text' => 'No Aprobado',
                    'prop_a' => array('href' => $url->ssl('registro', 'noaprobar', array('persona' => $result['persona'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'registro', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-24.png',
                    'text' => 'Aprobado',
                    'prop_a' => array('href' => $url->ssl('registro', 'aprobar', array('persona' => $result['persona'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'registro', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-24.png',
                    'text' => 'Generar Usuario',
                    'prop_a' => array('href' => $url->ssl('registro', 'crearusuario', array('persona' => $result['persona'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'registro', 'B')) {
                $action[] = array(
                    'icon' => 'img/iconos-11.png',
                    'text' => $language->get('button_delete'),
                    'prop_a' => array('href' => "javascript:ActionDelete('" . $url->ssl('registro', 'delete', array('persona' => $result['persona'])) . "');")
                );
            }

            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        if ($session->get('registro.persona')) {
            $view->set('persona', $session->get('registro.persona'));
        } else {
            $view->set('persona', '');
        }

        $view->set('heading_title', 'REGISTROS');
        $view->set('heading_title2', 'Filtrar');
        $view->set('heading_description', 'Gesti&oacute;n de Registros');

        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/NOTICIAS.png');
        $view->set('placeholder_buscar', 'BUSCA POR DNI O NOMBRE O estado');

        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        $view->set('button_exportar', $language->get('button_exportar'));

        $template->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $template->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('sort', $session->get('registro.sort'));
        $view->set('order', $session->get('registro.order'));
        $view->set('page', $session->get('registro.page'));

        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $view->set('titulo_ventana', '::. Exportar listado de registros');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('registro', 'page'));
        $view->set('list', $url->ssl('registro'));
        if ($user->hasPermisos($user->getPERSONA(), 'registro', 'A')) {
            $view->set('insert', $url->ssl('registro', 'insert'));
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">
        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_registros.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">
        if ($request->has('persona', 'post')) {
            $session->set('registro.persona', $request->get('persona', 'post'));
        }

        if ($request->has('estado', 'post')) {
            $session->set('registro.estado', $request->get('estado', 'post'));
        }
//                
//                if ($request->has('localidad', 'post')){
//                    $session->set('persona.localidad',$request->get('localidad','post'));
//                }

        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('registro.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('registro.order', (($session->get('registro.sort') == $request->get('sort', 'post')) && ($session->get('registro.order') == 'asc')) ? 'desc' : 'asc');
        }

        if ($request->has('sort', 'post')) {
            $session->set('registro.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('registrar'));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');

        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('entry_persona', 'Documento:');
        $view->set('entry_apellido', 'Apellido:');
        $view->set('entry_nombre', 'Nombre:');
        $view->set('entry_password', 'Contraseña:');
        $view->set('entry_repassword', 'Confirma Contraseña:');
        $view->set('entry_localidad', 'Localidad:');
        $view->set('entry_telefono', 'T.E.:');
        $view->set('entry_mail', 'E-mail:');
        $view->set('entry_club', 'Club:');
        $view->set('entry_remail', 'Confirma E-mail:');
        $view->set('entry_fecha', 'Fecha nacimiento:');
        $view->set('entry_grupo', 'Grupo:');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">

        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        $view->set('tab_general', $language->get('tab_general'));

        if (($request->get('persona')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM personas WHERE persona = '?'";
            $persona_info = $database->getRow($database->parse($consulta, $request->get('persona')));
        }

        if ($request->has('persona', 'post')) {
            $view->set('persona', $request->get('persona', 'post'));
            $persona_id = $request->get('persona', 'post');
        } else {
            $view->set('persona', @$persona_info['persona']);
            $persona_id = @$persona_info['persona'];
        }

        if ($request->has('password', 'post')) {
            $view->set('password', $request->get('password', 'post'));
        } else {
            $view->set('password', @$persona_info['clave']);
        }

        if ($request->has('repassword', 'post')) {
            $view->set('repassword', $request->get('repassword', 'post'));
        } else {
            $view->set('repassword', @$persona_info['clave']);
        }

        if ($request->has('apellido', 'post')) {
            $view->set('apellido', $request->get('apellido', 'post'));
        } else {
            $view->set('apellido', $persona_info['apellido']);
        }

        if ($request->has('nombre', 'post')) {
            $view->set('nombre', $request->get('nombre', 'post'));
        } else {
            $view->set('nombre', $persona_info['nombre']);
        }

//		if ($request->has('domicilio', 'post')) {
//			$view->set('domicilio', $request->get('domicilio', 'post'));
//		} else {
//			$view->set('domicilio', @$persona_info['domicilio']);
//		}

        if ($request->has('grupo', 'post')) {
            $view->set('grupo', $request->get('grupo', 'post'));
        } else {
            $view->set('grupo', 'UF');
        }

        if ($request->has('localidad', 'post')) {
            $view->set('localidad', $request->get('localidad', 'post'));
        } else {
            $view->set('localidad', @$persona_info['localidad']);
        }

        if ($request->has('telefono', 'post')) {
            $view->set('telefono', $request->get('telefono', 'post'));
        } else {
            $view->set('telefono', @$persona_info['telefono']);
        }

//               if ($request->has('fechanacimiento', 'post')) {
//                        $view->set('fechanacimiento', $request->get('fechanacimiento', 'post'));
//		} else {
//                    if ($persona_info['fechanacimiento'] != '') $view->set('fechanacimiento', date('d-m-Y', strtotime(@$persona_info['fechanacimiento'])));
//                    else $view->set('fechanacimiento','');
//			
//		}
        if ($request->has('fechanacimiento', 'post')) {
            $view->set('fechanacimiento', $request->get('fechanacimiento', 'post'));
        } else {
            if (@$persona_info['fechanacimiento'] != '') {
                $fecha = date('d/m/Y', strtotime(@$persona_info['fechanacimiento']));
            } else {
                $fecha = '';
            }
            $view->set('fechanacimiento', $fecha);
        }

        //$view->set('localidades', $database->getRows("SELECT localidad, descripcion FROM localidades ORDER BY descripcion"));

        if ($request->has('mail', 'post')) {
            $view->set('mail', $request->get('mail', 'post'));
        } else {
            $view->set('mail', @$persona_info['mail']);
        }

        if ($request->has('remail', 'post')) {
            $view->set('remail', $request->get('remail', 'post'));
        } else {
            $view->set('remail', @$persona_info['mail']);
        }
        if ($request->has('club', 'post')) {
            $view->set('club', $request->get('club', 'post'));
        } else {
            $view->set('club', @$persona_info['club']);
        }
        $view->set('clubes', $database->getRows("SELECT club, descripcion FROM clubes ORDER BY descripcion"));

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $template->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');
        $view->set('error_persona', @$this->error['persona']);
        $view->set('error_password', @$this->error['password']);
        $view->set('error_repassword', @$this->error['repassword']);
        $view->set('error_apellido', @$this->error['apellido']);
        $view->set('error_nombre', @$this->error['nombre']);
        $view->set('error_apellido', @$this->error['nombre']);
        $view->set('error_telefono', @$this->error['telefono']);
        $view->set('error_localidad', @$this->error['localidad']);
        $view->set('error_mail', @$this->error['mail']);
        $view->set('error_grupo', @$this->error['grupo']);
        $view->set('error_remail', @$this->error['remail']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('registrar', $request->get('action'), array('persona' => $request->get('persona'))));
        $view->set('actionBoton', $request->get('action'));
        $view->set('cancel', $url->ssl('registrar'));
        // </editor-fold>

        return $view->fetch('content/registrar.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $common = & $this->locator->get('common');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        if ((strlen($request->get('persona', 'post')) == 0)) {
            $this->error['persona'] = 'Debe ingresar el n&uacute;mero de documento';
        }

        if (strlen($request->get('persona', 'post')) < 7 || !is_numeric($request->get('persona', 'post'))) {
            $this->error['persona'] = 'No es un n&uacute;mero de documento v&aacute;lido.';
        }

        if ((strlen($request->get('password', 'post')) == 0)) {
            $this->error['password'] = 'Debe ingresar una contraseña';
        }

        if ((strlen($request->get('repassword', 'post')) == 0)) {
            $this->error['repassword'] = 'Debe ingresar la confirmacion de la contraseña';
        }

        if ($request->get('repassword', 'post') != $request->get('password', 'post')) {
            $this->error['repassword'] = 'La contraseña y la confirmacion no coinciden';
        }

        if (@$this->error['persona'] == '') {
            $sql = "SELECT count(persona) as total FROM personas WHERE persona ='?'";
            $persona = $database->getRow($database->parse($sql, $request->get('persona', 'post')));

            if ($persona['total'] > 0) {
                $this->error['persona'] = 'Esta documento ya existe en el sistema';
            }
        }

        if ((strlen($request->get('apellido', 'post')) < 2) || (strlen($request->get('apellido', 'post')) > 100)) {
            $this->error['apellido'] = 'Debe ingresar el apellido';
        }

        if ((strlen($request->get('nombre', 'post')) < 2) || (strlen($request->get('nombre', 'post')) > 100)) {
            $this->error['nombre'] = 'Debe ingresar el nombre';
        }

        if ($request->get('grupo', 'post') != 'UF' && $request->get('club', 'post') == '-1') {
            $this->error['grupo'] = 'Si no elige Usuario Final, debe seleccionar su club';
        }
//		if ((strlen($request->get('domicilio', 'post')) == 0)) {
//			$this->error['domicilio'] = 'Debe ingresar el domicilio';
//                        $this->error['tab_general'] = '1';
//		}
//
//		if (!$request->has('geonameinfo','post')) {
//                    
//			$this->error['localidad'] = 'Debe seleccionar la localidad';
//                        $this->error['tab_general'] = '1';
//		}
//                else {
//                    $geo = json_decode($request->get('geonameinfo','post'),true);
//                    if (!isset($geo['geonameId']) ) {
//                        $this->error['localidad'] = 'Debe seleccionar la localidad';
//                        $this->error['tab_general'] = '1';
//                    }
//                }
//
//		if ((strlen($request->get('telefono', 'post')) == 0)) {
//			$this->error['telefono'] = 'Debe ingresar el tel&eacute;fono';
//                        $this->error['tab_general'] = '1';
//		}
//
//		if ((strlen($request->get('celular', 'post')) == 0)) {
//			$this->error['celular'] = 'Debe ingresar el celular';
//                        $this->error['tab_general'] = '1';
//		}
//
        if (!$common->validarDuplicado($request->get('mail', 'post'), $database, $mensaje, $request->get('persona', 'post'))) {
            $this->error['mail'] = $mensaje;
        }

        if ($request->get('remail', 'post') != $request->get('mail', 'post')) {
            $this->error['remail'] = 'El mail y la confirmacion no coinciden';
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $common = & $this->locator->get('common');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Registro');

        if (($request->isPost()) && ($this->validateForm())) {
            $persona_id = strtoupper($request->get('persona', 'post'));

            // <editor-fold defaultstate="collapsed" desc="FECHA">
            $fecha_d = '';
            if ($request->get('fechanacimiento', 'post') != '') {
                $dated = explode('/', $request->get('fechanacimiento', 'post'));
                $fecha_d = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
            }
            // </editor-fold>

            $nombre = ucwords(strtolower($request->get('nombre', 'post')));
            $apellido = ucwords(strtolower($request->get('apellido', 'post')));
            $club = $request->get('club', 'post');
            $categoria = '';
            $clave = $common->getEcriptar($request->get('password', 'post'));

            if ($request->get('grupo', 'post') != '') {
                $grupo = $request->get('grupo', 'post');
            } else {
                $grupo = 'UF'; // Si viene vacío lo pongo como Usuario Final                        
            }

            $creacion = date("Y-m-d H:i:s");

            $url_registro = $url->rawssl('registrar', 'update', array('persona' => $persona_id, 'token' => sha1($persona_id . $creacion)));

            $sql = "INSERT INTO personas SET persona = '?', clave = '?', nombre ='?', apellido ='?', fechanacimiento = NULLIF('?',''), localidad = '?', club ='?', categoria = NULLIF('?',''), telefono ='?',  mail ='?', mail_confirmado=0, usuario = '?', creacion = '?'";
            $database->query($database->parse($sql, $persona_id, $clave, $nombre, $apellido, $fecha_d, $request->get('localidad', 'post'), $club, $categoria, $request->get('telefono', 'post'), $request->get('mail', 'post'), $user->getPERSONA(), $creacion));

            // <editor-fold defaultstate="collapsed" desc="GRUPO">


            $sql = "INSERT IGNORE INTO personasgrupos SET persona = '?', grupo = '?', usuario = '?', fechaultmodificacion = NOW()";
            $database->query($database->parse($sql, $persona_id, $grupo, $user->getPERSONA()));
            // </editor-fold>
            // 
            //ENVIAR MAIL DE VALIDACION QUE CUANDO HAGA CLICK EN UN LINK LO MANDO A verificarRegistro
            // $common->enviarMailRegistro($nombre, $request->get('mail', 'post'), $url_registro);

            $session->set('message', 'El registro fue satisfactorio, le llegar&aacute; un mail a la cuenta elegida para activar el registro.');

            die('ok');
        }

        $response->set($this->getForm());
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $common = & $this->locator->get('common');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Validar registro');

        if ($request->has('persona', 'get') && $request->has('token', 'get')) {

            $persona_id = strtoupper($request->get('persona', 'get'));

            $sql = "SELECT persona, creacion, mail_confirmado FROM personas WHERE persona = '?'";
            $results = $database->getRow($database->parse($sql, $persona_id));

            if (sha1($results['persona'] . $results['creacion']) == $request->get('token', 'get') && $results['mail_confirmado'] == 0) {
                $sql = "UPDATE personas SET mail_confirmado = 1, usuario = '?' WHERE persona = '?'";
                $database->query($database->parse($sql, $persona_id, $persona_id));
                $session->set('message', 'Se ha validado correctamente su cuenta de mail. Inicie sesi&oacute;n con su documento y la clave elegida en el registro. !Bienvenido!');
            } elseif ($results['mail_confirmado'] == 1) {
                $session->set('message', 'Usted ya valid&oacute; su cuenta!');
            } else {
                $session->set('error', 'Los datos son inv&aacute;lidos!');
            }
        }

        $response->redirect($url->ssl(DEFAULT_HOMEPAGE));
    }

    function registroAtrasado() {
        $common = & $this->locator->get('common');
        $url = & $this->locator->get('url');

        $common->enviarMailRegistroAtrasado($url);
    }
}

?>
