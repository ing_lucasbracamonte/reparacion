<?php

class ControllerTiposaccesorio extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'GESTIÓN DE TIPOS DE ACCESORIOS');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
         $cache = & $this->locator->get('cache');
       $request = & $this->locator->get('request');
        // </editor-fold>
         // <editor-fold defaultstate="collapsed" desc="LIMPIA FILTRO Y VARIBLES DE SESSION">

        if ($request->get('filtra') == NULL) {

            $session->set('tiposaccesorio.search', '');
            $session->set('tiposaccesorio.sort', '');
            $session->set('tiposaccesorio.order', '');
            $session->set('tiposaccesorio.page', '');

            $view->set('search', '');
            $view->set('tiposaccesorio.search', '');
                        
            $cache->delete('tiposaccesorio');
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">

        $cols = array();

        $cols[] = array(
            'name' => 'Tipo',
            'sort' => 'tipo',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Descripci&oacute;n',
            'sort' => 'descripcion',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'tipo',
            'descripcion'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        set_time_limit(0);

        if (!$session->get('tiposaccesorio.search')) {
            $sql = "SELECT * FROM tiposaccesorio  ";
        } else {
            $sql = "SELECT * FROM tiposaccesorio WHERE tipoaccesorio LIKE '?' OR descripcion LIKE '?'";
        }

        if (in_array($session->get('tiposaccesorio.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('tiposaccesorio.sort') . " " . (($session->get('tiposaccesorio.order') == 'desc') ? 'desc' : 'asc');
        } else {
            $sql .= " ORDER BY descripcion ASC";
        }

//		$consul = $database->splitQuery($database->parse($sql,'%' . $session->get('tiposaccesorio.search') . '%','%' . $session->get('tiposaccesorio.search') . '%'), $session->get('tiposaccesorio.page'), $config->get('config_max_rows'));
//
//                $results = $database->getRows($consul);

        $consulta = $database->parse($sql, '%' . $session->get('tiposaccesorio.search') . '%', '%' . $session->get('tiposaccesorio.search') . '%');
        $results = $database->getRows($consulta);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['tipoaccesorio'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['descripcion'],
                'align' => 'left',
                'default' => 0
            );

            $action = array();

            if ($user->hasPermisos($user->getPERSONA(), 'tiposaccesorio', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa fa-fw fa-pencil',
                    'text' => $language->get('button_update'),
                    'prop_a' => array('href' => $url->ssl('tiposaccesorio', 'update', array('tipoaccesorio' => $result['tipoaccesorio'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'tiposaccesorio', 'B')) {
                $action[] = array(
                    'icon' => 'img/iconos-11.png',
                    'text' => $language->get('button_delete'),
                    'class' => 'fa fa-fw fa-trash-o',
                    'prop_a' => array('href' => "javascript:ActionDelete('" . $url->ssl('tiposaccesorio', 'delete', array('tipoaccesorio' => $result['tipoaccesorio'])) . "');")
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'tiposaccesorio', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw fa-search',
                    'text' => $language->get('button_consult'),
                    'prop_a' => array('href' => $url->ssl('tiposaccesorio', 'consulta', array('tipoaccesorio' => $result['tipoaccesorio'])))
                );
            }

            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        //<editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('tiposaccesorio.page'));

        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        $view->set('heading_title', 'Tipos de Accesorio');
        $view->set('placeholder_buscar', 'BUSCA POR ID O DESCRIPCION');

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        //$view->set('consultaVacia', $url->ssl('consulta','consulta_vacia'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        //$view->set('button_consultavacia', "Vacía");
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('tiposaccesorio.search'));
        $view->set('sort', $session->get('tiposaccesorio.sort'));
        $view->set('order', $session->get('tiposaccesorio.order'));
        $view->set('page', $session->get('tiposaccesorio.page'));

        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $view->set('titulo_ventana', '::. Exportar');

        $view->set('entry_modificar', "Modificar");
        $view->set('entry_agregar', "Agregar");

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('tiposaccesorio', 'page'));
        $view->set('list', $url->ssl('tiposaccesorio'));

        if ($user->hasPermisos($user->getPERSONA(), 'tiposaccesorio', 'A')) {
            $view->set('insert', $url->ssl('tiposaccesorio', 'insert'));
        }

        if ($user->hasPermisos($user->getPERSONA(), 'tiposaccesorio', 'C'))
            $view->set('export', $url->ssl('tiposaccesorio', 'exportar'));

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_tiposaccesorio.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">
//		if ($request->has('tipoaccesorio', 'post')) {
//                    $session->set('tiposaccesorio.pais',$request->get('tipoaccesorio','post'));              
//                }

        if ($request->has('search', 'post')) {
            $session->set('tiposaccesorio.search', $request->get('search', 'post'));
        }


        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('tiposaccesorio.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('tiposaccesorio.order', (($session->get('tiposaccesorio.sort') == $request->get('sort', 'post')) && ($session->get('tiposaccesorio.order') == 'asc')) ? 'desc' : 'asc');
        }

        if ($request->has('sort', 'post')) {
            $session->set('tiposaccesorio.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('tiposaccesorio', 'index', array('filtra' => '1')));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $template = & $this->locator->get('template');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('heading_title', 'DATOS DE LA TIPO DE ACCESORIO');
        $view->set('entry_descripcion', 'Descripci&oacute;n:');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('tab_general', $language->get('tab_general'));

        $view->set('tipoaccesorio', $request->get('tipoaccesorio'));

        if (($request->get('tipoaccesorio')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM tiposaccesorio WHERE tipoaccesorio = '" . $request->get('tipoaccesorio') . "'";
            $objeto_info = $database->getRow($consulta);
        }

        if ($request->has('tipoaccesorio', 'post')) {
            $view->set('tipoaccesorio', $request->get('tipoaccesorio', 'post'));
            $objeto_id = @$objeto_info['tipoaccesorio'];
        } else {
            $view->set('tipoaccesorio', $request->get('tipoaccesorio', 'get'));
            $objeto_id = @$objeto_info['tipoaccesorio'];
        }

        if ($request->has('descripcion', 'post')) {
            $view->set('descripcion', $request->get('descripcion', 'post'));
        } else {
            $view->set('descripcion', @$objeto_info['descripcion']);
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        $view->set('error_texto_error', @$this->error['texto_error']);
        //$view->set('error_titulo', @$this->error['titulo']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        //$view->set('action', $url->ssl('tiposaccesorio', $request->get('action'), array('tipoaccesorio' => $request->get('tipoaccesorio'))));
        $view->set('actionBoton', $request->get('action'));
        $view->set('cancel', $url->ssl('tiposaccesorio'));
        // </editor-fold>

        return $view->fetch('content/tipoaccesorio.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        if ((strlen($request->get('descripcion', 'post')) == 0)) {
            $errores = 'Debe ingresar la descripción.';
        }

        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';

        $consulta_info = $database->getRow("SELECT COUNT(*) as total FROM accesorios WHERE tipoaccesorio = '" . $request->get('tipoaccesorio') . "'");
        if (@$consulta_info['total'] != 0) {
            $errores .= 'No es posible eliminar la tipo de accesorio, tiene accesorios asignados. <br>';
        }

        if ($errores != '') {
            $this->error['message'] = $errores;
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Tipos de Accesorio');

        if (($request->isPost()) && ($this->validateForm())) {
            $sql = "INSERT IGNORE INTO tiposaccesorio SET descripcion = '?' ";
            $consulta = $database->parse($sql, $request->get('descripcion', 'post'));
            $database->query($consulta);

            $id = $database->getLastId();
            $cache->delete('tiposaccesorio');
            $session->set('message', 'Se agreg&oacute; el tipo de accesorio: ' . $id);

            $response->redirect($url->ssl('tiposaccesorio'));
        }
        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos del tipo de accesorio');
        if (($request->isPost()) && ($this->validateForm())) {

            $id = $request->get('tipoaccesorio', 'post');
            $sql = "UPDATE tiposaccesorio SET descripcion ='?'  WHERE tipoaccesorio='?' ";
            $consulta = $database->parse($sql, $request->get('descripcion', 'post'), $id);
            $database->query($consulta);

            $cache->delete('tiposaccesorio');

            $session->set('message', 'Se actualiz&oacute; el tipo de accesorio: ' . $id);

            $response->redirect($url->ssl('tiposaccesorio'));
        }
        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos de la tipo de accesorio');
        if (($request->isPost())) {
            $response->redirect($url->ssl('tiposaccesorio'));
        }

        $response->set($this->getForm());
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        if (($request->get('tipoaccesorio')) && ($this->validateDelete())) {

            $id = $request->get('tipoaccesorio', 'get');

            //TODO: FALTA PROGRAMAR LA ELIMINACION Y VALIDACION VER SI NO HAY RELACIONES
            $sql = "DELETE FROM tiposaccesorio WHERE tipoaccesorio = '" . $id . "' ";
            $database->query($sql);

            $cache->delete('tiposaccesorio');

            $session->set('message', 'Se ha eliminado el parametro: ' . $id);

            $response->redirect($url->ssl('tiposaccesorio'));
        }

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

}

?>