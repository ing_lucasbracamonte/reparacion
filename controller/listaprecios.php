<?php

class ControllerListaprecios extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'GESTIÓN DE ARTICULOS');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
        // </editor-fold>
        //<editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('listaprecios.page'));

        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        $view->set('heading_title', 'ARTICULOS');
        $view->set('placeholder_buscar', 'BUSCA POR ID O MARCA O MODELO O TIPO');

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        //$view->set('consultaVacia', $url->ssl('consulta','consulta_vacia'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        //$view->set('button_consultavacia', "Vacía");
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('listaprecios.search'));
        $view->set('sort', $session->get('listaprecios.sort'));
        $view->set('order', $session->get('listaprecios.order'));
        $view->set('page', $session->get('listaprecios.page'));

        //$view->set('listaprecios.tipoproducto', '-1');
        $view->set('tiposproducto', $database->getRows("SELECT * FROM tiposproducto ORDER BY descripcion ASC"));

        $view->set('marcas', $database->getRows("SELECT * FROM marcas ORDER BY descripcion ASC"));


        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $view->set('titulo_ventana', '::. Exportar');

        $view->set('entry_modificar', "Modificar");
        $view->set('entry_agregar', "Agregar");

        $view->set('modelo', '');
        $view->set('search', '');
        $view->set('listaprecios.search', '');
        $cache->delete('listaprecios');

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('listaprecios', 'page'));
        $view->set('list', $url->ssl('listaprecios'));


        if ($user->hasPermisos($user->getPERSONA(), 'listaprecios', 'M')) {
            $view->set('insertImport', $url->ssl('listaprecios', 'insertImport'));
        }

        if ($user->hasPermisos($user->getPERSONA(), 'listaprecios', 'A'))
            $view->set('exportExcel', $url->ssl('listaprecios', 'exportExcel'));

        //$view->set('addPais', $url->ssl('listaprecios','insert'));
        // $view->set('updatePersona', $url->ssl('paciente','update', array('persona' => $result['persona'])));
        //$view->set('updatePais', $url->ssl('listaprecios','update'));
        // </editor-fold>

        return $view->fetch('content/listaprecios.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">
//		if ($request->has('modelo', 'post')) {
//                    $session->set('listaprecios.pais',$request->get('modelo','post'));              
//                }

        if ($request->has('search', 'post')) {
            $session->set('listaprecios.search', $request->get('search', 'post'));
        }

        if ($request->has('tipoproducto', 'post')) {
            $session->set('listaprecios.tipoproducto', $request->get('tipoproducto', 'post'));
        }

        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('listaprecios.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('listaprecios.order', (($session->get('listaprecios.sort') == $request->get('sort', 'post')) && ($session->get('listaprecios.order') == 'asc')) ? 'desc' : 'asc');
        }

        if ($request->has('sort', 'post')) {
            $session->set('listaprecios.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('listaprecios'));
    }

    function getFormImport() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $template = & $this->locator->get('template');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('heading_title', 'IMPORTAR ARTICULOS');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $template->set('message', $session->get('message'));
        $session->delete('message');


        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');
        $view->set('error_modelo', @$this->error['modelo']);
        //$view->set('error_descripcion', @$this->error['descripcion']);
        $view->set('error_marca', @$this->error['marca']);
        //$view->set('error_id', @$this->error['id']);
        //$view->set('error_tipoid', @$this->error['tipoid']);
        $view->set('error_tipoproducto', @$this->error['tipoproducto']);
        $view->set('error_texto_error', @$this->error['texto_error']);
        //$view->set('error_titulo', @$this->error['titulo']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        //$view->set('action', $url->ssl('listaprecios', $request->get('action'), array('modelo' => $request->get('modelo'))));
        $view->set('actionBoton', $request->get('action'));
        $view->set('cancel', $url->ssl('listaprecios'));

        //$view->set('exportXlsModelo', $url->ssl('listaprecios','exportXlsModelo'));
        // </editor-fold>

        return $view->fetch('content/listaprecioImport.tpl');
    }

    function insertImport() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $user = & $this->locator->get('user');
        $url = & $this->locator->get('url');
        $upload = & $this->locator->get('upload');
        $common = & $this->locator->get('common');
        // </editor-fold>

        $template->set('title', 'ARTICULOS');

        if (($request->isPost()) && ($this->validateFormImport())) {
            
            set_time_limit(0);
            
            $nombreDirectorio = '';
            $nombreArchivo = '';

            // <editor-fold defaultstate="collapsed" desc="XLS">
            //Cargo la imágen1     
            $objeto_id = $common->GetaaaaMMddhhmmssmm();
            if ($upload->has('archivoupload')) {
                $nombreArchivo = $objeto_id . '.' . end(explode(".", $upload->getName('archivoupload')));
                $nombreDirectorio = 'Archivos/ImportListaPrecio/';
                $upload->save('archivoupload', $nombreDirectorio . $nombreArchivo);

                $nombreCompleto = realpath($nombreDirectorio . $nombreArchivo);
                //ARMAR EL ARREGLO DESDE EL EXCEL
                require_once 'library/Excel/reader.php';
                $data = new Spreadsheet_Excel_Reader();
                $data->read($nombreCompleto);

                $errores = '';
                $articulosXls = array();
                $saltodelinea = '';

                for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
                    $Linea = $data->sheets[0]['cells'][$i];
                    if ($Linea != null) {
                        $ResultadoValidacion = $this->ValidaLinea($Linea);
                        if ($ResultadoValidacion == 'ok') {
                            //MARCA colA
                            $colA = $data->sheets[0]['cells'][$i][1];

                            //CON precio vta colG
                            $colD = $data->sheets[0]['cells'][$i][4];
                            $flo = str_replace(",", ".", $colD);
                            $colD = $flo;

                            $colH = $data->sheets[0]['cells'][$i][8];

                            $articulosXls[] = array(
                                'articulo' => $colA,
                                'preciovta' => $colD
                            );
                        } else {
                            //meto en un array de errores
                            $errores .= $saltodelinea . 'Linea: ' . $i . ' (' . $ResultadoValidacion . ')';
                            $saltodelinea = '<br>';
                        }
                    }
                }

                if ($errores != '') {

                    //CARGO UNA VARIABLE CON LOS ERRORES Y LOS MUESTRO EN ROJO SIN SUBIR NADA

                    $this->error['texto_error'] = $errores;
                } else {
                    if ($upload->getError('archivoupload') > 0) {
                        $this->error['archivo'] = $upload->getErrorMsg($upload->getError('archivoupload'));
                    } else {
                        //HAGO LOS INSERT ACA
                        foreach ($articulosXls as $art) {

                            $sql = "UPDATE articulos SET preciovta='?', au_usuario='?',au_accion='M',au_fecha_hora=NOW() WHERE articulo='?' ";
                            $consulta = $database->parse($sql, $art['preciovta'], $user->getPERSONA(), $art['articulo']);
                            $database->query($consulta);
                        }

                        $cache->delete('listaprecios');

                        $session->set('message', 'Se actualizado de forma correcta todos los precios.');

                        $response->redirect($url->ssl('listaprecios'));
                    }
                }
            }

            // </editor-fold>
        }

        $template->set('content', $this->getFormImport());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function exportExcel() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        // </editor-fold>

        set_time_limit(0);

        //** PHPExcel **//
        require_once (DIR_INCLUDES . '/library/phpexcel/PHPExcel.php');

        date_default_timezone_set('America/Argentina/Buenos_Aires');
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set properties of the file
        $objPHPExcel->getProperties()->setCreator("NombreEmpresa")
                ->setLastModifiedBy("NombreEmpresa")
                ->setTitle("Planilla web exportada: " . date('d-m-Y'))
                ->setSubject("Planilla web exportada: " . date('d-m-Y'));

        //genero las columnas del excel
        $letra = 'A';
        $fila_inicial = 2; //fila desde donde empiezo a cargar la tabla en el excel. Arriba va el encabezado
        // <editor-fold defaultstate="collapsed" desc="PROPIEDADES DE LAS COLUMNAS">
        $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //COLUMNA A
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'ID');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA B
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'COD INTERNO');
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA C
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(37);
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', 'MODELO');
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        //COLUMNA D
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(22);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', 'PRECIO VTA');
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">

        $sql = "SELECT * FROM vw_grilla_articulos  ";
        $conca = " WHERE ";

        //OBTENEMOS TODOS LOS TIPOS MARCADOS
        $tiposdeproducto = $request->get('tipos', 'post', array());

        if (count($tiposdeproducto) > 0) {
            $conca2 = ' (';
            foreach ($tiposdeproducto as $tipo) {
                $sql .= $conca . $conca2 . "tipoproducto = '" . $tipo . "'  ";
                $conca = " OR ";
                $conca2 = '';
            }
            $sql .= ") ";
            $conca = ' AND ';
        }

        //OBTENEMOS TODOS LOS TIPOS MARCADOS
        $marcas = $request->get('marcas', 'post', array());

        if (count($marcas) > 0) {
            $conca2 = ' (';
            foreach ($marcas as $marca) {
                $sql .= $conca . $conca2 . " marca = '" . $marca . "'  ";
                $conca = " OR ";
            }
            $sql .= ") ";
        }

        $sql .= " ORDER BY marca, modelo ASC";


//		$consul = $database->splitQuery($database->parse($sql,'%' . $session->get('articulos.search') . '%','%' . $session->get('articulos.search') . '%'), $session->get('articulos.page'), $config->get('config_max_rows'));
//
//                $results = $database->getRows($consul);

        $results = $database->getRows($sql);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="LLANAR LOS DATOS">

        $num = $fila_inicial;

        foreach ($results as $result) {

            //COLUMNA A
            $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $num, $result['articulo']);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            //COLUMNA B
            $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $num, $result['codbarrainterno']);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            //COLUMNA C
            $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $num, $result['descmodelo']);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            //COLUMNA D
            $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' . $num, $result['preciovta']);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $num++;
        }

        //SEGUIR EN COMPLETAR EL EXCEL
        //POR CADA INSCRIPTO IR BUSCANDO CADA RESPUESTA Y COMPLETAR SEGUN CORRESPONDA
        // </editor-fold>
        // Nombre de la hoja del libro
        $objPHPExcel->getActiveSheet()->setTitle('ExportPrecios');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Save Excel 2007 file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');

        // It will be called file.xls | esta linea hace que sea una descarga y no una pestaña aparte
        header('Content-Disposition: attachment; filename="ExportPrecios.xls"');

        header('Cache-Control: max-age=0');

        // Write file to the browser
        $objWriter->save('php://output');
    }

    function validateFormImport() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $upload = & $this->locator->get('upload');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        if ($upload->has('archivoupload')) {
            $tiposPermitidos = array(
                'application/vnd.ms-excel',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'application/vnd.ms-excel'
            );

            if (!in_array($upload->getType('archivoupload'), $tiposPermitidos)) {
                $errores = 'No es un documento v&aacute;lido para cargar (' . $upload->getType('archivoupload') . ')';
            }
        } elseif ($upload->hasError('archivoupload')) {
            $errores = $upload->getErrorMsg($upload->getError('archivoupload'));
        } else {
            $errores = 'Debe elegir un documento excel para cargar.';
        }


        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function ValidaLinea(array $Linea) {
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $upload = & $this->locator->get('upload');
        $common = & $this->locator->get('common');
        $user = & $this->locator->get('user');

        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        //valida que la marca exista
        //MARCA colA
        $colA = $Linea[1];
        if ($colA != null) {
            $verifica = $database->getRow("select count(articulo) as verifica from articulos where articulo ='" . $colA . "'");
            if ($verifica["verifica"] == 0) {
                return 'Artículo no existe.';
            }
        } else {
            return 'Artículo no existe.';
        }


        $colD2 = str_replace(".", "", $colD);
        $rt3 = is_numeric($colD);
        if ($colD != null && $colD != '') {
            if (!is_numeric($colD)) {
                return 'Debe ingresar un número valido en la columna D';
            }
        }

        $colD = $Linea[4];
        $colD = str_replace(".", "", $colD);
        $colD = str_replace(",", "", $colD);
        if ($colD != null && $colD != '') {
            if (!is_numeric($colD)) {
                return 'Debe ingresar un número valido en la columna D';
            }
        } else {
            return 'Debe ingresar un precio.';
        }

        // </editor-fold>

        return 'ok';
    }

    function getModelo() {
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');

        $miDescripcion = $request->get('term', 'get');
        //$miPersona = "lucas";
        // $codigo = $database->getRows("select concat(apellido,', ', nombre) as value, persona_id  as id from personas where persona_id ='" . $miPersona . "'");		

        $sql = "SELECT modelo as id, CONCAT(descmarca,' - ',descripcion) as value "
                . "FROM vw_list_modelos "
                . "WHERE descripcion LIKE '?' OR descmarca LIKE '?'";
        $consulta = $database->parse($sql, '%' . $miDescripcion . '%', '%' . $miDescripcion . '%');
        $codigo = $database->getRows($consulta);

        $varia = utf8_decode(json_encode($codigo));


        echo $varia;
    }

}

?>