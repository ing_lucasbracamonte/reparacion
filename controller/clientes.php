<?php

class ControllerClientes extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'Clientes');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
        $request = & $this->locator->get('request');
        // </editor-fold>
         // <editor-fold defaultstate="collapsed" desc="LIMPIA FILTRO Y VARIBLES DE SESSION">

        if ($request->get('filtra') == NULL) {

            $session->set('clientes.search', '');
            $session->set('clientes.sort', '');
            $session->set('clientes.order', '');
            $session->set('clientes.page', '');

            $view->set('search', '');
            $view->set('clientes.search', '');
                        
            $cache->delete('clientes');
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">
        $cols = array();

        $cols[] = array(
            'name' => 'Cliente',
            'sort' => 'persona',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Nombre',
            'sort' => 'apellido, nombre',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Mail',
            'sort' => 'mail',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Celular',
            'sort' => 'celular',
            'align' => 'left'
        );


        $cols[] = array(
            'name' => 'Tipo cliente',
            'sort' => 'descripcion',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'persona',
            'apellido, nombre',
            'descripcion'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        if (!$session->get('clientes.search')) {
            $sql = "SELECT * FROM vw_grilla_personas ";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT * FROM vw_grilla_personas WHERE persona LIKE '?' OR nombre LIKE '?' OR apellido LIKE '?' ";
            $conca = " AND ";
        }

        $sql .= $conca . " grupo = 'CL'";

        if (in_array($session->get('clientes.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('clientes.sort') . " " . (($session->get('clientes.order') == 'DESC') ? 'DESC' : 'ASC');
        } else {
            $sql .= " ORDER BY apellido, nombre ASC";
        }

        $consult = $database->parse($sql, '%' . $session->get('clientes.search') . '%', '%' . $session->get('clientes.search') . '%', '%' . $session->get('clientes.search') . '%');
        $results = $database->getRows($consult);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['persona'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['apellido'] . ', ' . @$result['nombre'],
                'align' => 'left',
                'default' => 0
            );

            $mail = "-";
            if (@$result['mail'])
                $mail = @$result['mail'];
            $cell[] = array(
                'value' => $mail,
                'align' => 'left',
                'default' => 0
            );

            $celular = "-";
            if (@$result['celular'])
                $celular = @$result['celular'];
            $cell[] = array(
                'value' => $celular,
                'align' => 'left',
                'default' => 0
            );

            $desctipocliente = "-";
            if (@$result['desctipocliente'])
                $desctipocliente = @$result['desctipocliente'];
            $cell[] = array(
                'value' => $desctipocliente,
                'align' => 'left',
                'default' => 0
            );

            $action = array();

            if ($user->hasPermisos($user->getPERSONA(), 'clientes', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa fa-fw fa-pencil',
                    'text' => $language->get('button_update'),
                    'prop_a' => array('href' => $url->ssl('clientes', 'update', array('persona' => $result['persona'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'clientes', 'B')) {
                $action[] = array(
                    'icon' => 'img/iconos-11.png',
                    'text' => $language->get('button_delete'),
                    'class' => 'fa fa-fw fa-trash-o',
                    'prop_a' => array('href' => "javascript:ActionDelete('" . $url->ssl('clientes', 'delete', array('persona' => $result['persona'])) . "');")
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'clientes', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw fa-search',
                    'text' => $language->get('button_consult'),
                    'prop_a' => array('href' => $url->ssl('clientes', 'consulta', array('persona' => $result['persona'])))
                );
            }

            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('clientes.page'));

        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        if ($session->get('clientes.persona')) {
            $view->set('persona', $session->get('clientes.persona'));
        } else {
            $view->set('persona', '');
        }

        $view->set('heading_title', 'CLIENTES');
        $view->set('heading_title2', 'Filtrar');
        $view->set('heading_description', 'Gesti&oacute;n de Clientes');

        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/NOTICIAS.png');
        $view->set('placeholder_buscar', 'BUSCA POR DNI, APELLIDO O NOMBRE');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('clientes.search'));
        $view->set('sort', $session->get('clientes.sort'));
        $view->set('order', $session->get('clientes.order'));
        $view->set('page', $session->get('clientes.page'));
                
        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('clientes', 'page'));
        $view->set('list', $url->ssl('clientes'));
        if ($user->hasPermisos($user->getPERSONA(), 'clientes', 'A')) {
            $view->set('insert', $url->ssl('clientes', 'insert'));
        }
        $view->set('listaMail', $url->ssl('clientes', 'listaMail'));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_clientes.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">

        if ($request->has('search', 'post')) {
            $session->set('clientes.search', $request->get('search', 'post'));
        }

        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('clientes.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('clientes.order', (($session->get('clientes.sort') == $request->get('sort', 'post')) && ($session->get('clientes.order') == 'ASC')) ? 'DESC' : 'ASC');
        }

        if ($request->has('sort', 'post')) {
            $session->set('clientes.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('clientes', 'index', array('filtra' => '1')));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $language = & $this->locator->get('language');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('entry_persona', 'Documento:');
        $view->set('entry_apellido', 'Apellido:');
        $view->set('entry_nombre', 'Nombre:');
        $view->set('entry_celular', 'Celular:');
        $view->set('entry_mail', 'E-mail:');
        $view->set('entry_remail', 'Confirma E-mail:');
        $view->set('entry_fecha', 'Fecha nacimiento:');
        $view->set('entry_direccion', 'Direccion:');
        $view->set('entry_numero', 'Nro');
        $view->set('entry_piso', 'piso');
        $view->set('entry_departamento', 'Dto:');
        $view->set('entry_localidad', 'Localidad:');

        $view->set('entry_tipocliente', 'Tipo de cliente:');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('heading_title', 'Clientes');
        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/user-8.png');

        $view->set('heading_description', 'Gesti&oacute;n de clientes');

        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        $view->set('tab_general', $language->get('tab_general'));

        if (($request->get('persona')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM personas WHERE persona = '?'";
            $persona_info = $database->getRow($database->parse($consulta, $request->get('persona')));
        }

        if ($request->has('persona', 'post')) {
            $view->set('persona', $request->get('persona', 'post'));
            $persona_id = $request->get('persona', 'post');
        } else {
            $view->set('persona', @$persona_info['persona']);
            $persona_id = @$persona_info['persona'];
        }

        if ($request->has('apellido', 'post')) {
            $view->set('apellido', $request->get('apellido', 'post'));
        } else {
            $view->set('apellido', @$persona_info['apellido']);
        }

        if ($request->has('nombre', 'post')) {
            $view->set('nombre', $request->get('nombre', 'post'));
        } else {
            $view->set('nombre', @$persona_info['nombre']);
        }

        if ($request->has('tipocliente', 'post')) {
            $view->set('tipocliente', $request->get('tipocliente', 'post'));
        } else {
            $view->set('tipocliente', @$persona_info['tipocliente']);
        }
        $view->set('tiposcliente', $database->getRows("SELECT * FROM tiposcliente ORDER BY descripcion ASC"));


        if ($request->has('mail', 'post')) {
            $view->set('mail', $request->get('mail', 'post'));
        } else {
            $view->set('mail', @$persona_info['mail']);
        }

        if ($request->has('remail', 'post')) {
            $view->set('remail', $request->get('remail', 'post'));
        } else {
            $view->set('remail', @$persona_info['mail']);
        }

        if ($request->has('celular', 'post')) {
            $view->set('celular', $request->get('celular', 'post'));
        } else {
            $view->set('celular', @$persona_info['celular']);
        }

        if ($request->has('fechanacimiento', 'post')) {
            $view->set('fechanacimiento', $request->get('fechanacimiento', 'post'));
        } else {
            if ($persona_info['fechanacimiento'] != '')
                $view->set('fechanacimiento', date('d/m/Y', strtotime(@$persona_info['fechanacimiento'])));
            else
                $view->set('fechanacimiento', '');
        }

        if ($request->has('direccion', 'post')) {
            $view->set('direccion', $request->get('direccion', 'post'));
        } else {
            $view->set('direccion', @$persona_info['direccion']);
        }

        if ($request->has('numero', 'post')) {
            $view->set('numero', $request->get('numero', 'post'));
        } else {
            $view->set('numero', @$persona_info['numero']);
        }

        if ($request->has('piso', 'post')) {
            $view->set('piso', $request->get('piso', 'post'));
        } else {
            $view->set('piso', @$persona_info['piso']);
        }

        if ($request->has('departamento', 'post')) {
            $view->set('departamento', $request->get('departamento', 'post'));
        } else {
            $view->set('departamento', @$persona_info['departamento']);
        }

        if ($request->has('auto_localidad', 'post')) {
            $view->set('auto_localidad', $request->get('auto_localidad', 'post'));
        } else {
            $consulta = "SELECT localidad  as id, desclocalidad as value "
                    . "FROM vw_grilla_localidades WHERE localidad = '?'";
            $localidad_info = $database->getRow($database->parse($consulta, @$persona_info['localidad']));
            $view->set('auto_localidad', @$localidad_info['value']);
        }
        if ($request->has('auto_localidad_cliente', 'post')) {
            $view->set('auto_localidad_cliente', $request->get('auto_localidad_cliente', 'post'));
        } else {
            $view->set('auto_localidad_cliente', @$persona_info['localidad']);
        }
        $view->set('script_busca_localidades', $url->rawssl('clientes', 'getLocalidad'));

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');
        $view->set('error_persona', @$this->error['persona']);

        $view->set('error_texto_error', @$this->error['texto_error']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $urlAction = $url->ssl('clientes', $request->get('action'), array('persona' => $request->get('persona')));
        $view->set('action', $urlAction);
        $view->set('accion_form', $request->get('action'));
        $view->set('cancel', $url->ssl('clientes'));

        // </editor-fold>

        return $view->fetch('content/cliente.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $common = & $this->locator->get('common');
        $upload = & $this->locator->get('upload');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        if ((strlen($request->get('persona', 'post')) == 0)) {
            $errores .= 'Debe ingresar el n&uacute;mero de documento. <br>';
            $this->error['persona'] = 'Debe ingresar el n&uacute;mero de documento';
        }

        if (strlen($request->get('persona', 'post')) < 7 || !is_numeric($request->get('persona', 'post'))) {
            $errores .= 'No es un n&uacute;mero de documento v&aacute;lido. <br>';
            $this->error['persona'] = 'No es un n&uacute;mero de documento v&aacute;lido.';
        }

        if (@$this->error['persona'] == '') {
            $sql = "SELECT count(persona) as total FROM personas WHERE persona ='?'";
            $persona = $database->getRow($database->parse($sql, $request->get('persona', 'post')));

            if ($persona['total'] > 0 && $request->get('accion_form', 'post') == 'insert') {
                $errores .= 'Este cliente ya existe en el sistema. <br>';
            }
        }

        if ((strlen($request->get('apellido', 'post')) < 2) || (strlen($request->get('apellido', 'post')) > 100)) {
            $errores .= 'Debe ingresar el apellido. <br>';
        }

        if ((strlen($request->get('nombre', 'post')) < 2) || (strlen($request->get('nombre', 'post')) > 100)) {
            $errores .= 'Debe ingresar el nombre. <br>';
        }

        if ((strlen($request->get('fechanacimiento', 'post')) == 0)) {
            $errores .= 'Debe seleccionar una fecha de nacimiento. <br>';
        }

//                if ((strlen($request->get('mail', 'post')) > 0 )) {
//                    if ($request->get('remail', 'post') != $request->get('mail', 'post')) {
//                                $this->error['remail'] = 'El mail y la confirmacion no coinciden';
//
//                        }
//                }
//                
        if ($request->get('tipocliente', 'post') == '-1') {
            $errores .= 'Debe seleccionar un tipo. <br>';
        }

        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function validateFormUpdate() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $common = & $this->locator->get('common');
        $upload = & $this->locator->get('upload');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">


        if ((strlen($request->get('apellido', 'post')) < 2) || (strlen($request->get('apellido', 'post')) > 100)) {
            $errores .= 'Debe ingresar el apellido. <br>';
        }

        if ((strlen($request->get('nombre', 'post')) < 2) || (strlen($request->get('nombre', 'post')) > 100)) {
            $errores .= 'Debe ingresar el nombre. <br>';
        }

        if ((strlen($request->get('fechanacimiento', 'post')) == 0)) {
            $errores .= 'Debe seleccionar una fecha de nacimiento. <br>';
        }

//                if ((strlen($request->get('mail', 'post')) > 0 )) {
//                    if ($request->get('remail', 'post') != $request->get('mail', 'post')) {
//                                $this->error['remail'] = 'El mail y la confirmacion no coinciden';
//
//                        }
//                }

        if ($request->get('tipocliente', 'post') == '-1') {
            $errores .= 'Debe seleccionar un tipo. <br>';
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $common = & $this->locator->get('common');
        $upload = & $this->locator->get('upload');
        // </editor-fold>

        $template->set('title', 'Clientes');

        if (($request->isPost()) && ($this->validateForm())) {
            $id = strtoupper($request->get('persona', 'post'));

            // <editor-fold defaultstate="collapsed" desc="FECHA">
            $fecha_d = '';
            if ($request->get('fechanacimiento', 'post') != '') {
                $dated = explode('/', $request->get('fechanacimiento', 'post'));
                $fecha_d = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
            } else {
                $fecha_d = NULL;
            }

            // </editor-fold>

            $clave = $common->getEcriptar($id);

            $sql = "INSERT INTO personas SET persona = '?',clave ='?', nombre ='?', apellido ='?', tipocliente='?', fechanacimiento = NULLIF('?',''), tipopersona='CL', direccion = '?', numero = '?',  piso='?', departamento='?', localidad='?',celular ='?',  mail ='?',  usuario = '?'";
            $sql = $database->parse($sql, $id, $clave, strtoupper($request->get('nombre', 'post')), strtoupper($request->get('apellido', 'post')), $request->get('tipocliente', 'post'), $fecha_d, $request->get('direccion', 'post'), $request->get('numero', 'post'), $request->get('piso', 'post'), $request->get('departamento', 'post'), $request->get('auto_localidad_cliente', 'post'), $request->get('celular', 'post'), $request->get('mail', 'post'), $user->getPERSONA());
            $database->query($sql);

            $sql = "INSERT IGNORE INTO personasgrupos SET persona = '?', grupo = 'CL', usuario = '?'";
            $database->query($database->parse($sql, $id, $user->getPERSONA()));

            $cache->delete('clientes');
            $session->set('message', 'Se agreg&oacute; el cliente: ' . $id);

            $response->redirect($url->ssl('clientes'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $upload = & $this->locator->get('upload');
        // </editor-fold>

        $template->set('title', 'Clientes');

        if (($request->isPost()) && ($this->validateFormUpdate())) {

            $id = $request->get('persona', 'post');

            // <editor-fold defaultstate="collapsed" desc="FECHA">
            $fecha_d = '';

            if ($request->get('fechanacimiento', 'post') != '') {
                $dated = explode('/', $request->get('fechanacimiento', 'post'));
                $fecha_d = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
            } else {
                $fecha_d = NULL;
            }

            // </editor-fold>
            //VALIDO QUE SI EL MAIL CAMBIO DEBE VOLVER A VALIDAR
            $consulta = "SELECT mail FROM personas WHERE persona = '?'";
            $persona_mail = $database->getRow($database->parse($consulta, $id));
            $mail_confirmado = 0;
            if ($request->get('mail', 'post') == $persona_mail['mail']) {
                $mail_confirmado = 1;
            }

            $sql = "UPDATE personas SET tipocliente='?',nombre = '?',apellido ='?', fechanacimiento=NULLIF('?',''), direccion = '?', numero = '?',  piso='?', departamento='?', localidad='?', celular = '?',  mail ='?', mail_confirmado = '?', usuario = '?' WHERE persona = '?'";
            $consult = $database->parse($sql, $request->get('tipocliente', 'post'), strtoupper($request->get('nombre', 'post')), strtoupper($request->get('apellido', 'post')), $fecha_d,$request->get('direccion', 'post'), $request->get('numero', 'post'), $request->get('piso', 'post'), $request->get('departamento', 'post'), $request->get('auto_localidad_cliente', 'post'), $request->get('celular', 'post'), $request->get('mail', 'post'), $mail_confirmado, $user->getPERSONA(), $id);
            $database->query($consult);

            $cache->delete('clientes');
            $session->set('message', 'Se actualiz&oacute; el cliente: ' . $id);

            $response->redirect($url->ssl('clientes'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        $template->set('title', 'Clientes');

        if (($request->isPost())) {

            $response->redirect($url->ssl('clientes'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        if (($request->get('persona')) && ($this->validateDelete())) {

            $id = $request->get('persona', 'get');

            //TODO: FALTA PROGRAMAR LA ELIMINACION Y VALIDACION VER SI NO HAY RELACIONES
            $sql = "UPDATE personas SET usuario = '?' WHERE persona = '?'";
            $database->query($database->parse($sql, $user->getPERSONA(), $id));

            $sql = "DELETE FROM personas WHERE persona = '?'";
            $database->query($database->parse($sql, $id));
            // Hago update para auditar el delete
            $sql = "UPDATE personasgrupos SET usuario = '?' WHERE persona = '?'";
            $database->query($database->parse($sql, $user->getPERSONA(), $id));

            $sql = "DELETE FROM personasgrupos WHERE persona = '?'";
            $database->query($database->parse($sql, $id));

            $cache->delete('clientes');

            $session->set('message', 'Se ha eliminado el cliente: ' . $id);

            $response->redirect($url->ssl('clientes'));
        }

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';

        if ($errores != '') {
            $this->error['message'] = $errores;
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function exportar() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $session = & $this->locator->get('session');
        // </editor-fold>

        set_time_limit(0);

        $filtro = 'Filtro:';

        $htmlTable = "<table border=0 width=180> 
			<tr><td colspan=6 align=center style=bold size=14>" . utf8_decode("xxxxxxxxx xxxxxxxxx xxxxxxx") . "</td></tr>
			<tr><td colspan=6 align=center style=bold size=12>SISTEMA DE xxxxxxxxxxx xxxxxxxxx</td></tr>
			<tr><td colspan=6 align=center style=bold size=10>Listado de personas</td></tr>
			<tr><td colspan=6 align=center></td></tr>
			<tr></tr>";

        if (!$session->get('clientes.persona') && !$session->get('clientes.nombre')) {
            $sql = "SELECT p.persona, p.nombre, p.domicilio, l.descripcion,  p.celular, p.mail FROM personas p LEFT JOIN localidades l ON p.localidad = l.localidad";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT p.persona, p.nombre, p.domicilio, l.descripcion,  p.celular, p.mail FROM personas p LEFT JOIN localidades l ON p.localidad = l.localidad WHERE p.persona LIKE '%" . $session->get('clientes.persona') . "%' AND p.nombre LIKE '%" . $session->get('clientes.nombre') . "%'";
            $conca = " AND ";
        }

        if ($session->get('clientes.localidad') != '-1' && $session->get('clientes.localidad') != '') {
            $sql .= $conca . "p.localidad = '" . $session->get('clientes.localidad') . "'";
            $conca = " AND ";
        }

        $sql .= " ORDER BY persona ASC";

        $reporte = $database->getRows($sql);

        foreach ($reporte as $estu) {

            $htmlTable .= "<tr><td colspan=6>______________________________________________________________________________________________________________</td></tr>
                                        <tr>
                                                <td align=left size=9>Documento</td>
                                                <td align=left size=9>" . utf8_decode($estu['persona']) . "</td>
                                        </tr>
                                        <tr>
                                                <td align=left size=9>Nombre</td>	
                                                <td align=left size=9 colspan= 5>" . utf8_decode($estu['nombre']) . "</td>
                                        </tr>
                                        <tr>
                                                <td align=left size=9>Domicilio<td>
                                                <td align=left size=9 colspan=2>" . utf8_decode($estu['domicilio']) . "</td>
                                                <td align=left size=9>Localidad</td>
                                                <td align=left size=9>" . utf8_decode($estu['descripcion']) . "</td>
                                        </tr>
                                        <tr>
                                                
                                                <td align=left size=9>Celular:</td>
                                                <td align=left size=9>" . utf8_decode($estu['celular']) . "</td>
                                                <td align=left size=9></td>
                                                <td align=left size=9></td>
                                        </tr>
                                        <tr>	
                                                <td align=left size=9>Email</td>
                                                <td align=left size=9 colspan=5>" . utf8_decode($estu['mail']) . "</td>
                                        </tr>";
        }

        $htmlTable .= "</table>";

        define('FPDF_FONTPATH', 'font/');
        require('library/pdf/pdftable.inc.php');
        //ob_end_clean();

        $p = new PDFTable();
        $p->AliasNbPages();
        $p->AddPage();
        $p->setfont('times', '', 6);
        $p->htmltable($htmlTable);
        $p->output('', 'I');
    }

    function listaMail() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        if (!$session->get('clientes.search')) {
            $sql = "SELECT persona, nombre, apellido,domicilio,telefono, mail, descripcion FROM vw_list_personas ";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT persona, nombre, apellido,domicilio,telefono, mail, descripcion FROM vw_list_personas WHERE persona LIKE '?' OR nombre LIKE '?' OR apellido LIKE '?' OR descripcion LIKE '?'  ";
            $conca = " AND ";
        }

        $sql .= " ORDER BY apellido, nombre ASC";

        $consult = $database->parse($sql, '%' . $session->get('clientes.search') . '%', '%' . $session->get('clientes.search') . '%', '%' . $session->get('clientes.search') . '%', '%' . $session->get('clientes.search') . '%');
        $results = $database->getRows($consult);

        // </editor-fold>

        $concat = '; ';
        foreach ($results as $integrante) {
            if ($integrante['mail'] != '') {
                $lista_de_mails .= $integrante['mail'] . $concat;
                $concat = '; ';
            }
        }
        if (strlen($lista_de_mails) < 4) {
            $lista_de_mails = "Sin inscriptos.";
        }


        $view = $this->locator->create('template');
        $view->set('lista_de_mails', $lista_de_mails);
        $response->set($view->fetch('content/listademail.tpl'));
    }

    function getLocalidad() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
// </editor-fold>

        $miDescripcion = $request->get('term', 'get');

        $sql = "SELECT localidad  as id, desclocalidad as value "
                . "FROM vw_grilla_localidades "
                . "WHERE descripcion LIKE '?' "
                . "LIMIT 10";
        $consulta = $database->parse($sql, '%' . $miDescripcion . '%');
        $codigo = $database->getRows($consulta);

        $varia = utf8_decode(json_encode($codigo));

        echo $varia;
    }

}

?>