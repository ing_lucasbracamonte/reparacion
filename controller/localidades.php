<?php

class ControllerLocalidades extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'GESTION DE LOCALIDADES');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="LIMPIA FILTRO Y VARIBLES DE SESSION">

        if ($request->get('filtra') == NULL) {

            $session->set('localidades.search', '');
            $session->set('localidades.sort', '');
            $session->set('localidades.order', '');
            $session->set('localidades.page', '');

            $view->set('localidad', '');
            $view->set('search', '');
            $view->set('liga', '');
            $view->set('localidades.search', '');
            $cache->delete('localidades');
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">

        $cols = array();

        $cols[] = array(
            'name' => 'N&uacute;mero',
            'sort' => 'localidad',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Descripci&oacute;n',
            'sort' => 'descripcion',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Provincia',
            'sort' => 'descprovincia',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'localidad',
            'descripcion'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        ini_set('memory_limit', '-1');

        if (!$session->get('localidades.search')) {
            $sql = "SELECT * FROM vw_grilla_localidades ";
        } else {
            $sql = "SELECT * FROM vw_grilla_localidades  WHERE localidad LIKE '?' OR descripcion LIKE '?' ";
        }

        if (in_array($session->get('localidades.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('localidades.sort') . " " . (($session->get('localidades.order') == 'desc') ? 'desc' : 'asc');
        } else {
            $sql .= " ORDER BY descripcion ASC";
        }

//		$consul = $database->splitQuery($database->parse($sql,'%' . $session->get('localidades.search') . '%','%' . $session->get('localidades.search') . '%'), $session->get('localidades.page'), $config->get('config_max_rows'));
//
//                $results = $database->getRows($consul);

        $consulta = $database->parse($sql, '%' . $session->get('localidades.search') . '%', '%' . $session->get('localidades.search') . '%', '%' . $session->get('localidades.search') . '%');
        $results = $database->getRows($consulta);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['localidad'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['descripcion'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['descprovincia'],
                'align' => 'left',
                'default' => 0
            );

            $action = array();


            if ($user->hasPermisos($user->getPERSONA(), 'localidades', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa fa-fw fa-pencil',
                    'text' => $language->get('button_update'),
                    'prop_a' => array('href' => $url->ssl('localidades', 'update', array('localidad' => $result['localidad'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'localidades', 'B')) {
                $action[] = array(
                    'icon' => 'img/iconos-11.png',
                    'text' => $language->get('button_delete'),
                    'class' => 'fa fa-fw fa-trash-o',
                    'prop_a' => array('href' => "javascript:ActionDelete('" . $url->ssl('localidades', 'delete', array('localidad' => $result['localidad'])) . "');")
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'localidades', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw fa-search',
                    'text' => $language->get('button_consult'),
                    'prop_a' => array('href' => $url->ssl('localidades', 'consulta', array('localidad' => $result['localidad'])))
                );
            }

            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        //<editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('localidades.page'));

        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        $view->set('heading_title', 'Localidades');
        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/NOTICIAS.png');
        $view->set('placeholder_buscar', 'BUSCA POR NRO O DESCRIPCION O PROVINCIA');

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        //$view->set('consultaVacia', $url->ssl('consulta','consulta_vacia'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        //$view->set('button_consultavacia', "Vacía");
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('localidades.search'));
        $view->set('sort', $session->get('localidades.sort'));
        $view->set('order', $session->get('localidades.order'));
        $view->set('page', $session->get('localidades.page'));

        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $view->set('titulo_ventana', '::. Exportar');

        $view->set('entry_modificar', "Modificar");
        $view->set('entry_agregar', "Agregar");

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('localidades', 'page'));
        $view->set('list', $url->ssl('localidades'));

        if ($user->hasPermisos($user->getPERSONA(), 'localidades', 'A')) {
            $view->set('insert', $url->ssl('localidades', 'insert'));
        }

        if ($user->hasPermisos($user->getPERSONA(), 'localidades', 'C'))
            $view->set('export', $url->ssl('localidades', 'exportar'));

        $view->set('addLocalidad', $url->ssl('localidades', 'insert'));
        // $view->set('updatePersona', $url->ssl('paciente','update', array('persona' => $result['persona'])));
        $view->set('updateLocalidad', $url->ssl('localidades', 'update'));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_localidades.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">
//		if ($request->has('localidad', 'post')) {
//                    $session->set('localidades.localidad',$request->get('localidad','post'));              
//                }

        if ($request->has('search', 'post')) {
            $session->set('localidades.search', $request->get('search', 'post'));
        }


        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('localidades.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('localidades.order', (($session->get('localidades.sort') == $request->get('sort', 'post')) && ($session->get('localidades.order') == 'asc')) ? 'desc' : 'asc');
        }

        if ($request->has('sort', 'post')) {
            $session->set('localidades.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('localidades', 'index', array('filtra' => '1')));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $template = & $this->locator->get('template');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES">
        $view->set('heading_title', 'DATOS DEL PAIS');
        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/NOTICIAS.png');
        $view->set('entry_provincia', 'Provincia:');
        $view->set('entry_descripcion', 'Descripci&oacute;n:');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('tab_general', $language->get('tab_general'));

        $view->set('localidad', $request->get('localidad'));

        if (($request->get('localidad')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM localidades WHERE localidad = '" . $request->get('localidad') . "'";
            $localidad_info = $database->getRow($consulta);
        }

        if ($request->has('localidad', 'post')) {
            $view->set('localidad', $request->get('localidad', 'post'));
            $localidad_id = @$localidad_info['localidad'];
        } else {
            $view->set('localidad', $request->get('localidad', 'get'));
            $localidad_id = @$localidad_info['localidad'];
        }

        if ($request->has('descripcion', 'post')) {
            $view->set('descripcion', $request->get('descripcion', 'post'));
        } else {
            $view->set('descripcion', @$localidad_info['descripcion']);
        }

        // <editor-fold defaultstate="collapsed" desc="LOCALIDAD PROVINCIA PAIS">

        if ($request->has('auto_provincia', 'post')) {
            $view->set('auto_provincia', $request->get('auto_provincia', 'post'));
        } else {
            $view->set('auto_provincia', @$localidad_info['descprovincia']);
        }
        if ($request->has('auto_provincia_localidad', 'post')) {
            $view->set('auto_provincia_localidad', $request->get('auto_provincia_localidad', 'post'));
        } else {
            $view->set('auto_provincia_localidad', @$localidad_info['provincia']);
        }
        $view->set('script_busca_provincia', $url->rawssl('localidades', 'getProvincia'));
        // </editor-fold>
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        $view->set('error_texto_error', @$this->error['texto_error']);
        //$view->set('error_titulo', @$this->error['titulo']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        // $view->set('action', $url->ssl('localidades', $request->get('action'), array('localidad' => $request->get('localidad'))));
        $view->set('actionBoton', $request->get('action'));
        $view->set('cancel', $url->ssl('localidades'));
        // </editor-fold>

        return $view->fetch('content/localidad.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        if ((strlen($request->get('descripcion', 'post')) == 0)) {
            $errores = 'Debe ingresar la descripción. <br>';
        }

        if ((strlen($request->get('auto_provincia', 'post')) == 0)) {
            $errores .= 'Debe ingresar una provincia. <br>';
        }

        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';
        $consulta_info = $database->getRow("SELECT COUNT(*) as total FROM personas WHERE localidad = '" . $request->get('localidad') . "'");
        if (@$consulta_info['total'] != 0) {
            $errores .= 'No es posible eliminar la localidad, se encuentra asignado al menos una persona.';
        }

        if ($errores != '') {
            $this->error['message'] = $errores;
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Localidades');

        if (($request->isPost()) && ($this->validateForm())) {
            $sql = "INSERT IGNORE INTO localidades SET descripcion = '?', provincia = '?'";
            $consulta = $database->parse($sql, $request->get('descripcion', 'post'), $request->get('auto_provincia_localidad', 'post'));
            $database->query($consulta);

            $id = $database->getLastId();
            $cache->delete('localidades');
            $session->set('message', 'Se agreg&oacute; la localidad: ' . $id);

            $response->redirect($url->ssl('localidades'));
        }
        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos de la localidad');
        if (($request->isPost()) && ($this->validateForm())) {

            $id = $request->get('localidad', 'post');
            $sql = "UPDATE localidades SET descripcion ='?', provincia = '?'  WHERE localidad='?' ";
            $consulta = $database->parse($sql, ucwords($request->get('descripcion', 'post')), $request->get('auto_provincia_localidad', 'post'), $id);
            $database->query($consulta);

            $cache->delete('localidades');

            $session->set('message', 'Se actualiz&oacute; la localidad: ' . $id);

            $response->redirect($url->ssl('localidades'));
        }
        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $url = & $this->locator->get('url');
        // </editor-fold>

        $template->set('title', 'Datos de la localidad');
        if (($request->isPost())) {
            $response->redirect($url->ssl('localidades'));
        }

        $response->set($this->getForm());
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        if (($request->get('localidad')) && ($this->validateDelete())) {

            $id = $request->get('localidad', 'get');

            //TODO: FALTA PROGRAMAR LA ELIMINACION Y VALIDACION VER SI NO HAY RELACIONES
            $sql = "DELETE FROM localidades WHERE localidad = '" . $id . "' ";
            $database->query($sql);

            $cache->delete('localidades');

            $session->set('message', 'Se ha eliminado la localidad: ' . $id);

            $response->redirect($url->ssl('localidades'));
        }

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getProvincia() {
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');

        $miDescripcion = $request->get('term', 'get');

        $sql = "SELECT pais as id, CONCAT(descripcion,' (', descpais, ')') as value "
                . "FROM vw_grilla_provincias "
                . "WHERE descripcion LIKE '?' "
                . "LIMIT 10";
        $consulta = $database->parse($sql, '%' . $miDescripcion . '%');
        $codigo = $database->getRows($consulta);

        $varia = utf8_decode(json_encode($codigo));

        echo $varia;
    }

}

?>