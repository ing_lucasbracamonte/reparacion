<?php

class ControllerVerlogs extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'Ver Logs');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">
        $cols = array();

        $cols[] = array(
            'name' => 'Log',
            'align' => 'left'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();
        $logs = array();
        $path = DIR_LOGS . 'error_log';

        $logs = scandir($path, SCANDIR_SORT_DESCENDING);

        if ($session->get('verlogs.search') && $session->get('verlogs.search') <> -1) {
            $index = $session->get('verlogs.search');
        } elseif (count($logs) > 0) {
            $index = 0;
        } else {
            $index = -1;
        }

        $log = '';
        if ($index >= 0) {
            if ($logs[$index] <> '.' && $logs[$index] <> '..') {
                $log = file_get_contents($path . D_S . $logs[$index]);
            }
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        if ($session->get('verlogs.search')) {
            $view->set('search', $session->get('verlogs.search'));
        } else {
            $view->set('search', '');
        }

        //$view->set('localidades', $database->getRows("SELECT localidad, descripcion FROM localidades ORDER BY descripcion"));

        $view->set('heading_title', 'VER LOGS');
        $view->set('heading_title2', 'Filtrar');
        $view->set('heading_description', 'Ver logs de errores');

        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/NOTICIAS.png');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        $view->set('button_exportar', $language->get('button_exportar'));

        $template->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)
        $template->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('verlogs.search'));

        $view->set('cols', $cols);
        $view->set('log', $log);
        $view->set('logs', $logs);
        $view->set('selectedindex', $index);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('verlogs', 'page'));
        // </editor-fold>


        return $view->fetch('content/list_verlogs.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">

        if ($request->has('search', 'post')) {
            $session->set('verlogs.search', $request->get('search', 'post'));
        }

        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('verlogs.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('verlogs.order', (($session->get('verlogs.sort') == $request->get('sort', 'post')) && ($session->get('verlogs.order') == 'ASC')) ? 'DESC' : 'ASC');
        }

        if ($request->has('sort', 'post')) {
            $session->set('verlogs.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('verlogs'));
    }

}

?>