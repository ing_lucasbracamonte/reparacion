<?php

class ControllerCompras extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'COMPRAS');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
        $request = & $this->locator->get('request');
        // </editor-fold>
         // <editor-fold defaultstate="collapsed" desc="LIMPIA FILTRO Y VARIBLES DE SESSION">

        if ($request->get('filtra') == NULL) {

            $session->set('compras.search', '');
            $session->set('compras.sort', '');
            $session->set('compras.order', '');
            $session->set('compras.page', '');

            $view->set('search', '');
            $view->set('compras.search', '');
                        
            $cache->delete('compras');
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">
        $cols = array();
        $cols[] = array(
            'name' => 'Compra',
            'sort' => 'tipo',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Tipo',
            'sort' => 'tipo',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Nro Doc',
            'sort' => 'numero',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Fecha ',
            'sort' => 'fecha',
            'align' => 'left'
        );
        $cols[] = array(
            'name' => '$ Total',
            'sort' => 'descproveedor',
            'align' => 'left'
        );
        $cols[] = array(
            'name' => 'Proveedor',
            'sort' => 'descproveedor',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Estado',
            'sort' => 'estadocompra',
            'align' => 'left'
        );


        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'tipodocumento',
            'nrodocrelacionado',
            'fecha',
            'descproveedor',
            'estadocompra'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        if (!$session->get('compras.search')) {
            $sql = "SELECT * FROM vw_grilla_compras  ";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT * FROM vw_grilla_compras  WHERE compra LIKE '?' OR nombreproveedor LIKE '?' ";
            $conca = " AND ";
        }

        //filtro por fecha

        if (in_array($session->get('compras.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('compras.sort') . " " . (($session->get('compras.order') == 'DESC') ? 'DESC' : 'ASC');
        } else {
            $sql .= " ORDER BY compra DESC";
        }

        $consult = $database->parse($sql, '%' . $session->get('compras.search') . '%', '%' . $session->get('compras.search') . '%');
        $results = $database->getRows($consult);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();
//tipo, nro, fecha, origen, enti origen, destino, entidad destino, estado
            $cell[] = array(
                'value' => @$result['compra'],
                'align' => 'left',
                'default' => 0
            );
            $cell[] = array(
                'value' => @$result['desctipodocumento'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['nrodocrelacionado'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => date('d/m/Y', strtotime(@$result['fecha'])),
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['totalcompra'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['nombreproveedor'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['estadocompra'],
                'align' => 'left',
                'default' => 0
            );

            $action = array();

            if ($result['estadocompra'] != 'CANCELADA') {
                if ($user->hasPermisos($user->getPERSONA(), 'compras', 'B')) {
                    $action[] = array(
                        'icon' => 'img/iconos-11.png',
                        'text' => 'Cancelar',
                        'class' => 'fa fa-fw  fa-sort-amount-desc',
                        'prop_a' => array('href' => $url->ssl('compras', 'cancelacion', array('compra' => $result['compra'])))
                    );
                }
            }

            if ($user->hasPermisos($user->getPERSONA(), 'compras', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw fa-search',
                    'text' => $language->get('button_consult'),
                    'prop_a' => array('href' => $url->ssl('compras', 'consulta', array('compra' => $result['compra'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'compras', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw  fa-print',
                    'text' => 'Detalle de la compra',
                    'prop_a' => array('href' => $url->ssl('compras', 'exportarPDF', array('compra' => $result['compra'])))
                );
            }

            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('compras.page'));


        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

        if ($session->get('compras.compra')) {
            $view->set('compra', $session->get('compras.compra'));
        } else {
            $view->set('compra', '');
        }

//		if ($session->get('compras.nombre')) {
//                    $view->set('nombre',$session->get('compras.nombre'));
//		} else {
//                    $view->set('nombre', '');
//		}
//		if ($session->get('compras.localidad')) {
//                    $view->set('localidad', $session->get('compras.localidad'));
//		} else {
//                    $view->set('localidad', '-1');
//		}


        $view->set('heading_title', 'COMPRA');
        $view->set('heading_title2', 'Filtrar');
        $view->set('heading_description', 'Gesti&oacute;n de COMPRAS');

        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/NOTICIAS.png');
        $view->set('placeholder_buscar', 'BUSCA POR NRO ');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('compras.search'));
        $view->set('sort', $session->get('compras.sort'));
        $view->set('order', $session->get('compras.order'));
        $view->set('page', $session->get('compras.page'));
        
        $view->set('search', '');
        $view->set('compras.search', '');
        $session->set('compras.search', '');
        
        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('compras', 'page'));
        $view->set('list', $url->ssl('compras'));
        if ($user->hasPermisos($user->getPERSONA(), 'compras', 'A')) {
            $view->set('insert', $url->ssl('compras', 'insert'));
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_compras.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">

        if ($request->has('search', 'post')) {
            $session->set('compras.search', $request->get('search', 'post'));
        }

        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('compras.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('compras.order', (($session->get('compras.sort') == $request->get('sort', 'post')) && ($session->get('compras.order') == 'ASC')) ? 'DESC' : 'ASC');
        }

        if ($request->has('sort', 'post')) {
            $session->set('compras.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('compras', 'index', array('filtra' => '1')));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $common = & $this->locator->get('common');
        $language = & $this->locator->get('language');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES Y VIEW VARIABLES">
        $view->set('heading_title', 'COMPRA');
        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/user-8.png');

        $view->set('heading_description', 'Gesti&oacute;n de COMPRA');

        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        $view->set('tab_general', $language->get('tab_general'));

        if (($request->get('compra')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM vw_grilla_compras WHERE compra = '?'";
            @$objeto_info = $database->getRow($database->parse($consulta, $request->get('compra')));
        }

        if ($request->has('compra', 'post')) {
            $view->set('compra', $request->get('compra', 'post'));
            $objeto_id = $request->get('compra', 'post');
        } else {
            if ($request->has('compra', 'get')) {
                $objeto_id = @$objeto_info['compra'];
            } else {
                $objeto_id = $common->GetaaaaMMddhhmmssmm();
            }
            $view->set('compra', $objeto_id);
        }


        $view->set('entry_fecha', 'Fecha:');
        if ($request->has('fecha', 'post')) {
            $view->set('fecha', $request->get('fecha', 'post'));
        } else {
            if ($request->has('fecha', 'get')) {
                $view->set('fecha', date('d/m/Y', strtotime(@$objeto_info['fecha'])));
            } else {
                $view->set('fecha', date('d/m/Y'));
            }
        }

        $view->set('entry_formapago', 'Forma de Pago:');
        if ($request->has('formapago', 'post')) {
            $view->set('tipodocrelacionado', $request->get('formapago', 'post'));
        } else {
            $view->set('formapago', @$objeto_info['formapago']);
        }
        $view->set('formaspago', $database->getRows("SELECT * FROM formaspago WHERE estado = 1 ORDER BY descripcion ASC"));

        $view->set('entry_nrotarjeta', 'Nro. Tarjeta / Cheque:');
        if ($request->has('nrotarjeta', 'post')) {
            $view->set('nrotarjeta', $request->get('nrotarjeta', 'post'));
        } else {
            $view->set('nrotarjeta', @$objeto_info['nrotarjeta']);
        }

        $view->set('entry_proveedor', 'Proveedor:');
        if ($request->has('proveedor', 'post')) {
            $view->set('proveedor', $request->get('proveedor', 'post'));
        } else {
            $view->set('proveedor', @$objeto_info['proveedor']);
        }
        $view->set('proveedores', $database->getRows("SELECT proveedor, nombre AS descripcion FROM proveedores ORDER BY descripcion ASC"));


        $view->set('entry_tipodocrelacionado', 'Tipo Documento Relacionado:');
        if ($request->has('tipodocrelacionado', 'post')) {
            $view->set('tipodocrelacionado', $request->get('tipodocrelacionado', 'post'));
        } else {
            $view->set('tipodocrelacionado', @$objeto_info['tipodocrelacionado']);
        }
        $view->set('tipodocrelacionados', $database->getRows("SELECT * FROM tipodocrelacionados ORDER BY descripcion ASC"));

        $view->set('entry_nrodocrelacionado', 'Número:');
        if ($request->has('nrodocrelacionado', 'post')) {
            $view->set('nrodocrelacionado', $request->get('nrodocrelacionado', 'post'));
        } else {
            $view->set('nrodocrelacionado', @$objeto_info['nrodocrelacionado']);
        }

        $view->set('entry_observacion', 'Observación:');
        if ($request->has('observacion', 'post')) {
            $view->set('observacion', $request->get('observacion', 'post'));
        } else {
            $view->set('observacion', @$objeto_info['observacion']);
        }

        $view->set('entry_totalcompra', 'Total:');
        if ($request->has('totalcompra', 'post')) {
            $view->set('totalcompra', $request->get('totalcompra', 'post'));
        } else {
            $view->set('totalcompra', @$objeto_info['totalcompra']);
        }

        $view->set('entry_total', 'TOTAL: $');
        if ($request->has('total', 'post')) {
            $view->set('total', $request->get('total', 'post'));
        } else {
            $view->set('total', @$objeto_info['totalcompra']);
        }


        if ($request->has('texttotal', 'post')) {
            $view->set('texttotal', $request->get('texttotal', 'post'));
        } else {
            if (@$objeto_info['totalcompra']) {
                $view->set('texttotal', 'TOTAL $ ' . @$objeto_info['totalcompra']);
            } else {
                $view->set('texttotal', 'TOTAL $ 0.00');
            }
        }

        $view->set('entry_fechabaja', 'Fecha Baja:');
        if ($request->has('fechabaja', 'post')) {
            $view->set('fechabaja', $request->get('fechabaja', 'post'));
        } else {
            if (@$objeto_info['fechabaja'] != '')
                $view->set('fechabaja', date('d/m/Y', strtotime(@$objeto_info['fechabaja'])));
            else
                $view->set('fechabaja', '');
        }

        $view->set('entry_motivobaja', 'Motivo Baja:');
        if ($request->has('motivobaja', 'post')) {
            $view->set('motivobaja', $request->get('motivobaja', 'post'));
        } else {
            $view->set('motivobaja', @$objeto_info['motivobaja']);
        }

        $view->set('entry_repuesto', 'Repuesto:');
        if ($request->has('repuesto_compra', 'post')) {
            $view->set('repuesto_compra', $request->get('repuesto_compra', 'post'));
        } else {
            $view->set('repuesto_compra', '-1');
        }
        $repuestos = $database->getRows("SELECT a.repuesto, a.descripcion, a.nroparte FROM vw_grilla_repuestos a WHERE  a.estadorepuesto = 'DI' ");
        $view->set('repuestoss', $repuestos);

        if ($request->has('preciocompra', 'post')) {
            $view->set('preciocompra', $request->get('preciocompra', 'post'));
        } else {
            $view->set('preciocompra', '0');
        }

        if ($request->has('nroparte', 'post')) {
            $view->set('nroparte', $request->get('nroparte', 'post'));
        } else {
            $view->set('nroparte', '');
        }

        $view->set('entry_cantidad', 'Cantidad:');
        if ($request->has('cantidad', 'post')) {
            $view->set('cantidad', $request->get('cantidad', 'post'));
        } else {
            $view->set('cantidad', '1');
        }

        $view->set('repuestosasignados', $database->getRows("SELECT *, (cantidad * preciocompra) as subtotal FROM vw_grilla_comprarepuestos where compra = '" . $objeto_id . "'"));

        $view->set('entry_fechabaja', 'Fecha Baja:');
        if ($request->has('fechabaja', 'post')) {
            $view->set('fechabaja', $request->get('fechabaja', 'post'));
        } else {
            if (@$objeto_info['fechabaja'] != '')
                $view->set('fechabaja', date('d/m/Y', strtotime(@$objeto_info['fechabaja'])));
            else
                $view->set('fechabaja', '');
        }

        $view->set('entry_motivobaja', 'Motivo Baja:');
        if ($request->has('motivobaja', 'post')) {
            $view->set('motivobaja', $request->get('motivobaja', 'post'));
        } else {
            $view->set('motivobaja', @$objeto_info['motivobaja']);
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        $view->set('error_texto_error', @$this->error['texto_error']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $urlAction = $url->ssl('compras', $request->get('action'), array('compra' => $request->get('compra')));
        $view->set('action', $urlAction);

        $view->set('accion_form', $request->get('action'));
        $view->set('cancel', $url->ssl('compras'));
        // </editor-fold>

        return $view->fetch('content/compra.tpl');
    }

    function getFormCancelacion() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $common = & $this->locator->get('common');
        $language = & $this->locator->get('language');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES Y VIEW VARIABLES">
        $view->set('heading_title', 'COMPRA');
        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/user-8.png');

        $view->set('heading_description', 'CANCELACIÓN DE COMPRA');

        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        $view->set('tab_general', $language->get('tab_general'));

        if (($request->get('compra')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM vw_grilla_compras WHERE compra = '?'";
            @$objeto_info = $database->getRow($database->parse($consulta, $request->get('compra')));
        }

        if ($request->has('compra', 'post')) {
            $view->set('compra', $request->get('compra', 'post'));
            $objeto_id = $request->get('compra', 'post');
        } else {

            $view->set('compra', $request->get('compra', 'get'));
            $objeto_id = $request->get('compra', 'get');
        }


        $view->set('entry_fecha', 'Fecha:');
        if ($request->has('fecha', 'post')) {
            $view->set('fecha', $request->get('fecha', 'post'));
        } else {
            if ($request->has('fecha', 'get')) {
                $view->set('fecha', date('d/m/Y', strtotime(@$objeto_info['fecha'])));
            } else {
                $view->set('fecha', date('d/m/Y'));
            }
        }
        $view->set('entry_formapago', 'Forma de Pago:');
        if ($request->has('formapago', 'post')) {
            $view->set('tipodocrelacionado', $request->get('formapago', 'post'));
        } else {
            $view->set('formapago', @$objeto_info['formapago']);
        }
        $view->set('formaspago', $database->getRows("SELECT * FROM formaspago WHERE estado = 1 ORDER BY descripcion ASC"));

        $view->set('entry_nrotarjeta', 'Nro. Tarjeta / Cheque:');
        if ($request->has('nrotarjeta', 'post')) {
            $view->set('nrotarjeta', $request->get('nrotarjeta', 'post'));
        } else {
            $view->set('nrotarjeta', @$objeto_info['nrotarjeta']);
        }
        $view->set('entry_proveedor', 'Proveedor:');
        if ($request->has('proveedor', 'post')) {
            $view->set('proveedor', $request->get('proveedor', 'post'));
        } else {
            $view->set('proveedor', @$objeto_info['proveedor']);
        }
        $view->set('proveedores', $database->getRows("SELECT proveedor, nombre AS descripcion FROM proveedores ORDER BY descripcion ASC"));


        $view->set('entry_tipodocrelacionado', 'Tipo Documento Relacionado:');
        if ($request->has('tipodocrelacionado', 'post')) {
            $view->set('tipodocrelacionado', $request->get('tipodocrelacionado', 'post'));
        } else {
            $view->set('tipodocrelacionado', @$objeto_info['tipodocrelacionado']);
        }
        $view->set('tipodocrelacionados', $database->getRows("SELECT * FROM tipodocrelacionados ORDER BY descripcion ASC"));

        $view->set('entry_nrodocrelacionado', 'Número:');
        if ($request->has('nrodocrelacionado', 'post')) {
            $view->set('nrodocrelacionado', $request->get('nrodocrelacionado', 'post'));
        } else {
            $view->set('nrodocrelacionado', @$objeto_info['nrodocrelacionado']);
        }

        $view->set('entry_observacion', 'Observación:');
        if ($request->has('observacion', 'post')) {
            $view->set('observacion', $request->get('observacion', 'post'));
        } else {
            $view->set('observacion', @$objeto_info['observacion']);
        }

        $view->set('entry_repuesto', 'Repuesto:');
        if ($request->has('repuesto_compra', 'post')) {
            $view->set('repuesto_compra', $request->get('repuesto_compra', 'post'));
        } else {
            $view->set('repuesto_compra', '-1');
        }
        $repuestos = $database->getRows("SELECT a.repuesto, a.descripcion, a.nroparte FROM vw_grilla_repuestos a WHERE  a.estadorepuesto = 'DI' ");
        $view->set('repuestoss', $repuestos);

        if ($request->has('preciocompra', 'post')) {
            $view->set('preciocompra', $request->get('preciocompra', 'post'));
        } else {
            $view->set('preciocompra', '0');
        }

        if ($request->has('nroparte', 'post')) {
            $view->set('nroparte', $request->get('nroparte', 'post'));
        } else {
            $view->set('nroparte', '');
        }

        $view->set('entry_cantidad', 'Cantidad:');
        if ($request->has('cantidad', 'post')) {
            $view->set('cantidad', $request->get('cantidad', 'post'));
        } else {
            $view->set('cantidad', '1');
        }

        $view->set('repuestosasignados', $database->getRows("SELECT *, (cantidad * preciocompra) as subtotal FROM vw_grilla_comprarepuestos where compra = '" . $objeto_id . "'"));


        $view->set('entry_fechabaja', 'Fecha Baja:');
        if ($request->has('fechabaja', 'post')) {
            $view->set('fechabaja', $request->get('fechabaja', 'post'));
        } else {
            if (@$objeto_info['fechabaja'] != '')
                $view->set('fechabaja', date('d/m/Y', strtotime(@$objeto_info['fechabaja'])));
            else
                $view->set('fechabaja', '');
        }

        $view->set('entry_motivobaja', 'Motivo Baja:');
        if ($request->has('motivobaja', 'post')) {
            $view->set('motivobaja', $request->get('motivobaja', 'post'));
        } else {
            $view->set('motivobaja', @$objeto_info['motivobaja']);
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        $view->set('error_texto_error', @$this->error['texto_error']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $urlAction = $url->ssl('compras', $request->get('action'), array('compra' => $request->get('compra')));
        $view->set('action', $urlAction);

        $view->set('accion_form', $request->get('action'));
        $view->set('cancel', $url->ssl('compras'));
        // </editor-fold>

        return $view->fetch('content/compracancelacion.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $common = & $this->locator->get('common');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">
        if ($request->get('proveedor', 'post') == '-1') {
            $errores = 'Debe seleccionar un proveedor. <br>';
            //$this->error['tab_general'] = '1';
        }

        if ((strlen($request->get('fecha', 'post')) != 10)) {
            $errores = 'Debe ingresar una fecha. <br>';
        }

        if ($request->get('tipodocrelacionado', 'post') == '-1') {
            $errores = 'Debe seleccionar un tipo de documento relacionado. <br>';
            //$this->error['tab_general'] = '1';
        }

        $articulos = $request->get('repuestoscompra', 'post', array());
        if (count($articulos) == 0) {
            $errores .= 'Debe ingresar al menos un repuesto. <br>';
        }

        if ($request->get('formapago', 'post') == '-1') {
            $errores = 'Debe seleccionar una forma de pago. <br>';
            //$this->error['tab_general'] = '1';
        } else {
            if ($request->get('formapago', 'post') == 'EFE') {
                $sql = "SELECT count(*) as total FROM cajas WHERE estadocaja = 'ABIERTA' ";
                $caja = $database->getRow($sql);
                if ($caja['total'] == 0) {
                    $errores .= 'No puede realizarse una compra en efectivo, no existe una caja abierta. <br>';
                }
            }
        }


        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function validateFormCancelacion() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $common = & $this->locator->get('common');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">


        if ((strlen($request->get('fechabaja', 'post')) != 10)) {
            $errores .= 'Debe ingresar una fecha de cancelación. <br>';
        }

        if ((strlen($request->get('motivobaja', 'post')) < 10)) {
            $errores .= 'Debe ingresar un motivo de cancelación. <br>';
        }

        //TENGO QUE VALIDAR QUE SI LA COMPRA FUE CON PAGO EN EFECTIVO TIENE 
        //QUE HABER UNA CAJA HABIERTA PARA PODER DEPOSITAR EL DINERO NUEVAMENTE
        //QUE PASA SI ES UNA NOTA DE CREDITO, SE CUENTA COMO EFECTIVO
        $sql = "SELECT * FROM vw_grilla_compras WHERE compra = '" . $request->get('compra') . "' ";
        $compra = $database->getRow($sql);

        if ($compra['formapago'] == 'EFE') {
            //BUSCO LA CAJA ABIERTA
            $sql = "SELECT count(*) as total FROM cajas WHERE estadocaja = 'ABIERTA' ";
            $caja = $database->getRow($sql);

            if ($caja['total'] == 0) {
                $errores = 'No se puede cancelar la compra, ya que la forma de pago fue en efectivo y no se encuentra una caja abierta para depositar el dinero. <br>';
            }
        }

        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $common = & $this->locator->get('common');
        // </editor-fold>

        $template->set('title', 'COMPRA');

        //SEGUIR EN HACER VALIDA FORM E INSERT
        if (($request->isPost()) && ($this->validateForm())) {

            // <editor-fold defaultstate="collapsed" desc="FECHA">
            $fecha_d = '';
            if ($request->get('fecha', 'post') != '') {
                $dated = explode('/', $request->get('fecha', 'post'));
                $fecha_d = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
                
            } else {
                $fecha_d = NULL;
            }

            // </editor-fold>

            $sql = "INSERT INTO compras SET compra='?', proveedor='?',  fecha='?', tipodocrelacionado='?', nrodocrelacionado='?', formapago='?', nrotarjeta='?', observacion='?' ,estadocompra='ENTRADA',  au_usuario='?',au_accion='A',au_fecha_hora=NOW() ";
            $sql = $database->parse($sql, $request->get('compra', 'post'), $request->get('proveedor', 'post'), $fecha_d, $request->get('tipodocrelacionado', 'post'), $request->get('nrodocrelacionado', 'post'), $request->get('formapago', 'post'), $request->get('nrotarjeta', 'post'), $request->get('observacion', 'post'), $user->getPERSONA());
            $database->query($sql);

            $id = $database->getLastId();

            $repuestos = $request->get('repuestoscompra', 'post', array());

            //REPUESTOS
            //guardar detalles de la compra
            //actualizar ultimo preciocompra en repuesto
            //sumar el stockactual en repuesto
            $totalCompra = 0;
            foreach ($repuestos as $art) {
                $sql = "INSERT INTO compradetalles SET compra='?', repuesto='?', cantidad='?', preciocompra='?',au_usuario='?',au_accion='A',au_fecha_hora=NOW()";
                $database->query($database->parse($sql, $request->get('compra', 'post'), $art['repuesto'], $art['cantidad'], $art['preciocompra'], $user->getPERSONA()));
                $totalCompra += ($art['cantidad'] * $art['preciocompra']);
                //actualizo en el stock general
                $cantidadenbase = $database->getRow("SELECT cantidadstock FROM repuestos where repuesto = '" . $art['repuesto'] . "'");
                $cantidadactualizada = $cantidadenbase['cantidadstock'] + $art['cantidad'];
                $sql = "UPDATE repuestos SET cantidadstock= '?',precioultcompra='?',au_accion='C',au_fecha_hora=NOW() WHERE repuesto='?'";
                $sql = $database->parse($sql, $cantidadactualizada, $art['preciocompra'], $art['repuesto']);
                $database->query($sql);
            }

            //SI ES EFECTIVO REGISTRO EL MOVIMIENTO DE LA CAJA Y ACTUALIZO EL SALDO DE LA CAJA
            // <editor-fold defaultstate="collapsed" desc="CAJA">
            if ($request->get('formapago', 'post') == 'EFE') {
                //BUSCO LA CAJA ABIERTA
                $sql = "SELECT * FROM cajas WHERE estadocaja = 'ABIERTA' ";
                $caja = $database->getRow($sql);

                $sql = "INSERT IGNORE INTO movimientoscaja SET caja='?', fechamovimiento=now(), montomovimiento = '?', tipomovimiento='EGRESO',compra='?',observacion = 'Relacionado con la compra " . $request->get('compra', 'post') . "',  au_usuario='?',au_accion='A',au_fecha_hora=NOW()";
                $consulta = $database->parse($sql, $caja['caja'], $totalCompra, $request->get('compra', 'post'), $user->getPERSONA());
                $database->query($consulta);

                //ACTUALIZO EL SALDO SEGUN EL TIPO DE MOVIMIENTO
                $sql = "UPDATE cajas SET saldo=saldo - '?', au_usuario='?',au_accion='M',au_fecha_hora=NOW() WHERE caja = '?'";
                $consulta = $database->parse($sql, $totalCompra, $user->getPERSONA(), $caja['caja']);
                $database->query($consulta);
            }

            // </editor-fold>

            $cache->delete('compras');
            $session->set('message', 'Se agreg&oacute; la Compra: ' . $id);

            $ur = $url->ssl('compras');
            die($ur);
            //$response->redirect($url->ssl('compras'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        $template->set('title', 'COMPRA');

        if (($request->isPost()) && ($this->validateFormUpdate())) {
            $id = $request->get('persona', 'post');

            // <editor-fold defaultstate="collapsed" desc="FECHA">
            $fecha_d = '';
            if ($request->get('fechanacimiento', 'post') != '') {
                $dated = explode('/', $request->get('fechanacimiento', 'post'));
                $fecha_d = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
                $categoria = date('Y', strtotime($request->get('fechanacimiento', 'post')));
            } else {
                $fecha_d = NULL;
            }

            $fecha_i = '';

            // </editor-fold>

            if ($request->get('vendedorpropio', 'post') == 'vendedorpropio') {
                $vendedorpropio = 1;
            } else {
                $vendedorpropio = 0;
            }

            if ($request->get('comisiona', 'post') == 'comisiona') {
                $comision = 1;
            } else {
                $comision = 0;
            }
            $depende = $id;
            if (strlen($request->get('auto_vendedor', 'post')) != 0) {
                $depende = $request->get('auto_vendedor_principal', 'post');
            }

            $puntosdeventa = $request->get('puntosdevta', 'post', array());

            foreach ($puntosdeventa as $puntovta) {
                $sql = "UPDATE vendedorespuntodeventa SET persona = '?', puntovta = '?', usuario = '?'";
                $database->query($database->parse($sql, $id, $puntovta, $user->getPERSONA()));
            }

            $sql = "UPDATE personas SET nombre ='?', apellido ='?', fechanacimiento = NULLIF('?',''),celular ='?',  mail ='?',tipoiva='?',tipovendedor='?',zona='?',comisiona='?',dependenciapersona='?' ,au_usuario='?',au_accion='M',au_fecha_hora=NOW() WHERE persona = '?' ";
            $sql = $database->parse($sql, strtoupper($request->get('nombre', 'post')), strtoupper($request->get('apellido', 'post')), $fecha_d, $request->get('celular', 'post'), $request->get('mail', 'post'), $request->get('tipoiva', 'post'), $request->get('tipovendedor', 'post'), $request->get('zona', 'post'), $comision, $depende, $user->getPERSONA(), $id);
            $database->query($sql);

            // Hago update para auditar el delete
            $sql = "UPDATE vendedorespuntodeventa SET au_usuario = '?' WHERE persona = '?'";
            $database->query($database->parse($sql, $user->getPERSONA(), $id));

            $sql = "DELETE FROM vendedorespuntodeventa WHERE persona = '?'";
            $database->query($database->parse($sql, $id));

            foreach ($puntosdeventa as $puntovta) {
                $sql = "INSERT IGNORE INTO vendedorespuntodeventa SET persona = '?', puntovta = '?'";
                $database->query($database->parse($sql, $id, $puntovta));
            }

            $cache->delete('compras');
            $session->set('message', 'Se actualiz&oacute; el vendedor: ' . $id);

            $response->redirect($url->ssl('compras'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function cancelacion() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        $template->set('title', 'COMPRA');

        if (($request->isPost()) && $this->validateFormCancelacion()) {
            // <editor-fold defaultstate="collapsed" desc="FECHA">
            $fecha_d = '';
            if ($request->get('fechabaja', 'post') != '') {
                $dated = explode('/', $request->get('fechabaja', 'post'));
                $fecha_d = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
                
            } else {
                $fecha_d = NULL;
            }

            // </editor-fold>

            $sql = "UPDATE compras SET fechabaja='?', motivobaja='?', estadocompra='CANCELADA',  au_usuario='?',au_accion='B',au_fecha_hora=NOW() WHERE compra = '?' ";
            $sql = $database->parse($sql, $fecha_d, $request->get('motivobaja', 'post'), $user->getPERSONA(), $request->get('compra'));
            $database->query($sql);

            $repuestos = $request->get('repuestoscompra', 'post', array());

            //REPUESTOS
            //guardar detalles de la compra
            //actualizar ultimo preciocompra en repuesto
            //sumar el stockactual en repuesto
            foreach ($repuestos as $art) {
                //$sql = "INSERT INTO compradetalles SET compra='?', repuesto='?', cantidad='?', preciocompra='?',au_usuario='?',au_accion='A',au_fecha_hora=NOW()";
                //$database->query($database->parse($sql,$request->get('compra', 'post'), $art['repuesto'], $art['cantidad'], $art['preciocompra'],  $user->getPERSONA()));
                //actualizo en el stock general
                $cantidadenbase = $database->getRow("SELECT cantidadstock FROM repuestos where repuesto = '" . $art['repuesto'] . "'");
                $cantidadactualizada = $cantidadenbase['cantidadstock'] - $art['cantidad'];
                $sql = "UPDATE repuestos SET cantidadstock= '?', au_accion='B',au_fecha_hora=NOW() WHERE repuesto='?'";
                $sql = $database->parse($sql, $cantidadactualizada, $art['repuesto']);
                $database->query($sql);
            }

            //SI ES EFECTIVO REGISTRO EL MOVIMIENTO DE LA CAJA Y ACTUALIZO EL SALDO DE LA CAJA
            // <editor-fold defaultstate="collapsed" desc="CAJA">
            $sql = "SELECT * FROM vw_grilla_compras WHERE compra = '" . $request->get('compra') . "' ";
            $compra = $database->getRow($sql);

            if ($compra['formapago'] == 'EFE') {
                //BUSCO LA CAJA ABIERTA
                $sql = "SELECT * FROM cajas WHERE estadocaja = 'ABIERTA' ";
                $caja = $database->getRow($sql);

                $sql = "INSERT IGNORE INTO movimientoscaja SET caja='?', fechamovimiento=now(), montomovimiento = '?', tipomovimiento='INGRESO',compra='?',observacion = 'Relacionado con la cancelación de la compra " . $request->get('compra') . "',  au_usuario='?',au_accion='A',au_fecha_hora=NOW()";
                $consulta = $database->parse($sql, $caja['caja'], $compra['totalcompra'], $request->get('compra'), $user->getPERSONA());
                $database->query($consulta);

                //ACTUALIZO EL SALDO SEGUN EL TIPO DE MOVIMIENTO
                $sql = "UPDATE cajas SET saldo=saldo + '?', au_usuario='?',au_accion='M',au_fecha_hora=NOW() WHERE caja = '?'";
                $consulta = $database->parse($sql, $compra['totalcompra'], $user->getPERSONA(), $caja['caja']);
                $database->query($consulta);
            }

            // </editor-fold>

            $cache->delete('compras');

            $session->set('message', 'Se ha cancelado la compra');

            $response->redirect($url->ssl('compras'));
        }

        $template->set('content', $this->getFormCancelacion());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        $template->set('title', 'COMPRA');

        if (($request->isPost())) {

            $response->redirect($url->ssl('compras'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        if (($request->get('compra')) && ($this->validateDelete())) {

            $id = $request->get('compra', 'get');
            $mensajeerror = 'Se elimino el COMPRA';

            //TODO: FALTA PROGRAMAR LA ELIMINACION Y VALIDACION VER SI NO HAY RELACIONES
            //
            //BUSCAR TODOS LOS DETALLES DEL COMPRA
            //RECORRERLOS Y VER SI TIENEN VENTA LOS PRODUCTOS DE CODIGO DE BARRAS
            //SI TIENEN VENTAS NO SE PUEDE ELIMINAR
            //SI NO TIENEN ELIMINAR EL COMPRA Y ACTUALIZAR LOS STOCK
            $consulta = $database->getRow("SELECT cantidadstock FROM articulos where articulo = '" . $art['articulo'] . "'");
            $tieneventas = $cantidadenbase['cantidadstock'] + $art['cantidad'];

            if (true) {
                $sql = "UPDATE articulos SET cantidadstock= '?',precioultcompra='?'au_accion='D',au_fecha_hora=NOW() WHERE articulo='?'";
                $sql = $database->parse($sql, $cantidadactualizada, $art['preciocompra'], $art['articulo']);
                $database->query($sql);
            }

            $cache->delete('compras');

            $session->set('message', $mensajeerror);

            $response->redirect($url->ssl('compras'));
        }

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';

        if ($errores != '') {
            $this->error['message'] = $errores;
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function exportar() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $session = & $this->locator->get('session');
        // </editor-fold>

        set_time_limit(0);

        $filtro = 'Filtro:';

        $htmlTable = "<table border=0 width=180> 
			<tr><td colspan=6 align=center style=bold size=14>" . utf8_decode("xxxxxxxxx xxxxxxxxx xxxxxxx") . "</td></tr>
			<tr><td colspan=6 align=center style=bold size=12>SISTEMA DE xxxxxxxxxxx xxxxxxxxx</td></tr>
			<tr><td colspan=6 align=center style=bold size=10>Listado de personas</td></tr>
			<tr><td colspan=6 align=center></td></tr>
			<tr></tr>";

        if (!$session->get('compras.persona') && !$session->get('compras.nombre')) {
            $sql = "SELECT p.persona, p.nombre, p.domicilio, l.descripcion,  p.celular, p.mail FROM personas p LEFT JOIN localidades l ON p.localidad = l.localidad";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT p.persona, p.nombre, p.domicilio, l.descripcion,  p.celular, p.mail FROM personas p LEFT JOIN localidades l ON p.localidad = l.localidad WHERE p.persona LIKE '%" . $session->get('compras.persona') . "%' AND p.nombre LIKE '%" . $session->get('compras.nombre') . "%'";
            $conca = " AND ";
        }

        if ($session->get('compras.localidad') != '-1' && $session->get('compras.localidad') != '') {
            $sql .= $conca . "p.localidad = '" . $session->get('compras.localidad') . "'";
            $conca = " AND ";
        }

        $sql .= " ORDER BY persona ASC";

        $reporte = $database->getRows($sql);

        foreach ($reporte as $estu) {

            $htmlTable .= "<tr><td colspan=6>______________________________________________________________________________________________________________</td></tr>
                                        <tr>
                                                <td align=left size=9>Documento</td>
                                                <td align=left size=9>" . utf8_decode($estu['persona']) . "</td>
                                        </tr>
                                        <tr>
                                                <td align=left size=9>Nombre</td>	
                                                <td align=left size=9 colspan= 5>" . utf8_decode($estu['nombre']) . "</td>
                                        </tr>
                                        <tr>
                                                <td align=left size=9>Domicilio<td>
                                                <td align=left size=9 colspan=2>" . utf8_decode($estu['domicilio']) . "</td>
                                                <td align=left size=9>Localidad</td>
                                                <td align=left size=9>" . utf8_decode($estu['descripcion']) . "</td>
                                        </tr>
                                        <tr>
                                                
                                                <td align=left size=9>Celular:</td>
                                                <td align=left size=9>" . utf8_decode($estu['celular']) . "</td>
                                                <td align=left size=9></td>
                                                <td align=left size=9></td>
                                        </tr>
                                        <tr>	
                                                <td align=left size=9>Email</td>
                                                <td align=left size=9 colspan=5>" . utf8_decode($estu['mail']) . "</td>
                                        </tr>";
        }

        $htmlTable .= "</table>";

        define('FPDF_FONTPATH', 'font/');
        require('library/pdf/pdftable.inc.php');
        //ob_end_clean();

        $p = new PDFTable();
        $p->AliasNbPages();
        $p->AddPage();
        $p->setfont('times', '', 6);
        $p->htmltable($htmlTable);
        $p->output('', 'I');
    }

    function exportarPDF() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $request = & $this->locator->get('request');
        // </editor-fold>

        set_time_limit(0);

        if ($request->has('compra', 'post')) {
            $compra_id = $request->get('compra', 'post');
        } else {
            if ($request->has('compra', 'get')) {
                $compra_id = $request->get('compra', 'get');
            }
        }

        $consulta = "SELECT DISTINCT * FROM vw_grilla_compras WHERE compra = '?'";
        $compra_info = $database->getRow($database->parse($consulta, $compra_id));

        $consult = "SELECT * FROM vw_grilla_comprarepuestos WHERE compra='" . $compra_id . "' ";
        $movimientoscompras = $database->getRows($consult);

        // <editor-fold defaultstate="collapsed" desc="config PDF">
        // Include the main TCPDF library (search for installation path).
        // Include the main TCPDF library (search for installation path).
        define('FPDF_FONTPATH', 'font/');
        require('library/pdf/tcpdf/tcpdf.php');
        require_once('library/pdf/tcpdf/tcpdf_include.php');

        $con = PDF_PAGE_ORIENTATION;
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF8 sin BOM', false);


        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('STC');
        $pdf->SetTitle('');
        $pdf->SetSubject(' ');
        $pdf->SetKeywords(' ');

        // set default header data
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        //LA IMAGEN DEBE ESTAR EN template\default\image
        $pdf->SetHeaderData('cabecera PDF-reporte.jpg', 173, '', '');

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font - conjunto predeterminado fuentemonoespaciada
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetMargins(20, 22, 20);
        //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetHeaderMargin(5);
        $pdf->SetFooterMargin(15);

        // set auto page breaks - establecer saltos de página automático
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // establecemos la medida del interlineado
        //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        //$pdf->SetDefaultMonospacedFont(30);
        // ---------------------------------------------------------
        // set font
        $pdf->SetFont('helvetica', '', 9);


        //$pdf->Image('images/sistema.png', 50, 50, 100, '', '', 'http://www.tcpdf.org', '', false, 300);
// </editor-fold>  
        // add a page
        $pdf->AddPage();

        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">
        $html = '<div style="text-align:center; " ><h1><b>DETALLE DE COMPRA</b></h1></div>';
        $pdf->writeHTML($html, true, false, true, false, '');

        //$html = '<div style="text-align:center; font-size: 13pt;" ><b>CONSTANCIA DE ORDEN DE REPARACIÓN</b><br></div>';
        //$pdf->writeHTML($html, true, false, true, false, '');
//            $html = '<div style="text-align:center; font-size: 11pt;" ><b>CAJA NRO: '.$caja_id.' </b><br></div>';
//            $pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="COMPRA">
        $html = '<div style="text-align:center; font-size: 11pt;" ><b>Datos de la compta nro ' . $compra_id . ' </b><hr /></div>';

        $pdf->writeHTML($html, true, false, true, false, '');
        // <editor-fold defaultstate="collapsed" desc="STYLE">
        $html = <<<EOF
                <!-- EXAMPLE OF CSS STYLE -->
                <style>
                    /*
                    table.first {
                        color: black;
                        font-family: helvetica;
                        font-size: 8pt;
                        background-color: white;
                    }
                    td {
                        border: 0px solid black;
                        background-color: white;
                    }
                    */
                </style>
EOF;
        //$pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>

        $fecha = date('d/m/Y', strtotime($compra_info['fecha']));
        $desctipodocumento = $compra_info['desctipodocumento'];
        $nrodocrelacionado = $compra_info['nrodocrelacionado'];
        $nombreproveedor = $compra_info['nombreproveedor'];
        $descformapago = $compra_info['descformapago'];
        $nrotarjeta = $compra_info['nrotarjeta'];
        $observacion = $compra_info['observacion'];
        $estadocompra = $compra_info['estadocompra'];
        $fechabaja = date('d/m/Y', strtotime($compra_info['fechabaja']));
        $motivobaja = $compra_info['motivobaja'];
        $totalcompra = $compra_info['totalcompra'];

        $html .= <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    <td width="300" style="text-align:left;"><b>FECHA: </b>$fecha</td>
                    <td width="300" style="text-align:left;"><b>PROVEEDOR: </b>$nombreproveedor</td>
                    
               </tr> 
                <tr >
                    <td width="300" style="text-align:left;" ><b>TIPO DOCUMENTO: </b>$desctipodocumento</td>
                    <td width="300" style="text-align:left;"><b>NRO DOC RELACIONADO: </b>$nrodocrelacionado</td>
               </tr> 
                <tr >
                    <td width="300" style="text-align:left;" ><b>FORMA DE PAGO: </b>$descformapago</td>
                    <td width="300" style="text-align:left;"><b>NRO TARJETA/CHEQUE: </b>$nrotarjeta</td>
               </tr>
                <tr >
                    <td width="300" style="text-align:left;" ><b>FECHA CANCELACIÓN: </b>$fechabaja</td>
                    <td width="300" style="text-align:left;"><b>MOTIVO CANCELACIÓN: </b>$motivobaja</td>
               </tr>
                <tr >
                    <td width="300" colspam=2 style="text-align:left;"><b>OBSERVACIÓN: </b>$observacion</td>
               </tr>
            </table>
EOF;

        $pdf->writeHTML($html, true, false, true, false, '');

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="MOVIMIENTOS">
        $html = '<div style="text-align:center; font-size: 11pt; " ><br><b>Movimientos</b><hr /></div>';

        $pdf->writeHTML($html, true, false, true, false, '');
        // <editor-fold defaultstate="collapsed" desc="STYLE">
        $html = <<<EOF
                <!-- EXAMPLE OF CSS STYLE -->
                <style>
                    
                    table.first {
                        color: black;
                        font-family: helvetica;
                        font-size: 8pt;
                        background-color: white;
                    }
                    td {
                        
                        background-color: white;
                    }
                    
                </style>
EOF;
        $pdf->writeHTML($html, true, false, true, false, '');

        // </editor-fold>

        $html .= <<<EOF
                        
            <table style="width:100%;font-size: 8pt;">
            
                <tr >
                    <td style="border: 1px solid black;" width="85"><b>NRO PARTE</b></td>
                    <td style="border: 1px solid black;" width="190"><b>REPUESTO</b></td>
                    <td style="border: 1px solid black;" width="95"><b>TIPO</b></td>
                    <td style="border: 1px solid black;" width="65"><b>CANTIDAD</b></td>
                    <td style="border: 1px solid black;" width="80"><b>PRECIO</b></td>
                    <td style="border: 1px solid black;" width="90"><b>SUB TOTAL</b></td>
               </tr> 
EOF;
        $totalsuma = 0;
        foreach ($movimientoscompras as $result) {

            // <editor-fold defaultstate="collapsed" desc="VARIABLES">
            $nroparte = $result['nroparte'];
            $descripcion = $result['descripcion'];
            $desctiporepuesto = $result['desctiporepuesto'];
            //$compra = ($result['compra'] ? $result['compra'] : '-');
            $cantidad = $result['cantidad'];
            $preciocompra = $result['preciocompra'];
            $subtotal = $result['cantidad'] * $result['preciocompra'];

            // </editor-fold>

            $html .= <<<EOF
                
                <tr style="border: 1px solid black;">
                    <td style="border: 1px solid black;" width="85">$nroparte</td>
                    <td style="border: 1px solid black;" width="190">$descripcion</td>
                    <td style="border: 1px solid black;" width="95">$desctiporepuesto</td>
                    <td style="border: 1px solid black;" width="65">$cantidad</td>
                    <td style="border: 1px solid black;" width="80">$$preciocompra</td>
                    <td style="border: 1px solid black;" width="90">$$subtotal</td>
                </tr>                  
                
EOF;
        }

        $html .= <<<EOF
                                 
            <tr >
                    <td   width="85"><b></b></td>
                    <td  width="190"><b></b></td>
                    <td  width="95"><b></b></td>
                    <td  width="65"><b></b></td>
                    <td style="border: 0px solid black;" width="80"><b>TOTAL</b></td>
                    <td style="border: 0px solid black;" width="90"><b> $totalcompra</b></td>
               </tr>     
</table>
EOF;

        $pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>
        //EJEMPLO
        //// print TEXT
        //$pdf->PrintChapter(1, 'LOREM IPSUM [TEXT]', 'data/chapter_demo_1.txt', false);
        // print HTML
        //$pdf->PrintChapter(2, 'LOREM IPSUM [HTML]', 'data/chapter_demo_2.txt', true);
        // output some RTL HTML content
        //$html = '<div style="text-align:right">ACOMPAÑA<br></div>';
        //$pdf->writeHTML($htmlTable, true, false, true, false, '');
        // reset pointer to the last page
        //$pdf->lastPage();
        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        // Print a table
        // remove default header
        $pdf->setPrintHeader(false);
        //ESTO HACE QUE ANDE EN EL SERVER DE LA DNNA
        ob_end_clean();
        //Close and output PDF document
        //D obliga la descarga
        //I abre en una ventana nueva
        $nombrePDF = 'DetalleCompra_' . $compra_id . '.pdf';
        $pdf->Output($nombrePDF, 'I');

        //$cache->delete('checkins');
        //die('ok');
        //============================================================+
        // END OF FILE
        //============================================================+
    }

    function getProveedor() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>		

        $miTerm = $request->get('term', 'get');

        $sql = "SELECT proveedor AS id, nombre AS value "
                . "FROM proveedores "
                . "WHERE proveedor LIKE '?' OR ciutcuil LIKE '?' ";
        $consulta = $database->parse($sql, '%' . $miTerm . '%', '%' . $miTerm . '%');

        $codigo = $database->getRows($consulta);

        echo json_encode($codigo);
    }

    function getRepuesto() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>		

        $miTerm = $request->get('term', 'get');

        $sql = "SELECT a.repuesto ,  a.descripcion, a.nroparte,  a.precioultcompra "
                . "FROM vw_grilla_repuestos a "
                . "WHERE a.repuesto = '?'";
        $consulta = $database->parse($sql, $miTerm);
        $codigo = $database->getRow($consulta);

        echo json_encode($codigo);
    }

    function getRepuestoNroParte() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>		

        $miTerm = $request->get('term', 'get');

        $sql = "SELECT a.articulo , concat(a.marca,' - ',a.descmodelo) AS descripcion, a.concoddebarra, a.codbarrainterno "
                . "FROM vw_grilla_articulos a "
                . "WHERE (a.descmarca LIKE '?' OR a.descmodelo LIKE '?' OR a.codbarrainterno LIKE '?' ) "
                . "AND a.estadoarticulo = 'DI' ";
        $consulta = $database->parse($sql, '%' . $miTerm . '%', '%' . $miTerm . '%', '%' . $miTerm . '%');
        $codigo = $database->getRows($consulta);

        echo json_encode($codigo);
    }

    function AgregarRepuesto() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $language = & $this->locator->get('language');
        $response = & $this->locator->get('response');
        $request = & $this->locator->get('request');
        $view = $this->locator->create('template');
        // </editor-fold>

        $view->set('button_add', $language->get('button_add'));
        $view->set('button_remove', $language->get('button_remove'));

        $repuesto = $request->get('repuesto');
        $compra = $request->get('compra');
        $cantidad = $request->get('cantidad');
        $nroparte = $request->get('nroparte');
        //$codbarrainterno = $request->get('codbarrainterno');
        $preciocompra = $request->get('preciocompra');

        $sql = "SELECT a.repuesto , descripcion FROM vw_grilla_repuestos a WHERE a.repuesto = '?' ";
        $consulta = $database->parse($sql, $repuesto);
        $art = $database->getRow($consulta);

        $descripcion = $art['descripcion'];

        $view->set('cargo_compra', $compra);
        $view->set('cargo_repuesto', $repuesto);
        $view->set('cargo_descripcion', $descripcion);
        $view->set('cargo_cantidad', $cantidad);
        $view->set('cargo_nroparte', $nroparte);
        $view->set('cargo_subtotal', ($preciocompra * $cantidad));
        $view->set('cargo_preciocompra', $preciocompra);

        //SI EL ENTRENAMIENTO ES 0 ES PORQUE ES UN ALTA
        $view->set('cargo_id', $request->get('cantReg'));

        $view->set('nombre_arreglo', 'repuestoscompra');
        $view->set('nombre_fila', 'repuestocompra_' . $request->get('cantReg'));

        $response->set($view->fetch('content/compra_repuesto.tpl'));
    }

}

?>