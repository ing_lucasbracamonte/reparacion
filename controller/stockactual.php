<?php

class ControllerStockactual extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'Búsqueda de Stock Actual');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function indexPuntoVta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'Stock Actual por Entidad');

        $template->set('content', $this->getListPuntoVta());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function indexProducto() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'Stock Actual por Entidad');

        $template->set('content', $this->getListProducto());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
        $request = & $this->locator->get('request');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VARIABLES">
        $primeravez = '0';
        if ($request->isPost()) {
            $primeravez = '1';
        }

        if ($request->has('primeravez', 'post')) {
            $primeravez = '1';
        } else {
            if ($request->has('primeravez', 'get')) {
                $view->set('primeravez', $request->get('primeravez', 'get'));
                $primeravez = $request->get('primeravez', 'get');
            }
        }

        $agrupadopor = '0';
        if ($request->has('agruparpor', 'post')) {
            $view->set('agruparpor', $request->get('agruparpor', 'post'));
            $agrupadopor = $request->get('agruparpor', 'post');
        } else {
            $view->set('agruparpor', $request->get('agruparpor', 'get'));
            $agrupadopor = $request->get('agruparpor', 'get');
        }

        $articuloID = "";
        if ($request->has('auto_articulo_stock', 'post')) {
            $view->set('auto_articulo_stock', $request->get('auto_articulo_stock', 'post'));
            $articuloID = $request->get('auto_articulo_stock', 'post');
        } else {
            $view->set('auto_articulo_stock', $request->get('auto_articulo_stock', 'get'));
            $articuloID = $request->get('auto_articulo_stock', 'get');
        }

        if ($request->has('auto_articulo', 'post')) {
            $view->set('auto_articulo', $request->get('auto_articulo', 'post'));
        } else {
            if ($articuloID != "" && $articuloID != null) {
                $sql = "SELECT articulo, concat(marca,' - ',descmodelo, ' ' ,if(codbarrainterno!='',concat('Cod: ',codbarrainterno), articulo)) as value "
                        . "FROM vw_grilla_articulos  "
                        . "WHERE articulo = '?'  ";
                $articulo_info = $database->getRow($database->parse($sql, $articuloID));
                $view->set('auto_articulo', $articulo_info['value']);
            } else {
                $view->set('auto_articulo', '');
            }
        }
        $view->set('script_busca_articulo', $url->rawssl('stockactual', 'getArticulos'));

        $tipoproducto = '-1';
        if ($request->has('tipoproducto', 'post')) {
            $view->set('tipoproducto', $request->get('tipoproducto', 'post'));
            $tipoproducto = $request->get('tipoproducto', 'post');
        } else {
            $view->set('tipoproducto', $request->get('tipoproducto', 'get'));
            $tipoproducto = $request->get('tipoproducto', 'get');
        }
        $view->set('tiposproducto', $database->getRows("SELECT * FROM tiposproducto ORDER BY descripcion ASC"));

        $puntovta = '-1';
        if ($request->has('puntovta', 'post')) {
            $view->set('puntovta', $request->get('puntovta', 'post'));
            $puntovta = $request->get('puntovta', 'post');
        } else {
            $view->set('puntovta', $request->get('puntovta', 'get'));
            $puntovta = $request->get('puntovta', 'get');
        }
        //SI ES EXENTO PUEDE FILTRAR POR TODOS LOS PUNTOS DE VENTA
        //SI NO ES EXCENTO SOLO VE LOS PUNTOS DE VENTA A LOS CUALES ESTA ASIGNADO
        if ($user->getExento() == 1) {
            $view->set('puntosdeventa', $database->getRows("SELECT * FROM puntosdeventa ORDER BY descripcion ASC"));
        } else {
            $view->set('puntosdeventa', $database->getRows("SELECT * FROM vendedorespuntodeventa vpv LEFT JOIN puntosdeventa pv ON vpv.puntovta = pv.puntovta WHERE vpv.persona = " . $user->getPERSONA() . " ORDER BY descripcion ASC"));
        }
        //$view->set('puntosdeventa', $database->getRows("SELECT * FROM puntosdeventa ORDER BY descripcion ASC"));
        // </editor-fold>

        if ($primeravez == 1) {

            set_time_limit(0);
            // <editor-fold defaultstate="collapsed" desc="AGRUPAR POR ENTIDAD">

            if ($agrupadopor == 0) {

                // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">

                $cols = array();

                $cols[] = array(
                    'name' => 'PUNTO DE VENTA',
                    'sort' => 'descpuntovta',
                    'align' => 'left'
                );

                $cols[] = array(
                    'name' => 'CANTIDAD',
                    'sort' => 'cantidad',
                    'align' => 'left'
                );
//                $cols[] = array(
//			'name'  => 'PRECIO VTA',
//			'sort'  => 'precio',
//			'align' => 'left'
//		);

                $cols[] = array(
                    'name' => 'Acciones',
                    'align' => 'center'
                );

                $sort = array(
                    'descpuntovta'
                );
                // </editor-fold>
                // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">

                $sql = "SELECT *, SUM(cantidad) AS total FROM vw_grilla_stockactual  ";
                $conca = " WHERE ";

                if ($tipoproducto != '-1' && $tipoproducto != '') {
                    $sql .= $conca . " tipoproducto = '" . $tipoproducto . "'  ";
                    $conca = " AND ";
                }

                if ($puntovta != '-1' && $puntovta != '') {
                    $sql .= $conca . " puntovta = '" . $puntovta . "'  ";
                    $conca = " AND ";
                } else {
                    //SI ES EXENTO PUEDE FILTRAR POR TODOS LOS PUNTOS DE VENTA
                    //SI NO ES EXCENTO SOLO VE LOS PUNTOS DE VENTA A LOS CUALES ESTA ASIGNADO
                    if ($user->getExento() == 1) {

                        $view->set('puntosdeventa', $database->getRows("SELECT * FROM puntosdeventa ORDER BY descripcion ASC"));
                    } else {
                        $puntosdeventaasignado = $database->getRows("SELECT * FROM vendedorespuntodeventa vpv LEFT JOIN puntosdeventa pv ON vpv.puntovta = pv.puntovta WHERE vpv.persona = " . $user->getPERSONA() . " ORDER BY descripcion ASC");
                        foreach ($puntosdeventaasignado as $pv) {
                            $sql .= $conca . " puntovta = '" . $pv['puntovta'] . "'  ";
                            $conca = " OR ";
                        }
                    }
                }

                if ($articuloID != null && $articuloID != '') {
                    $sql .= $conca . " articulo = '" . $articuloID . "'  ";
                    $conca = " AND ";
                }

                $sql .= " GROUP BY puntovta ";

                if (in_array($session->get('stockactual.sort'), $sort)) {
                    $sql .= " ORDER BY " . $session->get('stockactual.sort') . " " . (($session->get('stockactual.order') == 'desc') ? 'desc' : 'asc');
                } else {
                    $sql .= " ORDER BY descpuntovta ASC";
                }

                $results = $database->getRows($sql);

                // </editor-fold>
                // <editor-fold defaultstate="collapsed" desc="GRILLA">
                $rows = array();

                foreach ($results as $result) {
                    $cell = array();

                    $cell[] = array(
                        'value' => @$result['descpuntovta'],
                        'align' => 'left',
                        'default' => 0
                    );

                    $cell[] = array(
                        'value' => @$result['total'],
                        'align' => 'left',
                        'default' => 0
                    );

                    $action = array();

                    if ($user->hasPermisos($user->getPERSONA(), 'stockactual', 'C')) {
                        $action[] = array(
                            'icon' => 'img/iconos-17.png',
                            'class' => 'fa fa-fw fa-search',
                            'text' => $language->get('button_consult'),
                            'prop_a' => array('href' => $url->ssl('stockactual', 'indexPuntoVta', array('agrupadopor' => $agrupadopor, 'puntovta' => $puntovta, 'puntovtaid' => $result['puntovta'], 'tipoproducto' => $tipoproducto, 'auto_articulo_stock' => $articuloID, 'volvera' => 'getList')))
                        );
                    }

                    $cell[] = array(
                        'action' => $action,
                        'align' => 'center'
                    );

                    $rows[] = array('cell' => $cell);
                }
                // </editor-fold>
            }

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="AGRUPAR POR PRODUCTO">

            if ($agrupadopor == 1) {

                // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">

                $cols = array();

                $cols[] = array(
                    'name' => 'TIPO',
                    'sort' => 'desctipoproducto',
                    'align' => 'left'
                );

                $cols[] = array(
                    'name' => 'MARCA',
                    'sort' => 'descmarca',
                    'align' => 'left'
                );

                $cols[] = array(
                    'name' => 'MODELO',
                    'sort' => 'descmodelo',
                    'align' => 'left'
                );

                $cols[] = array(
                    'name' => 'GAMA',
                    'sort' => 'descgama',
                    'align' => 'left'
                );

                $cols[] = array(
                    'name' => 'COD INTERNO',
                    'sort' => 'codbarrainterno',
                    'align' => 'left'
                );

                $cols[] = array(
                    'name' => 'CANTIDAD',
                    'sort' => 'cantidad',
                    'align' => 'left'
                );

                $cols[] = array(
                    'name' => 'Acciones',
                    'align' => 'center'
                );

                $sort = array(
                    'desctipoproducto',
                    'desctmarca',
                    'descmodelo',
                    'descgama',
                    'codbarrainterno',
                    'cantidad'
                );
                // </editor-fold>
                // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
//        if (strlen($search) == 0) {
//            $sql = "SELECT * FROM vw_grilla_stockactual  ";
//            $conca = " WHERE ";
//        } else {
//            $sql = "SELECT * FROM vw_grilla_stockactual WHERE articulo LIKE '?' OR codbarrainterno LIKE '?' OR desctipoproducto LIKE '?' OR descmodelo LIKE '?' OR descmarca LIKE '?' ";
//            $conca = " AND ";
//        }

                $sql = "SELECT *, SUM(cantidad) AS total FROM vw_grilla_stockactual  ";
                $conca = " WHERE ";

                if ($tipoproducto != '-1' && $tipoproducto != '') {
                    $sql .= $conca . " tipoproducto = '" . $tipoproducto . "'  ";
                    $conca = " AND ";
                }

                if ($puntovta != '-1' && $puntovta != '') {
                    $sql .= $conca . " puntovta = '" . $puntovta . "'  ";
                    $conca = " AND ";
                } else {
                    //SI ES EXENTO PUEDE FILTRAR POR TODOS LOS PUNTOS DE VENTA
                    //SI NO ES EXCENTO SOLO VE LOS PUNTOS DE VENTA A LOS CUALES ESTA ASIGNADO
                    if ($user->getExento() == 1) {

                        $view->set('puntosdeventa', $database->getRows("SELECT * FROM puntosdeventa ORDER BY descripcion ASC"));
                    } else {
                        $puntosdeventaasignado = $database->getRows("SELECT * FROM vendedorespuntodeventa vpv LEFT JOIN puntosdeventa pv ON vpv.puntovta = pv.puntovta WHERE vpv.persona = " . $user->getPERSONA() . " ORDER BY descripcion ASC");
                        foreach ($puntosdeventaasignado as $pv) {
                            $sql .= $conca . " puntovta = '" . $pv['puntovta'] . "'  ";
                            $conca = " OR ";
                        }
                    }
                }
                //$conca = " AND ";

                if ($articuloID != null && $articuloID != '') {
                    $sql .= $conca . " articulo = '" . $articuloID . "'  ";
                    $conca = " AND ";
                }

                $sql .= " GROUP BY articulo ASC";

                if (in_array($session->get('stockactual.sort'), $sort)) {
                    $sql .= " ORDER BY " . $session->get('stockactual.sort') . " " . (($session->get('stockactual.order') == 'desc') ? 'desc' : 'asc');
                } else {
                    $sql .= " ORDER BY desctipoproducto, descmarca, descmodelo ASC";
                }

                //$consulta = $database->parse($sql, '%' . $search . '%', '%' . $search . '%', '%' . $search . '%', '%' . $search . '%', '%' . $search . '%', '%' . $search . '%');
                $results = $database->getRows($sql);


                // </editor-fold>
                // <editor-fold defaultstate="collapsed" desc="GRILLA">
                $rows = array();

                foreach ($results as $result) {
                    $cell = array();

                    $cell[] = array(
                        'value' => @$result['desctipoproducto'],
                        'align' => 'left',
                        'default' => 0
                    );

                    $descmarca = '-';
                    if (@$result['descmarca']) {
                        $descmarca = $result['descmarca'];
                    }
                    $cell[] = array(
                        'value' => $descmarca,
                        'align' => 'left',
                        'default' => 0
                    );

                    $cell[] = array(
                        'value' => @$result['descmodelo'],
                        'align' => 'left',
                        'default' => 0
                    );

                    $desc = "-";
                    if (@$result['desctipogama']) {
                        $desc = $result['desctipogama'];
                    }
                    $cell[] = array(
                        'value' => $desc,
                        'align' => 'left',
                        'default' => 0
                    );

                    $cell[] = array(
                        'value' => @$result['codbarrainterno'],
                        'align' => 'left',
                        'default' => 0
                    );

                    $cell[] = array(
                        'value' => @$result['total'],
                        'align' => 'left',
                        'default' => 0
                    );

                    $action = array();

                    if ($user->hasPermisos($user->getPERSONA(), 'stockactual', 'C')) {
                        $action[] = array(
                            'icon' => 'img/iconos-17.png',
                            'class' => 'fa fa-fw fa-search',
                            'text' => $language->get('button_consult'),
                            'prop_a' => array('href' => $url->ssl('stockactual', 'indexProducto', array('agrupadopor' => $agrupadopor, 'puntovta' => $puntovta, 'articuloid' => $result['articulo'], 'tipoproducto' => $tipoproducto, 'auto_articulo_stock' => $articuloID, 'volvera' => 'getList')))
                        );
                    }

                    $cell[] = array(
                        'action' => $action,
                        'align' => 'center'
                    );

                    $rows[] = array('cell' => $cell);
                }
                // </editor-fold>
            }

            // </editor-fold>
        }
        //<editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">

        $view->set('heading_title', 'STOCK ACTUAL');
        $view->set('placeholder_buscar', 'BUSCA POR ID O MARCA O MODELO O TIPO');

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        //$view->set('consultaVacia', $url->ssl('consulta','consulta_vacia'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        //$view->set('button_consultavacia', "Vacía");
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        //$view->set('search', $session->get('stockactual.search'));
        //$view->set('agruparpor', $session->get('stockactual.agruparpor'));
        $view->set('sort', $session->get('stockactual.sort'));
        $view->set('order', $session->get('stockactual.order'));
        $view->set('page', $session->get('stockactual.page'));

        //$view->set('stockactual.tipoproducto', '-1');
        //$view->set('stockactual.puntovta', '-1');
        //$view->set('tiposproducto', $database->getRows("SELECT * FROM tiposproducto ORDER BY descripcion ASC"));
        //$view->set('puntosdeventa', $database->getRows("SELECT * FROM puntosdeventa ORDER BY descripcion ASC"));

        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $view->set('titulo_ventana', '::. Exportar');

        $view->set('entry_modificar', "Modificar");
        $view->set('entry_agregar', "Agregar");

        $view->set('primeravez', $primeravez);

        //$view->set('modelo', '');
        //$view->set('search', '');
        //$view->set('agruparpor', '1');
        //$view->set('stockactual.search', '');
        //$view->set('stockactual.agruparpor', '1');
        $cache->delete('stockactual');

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('stockactual'));
        //$view->set('list', $url->ssl('stockactual'));


        if ($user->hasPermisos($user->getPERSONA(), 'stockactual', 'C')) {
            if ($agrupadopor == 0) {
                $view->set('exportXLS', $url->ssl('stockactual', 'exportxlsdataXLSPuntoVta', array('agrupadopor' => $agrupadopor, 'puntovta' => $puntovta, 'puntovtaid' => $result['puntovta'], 'tipoproducto' => $tipoproducto, 'auto_articulo_stock' => $articuloID)));
                $view->set('exportCSV', $url->ssl('stockactual', 'exportxlsdataCSVPuntoVta', array('agrupadopor' => $agrupadopor, 'puntovta' => $puntovta, 'puntovtaid' => $result['puntovta'], 'tipoproducto' => $tipoproducto, 'auto_articulo_stock' => $articuloID)));
            }
            if ($agrupadopor == 1) {
                $view->set('exportXLS', $url->ssl('stockactual', 'exportxlsdataXLSProducto', array('agrupadopor' => $agrupadopor, 'puntovta' => $puntovta, 'articuloid' => $result['articulo'], 'tipoproducto' => $tipoproducto, 'auto_articulo_stock' => $articuloID)));
                $view->set('exportCSV', $url->ssl('stockactual', 'exportxlsdataCSVProducto', array('agrupadopor' => $agrupadopor, 'puntovta' => $puntovta, 'articuloid' => $result['articulo'], 'tipoproducto' => $tipoproducto, 'auto_articulo_stock' => $articuloID)));
            }
        }

        // </editor-fold>

        return $view->fetch('content/list_stockactual.tpl');
    }

    function getListPuntoVta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
        $request = & $this->locator->get('request');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VARIABLES">
        $primeravez = '0';
        if ($request->isPost()) {
            $primeravez = '1';
        }

        if ($request->has('primeravez', 'post')) {
            $primeravez = '1';
        } else {
            if ($request->has('primeravez', 'get')) {
                $view->set('primeravez', $request->get('primeravez', 'get'));
                $primeravez = $request->get('primeravez', 'get');
            }
        }

        $agrupadopor = '0';
        if ($request->has('agruparpor', 'post')) {
            $view->set('agruparpor', $request->get('agruparpor', 'post'));
            $agrupadopor = $request->get('agruparpor', 'post');
        } else {
            $view->set('agruparpor', $request->get('agruparpor', 'get'));
            $agrupadopor = $request->get('agruparpor', 'get');
        }

        $articuloID = "";
        if ($request->has('auto_articulo_stock', 'post')) {
            $view->set('auto_articulo_stock', $request->get('auto_articulo_stock', 'post'));
            $articuloID = $request->get('auto_articulo_stock', 'post');
        } else {
            $view->set('auto_articulo_stock', $request->get('auto_articulo_stock', 'get'));
            $articuloID = $request->get('auto_articulo_stock', 'get');
        }

        if ($request->has('auto_articulo', 'post')) {
            $view->set('auto_articulo', $request->get('auto_articulo', 'post'));
        } else {
            if ($articuloID != "" && $articuloID != null) {
                $sql = "SELECT articulo, concat(marca,' - ',descmodelo, ' ' ,if(codbarrainterno!='',concat('Cod: ',codbarrainterno), articulo)) as value "
                        . "FROM vw_grilla_articulos  "
                        . "WHERE articulo = '?'  ";
                $articulo_info = $database->getRow($database->parse($sql, $articuloID));
                $view->set('auto_articulo', $articulo_info['value']);
            } else {
                $view->set('auto_articulo', '');
            }
        }
        $view->set('script_busca_articulo', $url->rawssl('stockactual', 'getArticulos'));

        $tipoproducto = '-1';
        if ($request->has('tipoproducto', 'post')) {
            $view->set('tipoproducto', $request->get('tipoproducto', 'post'));
            $tipoproducto = $request->get('tipoproducto', 'post');
        } else {
            $view->set('tipoproducto', $request->get('tipoproducto', 'get'));
            $tipoproducto = $request->get('tipoproducto', 'get');
        }
        $view->set('tiposproducto', $database->getRows("SELECT * FROM tiposproducto ORDER BY descripcion ASC"));

        $puntovta = '-1';
        if ($request->has('puntovta', 'post')) {
            $view->set('puntovta', $request->get('puntovta', 'post'));
            $puntovta = $request->get('puntovta', 'post');
        } else {
            $view->set('puntovta', $request->get('puntovta', 'get'));
            $puntovta = $request->get('puntovta', 'get');
        }
        $view->set('puntosdeventa', $database->getRows("SELECT * FROM puntosdeventa ORDER BY descripcion ASC"));

        $puntovtaid = '';
        if ($request->has('puntovtaid', 'post')) {
            $view->set('puntovtaid', $request->get('puntovtaid', 'post'));
            $puntovtaid = $request->get('puntovtaid', 'post');
        } else {
            $view->set('puntovtaid', $request->get('puntovtaid', 'get'));
            $puntovtaid = $request->get('puntovtaid', 'get');
        }
        $puntovta_info = $database->getRow("SELECT * FROM puntosdeventa WHERE puntovta = '" . $puntovtaid . "'");


        $volvera = 'getList';
        if ($request->has('volvera', 'post')) {
            $view->set('volvera', $request->get('volvera', 'post'));
            $volvera = $request->get('volvera', 'post');
        } else {
            $view->set('volvera', $request->get('volvera', 'get'));
            $volvera = $request->get('volvera', 'get');
        }

        $search = '';
        if ($request->has('search', 'post')) {
            $view->set('search', $request->get('search', 'post'));
            $search = $request->get('search', 'post');
        } else {
            $view->set('search', $request->get('search', 'get'));
            $search = $request->get('search', 'get');
        }
        // </editor-fold>


        set_time_limit(0);

        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">

        $cols = array();

        $cols[] = array(
            'name' => 'TIPO',
            'sort' => 'desctipoproducto',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'MARCA',
            'sort' => 'descmarca',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'MODELO',
            'sort' => 'descmodelo',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'GAMA',
            'sort' => 'descgama',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'COD INTERNO',
            'sort' => 'codbarrainterno',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'CANTIDAD',
            'sort' => 'cantidad',
            'align' => 'left'
        );


        $sort = array(
            'desctipoproducto',
            'desctmarca',
            'descmodelo',
            'descgama',
            'codbarrainterno',
            'cantidad'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">

        if (strlen($search) == 0) {
            $sql = "SELECT * FROM vw_grilla_stockactual  ";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT * FROM vw_grilla_stockactual WHERE articulo LIKE '?' OR codbarrainterno LIKE '?' OR desctipoproducto LIKE '?' OR descmodelo LIKE '?' OR descmarca LIKE '?' ";
            $conca = " AND ";
        }

        //$sql = "SELECT * FROM vw_grilla_stockactual  ";
        //$conca = " WHERE ";

        if ($tipoproducto != '-1' && $tipoproducto != '') {
            $sql .= $conca . " tipoproducto = '" . $tipoproducto . "'  ";
            $conca = " AND ";
        }

        if ($puntovtaid != '-1' && $puntovtaid != '') {
            $sql .= $conca . " puntovta = '" . $puntovtaid . "'  ";
            $conca = " AND ";
        } else {
            //SI ES EXENTO PUEDE FILTRAR POR TODOS LOS PUNTOS DE VENTA
            //SI NO ES EXCENTO SOLO VE LOS PUNTOS DE VENTA A LOS CUALES ESTA ASIGNADO
            if ($user->getExento() == 1) {

                $view->set('puntosdeventa', $database->getRows("SELECT * FROM puntosdeventa ORDER BY descripcion ASC"));
            } else {
                $puntosdeventaasignado = $database->getRows("SELECT * FROM vendedorespuntodeventa vpv LEFT JOIN puntosdeventa pv ON vpv.puntovta = pv.puntovta WHERE vpv.persona = " . $user->getPERSONA() . " ORDER BY descripcion ASC");
                foreach ($puntosdeventaasignado as $pv) {
                    $sql .= $conca . " puntovta = '" . $pv['puntovta'] . "'  ";
                    $conca = " OR ";
                }
            }
        }
        $conca = " AND ";
            if ($articuloID != null && $articuloID != '') {
                $sql .= $conca . " articulo = '" . $articuloID . "'  ";
            }
        //$sql .= " GROUP BY puntovta ASC";

        if (in_array($session->get('stockactual.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('stockactual.sort') . " " . (($session->get('stockactual.order') == 'desc') ? 'desc' : 'asc');
        } else {
            $sql .= " ORDER BY descpuntovta ASC";
        }

        $consulta = $database->parse($sql, '%' . $search . '%', '%' . $search . '%', '%' . $search . '%', '%' . $search . '%', '%' . $search . '%', '%' . $search . '%');
        $results = $database->getRows($consulta);


        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['desctipoproducto'],
                'align' => 'left',
                'default' => 0
            );

            $descmarca = '-';
            if (@$result['descmarca']) {
                $descmarca = $result['descmarca'];
            }
            $cell[] = array(
                'value' => $descmarca,
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['descmodelo'],
                'align' => 'left',
                'default' => 0
            );

            $desc = "-";
            if (@$result['desctipogama']) {
                $desc = $result['desctipogama'];
            }
            $cell[] = array(
                'value' => $desc,
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['codbarrainterno'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['cantidad'],
                'align' => 'left',
                'default' => 0
            );


            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        //<editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">


        $view->set('heading_title', 'STOCK DEL PUNTO DE VENTA: ' . $puntovta_info['descripcion']);
        $view->set('placeholder_buscar', 'BUSCA POR ID O MARCA O MODELO O TIPO');

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        //$view->set('consultaVacia', $url->ssl('consulta','consulta_vacia'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        //$view->set('button_consultavacia', "Vacía");
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        //$view->set('search', $session->get('stockactual.search'));
        //$view->set('agruparpor', $session->get('stockactual.agruparpor'));
        $view->set('sort', $session->get('stockactual.sort'));
        $view->set('order', $session->get('stockactual.order'));
        $view->set('page', $session->get('stockactual.page'));

        //$view->set('stockactual.tipoproducto', '-1');
        //$view->set('stockactual.puntovta', '-1');
        //$view->set('tiposproducto', $database->getRows("SELECT * FROM tiposproducto ORDER BY descripcion ASC"));
        //$view->set('puntosdeventa', $database->getRows("SELECT * FROM puntosdeventa ORDER BY descripcion ASC"));

        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $view->set('titulo_ventana', '::. Exportar');

        $view->set('entry_modificar', "Modificar");
        $view->set('entry_agregar', "Agregar");

        $view->set('primeravez', $primeravez);

        if ($request->has('tiporeporte', 'post')) {
            $view->set('tiporeporte', $request->get('tiporeporte', 'post'));
        } else {
            $view->set('tiporeporte', 'CSV');
        }

        //$view->set('modelo', '');
        //$view->set('search', '');
        //$view->set('agruparpor', '1');
        //$view->set('stockactual.search', '');
        //$view->set('stockactual.agruparpor', '1');
        $cache->delete('stockactual');

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('stockactual'));
        $view->set('volver', $url->ssl('stockactual', 'index', array('agrupadopor' => $agrupadopor, 'puntovta' => $puntovta, 'tipoproducto' => $tipoproducto, 'auto_articulo_stock' => $articuloID, 'volvera' => 'getList', 'primeravez' => '1')));


        if ($user->hasPermisos($user->getPERSONA(), 'stockactual', 'C')) {
            $view->set('exportXLS', $url->ssl('stockactual', 'exportxlsdataXLSPuntoVtaDetalle', array('agrupadopor' => $agrupadopor, 'puntovta' => $puntovta, 'puntovtaid' => $result['puntovta'], 'tipoproducto' => $tipoproducto, 'auto_articulo_stock' => $articuloID)));
            $view->set('exportCSV', $url->ssl('stockactual', 'exportxlsdataCSVPuntoVtaDetalle', array('agrupadopor' => $agrupadopor, 'puntovta' => $puntovta, 'puntovtaid' => $result['puntovta'], 'tipoproducto' => $tipoproducto, 'auto_articulo_stock' => $articuloID)));
        }

        //$view->set('addPais', $url->ssl('stockactual','insert'));
        // $view->set('updatePersona', $url->ssl('paciente','update', array('persona' => $result['persona'])));
        //$view->set('updatePais', $url->ssl('stockactual','update'));
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">
        // </editor-fold>

        return $view->fetch('content/list_stockactualPuntoVta.tpl');
    }

    function getListProducto() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
        $request = & $this->locator->get('request');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VARIABLES">
        $primeravez = '0';
        if ($request->isPost()) {
            $primeravez = '1';
        }

        if ($request->has('primeravez', 'post')) {
            $primeravez = '1';
        } else {
            if ($request->has('primeravez', 'get')) {
                $view->set('primeravez', $request->get('primeravez', 'get'));
                $primeravez = $request->get('primeravez', 'get');
            }
        }

        $agrupadopor = '0';
        if ($request->has('agruparpor', 'post')) {
            $view->set('agruparpor', $request->get('agruparpor', 'post'));
            $agrupadopor = $request->get('agruparpor', 'post');
        } else {
            $view->set('agruparpor', $request->get('agruparpor', 'get'));
            $agrupadopor = $request->get('agruparpor', 'get');
        }

        $articuloID = "";
        if ($request->has('auto_articulo_stock', 'post')) {
            $view->set('auto_articulo_stock', $request->get('auto_articulo_stock', 'post'));
            $articuloID = $request->get('auto_articulo_stock', 'post');
        } else {
            $view->set('auto_articulo_stock', $request->get('auto_articulo_stock', 'get'));
            $articuloID = $request->get('auto_articulo_stock', 'get');
        }

        if ($request->has('auto_articulo', 'post')) {
            $view->set('auto_articulo', $request->get('auto_articulo', 'post'));
        } else {
            if ($articuloID != "" && $articuloID != null) {
                $sql = "select articulo, concat(marca,' - ',descmodelo, ' ' ,if(codbarrainterno!='',concat('Cod: ',codbarrainterno), articulo)) as value "
                        . "FROM vw_grilla_articulos  "
                        . "WHERE articulo = '?'  ";
                $articulo_info = $database->getRow($database->parse($sql, $articuloID));
                $view->set('auto_articulo', $articulo_info['value']);
            } else {
                $view->set('auto_articulo', '');
            }
        }
        $view->set('script_busca_articulo', $url->rawssl('stockactual', 'getArticulos'));

        $tipoproducto = '-1';
        if ($request->has('tipoproducto', 'post')) {
            $view->set('tipoproducto', $request->get('tipoproducto', 'post'));
            $tipoproducto = $request->get('tipoproducto', 'post');
        } else {
            $view->set('tipoproducto', $request->get('tipoproducto', 'get'));
            $tipoproducto = $request->get('tipoproducto', 'get');
        }
        $view->set('tiposproducto', $database->getRows("SELECT * FROM tiposproducto ORDER BY descripcion ASC"));

        $puntovta = '-1';
        if ($request->has('puntovta', 'post')) {
            $view->set('puntovta', $request->get('puntovta', 'post'));
            $puntovta = $request->get('puntovta', 'post');
        } else {
            $view->set('puntovta', $request->get('puntovta', 'get'));
            $puntovta = $request->get('puntovta', 'get');
        }
        $view->set('puntosdeventa', $database->getRows("SELECT * FROM puntosdeventa ORDER BY descripcion ASC"));

        $articuloid = '';
        if ($request->has('articuloid', 'post')) {
            $view->set('articuloid', $request->get('articuloid', 'post'));
            $articuloid = $request->get('articulo', 'post');
        } else {
            $view->set('articuloid', $request->get('articuloid', 'get'));
            $articuloid = $request->get('articuloid', 'get');
        }
        $articulo_info = $database->getRow("SELECT articulo, concat(marca,' - ',descmodelo, ' ' ,if(codbarrainterno!='',concat('Cod: ',codbarrainterno), articulo)) as descripcion FROM vw_grilla_articulos WHERE articulo = '" . $articuloid . "'");

        $volvera = 'getList';
        if ($request->has('volvera', 'post')) {
            $view->set('volvera', $request->get('volvera', 'post'));
            $volvera = $request->get('volvera', 'post');
        } else {
            $view->set('volvera', $request->get('volvera', 'get'));
            $volvera = $request->get('volvera', 'get');
        }

        $search = '';
        if ($request->has('search', 'post')) {
            $view->set('search', $request->get('search', 'post'));
            $search = $request->get('search', 'post');
        } else {
            $view->set('search', $request->get('search', 'get'));
            $search = $request->get('search', 'get');
        }
        // </editor-fold>

        set_time_limit(0);

        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">

        $cols = array();

        $cols[] = array(
            'name' => 'PUNTO DE VENTA',
            'sort' => 'desctipoproducto',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'CANTIDAD',
            'sort' => 'descmarca',
            'align' => 'left'
        );


        $sort = array(
            'descpuntovta',
            'cantidad'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">

        if (strlen($search) == 0) {
            $sql = "SELECT * FROM vw_grilla_stockactual  ";
            $conca = " WHERE ";
        } else {
            $sql = "SELECT * FROM vw_grilla_stockactual WHERE descpuntovta LIKE '?'  ";
            $conca = " AND ";
        }

        //$sql = "SELECT * FROM vw_grilla_stockactual  ";
        //$conca = " WHERE ";

        if ($tipoproducto != '-1' && $tipoproducto != '') {
            $sql .= $conca . " tipoproducto = '" . $tipoproducto . "'  ";
            $conca = " AND ";
        }

//            if ($puntovtaid != '-1' && $puntovtaid != '') {
//                $sql .= $conca . " puntovta = '" . $puntovtaid . "'  ";
//            }

        if ($articuloid != null && $articuloid != '') {
            $sql .= $conca . " articulo = '" . $articuloid . "'  ";
            $conca = " AND ";
        }

        $sql .= " GROUP BY puntovta ASC";

        if (in_array($session->get('stockactual.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('stockactual.sort') . " " . (($session->get('stockactual.order') == 'desc') ? 'desc' : 'asc');
        } else {
            $sql .= " ORDER BY descpuntovta ASC";
        }

        $consulta = $database->parse($sql, '%' . $search . '%');
        $results = $database->getRows($consulta);


        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();

            $cell[] = array(
                'value' => @$result['descpuntovta'],
                'align' => 'left',
                'default' => 0
            );


            $cell[] = array(
                'value' => @$result['cantidad'],
                'align' => 'left',
                'default' => 0
            );


            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        //<editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">

        $view->set('heading_title', 'ARTICULO: ' . $articulo_info['descripcion']);
        $view->set('placeholder_buscar', 'BUSCA POR DESCRIPCION DEL PUNTO DE VENTA');

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        //$view->set('consultaVacia', $url->ssl('consulta','consulta_vacia'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        //$view->set('button_consultavacia', "Vacía");
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        //$view->set('search', $session->get('stockactual.search'));
        //$view->set('agruparpor', $session->get('stockactual.agruparpor'));
        $view->set('sort', $session->get('stockactual.sort'));
        $view->set('order', $session->get('stockactual.order'));
        $view->set('page', $session->get('stockactual.page'));

        //$view->set('stockactual.tipoproducto', '-1');
        //$view->set('stockactual.puntovta', '-1');
        //$view->set('tiposproducto', $database->getRows("SELECT * FROM tiposproducto ORDER BY descripcion ASC"));
        //$view->set('puntosdeventa', $database->getRows("SELECT * FROM puntosdeventa ORDER BY descripcion ASC"));

        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $view->set('titulo_ventana', '::. Exportar');

        $view->set('entry_modificar', "Modificar");
        $view->set('entry_agregar', "Agregar");

        $view->set('primeravez', $primeravez);

        if ($request->has('tiporeporte', 'post')) {
            $view->set('tiporeporte', $request->get('tiporeporte', 'post'));
        } else {
            $view->set('tiporeporte', 'CSV');
        }
        //$view->set('modelo', '');
        //$view->set('search', '');
        //$view->set('agruparpor', '1');
        //$view->set('stockactual.search', '');
        //$view->set('stockactual.agruparpor', '1');
        $cache->delete('stockactual');

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('stockactual'));
        $view->set('volver', $url->ssl('stockactual', 'index', array('agrupadopor' => $agrupadopor, 'puntovta' => $puntovta, 'tipoproducto' => $tipoproducto, 'auto_articulo_stock' => $articuloID, 'volvera' => 'getList', 'primeravez' => '1')));

        if ($user->hasPermisos($user->getPERSONA(), 'stockactual', 'C')) {
            $view->set('exportXLS', $url->ssl('stockactual', 'exportxlsdataXLSProductoDetalle', array('agrupadopor' => $agrupadopor, 'puntovta' => $puntovta, 'articuloid' => $result['articulo'], 'tipoproducto' => $tipoproducto, 'auto_articulo_stock' => $articuloID)));
            $view->set('exportCSV', $url->ssl('stockactual', 'exportxlsdataCSVProductoDetalle', array('agrupadopor' => $agrupadopor, 'puntovta' => $puntovta, 'articuloid' => $result['articulo'], 'tipoproducto' => $tipoproducto, 'auto_articulo_stock' => $articuloID)));
        }

        //$view->set('addPais', $url->ssl('stockactual','insert'));
        // $view->set('updatePersona', $url->ssl('paciente','update', array('persona' => $result['persona'])));
        //$view->set('updatePais', $url->ssl('stockactual','update'));
        // </editor-fold>

        return $view->fetch('content/list_stockactualPuntoVta.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">
//		if ($request->has('modelo', 'post')) {
//                    $session->set('stockactual.pais',$request->get('modelo','post'));              
//                }

        if ($request->has('search', 'post')) {
            $session->set('stockactual.search', $request->get('search', 'post'));
        }

        if ($request->has('tipoproducto', 'post')) {
            $session->set('stockactual.tipoproducto', $request->get('tipoproducto', 'post'));
        }

        if ($request->has('puntovta', 'post')) {
            $session->set('stockactual.puntovta', $request->get('puntovta', 'post'));
        }

        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('stockactual.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('stockactual.order', (($session->get('stockactual.sort') == $request->get('sort', 'post')) && ($session->get('stockactual.order') == 'asc')) ? 'desc' : 'asc');
        }

        if ($request->has('sort', 'post')) {
            $session->set('stockactual.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('stockactual'));
    }

    function exportxlsdataXLSPuntoVta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $common = & $this->locator->get('common');
        // </editor-fold>

        set_time_limit(0);

        try {
            $msg = '';

            // <editor-fold defaultstate="collapsed" desc="VARIABLES">
            $primeravez = '0';
            if ($request->isPost()) {
                $primeravez = '1';
            }

            if ($request->has('primeravez', 'post')) {
                $primeravez = '1';
            } else {
                if ($request->has('primeravez', 'get')) {
                    //$view->set('primeravez', $request->get('primeravez', 'get'));
                    $primeravez = $request->get('primeravez', 'get');
                }
            }

            $agrupadopor = '0';
            if ($request->has('agruparpor', 'post')) {
                //$view->set('agruparpor', $request->get('agruparpor', 'post'));
                $agrupadopor = $request->get('agruparpor', 'post');
            } else {
                //$view->set('agruparpor', $request->get('agruparpor', 'get'));
                $agrupadopor = $request->get('agruparpor', 'get');
            }

            $articuloID = "";
            if ($request->has('auto_articulo_stock', 'post')) {
                //$view->set('auto_articulo_stock', $request->get('auto_articulo_stock', 'post'));
                $articuloID = $request->get('auto_articulo_stock', 'post');
            } else {
                //$view->set('auto_articulo_stock', $request->get('auto_articulo_stock', 'get'));
                $articuloID = $request->get('auto_articulo_stock', 'get');
            }

            if ($request->has('auto_articulo', 'post')) {
                //$view->set('auto_articulo', $request->get('auto_articulo', 'post'));
            } else {
                if ($articuloID != "" && $articuloID != null) {
                    $sql = "SELECT articulo, concat(marca,' - ',descmodelo, ' ' ,if(codbarrainterno!='',concat('Cod: ',codbarrainterno), articulo)) as value "
                            . "FROM vw_grilla_articulos  "
                            . "WHERE articulo = '?'  ";
                    $articulo_info = $database->getRow($database->parse($sql, $articuloID));
                    //$view->set('auto_articulo', $articulo_info['value']);
                } else {
                    //$view->set('auto_articulo', '');
                }
            }
            //$view->set('script_busca_articulo', $url->rawssl('stockactual', 'getArticulos'));

            $tipoproducto = '-1';
            if ($request->has('tipoproducto', 'post')) {
                //$view->set('tipoproducto', $request->get('tipoproducto', 'post'));
                $tipoproducto = $request->get('tipoproducto', 'post');
            } else {
                //$view->set('tipoproducto', $request->get('tipoproducto', 'get'));
                $tipoproducto = $request->get('tipoproducto', 'get');
            }
            //$view->set('tiposproducto', $database->getRows("SELECT * FROM tiposproducto ORDER BY descripcion ASC"));

            $puntovta = '-1';
            if ($request->has('puntovta', 'post')) {
                //$view->set('puntovta', $request->get('puntovta', 'post'));
                $puntovta = $request->get('puntovta', 'post');
            } else {
                //$view->set('puntovta', $request->get('puntovta', 'get'));
                $puntovta = $request->get('puntovta', 'get');
            }
            //$view->set('puntosdeventa', $database->getRows("SELECT * FROM puntosdeventa ORDER BY descripcion ASC"));
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="HOJA UNO">
            // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">
            //** PHPExcel **//
            require_once (DIR_INCLUDES . '/library/phpexcel/PHPExcel.php');

            date_default_timezone_set('America/Argentina/Buenos_Aires');
            // Create new PHPExcel object
            $objPHPExcel = new PHPExcel();

            // Set properties of the file
            $objPHPExcel->getProperties()->setCreator("Accesorios")
                    ->setLastModifiedBy("Accesorios")
                    ->setTitle("Planilla web exportada: " . date('d-m-Y'))
                    ->setSubject("Planilla web exportada: " . date('d-m-Y'));


            //$nombre_de_las_columnas = array("TIPO", "MARCA", "MODELO", "GAMA", "COD INTERNO", "CANTIDAD");
            //genero las columnas del excel
            $letra = 'A';
            //$letra_fin = $this->devuelveLetra($cant_columnas - 1) ;
            $fila_inicial = 2; //fila desde donde empiezo a cargar la tabla en el excel. Arriba va el encabezado
            //$letra_numero_fin = $letra_fin . $cant_columnas;
            // <editor-fold defaultstate="collapsed" desc="PROPIEDADES DE LAS COLUMNAS VENTAS" >
            //$funciona = 'A1:'.$letra_fin . '1';
            $objPHPExcel->getActiveSheet()->getStyle('A1:B1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

            //COLUMNA A
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'PUNTO VTA');
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA B
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'CANTIDAD');
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            // </editor-fold>
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">

            $sql = "SELECT descpuntovta, SUM(cantidad) AS total FROM vw_grilla_stockactual  ";
            $conca = " WHERE ";

            if ($tipoproducto != '-1' && $tipoproducto != '') {
                $sql .= $conca . " tipoproducto = '" . $tipoproducto . "'  ";
                $conca = " AND ";
            }

            if ($puntovta != '-1' && $puntovta != '') {
                $sql .= $conca . " puntovta = '" . $puntovta . "'  ";
                $conca = " AND ";
            }

//        if ($articuloID != null && $articuloID != '') {
//            $sql .= $conca . " articulo = '" . $articuloID . "'  ";
//            $conca = " AND ";
//        }

            $sql .= " GROUP BY puntovta ASC";

            $sql .= " ORDER BY descpuntovta ASC";


            $results = $database->getRows($sql);


            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="LLANAR LOS DATOS VENTAS">

            $num = $fila_inicial;

            foreach ($results as $result) {

                //COLUMNA A
                $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $num, $result['descpuntovta']);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //COLUMNA B
                $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $num, $result['total']);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //HACE QUE EL NUMERO DE VENTA SALGA EN FORMATO CORRECTO
                //$objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getNumberFormat()->setFormatCode('0000');


                $num++;
            }

            // </editor-fold>
            //Cargo los datos celda por celda
            // Nombre de la hoja del libro
            $objPHPExcel->getActiveSheet()->setTitle('stockactual');

            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $objPHPExcel->setActiveSheetIndex(0);

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="HOJA DOS">
            // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">
            //** PHPExcel **//
            $positionInExcel = 1; //esto es para que ponga la nueva pestaña al principio

            $objPHPExcel->createSheet($positionInExcel); //creamos la pestaña

            $objPHPExcel->setActiveSheetIndex(1);

            $objPHPExcel->getActiveSheet()->setTitle('Articulos seriados');


            //$nombre_de_las_columnas = array("TIPO", "MARCA", "MODELO", "GAMA", "COD INTERNO", "CANTIDAD");
            //genero las columnas del excel
            $letra = 'A';
            //$letra_fin = $this->devuelveLetra($cant_columnas - 1) ;
            $fila_inicial = 2; //fila desde donde empiezo a cargar la tabla en el excel. Arriba va el encabezado
            //$letra_numero_fin = $letra_fin . $cant_columnas;
            // <editor-fold defaultstate="collapsed" desc="PROPIEDADES DE LAS COLUMNAS VENTAS" >
            //$funciona = 'A1:'.$letra_fin . '1';
            $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

            //COLUMNA A
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('A1', 'TIPO');
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA B
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('B1', 'MARCA');
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA C
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('C1', 'MODELO');
            $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA D
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('D1', 'GAMA');
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA E
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('E1', 'COD INTERNO');
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA F
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('F1', 'COD BARRAS');
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

            //COLUMNA G
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('G1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('G1', 'PUNTO VTA');
            $objPHPExcel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            // </editor-fold>
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">

            $sql = "SELECT desctipoproducto,descmarca,descmodelo,desctipogama,codbarrainterno, codbarra, descpuntovta FROM vw_grilla_stockactualbarras  ";
            $conca = " WHERE ";

            if ($tipoproducto != '-1' && $tipoproducto != '') {
                $sql .= $conca . " tipoproducto = '" . $tipoproducto . "'  ";
                $conca = " AND ";
            }

            if ($puntovta != '-1' && $puntovta != '') {
                $sql .= $conca . " puntovta = '" . $puntovta . "'  ";
                $conca = " AND ";
            }

//        if ($articuloID != null && $articuloID != '') {
            //$sql .= $conca . " estadoarticulo = 'DI'  ";
//            $conca = " AND ";
//        }
            $sql .= $conca . " estadoarticulo = 'DI'  ";
            //$sql .= " GROUP BY puntovta ASC";

            $sql .= " ORDER BY descpuntovta ASC";


            $results = $database->getRows($sql);


            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="LLANAR LOS DATOS VENTAS">



            $num = $fila_inicial;

            foreach ($results as $result) {

                //COLUMNA A
                $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('A' . $num, $result['desctipoproducto']);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //COLUMNA B
                $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('B' . $num, $result['descmarca']);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //HACE QUE EL NUMERO DE VENTA SALGA EN FORMATO CORRECTO
                $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getNumberFormat()->setFormatCode('0000');

                //COLUMNA C
                $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('C' . $num, $result['descmodelo']);
                $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                //COLUMNA D
                $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('D' . $num, $result['desctipogama']);
                $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                //COLUMNA E
                $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('E' . $num, $result['codbarrainterno']);
                $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //COLUMNA F
                $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('F' . $num, $result['codbarra']);
                $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //HACE QUE EL NUMERO DE VENTA SALGA EN FORMATO CORRECTO
                $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getNumberFormat()->setFormatCode('0000');

                //COLUMNA G
                $objPHPExcel->getActiveSheet()->getStyle('G' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('G' . $num, $result['descpuntovta']);
                $objPHPExcel->getActiveSheet()->getStyle('G' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                $num++;
            }


            // </editor-fold>
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="EXPORT EXCEL">
            //** PHPExcel **//
            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $objPHPExcel->setActiveSheetIndex(0);

            // Save Excel 2007 file
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

            // We'll be outputting an excel file
            header('Content-type: application/vnd.ms-excel');

            $fecha_d = date(" Y m d");
            $nombre_archivo = 'stockactual ' . $fecha_d . '.xls';
            // It will be called file.xls | esta linea hace que sea una descarga y no una pestaña aparte
            header('Content-Disposition: attachment; filename=' . $nombre_archivo);

            header("Expires: 0");

            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

            // Write file to the browser
            //$objWriter->save($nombre_archivo); 
            // $objWriter->save('Archivos/exportatablaxls/' . $nombre_archivo);
            // Write file to the browser
            $objWriter->save('php://output');

            $common->log_msg($msg, 'excel_log');
            //$objPHPExcel->
            //$objWriter->close();
            //$objPHPExcel->disconnectWorksheets();
            //unset($objWriter, $objPHPExcel);
            // </editor-fold>
        } catch (Exception $e) {
            $msg .= date('c') . "\r\n \t CATCH:\t " . $e . " \r\n";
            $common->log_msg($msg, 'excel_log');
        }
        //$view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
    }

    function exportxlsdataCSV() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $common = & $this->locator->get('common');
        // </editor-fold>
        //$tabla = 'vw_grilla_clientecontactos_xls';
        $tabla = 'vw_lista_reparaciondetallada_xls';
        set_time_limit(0);

        try {
            $msg = '';

            $TABLE_NAME = $tabla;

            // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">
            $sql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '" . $TABLE_NAME . "' AND table_schema = '" . DB_NAME . "'  ";
            $list_colum_name = $database->getRows($sql);
            $cant_columnas = count($list_colum_name);

            $nombre_de_las_columnas = array();
            for ($i = 0; $i < $cant_columnas; $i++) {
                $name = $list_colum_name[$i][COLUMN_NAME];
                $nombre_de_las_columnas[] = $name;
            }

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
            set_time_limit(0);

//                if (!$session->get('reporteplanillaventa.search')) {
            $sql = "SELECT * FROM " . $TABLE_NAME;
            $conca = " WHERE ";
//		} else {
//			$sql = "SELECT * FROM " . $TABLE_NAME; 
//                        $sql .= " WHERE (CLIENTE LIKE '?' or CONTACTO LIKE '?')  ";
//			$conca = " AND ";
//		}
            // <editor-fold defaultstate="collapsed" desc="FECHA">
            $fecha_d = '';
            if ($request->get('desde', 'post') != '') {
                $dated = explode('/', $request->get('desde', 'post'));
                // $fecha_d = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
                $fecha_d = str_replace('/', '-', $request->get('desde', 'post'));
            }
            if ($fecha_d != '') {
                $sql .= $conca . " STR_TO_DATE(FECHA, '%d-%m-%Y') >= STR_TO_DATE('" . $fecha_d . "', '%d-%m-%Y') ";
                $conca = " AND ";
            }

            $fecha_h = '';
            if ($request->get('hasta', 'post') != '') {
                $dated = explode('/', $request->get('hasta', 'post'));
                // $fecha_h = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
                $fecha_h = str_replace('/', '-', $request->get('hasta', 'post'));
                // $fecha_h = $request->get('hasta', 'post');
            }
            if ($fecha_h != '') {
                $sql .= $conca . " STR_TO_DATE(FECHA, '%d-%m-%Y') <= STR_TO_DATE('" . $fecha_h . "', '%d-%m-%Y')";
                $conca = " AND ";
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="VENDEDORES">
            $vendedores = $request->get('vendedores', 'post', array());
            $primero = "si";
            foreach ($vendedores as $vendedor) {
                if ($primero == "si") {
                    $conca .= 'vendedor IN (';
                }
                $sql .= $conca . "'" . $vendedor . "'";
                $conca = ", ";
                $primero = "no";
            }
            if ($vendedores) {
                $sql .= ') ';
                $conca = " AND ";
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="PUNTOS DE VENTA">
            $puntosdeventa = $request->get('puntosdeventa', 'post', array());
            $primero = "si";
            foreach ($puntosdeventa as $item) {
                if ($primero == "si") {
                    $conca .= 'PUNTOVTA IN (';
                }
                $sql .= $conca . "'" . $item . "'";
                $conca = ", ";
                $primero = "no";
            }
            if ($puntosdeventa) {
                $sql .= ') ';
                $conca = " AND ";
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="TIPOS DE PRODUCTO">
            $vendedores = $request->get('vendedores', 'post', array());
            $primero = "si";
            foreach ($vendedores as $vendedor) {
                if ($primero == "si") {
                    $conca .= 'vendedor IN (';
                }
                $sql .= $conca . "'" . $vendedor . "'";
                $conca = ", ";
                $primero = "no";
            }
            if ($vendedores) {
                $sql .= ') ';
                $conca = " AND ";
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="MARCAS">
            $marcas = $request->get('marcas', 'post', array());
            $primero = "si";
            foreach ($marcas as $item) {
                if ($primero == "si") {
                    $conca .= 'marca IN (';
                }
                $sql .= $conca . "'" . $item . "'";
                $conca = ", ";
                $primero = "no";
            }
            if ($marcas) {
                $sql .= ') ';
                $conca = " AND ";
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="MODELOS">
            $vendedores = $request->get('vendedores', 'post', array());
            $primero = "si";
            foreach ($vendedores as $vendedor) {
                if ($primero == "si") {
                    $conca .= 'VENDEDOR IN (';
                }
                $sql .= $conca . "'" . $vendedor . "'";
                $conca = ", ";
                $primero = "no";
            }
            if ($vendedores) {
                $sql .= ') ';
                $conca = " AND ";
            }
            // </editor-fold>
            //$sql = "SELECT * FROM " . $TABLE_NAME;
            $results = $database->getRows($sql);

            //LE METO EN ELCABEZADO
            array_unshift($results, $nombre_de_las_columnas);

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="TABLA SELECCIONADA A CSV">
            //cabeceras para descarga
            header('Content-Type: application/octet-stream');
            header("Content-Transfer-Encoding: Binary");
            header("Content-disposition: attachment; filename=\"my_csv_file.csv\"");

            //preparar el wrapper de salida
            $outputBuffer = fopen("php://output", 'w');

            //volcamos el contenido del array en formato csv
            foreach ($results as $val) {
                //fputcsv($outputBuffer, $val);
                fputcsv($outputBuffer, $val, ";");
            }
            //cerramos el wrapper
            fclose($outputBuffer);
            exit;

            // </editor-fold>
        } catch (Exception $e) {
            $msg .= date('c') . "\r\n \t CATCH:\t " . $e . " \r\n";
            $common->log_msg($msg, 'excel_log');
        }
        //$view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
    }

    function exportxlsdataCSVPuntoVta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $common = & $this->locator->get('common');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VARIABLES">
        $primeravez = '0';
        if ($request->isPost()) {
            $primeravez = '1';
        }

        if ($request->has('primeravez', 'post')) {
            $primeravez = '1';
        } else {
            if ($request->has('primeravez', 'get')) {
                //$view->set('primeravez', $request->get('primeravez', 'get'));
                $primeravez = $request->get('primeravez', 'get');
            }
        }

        $agrupadopor = '0';
        if ($request->has('agruparpor', 'post')) {
            //$view->set('agruparpor', $request->get('agruparpor', 'post'));
            $agrupadopor = $request->get('agruparpor', 'post');
        } else {
            //$view->set('agruparpor', $request->get('agruparpor', 'get'));
            $agrupadopor = $request->get('agruparpor', 'get');
        }

        $articuloID = "";
        if ($request->has('auto_articulo_stock', 'post')) {
            //$view->set('auto_articulo_stock', $request->get('auto_articulo_stock', 'post'));
            $articuloID = $request->get('auto_articulo_stock', 'post');
        } else {
            //$view->set('auto_articulo_stock', $request->get('auto_articulo_stock', 'get'));
            $articuloID = $request->get('auto_articulo_stock', 'get');
        }

        if ($request->has('auto_articulo', 'post')) {
            //$view->set('auto_articulo', $request->get('auto_articulo', 'post'));
        } else {
            if ($articuloID != "" && $articuloID != null) {
                $sql = "SELECT articulo, concat(marca,' - ',descmodelo, ' ' ,if(codbarrainterno!='',concat('Cod: ',codbarrainterno), articulo)) as value "
                        . "FROM vw_grilla_articulos  "
                        . "WHERE articulo = '?'  ";
                $articulo_info = $database->getRow($database->parse($sql, $articuloID));
                //$view->set('auto_articulo', $articulo_info['value']);
            } else {
                //$view->set('auto_articulo', '');
            }
        }
        //$view->set('script_busca_articulo', $url->rawssl('stockactual', 'getArticulos'));

        $tipoproducto = '-1';
        if ($request->has('tipoproducto', 'post')) {
            //$view->set('tipoproducto', $request->get('tipoproducto', 'post'));
            $tipoproducto = $request->get('tipoproducto', 'post');
        } else {
            //$view->set('tipoproducto', $request->get('tipoproducto', 'get'));
            $tipoproducto = $request->get('tipoproducto', 'get');
        }
        //$view->set('tiposproducto', $database->getRows("SELECT * FROM tiposproducto ORDER BY descripcion ASC"));

        $puntovta = '-1';
        if ($request->has('puntovta', 'post')) {
            //$view->set('puntovta', $request->get('puntovta', 'post'));
            $puntovta = $request->get('puntovta', 'post');
        } else {
            //$view->set('puntovta', $request->get('puntovta', 'get'));
            $puntovta = $request->get('puntovta', 'get');
        }
        //$view->set('puntosdeventa', $database->getRows("SELECT * FROM puntosdeventa ORDER BY descripcion ASC"));
        // </editor-fold>

        set_time_limit(0);

        try {
            $msg = '';


            // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">

            $nombre_de_las_columnas = array("PUNTO VTA", "CANTIDAD");

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">

            $sql = "SELECT descpuntovta, SUM(cantidad) AS total FROM vw_grilla_stockactual  ";
            $conca = " WHERE ";

            if ($tipoproducto != '-1' && $tipoproducto != '') {
                $sql .= $conca . " tipoproducto = '" . $tipoproducto . "'  ";
                $conca = " AND ";
            }

            if ($puntovta != '-1' && $puntovta != '') {
                $sql .= $conca . " puntovta = '" . $puntovta . "'  ";
                $conca = " AND ";
            }

//        if ($articuloID != null && $articuloID != '') {
//            $sql .= $conca . " articulo = '" . $articuloID . "'  ";
//            $conca = " AND ";
//        }

            $sql .= " GROUP BY puntovta ASC";

            $sql .= " ORDER BY descpuntovta ASC";


            $results = $database->getRows($sql);

            //LE METO EN ELCABEZADO
            array_unshift($results, $nombre_de_las_columnas);
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="TABLA SELECCIONADA A CSV">
            //cabeceras para descarga
            header('Content-Type: application/octet-stream');
            header("Content-Transfer-Encoding: Binary");
            header("Content-disposition: attachment; filename=\"StockActual.csv\"");

            //preparar el wrapper de salida
            $outputBuffer = fopen("php://output", 'w');

            //volcamos el contenido del array en formato csv
            foreach ($results as $val) {
                //fputcsv($outputBuffer, $val);
                fputcsv($outputBuffer, $val, ";");
            }
            //cerramos el wrapper
            fclose($outputBuffer);
            exit;

            // </editor-fold>
        } catch (Exception $e) {
            $msg .= date('c') . "\r\n \t CATCH:\t " . $e . " \r\n";
            $common->log_msg($msg, 'excel_log');
        }
        //$view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
    }

    function exportxlsdataCSVProducto() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $common = & $this->locator->get('common');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VARIABLES">
        $primeravez = '0';
        if ($request->isPost()) {
            $primeravez = '1';
        }

        if ($request->has('primeravez', 'post')) {
            $primeravez = '1';
        } else {
            if ($request->has('primeravez', 'get')) {
                //$view->set('primeravez', $request->get('primeravez', 'get'));
                $primeravez = $request->get('primeravez', 'get');
            }
        }

        $agrupadopor = '0';
        if ($request->has('agruparpor', 'post')) {
            //$view->set('agruparpor', $request->get('agruparpor', 'post'));
            $agrupadopor = $request->get('agruparpor', 'post');
        } else {
            //$view->set('agruparpor', $request->get('agruparpor', 'get'));
            $agrupadopor = $request->get('agruparpor', 'get');
        }

        $articuloID = "";
        if ($request->has('auto_articulo_stock', 'post')) {
            //$view->set('auto_articulo_stock', $request->get('auto_articulo_stock', 'post'));
            $articuloID = $request->get('auto_articulo_stock', 'post');
        } else {
            //$view->set('auto_articulo_stock', $request->get('auto_articulo_stock', 'get'));
            $articuloID = $request->get('auto_articulo_stock', 'get');
        }

        if ($request->has('auto_articulo', 'post')) {
            //$view->set('auto_articulo', $request->get('auto_articulo', 'post'));
        } else {
            if ($articuloID != "" && $articuloID != null) {
                $sql = "SELECT articulo, concat(marca,' - ',descmodelo, ' ' ,if(codbarrainterno!='',concat('Cod: ',codbarrainterno), articulo)) as value "
                        . "FROM vw_grilla_articulos  "
                        . "WHERE articulo = '?'  ";
                $articulo_info = $database->getRow($database->parse($sql, $articuloID));
                //$view->set('auto_articulo', $articulo_info['value']);
            } else {
                //$view->set('auto_articulo', '');
            }
        }
        //$view->set('script_busca_articulo', $url->rawssl('stockactual', 'getArticulos'));

        $tipoproducto = '-1';
        if ($request->has('tipoproducto', 'post')) {
            //$view->set('tipoproducto', $request->get('tipoproducto', 'post'));
            $tipoproducto = $request->get('tipoproducto', 'post');
        } else {
            //$view->set('tipoproducto', $request->get('tipoproducto', 'get'));
            $tipoproducto = $request->get('tipoproducto', 'get');
        }
        //$view->set('tiposproducto', $database->getRows("SELECT * FROM tiposproducto ORDER BY descripcion ASC"));

        $puntovta = '-1';
        if ($request->has('puntovta', 'post')) {
            //$view->set('puntovta', $request->get('puntovta', 'post'));
            $puntovta = $request->get('puntovta', 'post');
        } else {
            //$view->set('puntovta', $request->get('puntovta', 'get'));
            $puntovta = $request->get('puntovta', 'get');
        }
        //$view->set('puntosdeventa', $database->getRows("SELECT * FROM puntosdeventa ORDER BY descripcion ASC"));
        // </editor-fold>

        set_time_limit(0);

        try {
            $msg = '';

            // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">

            $nombre_de_las_columnas = array("TIPO", "MARCA", "MODELO", "GAMA", "COD INTERNO", "CANTIDAD");

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
            $sql = "SELECT desctipoproducto,descmarca,descmodelo,desctipogama,codbarrainterno, SUM(cantidad) AS total FROM vw_grilla_stockactual  ";
            $conca = " WHERE ";

            if ($tipoproducto != '-1' && $tipoproducto != '') {
                $sql .= $conca . " tipoproducto = '" . $tipoproducto . "'  ";
                $conca = " AND ";
            }

            if ($puntovta != '-1' && $puntovta != '') {
                $sql .= $conca . " puntovta = '" . $puntovta . "'  ";
                $conca = " AND ";
            }

            if ($articuloID != null && $articuloID != '') {
                $sql .= $conca . " articulo = '" . $articuloID . "'  ";
                $conca = " AND ";
            }

            $sql .= " GROUP BY articulo ASC";

            $sql .= " ORDER BY desctipoproducto, descmarca, descmodelo ASC";

            $results = $database->getRows($sql);

            //LE METO EN ELCABEZADO
            array_unshift($results, $nombre_de_las_columnas);
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="TABLA SELECCIONADA A CSV">
            //cabeceras para descarga
            header('Content-Type: application/octet-stream');
            header("Content-Transfer-Encoding: Binary");
            header("Content-disposition: attachment; filename=\"StockActual.csv\"");

            //preparar el wrapper de salida
            $outputBuffer = fopen("php://output", 'w');

            //volcamos el contenido del array en formato csv
            foreach ($results as $val) {
                //fputcsv($outputBuffer, $val);
                fputcsv($outputBuffer, $val, ";");
            }
            //cerramos el wrapper
            fclose($outputBuffer);
            exit;

            // </editor-fold>
        } catch (Exception $e) {
            $msg .= date('c') . "\r\n \t CATCH:\t " . $e . " \r\n";
            $common->log_msg($msg, 'excel_log');
        }
        //$view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
    }

    function exportxlsdataXLSProducto() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $common = & $this->locator->get('common');
        // </editor-fold>

        set_time_limit(0);

        try {
            $msg = '';

            // <editor-fold defaultstate="collapsed" desc="VARIABLES">
            $primeravez = '0';
            if ($request->isPost()) {
                $primeravez = '1';
            }

            if ($request->has('primeravez', 'post')) {
                $primeravez = '1';
            } else {
                if ($request->has('primeravez', 'get')) {
                    //$view->set('primeravez', $request->get('primeravez', 'get'));
                    $primeravez = $request->get('primeravez', 'get');
                }
            }

            $agrupadopor = '0';
            if ($request->has('agruparpor', 'post')) {
                //$view->set('agruparpor', $request->get('agruparpor', 'post'));
                $agrupadopor = $request->get('agruparpor', 'post');
            } else {
                //$view->set('agruparpor', $request->get('agruparpor', 'get'));
                $agrupadopor = $request->get('agruparpor', 'get');
            }

            $articuloID = "";
            if ($request->has('auto_articulo_stock', 'post')) {
                //$view->set('auto_articulo_stock', $request->get('auto_articulo_stock', 'post'));
                $articuloID = $request->get('auto_articulo_stock', 'post');
            } else {
                //$view->set('auto_articulo_stock', $request->get('auto_articulo_stock', 'get'));
                $articuloID = $request->get('auto_articulo_stock', 'get');
            }

            if ($request->has('auto_articulo', 'post')) {
                //$view->set('auto_articulo', $request->get('auto_articulo', 'post'));
            } else {
                if ($articuloID != "" && $articuloID != null) {
                    $sql = "SELECT articulo, concat(marca,' - ',descmodelo, ' ' ,if(codbarrainterno!='',concat('Cod: ',codbarrainterno), articulo)) as value "
                            . "FROM vw_grilla_articulos  "
                            . "WHERE articulo = '?'  ";
                    $articulo_info = $database->getRow($database->parse($sql, $articuloID));
                    //$view->set('auto_articulo', $articulo_info['value']);
                } else {
                    //$view->set('auto_articulo', '');
                }
            }
            //$view->set('script_busca_articulo', $url->rawssl('stockactual', 'getArticulos'));

            $tipoproducto = '-1';
            if ($request->has('tipoproducto', 'post')) {
                //$view->set('tipoproducto', $request->get('tipoproducto', 'post'));
                $tipoproducto = $request->get('tipoproducto', 'post');
            } else {
                //$view->set('tipoproducto', $request->get('tipoproducto', 'get'));
                $tipoproducto = $request->get('tipoproducto', 'get');
            }
            //$view->set('tiposproducto', $database->getRows("SELECT * FROM tiposproducto ORDER BY descripcion ASC"));

            $puntovta = '-1';
            if ($request->has('puntovta', 'post')) {
                //$view->set('puntovta', $request->get('puntovta', 'post'));
                $puntovta = $request->get('puntovta', 'post');
            } else {
                //$view->set('puntovta', $request->get('puntovta', 'get'));
                $puntovta = $request->get('puntovta', 'get');
            }
            //$view->set('puntosdeventa', $database->getRows("SELECT * FROM puntosdeventa ORDER BY descripcion ASC"));
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="HOJA UNO">
            // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">
            //** PHPExcel **//
            require_once (DIR_INCLUDES . '/library/phpexcel/PHPExcel.php');

            date_default_timezone_set('America/Argentina/Buenos_Aires');
            // Create new PHPExcel object
            $objPHPExcel = new PHPExcel();

            // Set properties of the file
            $objPHPExcel->getProperties()->setCreator("Accesorios")
                    ->setLastModifiedBy("Accesorios")
                    ->setTitle("Planilla web exportada: " . date('d-m-Y'))
                    ->setSubject("Planilla web exportada: " . date('d-m-Y'));


            //$nombre_de_las_columnas = array("TIPO", "MARCA", "MODELO", "GAMA", "COD INTERNO", "CANTIDAD");
            //genero las columnas del excel
            $letra = 'A';
            //$letra_fin = $this->devuelveLetra($cant_columnas - 1) ;
            $fila_inicial = 2; //fila desde donde empiezo a cargar la tabla en el excel. Arriba va el encabezado
            //$letra_numero_fin = $letra_fin . $cant_columnas;
            // <editor-fold defaultstate="collapsed" desc="PROPIEDADES DE LAS COLUMNAS VENTAS" >
            //$funciona = 'A1:'.$letra_fin . '1';
            $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

            //COLUMNA A
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'TIPO');
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA B
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'MARCA');
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA C
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', 'MODELO');
            $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA D
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', 'GAMA');
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA E
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', 'COD INTERNO');
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA F
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', 'CANTIDAD');
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            // </editor-fold>         
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
            $sql = "SELECT desctipoproducto,descmarca,descmodelo,desctipogama,codbarrainterno, SUM(cantidad) AS total FROM vw_grilla_stockactual  ";
            $conca = " WHERE ";

            if ($tipoproducto != '-1' && $tipoproducto != '') {
                $sql .= $conca . " tipoproducto = '" . $tipoproducto . "'  ";
                $conca = " AND ";
            }

            if ($puntovta != '-1' && $puntovta != '') {
                $sql .= $conca . " puntovta = '" . $puntovta . "'  ";
                $conca = " AND ";
            }

            if ($articuloID != null && $articuloID != '') {
                $sql .= $conca . " articulo = '" . $articuloID . "'  ";
                $conca = " AND ";
            }

            $sql .= " GROUP BY articulo ASC";

            $sql .= " ORDER BY desctipoproducto, descmarca, descmodelo ASC";

            $results = $database->getRows($sql);


            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="LLANAR LOS DATOS VENTAS">

            $num = $fila_inicial;

            foreach ($results as $result) {


                //COLUMNA A
                $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $num, $result['desctipoproducto']);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //COLUMNA B
                $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $num, $result['descmarca']);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //HACE QUE EL NUMERO DE VENTA SALGA EN FORMATO CORRECTO
                $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getNumberFormat()->setFormatCode('0000');

                //COLUMNA C
                $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $num, $result['descmodelo']);
                $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                //COLUMNA D
                $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' . $num, $result['desctipogama']);
                $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                //COLUMNA E
                $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' . $num, $result['codbarrainterno']);
                $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //COLUMNA F
                $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' . $num, $result['total']);
                $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                $num++;
            }

            // </editor-fold>
            //Cargo los datos celda por celda
            // Nombre de la hoja del libro
            $objPHPExcel->getActiveSheet()->setTitle('stockactual');

            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $objPHPExcel->setActiveSheetIndex(0);

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="HOJA DOS">
            // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">
            //** PHPExcel **//
            $positionInExcel = 1; //esto es para que ponga la nueva pestaña al principio

            $objPHPExcel->createSheet($positionInExcel); //creamos la pestaña

            $objPHPExcel->setActiveSheetIndex(1);

            $objPHPExcel->getActiveSheet()->setTitle('Articulos seriados');


            //$nombre_de_las_columnas = array("TIPO", "MARCA", "MODELO", "GAMA", "COD INTERNO", "CANTIDAD");
            //genero las columnas del excel
            $letra = 'A';
            //$letra_fin = $this->devuelveLetra($cant_columnas - 1) ;
            $fila_inicial = 2; //fila desde donde empiezo a cargar la tabla en el excel. Arriba va el encabezado
            //$letra_numero_fin = $letra_fin . $cant_columnas;
            // <editor-fold defaultstate="collapsed" desc="PROPIEDADES DE LAS COLUMNAS VENTAS" >
            //$funciona = 'A1:'.$letra_fin . '1';
            $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

            //COLUMNA A
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('A1', 'TIPO');
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA B
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('B1', 'MARCA');
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA C
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('C1', 'MODELO');
            $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA D
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('D1', 'GAMA');
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA E
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('E1', 'COD INTERNO');
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA F
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('F1', 'COD BARRAS');
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

            //COLUMNA G
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('G1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('G1', 'PUNTO VTA');
            $objPHPExcel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            // </editor-fold>
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">

            $sql = "SELECT desctipoproducto,descmarca,descmodelo,desctipogama,codbarrainterno, codbarra, descpuntovta FROM vw_grilla_stockactualbarras  ";
            $conca = " WHERE ";

            if ($tipoproducto != '-1' && $tipoproducto != '') {
                $sql .= $conca . " tipoproducto = '" . $tipoproducto . "'  ";
                $conca = " AND ";
            }

            if ($puntovta != '-1' && $puntovta != '') {
                $sql .= $conca . " puntovta = '" . $puntovta . "'  ";
                $conca = " AND ";
            }

//        if ($articuloID != null && $articuloID != '') {
            //$sql .= $conca . " estadoarticulo = 'DI'  ";
//            $conca = " AND ";
//        }
            $sql .= $conca . " estadoarticulo = 'DI'  ";
            //$sql .= " GROUP BY puntovta ASC";

            $sql .= " ORDER BY descpuntovta ASC";


            $results = $database->getRows($sql);


            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="LLANAR LOS DATOS VENTAS">



            $num = $fila_inicial;

            foreach ($results as $result) {

                //COLUMNA A
                $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('A' . $num, $result['desctipoproducto']);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //COLUMNA B
                $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('B' . $num, $result['descmarca']);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //HACE QUE EL NUMERO DE VENTA SALGA EN FORMATO CORRECTO
                $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getNumberFormat()->setFormatCode('0000');

                //COLUMNA C
                $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('C' . $num, $result['descmodelo']);
                $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                //COLUMNA D
                $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('D' . $num, $result['desctipogama']);
                $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                //COLUMNA E
                $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('E' . $num, $result['codbarrainterno']);
                $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //COLUMNA F
                $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('F' . $num, $result['codbarra']);
                $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //HACE QUE EL NUMERO DE VENTA SALGA EN FORMATO CORRECTO
                $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getNumberFormat()->setFormatCode('0000');

                //COLUMNA G
                $objPHPExcel->getActiveSheet()->getStyle('G' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('G' . $num, $result['descpuntovta']);
                $objPHPExcel->getActiveSheet()->getStyle('G' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                $num++;
            }


            // </editor-fold>
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="EXPORT EXCEL">
            //** PHPExcel **//
            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $objPHPExcel->setActiveSheetIndex(0);

            // Save Excel 2007 file
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

            // We'll be outputting an excel file
            header('Content-type: application/vnd.ms-excel');

            $fecha_d = date(" Y m d");
            $nombre_archivo = 'stockactual ' . $fecha_d . '.xls';
            // It will be called file.xls | esta linea hace que sea una descarga y no una pestaña aparte
            header('Content-Disposition: attachment; filename=' . $nombre_archivo);

            header("Expires: 0");

            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

            // Write file to the browser
            //$objWriter->save($nombre_archivo); 
            // $objWriter->save('Archivos/exportatablaxls/' . $nombre_archivo);
            // Write file to the browser
            $objWriter->save('php://output');

            $common->log_msg($msg, 'excel_log');
            //$objPHPExcel->
            //$objWriter->close();
            //$objPHPExcel->disconnectWorksheets();
            //unset($objWriter, $objPHPExcel);
            // </editor-fold>
        } catch (Exception $e) {
            $msg .= date('c') . "\r\n \t CATCH:\t " . $e . " \r\n";
            $common->log_msg($msg, 'excel_log');
        }
        //$view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
    }

    function exportxlsdataXLSProducto_old() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $common = & $this->locator->get('common');
        // </editor-fold>

        set_time_limit(0);

        try {
            $msg = '';

            // <editor-fold defaultstate="collapsed" desc="VARIABLES">
            $primeravez = '0';
            if ($request->isPost()) {
                $primeravez = '1';
            }

            if ($request->has('primeravez', 'post')) {
                $primeravez = '1';
            } else {
                if ($request->has('primeravez', 'get')) {
                    //$view->set('primeravez', $request->get('primeravez', 'get'));
                    $primeravez = $request->get('primeravez', 'get');
                }
            }

            $agrupadopor = '0';
            if ($request->has('agruparpor', 'post')) {
                //$view->set('agruparpor', $request->get('agruparpor', 'post'));
                $agrupadopor = $request->get('agruparpor', 'post');
            } else {
                //$view->set('agruparpor', $request->get('agruparpor', 'get'));
                $agrupadopor = $request->get('agruparpor', 'get');
            }

            $articuloID = "";
            if ($request->has('auto_articulo_stock', 'post')) {
                //$view->set('auto_articulo_stock', $request->get('auto_articulo_stock', 'post'));
                $articuloID = $request->get('auto_articulo_stock', 'post');
            } else {
                //$view->set('auto_articulo_stock', $request->get('auto_articulo_stock', 'get'));
                $articuloID = $request->get('auto_articulo_stock', 'get');
            }

            if ($request->has('auto_articulo', 'post')) {
                //$view->set('auto_articulo', $request->get('auto_articulo', 'post'));
            } else {
                if ($articuloID != "" && $articuloID != null) {
                    $sql = "SELECT articulo, concat(marca,' - ',descmodelo, ' ' ,if(codbarrainterno!='',concat('Cod: ',codbarrainterno), articulo)) as value "
                            . "FROM vw_grilla_articulos  "
                            . "WHERE articulo = '?'  ";
                    $articulo_info = $database->getRow($database->parse($sql, $articuloID));
                    //$view->set('auto_articulo', $articulo_info['value']);
                } else {
                    //$view->set('auto_articulo', '');
                }
            }
            //$view->set('script_busca_articulo', $url->rawssl('stockactual', 'getArticulos'));

            $tipoproducto = '-1';
            if ($request->has('tipoproducto', 'post')) {
                //$view->set('tipoproducto', $request->get('tipoproducto', 'post'));
                $tipoproducto = $request->get('tipoproducto', 'post');
            } else {
                //$view->set('tipoproducto', $request->get('tipoproducto', 'get'));
                $tipoproducto = $request->get('tipoproducto', 'get');
            }
            //$view->set('tiposproducto', $database->getRows("SELECT * FROM tiposproducto ORDER BY descripcion ASC"));

            $puntovta = '-1';
            if ($request->has('puntovta', 'post')) {
                //$view->set('puntovta', $request->get('puntovta', 'post'));
                $puntovta = $request->get('puntovta', 'post');
            } else {
                //$view->set('puntovta', $request->get('puntovta', 'get'));
                $puntovta = $request->get('puntovta', 'get');
            }
            //$view->set('puntosdeventa', $database->getRows("SELECT * FROM puntosdeventa ORDER BY descripcion ASC"));
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">
            //** PHPExcel **//
            require_once (DIR_INCLUDES . '/library/phpexcel/PHPExcel.php');

            date_default_timezone_set('America/Argentina/Buenos_Aires');
            // Create new PHPExcel object
            $objPHPExcel = new PHPExcel();

            // Set properties of the file
            $objPHPExcel->getProperties()->setCreator("Accesorios")
                    ->setLastModifiedBy("Accesorios")
                    ->setTitle("Planilla web exportada: " . date('d-m-Y'))
                    ->setSubject("Planilla web exportada: " . date('d-m-Y'));


            //$nombre_de_las_columnas = array("TIPO", "MARCA", "MODELO", "GAMA", "COD INTERNO", "CANTIDAD");
            //genero las columnas del excel
            $letra = 'A';
            //$letra_fin = $this->devuelveLetra($cant_columnas - 1) ;
            $fila_inicial = 2; //fila desde donde empiezo a cargar la tabla en el excel. Arriba va el encabezado
            //$letra_numero_fin = $letra_fin . $cant_columnas;
            // <editor-fold defaultstate="collapsed" desc="PROPIEDADES DE LAS COLUMNAS VENTAS" >
            //$funciona = 'A1:'.$letra_fin . '1';
            $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

            //COLUMNA A
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'TIPO');
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA B
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'MARCA');
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA C
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', 'MODELO');
            $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA D
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', 'GAMA');
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA E
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', 'COD INTERNO');
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA F
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', 'CANTIDAD');
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            // </editor-fold>         
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
            $sql = "SELECT desctipoproducto,descmarca,descmodelo,desctipogama,codbarrainterno, SUM(cantidad) AS total FROM vw_grilla_stockactual  ";
            $conca = " WHERE ";

            if ($tipoproducto != '-1' && $tipoproducto != '') {
                $sql .= $conca . " tipoproducto = '" . $tipoproducto . "'  ";
                $conca = " AND ";
            }

            if ($puntovta != '-1' && $puntovta != '') {
                $sql .= $conca . " puntovta = '" . $puntovta . "'  ";
                $conca = " AND ";
            }

            if ($articuloID != null && $articuloID != '') {
                $sql .= $conca . " articulo = '" . $articuloID . "'  ";
                $conca = " AND ";
            }

            $sql .= " GROUP BY articulo ASC";

            $sql .= " ORDER BY desctipoproducto, descmarca, descmodelo ASC";

            $results = $database->getRows($sql);


            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="LLANAR LOS DATOS VENTAS">

            $num = $fila_inicial;

            foreach ($results as $result) {

                //COLUMNA A
                $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $num, $result['desctipoproducto']);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //COLUMNA B
                $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $num, $result['descmarca']);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //HACE QUE EL NUMERO DE VENTA SALGA EN FORMATO CORRECTO
                $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getNumberFormat()->setFormatCode('0000');

                //COLUMNA C
                $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $num, $result['descmodelo']);
                $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                //COLUMNA D
                $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' . $num, $result['desctipogama']);
                $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                //COLUMNA E
                $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' . $num, $result['codbarrainterno']);
                $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //COLUMNA F
                $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' . $num, $result['total']);
                $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                $num++;
            }

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="EXPORT EXCEL">
            //** PHPExcel **//
            //Cargo los datos celda por celda
            // Nombre de la hoja del libro
            $objPHPExcel->getActiveSheet()->setTitle('stockactual');

            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $objPHPExcel->setActiveSheetIndex(0);

            // Save Excel 2007 file
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

            // We'll be outputting an excel file
            header('Content-type: application/vnd.ms-excel');

            $fecha_d = date(" Y m d");

            $nombre_archivo = 'stockactual ' . $fecha_d . '.xls';
            // It will be called file.xls | esta linea hace que sea una descarga y no una pestaña aparte
            header('Content-Disposition: attachment; filename=' . $nombre_archivo);

            header("Expires: 0");

            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

            // Write file to the browser
            //$objWriter->save($nombre_archivo); 
            // $objWriter->save('Archivos/exportatablaxls/' . $nombre_archivo);
            // Write file to the browser
            $objWriter->save('php://output');

            $common->log_msg($msg, 'excel_log');
            //$objPHPExcel->
            //$objWriter->close();
            //$objPHPExcel->disconnectWorksheets();
            //unset($objWriter, $objPHPExcel);
            // </editor-fold>
        } catch (Exception $e) {
            $msg .= date('c') . "\r\n \t CATCH:\t " . $e . " \r\n";
            $common->log_msg($msg, 'excel_log');
        }
        //$view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
    }

    function exportxlsdataCSVPuntoVtaDetalle() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $common = & $this->locator->get('common');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VARIABLES">
        $primeravez = '0';
        if ($request->isPost()) {
            $primeravez = '1';
        }

        if ($request->has('primeravez', 'post')) {
            $primeravez = '1';
        } else {
            if ($request->has('primeravez', 'get')) {
                //$view->set('primeravez', $request->get('primeravez', 'get'));
                $primeravez = $request->get('primeravez', 'get');
            }
        }

        $agrupadopor = '0';
        if ($request->has('agruparpor', 'post')) {
            //$view->set('agruparpor', $request->get('agruparpor', 'post'));
            $agrupadopor = $request->get('agruparpor', 'post');
        } else {
            //$view->set('agruparpor', $request->get('agruparpor', 'get'));
            $agrupadopor = $request->get('agruparpor', 'get');
        }

        $articuloID = "";
        if ($request->has('auto_articulo_stock', 'post')) {
            //$view->set('auto_articulo_stock', $request->get('auto_articulo_stock', 'post'));
            $articuloID = $request->get('auto_articulo_stock', 'post');
        } else {
            //$view->set('auto_articulo_stock', $request->get('auto_articulo_stock', 'get'));
            $articuloID = $request->get('auto_articulo_stock', 'get');
        }

        if ($request->has('auto_articulo', 'post')) {
            //$view->set('auto_articulo', $request->get('auto_articulo', 'post'));
        } else {
            if ($articuloID != "" && $articuloID != null) {
                $sql = "SELECT articulo, concat(marca,' - ',descmodelo, ' ' ,if(codbarrainterno!='',concat('Cod: ',codbarrainterno), articulo)) as value "
                        . "FROM vw_grilla_articulos  "
                        . "WHERE articulo = '?'  ";
                $articulo_info = $database->getRow($database->parse($sql, $articuloID));
                //$view->set('auto_articulo', $articulo_info['value']);
            } else {
                //$view->set('auto_articulo', '');
            }
        }
        //$view->set('script_busca_articulo', $url->rawssl('stockactual', 'getArticulos'));

        $tipoproducto = '-1';
        if ($request->has('tipoproducto', 'post')) {
            //$view->set('tipoproducto', $request->get('tipoproducto', 'post'));
            $tipoproducto = $request->get('tipoproducto', 'post');
        } else {
            //$view->set('tipoproducto', $request->get('tipoproducto', 'get'));
            $tipoproducto = $request->get('tipoproducto', 'get');
        }
        //$view->set('tiposproducto', $database->getRows("SELECT * FROM tiposproducto ORDER BY descripcion ASC"));

        $puntovta = '-1';
        if ($request->has('puntovta', 'post')) {
            //$view->set('puntovta', $request->get('puntovta', 'post'));
            $puntovta = $request->get('puntovta', 'post');
        } else {
            //$view->set('puntovta', $request->get('puntovta', 'get'));
            $puntovta = $request->get('puntovta', 'get');
        }
        //$view->set('puntosdeventa', $database->getRows("SELECT * FROM puntosdeventa ORDER BY descripcion ASC"));

        $puntovtaid = '-1';
        if ($request->has('puntovtaid', 'post')) {
            //$view->set('puntovta', $request->get('puntovta', 'post'));
            $puntovtaid = $request->get('puntovtaid', 'post');
        } else {
            //$view->set('puntovta', $request->get('puntovta', 'get'));
            $puntovtaid = $request->get('puntovtaid', 'get');
        }

        $puntovta_info = $database->getRow("SELECT * FROM puntosdeventa WHERE puntovta = '" . $puntovtaid . "'");
        // </editor-fold>

        set_time_limit(0);

        try {
            $msg = '';

            // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">

            $nombre_de_las_columnas = array("TIPO", "MARCA", "MODELO", "GAMA", "COD INTERNO", "CANTIDAD");

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
            $sql = "SELECT desctipoproducto,descmarca,descmodelo,desctipogama,codbarrainterno, SUM(cantidad) AS total FROM vw_grilla_stockactual  ";
            $conca = " WHERE ";

            if ($tipoproducto != '-1' && $tipoproducto != '') {
                $sql .= $conca . " tipoproducto = '" . $tipoproducto . "'  ";
                $conca = " AND ";
            }

            if ($puntovtaid != '-1' && $puntovtaid != '') {
                $sql .= $conca . " puntovta = '" . $puntovtaid . "'  ";
                $conca = " AND ";
            }

            if ($articuloID != null && $articuloID != '') {
                $sql .= $conca . " articulo = '" . $articuloID . "'  ";
                $conca = " AND ";
            }

            $sql .= " GROUP BY articulo ASC";

            $sql .= " ORDER BY desctipoproducto, descmarca, descmodelo ASC";

            $results = $database->getRows($sql);

            //LE METO EN ELCABEZADO
            array_unshift($results, $nombre_de_las_columnas);
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="TABLA SELECCIONADA A CSV">
            $desc = str_replace(array("\'", '´', ',', '.', '{', '}', '+', '´', '\'', '*', '¨', '[', ']', '%', '&', '/', '%', '\$', '#', '"', '!', '?', '¡'), '', utf8_encode($puntovta_info['descripcion']));
            $nombreArchivo = "StockActual - " . $desc . ".csv";

            //cabeceras para descarga
            header('Content-Type: application/octet-stream');
            header("Content-Transfer-Encoding: Binary");
            header('Content-disposition: attachment; filename=' . $nombreArchivo);

            //preparar el wrapper de salida
            $outputBuffer = fopen("php://output", 'w');

            //volcamos el contenido del array en formato csv
            foreach ($results as $val) {
                //fputcsv($outputBuffer, $val);
                fputcsv($outputBuffer, $val, ";");
            }
            //cerramos el wrapper
            fclose($outputBuffer);
            exit;

            // </editor-fold>
        } catch (Exception $e) {
            $msg .= date('c') . "\r\n \t CATCH:\t " . $e . " \r\n";
            $common->log_msg($msg, 'excel_log');
        }
        //$view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
    }

    function exportxlsdataXLSPuntoVtaDetalle() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $common = & $this->locator->get('common');
        // </editor-fold>

        set_time_limit(0);

        try {
            $msg = '';

            // <editor-fold defaultstate="collapsed" desc="VARIABLES">
            $primeravez = '0';
            if ($request->isPost()) {
                $primeravez = '1';
            }

            if ($request->has('primeravez', 'post')) {
                $primeravez = '1';
            } else {
                if ($request->has('primeravez', 'get')) {
                    //$view->set('primeravez', $request->get('primeravez', 'get'));
                    $primeravez = $request->get('primeravez', 'get');
                }
            }

            $agrupadopor = '0';
            if ($request->has('agruparpor', 'post')) {
                //$view->set('agruparpor', $request->get('agruparpor', 'post'));
                $agrupadopor = $request->get('agruparpor', 'post');
            } else {
                //$view->set('agruparpor', $request->get('agruparpor', 'get'));
                $agrupadopor = $request->get('agruparpor', 'get');
            }

            $articuloID = "";
            if ($request->has('auto_articulo_stock', 'post')) {
                //$view->set('auto_articulo_stock', $request->get('auto_articulo_stock', 'post'));
                $articuloID = $request->get('auto_articulo_stock', 'post');
            } else {
                //$view->set('auto_articulo_stock', $request->get('auto_articulo_stock', 'get'));
                $articuloID = $request->get('auto_articulo_stock', 'get');
            }

            if ($request->has('auto_articulo', 'post')) {
                //$view->set('auto_articulo', $request->get('auto_articulo', 'post'));
            } else {
                if ($articuloID != "" && $articuloID != null) {
                    $sql = "SELECT articulo, concat(marca,' - ',descmodelo, ' ' ,if(codbarrainterno!='',concat('Cod: ',codbarrainterno), articulo)) as value "
                            . "FROM vw_grilla_articulos  "
                            . "WHERE articulo = '?'  ";
                    $articulo_info = $database->getRow($database->parse($sql, $articuloID));
                    //$view->set('auto_articulo', $articulo_info['value']);
                } else {
                    //$view->set('auto_articulo', '');
                }
            }
            //$view->set('script_busca_articulo', $url->rawssl('stockactual', 'getArticulos'));

            $tipoproducto = '-1';
            if ($request->has('tipoproducto', 'post')) {
                //$view->set('tipoproducto', $request->get('tipoproducto', 'post'));
                $tipoproducto = $request->get('tipoproducto', 'post');
            } else {
                //$view->set('tipoproducto', $request->get('tipoproducto', 'get'));
                $tipoproducto = $request->get('tipoproducto', 'get');
            }
            //$view->set('tiposproducto', $database->getRows("SELECT * FROM tiposproducto ORDER BY descripcion ASC"));

            $puntovtaid = '-1';
            if ($request->has('puntovtaid', 'post')) {
                //$view->set('puntovta', $request->get('puntovta', 'post'));
                $puntovtaid = $request->get('puntovtaid', 'post');
            } else {
                //$view->set('puntovta', $request->get('puntovta', 'get'));
                $puntovtaid = $request->get('puntovtaid', 'get');
            }
            //$view->set('puntosdeventa', $database->getRows("SELECT * FROM puntosdeventa ORDER BY descripcion ASC"));
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="HOJA UNO">
            // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">
            //** PHPExcel **//
            require_once (DIR_INCLUDES . '/library/phpexcel/PHPExcel.php');

            date_default_timezone_set('America/Argentina/Buenos_Aires');
            // Create new PHPExcel object
            $objPHPExcel = new PHPExcel();

            // Set properties of the file
            $objPHPExcel->getProperties()->setCreator("Accesorios")
                    ->setLastModifiedBy("Accesorios")
                    ->setTitle("Planilla web exportada: " . date('d-m-Y'))
                    ->setSubject("Planilla web exportada: " . date('d-m-Y'));


            //$nombre_de_las_columnas = array("TIPO", "MARCA", "MODELO", "GAMA", "COD INTERNO", "CANTIDAD");
            //genero las columnas del excel
            $letra = 'A';
            //$letra_fin = $this->devuelveLetra($cant_columnas - 1) ;
            $fila_inicial = 2; //fila desde donde empiezo a cargar la tabla en el excel. Arriba va el encabezado
            //$letra_numero_fin = $letra_fin . $cant_columnas;
            // <editor-fold defaultstate="collapsed" desc="PROPIEDADES DE LAS COLUMNAS VENTAS" >
            //$funciona = 'A1:'.$letra_fin . '1';
            $objPHPExcel->getActiveSheet()->getStyle('A1:B1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

            $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

            //COLUMNA A
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'TIPO');
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA B
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'MARCA');
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA C
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', 'MODELO');
            $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA D
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', 'GAMA');
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA E
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', 'COD INTERNO');
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA F
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', 'CANTIDAD');
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            // </editor-fold>
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
            $sql = "SELECT desctipoproducto,descmarca,descmodelo,desctipogama,codbarrainterno, SUM(cantidad) AS total FROM vw_grilla_stockactual  ";
            $conca = " WHERE ";

            if ($tipoproducto != '-1' && $tipoproducto != '') {
                $sql .= $conca . " tipoproducto = '" . $tipoproducto . "'  ";
                $conca = " AND ";
            }

            if ($puntovtaid != '-1' && $puntovtaid != '') {
                $sql .= $conca . " puntovta = '" . $puntovtaid . "'  ";
                $conca = " AND ";
            }

            if ($articuloID != null && $articuloID != '') {
                $sql .= $conca . " articulo = '" . $articuloID . "'  ";
                $conca = " AND ";
            }

            $sql .= " GROUP BY articulo ASC";

            $sql .= " ORDER BY desctipoproducto, descmarca, descmodelo ASC";

            $results = $database->getRows($sql);


            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="LLANAR LOS DATOS VENTAS">

            $num = $fila_inicial;

            foreach ($results as $result) {

                //COLUMNA A
                $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $num, $result['desctipoproducto']);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //COLUMNA B
                $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $num, $result['descmarca']);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //HACE QUE EL NUMERO DE VENTA SALGA EN FORMATO CORRECTO
                $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getNumberFormat()->setFormatCode('0000');

                //COLUMNA C
                $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $num, $result['descmodelo']);
                $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                //COLUMNA D
                $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' . $num, $result['desctipogama']);
                $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                //COLUMNA E
                $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' . $num, $result['codbarrainterno']);
                $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //COLUMNA F
                $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' . $num, $result['total']);
                $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);


                $num++;
            }

            // </editor-fold>
            //Cargo los datos celda por celda
            // Nombre de la hoja del libro
            $objPHPExcel->getActiveSheet()->setTitle('stockactual');

            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $objPHPExcel->setActiveSheetIndex(0);

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="HOJA DOS">
            // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">
            //** PHPExcel **//
            $positionInExcel = 1; //esto es para que ponga la nueva pestaña al principio

            $objPHPExcel->createSheet($positionInExcel); //creamos la pestaña

            $objPHPExcel->setActiveSheetIndex(1);

            $objPHPExcel->getActiveSheet()->setTitle('Articulos seriados');


            //$nombre_de_las_columnas = array("TIPO", "MARCA", "MODELO", "GAMA", "COD INTERNO", "CANTIDAD");
            //genero las columnas del excel
            $letra = 'A';
            //$letra_fin = $this->devuelveLetra($cant_columnas - 1) ;
            $fila_inicial = 2; //fila desde donde empiezo a cargar la tabla en el excel. Arriba va el encabezado
            //$letra_numero_fin = $letra_fin . $cant_columnas;
            // <editor-fold defaultstate="collapsed" desc="PROPIEDADES DE LAS COLUMNAS VENTAS" >
            //$funciona = 'A1:'.$letra_fin . '1';
            $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

            //COLUMNA A
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('A1', 'TIPO');
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA B
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('B1', 'MARCA');
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA C
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('C1', 'MODELO');
            $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA D
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('D1', 'GAMA');
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA E
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('E1', 'COD INTERNO');
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA F
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('F1', 'COD BARRAS');
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

            //COLUMNA G
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('G1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('G1', 'PUNTO VTA');
            $objPHPExcel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            // </editor-fold>
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">

            $sql = "SELECT desctipoproducto,descmarca,descmodelo,desctipogama,codbarrainterno, codbarra, descpuntovta FROM vw_grilla_stockactualbarras  ";
            $conca = " WHERE ";

            if ($tipoproducto != '-1' && $tipoproducto != '') {
                $sql .= $conca . " tipoproducto = '" . $tipoproducto . "'  ";
                $conca = " AND ";
            }

            if ($puntovtaid != '-1' && $puntovtaid != '') {
                $sql .= $conca . " puntovta = '" . $puntovtaid . "'  ";
                $conca = " AND ";
            }

//        if ($articuloID != null && $articuloID != '') {
            //$sql .= $conca . " estadoarticulo = 'DI'  ";
//            $conca = " AND ";
//        }
            $sql .= $conca . " estadoarticulo = 'DI'  ";
            //$sql .= " GROUP BY puntovta ASC";

            $sql .= " ORDER BY descpuntovta ASC";


            $results = $database->getRows($sql);


            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="LLANAR LOS DATOS VENTAS">



            $num = $fila_inicial;

            foreach ($results as $result) {

                //COLUMNA A
                $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('A' . $num, $result['desctipoproducto']);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //COLUMNA B
                $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('B' . $num, $result['descmarca']);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //HACE QUE EL NUMERO DE VENTA SALGA EN FORMATO CORRECTO
                $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getNumberFormat()->setFormatCode('0000');

                //COLUMNA C
                $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('C' . $num, $result['descmodelo']);
                $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                //COLUMNA D
                $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('D' . $num, $result['desctipogama']);
                $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                //COLUMNA E
                $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('E' . $num, $result['codbarrainterno']);
                $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //COLUMNA F
                $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('F' . $num, $result['codbarra']);
                $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //HACE QUE EL NUMERO DE VENTA SALGA EN FORMATO CORRECTO
                $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getNumberFormat()->setFormatCode('0000');

                //COLUMNA G
                $objPHPExcel->getActiveSheet()->getStyle('G' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('G' . $num, $result['descpuntovta']);
                $objPHPExcel->getActiveSheet()->getStyle('G' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                $num++;
            }


            // </editor-fold>
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="EXPORT EXCEL">
            //** PHPExcel **//
            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $objPHPExcel->setActiveSheetIndex(0);

            // Save Excel 2007 file
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

            // We'll be outputting an excel file
            header('Content-type: application/vnd.ms-excel');

            $fecha_d = date(" Y m d");
            $nombre_archivo = 'stockactual ' . $fecha_d . '.xls';
            // It will be called file.xls | esta linea hace que sea una descarga y no una pestaña aparte
            header('Content-Disposition: attachment; filename=' . $nombre_archivo);

            header("Expires: 0");

            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

            // Write file to the browser
            //$objWriter->save($nombre_archivo); 
            // $objWriter->save('Archivos/exportatablaxls/' . $nombre_archivo);
            // Write file to the browser
            $objWriter->save('php://output');

            $common->log_msg($msg, 'excel_log');
            //$objPHPExcel->
            //$objWriter->close();
            //$objPHPExcel->disconnectWorksheets();
            //unset($objWriter, $objPHPExcel);
            // </editor-fold>
        } catch (Exception $e) {
            $msg .= date('c') . "\r\n \t CATCH:\t " . $e . " \r\n";
            $common->log_msg($msg, 'excel_log');
        }
        //$view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
    }

    function exportxlsdataCSVProductoDetalle() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $common = & $this->locator->get('common');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VARIABLES">
        $primeravez = '0';
        if ($request->isPost()) {
            $primeravez = '1';
        }

        if ($request->has('primeravez', 'post')) {
            $primeravez = '1';
        } else {
            if ($request->has('primeravez', 'get')) {
                //$view->set('primeravez', $request->get('primeravez', 'get'));
                $primeravez = $request->get('primeravez', 'get');
            }
        }

        $agrupadopor = '0';
        if ($request->has('agruparpor', 'post')) {
            //$view->set('agruparpor', $request->get('agruparpor', 'post'));
            $agrupadopor = $request->get('agruparpor', 'post');
        } else {
            //$view->set('agruparpor', $request->get('agruparpor', 'get'));
            $agrupadopor = $request->get('agruparpor', 'get');
        }

        $articuloID = "";
        if ($request->has('auto_articulo_stock', 'post')) {
            //$view->set('auto_articulo_stock', $request->get('auto_articulo_stock', 'post'));
            $articuloID = $request->get('auto_articulo_stock', 'post');
        } else {
            //$view->set('auto_articulo_stock', $request->get('auto_articulo_stock', 'get'));
            $articuloID = $request->get('auto_articulo_stock', 'get');
        }

        if ($request->has('auto_articulo', 'post')) {
            //$view->set('auto_articulo', $request->get('auto_articulo', 'post'));
        } else {
            if ($articuloID != "" && $articuloID != null) {
                $sql = "SELECT articulo, concat(marca,' - ',descmodelo, ' ' ,if(codbarrainterno!='',concat('Cod: ',codbarrainterno), articulo)) as value "
                        . "FROM vw_grilla_articulos  "
                        . "WHERE articulo = '?'  ";
                $articulo_info = $database->getRow($database->parse($sql, $articuloID));
                //$view->set('auto_articulo', $articulo_info['value']);
            } else {
                //$view->set('auto_articulo', '');
            }
        }
        //$view->set('script_busca_articulo', $url->rawssl('stockactual', 'getArticulos'));

        $tipoproducto = '-1';
        if ($request->has('tipoproducto', 'post')) {
            //$view->set('tipoproducto', $request->get('tipoproducto', 'post'));
            $tipoproducto = $request->get('tipoproducto', 'post');
        } else {
            //$view->set('tipoproducto', $request->get('tipoproducto', 'get'));
            $tipoproducto = $request->get('tipoproducto', 'get');
        }
        //$view->set('tiposproducto', $database->getRows("SELECT * FROM tiposproducto ORDER BY descripcion ASC"));

        $puntovta = '-1';
        if ($request->has('puntovta', 'post')) {
            //$view->set('puntovta', $request->get('puntovta', 'post'));
            $puntovta = $request->get('puntovta', 'post');
        } else {
            //$view->set('puntovta', $request->get('puntovta', 'get'));
            $puntovta = $request->get('puntovta', 'get');
        }

        $articuloid = '';
        if ($request->has('articuloid', 'post')) {
            //$view->set('puntovta', $request->get('puntovta', 'post'));
            $articuloid = $request->get('articuloid', 'post');
        } else {
            //$view->set('puntovta', $request->get('puntovta', 'get'));
            $articuloid = $request->get('articuloid', 'get');
        }
        $sql = "select articulo, concat(marca,' - ',descmodelo, ' ' ,if(codbarrainterno!='',concat('Cod: ',codbarrainterno), articulo)) as value "
                . "FROM vw_grilla_articulos  "
                . "WHERE articulo = '?'  ";
        $articulo_info = $database->getRow($database->parse($sql, $articuloid));
        //$view->set('puntosdeventa', $database->getRows("SELECT * FROM puntosdeventa ORDER BY descripcion ASC"));
        // </editor-fold>

        set_time_limit(0);

        try {
            $msg = '';

            // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">

            $nombre_de_las_columnas = array("PUNTO DE VENTA", "CANTIDAD");

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">

            $sql = "SELECT descpuntovta, cantidad FROM vw_grilla_stockactual  ";
            $conca = " WHERE ";

            if ($tipoproducto != '-1' && $tipoproducto != '') {
                $sql .= $conca . " tipoproducto = '" . $tipoproducto . "'  ";
                $conca = " AND ";
            }

            if ($puntovta != '-1' && $puntovta != '') {
                $sql .= $conca . " puntovta = '" . $puntovta . "'  ";
                $conca = " AND ";
            }

            if ($articuloid != null && $articuloid != '') {
                $sql .= $conca . " articulo = '" . $articuloid . "'  ";
                $conca = " AND ";
            }

            $sql .= " GROUP BY puntovta ASC";

            $sql .= " ORDER BY descpuntovta ASC";


            $results = $database->getRows($sql);

            //LE METO EN ELCABEZADO
            array_unshift($results, $nombre_de_las_columnas);
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="TABLA SELECCIONADA A CSV">
            $desc = str_replace(array("\'", '´', ',', '.', '{', '}', '+', '´', '\'', '*', '¨', '[', ']', '%', '&', '/', '%', '\$', '#', '"', '!', '?', '¡'), '', utf8_encode($articulo_info['value']));
            $nombreArchivo = "StockActual - " . $desc . ".csv";

            //cabeceras para descarga
            header('Content-Type: application/octet-stream');
            header("Content-Transfer-Encoding: Binary");
            header('Content-disposition: attachment; filename=' . $nombreArchivo);

            //preparar el wrapper de salida
            $outputBuffer = fopen("php://output", 'w');

            //volcamos el contenido del array en formato csv
            foreach ($results as $val) {
                //fputcsv($outputBuffer, $val);
                fputcsv($outputBuffer, $val, ";");
            }
            //cerramos el wrapper
            fclose($outputBuffer);
            exit;

            // </editor-fold>
        } catch (Exception $e) {
            $msg .= date('c') . "\r\n \t CATCH:\t " . $e . " \r\n";
            $common->log_msg($msg, 'excel_log');
        }
        //$view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
    }

    function exportxlsdataXLSProductoDetalle() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $common = & $this->locator->get('common');
        // </editor-fold>

        set_time_limit(0);

        try {
            $msg = '';

            // <editor-fold defaultstate="collapsed" desc="VARIABLES">
            $primeravez = '0';
            if ($request->isPost()) {
                $primeravez = '1';
            }

            if ($request->has('primeravez', 'post')) {
                $primeravez = '1';
            } else {
                if ($request->has('primeravez', 'get')) {
                    //$view->set('primeravez', $request->get('primeravez', 'get'));
                    $primeravez = $request->get('primeravez', 'get');
                }
            }

            $agrupadopor = '0';
            if ($request->has('agruparpor', 'post')) {
                //$view->set('agruparpor', $request->get('agruparpor', 'post'));
                $agrupadopor = $request->get('agruparpor', 'post');
            } else {
                //$view->set('agruparpor', $request->get('agruparpor', 'get'));
                $agrupadopor = $request->get('agruparpor', 'get');
            }

            $articuloID = "";
            if ($request->has('auto_articulo_stock', 'post')) {
                //$view->set('auto_articulo_stock', $request->get('auto_articulo_stock', 'post'));
                $articuloID = $request->get('auto_articulo_stock', 'post');
            } else {
                //$view->set('auto_articulo_stock', $request->get('auto_articulo_stock', 'get'));
                $articuloID = $request->get('auto_articulo_stock', 'get');
            }

            $tipoproducto = '-1';
            if ($request->has('tipoproducto', 'post')) {
                //$view->set('tipoproducto', $request->get('tipoproducto', 'post'));
                $tipoproducto = $request->get('tipoproducto', 'post');
            } else {
                //$view->set('tipoproducto', $request->get('tipoproducto', 'get'));
                $tipoproducto = $request->get('tipoproducto', 'get');
            }
            //$view->set('tiposproducto', $database->getRows("SELECT * FROM tiposproducto ORDER BY descripcion ASC"));

            $puntovtaid = '-1';
            if ($request->has('puntovtaid', 'post')) {
                //$view->set('puntovta', $request->get('puntovta', 'post'));
                $puntovtaid = $request->get('puntovtaid', 'post');
            } else {
                //$view->set('puntovta', $request->get('puntovta', 'get'));
                $puntovtaid = $request->get('puntovtaid', 'get');
            }

            $articuloid = '';
            if ($request->has('articuloid', 'post')) {
                //$view->set('articuloid', $request->get('articuloid', 'post'));
                $articuloid = $request->get('articuloid', 'post');
            } else {
                //$view->set('articuloid', $request->get('articuloid', 'get'));
                $articuloid = $request->get('articuloid', 'get');
            }
            //$view->set('puntosdeventa', $database->getRows("SELECT * FROM puntosdeventa ORDER BY descripcion ASC"));
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="HOJA UNO">
            // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">
            //** PHPExcel **//
            require_once (DIR_INCLUDES . '/library/phpexcel/PHPExcel.php');

            date_default_timezone_set('America/Argentina/Buenos_Aires');
            // Create new PHPExcel object
            $objPHPExcel = new PHPExcel();

            // Set properties of the file
            $objPHPExcel->getProperties()->setCreator("Accesorios")
                    ->setLastModifiedBy("Accesorios")
                    ->setTitle("Planilla web exportada: " . date('d-m-Y'))
                    ->setSubject("Planilla web exportada: " . date('d-m-Y'));


            //$nombre_de_las_columnas = array("TIPO", "MARCA", "MODELO", "GAMA", "COD INTERNO", "CANTIDAD");
            //genero las columnas del excel
            $letra = 'A';
            //$letra_fin = $this->devuelveLetra($cant_columnas - 1) ;
            $fila_inicial = 2; //fila desde donde empiezo a cargar la tabla en el excel. Arriba va el encabezado
            //$letra_numero_fin = $letra_fin . $cant_columnas;
            // <editor-fold defaultstate="collapsed" desc="PROPIEDADES DE LAS COLUMNAS VENTAS" >
            //$funciona = 'A1:'.$letra_fin . '1';
            $objPHPExcel->getActiveSheet()->getStyle('A1:B1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

            //COLUMNA A
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'PUNTO VTA');
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA B
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'CANTIDAD');
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            // </editor-fold>   
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">


            $sql = "SELECT descpuntovta, cantidad FROM vw_grilla_stockactual  ";
            $conca = " WHERE ";

            if ($tipoproducto != '-1' && $tipoproducto != '') {
                $sql .= $conca . " tipoproducto = '" . $tipoproducto . "'  ";
                $conca = " AND ";
            }

            if ($puntovtaid != '-1' && $puntovtaid != '') {
                $sql .= $conca . " puntovta = '" . $puntovtaid . "'  ";
                $conca = " AND ";
            }

            if ($articuloid != null && $articuloid != '') {
                $sql .= $conca . " articulo = '" . $articuloid . "'  ";
                $conca = " AND ";
            }

            $sql .= " GROUP BY puntovta ASC";

            $sql .= " ORDER BY descpuntovta ASC";


            $results = $database->getRows($sql);

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="LLANAR LOS DATOS VENTAS">

            $num = $fila_inicial;

            foreach ($results as $result) {

                //COLUMNA A
                $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $num, $result['descpuntovta']);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //COLUMNA B
                $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $num, $result['cantidad']);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //HACE QUE EL NUMERO DE VENTA SALGA EN FORMATO CORRECTO
                //$objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getNumberFormat()->setFormatCode('0000');

                $num++;
            }

            // </editor-fold>
            //Cargo los datos celda por celda
            // Nombre de la hoja del libro
            $objPHPExcel->getActiveSheet()->setTitle('stockactual');

            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $objPHPExcel->setActiveSheetIndex(0);

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="HOJA DOS">
            // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">
            //** PHPExcel **//
            $positionInExcel = 1; //esto es para que ponga la nueva pestaña al principio

            $objPHPExcel->createSheet($positionInExcel); //creamos la pestaña

            $objPHPExcel->setActiveSheetIndex(1);

            $objPHPExcel->getActiveSheet()->setTitle('Articulos seriados');


            //$nombre_de_las_columnas = array("TIPO", "MARCA", "MODELO", "GAMA", "COD INTERNO", "CANTIDAD");
            //genero las columnas del excel
            $letra = 'A';
            //$letra_fin = $this->devuelveLetra($cant_columnas - 1) ;
            $fila_inicial = 2; //fila desde donde empiezo a cargar la tabla en el excel. Arriba va el encabezado
            //$letra_numero_fin = $letra_fin . $cant_columnas;
            // <editor-fold defaultstate="collapsed" desc="PROPIEDADES DE LAS COLUMNAS VENTAS" >
            //$funciona = 'A1:'.$letra_fin . '1';
            $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

            //COLUMNA A
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('A1', 'TIPO');
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA B
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('B1', 'MARCA');
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA C
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('C1', 'MODELO');
            $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA D
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('D1', 'GAMA');
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA E
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('E1', 'COD INTERNO');
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            //COLUMNA F
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('F1', 'COD BARRAS');
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);

            //COLUMNA G
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getStyle('G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle('G1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('G1', 'PUNTO VTA');
            $objPHPExcel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true)->setName('Verdana')->setSize(8);
            // </editor-fold>
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">

            $sql = "SELECT desctipoproducto,descmarca,descmodelo,desctipogama,codbarrainterno, codbarra, descpuntovta FROM vw_grilla_stockactualbarras  ";
            $conca = " WHERE ";

            if ($tipoproducto != '-1' && $tipoproducto != '') {
                $sql .= $conca . " tipoproducto = '" . $tipoproducto . "'  ";
                $conca = " AND ";
            }

            if ($puntovtaid != '-1' && $puntovtaid != '') {
                $sql .= $conca . " puntovta = '" . $puntovtaid . "'  ";
                $conca = " AND ";
            }

            if ($articuloid != null && $articuloid != '') {
                $sql .= $conca . " articulo = '" . $articuloid . "'  ";
                $conca = " AND ";
            }
//        if ($articuloID != null && $articuloID != '') {
            //$sql .= $conca . " estadoarticulo = 'DI'  ";
//            $conca = " AND ";
//        }
            $sql .= $conca . " estadoarticulo = 'DI'  ";
            //$sql .= " GROUP BY puntovta ASC";

            $sql .= " ORDER BY descpuntovta ASC";


            $results = $database->getRows($sql);


            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="LLANAR LOS DATOS VENTAS">



            $num = $fila_inicial;

            foreach ($results as $result) {

                //COLUMNA A
                $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('A' . $num, $result['desctipoproducto']);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //COLUMNA B
                $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('B' . $num, $result['descmarca']);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //HACE QUE EL NUMERO DE VENTA SALGA EN FORMATO CORRECTO
                $objPHPExcel->getActiveSheet()->getStyle('B' . $num)->getNumberFormat()->setFormatCode('0000');

                //COLUMNA C
                $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('C' . $num, $result['descmodelo']);
                $objPHPExcel->getActiveSheet()->getStyle('C' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                //COLUMNA D
                $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('D' . $num, $result['desctipogama']);
                $objPHPExcel->getActiveSheet()->getStyle('D' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                //COLUMNA E
                $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('E' . $num, $result['codbarrainterno']);
                $objPHPExcel->getActiveSheet()->getStyle('E' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //COLUMNA F
                $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('F' . $num, $result['codbarra']);
                $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //HACE QUE EL NUMERO DE VENTA SALGA EN FORMATO CORRECTO
                $objPHPExcel->getActiveSheet()->getStyle('F' . $num)->getNumberFormat()->setFormatCode('0000');

                //COLUMNA G
                $objPHPExcel->getActiveSheet()->getStyle('G' . $num)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->setActiveSheetIndex(1)->setCellValue('G' . $num, $result['descpuntovta']);
                $objPHPExcel->getActiveSheet()->getStyle('G' . $num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                $num++;
            }


            // </editor-fold>
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="EXPORT EXCEL">
            //** PHPExcel **//
            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $objPHPExcel->setActiveSheetIndex(0);

            // Save Excel 2007 file
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

            // We'll be outputting an excel file
            header('Content-type: application/vnd.ms-excel');

            $fecha_d = date(" Y m d");
            $nombre_archivo = 'stockactual ' . $fecha_d . '.xls';
            // It will be called file.xls | esta linea hace que sea una descarga y no una pestaña aparte
            header('Content-Disposition: attachment; filename=' . $nombre_archivo);

            header("Expires: 0");

            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

            // Write file to the browser
            //$objWriter->save($nombre_archivo); 
            // $objWriter->save('Archivos/exportatablaxls/' . $nombre_archivo);
            // Write file to the browser
            $objWriter->save('php://output');

            $common->log_msg($msg, 'excel_log');
            //$objPHPExcel->
            //$objWriter->close();
            //$objPHPExcel->disconnectWorksheets();
            //unset($objWriter, $objPHPExcel);
            // </editor-fold>
        } catch (Exception $e) {
            $msg .= date('c') . "\r\n \t CATCH:\t " . $e . " \r\n";
            $common->log_msg($msg, 'excel_log');
        }
        //$view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
    }

    function devuelveLetra($num) {
        $letra = '';
        switch ($num) {
            case 1:$letra = 'A';
                break;
            case 2:$letra = 'B';
                break;
            case 3:$letra = 'C';
                break;
            case 4:$letra = 'D';
                break;
            case 5:$letra = 'E';
                break;
            case 6:$letra = 'F';
                break;
            case 7:$letra = 'G';
                break;
            case 8:$letra = 'H';
                break;
            case 9:$letra = 'I';
                break;
            case 10:$letra = 'J';
                break;
            case 11:$letra = 'K';
                break;
            case 12:$letra = 'L';
                break;
            case 13:$letra = 'M';
                break;
            case 14:$letra = 'N';
                break;
            case 15:$letra = 'O';
                break;
            case 16:$letra = 'P';
                break;
            case 17:$letra = 'Q';
                break;
            case 18:$letra = 'R';
                break;
            case 19:$letra = 'S';
                break;
            case 20:$letra = 'T';
                break;
            case 21:$letra = 'U';
                break;
            case 22:$letra = 'V';
                break;
            case 23:$letra = 'W';
                break;
            case 24:$letra = 'X';
                break;
            case 25:$letra = 'Y';
                break;
            case 26:$letra = 'Z';
                break;
            case 27:$letra = 'AA';
                break;
            case 28:$letra = 'AB';
                break;
            case 29:$letra = 'AC';
                break;
            case 30:$letra = 'AD';
                break;
            case 31:$letra = 'AE';
                break;
            case 32:$letra = 'AF';
                break;
            case 33:$letra = 'AG';
                break;
            case 34:$letra = 'AH';
                break;
            case 35:$letra = 'AI';
                break;
            case 36:$letra = 'AJ';
                break;
            case 37:$letra = 'AK';
                break;
            case 38:$letra = 'AL';
                break;
            case 39:$letra = 'AM';
                break;
            case 40:$letra = 'AN';
                break;
            case 41:$letra = 'AO';
                break;
            case 42:$letra = 'AP';
                break;
            case 43:$letra = 'AQ';
                break;
            case 44:$letra = 'AR';
                break;
            case 45:$letra = 'AS';
                break;
            case 46:$letra = 'AT';
                break;
            case 47:$letra = 'AU';
                break;
            case 48:$letra = 'AV';
                break;
            case 49:$letra = 'AW';
                break;
            case 50:$letra = 'AX';
                break;
            case 51:$letra = 'AY';
                break;
            case 52:$letra = 'AZ';
                break;

            case 53:$letra = 'BA';
                break;
            case 54:$letra = 'BB';
                break;
            case 55:$letra = 'BC';
                break;
            case 56:$letra = 'BD';
                break;
            case 57:$letra = 'BE';
                break;
            case 58:$letra = 'BF';
                break;
            case 59:$letra = 'BG';
                break;
            case 60:$letra = 'BH';
                break;
            case 61:$letra = 'BI';
                break;
            case 62:$letra = 'BJ';
                break;
            case 63:$letra = 'BK';
                break;
            case 64:$letra = 'BL';
                break;
            case 65:$letra = 'BM';
                break;
            case 66:$letra = 'BN';
                break;
            case 67:$letra = 'BO';
                break;
            case 68:$letra = 'BP';
                break;
            case 69:$letra = 'BQ';
                break;
            case 70:$letra = 'BR';
                break;
            case 71:$letra = 'BS';
                break;
            case 72:$letra = 'BT';
                break;
            case 73:$letra = 'BU';
                break;
            case 74:$letra = 'BV';
                break;
            case 75:$letra = 'BW';
                break;
            case 76:$letra = 'BX';
                break;
            case 77:$letra = 'BY';
                break;
            case 78:$letra = 'BZ';
                break;

            case 79:$letra = 'CA';
                break;
            case 80:$letra = 'CB';
                break;
            case 81:$letra = 'CC';
                break;
            case 82:$letra = 'CD';
                break;
            case 83:$letra = 'CE';
                break;
            case 84:$letra = 'CF';
                break;
            case 85:$letra = 'CG';
                break;
            case 86:$letra = 'CH';
                break;
            case 87:$letra = 'CI';
                break;
            case 88:$letra = 'CJ';
                break;
            case 89:$letra = 'CK';
                break;
            case 90:$letra = 'CL';
                break;
            case 91:$letra = 'CM';
                break;
            case 92:$letra = 'CN';
                break;
            case 93:$letra = 'CO';
                break;
            case 94:$letra = 'CP';
                break;
            case 95:$letra = 'CQ';
                break;
            case 96:$letra = 'CR';
                break;
            case 97:$letra = 'CS';
                break;
            case 98:$letra = 'CT';
                break;
            case 99:$letra = 'CU';
                break;
            case 100:$letra = 'CV';
                break;
            case 101:$letra = 'CW';
                break;
            case 102:$letra = 'CX';
                break;
            case 103:$letra = 'CY';
                break;
            case 104:$letra = 'CZ';
                break;

            case 105:$letra = 'DA';
                break;
            case 106:$letra = 'DB';
                break;
            case 107:$letra = 'DC';
                break;
            case 108:$letra = 'DD';
                break;
            case 109:$letra = 'DE';
                break;
            case 110:$letra = 'DF';
                break;
            case 111:$letra = 'DG';
                break;
            case 112:$letra = 'DH';
                break;
            case 113:$letra = 'DI';
                break;
            case 114:$letra = 'DJ';
                break;
            case 115:$letra = 'DK';
                break;
            case 116:$letra = 'DL';
                break;
            case 117:$letra = 'DM';
                break;
            case 118:$letra = 'DN';
                break;
            case 119:$letra = 'DO';
                break;
            case 120:$letra = 'DP';
                break;
            case 121:$letra = 'DQ';
                break;
            case 122:$letra = 'DR';
                break;
            case 123:$letra = 'DS';
                break;
            case 124:$letra = 'DT';
                break;
            case 125:$letra = 'DU';
                break;
            case 126:$letra = 'DV';
                break;
            case 127:$letra = 'DW';
                break;
            case 128:$letra = 'DX';
                break;
            case 129:$letra = 'DY';
                break;
            case 130:$letra = 'DZ';
                break;

            case 131:$letra = 'EA';
                break;
            case 132:$letra = 'EB';
                break;
            case 133:$letra = 'EC';
                break;
            case 134:$letra = 'ED';
                break;
            case 135:$letra = 'EE';
                break;
            case 136:$letra = 'EF';
                break;
            case 137:$letra = 'EG';
                break;
            case 138:$letra = 'EH';
                break;
            case 139:$letra = 'EI';
                break;
            case 140:$letra = 'EJ';
                break;
            case 141:$letra = 'EK';
                break;
            case 142:$letra = 'EL';
                break;
            case 143:$letra = 'EM';
                break;
            case 144:$letra = 'EN';
                break;
            case 145:$letra = 'EO';
                break;

            default:
                break;
        }
        return $letra;
    }

    function getArticulos() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>

        $miDescripcion = $request->get('term', 'get');
        $listaprecio = $request->get('listaprecio', 'get');

        $sql = "SELECT DISTINCT if(a.codbarra!='',concat(a.articulo, '_',a.codbarra), a.articulo) as id, "
                . " concat(a.marca,' - ',a.descmodelo, ' ' ,if(a.codbarra is not null ,concat('Cod: ',a.codbarra), concat('Cod: ',a.codbarrainterno))) as value  "
                . "FROM articulospuntovta apv "
                . "LEFT JOIN vw_grilla_articulostodos a ON apv.articulo = a.articulo "
                . "LEFT JOIN listasdeprecioarticulos lp ON lp.articulo = a.articulo "
                . "WHERE apv.cantidad > 0 "
                . "AND (a.articulo LIKE '?' OR a.codbarrainterno LIKE '?' OR a.desctipoproducto LIKE '?' OR a.descmarca LIKE '?' OR a.descmodelo LIKE '?' OR a.codbarra like '?' ) "
                . "AND a.estadoarticulo = 'DI' "
                . "LIMIT 10 ";
        $consulta = $database->parse($sql, '%' . $miDescripcion . '%', '%' . $miDescripcion . '%', '%' . $miDescripcion . '%', '%' . $miDescripcion . '%', '%' . $miDescripcion . '%', '%' . $miDescripcion . '%');
        $codigo = $database->getRows($consulta);

        $varia = utf8_decode(json_encode($codigo));

        echo $varia;
    }

    function actualizaStockPuntovta()
    {
        $database = & $this->locator->get('database');
        set_time_limit(0);
        //set_time_limit(300);
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">

        $puntosdeventa = $database->getRows("SELECT * FROM puntosdeventa");
        $articulos = $database->getRows("SELECT * FROM articulos");
        foreach ($puntosdeventa as $pvta) {
            $puntovta = $pvta['puntovta'];
           foreach ($articulos as $articulo) {
            $articuloID = $articulo['articulo'];
            $sql = "SELECT * FROM vw_list_trazabilidad WHERE puntovta = '" . $puntovta . "' AND  articulo = '" . $articuloID . "'  ";
            $results = $database->getRows($sql);
            $entrada = 0;
            $salida = 0;
                foreach ($results as $result) {
                    switch (@$result['tipo']) {
                        case 'DUA INICIO': $entrada += @$result['cantidad'];break;
                        case 'DUA ENTRADA BODEGA CENTRAL': $entrada += @$result['cantidad'];break;
                        case 'DUA ENTRADA': $entrada += @$result['cantidad'];break;
                        case 'NOTA DE CREDITO': $entrada += @$result['cantidad'];break;
                        case 'DUA SALIDA': $salida += @$result['cantidad'];break;
                        case 'DUA ARQUEO': $salida += @$result['cantidad'];break;
                        case 'VENTA': $salida += @$result['cantidad'];break;
                        default:
                            break;
                    }
                }
                    
            $sql = "UPDATE articulospuntovta SET cantidad='?' "
                    . "WHERE articulo='?' AND puntovta='?'";
            $sql = $database->parse($sql, ($entrada - $salida), $articuloID, $puntovta);
            $database->query($sql);
         } 
        }
               
                // </editor-fold>
        
    }
    
    function actualizaStockPuntovta2()
    {
        $database = & $this->locator->get('database');
        //set_time_limit(0);
        set_time_limit(300);
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">

        //$puntosdeventa = $database->getRows("SELECT * FROM puntosdeventa");
        //$articulos = $database->getRows("SELECT * FROM articulos");
        //foreach ($puntosdeventa as $pvta) {
            $puntovta = 31;
           //foreach ($articulos as $articulo) {
            $articuloID = 30;
            $sql = "SELECT * FROM vw_list_trazabilidad WHERE puntovta = '" . $puntovta . "' AND  articulo = '" . $articuloID . "'  ";
            $results = $database->getRows($sql);
            $entrada = 0;
            $salida = 0;
                foreach ($results as $result) {
                    switch (@$result['tipo']) {
                        case 'DUA INICIO': $entrada += @$result['cantidad'];break;
                        case 'DUA ENTRADA BODEGA CENTRAL': $entrada += @$result['cantidad'];break;
                        case 'DUA ENTRADA': $entrada += @$result['cantidad'];break;
                        case 'NOTA DE CREDITO': $entrada += @$result['cantidad'];break;
                        case 'DUA SALIDA': $salida += @$result['cantidad'];break;
                        case 'DUA ARQUEO': $salida += @$result['cantidad'];break;
                        case 'VENTA': $salida += @$result['cantidad'];break;
                        default:
                            break;
                    }
                }
                    
            $sql = "UPDATE articulospuntovta SET cantidad='?' "
                    . "WHERE articulo='?' AND puntovta='?'";
            $sql = $database->parse($sql, ($entrada - $salida), $articuloID, $puntovta);
            $database->query($sql);
         //} 
        //}
               
                // </editor-fold>
        
    }
}

?>