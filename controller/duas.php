<?php

class ControllerDuas extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'DUA');

        $template->set('content', $this->getList());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getList() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $config = & $this->locator->get('config');
        $url = & $this->locator->get('url');
        $language = & $this->locator->get('language');
        $session = & $this->locator->get('session');
        $user = & $this->locator->get('user');
        $view = $this->locator->create('template');
        $template = & $this->locator->get('template');
        $cache = & $this->locator->get('cache');
        $request = & $this->locator->get('request');
        // </editor-fold>
         // <editor-fold defaultstate="collapsed" desc="LIMPIA FILTRO Y VARIBLES DE SESSION">

        if ($request->get('filtra') == NULL) {

            $session->set('duas.search', '');
            $session->set('duas.puntovta', '-1');
            $session->set('duas.desde', '');
            $session->set('duas.hasta', '');
        
            $session->set('duas.sort', '');
            $session->set('duas.order', '');
            $session->set('duas.page', '');

            $view->set('search', '');
            $view->set('duas.search', '');
            $view->set('duas.puntovta', '-1');
            $view->set('duas.desde', '');
            $view->set('duas.hasta', '');
            
            $cache->delete('duas');
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO GRILLA">
        $cols = array();


        $cols[] = array(
            'name' => 'Tipo',
            'sort' => 'tipo',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Nro',
            'sort' => 'numero',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Fecha ',
            'sort' => 'fecha',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Origen',
            'sort' => 'tipoentidadorigen',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Entidad Origen',
            'sort' => 'entidadorigen',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Destino',
            'sort' => 'tipoentidaddestino',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Entidad Destino',
            'sort' => 'entidaddestino',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Estado',
            'sort' => 'estado',
            'align' => 'left'
        );

        $cols[] = array(
            'name' => 'Acciones',
            'align' => 'center'
        );

        $sort = array(
            'dua',
            'fecha',
            'entidadorigen',
            'entidaddestino'
        );
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="FILTRO Y CONSULTA">
        if (!$session->get('duas.search')) {
            $sql = "SELECT * FROM vw_grilla_duas WHERE tipodua != 'A'  ";
            $conca = " AND ";
        } else {
            $sql = "SELECT * FROM vw_grilla_duas  WHERE tipodua != 'A' AND dua LIKE '%" . $session->get('duas.search') . "%'  ";
            $conca = " AND ";
        }

        //filtro por fecha
        // <editor-fold defaultstate="collapsed" desc="DESDE">
                $fecha_d = '';
                
                if ($session->get('duas.desde')) {
                    $dated = explode('/', $session->get('duas.desde'));
                    //$fecha_d = $dated[0] . '-' . $dated[1] . '-' . $dated[2];
                    $fecha_d = 	str_replace('/','-',$session->get('duas.desde'));
                }
                   
                
                if ($fecha_d != '') {
                    $sql .= $conca . " STR_TO_DATE(DATE_FORMAT(fecha, '%d-%m-%Y'), '%d-%m-%Y') >= STR_TO_DATE('" . $fecha_d . "', '%d-%m-%Y') ";
                    $conca = " AND ";
                }
                

                
                // </editor-fold>
            
        // <editor-fold defaultstate="collapsed" desc="HASTA">
                $fecha_h = '';
                
                if ($session->get('duas.hasta')) {
                    $dated = explode('/', $session->get('duas.hasta'));
                    //$fecha_d = $dated[0] . '-' . $dated[1] . '-' . $dated[2];
                    $fecha_h = 	str_replace('/','-',$session->get('duas.hasta'));
                }
                   
                
                if ($fecha_h != '') {
                    $sql .= $conca . " STR_TO_DATE(DATE_FORMAT(fecha, '%d-%m-%Y'), '%d-%m-%Y') <= STR_TO_DATE('" . $fecha_h . "', '%d-%m-%Y') ";
                    $conca = " AND ";
                }
                

                
                // </editor-fold>
            
        //SE FIJA SI ES EXCENTO MIRA SI SELECCIONO UN PUNTO DE VENTA
        //SI NO LO ES FIJA SI TIENE SELECCIONADO ALGUN PUNTO DE VENTA DE LOS QUE ESTA ASIGNADO
        //SINO BUSCA EN LOS PUNTO DE VENTA QUE ESTA ASIGNADO
        if ($user->getExento() == 1) {
            if ($session->get('duas.puntovta') != '-1') {
            $sql .= $conca . " entidaddestino = '" . $session->get('duas.puntovta') . "'  ";
            }
        } else {
            if ($session->get('duas.puntovta') != '-1') {
                $sql .= $conca . " entidaddestino = '" . $session->get('duas.puntovta') . "'  ";
            }
            else{
                $sql .= $conca . " entidaddestino = '" . $user->getPuntovtaasignado() . "'  ";
            }            
        }
        
        
                  
              
        if (in_array($session->get('duas.sort'), $sort)) {
            $sql .= " ORDER BY " . $session->get('duas.sort') . " " . (($session->get('duas.order') == 'DESC') ? 'DESC' : 'ASC');
        } else {
            $sql .= " ORDER BY dua DESC";
        }
         //$sql .= " ORDER BY dua DESC";
        
        //$consult = $database->parse($sql, '%' . $session->get('duas.search') . '%');
        $results = $database->getRows($sql);

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="GRILLA">
        $rows = array();

        foreach ($results as $result) {
            $cell = array();
            //tipo, nro, fecha, origen, enti origen, destino, entidad destino, estado

            $cell[] = array(
                'value' => @$result['desctipodua'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['dua'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => date('d/m/Y', strtotime(@$result['fecha'])),
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['tipoentidadorigen'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['ENTIDAD_ORG'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['tipoentidaddestino'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['ENTIDAD_DES'],
                'align' => 'left',
                'default' => 0
            );

            $cell[] = array(
                'value' => @$result['estadodua'],
                'align' => 'left',
                'default' => 0
            );

            $action = array();

            if ($user->hasPermisos($user->getPERSONA(), 'duas', 'M')) {
                $action[] = array(
                    'icon' => 'img/iconos-01.png',
                    'class' => 'fa fa-fw fa-pencil',
                    'text' => $language->get('button_update'),
                    'prop_a' => array('href' => $url->ssl('duas', 'update', array('dua' => $result['dua'])))
                );
            }

            if ($user->hasPermisos($user->getPERSONA(), 'duas', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw  fa-money',
                    'text' => 'Remito',
                    'target' => ' target="_blank" ',
                    'prop_a' => array('href' => $url->ssl('duas', 'RemitoPDF', array('dua' => $result['dua'])))
                );
            }

            if (@$result['estadodua'] == 'V') { //Puede anularla solamenete si esta vigente
                if ($user->hasPermisos($user->getPERSONA(), 'duas', 'B')) {
                    $action[] = array(
                        'icon' => 'img/iconos-11.png',
                        'text' => $language->get('button_delete'),
                        'class' => 'fa fa-fw fa-trash-o',
                        'prop_a' => array('href' => $url->ssl('duas', 'delete', array('dua' => $result['dua'])))
                    );
                }
            }

            if ($user->hasPermisos($user->getPERSONA(), 'duas', 'C')) {
                $action[] = array(
                    'icon' => 'img/iconos-17.png',
                    'class' => 'fa fa-fw fa-search',
                    'text' => $language->get('button_consult'),
                    'prop_a' => array('href' => $url->ssl('duas', 'consulta', array('dua' => $result['dua'])))
                );
            }

            $cell[] = array(
                'action' => $action,
                'align' => 'center'
            );

            $rows[] = array('cell' => $cell);
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES PAGINACION">
        $database->paginar($rows, $config->get('config_max_rows'), $session->get('duas.page'));


        $view->set('text_default', $language->get('text_default'));
        $view->set('text_results', $language->get('text_results', $database->getFrom(), $database->getTo(), $database->getTotal()));
        $view->set('pages_first', $database->getFirst());
        $view->set('pages_previous', $database->getPrevious());
        $view->set('pages_next', $database->getNext());
        $view->set('pages_last', $database->getLast());
        // </editor-fold>

//        if ($session->get('duas.dua')) {
//            $view->set('dua', $session->get('duas.dua'));
//        } else {
//            $view->set('dua', '');
//        }

        
        
        $view->set('heading_title', 'DUA');
        $view->set('heading_title2', 'Filtrar');
        $view->set('heading_description', 'Gesti&oacute;n de DUA');

        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/NOTICIAS.png');
        $view->set('placeholder_buscar', 'BUSCA POR NRO ');

        $view->set('entry_page', $language->get('entry_page'));
        $view->set('entry_search', $language->get('entry_search'));
        $view->set('button_search', $language->get('button_search'));

        $view->set('button_list', $language->get('button_list'));
        $view->set('button_insert', $language->get('button_insert'));
        $view->set('button_exportar', $language->get('button_exportar'));

        $view->set('mensaje_sin_filas', 'A&uacute;n no existen registros.');

        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        // Ahora (tb hay que agregar $template =& $this->locator->get('template'); arriba)

        $view->set('message', $session->get('message'));
        $session->delete('message');

        $view->set('search', $session->get('duas.search'));
        $view->set('puntovta', $session->get('duas.puntovta'));
        $view->set('desde', $session->get('duas.desde'));
        $view->set('hasta', $session->get('duas.hasta'));
        $view->set('sort', $session->get('duas.sort'));
        $view->set('order', $session->get('duas.order'));
        $view->set('page', $session->get('duas.page'));
        
        if ($user->getExento() == 1) {
            $view->set('puntosdeventa', $database->getRows("SELECT * FROM puntosdeventa ORDER BY descripcion ASC"));
        }
        else{
            $view->set('puntosdeventa', $database->getRows("SELECT * FROM vendedorespuntodeventa vpv LEFT JOIN puntosdeventa pv ON vpv.puntovta = pv.puntovta WHERE vpv.persona = ".$user->getPERSONA()." ORDER BY descripcion ASC"));
        }
        
        $view->set('cols', $cols);
        $view->set('rows', $rows);

        $mensaje = " ";
        $view->set('textMessageAyuda', $mensaje);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $view->set('action', $url->ssl('duas', 'page'));
        $view->set('list', $url->ssl('duas'));
        if ($user->hasPermisos($user->getPERSONA(), 'duas', 'A')) {
            $view->set('insert', $url->ssl('duas', 'insert'));
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PAGINACION">

        $page_data = array();

        for ($i = 1; $i <= $database->getPages(); $i++) {
            $page_data[] = array(
                'text' => $language->get('text_pages', $i, $database->getPages()),
                'value' => $i
            );
        }

        $view->set('pages', $page_data);
        // </editor-fold>

        return $view->fetch('content/list_duas.tpl');
    }

    function page() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $url = & $this->locator->get('url');
        $session = & $this->locator->get('session');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SESSION VARIABLES">

        if ($request->has('search', 'post')) {
            $session->set('duas.search', $request->get('search', 'post'));
        }

        if ($request->has('puntovta', 'post')) {
            $session->set('duas.puntovta', $request->get('puntovta', 'post'));
        }
        
        if ($request->has('desde', 'post')) {
            $session->set('duas.desde', $request->get('desde', 'post'));
        }
        
        if ($request->has('hasta', 'post')) {
            $session->set('duas.hasta', $request->get('hasta', 'post'));
        }
        
        if (($request->has('page', 'post')) || ($request->has('search', 'post'))) {
            $session->set('duas.page', $request->get('page', 'post'));
        }

        if ($request->has('sort', 'post')) {
            $session->set('duas.order', (($session->get('duas.sort') == $request->get('sort', 'post')) && ($session->get('duas.order') == 'ASC')) ? 'DESC' : 'ASC');
        }

        if ($request->has('sort', 'post')) {
            $session->set('duas.sort', $request->get('sort', 'post'));
        }
        // </editor-fold>

        $response->redirect($url->ssl('duas', 'index', array('filtra' => '1')));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $common = & $this->locator->get('common');
        $language = & $this->locator->get('language');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENTRY VARIABLES Y VIEW VARIABLES">
        $view->set('heading_title', 'DUA');
        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/user-8.png');

        $view->set('heading_description', 'Gesti&oacute;n de DUA');

        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        $view->set('tab_general', $language->get('tab_general'));

        if (($request->get('dua')) && (!$request->isPost())) {
            $consulta = "SELECT DISTINCT * FROM vw_grilla_duas WHERE dua = '?'";
            @$objeto_info = $database->getRow($database->parse($consulta, $request->get('dua')));
        }

        $tipoentidadorigen = @$objeto_info['tipoentidadorigen'];
        $tipoentidaddestino = @$objeto_info['tipoentidaddestino'];

        if ($request->has('dua', 'post')) {
            $view->set('dua', $request->get('dua', 'post'));
            $objeto_id = $request->get('dua', 'post');
        } else {
            if ($request->has('dua', 'get')) {
                $objeto_id = @$objeto_info['dua'];
            } else {
                $objeto_id = $common->GetaaaaMMddhhmmssmm();
            }
            $view->set('dua', $objeto_id);
        }

        $view->set('entry_fecha', 'Fecha:');
        if ($request->has('fecha', 'post')) {
            $view->set('fecha', $request->get('fecha', 'post'));
        } else {
            if ($request->has('fecha', 'get')) {
                $view->set('fecha', date('d/m/Y', strtotime(@$objeto_info['fecha'])));
            } else {
                $view->set('fecha', date('d/m/Y'));
            }
        }

        $view->set('entry_tipodua', 'Tipo:');
        if ($request->has('tipodua', 'post')) {
            $view->set('tipodua', $request->get('tipodua', 'post'));
        } else {
            $view->set('tipodua', @$objeto_info['tipodua']);
        }
        $view->set('tiposdua', $database->getRows("SELECT * FROM tiposdua WHERE estado = 1 ORDER BY descripcion ASC"));

        $view->set('entry_estadodua', 'Estado:');
        if ($request->get('action') == 'delete') {
            $view->set('estadodua', 'A');
        } else {
            if ($request->has('estadodua', 'post')) {
                $view->set('estadodua', $request->get('estadodua', 'post'));
            } else {
                $view->set('estadodua', @$objeto_info['estadodua']);
            }
        }

        $view->set('estadosdua', $database->getRows("SELECT * FROM estadosdua ORDER BY descripcion DESC"));

        $view->set('entry_entidadorigen', 'Entidad orígen:');
        $view->set('entry_entidaddestino', 'Entidad destino:');

        $consulta = "SELECT idconcat AS id, descripcion AS value FROM vw_list_entidad ";
        $view->set('entidades', $database->getRows($consulta));

        if ($request->has('auto_entidadorigen', 'post')) {
            $view->set('auto_entidadorigen', $request->get('auto_entidadorigen', 'post'));
            $view->set('auto_entidadorigen_dua', $request->get('auto_entidadorigen_dua', 'post'));
        } else {
            $entidadorigen = $database->getRow("SELECT * FROM vw_list_entidad "
                    . "WHERE id = '" . $objeto_info['entidadorigen'] . "' "
                    . "AND tipoentidad = '" . $tipoentidadorigen . "'");

            $view->set('auto_entidadorigen', @$entidadorigen['descripcion']);
            $view->set('auto_entidadorigen_dua', @$entidadorigen['idconcat']);
        }
        $view->set('script_busca_entidadorigen', $url->rawssl('duas', 'getEntidadOrigen'));

        if ($request->has('auto_entidaddestino', 'post')) {
            $view->set('auto_entidaddestino', $request->get('auto_entidaddestino', 'post'));
            $view->set('auto_entidaddestino_dua', $request->get('auto_entidaddestino_dua', 'post'));
        } else {
            $entidaddestino = $database->getRow("SELECT * FROM vw_list_entidad "
                    . "WHERE id = '" . $objeto_info['entidaddestino'] . "' "
                    . "AND tipoentidad = '" . $tipoentidaddestino . "'");

            $view->set('auto_entidaddestino', @$entidaddestino['descripcion']);
            $view->set('auto_entidaddestino_dua', @$entidaddestino['idconcat']);
        }
        $view->set('script_busca_entidaddestino', $url->rawssl('duas', 'getEntidadDestino'));

        $view->set('entry_tipodocrelacionado', 'Tipo Documento Relacionado:');
        if ($request->has('tipodocrelacionado', 'post')) {
            $view->set('tipodocrelacionado', $request->get('tipodocrelacionado', 'post'));
        } else {
            $view->set('tipodocrelacionado', @$objeto_info['tipodocrelacionado']);
        }
        $view->set('tipodocrelacionados', $database->getRows("SELECT tipodocumento, descripcion FROM tipodocrelacionados ORDER BY descripcion ASC"));

        $view->set('entry_nrodocrelacionado', 'Número Documento Relacionado:');
        if ($request->has('nrodocrelacionado', 'post')) {
            $view->set('nrodocrelacionado', $request->get('nrodocrelacionado', 'post'));
        } else {
            $view->set('nrodocrelacionado', @$objeto_info['nrodocrelacionado']);
        }

        $view->set('entry_nrotransporte', 'Transporte:');
        if ($request->has('nrotransporte', 'post')) {
            $view->set('nrotransporte', $request->get('nrotransporte', 'post'));
        } else {
            $view->set('nrotransporte', @$objeto_info['nrotransporte']);
        }

        $view->set('entry_nroremito', 'Nro Remito:');
        if ($request->has('nroremito', 'post')) {
            $view->set('nroremito', $request->get('nroremito', 'post'));
        } else {
            $view->set('nroremito', @$objeto_info['nroremito']);
        }

        $view->set('entry_nroguia', 'Nro Guía:');
        if ($request->has('nroguia', 'post')) {
            $view->set('nroguia', $request->get('nroguia', 'post'));
        } else {
            $view->set('nroguia', @$objeto_info['nroguia']);
        }

        $view->set('entry_nrofactura', 'Nro Factura:');
        if ($request->has('nrofactura', 'post')) {
            $view->set('nrofactura', $request->get('nrofactura', 'post'));
        } else {
            $view->set('nrofactura', @$objeto_info['nrofactura']);
        }

        $view->set('entry_nombreresponsable', 'Responsable:');
        if ($request->has('nombreresponsable', 'post')) {
            $view->set('nombreresponsable', $request->get('nombreresponsable', 'post'));
        } else {
            $view->set('nombreresponsable', @$objeto_info['nombreresponsable']);
        }

        $view->set('entry_lugarentrega', 'Entrega:');
        if ($request->has('lugarentrega', 'post')) {
            $view->set('lugarentrega', $request->get('lugarentrega', 'post'));
        } else {
            $view->set('lugarentrega', @$objeto_info['lugarentrega']);
        }

        $view->set('entry_observacion', 'Observaciones:');
        if ($request->has('observacion', 'post')) {
            $view->set('observacion', $request->get('observacion', 'post'));
        } else {
            $view->set('observacion', @$objeto_info['observacion']);
        }

        $view->set('entry_nroaduana', 'Nro Aduana:');
        if ($request->has('nroaduana', 'post')) {
            $view->set('nroaduana', $request->get('nroaduana', 'post'));
        } else {
            $view->set('nroaduana', @$objeto_info['nroaduana']);
        }

        $view->set('entry_fechabaja', 'Fecha Baja:');
        if ($request->get('action') == 'delete') {
            $view->set('fechabaja', date('d/m/Y'));
        } else {
            if ($request->has('fechabaja', 'post')) {
                $view->set('fechabaja', $request->get('fechabaja', 'post'));
            } else {
                if (@$objeto_info['fechabaja'] != '')
                    $view->set('fechabaja', date('d/m/Y', strtotime(@$objeto_info['fechabaja'])));
                else
                    $view->set('fechabaja', '');
            }
        }

        $view->set('entry_motivobaja', 'Motivo Baja:');
        if ($request->has('motivobaja', 'post')) {
            $view->set('motivobaja', $request->get('motivobaja', 'post'));
        } else {
            $view->set('motivobaja', @$objeto_info['motivobaja']);
        }

        $view->set('entry_articulo', 'Producto:');
        if ($request->has('articulo_dua', 'post')) {
            $view->set('articulo_dua', $request->get('articulo_dua', 'post'));
        } else {
            $view->set('articulo_dua', '-1');
        }


        if ($request->has('preciocosto', 'post')) {
            $view->set('preciocosto', $request->get('preciocosto', 'post'));
        } else {
            $view->set('preciocosto', '0');
        }

        if ($request->has('codbarra', 'post')) {
            $view->set('codbarra', $request->get('codbarra', 'post'));
        } else {
            $view->set('codbarra', '');
        }

        if ($request->has('codbarrainterno', 'post')) {
            $view->set('codbarrainterno', $request->get('codbarrainterno', 'post'));
        } else {
            $view->set('codbarrainterno', '');
        }

        $view->set('entry_cantidad', 'Cantidad:');
        if ($request->has('cantidad', 'post')) {
            $view->set('cantidad', $request->get('cantidad', 'post'));
        } else {
            $view->set('cantidad', '1');
        }

        if ($request->has('articulosdua', 'post')) {
            $view->set('articulosasignados', $request->get('articulosdua', 'post'));
        } else {
            $articulosasignados = $database->getRows("SELECT * FROM vw_grilla_duaarticulos where dua = '" . $objeto_id . "'");
            $view->set('articulosasignados', $articulosasignados);
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');

        $view->set('error_texto_error', @$this->error['texto_error']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $urlAction = $url->ssl('duas', $request->get('action'), array('dua' => $request->get('dua')));
        $view->set('action', $urlAction);

        $view->set('accion_form', $request->get('action'));
        $view->set('cancel', $url->ssl('duas'));
        // </editor-fold>

        return $view->fetch('content/dua.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $common = & $this->locator->get('common');
        // </editor-fold>

        $errores = '';
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        if (($request->get('tipodua', 'post') == '-1')) {
            $errores = 'Debe seleccionar un tipo de DUA. <br>';
        }

        if ((strlen($request->get('fecha', 'post')) != 10)) {
            $errores = 'Debe ingresar una fecha. <br>';
        }

        if (is_null($request->get('auto_entidadorigen_dua', 'post')) || $request->get('auto_entidadorigen_dua', 'post') == '') {
            $errores = 'Debe seleccionar una entidad origen. <br>';
        }

        if ((is_null($request->get('auto_entidaddestino_dua', 'post')) || $request->get('auto_entidaddestino_dua', 'post') == '')) {
            //if ($request->get('auto_entidaddestino_dua', 'post') == '' && $request->get('tipodua', 'post') == 'E') {
            $errores .= 'Debe seleccionar una entidad destino. <br>';
        }

        $articulos = $request->get('articulosdua', 'post', array());
        if (count($articulos) == 0) {
            $errores .= 'Debe ingresar al menos un producto. <br>';
        }
        else{
             //VALIDAR QUE LOS ARTICULOS CON CODIGO DE BARRA NO EXISTAN YA EN EL SISTEMA 
             if ($request->get('tipodua', 'post') == 'E') {
                 foreach ($articulos as $art) {
                    if ($art['codbarra']) {
                       $existe = $database->getRow("SELECT COUNT(*) AS TOTAL FROM articulosbarra WHERE codbarra='" . $art['codbarra'] . "'  ");
                            if ($existe['TOTAL']>0) {
                                 $errores .= 'El Articulo con codigo de barra '.$art['codbarra'].' ya éxiste en el sistema. <br>';
                            }  
                    }

                }
             }
            
        }
        
       
        
        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $common = & $this->locator->get('common');
        // </editor-fold>

        $template->set('title', 'DUA');

        if (($request->isPost()) && ($this->validateForm())) {

            //CONTROLA QUE NO SE ENCUENTRE INGRESADA LA DUA
            $consulta_info = $database->getRow("SELECT COUNT(*) as total FROM duas WHERE dua = '" . $request->get('dua', 'post') . "' ");
            if (@$consulta_info['total'] != 0) {

                $response->redirect($url->ssl('duas'));
                return;
            }
            
            $tipodua = $request->get('tipodua', 'post');
            $estadodua = $request->get('estadodua', 'post');
            // <editor-fold defaultstate="collapsed" desc="FECHA">
            $fecha_d = '';
            if ($request->get('fecha', 'post') != '') {
                $dated = explode('/', $request->get('fecha', 'post'));
                $fecha_d = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
            } else {
                $fecha_d = NULL;
            }

            $fecha_b = '';
            if ($request->get('fechabaja', 'post') == '' || $request->get('fechabaja', 'post') == NULL) {
                $fecha_b = NULL;
            } else {
                $dated = explode('/', $request->get('fechabaja', 'post'));
                $fecha_b = $dated[2] . '-' . $dated[1] . '-' . $dated[0];
            }

            // </editor-fold>

            $identidadorigenconcat = $request->get('auto_entidadorigen_dua', 'post');
            $arrayValores = explode('-', $identidadorigenconcat);
            $entidadorigen = $arrayValores[0];
            $tipoentidadorigen = $arrayValores[1];

            $identidaddestinoconcat = $request->get('auto_entidaddestino_dua', 'post');
            $arrayValores = explode('-', $identidaddestinoconcat);
            $entidaddestino = $arrayValores[0];
            $tipoentidaddestino = $arrayValores[1];

            $sql = "INSERT INTO duas SET dua='?', "
                    . "tipoentidadorigen='?', "
                    . "entidadorigen='?', "
                    . "tipoentidaddestino='?', "
                    . "entidaddestino='?', "
                    . "fecha='?', "
                    . "tipodocrelacionado='?', "
                    . "nrodocrelacionado='?', "
                    . "nroremito='?', "
                    . "nroguia='?', "
                    . "nroaduana='?', "
                    . "estadodua='V', "
                    . "tipodua='?', "
                    . "nrotransporte='?', "
                    . "lugarentrega='?', "
                    . "nrofactura='?', "
                    . "nombreresponsable='?', "
                    . "observacion='?', "
                    . "fechabaja='?', "
                    . "motivobaja='?', "
                    . "au_usuario='?', "
                    . "au_accion='A', "
                    . "au_fecha_hora=NOW() ";
            $sql = $database->parse($sql, $request->get('dua', 'post'), $tipoentidadorigen, $entidadorigen, $tipoentidaddestino, $entidaddestino, $fecha_d, $request->get('tipodocrelacionado', 'post'), $request->get('nrodocrelacionado', 'post'), $request->get('nroremito', 'post'), $request->get('nroguia', 'post'), $request->get('nroaduana', 'post'), $tipodua, $request->get('nrotransporte', 'post'), $request->get('lugarentrega', 'post'), $request->get('nrofactura', 'post'), $request->get('nombreresponsable', 'post'), $request->get('observacion', 'post'), $fecha_b, $request->get('motivobaja', 'post'), $user->getPERSONA());
            $database->query($sql);

            $id = $request->get('dua', 'post');

            $articulos = $request->get('articulosdua', 'post', array());

            //ARTICULOS
            //guardar detalles del dua
            //actualizar ultimo preciocompra en articulo
            //sumar el stockactual en articulo
            foreach ($articulos as $art) {

                $sql = "INSERT INTO duadetalles SET dua='?', articulo='?', cantidad='?', codbarra='?', preciocompra='?',au_usuario='?',au_accion='A',au_fecha_hora=NOW()";
                $sql = $database->parse($sql, $request->get('dua', 'post'), $art['articulo'], $art['cantidad'], $art['codbarra'], $art['preciocompra'], $user->getPERSONA());
                $database->query($sql);

                // ENTRADA
                if ($tipodua == 'E') {

                    //si tiene codigo de barra lo guardo en articulosbarra y le asigno el pto de venta
                    //si no tiene busco en la tabla articulospuntovta si esta le aumento la cantidad si no esta se ingresa
                    if ($art['codbarra']) {
                        $sql = "INSERT INTO articulosbarra SET articulo='?', codbarra='?', preciocompra='?', estadoarticulo='DI', puntovta='?', au_usuario='?',au_accion='A',au_fecha_hora=NOW()";
                        $database->query($database->parse($sql, $art['articulo'], $art['codbarra'], $art['preciocompra'], $entidaddestino, $user->getPERSONA()));
                    }

                        //actualzo el stock del articulo en el pdv
                        $articulopuntovta = $database->getRow("SELECT * FROM articulospuntovta WHERE articulo='" . $art['articulo'] . "' AND puntovta='" . $entidaddestino . "' ");
                        if ($articulopuntovta) {
                            $cantidad = $articulopuntovta['cantidad'] + $art['cantidad'];
                            $sql = "UPDATE articulospuntovta SET cantidad='?' "
                                    . "WHERE articulo='?' AND puntovta='?'";
                            $sql = $database->parse($sql, $cantidad, $art['articulo'], $entidaddestino);
                            $database->query($sql);
                        } else {
                            $sql = "INSERT INTO articulospuntovta SET articulo='?', puntovta='?', cantidad='?'";
                            $sql = $database->parse($sql, $art['articulo'], $entidaddestino, $art['cantidad']);
                            $database->query($sql);
                        }
                        
                    //actualizo en el stock general
                    $cantidadenbase = $database->getRow("SELECT cantidadstock FROM articulos where articulo = '" . $art['articulo'] . "'");
                    $cantidadactualizada = $cantidadenbase['cantidadstock'] + $art['cantidad'];
                    $sql = "UPDATE articulos SET cantidadstock= '?', precioultcompra='?', au_accion='D', au_fecha_hora=NOW() "
                            . "WHERE articulo='?'";
                    $sql = $database->parse($sql, $cantidadactualizada, $art['preciocompra'], $art['articulo']);
                    $database->query($sql);
                }
                // TRANSFERENCIA
                if ($tipodua == 'T') {
                    // SE DISMINUYE LA CANTIDAD DE LA ENTIDAD ORIGEN
                    // NO se actualiza en el stock general
                    // 
                    //si tiene codigo de barra actualizo el pto de venta
                    //si no tiene busco en la tabla articulospuntovta si esta le aumento la cantidad si no esta se ingresa, y disminuye la cantidad de entidad origen
                    if ($art['codbarra']) {
                        $sql = "UPDATE articulosbarra SET puntovta='?' "
                                . "WHERE codbarra='?'";
                        $database->query($database->parse($sql, $entidaddestino, $art['codbarra']));
                    } 
                        $articulopuntovta = $database->getRow("SELECT * FROM articulospuntovta WHERE articulo='" . $art['articulo'] . "' AND puntovta='" . $entidaddestino . "' ");
                        if ($articulopuntovta) {
                            $cantidad = $articulopuntovta['cantidad'] + $art['cantidad'];
                            $sql = "UPDATE articulospuntovta SET cantidad='?' "
                                    . "WHERE articulo='?' AND puntovta='?'";
                            $sql = $database->parse($sql, $cantidad, $art['articulo'], $entidaddestino);
                            $database->query($sql );
                        } else {
                            $sql = "INSERT INTO articulospuntovta SET articulo='?', puntovta='?', cantidad='?'";
                            $sql = $database->parse($sql, $art['articulo'], $entidaddestino, $art['cantidad']);
                            $database->query($sql);
                        }

                        //disminuye la cantidad de entidad origen
                        $articuloEntidadOrigen = $database->getRow("SELECT * FROM articulospuntovta WHERE articulo='" . $art['articulo'] . "' AND puntovta='" . $entidadorigen . "' ");
                        $cantidad = $articuloEntidadOrigen['cantidad'] - $art['cantidad'];
                        $sql = "UPDATE articulospuntovta SET cantidad='?' "
                                . "WHERE articulo='?' AND puntovta='?'";
                        
                        $database->query($database->parse($sql, $cantidad, $art['articulo'], $entidadorigen));
                    
                }
            }

            $cache->delete('duas');
            $session->set('message', 'Se agreg&oacute; el DUA: ' . $id);

            $response->redirect($url->ssl('duas'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function update() {
        // regla de negocio, no se puede modificar un dua
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        $template->set('title', 'DUA');

        if (($request->isPost())) {

            $response->redirect($url->ssl('duas'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function delete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        $template->set('title', 'DUA');

        if (($request->isPost())) {

            $id = $request->get('dua', 'post');

            $sql = "UPDATE duas SET "
                    . "estadodua='A', "
                    . "fechabaja='?', "
                    . "motivobaja='?', "
                    . "au_usuario='?', "
                    . "au_accion='M', "
                    . "au_fecha_hora=NOW() "
                    . "WHERE dua='?'";
            $sql = $database->parse($sql, date('d/m/Y H:i:s'), $request->get('motivobaja', 'post'), $user->getPERSONA(), $request->get('dua', 'post'));
            $database->query($sql);

            $cache->delete('duas');
            $session->set('message', 'Se anul&oacute; el DUA: ' . $id);

            $response->redirect($url->ssl('duas'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function validateDelete() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VALIDACIONES">

        $errores = '';

        if ($errores != '') {
            $this->error['message'] = $errores;
        }

        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function exportar() {
        // regla de negocio, no se puede exportar un dua
    }

    function RemitoPDF() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        // </editor-fold>

        set_time_limit(0);

        if ($request->has('dua', 'post')) {
            $dua_id = $request->get('dua', 'post');
        } else {
            if ($request->has('dua', 'get')) {
                $dua_id = $request->get('dua', 'get');
            }
        }


        $consulta = "SELECT DISTINCT * FROM vw_grilla_duas WHERE dua = '?'";
        $dua_info = $database->getRow($database->parse($consulta, $dua_id));

        $consult = "SELECT * FROM vw_grilla_duaarticulos WHERE dua='" . $dua_id . "' ";
        $detalledua = $database->getRows($consult);

        // <editor-fold defaultstate="collapsed" desc="config PDF">
        // Include the main TCPDF library (search for installation path).
        // Include the main TCPDF library (search for installation path).
        define('FPDF_FONTPATH', 'font/');
        require('library/pdf/tcpdf/tcpdf.php');
        require_once('library/pdf/tcpdf/tcpdf_include.php');

        $con = PDF_PAGE_ORIENTATION;
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF8 sin BOM', false);


        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('STC');
        $pdf->SetTitle('');
        $pdf->SetSubject(' ');
        $pdf->SetKeywords(' ');

        // set default header data
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        //LA IMAGEN DEBE ESTAR EN template\default\image
        // $pdf->SetHeaderData('cabecera PDF-02.jpg', 173, '', '');
        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font - conjunto predeterminado fuentemonoespaciada
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetMargins(20, 10, 20);
        //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetHeaderMargin(5);
        $pdf->SetFooterMargin(15);

        // set auto page breaks - establecer saltos de página automático
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // establecemos la medida del interlineado
        //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        //$pdf->SetDefaultMonospacedFont(30);
        // ---------------------------------------------------------
        // set font
        $pdf->SetFont('helvetica', '', 9);


        //$pdf->Image('images/sistema.png', 50, 50, 100, '', '', 'http://www.tcpdf.org', '', false, 300);
// </editor-fold>  
        // add a page
        $pdf->AddPage();

        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO">
        // $html = '<div style="text-align:center; " ><h1><b>UNIÓN DE RUGBY ARGENTINO</b></h1><br></div>';
        //$pdf->writeHTML($html, true, false, true, false, '');
        //$html = '<div style="text-align:center; font-size: 13pt;" ><b>CONSTANCIA DE ORDEN DE REPARACIÓN</b><br></div>';
        //$pdf->writeHTML($html, true, false, true, false, '');
//            $html = '<div style="text-align:center; font-size: 11pt;" ><br><b>ORDEN NRO: '.$reparacion_id.' </b><br></div>';
//            $pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ENCABEZADO REMITO">
        // define barcode style
        $style = array(
            'position' => '',
            'align' => 'C',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );

        // PRINT VARIOUS 1D BARCODES
        // CODE 39 EXTENDED + CHECKSUM
        //$pdf->Cell(0, 240, 'NÚMERO DE ORDEN', 0, 1);
        //public function write1DBarcode($code, $type, $x='', $y='', $w='', $h='', $xres='', $style='', $align='')
        //$pdf->write1DBarcode($reparacion_id, 'S25', '', '242', '', 18, 0.4, $style, 'N');
        //$pdf->Cell(110, 0, 'NÚMERO DE ORDEN', 0, 1);
        // Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)
        $pdf->Image('template/default/image/Remito.jpg', 95, 10, 20, 30, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
        $pdf->Image('template/default/image/Insight.jpg', 25, 10, 45, 20, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
        //$pdf->Text(55, 260, 'NÚMERO DE ORDEN');

        $tipo = $dua_info['desctipodua'];
        $fecha = $dua_info['fecha'];

        $html = <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    <td width="100" style="text-align:left;"><b></b></td>
                    <td width="300" style="text-align:right;"><b></b></td>
                    <td width="300" style="text-align:left;" ><b></b></td>
                    
               </tr> 
                <tr >
                    <td width="100" style="text-align:left;" ><b></b></td>
                    <td width="300" style="text-align:right;"><b></b></td>
                    <td width="300" style="text-align:left;" ><b>REMITO NRO: $dua_id</b></td>
               </tr> 
                <tr >
                    <td width="100" style="text-align:left;" ><b></b></td>
                    <td width="300" style="text-align:right;"><b></b></td>
                    <td width="300" style="text-align:left;" ><b>FECHA: $fecha</b></td>
                    
               </tr>
                
                
            </table>
EOF;


        $pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>
        /// <editor-fold defaultstate="collapsed" desc="CLIENTE EQUIPO">
        //$html = '<div style="text-align:center; font-size: 11pt;" ><b>Datos del Cliente</b><hr /></div>';
        $html = <<<EOF
                 <br> <br><br><br><hr> 
EOF;
        $pdf->writeHTML($html, true, false, true, false, '');
        // <editor-fold defaultstate="collapsed" desc="STYLE">
        $html = <<<EOF
                <!-- EXAMPLE OF CSS STYLE -->
                <style>
                    /*
                    table.first {
                        color: black;
                        font-family: helvetica;
                        font-size: 5pt;
                        background-color: white;
                border: 0px solid black;
                    }
                    td {
                        border: 0px solid black;
                        background-color: white;
                    }
                    */
                </style>
EOF;
        //$pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>

        $tipo = $dua_info['desctipodua'];
        $descestadodua = $dua_info['descestadosdua'];
        $origen = $dua_info['ENTIDAD_ORG'];
        $destino = $dua_info['ENTIDAD_DES'];
        $descnrodocrelacionado = $dua_info['desctipodocrelacionado'];
        $nrodocrelacionado = $dua_info['nrodocrelacionado'];
        $nrotransporte = $dua_info['nrotransporte'];
        $nombreresponsable = $dua_info['nombreresponsable'];

        $html .= <<<EOF
                       
                <table>
                <tr>
                    <td width="165" style="text-align:left;font-size: 6pt;"><b>TIPO</b></td>
                    <td width="165" style="text-align:left;font-size: 6pt;"><b>ESTADO</b></td>
                    <td width="165" style="text-align:left;font-size: 6pt;"><b>ENTIDAD ORIGEN</b></td>
                    <td width="165" style="text-align:left;font-size: 6pt;"><b>ENTIDAD DESTINO</b></td>
               </tr> 
                <tr>
                    <td width="165" style="text-align:left;font-size: 9pt;">$tipo</td>
                    <td width="165" style="text-align:left;font-size: 9pt;">$descestadodua</td>
                    <td width="165" style="text-align:left;font-size: 9pt;">$origen</td>
                    <td width="165" style="text-align:left;font-size: 9pt;">$destino</td>
               </tr> 
                <tr>
                    <td width="165" style="text-align:left;font-size: 6pt;"><b>DOC. RELACIONADO</b></td>
                    <td width="165" style="text-align:left;font-size: 6pt;"><b>NRO DOC RELACIONADO</b></td>
                    <td width="165" style="text-align:left;font-size: 6pt;"><b>TRANSPORTE</b></td>
                    <td width="165" style="text-align:left;font-size: 6pt;"><b>RESPONSABLE</b></td>
               </tr> 
                <tr>
                    <td width="165" style="text-align:left;font-size: 9pt;">$descnrodocrelacionado</td>
                    <td width="165" style="text-align:left;font-size: 9pt;">$nrodocrelacionado</td>
                    <td width="165" style="text-align:left;font-size: 9pt;">$nrotransporte</td>
                    <td width="165" style="text-align:left;font-size: 9pt;">$nombreresponsable</td>
               </tr>
                
            </table>
                <br><hr>
EOF;


        $pdf->writeHTML($html, true, false, true, false, '');

        // </editor-fold>
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="DETALLE">
        $html = '<div style="text-align:center; font-size: 11pt;" ><b>DETALLE</b><hr /></div>';
        $html .= <<<EOF
                   <br>
EOF;
        $pdf->writeHTML($html, true, false, true, false, '');
        // <editor-fold defaultstate="collapsed" desc="STYLE">
        $html = <<<EOF
                <!-- EXAMPLE OF CSS STYLE -->
                <style>
                    /*
                    table.first {
                        color: black;
                        font-family: helvetica;
                        font-size: 8pt;
                        background-color: white;
                    }
                    td {
                        border: 0px solid black;
                        background-color: white;
                    }
                    */
                </style>
EOF;
        //$pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>
        //lista de repuestos reparacion




        $html .= <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    <td width="70" style="text-align:left;"><b>Artículo</b></td>
                    <td width="280" style="text-align:left;"><b>Descripción</b></td>
                     <td width="90" style="text-align:left;"><b>Tipo</b></td>
                    <td width="50" style="text-align:center;"><b>Cant.</b></td>
                    <td width="80" style="text-align:right;"><b>Cod. barras</b></td>
               </tr> 
                
            </table>
                <hr />
EOF;

        foreach ($detalledua as $detalle) {
            $articulo = $detalle['articulo'];
            $descripcion = $detalle['descmodelo'];
            $desctipoproducto = $detalle['desctipoproducto'];
            $cantidad = $detalle['cantidad'];
            $codinterno = $detalle['codbarrainterno'];
            $codbarra = $detalle['codbarra'];

            $html .= <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    <td width="70" style="text-align:left;">$codinterno</td>
                    <td width="280" style="text-align:left;">$descripcion</td>
                    <td width="90" style="text-align:left;">$desctipoproducto</td>
                    <td width="50" style="text-align:center;">$cantidad</td>
                    <td width="80" style="text-align:right;">$codbarra</td>
               </tr> 
                
            </table>
EOF;
        }

        $pdf->writeHTML($html, true, false, true, false, '');

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="SELLO">
        // Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)
        //  $pdf->Image('template/default/image/sello-entregado-2.png', 130, 180, 60, 50, 'png', '', '', true, 150, '', false, false, 0, false, false, false);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PIE">
        //$html = '<div style="text-align:center; font-size: 11pt;" ><b>Datos del Cliente</b><hr /></div>';
        $html = <<<EOF
                 <br> <br><br><br><hr> 
EOF;
        $pdf->writeHTML($html, true, false, true, false, '');
        // <editor-fold defaultstate="collapsed" desc="STYLE">
        $html = <<<EOF
                <!-- EXAMPLE OF CSS STYLE -->
                <style>
                    /*
                    table.first {
                        color: black;
                        font-family: helvetica;
                        font-size: 8pt;
                        background-color: white;
                    }
                    td {
                        border: 0px solid black;
                        background-color: white;
                    }
                    */
                </style>
EOF;
        //$pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>

        $totalReparacion = $venta_info['total'];
        $html = <<<EOF
                       
                <table class='first' border=0>
                <tr >
                    
                    <td width="600" style="text-align:center;" ><b>TOTAL $ $totalReparacion</b></td>
               </tr> 
                
                
            </table>
                <br><hr>
EOF;


        //$pdf->writeHTML($html, true, false, true, false, '');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="PIE">
        $pdf->Text(20, 255, '_____________________________________');
        $pdf->Text(25, 260, 'FIRMA Y ACLARACION DEL RESPONSABLE');
        $pdf->Text(110, 255, '_____________________________________');
        $pdf->Text(115, 260, 'FIRMA Y ACLARACION DEL RESPONSABLE');
        // </editor-fold>
        //EJEMPLO
        //// print TEXT
        //$pdf->PrintChapter(1, 'LOREM IPSUM [TEXT]', 'data/chapter_demo_1.txt', false);
        // print HTML
        //$pdf->PrintChapter(2, 'LOREM IPSUM [HTML]', 'data/chapter_demo_2.txt', true);
        // output some RTL HTML content
        //$html = '<div style="text-align:right">ACOMPAÑA<br></div>';
        //$pdf->writeHTML($htmlTable, true, false, true, false, '');
        // reset pointer to the last page
        //$pdf->lastPage();
        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        // Print a table
        // remove default header
        $pdf->setPrintHeader(false);
        //ESTO HACE QUE ANDE EN EL SERVER DE LA DNNA
        ob_end_clean();
        //Close and output PDF document
        //D obliga la descarga
        //I abre en una ventana nueva
        $nombrePDF = 'RemitoNro' . $reparacion_id . '.pdf';
        $pdf->Output($nombrePDF, 'I');

        //$cache->delete('checkins');
        //die('ok');
        //============================================================+
        // END OF FILE
        //============================================================+
    }

    function getEntidadOrigen() {
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');

        $tipodua = $request->get('tipodua', 'get');
        $where_tipoentidad = '';
        if ($tipodua == 'E') { //ENTRADA
            $where_tipoentidad = " AND tipoentidad = 'P' ";
        }
        if ($tipodua == 'T') { //TRANSFERENCIA
            $where_tipoentidad = " AND tipoentidad = 'PV' ";
        }

        $miDescripcion = $request->get('term', 'get');
        $sql = "SELECT idconcat AS id, descripcion as value "
                . "FROM vw_list_entidad "
                . "WHERE descripcion LIKE '?' "
                . $where_tipoentidad
                . "LIMIT 10";
        $consulta = $database->parse($sql, '%' . $miDescripcion . '%');
        $codigo = $database->getRows($consulta);

        $varia = utf8_decode(json_encode($codigo));

        echo $varia;
    }

    function getEntidadDestino() {
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');

        $tipodua = $request->get('tipodua', 'get');
        $entidadorigen = $request->get('entidadorigen', 'get');
        $where = '';
        if ($tipodua == 'E') { //ENTRADA
            $where = " AND tipoentidad = 'P' ";
        }
        if ($tipodua == 'T') { //TRANSFERENCIA
            $where .= " AND tipoentidad = 'PV' ";
            $where .= " AND idconcat <> '" . $entidadorigen . "' ";
        }

        $miDescripcion = $request->get('term', 'get');
        $sql = "SELECT idconcat AS id, descripcion as value "
                . "FROM vw_list_entidad "
                . "WHERE descripcion LIKE '?' "
                . $where
                . "LIMIT 10";
        $consulta = $database->parse($sql, '%' . $miDescripcion . '%');
        $codigo = $database->getRows($consulta);

        $varia = utf8_decode(json_encode($codigo));

        echo $varia;
    }

    function getArticulo() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        // </editor-fold>		

        $miDescripcion = $request->get('term', 'get');
        $tipodua = $request->get('tipodua', 'get');
        $eo = $request->get('entidadorigen', 'get');
        $arrayValores = explode('-', $eo);
        $entidadorigen = $arrayValores[0];
        $tipoentidadorigen = $arrayValores[1];

        if ($tipodua == 'T') { //TRANSFERENCIA, trae solamente los articulos disponibles en esa entidad y su cantidad > 0
            $sql = "SELECT a.articulo AS id, "
                    . " IF(a.concoddebarra='1',concat(a.marca,' - ',a.descmodelo, ' (Cod: ',a.codbarra,')'),concat(a.marca,' - ',a.descmodelo, ' (Cod: ',a.codbarrainterno,')')) AS value, "
                    . "a.concoddebarra, "
                    . "a.codbarra, "
                    . "a.codbarrainterno, "
                    . "a.precioultcompra,"
                    . "apv.cantidad AS cantidad "
                    . "FROM vw_grilla_articulostodos a "
                    . "INNER JOIN articulospuntovta apv ON a.articulo = apv.articulo "
                    . "WHERE (a.descmarca LIKE '?' OR a.descmodelo LIKE '?' OR a.codbarrainterno LIKE '?' OR a.codbarra LIKE '?' ) "
                    . "AND a.estadoarticulo = 'DI' "
                    . "AND apv.cantidad > 0 "
                    . "AND apv.puntovta = '?' "
                    . "LIMIT 10";
            $consulta = $database->parse($sql, '%' . $miDescripcion . '%', '%' . $miDescripcion . '%', '%' . $miDescripcion . '%', '%' . $miDescripcion . '%', $entidadorigen);
        } else {
            $sql = "SELECT a.articulo AS id, "
                    . " concat(a.marca,' - ',a.descmodelo, ' (Cod: ',a.codbarrainterno,')') AS value, "
                    . " a.concoddebarra as concoddebarra, "
                    . " '' as codbarra, "
                    . " a.codbarrainterno, "
                    . " a.precioultcompra,"
                    . " 0 AS cantidad "
                    . " FROM vw_grilla_articulos a "
                    . " WHERE (a.descmarca LIKE '?' OR a.descmodelo LIKE '?' OR a.codbarrainterno LIKE '?'   ) "
                    . " AND a.estadoarticulo = 'DI' "
                    //. "AND apv.cantidad > 0 "
                    . "LIMIT 10";
            $consulta = $database->parse($sql, '%' . $miDescripcion . '%', '%' . $miDescripcion . '%', '%' . $miDescripcion . '%', '%' . $miDescripcion . '%');
        }

        $registros = $database->getRows($consulta);

        $varia = utf8_decode(json_encode($registros));

        echo $varia;
    }

    function AgregarArticulo() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $database = & $this->locator->get('database');
        $language = & $this->locator->get('language');
        $response = & $this->locator->get('response');
        $request = & $this->locator->get('request');
        $view = $this->locator->create('template');
        // </editor-fold>

        $view->set('button_add', $language->get('button_add'));
        $view->set('button_remove', $language->get('button_remove'));

        $articulo = $request->get('articulo');
        $dua = $request->get('dua');
        $cantidad = $request->get('cantidad');
        $codbarra = $request->get('codbarra');
        $codbarrainterno = $request->get('codbarrainterno');
        $preciocompra = $request->get('preciocompra');

        $sql = "SELECT a.articulo, "
                . "concat(a.marca,' - ',a.descmodelo) AS descripcion "
                . "FROM vw_grilla_articulos a "
                . "WHERE a.articulo = '?' ";
        $consulta = $database->parse($sql, $articulo);
        $art = $database->getRow($consulta);

        $descmodelo = $art['descripcion'];

        $view->set('cargo_dua', $dua);
        $view->set('cargo_articulo', $articulo);
        $view->set('cargo_descmodelo', $descmodelo);
        $view->set('cargo_cantidad', $cantidad);
        $view->set('cargo_codbarra', $codbarra);
        $view->set('cargo_codbarrainterno', $codbarrainterno);
        $view->set('cargo_preciocompra', $preciocompra);

        //SI EL ENTRENAMIENTO ES 0 ES PORQUE ES UN ALTA
        $view->set('cargo_id', $request->get('cantReg'));

        $view->set('nombre_arreglo', 'articulosdua');
        $view->set('nombre_fila', 'duaarticulo_' . $request->get('cantReg'));

        $response->set($view->fetch('content/dua_articulo.tpl'));
    }

}

?>