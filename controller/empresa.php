<?php

class ControllerEmpresa extends Controller {

    var $error = array();

    function index() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $response = & $this->locator->get('response');
        $template = & $this->locator->get('template');
        $module = & $this->locator->get('module');
        // </editor-fold>

        $template->set('title', 'Empresa');

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $language = & $this->locator->get('language');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $view = $this->locator->create('template');
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="VIEW VARIABLES">
        $view->set('heading_title', 'Cliente');
        $view->set('heading_title_icon', 'template/default/image/img/gold_icons/user-8.png');

        $view->set('heading_description', 'Gesti&oacute;n de clientes');

        $view->set('text_enabled', $language->get('text_enabled'));
        $view->set('text_disabled', $language->get('text_disabled'));

        $view->set('button_save', $language->get('button_save'));
        $view->set('button_cancel', $language->get('button_cancel'));

        $view->set('tab_general', $language->get('tab_general'));

        // <editor-fold defaultstate="collapsed" desc="EMPRESA">
        
        if ($request->get('empresa','post')) {
            $consulta = "SELECT DISTINCT * FROM empresa WHERE empresa = '?' ";
            @$objeto_info = $database->getRow($database->parse($consulta, $request->get('empresa','post')));
        }
        else{
            $consulta = "SELECT * FROM empresa LIMIT 1 ";
            @$objeto_info = $database->getRow($consulta);
            if (!$objeto_info) {
                //meto la direccion de los arhivos por defectos favicon encabezado logoempresa
                $sql = "insert into empresa SET empresa =1 ,nombre='sin nombre', favicon='template/default/image/favicon.ico' , logoempresa='template/default/image/logoempresa.png'";
                $database->query($sql);
                
                $consulta = "SELECT * FROM empresa LIMIT 1 ";
                @$objeto_info = $database->getRow($consulta);
            }
        }

        if ($request->has('empresa', 'post')) {
            $view->set('empresa', $request->get('empresa', 'post'));
        } else {
            
            $view->set('empresa', @$objeto_info['empresa']);
        }

        $view->set('entry_nombre', 'Nombre:');
        if ($request->has('nombre', 'post')) {
            $view->set('nombre', $request->get('nombre', 'post'));
        } else {
            $view->set('nombre', @$objeto_info['nombre']);
        }

        $view->set('entry_telefono', 'Teléfono:');
        if ($request->has('telefono', 'post')) {
            $view->set('telefono', $request->get('telefono', 'post'));
        } else {
            $view->set('telefono', @$objeto_info['telefono']);
        }

        $view->set('entry_localidad', 'Localidad:');
        if ($request->has('localidad', 'post')) {
            $view->set('localidad', $request->get('localidad', 'post'));
        } else {
            $view->set('localidad', @$objeto_info['localidad']);
        }
        $view->set('localidades', $database->getRows("SELECT * FROM localidades ORDER BY descripcion ASC"));

        $view->set('entry_domicilio', 'Domicilio:');
        if ($request->has('domicilio', 'post')) {
            $view->set('domicilio', $request->get('domicilio', 'post'));
        } else {
            $view->set('domicilio', @$objeto_info['domicilio']);
        }

        
        $view->set('entry_mail', 'E-Mail:');
        if ($request->has('mail', 'post')) {
            $view->set('mail', $request->get('mail', 'post'));
        } else {
            $view->set('mail', @$objeto_info['mail']);
        }

        $view->set('entry_web', 'Página Web:');
        if ($request->has('web', 'post')) {
            $view->set('web', $request->get('web', 'post'));
        } else {
            $view->set('web', @$objeto_info['web']);
        }

       
        
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="FACTURACION">
        
        $view->set('entry_cuit', 'CUIT:');
        if ($request->has('cuit', 'post')) {
            $view->set('cuit', $request->get('cuit', 'post'));
        } else {
            $view->set('cuit', @$objeto_info['cuit']);
        }

        $view->set('entry_razonsocial', 'Razón social:');
        if ($request->has('razonsocial', 'post')) {
            $view->set('razonsocial', $request->get('razonsocial', 'post'));
        } else {
            $view->set('razonsocial', @$objeto_info['razonsocial']);
        }

        $view->set('entry_domiciliofiscal', 'Domicilio fiscal:');
        if ($request->has('domiciliofiscal', 'post')) {
            $view->set('domiciliofiscal', $request->get('domiciliofiscal', 'post'));
        } else {
            $view->set('domiciliofiscal', @$objeto_info['domiciliofiscal']);
        }

        $view->set('entry_telefonofacturacion', 'Teléfono de facturación:');
        if ($request->has('telefonofacturacion', 'post')) {
            $view->set('telefonofacturacion', $request->get('telefonofacturacion', 'post'));
        } else {
            $view->set('telefonofacturacion', @$objeto_info['telefonofacturacion']);
        }

        $view->set('entry_tipoiva', 'Condición IVA:');
        if ($request->has('tipoiva', 'post')) {
            $view->set('tipoiva', $request->get('tipoiva', 'post'));
        } else {
            if (@$objeto_info['tipoiva']) {
                $view->set('tipoiva', @$objeto_info['tipoiva']);
            } else {
                $view->set('tipoiva', 'RI');
            }
        }
        $view->set('tiposiva', $database->getRows("SELECT * FROM tiposiva ORDER BY descripcion ASC"));

        $view->set('entry_mailfactura', 'E-Mail facturación:');
        if ($request->has('mailfactura', 'post')) {
            $view->set('mailfactura', $request->get('mailfactura', 'post'));
        } else {
            $view->set('mailfactura', @$objeto_info['mailfactura']);
        }

        $view->set('entry_horarioatencion', 'Horarios de atención:');
        if ($request->has('horarioatencion', 'post')) {
            $view->set('horarioatencion', $request->get('horarioatencion', 'post'));
        } else {
            $view->set('horarioatencion', @$objeto_info['horarioatencion']);
        }

        // <editor-fold defaultstate="collapsed" desc="Imagenes">
        
         
        
        $view->set('entry_logofactura', 'Logo Factura:');
        $view->set('logofactura', @$objeto_info['logofactura']);

        $view->set('entry_certificado_crt', 'Certificado CRT:');
        $view->set('certificado_crt', @$objeto_info['certificado_crt']);
        
        $view->set('entry_claveprivada_key', 'Clave privada .key:');
        $view->set('claveprivada_key', @$objeto_info['claveprivada_key']);
        
        // </editor-fold>
        
         // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="PARAMETROS">
        
        // <editor-fold defaultstate="collapsed" desc="Imagenes">
        
        $view->set('entry_encabezadoreporte', 'Encabezado reporte:');
        $view->set('encabezadoreporte', @$objeto_info['encabezadoreporte']);
        
        $view->set('entry_encabezadoinforme', 'Encabezado informe:');
        $view->set('encabezadoinforme', @$objeto_info['encabezadoinforme']);
        
        $view->set('entry_encabezadomail', 'Encabezado mail:');
        $view->set('encabezadomail', @$objeto_info['encabezadomail']);
        
         $view->set('entry_favicon', 'Favicon (imagen de la pestaña):');
        $view->set('favicon', @$objeto_info['favicon']);
        
        $view->set('entry_logoempresa', 'Logo Empresa:');
        $view->set('logoempresa', @$objeto_info['logoempresa']);
        
        // </editor-fold>
        
        // </editor-fold>
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ERROR VARIABLES">
        $view->set('error', (isset($this->error['message'])) ? $this->error['message'] : $session->get('error'));
        $session->delete('error');
        //$view->set('error_persona', @$this->error['persona']);

        $view->set('error_texto_error', @$this->error['texto_error']);
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="ACTION">
        $urlAction = $url->ssl('empresa', 'update', array('empresa' => $request->get('empresa')));
        $view->set('action', $urlAction);
        $view->set('accion_form', $request->get('action'));
        $view->set('cancel', $url->ssl('empresa'));
       

        // </editor-fold>

        return $view->fetch('content/empresa.tpl');
    }

    function validateForm() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $database = & $this->locator->get('database');
        $common = & $this->locator->get('common');
        $upload = & $this->locator->get('upload');
        // </editor-fold>

        $errores = '';

        if ((strlen($request->get('nombre', 'post')) < 2) || (strlen($request->get('nombre', 'post')) > 100)) {
            $errores .= 'Debe ingresar el nombre. <br>';
        }

        // <editor-fold defaultstate="collapsed" desc="CUIT">
        if ($common->validateCUIT($request->get('cuit', 'post'))) {
            $errores .= 'Debe ingresar un cuit correcto. <br>';
        }
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="favicon">
//        if ($upload->has('favicon')) {
//            $tiposPermitidos = array(
//                'image/ico'
//            );
//
//            if (!in_array($upload->getType('favicon'), $tiposPermitidos)) {
//               $errores .= 'No es un icono v&aacute;lido para cargar (' . $upload->getType('favicon') . ') <br>';
//            }
//        } elseif ($upload->hasError('favicon')) {
//            $errores .= $upload->getErrorMsg($upload->getError('favicon')) . ' <br>';
//        } else {
//            $errores .= 'El favicon a elegir debe ser un archivo .ico <br>';
//        }
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="logoempresa">
//        if ($upload->has('logoempresa')) {
//            $tiposPermitidos = array(
//                'image/png'
//            );
//
//            if (!in_array($upload->getType('logoempresa'), $tiposPermitidos)) {
//               $errores .= 'No es un png v&aacute;lido para cargar (' . $upload->getType('logoempresa') . ') <br>';
//            }
//        } elseif ($upload->hasError('logoempresa')) {
//            $errores .= $upload->getErrorMsg($upload->getError('logoempresa')) . ' <br>';
//        } else {
//            $errores .= 'El logo de la empresa a elegir debe ser un archivo .png <br>';
//        }
        // </editor-fold>
        
        if ($errores != '') {
            $this->error['texto_error'] = $errores;
        }
        // </editor-fold>

        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function update() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        $upload = & $this->locator->get('upload');
        // </editor-fold>

        $template->set('title', 'Empresa');

        if (($request->isPost()) && ($this->validateForm())) {

            $id = $request->get('empresa', 'post');

            $sql = "UPDATE empresa SET nombre='?',domicilio='?',localidad='?',mail='?',web='?',telefono='?',cuit='?',razonsocial='?',tipofacturacion='?',telefonofacturacion='?',domiciliofiscal='?',tipoiva='?',horariosatencion='?',au_usuario='?',au_fecha_hora=now(),au_accion='M' WHERE empresa = '?'";
            $consult = $database->parse($sql, strtoupper($request->get('nombre', 'post')), $request->get('domicilio', 'post'), $request->get('localidad', 'post'), $request->get('mail', 'post'), $request->get('web', 'post'), $request->get('telefono', 'post'), $request->get('cuit', 'post'),$request->get('razonsocial', 'post'),$request->get('tipofacturacion', 'post'),$request->get('telefonofacturacion', 'post'),$request->get('domiciliofiscal', 'post'),$request->get('horariosatencion', 'post'),$request->get('tipoiva', 'post'), $user->getPERSONA(), $id);
            $database->query($consult);

            // <editor-fold defaultstate="collapsed" desc="favicon">
            //Cargo la imágen1                   
            if ($upload->has('favicon')) {
                $nombreArchivo1 = 'favicon.ico';
                $nombreDirectorio = 'template/default/image/';
                $upload->save('favicon', $nombreDirectorio . $nombreArchivo1);

                $Imagen1 = $nombreDirectorio . $nombreArchivo1;
                $sql = "UPDATE empresa SET favicon = '?' WHERE empresa = '?'";
                $database->query($database->parse($sql, $Imagen1, $request->get('empresa', 'post')));
            }

            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="logoempresa">
            //Cargo la imágen1                   
            if ($upload->has('logoempresa')) {
                $nombreArchivo1 = 'logoempresa.png';
                $nombreDirectorio = 'template/default/image/';
                $upload->save('logoempresa', $nombreDirectorio . $nombreArchivo1);

                $Imagen1 = $nombreDirectorio . $nombreArchivo1;
                $sql = "UPDATE empresa SET logoempresa = '?' WHERE empresa = '?'";
                $database->query($database->parse($sql, $Imagen1, $request->get('empresa', 'post')));
            }

            // </editor-fold>
            
            //SI NO EXISTE LA CARPETA CON EL CUIT CREAR LA CARPETA CON LAS SUBCARPETAS CORRESPONDIENTES
            //
            $cache->delete('empresa');
            $session->set('message', 'Se actualiz&oacute; los datos de la empresa.');

            $response->redirect($url->ssl('home'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function consulta() {
        // <editor-fold defaultstate="collapsed" desc="INCLUDE">
        $request = & $this->locator->get('request');
        $response = & $this->locator->get('response');
        $database = & $this->locator->get('database');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $template = & $this->locator->get('template');
        $session = & $this->locator->get('session');
        $module = & $this->locator->get('module');
        $cache = & $this->locator->get('cache');
        // </editor-fold>

        $template->set('title', 'Clientes');

        if (($request->isPost())) {

            $response->redirect($url->ssl('empresa'));
        }

        $template->set('content', $this->getForm());

        $template->set($module->fetch());

        $response->set($template->fetch('layout.tpl'));
    }

    function getLocalidad() {
        $request = & $this->locator->get('request');
//		$response =& $this->locator->get('response');
        $database = & $this->locator->get('database');
//                $view = $this->locator->create('template');

        $miDescripcion = $request->get('term', 'get');
        //$miPersona = "lucas";
        // $codigo = $database->getRows("select concat(apellido,', ', nombre) as value, persona_id  as id from personas where persona_id ='" . $miPersona . "'");		

        $sql = "SELECT localidad  as id, desclocalidad as value FROM vw_grilla_localidades WHERE descripcion LIKE '?' ";
        $consulta = $database->parse($sql, '%' . $miDescripcion . '%');
        $codigo = $database->getRows($consulta);

        $varia = utf8_decode(json_encode($codigo));


        echo $varia;
    }

}

?>