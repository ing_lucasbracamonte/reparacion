<?php

class ModuleHeader extends Controller {

    function fetch() {
        $config = & $this->locator->get('config');
        $language = & $this->locator->get('language');
        $user = & $this->locator->get('user');

        if ($config->get('header_status')) {
            $language->load('extension/module/header.php');

            $view = $this->locator->create('template');
            $view->set('log', 'no');
            if ($user->isLogged()) {
                $view->set('log', 'ok');
            }
            return $view->fetch('module/header.tpl');
        }
    }

}

?>