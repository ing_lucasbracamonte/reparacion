<?php

class ModuleMenu extends Controller {

    function fetch() {
        $config = & $this->locator->get('config');
        $language = & $this->locator->get('language');
        $url = & $this->locator->get('url');
        $user = & $this->locator->get('user');
        $database = & $this->locator->get('database');
        $request = & $this->locator->get('request');
        $session = & $this->locator->get('session');

        if ($config->get('menu_status')) {

            $language->load('extension/module/menu.php');

            $view = $this->locator->create('template');

            $view->set('text_logout', 'Salir');
            $view->set('link_logout', $url->ssl('logout'));
            $view->set('text_login', 'Iniciar sesi&oacute;n');

            $view->set('text_register', 'Registrarse');
            $view->set('link_logout', $url->ssl('logout'));
            $view->set('text_profile', 'Perfil');

            $view->set('text_contacto', 'Contacto');

            $grupos = $user->getGrupos();
            switch ($grupos[0]['Grupo']) {
                case 'CA': $icono = "user-8.png";
                    break;
                case 'EN': $icono = "user-8.png";
                    break;
                case 'JU': $icono = "SELECCIONES.png";
                    break;
                case 'PG': $icono = "user-8.png";
                    break;
                case 'SU': $icono = "user-8.png";
                    break;
                case 'UC': $icono = "user-8.png";
                    break;
                default : $icono = "user-8.png";
                    break;
            }

            $usuario = array('nombre' => $user->getNombreCompleto(),
                'icono' => $icono);

            $view->set('usuario', $usuario);
            $view->set('isLogged', $user->isLogged());

            if ($session->has('menu')) {

                $menu = $this->generarMenu($session->get('menu'));

                $view->set('controladora', $request->get('controller', 'get'));
                $view->set('menu', $menu);
            } else {

                $sql = "SELECT DISTINCT m.*,"
                        . "(SELECT descripcion FROM menus m1 WHERE m.parent_id = m1.id) AS parent_descripcion "
                        . "FROM perfiles f "
                        . "INNER JOIN grupos g ON f.grupo = g.grupo "
                        . "INNER JOIN personasgrupos p ON g.grupo = p.grupo "
                        . "RIGHT JOIN menus m ON f.formulario = m.formulario or m.formulario = '' "
                        . "WHERE p.persona = '?' OR m.mostrarsiempre = 1 "
                        . "ORDER BY m.parent_id, m.orden ";
                $resultado = $database->getRows($database->parse($sql, $user->getPersona()));

                $menuArray = array();
                foreach ($resultado as $unResultado) {
                    $menuArray[] = Array(
                        'id' => $unResultado['id'],
                        'parent_id' => $unResultado['parent_id'],
                        'tienehijo' => $unResultado['tienehijo'],
                        'descripcion' => $unResultado['descripcion'],
                        'formulario' => $unResultado['formulario'],
                        'link' => $url->ssl($unResultado['formulario']),
                        'icono' => $unResultado['icono'],
                        'esnuevo' => $unResultado['esnuevo']
                    );
                }
                $session->set('menu', $menuArray);

                $menu = $this->generarMenu($session->get('menu'));

                $view->set('controladora', $request->get('controller', 'get'));
                $view->set('menu', $menu);
            }

            return $view->fetch('module/menu.tpl');
        }
    }

    function generarMenu($items = array(), $parent_id = 0) {
        $user = & $this->locator->get('user');
        $database = & $this->locator->get('database');

        $menu1 = '';
        for ($i = 0, $ni = count($items); $i < $ni; $i++) {
            if ($items[$i]['parent_id'] == $parent_id) {

                $esnuevo = '';
                if ($items[$i]['esnuevo'] == 1) {
                    $esnuevo .= '<span class="pull-right-container"><small class="label pull-right bg-green">new</small></span>';
                }

                if ($items[$i]['tienehijo'] == 0) {
                    $menu1 .= '<li class="treeview" style="border-bottom:  1px solid #1b1b1b;"><a href=' . $items[$i]['link'] . '><i class="fa fa-circle-o"></i>  ' . $items[$i]['descripcion'] . $esnuevo . '</a></li>';
                } else {
                    if ($items[$i]['parent_id'] == 0 || ($items[$i]['parent_id'] != 0 && $items[$i]['tienehijo'] == 1)) {

                        //controlar si tiene algun permiso de los hijos
                        //si no tiene no muestro el menu padre
                        $sql = "SELECT DISTINCT m.*,"
                                . "(SELECT descripcion FROM menus m1 WHERE m.parent_id = m1.id) AS parent_descripcion "
                                . "FROM perfiles f "
                                . "INNER JOIN grupos g ON f.grupo = g.grupo "
                                . "INNER JOIN personasgrupos p ON g.grupo = p.grupo "
                                . "RIGHT JOIN menus m ON f.formulario = m.formulario "
                                . "WHERE ((p.persona = '?' OR  m.tienehijo = 1) AND  m.parent_id = '?') OR m.mostrarsiempre = 1 "
                                . "ORDER BY m.parent_id, m.orden ";
                        $resultado = $database->getRows($database->parse($sql, $user->getPersona(), $items[$i]['id']));
                        $mostrar = false;
                        foreach ($resultado as $r) {
                            if ($r['tienehijo'] == 0) {
                                $mostrar = true;
                                break;
                            } else if ($r['parent_id'] != 0 && $r['tienehijo'] == 1) {
                                //controlar si tiene algun permiso de los hijos
                                //si no tiene no muestro el menu padre
                                $sql = "SELECT DISTINCT m.*,"
                                        . "(SELECT descripcion FROM menus m1 WHERE m.parent_id = m1.id) AS parent_descripcion "
                                        . "FROM perfiles f "
                                        . "INNER JOIN grupos g ON f.grupo = g.grupo "
                                        . "INNER JOIN personasgrupos p ON g.grupo = p.grupo "
                                        . "RIGHT JOIN menus m ON f.formulario = m.formulario "
                                        . "WHERE (p.persona = '?' AND  m.parent_id = '?') OR m.mostrarsiempre = 1 "
                                        . "ORDER BY m.parent_id, m.orden ";
                                $resultado2 = $database->getRows($database->parse($sql, $user->getPersona(), $r['id']));
                                foreach ($resultado2 as $r2) {
                                    if ($r2['tienehijo'] == 0) {
                                        $mostrar = true;
                                        break;
                                    }
                                }
                            }
                        }

                        if ($mostrar) {
                            $menu1 .= '<li class="treeview" style="border-bottom:  1px solid #1b1b1b;"><a class="desplegable" href="/" >' . $items[$i]['descripcion'] . '<i class="fa fa-angle-left pull-right"></i></a>';
                            $menu1 .= '<ul>';
                            $menu1 .= $this->generarMenu($items, $items[$i]['id']);
                            $menu1 .= '</ul>';
                            $menu1 .= '</li>';
                        }
                    } else {
                        $menu1 .= '<li class="treeview"><a class="desplegable" href="/"><i class="fa fa-circle-o"></i> ' . $items[$i]['descripcion'] . '<i class="fa fa-angle-left pull-right"></i></a>';
                        $menu1 .= '<ul>';
                        $menu1 .= $this->generarMenu($items, $items[$i]['id']);
                        $menu1 .= '</ul>';
                        $menu1 .= '</li>';
                    }
                }
            }
        }

        $menu1 .= '';
        return $menu1;
    }

}

?>