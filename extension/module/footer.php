<?php

class ModuleFooter extends Controller {

    function fetch() {
        $request = & $this->locator->get('request');
        $language = & $this->locator->get('language');
        $user = & $this->locator->get('user');
        $url = & $this->locator->get('url');
        $image = & $this->locator->get('image');

        $language->load('extension/module/footer.php');

        $view = $this->locator->create('template');

        $miImagen = $image->resize('template/default/image/pie_largo.png', '800', '55');
        $view->set('imagen_pie', $miImagen);

        if ($user->isLogged()) {
            $view->set('log', 'ok');
            $view->set('text_home', 'Inicio');
            $view->set('usuario', $user->getNOMBRE());

            $view->set('text_logout', 'Salir');
            $view->set('logout', $url->ssl('logout'));
        } else {
            $view->set('log', 'no');
        }

        if ($request->get('controller', 'get') == 'contacto') {
            $view->set('log', 'ok');
        }

        return $view->fetch('module/footer.tpl');
    }

}

?>