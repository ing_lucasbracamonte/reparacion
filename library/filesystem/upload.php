<?php
class Upload {
	function get($key) {
		return (isset($_FILES[$key]) ? $_FILES[$key] : NULL);
	}
		
	function has($key){
		return is_uploaded_file($_FILES[$key]['tmp_name']);
        }	

	function getName($key) {
		return (isset($_FILES[$key]['name']) ? $_FILES[$key]['name'] : NULL);
	}
 
	function getType($key) {
		return (isset($_FILES[$key]['type']) ? $_FILES[$key]['type'] : NULL);
	}

	function getSize($key) {
		return (isset($_FILES[$key]['size']) ? $_FILES[$key]['size'] : NULL);
	}

	function getError($key) {
		return (isset($_FILES[$key]['error']) ? $_FILES[$key]['error'] : NULL);
	}

	function hasError($key) {
		if (!is_uploaded_file($_FILES[$key]['tmp_name'])) {
			if ($_FILES[$key]['error']) {
				return !empty($_FILES[$key]['error']);
			}
			return true;
		}
	}

	function save($key, $file) {
		if (file_exists($file)) @unlink($file);
		$status=@copy($_FILES[$key]['tmp_name'], $file);
		if ($status) {
			@chmod($file, 0644);
			@unlink($_FILES[$key]['tmp_name']);
		}
		return $status;
	}	
        
        function getErrorMsg($error_code) {
            switch ($error_code) { 
                case UPLOAD_ERR_INI_SIZE: 
                    return "El archivo es más grande que lo permitido en el servidor (".ini_get('upload_max_filesize').')'; 
                case UPLOAD_ERR_FORM_SIZE: 
                    return "El archivo subido es demasiado grande."; 
                case UPLOAD_ERR_PARTIAL: 
                    return "El archivo subido no se termin&oacute; de cargar. Intente nuevamente."; 
                case UPLOAD_ERR_NO_FILE: 
                    return "No se subi&oacute; ningún archivo."; 
                case UPLOAD_ERR_NO_TMP_DIR: 
                    return "Error del servidor: falta el directorio temporal."; 
                case UPLOAD_ERR_CANT_WRITE: 
                    return "Error del servidor: error de escritura en disco."; 
                case UPLOAD_ERR_EXTENSION: 
                    return "Error del servidor: subida detenida por una extenci&oacute;n de php.";
              default: 
                  $error = error_get_last();
                    return "Error desconocido del servidor: ".$error_code; 
            } 
        }        
}
?>