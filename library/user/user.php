<?php

class User {

    var $data = array();
    var $permisos = array();

    function __construct(&$locator) {
        $this->database = & $locator->get('database');
        $this->request = & $locator->get('request');
        $this->session = & $locator->get('session');

        if ($this->session->has('persona')) {
            $this->data = array('persona' => $this->session->get('persona'),
                'nombre' => $this->session->get('nombre'),
                'apellido' => $this->session->get('apellido'),
                'puntovtaasignado' => $this->session->get('puntovtaasignado'),
                'descpuntovtaasignado' => $this->session->get('descpuntovtaasignado'),
                'exento' => $this->session->get('exento'));
        } else {
            $this->logout();
        }
    }

    function login($username, $password) {

        $sql = "SELECT a.persona, a.clave, a.nombre, a.apellido, b.grupo, a.puntovtaasignado, pv.descripcion AS descpuntovtaasignado FROM personas a INNER JOIN personasgrupos b ON a.persona = b.persona LEFT JOIN puntosdeventa pv ON pv.puntovta = a.puntovtaasignado  WHERE a.persona = '?' AND a.clave = '?'";
        $consulta = $this->database->parse($sql, $username, md5($password));
        $user_info = $this->database->getRow($consulta);

        if ($user_info) {
            $this->session->set('persona', $user_info['persona']);
            $this->session->set('nombre', $user_info['nombre']);
            $this->session->set('apellido', $user_info['apellido']);
            $this->session->set('puntovtaasignado', $user_info['puntovtaasignado']);
            $this->session->set('descpuntovtaasignado', $user_info['descpuntovtaasignado']);
            //$this->session->set('club', $user_info['club']);//ASIGNO EL CLUB A LA SESION
            $this->data = $user_info;

            // <editor-fold defaultstate="collapsed" desc="EXENTO">
            $sql = "SELECT count(*) as total FROM personasgrupos pg LEFT JOIN grupos g ON pg.grupo=g.grupo WHERE pg.Persona = '?' AND g.exento = 1";
            $consult = $this->database->parse($sql, $this->getPERSONA());
            $exento = $this->database->getRow($consult);
            if ($exento['total'] > 0)
                $this->session->set('exento', 1);
            else
                $this->session->set('exento', 0);

            // </editor-fold>
            
            //SE FIJA SI SE LOGIO HOY SUMA EL CANTODOR SI NO PONE EL CONTADOR EN 1
            $contadorlogindia = 0;
             $consulta = "SELECT  UltimoLogin, contadorlogindia  FROM personas WHERE persona = '?' AND DATE(UltimoLogin) = DATE(NOW()) ";
             $persona_info = $this->database->getRow($this->database->parse($consulta, $this->session->get('persona')));
             $hoy = date('Y-m-d');   
             $UltimoLogin = date('Y-m-d', strtotime($persona_info['UltimoLogin']));
             if ($UltimoLogin < $hoy) {
                    $contadorlogindia = 1;
                }
                else {
                    $contadorlogindia = $persona_info['contadorlogindia'] + 1;
                }  
                
            $sql = "UPDATE personas SET UltimoLogin = NOW(),contadorlogindia='?', ip = NULLIF('?',''), usuario = NULLIF('?','') WHERE persona = '?'";
            $this->database->query($this->database->parse($sql,$contadorlogindia, filter_input(INPUT_SERVER, 'REMOTE_ADDR'), $this->getPersona(), $this->session->get('persona')));

            

            return TRUE;
        } else {
            return FALSE;
        }
    }

    function logout() {
        $this->session->delete('persona');
        $this->session->delete('nombre');
        $this->session->delete('apellido');
        $this->session->delete('menu');
        $this->session->delete('permisos');
        $this->session->delete('puntovtaasignado');
        $this->session->delete('descpuntovtaasignado');
        $this->session->delete('paginasPublicas');

        $this->session->finalizar();
        $this->data = array();
    }

    function isLogged() {
        //$ve = !empty($this->data);
        //$ve2 = $this->data;
        return !empty($this->data);
    }

    function getPersona() {
        return (isset($this->data['persona']) ? $this->data['persona'] : NULL);
    }

    function getNombre() {
        return (isset($this->data['nombre']) ? $this->data['nombre'] : NULL);
    }

    function getNombreCompleto() {
        return (isset($this->data['nombre']) && isset($this->data['apellido'])) ? $this->data['apellido'] . ', ' . $this->data['nombre'] : NULL;
    }

    function getApellido() {
        return (isset($this->data['apellido']) ? $this->data['apellido'] : NULL);
    }

    function getPuntovtaasignado() {
        return (isset($this->data['puntovtaasignado']) ? $this->data['puntovtaasignado'] : NULL);
    }
    
    function getDescpuntovtaasignado() {
        
        return (isset($this->data['descpuntovtaasignado']) ? ' - ' . $this->data['descpuntovtaasignado'] : NULL);
    }
    
    function setDescpuntovtaasignado($puntovtaasignado, $descpuntovtaasignado) {
        $this->session->set('puntovtaasignado', $puntovtaasignado);
        $this->session->set('descpuntovtaasignado', $descpuntovtaasignado);
    }
    
    function getExento() {
        return (isset($this->data['exento']) ? $this->data['exento'] : NULL);
    }

    function hasPermisos($persona, $formulario, $permiso) {
        if ($this->session->has('permisos')) {
            $this->permisos = $this->session->get('permisos', $this->permisos);
        } else {
            $sql = "SELECT DISTINCT f.formulario, f.permiso FROM perfiles f INNER JOIN grupos g ON f.grupo = g.grupo INNER JOIN personasgrupos p ON g.grupo = p.grupo WHERE p.persona = '?'";
            $permisos = $this->database->getRows($this->database->parse($sql, $persona, $formulario, $permiso));

            foreach ($permisos as $unPermiso) {
                $this->permisos[$unPermiso['formulario']][$unPermiso['permiso']] = true;
            }

            $this->session->set('permisos', $this->permisos);
        }
        return (isset($this->permisos[$formulario][$permiso])) ? true : false;
    }

    function getGrupos() {
        $sql = "SELECT Grupo FROM personasgrupos WHERE persona = '?'";
        return $this->database->getRows($this->database->parse($sql, $this->getPERSONA()));
    }

    function getNombresGrupos() {
        $sql = "SELECT descripcion FROM personasgrupos pg LEFT JOIN grupos g ON pg.grupo = g.grupo  WHERE pg.persona = '?'";
        $grupos = $this->database->getRows($this->database->parse($sql, $this->getPERSONA()));
        $NombresGrupos = '';
        $concat = '';
        foreach ($grupos as $value) {
            $NombresGrupos .= $value['descripcion'] . $concat;
            $concat = '<br>';
        }
        return $NombresGrupos;
    }

}

?>