<?php

class Common {

    function __construct(&$locator) {
        $this->database = & $locator->get('database');
        $this->user = & $locator->get('user');
        $this->request = & $locator->get('request');
        $this->session = & $locator->get('session');
        $this->sendgrid = & $locator->get('SendGrid');
        $this->template = & $locator->get('template');
    }

    function formatBytes($size, $precision = 2) {
        $base = log($size) / log(1024);
        $suffixes = array('b', 'Kb', 'Mb', 'Gb', 'Tb');

        return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
    }

    function ValidaURL($url) {
        if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
            return true;
        } else {
            return false;
        }
    }
    
    function validateCUIT($cuit) {
        $cadena = str_split($cuit);

        $result = $cadena[0]*5;
        $result += $cadena[1]*4;
        $result += $cadena[2]*3;
        $result += $cadena[3]*2;
        $result += $cadena[4]*7;
        $result += $cadena[5]*6;
        $result += $cadena[6]*5;
        $result += $cadena[7]*4;
        $result += $cadena[8]*3;
        $result += $cadena[9]*2;

        $div = intval($result/11);
        $resto = $result - ($div*11);

        if($resto==0){
        if($resto==$cadena[10]){
        return true;
        }else{
        return false;
        }
        }elseif($resto==1){
        if($cadena[10]==9 AND $cadena[0]==2 AND $cadena[1]==3){
        return true;
        }elseif($cadena[10]==4 AND $cadena[0]==2 AND $cadena[1]==3){
        return true;
        }
        }elseif($cadena[10]==(11-$resto)){
        return true;
        }else{
        return false;
        }
    }

    function GetaaaaMMddhhmmss() {
        //$ahora = getdate();
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $aaaaMMddhhmmss = date("YmdHis");
        return $aaaaMMddhhmmss;
    }

    function GetaaaaMMddhhmmssmm() {
        //$ahora = getdate();
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $aaaaMMddhhmmssmm = date("YmdHis");
        $aaaaMMddhhmmssmm = $aaaaMMddhhmmssmm . rand(10, 99);
        return $aaaaMMddhhmmssmm;
    }

    function getFormateoFecha($fecha) {
        $fecha_d = '';
        if ($fecha != '') {
            $dated = explode('/', $fecha);
            $fecha_d = date('Y-m-d', strtotime($dated[1] . '/' . $dated[0] . '/' . $dated[2]));
        } else {
            $fecha_d = '0000-00-00';
        }
        return $fecha_d;
    }

    function getEcriptar($password) {
        return md5($password);
    }

    function getFormateoFecha_d_m_a($fechadma, $caracter) {
        trim($fechadma);
        $trozos = explode($caracter, $fechadma);
        $dia = $trozos[0];
        $mes = $trozos[1];
        $año = $trozos[2];
        if (checkdate($mes, $dia, $año)) {
            return true;
        } else {
            return false;
        }
    }

    function getVerificarPassword($password_entered, $password_hash) {
        $rta = false;
        if (password_verify($password_entered, $password_hash)) {
            $rta = true;
        }
        return $rta;
    }

    function generar_clave($longitud) {
        $cadena = "[^A-Z0-9]";
        return substr(eregi_replace($cadena, "", md5(rand())) .
                eregi_replace($cadena, "", md5(rand())) .
                eregi_replace($cadena, "", md5(rand())), 0, $longitud);

        return $cadena;
    }

    function validarDuplicado($mail, $database, &$mensaje, $persona = '') {

        if (strlen($mail) == 0) {
            $mensaje = 'Debe ingresar el e-mail.';
            return false;
        }

        if (!$this->validarMail($mail)) {
            $mensaje = 'El e-mail ingresado es inv&aacute;lido.';
            return false;
        }

        //ESTA VALIDACION LA COMENTAMOS PARA QUE SE PUEDA REGISTRAR CON EL MISMO MAIL VARIAS PERSONAS
//            $cantMails = $this->database->getRow("SELECT count(mail) as cantMails FROM personas WHERE mail ='$mail'");
//                        
//            if($persona <> ''){ //si el mail ya estaba ingresado en la base y es de esa persona
//        
//                $mailPersona = $this->database->getRow("SELECT mail FROM personas WHERE persona ='$persona'");
//                
//                if($cantMails['cantMails'] > 0 && $mailPersona['mail'] <> $mail){
//                    $mensaje = 'El e-mail ingresado se encuentra asignado a otra persona.';
//                    return false;
//                }
//                
//            }elseif($cantMails['cantMails'] > 0){
//                $mensaje = 'El e-mail ingresado se encuentra asignado a otra persona.';
//                return false;
//            }

        return true;
    }

    function comprobar_email($email) {
        $mail_correcto = 0;
        //compruebo unas cosas primeras
        if ((strlen($email) >= 6) && (substr_count($email, "@") == 1) && (substr($email, 0, 1) != "@") && (substr($email, strlen($email) - 1, 1) != "@")) {
            if ((!strstr($email, "'")) && (!strstr($email, "\"")) && (!strstr($email, "\\")) && (!strstr($email, "\$")) && (!strstr($email, " "))) {
                //miro si tiene caracter .
                if (substr_count($email, ".") >= 1) {
                    //obtengo la terminacion del dominio
                    $term_dom = substr(strrchr($email, '.'), 1);
                    //compruebo que la terminación del dominio sea correcta
                    if (strlen($term_dom) > 1 && strlen($term_dom) < 5 && (!strstr($term_dom, "@"))) {
                        //compruebo que lo de antes del dominio sea correcto
                        $antes_dom = substr($email, 0, strlen($email) - strlen($term_dom) - 1);
                        $caracter_ult = substr($antes_dom, strlen($antes_dom) - 1, 1);
                        if ($caracter_ult != "@" && $caracter_ult != ".") {
                            $mail_correcto = 1;
                        }
                    }
                }
            }
        }
        if ($mail_correcto)
            return true;
        else
            return false;
    }

    function VerificarrDireccionCorreo($mail) {
        $Sintaxis = '#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#';
        if (preg_match($Sintaxis, $direccion))
            return true;
        else
            return false;
    }

    function validarMail($mail) {
        if (filter_var($mail, FILTER_VALIDATE_EMAIL))
            return true;
        else
            return false;
    }

    function GetNombreMes($cmes) {
        $mes = "";
        switch ($cmes) {
            case 1:
                $mes = "Enero";
                break;
            case 2:
                $mes = "Febrero";
                break;
            case 3:
                $mes = "Marzo";
                break;
            case 4:
                $mes = "Abril";
                break;
            case 5:
                $mes = "Mayo";
                break;
            case 6:
                $mes = "Junio";
                break;
            case 7:
                $mes = "Julio";
                break;
            case 8:
                $mes = "Agosto";
                break;
            case 9:
                $mes = "Septiembre";
                break;
            case 10:
                $mes = "Octubre";
                break;
            case 11:
                $mes = "Noviembre";
                break;
            case 12:
                $mes = "Diciembre";
                break;
        }
        return $mes;
    }

    function RestarHoras($horaini, $horafin) {
        $horai = substr($horaini, 0, 2);
        $mini = substr($horaini, 3, 2);
        $segi = substr($horaini, 6, 2);

        $horaf = substr($horafin, 0, 2);
        $minf = substr($horafin, 3, 2);
        $segf = substr($horafin, 6, 2);

        $ini = ((($horai * 60) * 60) + ($mini * 60) + $segi);
        $fin = ((($horaf * 60) * 60) + ($minf * 60) + $segf);

        $dif = $fin - $ini;

        $difh = floor($dif / 3600);
        $difm = floor(($dif - ($difh * 3600)) / 60);
        $difs = $dif - ($difm * 60) - ($difh * 3600);
        //return date("H:i:s",mktime($difh,$difm,$difs));
        return ($difh * 60) + $difm;
    }

    function enviarMailRegistro($nombre_destino, $mail_destino, $url_registro) {

        $email = new SendGrid\Email();
        $email
                ->addTo($mail_destino)
                ->setFrom(MAIL_CONTACTO)
                ->setSubject('Finalización de registro en xxxx')
                ->setHtml(' ')
                ->setFromName('Registro xxxx')
                ->setTemplateId(TEMPLATE_REGISTRO)
                ->setSubstitutions(array('%nombre%' => array(ucwords(strtolower($nombre_destino))),
                    '%link_registro%' => array($url_registro)))
        ;

        try {
            $this->sendgrid->send($email);
        } catch (\SendGrid\Exception $e) {
            $this->log_error_msg_sendgrid($e->getErrors());
        }
    }

    function enviarMailRecuperarPassword($nombre_destino, $mail_destino, $url_registro) {

        $email = new SendGrid\Email();
        $email
                ->addTo($mail_destino)
                ->setFrom(MAIL_CONTACTO)
                ->setSubject('Recuperar contraseña xxxxxx')
                ->setHtml(' ')
                ->setFromName('Coaching xxxxxxxxx')
                ->setTemplateId(TEMPLATE_RECUPERAR)
                ->setSubstitutions(array('%nombre%' => array(ucwords(strtolower($nombre_destino))),
                    '%link_registro%' => array($url_registro),
                    '%nueva_clave%' => array(substr(md5($nombre_destino), 0, 8)))) //Creo la clave usando el MD5 del nombre así la puedo generar de nuevo en el updatePassword
        ;

        try {
            $this->sendgrid->send($email);
        } catch (\SendGrid\Exception $e) {
            $this->log_error_msg_sendgrid($e->getErrors());
        }
    }

    function enviarMailRegistroAtrasado(&$url) {
        $sql = "SELECT persona, nombre, apellido, creacion, mail FROM personas WHERE mail_confirmado = 0";
        $results = getNombreCompletogetRows($sql);

        $i = 1;
        $mails = array();
        $nombres = array();
        $direcciones = array();
        $total = array();
        foreach ($results as $unaPersona) {
            $nombres[] = trim((trim($unaPersona['nombre']) != '') ? ucwords(strtolower($unaPersona['nombre'])) : ucwords(strtolower($unaPersona['apellido'])));
            $mails[] = trim(strtolower($unaPersona['mail']));
            $direcciones[] = $url->rawssl('registrar', 'update', array('persona' => $unaPersona['persona'], 'token' => sha1($unaPersona['persona'] . $unaPersona['creacion'])));

            if ($i == 1000) {
                $total[] = array('nombres' => $nombres, 'mails' => $mails, 'direcciones' => $direcciones);
                $i = 0;
                $mails = array();
                $nombres = array();
                $direcciones = array();
            }
            $i++;
        }

        if ($i > 1)
            $total[] = array('nombres' => $nombres, 'mails' => $mails, 'direcciones' => $direcciones);

//        $total = array();
//        
//        $total[] = array('nombres' => array('Matías'), 'mails' => array('maticomba@gmail.com'), 'direcciones' => array($url->rawssl('registrar','update',array('persona'=> '31385788', 'token' => sha1('31385788'.$unaPersona['creacion'])))));
//        $total[] = array('nombres' => array('Matías'), 'mails' => array('maticomba06@hotmail.com'), 'direcciones' => array('test'));
//        $total[] = array('nombres' => array('Matías1','Matías2'), 'mails' => array('maticomba06@hotmail.com','maticomba@gmail.com'), 'direcciones' => array('test','test'));

        print_r($total);

        foreach ($total as $unGrupo) {
//            echo "1000<br>";
//            for($i=0;$i < count($unGrupo['nombres']);$i++){
//                echo ($i+1).'- '.$unGrupo['nombres'][$i].';'.$unGrupo['mails'][$i].';'.$unGrupo['direcciones'][$i].'<br>';
//            }
            /* $email = new SendGrid\Email();
              $email
              ->setFrom(MAIL_CONTACTO)
              ->setSubject('Confirmar correo electrónico Coaching UAR')
              ->setHtml(' ')
              ->setFromName('Coaching UAR')
              ->setTemplateId('6950a4f4-6eec-4c60-8c00-f62151035f39')
              ->setSmtpapiTos($unGrupo['mails'])
              ->setSubstitutions(array('%nombre%'=>$unGrupo['nombres'],
              '%link_registro%' => $unGrupo['direcciones']))
              ;

              try {
              $this->sendgrid->send($email);
              } catch(\SendGrid\Exception $e) {
              $this->log_error_msg_sendgrid($e->getErrors());
              }

              sleep(2); */
        }
    }

    function enviarMailMasivos($asunto, $personas, $cuerpo, $mail_from_mail = false, $mail_from_nombre = false, $mail_template = false) {

        if (!$mail_from_mail)
            $mail_from_mail = MAIL_BATCH_FROM_MAIL;

        if (!$mail_from_nombre)
            $mail_from_nombre = MAIL_BATCH_FROM_NOMBRE;

        if (!$mail_template)
            $mail_template = MAIL_BATCH_TEMPLATE;

        $total = array();
        $log_envios = '';
        $error = false;

        $mails = array();
        $bodys = array();
        $body_preheaders = array();

        foreach ($personas as $unaPersona) {

            $mails[] = trim(strtolower($unaPersona['mail']));
            //$mails[] = trim(strtolower('ing.lucasbracamonte@gmail.com'));
            //$mails[] = trim(strtolower('lucasb@destelnorte.com.ar'));
            //           $mails[] = trim(strtolower('mafraya@outlook.com'));
            //$mails[] = trim(strtolower('marcelo.vaquero@hotmail.com'));
            //  $mails[] = trim(strtolower('maximiliano_mata@hotmail.com'));
//            $mails[] = trim(strtolower('martinmackey@gmail.com'));
            //$mails[] = array('mail' => 'marcelo.vaquero@hotmail.com'); 
            //$mails[] = array('mail' => 'mafraya@outlook.com'); 
            //$mails[] = array('mail' => 'ing.lucasbracamonte@gmail.com'); 
            //$mails[] = array('mail' => 'martinmackey@gmail.com'); 

            $bodys[] = $cuerpo;
            $body_preheaders[] = wordwrap($cuerpo, 50, ' ');

            if ($i == MAIL_PERSONAS_BATCH) {
                $total[] = array('mails' => $mails, 'bodys' => $bodys, 'body_preheaders' => $body_preheaders);
                $i = 0;
                $mails = array();
                $bodys = array();
                $body_preheaders = array();
            }
            $i++;
        }

        if ($i >= 1)
            $total[] = array('mails' => $mails, 'bodys' => $bodys, 'body_preheaders' => $body_preheaders);

        $log_envios .= date('c') . "\t INFO:\t" . "==================== Inicio batch envío de mails masivos ====================\n";
        $log_envios .= date('c') . "\t INFO:\t" . "Se enviaran " . count($personas) . " mails.\n";

        $i = 1;
        $cant_grupos = count($total);
        foreach ($total as $unGrupo) {
            $log_envios .= date('c') . "\t INFO:\t" . "Enviando grupo $i de $cant_grupos...\n";
            $email = new SendGrid\Email();
            $email
                    ->setFrom($mail_from_mail)
                    ->setSubject($asunto)
                    ->setHtml(' ')
                    ->addSubstitution('%body%', $unGrupo['bodys'])
                    ->addSubstitution('%body_preheader%', $unGrupo['body_preheaders'])
                    ->setFromName($mail_from_nombre)
                    ->setTemplateId($mail_template)
                    ->setSmtpapiTos($unGrupo['mails']);

            try {
                $this->sendgrid->send($email);
            } catch (\SendGrid\Exception $e) {
                $this->log_error_msg_sendgrid($e->getErrors());
                $log_envios .= date('c') . "\t ERROR:\t" . $e->getErrors();
                $error = $e->getErrors();
            }

            if ($error) {
                break;
            } else {
                $i++;
                sleep(2);
            }
        }

        if ($error) {
            $log_envios .= date('c') . "\t INFO:\t" . "==================== Finalizado con errores ====================\n";
            return $error;
        } else {
            $log_envios .= date('c') . "\t INFO:\t" . "==================== Finalizado correctamente ====================\n";
            return true;
        }
    }

    function log_msg($msg, $carpeta) {

        $log_path = DIR_BASE . 'logs' . D_S . $carpeta . D_S;

        if (is_writable($log_path)) {
            $this->log_file = $log_path . 'logs-' . date("Ymd") . '.txt';
        } else {
            $this->log_file = FALSE;
        }

        if ($this->log_file) {
            $this->log_message = '';


            $message = "time: " . date("j-m-d H:i:s (T)", time()) . "\r\n";
            $message .= $msg . "\r\n";
            $message .= "##################################################\r\n\r\n";
            if (!$fp = fopen($this->log_file, 'a+')) {
                $this->log_message = "Could not open/create file: $this->log_file to log error.";
                $log_error = true;
            }
            if (!fwrite($fp, $message)) {
                $this->log_message = "Could not log error to file: $this->log_file. Write Error.";
                $log_error = true;
            }

            fclose($fp);
        }
    }
    
    function log_error_msg_sendgrid($error) {

        $log_path = DIR_BASE . 'logs' . D_S . 'error_log' . D_S;

        if (is_writable($log_path)) {
            $this->log_file = $log_path . 'error-' . date("Ymd") . '.txt';
        } else {
            $this->log_file = FALSE;
        }

        if ($this->log_file) {
            $this->log_message = '';
            $list_error = '';
            foreach ($error as $er) {
                $list_error .= str_replace(array('<br />', '<br>'), "\n", $er) . '\n';
            }

            $message = "time: " . date("j-m-d H:i:s (T)", time()) . "\n";
            $message .= "SendGrid " . $list_error . "\n";
            $message .= isset($_SERVER['REQUEST_URI']) ? 'Path: ' . @$_SERVER['REQUEST_URI'] . "\n" : "";
            $message .= isset($_SERVER['QUERY_STRING']) ? 'Query String: ' . @$_SERVER['QUERY_STRING'] . "\n" : "";
            $message .= isset($_SERVER['HTTP_REFERER']) ? 'HTTP Referer: ' . @$_SERVER['HTTP_REFERER'] . "\n" : "";
            $message .= 'IP:' . $_SERVER['REMOTE_ADDR'] . ' Remote Host:' . (isset($_SERVER['REMOTE_HOST']) ? @$_SERVER['REMOTE_HOST'] : gethostbyaddr($_SERVER['REMOTE_ADDR'])) . "\n";
            $message .= "##################################################\n\n";
            if (!$fp = fopen($this->log_file, 'a+')) {
                $this->log_message = "Could not open/create file: $this->log_file to log error.";
                $log_error = true;
            }
            if (!fwrite($fp, $message)) {
                $this->log_message = "Could not log error to file: $this->log_file. Write Error.";
                $log_error = true;
            }
            if (!$this->log_message) {
                $this->log_message = "Error was logged to file: $this->log_file.";
            }
            fclose($fp);

            if ($this->log_message) {
                echo $this->log_message;
            }
        }
    }

    function log_error_msg_ws($error) {

        $log_path = DIR_BASE . 'logs' . D_S . 'error_log' . D_S;

        if (is_writable($log_path)) {
            $this->log_file = $log_path . 'error-' . date("Ymd") . '.txt';
        } else {
            $this->log_file = FALSE;
        }

        if ($this->log_file) {
            $this->log_message = '';
            $list_error = '';

            if (is_array($error)) {
                foreach ($error as $er) {
                    $list_error .= str_replace(array('<br />', '<br>'), "\n", $er) . '\n';
                }
            } else {
                $list_error .= str_replace(array('<br />', '<br>'), "\n", $error) . '\n';
            }

            if (!strstr($list_error, 'documento desconocido')) {
                $message = "time: " . date("j-m-d H:i:s (T)", time()) . "\n";
                $message .= "WebServiceJugadores " . $list_error . "\n";
                $message .= isset($_SERVER['REQUEST_URI']) ? 'Path: ' . @$_SERVER['REQUEST_URI'] . "\n" : "";
                $message .= isset($_SERVER['QUERY_STRING']) ? 'Query String: ' . @$_SERVER['QUERY_STRING'] . "\n" : "";
                $message .= isset($_SERVER['HTTP_REFERER']) ? 'HTTP Referer: ' . @$_SERVER['HTTP_REFERER'] . "\n" : "";
                $message .= 'IP:' . $_SERVER['REMOTE_ADDR'] . ' Remote Host:' . (isset($_SERVER['REMOTE_HOST']) ? @$_SERVER['REMOTE_HOST'] : gethostbyaddr($_SERVER['REMOTE_ADDR'])) . "\n";
                $message .= "##################################################\n\n";
                if (!$fp = fopen($this->log_file, 'a+')) {
                    $this->log_message = "Could not open/create file: $this->log_file to log error.";
                    $log_error = true;
                }
                if (!fwrite($fp, $message)) {
                    $this->log_message = "Could not log error to file: $this->log_file. Write Error.";
                    $log_error = true;
                }
                if (!$this->log_message) {
                    $this->log_message = "Error was logged to file: $this->log_file.";
                }
                fclose($fp);
            }

//                    if($this->log_message){
//                        echo $this->log_message;
//                    }
        }
    }

    function enviarMailConstanciaReparacion($persona, $reparacion) {

        //$template->set('title', 'Envío de notificaciones por mails');
//		if (($this->validateForm())) 
//		{
        //$view = $this->template->create('template');                    
//                    if(count($request->get('actividad', 'post',array())) > 0){
//                        $view->set('numero', 1);
//                        $view->set('actividades', $request->get('actividad', 'post',array()));                        
//                    }
        $sql = "SELECT * FROM personas WHERE persona ='" . $persona . "'";
        $persona_info = $this->database->getRow($sql);

        $sql = "SELECT * FROM vw_grilla_reparaciones WHERE reparacion ='" . $reparacion . "'";
        $reparacion_info = $this->database->getRow($sql);

        // <editor-fold defaultstate="collapsed" desc="TEXTO">
        $texto = "<div align=center>";
        $texto .= "<b>MUCHAS GRACIAS POR CONFIAR EN NOSOTROS PARA LA REPARACIÓN DE SU EQUIPO</b> <br><br>";
        $texto .="</div>";
        $texto .="<div>";
        $texto .= "<b>Detalle de la orden de reparación</b> <br>";

        $datosReparacion = '';
        $texto .='<br><b>Nro de Orden:</b> ' . $reparacion_info['reparacion'] . '<br><b>Fecha:</b> ' . $reparacion_info['fecha'] . '<br><b>Lugar ingreso:</b> ' . $reparacion_info['descpuntovta'] . '<br><b>Estado:</b> ' . $reparacion_info['descestadoreparacion'];
        //$view->set('datosReparacion', $datosReparacion);

        $datosCliente = '';
        $texto .='<br><br><b>Documento:</b> ' . $reparacion_info['cliente'] . '<br><b>Apellido y nombre:</b> ' . $reparacion_info['apellidocliente'] . ', ' . $reparacion_info['nombrecliente'] . '<br><b>Teléfono:</b> ' . $reparacion_info['telefonocliente'] . '<br><b>Mail:</b> ' . $reparacion_info['mailcliente'];
        //$view->set('datosCliente', $datosCliente);

        $datosEquipo = '';
        $engarantia = 'No.';
        if (@$reparacion_info['engarantia'] == 1) {
            $engarantia = 'Si.';
        }
        $texto .='<br><br><b>Nro Serie:</b> ' . $reparacion_info['nroserie'] . '<br><b>Tipo:</b> ' . $reparacion_info['desctipoproducto'] . '<br><b>Modelo:</b> ' . $reparacion_info['tipomarcamodelo'] . '<br><b>En garantía?:</b> ' . $engarantia;
        //$view->set('datosEquipo', $datosEquipo);

        $datosFalla = '';
        $texto .='<br><br><b>Accesorios:</b> ' . $reparacion_info['accesorios'] . '<br><br><b>Falla:</b> ' . $reparacion_info['descripcionfalla'];
        //$view->set('datosFalla', $datosFalla);

        $texto .= "<br><br>";

        $texto .="</div>";
        // </editor-fold>

        $this->template->set('texto_cuerpo', $texto);

        $cuerpo = $this->template->fetch('content/newsletter_layout_mail.tpl');
        $asunto = "Orden Nro: " . $reparacion;


        //$mailprueba = 'ing.lucasbracamonte@gmail.com';
        $mails[] = array();
        $mails[] = array('mail' => $persona_info['mail']);
        //$mails[] = array('mail' => $mail['mail']);
//                    foreach ($listamails as $mail) {
//                      $mails[] = array('mail' => $mail['mail']);   
//                    }
        // $mails[] = array('mail' => 'ing.lucasbracamonte@gmail.com'); 


        $respuesta_envio = $this->enviarMailMasivos($asunto, $mails, $cuerpo);

        if ($respuesta_envio == true) {
            $respuesta_envio = 'enviado ok';
        }

        //$response->redirect($url->ssl('home'));
        // $template->set('content', $this->getForm());
        // $template->set($module->fetch());
        // $response->set($template->fetch('layout.tpl'));
    }

    function enviarMailAvisoLlamarACliente($mail_responsable, $reparacion, $textofalla, $observaciontecnico) {

        //$template->set('title', 'Envío de notificaciones por mails');
//		if (($this->validateForm())) 
//		{
        //$view = $this->template->create('template');                    
//                    if(count($request->get('actividad', 'post',array())) > 0){
//                        $view->set('numero', 1);
//                        $view->set('actividades', $request->get('actividad', 'post',array()));                        
//                    }

        $sql = "SELECT * FROM vw_grilla_reparaciones WHERE reparacion ='" . $reparacion . "'";
        $reparacion_info = $this->database->getRow($sql);

        // <editor-fold defaultstate="collapsed" desc="TEXTO">
        $texto = "<div align=center>";
        $texto .= "<b>Llamar al cliente para aceptación de valor de reparación.</b> <br><br>";
        $texto .="</div>";
        $texto .="<div>";
        $texto .= "<b>Detalle de la orden de reparación</b> <br>";

        $datosReparacion = '';
        $texto .='<br><b>Nro de Orden:</b> ' . $reparacion_info['reparacion'] . '<br><b>Fecha:</b> ' . $reparacion_info['fecha'] . '<br><b>Lugar ingreso:</b> ' . $reparacion_info['descpuntovta'] . '<br><b>Estado:</b> ' . $reparacion_info['descestadoreparacion'];
        //$view->set('datosReparacion', $datosReparacion);

        $datosCliente = '';
        $texto .='<br><br><b>Documento:</b> ' . $reparacion_info['cliente'] . '<br><b>Apellido y nombre:</b> ' . $reparacion_info['apellidocliente'] . ', ' . $reparacion_info['nombrecliente'] . '<br><b>Teléfono:</b> ' . $reparacion_info['telefonocliente'] . '<br><b>Mail:</b> ' . $reparacion_info['mailcliente'];
        //$view->set('datosCliente', $datosCliente);

        $datosEquipo = '';
        $engarantia = 'No.';
        if (@$reparacion_info['engarantia'] == 1) {
            $engarantia = 'Si.';
        }
        $texto .='<br><br><b>Nro Serie:</b> ' . $reparacion_info['nroserie'] . '<br><b>Tipo:</b> ' . $reparacion_info['desctipoproducto'] . '<br><b>Modelo:</b> ' . $reparacion_info['tipomarcamodelo'] . '<br><b>En garanctía?:</b> ' . $engarantia;
        //$view->set('datosEquipo', $datosEquipo);

        $datosFalla = '';
        $texto .='<br><br><b>Accesorios:</b> ' . $reparacion_info['accesorios'] . '<br><br><b>Descripcion de la Falla:</b> ' . $reparacion_info['descripcionfalla'];
        //$view->set('datosFalla', $datosFalla);

        $datosFalla = '';
        $texto .='<br><br><b>Falla detectada:</b> ' . $textofalla;
        $texto .='<br><br><b>Observación del técnico:</b> ' . $observaciontecnico;

        $texto .= "<br><br>";

        $texto .="<b>VALOR DE LA REPARACIÓN: $" . $reparacion_info['precioreparacion'] . " </b>";

        $texto .="</div>";
        // </editor-fold>

        $this->template->set('texto_cuerpo', $texto);

        $cuerpo = $this->template->fetch('content/newsletter_layout_mail.tpl');
        $asunto = "Orden Nro: " . $reparacion;


        //$mailprueba = 'ing.lucasbracamonte@gmail.com';
        $mails[] = array();
        $mails[] = array('mail' => $mail_responsable);
        //$mails[] = array('mail' => $mail['mail']);
//                    foreach ($listamails as $mail) {
//                      $mails[] = array('mail' => $mail['mail']);   
//                    }
        // $mails[] = array('mail' => 'ing.lucasbracamonte@gmail.com'); 


        $respuesta_envio = $this->enviarMailMasivos($asunto, $mails, $cuerpo);

        if ($respuesta_envio == true) {
            $respuesta_envio = 'enviado ok';
        }

        //$response->redirect($url->ssl('home'));
        // $template->set('content', $this->getForm());
        // $template->set($module->fetch());
        // $response->set($template->fetch('layout.tpl'));
    }

    function enviarMailAvisoRetirar($reparacion) {

        //$template->set('title', 'Envío de notificaciones por mails');
//		if (($this->validateForm())) 
//		{
        //$view = $this->template->create('template');                    
//                    if(count($request->get('actividad', 'post',array())) > 0){
//                        $view->set('numero', 1);
//                        $view->set('actividades', $request->get('actividad', 'post',array()));                        
//                    }

        $sql = "SELECT * FROM vw_grilla_reparaciones WHERE reparacion ='" . $reparacion . "'";
        $reparacion_info = $this->database->getRow($sql);
        $sql = "SELECT direccion FROM puntosdeventa WHERE puntovta ='" . $reparacion_info['lugarentrega'] . "'";
        $direccionentrega = $this->database->getRow($sql);

        // <editor-fold defaultstate="collapsed" desc="TEXTO">
        $texto = "<div align=center>";
        $texto .= "<b>ORDEN FINALIZADA.</b> <br><br>";
        $texto .="</div>";
        $texto .="<div>";

        $texto .='<br>Estimada/o  ' . $reparacion_info['nombreapellidocliente'] . ' el equipo con Nro Serie ' . $reparacion_info['nroserie'] . ' de la Orden Nro ' . $reparacion_info['reparacion'] . ', se encuentra en el lugar de entrega (' . $direccionentrega['$direccion'] . ') listo para ser retirado. <br><br> Lo esperamos. <br><br> Saludos cordiales. <br> ';
        //$view->set('datosReparacion', $datosReparacion);
        // </editor-fold>

        $this->template->set('texto_cuerpo', $texto);

        $cuerpo = $this->template->fetch('content/newsletter_layout_mail.tpl');
        $asunto = "Orden Nro: " . $reparacion . " lista para retirar";


        //$mailprueba = 'ing.lucasbracamonte@gmail.com';
        $mails[] = array();
        $mails[] = array('mail' => $reparacion_info['mailcliente']);
        //$mails[] = array('mail' => $mail['mail']);
//                    foreach ($listamails as $mail) {
//                      $mails[] = array('mail' => $mail['mail']);   
//                    }
        // $mails[] = array('mail' => 'ing.lucasbracamonte@gmail.com'); 


        $respuesta_envio = $this->enviarMailMasivos($asunto, $mails, $cuerpo);

        if ($respuesta_envio == true) {
            $respuesta_envio = 'enviado ok';
        }

        //$response->redirect($url->ssl('home'));
        // $template->set('content', $this->getForm());
        // $template->set($module->fetch());
        // $response->set($template->fetch('layout.tpl'));
    }

    function EsVendedor($persona) {
        $es = false;
        $grupos = $this->user->getGrupos();
        foreach ($grupos as $unGrupo) {
            if ($unGrupo['Grupo'] == 'VE') {
                $es = true;
            }
        }

        return $es;
    }

    //va aca y no en el usuario porque puede cambiar sin cerrar sesion
    function getPuntoVtaAsignado() {
        $ptovtaasignado = $this->database->getRow("SELECT p.puntovtaasignado, pv.descripcion as descpuntovta, pv.listaprecio "
                . "FROM personas p "
                . "LEFT JOIN puntosdeventa pv ON p.puntovtaasignado = pv.puntovta where p.persona = '" . $this->user->getPERSONA() . "' ");

        return $ptovtaasignado;
    }

    function EsTecnico($persona) {
        $es = false;
        $grupos = $this->user->getGrupos();
        foreach ($grupos as $unGrupo) {
            if ($unGrupo['Grupo'] == 'TE') {
                $es = true;
            }
        }

        return $es;
    }

    function generaNumRepuesto($tipo) {

        $sql = "SELECT * FROM tiposrepuesto WHERE tiporepuesto ='" . utf8_encode($tipo) . "'";
        $repuesto_info = $this->database->getRow($sql);
        $cantidaddedigitos = strlen($repuesto_info['ultimonumero']);
        $numero = $repuesto_info['ultimonumero'];
        switch ($cantidaddedigitos) {
            case 1: $numero = '000000' . $numero;
                break;
            case 2: $numero = '00000' . $numero;
                break;
            case 3: $numero = '0000' . $numero;
                break;
            case 4: $numero = '000' . $numero;
                break;
            case 5: $numero = '00' . $numero;
                break;
            case 6: $numero = '0' . $numero;
                break;
            default:
                $numero = $numero;
                break;
        }
        $NroParte = $repuesto_info['codidentificador'] . '-' . $numero;

        $sql = "UPDATE tiposrepuesto SET ultimonumero=(ultimonumero+1) WHERE tiporepuesto='" . $repuesto_info['tiporepuesto'] . "'";
        $this->database->query($sql);

        return $NroParte;
    }

    function generaCodInternoProducto($tipo) {

        $sql = "SELECT * FROM tiposproducto WHERE tipoproducto ='" . utf8_encode($tipo) . "'";
        $producto_info = $this->database->getRow($sql);
        $cantidaddedigitos = strlen($producto_info['ultimonumero']);
        $numero = $producto_info['ultimonumero'];
        switch ($cantidaddedigitos) {
            case 1: $numero = '000000' . $numero;
                break;
            case 2: $numero = '00000' . $numero;
                break;
            case 3: $numero = '0000' . $numero;
                break;
            case 4: $numero = '000' . $numero;
                break;
            case 5: $numero = '00' . $numero;
                break;
            case 6: $numero = '0' . $numero;
                break;
            default:
                $numero = $numero;
                break;
        }
        $CodInterno = $producto_info['codidentificador'] . '-' . $numero;

        $sql = "UPDATE tiposproducto SET ultimonumero=(ultimonumero+1) WHERE tipoproducto='" . $producto_info['tipoproducto'] . "'";
        $this->database->query($sql);

        return $CodInterno;
    }
}

?>
