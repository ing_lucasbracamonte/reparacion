<?php
include '../src/Afip.php'; 
//$voucher = array(
//    'TipoComprobante' 		=>1, 
//    'letra'                     =>6,
//    'numeroPuntoVenta' 		=>1,
//    'fechaComprobante' 		=>intval(date('Ymd')),
//    'codigoTipoComprobante' 	=>6,
//    'codigoConcepto' 		=>1,
//    'fechaDesde' 		=>NULL,
//    'fechaHasta' 		=>NULL,
//    'fechaVtoPago' 		=>NULL,
//    'TipoDocumento' 		=>80, // Tipo de documento del comprador (ver tipos disponibles)
//    'numeroDocumento' 		=>20303088661, 
//    'nombreCliente' 		=>'Braca Lucas',
//    'tipoResponsable' 		=>5,
//    'domicilioCliente' 		=>'plf 1377',
//    'CondicionVenta' 		=>NULL,
//    'items' => array (
//        'scanner' 		=>0000,
//        'codigo' 		=>1234567890123,
//        'descripcion' 		=>'palo y a la bolsa',
//        'cantidad' 		=>1,
//        'UnidadMedida' 		=>'UN',
//        'precioUnitario' 	=>115,
//        'porcBonif' 		=>0,
//        'impBonif' 		=>0,
//        'Alic'                  =>0,
//        'importeItem' 		=>115
//        ),
//    'Tributos' => array (
//        'Desc' 		=>'Ingresos Brutos',
//        'Importe'	=>7.8
//        ),
//    'importeOtrosTributos' 	=>0,
//    'codigoMoneda' 		=>'PES',
//    'importeGravado' 		=>115,
//    'subtotivas' 		=>115,
//    'importeTotal' 		=>115,
//    'cae'                       =>NULL,
//    'fechaVencimientoCAE' 	=>NULL,
//    'cotizacionMoneda' 		=>1, 
//);

$data = array(
	'CantReg' 		=> 1, // Cantidad de comprobantes a registrar
	'PtoVta' 		=> 1, 
	'CbteTipo' 		=> 6,  
	'Concepto' 		=> 1, 
	'DocTipo' 		=> 80, 
	'DocNro' 		=> 20303088661, 
	'CbteDesde' 	=> 1, 
	'CbteHasta' 	=> 1, // Numero de comprobante o numero del ultimo comprobante en caso de ser mas de uno
	'CbteFch' 		=> intval(date('Ymd')), 
	'ImpTotal' 		=> 184.05, 
	'ImpTotConc' 	=> 0, 
	'ImpNeto' 		=> 150, 
	'ImpOpEx' 		=> 0, // Importe exento de IVA
	'ImpIVA' 		=> 26.25, 
	'ImpTrib' 		=> 7.8, 
	'FchServDesde' 	=> NULL, // (Opcional) Fecha de inicio del servicio (yyyymmdd), obligatorio para Concepto 2 y 3
	'FchServHasta' 	=> NULL, // (Opcional) Fecha de fin del servicio (yyyymmdd), obligatorio para Concepto 2 y 3
	'FchVtoPago' 	=> NULL, // (Opcional) Fecha de vencimiento del servicio (yyyymmdd), obligatorio para Concepto 2 y 3
	'MonId' 		=> 'PES', 
	'MonCotiz' 		=> 1,  
	'CbtesAsoc' 	=> array( // (Opcional) Comprobantes asociados
		array(
			'Tipo' 		=> 6, // Tipo de comprobante (ver tipos disponibles) 
			'PtoVta' 	=> 1, // Punto de venta
			'Nro' 		=> 1, // Numero de comprobante
			'Cuit' 		=> 20111111112 // (Opcional) Cuit del emisor del comprobante
			)
		),
	'Tributos' 		=> array( // (Opcional) Tributos asociados al comprobante
		array(
			'Id' 		=>  99, // Id del tipo de tributo (ver tipos disponibles) 
			'Desc' 		=> 'Ingresos Brutos', 
			'BaseImp' 	=> 150, // Base imponible para el tributo
			'Alic' 		=> 5.2, // Alícuota
			'Importe' 	=> 7.8 
		)
	), 
	'Iva' 			=> array( // (Opcional) Alícuotas asociadas al comprobante
		array(
			'Id' 		=> 5, // Id del tipo de IVA (ver tipos disponibles) 
			'BaseImp' 	=> 100, // Base imponible
			'Importe' 	=> 21 // Importe 
		)
	), 
	'Opcionales' 	=> array( // (Opcional) Campos auxiliares
		array(
			'Id' 		=> 17, // Codigo de tipo de opcion (ver tipos disponibles) 
			'Valor' 	=> 2 // Valor 
		)
	), 
	'Compradores' 	=> array( // (Opcional) Detalles de los clientes del comprobante 
		array(
			'DocTipo' 		=> 80, // Tipo de documento (ver tipos disponibles) 
			'DocNro' 		=> 20303088661, // Numero de documento
			'Porcentaje' 	=> 100 // Porcentaje de titularidad del comprador
		)
	)
);

$afip = new Afip(array('CUIT' => 20303088661));

$afip->ElectronicBilling->CreateVoucher($data);

?>