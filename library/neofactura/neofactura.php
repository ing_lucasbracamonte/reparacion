<?php

include_once (__DIR__ . '/wsfev1.php');
include_once (__DIR__ . '/wsfexv1.php');
include_once (__DIR__ . '/wsaa.php');
include_once (__DIR__ . '/neopdf/pdf_voucher.php');

class Neofactura {

    private $CUIT = '';
    private $MODO = '';
     
    function __construct(&$locator) {
        $this->database = & $locator->get('database');
        $this->user = & $locator->get('user');
        $this->request = & $locator->get('request');
        $this->session = & $locator->get('session');
        $this->template = & $locator->get('template');
        //$this->neopdf = & $locator->get('neopdf');
        
        $this->CUIT = 30711081557;
        $this->MODO = Wsaa::MODO_HOMOLOGACION;
        //$MODO = Wsaa::MODO_PRODUCCION;
    }

    function emitirComprobante($voucher) {
        $es = false;
        $afip = new Wsfev1($this->CUIT,$this->MODO);
        $result = $afip->init();
        
        $error = '';
        if ($result["code"] === Wsfev1::RESULT_OK) {
            $result = $afip->dummy();
            if ($result["code"] === Wsfev1::RESULT_OK) {
            // <editor-fold defaultstate="collapsed" desc="EJECUTA">    
                $result = $afip->emitirComprobante($voucher);

                // </editor-fold>
            } else {
                $error .= $result["msg"] . "\n";
            }
        } else {
            $error .= $result["msg"] . "\n";
        }

        
        
        if ($error) {
            $this->log_error($error);
            $es = false;
        } 

        return $result;
    }
    
    function ImprimirFactura($voucher, $vconfig) {
        $es = false;
        $afip = new Wsfev1($this->CUIT,$this->MODO);
        $result = $afip->init();
        
        $error = '';
        if ($result["code"] === Wsfev1::RESULT_OK) {
            $result = $afip->dummy();
            if ($result["code"] === Wsfev1::RESULT_OK) {
            // <editor-fold defaultstate="collapsed" desc="EJECUTA">    
                //OBTENER ULTIMO NUMERO DE COMPROBANTE
                // Ultimo comprobante autorizado, a este le sumo uno para procesar el siguiente.
                $Comprobante = $afip->consultarUltimoComprobanteAutorizado($voucher['TipoComprobante'], $voucher['numeroPuntoVenta']);
                
                $numeroComprobante = $Comprobante['number'] + 1;
                //OBTENER CAE
                //OBTENER FECHA VENCIMIENTO CAE
                
                $voucher['cae'] = '';
                $voucher['fechaVencimientoCAE'] = '';
                $voucher['numeroComprobante'] = $numeroComprobante;
                
             //$rta = $afip->emitirComprobante($voucher);  
             //if ($rta["code"] != Wsfev1::RESULT_OK) {
                 //$error .= $rta["msg"] . "\n";
             //}   
             
             
            
            ob_start();
            $logo_path = __DIR__ . '/neopdf/image/uno.jpg';
            $pdf = new PDFVoucher($voucher, $vconfig);
            $pdf->emitirPDF($logo_path);
            //ob_end_clean();//rompimiento de pagina
            ob_clean();
            $pdf->Output("factura.pdf","I");
                // </editor-fold>
            } else {
                $error .= $result["msg"] . "\n";
            }
        } else {
            $error .= $result["msg"] . "\n";
        }

        
        
        if ($error) {
            $this->log_error($error);
            $es = false;
            //actualizo en la venta el log
        } 
        else{
            //actualizo en la venta el cae el id del log la fecha del cae y nro de factura
        }

        return $es;
        
    }
    
    function consultarUltimoComprobanteAutorizado($PV, $TC) {
        $es = false;
        $afip = new Wsfev1($this->CUIT,$this->MODO);
        $result = $afip->init();
        
        $error = '';
        if ($result["code"] === Wsfev1::RESULT_OK) {
            $result = $afip->dummy();
            if ($result["code"] === Wsfev1::RESULT_OK) {
            // <editor-fold defaultstate="collapsed" desc="EJECUTA">    
                $result = $afip->consultarUltimoComprobanteAutorizado($PV, $TC);

                // </editor-fold>
            } else {
                return array("code" => Wsfev1::RESULT_ERROR, "msg" => Wsfev1::MSG_AFIP_CONNECTION . $e->getMessage(), "datos" => NULL);
            }
        } else {
            return array("code" => Wsfev1::RESULT_ERROR, "msg" => Wsfev1::MSG_AFIP_CONNECTION . $e->getMessage(), "datos" => NULL);
        }

        
        
        if ($error) {
            $this->log_error($error);
            $es = false;
        } 

        return $result;
    }
    
    function ExistePuntoVta($prefijo) {
        $es = false;
        $afip = new Wsfev1($this->CUIT,$this->MODO);
        $result = $afip->init();
        
        $error = '';
        if ($result["code"] === Wsfev1::RESULT_OK) {
            $result = $afip->dummy();
            if ($result["code"] === Wsfev1::RESULT_OK) {
            // <editor-fold defaultstate="collapsed" desc="EJECUTA">    
                $results = $afip->consultarPuntosVenta();

                foreach ($results as $puntosvta) {
//                    foreach ($puntosvta['datos'] as $pv) {
//                       if ($prefijo == $puntovta[]) {
//                        $es = true;
//                       } 
//                    }
                    
                }
                // </editor-fold>
            } else {
                $error .= $result["msg"] . "\n";
            }
        } else {
            $error .= $result["msg"] . "\n";
        }

        
        
        if ($error) {
            $this->log_error($error);
            $es = false;
        } 

        return $es;
    }
    
    function log_error($error) {

        $log_path = DIR_BASE . 'logs' . D_S . 'afip' . D_S .'error_log' . D_S;

        if (is_writable($log_path)) {
            $this->log_file = $log_path . 'error-' . date("Ymd") . '.txt';
        } else {
            $this->log_file = FALSE;
        }

        if ($this->log_file) {
            $this->log_message = '';
            $list_error = '';

            if (is_array($error)) {
                foreach ($error as $er) {
                    $list_error .= str_replace(array('<br />', '<br>'), "\n", $er) . '\n';
                }
            } else {
                $list_error .= str_replace(array('<br />', '<br>'), "\n", $error) . '\n';
            }

            if (!strstr($list_error, 'documento desconocido')) {
                $message = "time: " . date("j-m-d H:i:s (T)", time()) . "\n";
                $message .= "WebServiceAFIP " . $list_error . "\n";
                $message .= "##################################################\n\n";
                if (!$fp = fopen($this->log_file, 'a+')) {
                    $this->log_message = "Could not open/create file: $this->log_file to log error.";
                    $log_error = true;
                }
                if (!fwrite($fp, $message)) {
                    $this->log_message = "Could not log error to file: $this->log_file. Write Error.";
                    $log_error = true;
                }
                if (!$this->log_message) {
                    $this->log_message = "Error was logged to file: $this->log_file.";
                }
                fclose($fp);
            }

        }
    }

    
    function enviarMailMasivos($asunto, $personas, $cuerpo, $mail_from_mail = false, $mail_from_nombre = false, $mail_template = false) {

        if (!$mail_from_mail)
            $mail_from_mail = MAIL_BATCH_FROM_MAIL;

        if (!$mail_from_nombre)
            $mail_from_nombre = MAIL_BATCH_FROM_NOMBRE;

        if (!$mail_template)
            $mail_template = MAIL_BATCH_TEMPLATE;

        $total = array();
        $log_envios = '';
        $error = false;

        $mails = array();
        $bodys = array();
        $body_preheaders = array();

        foreach ($personas as $unaPersona) {

            $mails[] = trim(strtolower($unaPersona['mail']));
            //$mails[] = trim(strtolower('ing.lucasbracamonte@gmail.com'));
            //$mails[] = trim(strtolower('lucasb@destelnorte.com.ar'));
            //           $mails[] = trim(strtolower('mafraya@outlook.com'));
            //$mails[] = trim(strtolower('marcelo.vaquero@hotmail.com'));
            //  $mails[] = trim(strtolower('maximiliano_mata@hotmail.com'));
//            $mails[] = trim(strtolower('martinmackey@gmail.com'));
            //$mails[] = array('mail' => 'marcelo.vaquero@hotmail.com'); 
            //$mails[] = array('mail' => 'mafraya@outlook.com'); 
            //$mails[] = array('mail' => 'ing.lucasbracamonte@gmail.com'); 
            //$mails[] = array('mail' => 'martinmackey@gmail.com'); 

            $bodys[] = $cuerpo;
            $body_preheaders[] = wordwrap($cuerpo, 50, ' ');

            if ($i == MAIL_PERSONAS_BATCH) {
                $total[] = array('mails' => $mails, 'bodys' => $bodys, 'body_preheaders' => $body_preheaders);
                $i = 0;
                $mails = array();
                $bodys = array();
                $body_preheaders = array();
            }
            $i++;
        }

        if ($i >= 1)
            $total[] = array('mails' => $mails, 'bodys' => $bodys, 'body_preheaders' => $body_preheaders);

        $log_envios .= date('c') . "\t INFO:\t" . "==================== Inicio batch envío de mails masivos ====================\n";
        $log_envios .= date('c') . "\t INFO:\t" . "Se enviaran " . count($personas) . " mails.\n";

        $i = 1;
        $cant_grupos = count($total);
        foreach ($total as $unGrupo) {
            $log_envios .= date('c') . "\t INFO:\t" . "Enviando grupo $i de $cant_grupos...\n";
            $email = new SendGrid\Email();
            $email
                    ->setFrom($mail_from_mail)
                    ->setSubject($asunto)
                    ->setHtml(' ')
                    ->addSubstitution('%body%', $unGrupo['bodys'])
                    ->addSubstitution('%body_preheader%', $unGrupo['body_preheaders'])
                    ->setFromName($mail_from_nombre)
                    ->setTemplateId($mail_template)
                    ->setSmtpapiTos($unGrupo['mails']);

            try {
                $this->sendgrid->send($email);
            } catch (\SendGrid\Exception $e) {
                $this->log_error_msg_sendgrid($e->getErrors());
                $log_envios .= date('c') . "\t ERROR:\t" . $e->getErrors();
                $error = $e->getErrors();
            }

            if ($error) {
                break;
            } else {
                $i++;
                sleep(2);
            }
        }

        if ($error) {
            $log_envios .= date('c') . "\t INFO:\t" . "==================== Finalizado con errores ====================\n";
            return $error;
        } else {
            $log_envios .= date('c') . "\t INFO:\t" . "==================== Finalizado correctamente ====================\n";
            return true;
        }
    }

    function log_msg($msg, $carpeta) {

        $log_path = DIR_BASE . 'logs' . D_S . $carpeta . D_S;

        if (is_writable($log_path)) {
            $this->log_file = $log_path . 'logs-' . date("Ymd") . '.txt';
        } else {
            $this->log_file = FALSE;
        }

        if ($this->log_file) {
            $this->log_message = '';


            $message = "time: " . date("j-m-d H:i:s (T)", time()) . "\r\n";
            $message .= $msg . "\r\n";
            $message .= "##################################################\r\n\r\n";
            if (!$fp = fopen($this->log_file, 'a+')) {
                $this->log_message = "Could not open/create file: $this->log_file to log error.";
                $log_error = true;
            }
            if (!fwrite($fp, $message)) {
                $this->log_message = "Could not log error to file: $this->log_file. Write Error.";
                $log_error = true;
            }

            fclose($fp);
        }
    }
   
    function enviarMailConstanciaReparacion($persona, $reparacion) {

        //$template->set('title', 'Envío de notificaciones por mails');
//		if (($this->validateForm())) 
//		{
        //$view = $this->template->create('template');                    
//                    if(count($request->get('actividad', 'post',array())) > 0){
//                        $view->set('numero', 1);
//                        $view->set('actividades', $request->get('actividad', 'post',array()));                        
//                    }
        $sql = "SELECT * FROM personas WHERE persona ='" . $persona . "'";
        $persona_info = $this->database->getRow($sql);

        $sql = "SELECT * FROM vw_grilla_reparaciones WHERE reparacion ='" . $reparacion . "'";
        $reparacion_info = $this->database->getRow($sql);

        // <editor-fold defaultstate="collapsed" desc="TEXTO">
        $texto = "<div align=center>";
        $texto .= "<b>MUCHAS GRACIAS POR CONFIAR EN NOSOTROS PARA LA REPARACIÓN DE SU EQUIPO</b> <br><br>";
        $texto .="</div>";
        $texto .="<div>";
        $texto .= "<b>Detalle de la orden de reparación</b> <br>";

        $datosReparacion = '';
        $texto .='<br><b>Nro de Orden:</b> ' . $reparacion_info['reparacion'] . '<br><b>Fecha:</b> ' . $reparacion_info['fecha'] . '<br><b>Lugar ingreso:</b> ' . $reparacion_info['descpuntovta'] . '<br><b>Estado:</b> ' . $reparacion_info['descestadoreparacion'];
        //$view->set('datosReparacion', $datosReparacion);

        $datosCliente = '';
        $texto .='<br><br><b>Documento:</b> ' . $reparacion_info['cliente'] . '<br><b>Apellido y nombre:</b> ' . $reparacion_info['apellidocliente'] . ', ' . $reparacion_info['nombrecliente'] . '<br><b>Teléfono:</b> ' . $reparacion_info['telefonocliente'] . '<br><b>Mail:</b> ' . $reparacion_info['mailcliente'];
        //$view->set('datosCliente', $datosCliente);

        $datosEquipo = '';
        $engarantia = 'No.';
        if (@$reparacion_info['engarantia'] == 1) {
            $engarantia = 'Si.';
        }
        $texto .='<br><br><b>Nro Serie:</b> ' . $reparacion_info['nroserie'] . '<br><b>Tipo:</b> ' . $reparacion_info['desctipoproducto'] . '<br><b>Modelo:</b> ' . $reparacion_info['tipomarcamodelo'] . '<br><b>En garantía?:</b> ' . $engarantia;
        //$view->set('datosEquipo', $datosEquipo);

        $datosFalla = '';
        $texto .='<br><br><b>Accesorios:</b> ' . $reparacion_info['accesorios'] . '<br><br><b>Falla:</b> ' . $reparacion_info['descripcionfalla'];
        //$view->set('datosFalla', $datosFalla);

        $texto .= "<br><br>";

        $texto .="</div>";
        // </editor-fold>

        $this->template->set('texto_cuerpo', $texto);

        $cuerpo = $this->template->fetch('content/newsletter_layout_mail.tpl');
        $asunto = "Orden Nro: " . $reparacion;


        //$mailprueba = 'ing.lucasbracamonte@gmail.com';
        $mails[] = array();
        $mails[] = array('mail' => $persona_info['mail']);
        //$mails[] = array('mail' => $mail['mail']);
//                    foreach ($listamails as $mail) {
//                      $mails[] = array('mail' => $mail['mail']);   
//                    }
        // $mails[] = array('mail' => 'ing.lucasbracamonte@gmail.com'); 


        $respuesta_envio = $this->enviarMailMasivos($asunto, $mails, $cuerpo);

        if ($respuesta_envio == true) {
            $respuesta_envio = 'enviado ok';
        }

        //$response->redirect($url->ssl('home'));
        // $template->set('content', $this->getForm());
        // $template->set($module->fetch());
        // $response->set($template->fetch('layout.tpl'));
    }

    function enviarMailAvisoLlamarACliente($mail_responsable, $reparacion, $textofalla, $observaciontecnico) {

        //$template->set('title', 'Envío de notificaciones por mails');
//		if (($this->validateForm())) 
//		{
        //$view = $this->template->create('template');                    
//                    if(count($request->get('actividad', 'post',array())) > 0){
//                        $view->set('numero', 1);
//                        $view->set('actividades', $request->get('actividad', 'post',array()));                        
//                    }

        $sql = "SELECT * FROM vw_grilla_reparaciones WHERE reparacion ='" . $reparacion . "'";
        $reparacion_info = $this->database->getRow($sql);

        // <editor-fold defaultstate="collapsed" desc="TEXTO">
        $texto = "<div align=center>";
        $texto .= "<b>Llamar al cliente para aceptación de valor de reparación.</b> <br><br>";
        $texto .="</div>";
        $texto .="<div>";
        $texto .= "<b>Detalle de la orden de reparación</b> <br>";

        $datosReparacion = '';
        $texto .='<br><b>Nro de Orden:</b> ' . $reparacion_info['reparacion'] . '<br><b>Fecha:</b> ' . $reparacion_info['fecha'] . '<br><b>Lugar ingreso:</b> ' . $reparacion_info['descpuntovta'] . '<br><b>Estado:</b> ' . $reparacion_info['descestadoreparacion'];
        //$view->set('datosReparacion', $datosReparacion);

        $datosCliente = '';
        $texto .='<br><br><b>Documento:</b> ' . $reparacion_info['cliente'] . '<br><b>Apellido y nombre:</b> ' . $reparacion_info['apellidocliente'] . ', ' . $reparacion_info['nombrecliente'] . '<br><b>Teléfono:</b> ' . $reparacion_info['telefonocliente'] . '<br><b>Mail:</b> ' . $reparacion_info['mailcliente'];
        //$view->set('datosCliente', $datosCliente);

        $datosEquipo = '';
        $engarantia = 'No.';
        if (@$reparacion_info['engarantia'] == 1) {
            $engarantia = 'Si.';
        }
        $texto .='<br><br><b>Nro Serie:</b> ' . $reparacion_info['nroserie'] . '<br><b>Tipo:</b> ' . $reparacion_info['desctipoproducto'] . '<br><b>Modelo:</b> ' . $reparacion_info['tipomarcamodelo'] . '<br><b>En garanctía?:</b> ' . $engarantia;
        //$view->set('datosEquipo', $datosEquipo);

        $datosFalla = '';
        $texto .='<br><br><b>Accesorios:</b> ' . $reparacion_info['accesorios'] . '<br><br><b>Descripcion de la Falla:</b> ' . $reparacion_info['descripcionfalla'];
        //$view->set('datosFalla', $datosFalla);

        $datosFalla = '';
        $texto .='<br><br><b>Falla detectada:</b> ' . $textofalla;
        $texto .='<br><br><b>Observación del técnico:</b> ' . $observaciontecnico;

        $texto .= "<br><br>";

        $texto .="<b>VALOR DE LA REPARACIÓN: $" . $reparacion_info['precioreparacion'] . " </b>";

        $texto .="</div>";
        // </editor-fold>

        $this->template->set('texto_cuerpo', $texto);

        $cuerpo = $this->template->fetch('content/newsletter_layout_mail.tpl');
        $asunto = "Orden Nro: " . $reparacion;


        //$mailprueba = 'ing.lucasbracamonte@gmail.com';
        $mails[] = array();
        $mails[] = array('mail' => $mail_responsable);
        //$mails[] = array('mail' => $mail['mail']);
//                    foreach ($listamails as $mail) {
//                      $mails[] = array('mail' => $mail['mail']);   
//                    }
        // $mails[] = array('mail' => 'ing.lucasbracamonte@gmail.com'); 


        $respuesta_envio = $this->enviarMailMasivos($asunto, $mails, $cuerpo);

        if ($respuesta_envio == true) {
            $respuesta_envio = 'enviado ok';
        }

        //$response->redirect($url->ssl('home'));
        // $template->set('content', $this->getForm());
        // $template->set($module->fetch());
        // $response->set($template->fetch('layout.tpl'));
    }

    function enviarMailAvisoRetirar($reparacion) {

        //$template->set('title', 'Envío de notificaciones por mails');
//		if (($this->validateForm())) 
//		{
        //$view = $this->template->create('template');                    
//                    if(count($request->get('actividad', 'post',array())) > 0){
//                        $view->set('numero', 1);
//                        $view->set('actividades', $request->get('actividad', 'post',array()));                        
//                    }

        $sql = "SELECT * FROM vw_grilla_reparaciones WHERE reparacion ='" . $reparacion . "'";
        $reparacion_info = $this->database->getRow($sql);
        $sql = "SELECT direccion FROM puntosdeventa WHERE puntovta ='" . $reparacion_info['lugarentrega'] . "'";
        $direccionentrega = $this->database->getRow($sql);

        // <editor-fold defaultstate="collapsed" desc="TEXTO">
        $texto = "<div align=center>";
        $texto .= "<b>ORDEN FINALIZADA.</b> <br><br>";
        $texto .="</div>";
        $texto .="<div>";

        $texto .='<br>Estimada/o  ' . $reparacion_info['nombreapellidocliente'] . ' el equipo con Nro Serie ' . $reparacion_info['nroserie'] . ' de la Orden Nro ' . $reparacion_info['reparacion'] . ', se encuentra en el lugar de entrega (' . $direccionentrega['$direccion'] . ') listo para ser retirado. <br><br> Lo esperamos. <br><br> Saludos cordiales. <br> ';
        //$view->set('datosReparacion', $datosReparacion);
        // </editor-fold>

        $this->template->set('texto_cuerpo', $texto);

        $cuerpo = $this->template->fetch('content/newsletter_layout_mail.tpl');
        $asunto = "Orden Nro: " . $reparacion . " lista para retirar";


        //$mailprueba = 'ing.lucasbracamonte@gmail.com';
        $mails[] = array();
        $mails[] = array('mail' => $reparacion_info['mailcliente']);
        //$mails[] = array('mail' => $mail['mail']);
//                    foreach ($listamails as $mail) {
//                      $mails[] = array('mail' => $mail['mail']);   
//                    }
        // $mails[] = array('mail' => 'ing.lucasbracamonte@gmail.com'); 


        $respuesta_envio = $this->enviarMailMasivos($asunto, $mails, $cuerpo);

        if ($respuesta_envio == true) {
            $respuesta_envio = 'enviado ok';
        }

        //$response->redirect($url->ssl('home'));
        // $template->set('content', $this->getForm());
        // $template->set($module->fetch());
        // $response->set($template->fetch('layout.tpl'));
    }

    function EsVendedor($persona) {
        $es = false;
        $grupos = $this->user->getGrupos();
        foreach ($grupos as $unGrupo) {
            if ($unGrupo['Grupo'] == 'VE') {
                $es = true;
            }
        }

        return $es;
    }

    //va aca y no en el usuario porque puede cambiar sin cerrar sesion
    function getPuntoVtaAsignado() {
        $ptovtaasignado = $this->database->getRow("SELECT p.puntovtaasignado, pv.descripcion as descpuntovta, pv.listaprecio "
                . "FROM personas p "
                . "LEFT JOIN puntosdeventa pv ON p.puntovtaasignado = pv.puntovta where p.persona = '" . $this->user->getPERSONA() . "' ");

        return $ptovtaasignado;
    }

    function EsTecnico($persona) {
        $es = false;
        $grupos = $this->user->getGrupos();
        foreach ($grupos as $unGrupo) {
            if ($unGrupo['Grupo'] == 'TE') {
                $es = true;
            }
        }

        return $es;
    }

    function generaNumRepuesto($tipo) {

        $sql = "SELECT * FROM tiposrepuesto WHERE tiporepuesto ='" . utf8_encode($tipo) . "'";
        $repuesto_info = $this->database->getRow($sql);
        $cantidaddedigitos = strlen($repuesto_info['ultimonumero']);
        $numero = $repuesto_info['ultimonumero'];
        switch ($cantidaddedigitos) {
            case 1: $numero = '000000' . $numero;
                break;
            case 2: $numero = '00000' . $numero;
                break;
            case 3: $numero = '0000' . $numero;
                break;
            case 4: $numero = '000' . $numero;
                break;
            case 5: $numero = '00' . $numero;
                break;
            case 6: $numero = '0' . $numero;
                break;
            default:
                $numero = $numero;
                break;
        }
        $NroParte = $repuesto_info['codidentificador'] . '-' . $numero;

        $sql = "UPDATE tiposrepuesto SET ultimonumero=(ultimonumero+1) WHERE tiporepuesto='" . $repuesto_info['tiporepuesto'] . "'";
        $this->database->query($sql);

        return $NroParte;
    }

    function generaCodInternoProducto($tipo) {

        $sql = "SELECT * FROM tiposproducto WHERE tipoproducto ='" . utf8_encode($tipo) . "'";
        $producto_info = $this->database->getRow($sql);
        $cantidaddedigitos = strlen($producto_info['ultimonumero']);
        $numero = $producto_info['ultimonumero'];
        switch ($cantidaddedigitos) {
            case 1: $numero = '000000' . $numero;
                break;
            case 2: $numero = '00000' . $numero;
                break;
            case 3: $numero = '0000' . $numero;
                break;
            case 4: $numero = '000' . $numero;
                break;
            case 5: $numero = '00' . $numero;
                break;
            case 6: $numero = '0' . $numero;
                break;
            default:
                $numero = $numero;
                break;
        }
        $CodInterno = $producto_info['codidentificador'] . '-' . $numero;

        $sql = "UPDATE tiposproducto SET ultimonumero=(ultimonumero+1) WHERE tipoproducto='" . $producto_info['tipoproducto'] . "'";
        $this->database->query($sql);

        return $CodInterno;
    }
}

?>
