<?php

include_once (__DIR__ . '/wsfev1.php');
include_once (__DIR__ . '/wsfexv1.php');
include_once (__DIR__ . '/wsaa.php');
include_once (__DIR__ . '/neopdf/pdf_voucher.php');
/**
* Este script sirve para probar el webservice
* Hay que indicar el CUIT con el cual vamos a realizar las pruebas
* Recordar tener todos los servicios de homologación habilitados en AFIP
* Ejecutar desde consola con "php script_prueba.php"
*/
$CUIT = 30711081557;
$MODO = Wsaa::MODO_HOMOLOGACION;
//$MODO = Wsaa::MODO_PRODUCCION;

echo "----------Script de prueba de AFIP WSFEV1----------\n";
$afip = new Wsfev1($CUIT,$MODO);
$result = $afip->init();
$result2 = $afip->consultarPuntosVenta();
//$result3 = $afip->consultarTiposDocumento();
//$result4 = $afip->consultarTiposComprobante();
//$result5 = $afip->consultarAlicuotasIVA();
//$result6 = $afip->consultarMonedas();
$result7 = $afip->consultarCotizacionMoneda('PES');
$result8 = $afip->consultarCotizacionMoneda('DOL');



if ($result["code"] === Wsfev1::RESULT_OK) {
    $result = $afip->dummy();
    if ($result["code"] === Wsfev1::RESULT_OK) {
        $datos = print_r($result["msg"], TRUE);
        echo "Resultado: " . $datos . "\n";
    } else {
        echo $result["msg"] . "\n";
    }
} else {
    echo $result["msg"] . "\n";
}
echo "--------------Ejecución WSFEV1 finalizada-----------------\n";
//echo "----------Script de prueba de AFIP WSFEXV1----------\n";
//$afip = new Wsfexv1($CUIT,$MODO);
//$result = $afip->init();
//if ($result["code"] === Wsfexv1::RESULT_OK) {
//    $result = $afip->dummy();
//    if ($result["code"] === Wsfexv1::RESULT_OK) {
//        $datos = print_r($result["msg"], TRUE);
//        echo "Resultado: " . $datos . "\n";
//    } else {
//        echo $result["msg"] . "\n";
//    }
//} else {
//    echo $result["msg"] . "\n";
//}
//echo "--------------Ejecución WSFEXV1 finalizada-----------------\n";

echo "----------Script de prueba de factura----------\n";

//NEW VOUCHER
//20303088661_011_00002_00000096
//$voucher = array();

$voucher = array(
    'TipoComprobante' 		=>1, 
    'numeroComprobante' 	=>97,
    'letra'                     =>6,
    'numeroPuntoVenta' 		=>1,
    'fechaComprobante' 		=>intval(date('Ymd')),
    'codigoTipoComprobante' 	=>6,
    'codigoConcepto' 		=>1,
    'fechaDesde' 		=>NULL,
    'fechaHasta' 		=>NULL,
    'fechaVtoPago' 		=>NULL,
    'TipoDocumento' 		=>80,
    'numeroDocumento' 		=>20303088661,
    'nombreCliente' 		=>'Braca Lucas',
    'tipoResponsable' 		=>5,
    'domicilioCliente' 		=>'plf 1377',
    'CondicionVenta' 		=>NULL,
    'items' => array ( array(
        'scanner' 		=>'8410990015311',
        'codigo' 		=>123,
        'descripcion' 		=>'palo y a la bolsa',
        'cantidad' 		=>1,
        'UnidadMedida' 		=>'UN',
        'precioUnitario' 	=>115,
        'porcBonif' 		=>0,
        'impBonif' 		=>0,
        'Alic'                  =>0,
        'importeItem' 		=>115
        )),
    'Tributos' => array ( array (
        'Desc' 		=>'Ingresos Brutos',
        'Importe'	=>7.8
        )),
    'importeOtrosTributos' 	=>0,
    'codigoMoneda' 		=>'PES',
    'importeGravado' 		=>115,
    'subtotivas' 		=>115,
    'importeTotal' 		=>115,
    'cae'                       =>NULL,
    'fechaVencimientoCAE' 	=>NULL,
    'cotizacionMoneda' 		=>1, 
);

//NEW CONFIG
//private $config = array();
$vconfig = array();
$vconfig["footer"] = false;
$vconfig["total_line"] = false;
$vconfig["header"] = false;
$vconfig["receiver"] = false;
$vconfig["footer"] = false;
$vconfig["TRADE_SOCIAL_REASON"] = 'JBIngenieria';
$vconfig["TRADE_CUIT"] = '30711081557';
$vconfig["TRADE_ADDRESS"] = 'PLF 1377';
$vconfig["TRADE_TAX_CONDITION"] = 'Excento';
$vconfig["TRADE_INIT_ACTIVITY"] = '01/11/2014';
$vconfig["TYPE_CODE"] = 'codigo';
$vconfig["VOUCHER_OBSERVATION"] = false;
$vconfig["VOUCHER_CONFIG"] = false;
ob_start();
$logo_path = __DIR__ . '/neopdf/image/uno.jpg';
$pdf = new PDFVoucher($voucher, $vconfig);
$pdf->emitirPDF($logo_path);
//ob_end_clean();//rompimiento de pagina
ob_clean();
$pdf->Output("nombre_archivo.pdf","I");
//die();
echo "--------------Ejecución WSFEXV1 finalizada-----------------\n";